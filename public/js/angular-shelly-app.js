angular.module('shellyApp', ['ngResource','angular-input-stars','notifications','ui.bootstrap'])
  .service('CategoryService',function($q,$resource){
        return $resource('backend/api/categories/:CategoryId',{},{
            'ecommerce' : { method: 'GET' , url: 'backend/api/categories/ecommerce/' , isArray:true }
        });
    })
  .controller('HomeController', function(CategoryService,$window,$scope,$http) {
    
    var home = this;
    
    $scope.featured_categories = [];
    $scope.categories = [];
    $scope.featured_products = [];
    $scope.UserIsLoggedIn = false;
    $scope.user_id = 0;
    
    if($window.sessionStorage["user"]){
            
        $scope.info = JSON.parse($window.sessionStorage["user"]); 
        
        if($scope.info.user)
        {
            $scope.user_id = $scope.info.user.UserId;
            $scope.UserIsLoggedIn = true;
        }
            
    }
    
   
    CategoryService.ecommerce(function(response){

   
        $scope.categories = response;
        
               
    });
    
    
    $scope.showFeaturedProducts = function(category_id){
        
        console.log('click');
        
        $http({
            url: 'backend/api/products/featured/', 
            method: "GET",
            params: { category_id :category_id}
         }).success(function(response){
                
                console.log(response);
                $scope.featured_category_products = response;
            })
            .error(function(error){
                
                console.log(error);
            });
        
    };
    
     
    $http({
            url: 'backend/api/products/featured/', 
            method: "GET",
            params: { user_id : $scope.user_id }
            
    }).success(function(response){
                
        console.log(response);
        $scope.featured_products = response;
        
    }).error(function(error){
                
        console.log(error);
        
    });
    
    
    $http({
            url: 'backend/api/categories/featured/', 
            method: "GET",
            params: { }
            
         }).success(function(response){
                
                $scope.featured_categories = response;
            })
            .error(function(error){
                
                console.log(error);
                
            });
            
    $scope.showFeaturedProducts = function(category_id){
        
        console.log('click');
        
        $http({
            url: 'backend/api/products/featured/', 
            method: "GET",
            params: { category_id :category_id}
         }).success(function(response){
                
                console.log(response);
                $scope.featured_category_products = response;
            })
            .error(function(error){
                
                console.log(error);
            });
        
    };
    
     
    $http({
            url: 'backend/api/products/recommended/', 
            method: "GET",
            params: { user_id : $scope.user_id }
    }).success(function(response){
                
                console.log(response);
                $scope.recommended_products = response;
    })
            .error(function(error){
                
                console.log(error);
    });
    
            
    

 
    
 
   
  })
  .controller('ProductController', function(CategoryService,$window,$scope,$http,$notification) {
      
      
      $scope.user_id = 0;
      $scope.UserIsLoggedIn  = false; 
      $scope.user_id = 0;
      
      
      
    //$scope.data = [{"name":"Bell","id":"K0H 2V5"},{"name":"Octavius","id":"X1E 6J0"},{"name":"Alexis","id":"N6E 1L6"},{"name":"Colton","id":"U4O 1H4"},{"name":"Abdul","id":"O9Z 2Q8"},{"name":"Ian","id":"Q7W 8M4"},{"name":"Eden","id":"H8X 5E0"},{"name":"Britanney","id":"I1Q 1O1"},{"name":"Ulric","id":"K5J 1T0"},{"name":"Geraldine","id":"O9K 2M3"},{"name":"Hamilton","id":"S1D 3O0"},{"name":"Melissa","id":"H9L 1B7"},{"name":"Remedios","id":"Z3C 8P4"},{"name":"Ignacia","id":"K3B 1Q4"},{"name":"Jaime","id":"V6O 7C9"},{"name":"Savannah","id":"L8B 8T1"},{"name":"Declan","id":"D5Q 3I9"},{"name":"Skyler","id":"I0O 4O8"},{"name":"Lawrence","id":"V4K 0L2"},{"name":"Yael","id":"R5E 9D9"},{"name":"Herrod","id":"V5W 6L3"},{"name":"Lydia","id":"G0E 2K3"},{"name":"Tobias","id":"N9P 2V5"},{"name":"Wing","id":"T5M 0E2"},{"name":"Callum","id":"L9P 3W5"},{"name":"Tiger","id":"R9A 4E4"},{"name":"Summer","id":"R4B 4Q4"},{"name":"Beverly","id":"M5E 4V4"},{"name":"Xena","id":"I8G 6O1"},{"name":"Yael","id":"L1K 5C3"},{"name":"Stacey","id":"A4G 1S4"},{"name":"Marsden","id":"T1J 5J3"},{"name":"Uriah","id":"S9S 8I7"},{"name":"Kamal","id":"Y8Z 6X0"},{"name":"MacKensie","id":"W2N 7P9"},{"name":"Amelia","id":"X7A 0U3"},{"name":"Xavier","id":"B8I 6C9"},{"name":"Whitney","id":"H4M 9U2"},{"name":"Linus","id":"E2W 7U1"},{"name":"Aileen","id":"C0C 3N2"},{"name":"Keegan","id":"V1O 6X2"},{"name":"Leonard","id":"O0L 4M4"},{"name":"Honorato","id":"F4M 8M6"},{"name":"Zephr","id":"I2E 1T9"},{"name":"Karen","id":"H8W 4I7"},{"name":"Orlando","id":"L8R 0U4"},{"name":"India","id":"N8M 8F4"},{"name":"Luke","id":"Q4Y 2Y8"},{"name":"Sophia","id":"O7F 3F9"},{"name":"Faith","id":"B8P 1U5"},{"name":"Dara","id":"J4A 0P3"},{"name":"Caryn","id":"D5M 8Y8"},{"name":"Colton","id":"A4Q 2U1"},{"name":"Kelly","id":"J2E 2L3"},{"name":"Victor","id":"H1V 8Y5"},{"name":"Clementine","id":"Q9R 4G8"},{"name":"Dale","id":"Q1S 3I0"},{"name":"Xavier","id":"Z0N 0L5"},{"name":"Quynn","id":"D1V 7B8"},{"name":"Christine","id":"A2X 0Z8"},{"name":"Matthew","id":"L1H 2I4"},{"name":"Simon","id":"L2Q 7V7"},{"name":"Evan","id":"Z8Y 6G8"},{"name":"Zachary","id":"F4K 8V9"},{"name":"Deborah","id":"I0D 4J6"},{"name":"Carl","id":"X7H 3J3"},{"name":"Colin","id":"C8P 0O1"},{"name":"Xenos","id":"K3S 1H5"},{"name":"Sonia","id":"W9C 0N3"},{"name":"Arsenio","id":"B0M 2G6"},{"name":"Angela","id":"N9X 5O7"},{"name":"Cassidy","id":"T8T 0Q5"},{"name":"Sebastian","id":"Y6O 0A5"},{"name":"Bernard","id":"P2K 0Z5"},{"name":"Kerry","id":"T6S 4T7"},{"name":"Uriel","id":"K6G 5V2"},{"name":"Wanda","id":"S9G 2E5"},{"name":"Drake","id":"G3G 8Y2"},{"name":"Mia","id":"E4F 4V8"},{"name":"George","id":"K7Y 4L4"},{"name":"Blair","id":"Z8E 0F0"},{"name":"Phelan","id":"C5Z 0C7"},{"name":"Margaret","id":"W6F 6Y5"},{"name":"Xaviera","id":"T5O 7N5"},{"name":"Willow","id":"W6K 3V0"},{"name":"Alden","id":"S2M 8C1"},{"name":"May","id":"L5B 2H3"},{"name":"Amaya","id":"Q3B 7P8"},{"name":"Julian","id":"W6T 7I6"},{"name":"Colby","id":"N3Q 9Z2"},{"name":"Cole","id":"B5G 0V7"},{"name":"Lana","id":"O3I 2W9"},{"name":"Dieter","id":"J4A 9Y6"},{"name":"Rowan","id":"I7E 9U4"},{"name":"Abraham","id":"S7V 0W9"},{"name":"Eleanor","id":"K7K 9P4"},{"name":"Martina","id":"V0Z 5Q7"},{"name":"Kelsie","id":"R7N 7P2"},{"name":"Hedy","id":"B7E 7F2"},{"name":"Hakeem","id":"S5P 3P6"}];
    $scope.totalItems = 0;//$scope.data.length;
    
    
    $scope.viewby = 12;
    
    $scope.currentPage =0;
    $scope.itemsPerPage = $scope.viewby;
    $scope.maxSize = 5; //Number of pager buttons to show
    $scope.numPages = 0;
  
    
    var product = this;
    
    var category_id = "";
    var search="";
    //var page=0;
    //var $scope.selected_category = 0;
  
    
    
    if($window.sessionStorage["user"]){
            
        $scope.info = JSON.parse($window.sessionStorage["user"]); 
        
        if($scope.info.user){
            
            $scope.UserIsLoggedIn = true;
            $scope.user_id = $scope.info.user.UserId;
        }
        //console.log($scope.info);
          
            
    }
    
   
    
    
   
    CategoryService.ecommerce(function(response){

   
        $scope.categories = response;
        
               
    });
    
    
    $scope.showFeaturedProducts = function(category_id){
        
        console.log('click');
        
        $http({
            url: 'backend/api/products/by_category/'+category_id, 
            method: "GET",
            params:  { category_id :category_id , user_id: $scope.user_id }
         }).success(function(response){
                
                console.log(response);
                $scope.featured_products = response;
            })
            .error(function(error){
                
                console.log(error);
            });
        
    };
    
    var parm_category_id = getParameterByName('category_id');
    var parm_parent_category_id = getParameterByName('parent_category_id');
    var parm_product_search = getParameterByName('product_search');
    //var parm_page = getParameterByName('page');
    
    
    if(parm_parent_category_id){
        $scope.selected_category = parseInt(parm_parent_category_id);
    }
    
   if(parm_category_id && parseInt(parm_category_id)>0){
       category_id=parm_category_id
   }
   
   var api_url = 'backend/api/products/by_category/'+category_id;
   
   if(parm_product_search && parm_product_search !== "" ){
       
       api_url = 'backend/api/products/by_category/?product_search='+parm_product_search;
   }
   
   /*if(parm_page && parseInt(parm_page) > 0){
       
       page = parseInt(parm_page);
       $scope.currentPage =page;
       
      
         
         //api_url = api_url +'&page'+page;
   }*/
   
   
   
   
       
   $http({
        url: api_url, 
        method: "GET",
        params:  { page : $scope.currentPage, user_id : $scope.user_id }
    }).success(function(response){
            
            //console.log(response);
            $scope.totalItems = response.count;
            $scope.featured_products = response.data;
            
    })
        .error(function(error){
            
            console.log(error);
    });
       
   
    
   
    
      $scope.pageChanged = function() {
          
         $window.sessionStorage["currentPage"] = $scope.currentPage;
          
        console.log('Page changed to: ' + $scope.currentPage);
        
        
          $http({
            url: api_url, 
            method: "GET",
            params:  { page : $scope.currentPage }
            
        }).success(function(response){
                
            $scope.totalItems = response.count;
            $scope.featured_products = response.data;
        })
            .error(function(error){
                
                console.log(error);
        });
        
      };
    
   /* $scope.setItemsPerPage = function(num) {
      $scope.itemsPerPage = num;
      $scope.currentPage = 1; //reset to first page
    } */
                
    
    
    
    

 
    
 
   
  })
  .controller('ProductDetailsController', function(CategoryService,$window,$scope,$http) {
    
    var product = this;
    $scope.UserIsLoggedIn = false;
    $scope.user_id = 0;
  
    
    if($window.sessionStorage["user"]){
            
        $scope.info = JSON.parse($window.sessionStorage["user"]); 
        
        if($scope.info.user){
            
            $scope.UserIsLoggedIn = true;
            $scope.user_id = $scope.info.user.UserId;
        }
            
    }
    
   
    CategoryService.ecommerce(function(response){

   
        $scope.categories = response;
        
               
    });
    
   var parm_product_id = getParameterByName('product_id');
    

    
   if( parm_product_id && parseInt(parm_product_id)>0){
       
       $http({
            url: 'backend/api/products/ecommerce/'+parm_product_id, 
            method: "GET",
            params:  { }
         }).success(function(response){
                
                console.log(response);
                product.info = response;
                product.info.ProductQuantity = 1;
                //product.info.rating = 5;
            })
            .error(function(error){
                
                console.log(error);
            });
       
   }

    product.createReview = function(review){
        
         var request = $http.post( 'backend/api/products/ecommerce/'+product.info.ProductId+'/review', 
                                review,
                                { 
                                    headers : { 
                                        'Content-Type' : 'application/json',
                                        'Authorization' : 'Bearer ' + $scope.info.token.access_token 
                                    }
                                }
            );

            return request.then( function( response ) {
                
                product.info.reviews.push(response.data);
                
                review = {};
    
            });
    };

  })
  .controller('UserController', function($http,$window,$scope,$notification){
    
        var user =  this;

        if($window.sessionStorage["user"]){
            
            $scope.info = JSON.parse($window.sessionStorage["user"]); 
          
        }
        
        
        var recovery_token = getParameterByName('token');
        
        if(recovery_token){
            user.recovery_token = recovery_token;
        }
    
        user.login = function() {
        
            $http.post( 'backend/api/login', 
                                  { email: user.email, password: user.password },
                                  { headers : { 'Content-Type' : 'application/x-www-form-urlenconded'}
            }).success(function(response){
                
                
                
                $window.sessionStorage["user"] = JSON.stringify(response);
               
                
                $notification.success("Bienvenido", "Acceso correcto!");
                
                
                $window.location.href = 'index.html';
              //return currentUser.isAuthenticated();
              
                
            }).error(function(response){
                
                
                $notification.error("Error", "Acceso incorrecto!");
                  
            });

            
      };
      
      user.recover = function() {
        
            $http.post( 'cart/public/v1/open/user/recovery/', 
                                  { email: user.email },
                                  { headers : { 'Content-Type' : 'application/json'}
            }).success(function(response){
                
                
                
                //$window.sessionStorage["user"] = JSON.stringify(response);
               
                
                $notification.success("Recuperar", "Se ha enviado un mensaje de recuperación a su correo electrnico!");
                
                
                $window.location.href = 'password-enviado.html';
              
              
                
            }).error(function(response){
                
                
                $notification.error("Error", "Email incorrecto!");
                  
            });

            
      };
      
       user.change_password = function() {
        
            $http.post( 'backend/api/users/setnewpassword/', 
                                  { user: user },
                                  { headers : { 'Content-Type' : 'application/json'}
            }).success(function(response){
                
                
                
                //$window.sessionStorage["user"] = JSON.stringify(response);
               
                
                //$notification.success("Recuperar", "Se ha enviado un mensaje de recuperación a su correo electrnico!");
                
                
                $window.location.href = 'iniciar-sesion.html';
              
              
                
            }).error(function(response){
                
                
                $notification.error("Error", "Email incorrecto!");
                  
            });

            
      };
      
        user.create = function(){
            
            user.error = {};
            
            $http.post( 'backend/api/ecommerce_users', 
                                  { name: user.new_name, email: user.new_email, password: user.new_password },
                                  { headers : { 'Content-Type' : 'application/json'} }
            ).success(function( response ) {
            
            

                $window.sessionStorage["user"] = JSON.stringify(response);
              
              
                $http.post( 'cart/public/v1/user/'+response.user.UserId+'/welcome/', 
                                 {},
                                 
                                  { headers : { 'Content-Type' : 'application/json', 'X-Token' : response.token.access_token}
                              
                }).success(function(response){
                
                 

                $notification.success("Bienvenido", "El articulo fue agregado al carrito de compra");
                
                $window.location.href = 'registro-satisfactorio.html';
                
            }).error(function(response){

               user.error.message = response.data.error;

            });
                                  
               
            
              //return currentUser.isAuthenticated();
            });
      };
      
      user.info = function() {
        
      };

    
  })
  .controller('CartController',function($http,$window,$scope,$location,$notification){
      
      var cart = this;
      
      $scope.info = {};
      
      console.log($location.absUrl());
      
      if($window.sessionStorage["user"]){
            
            $scope.info = JSON.parse($window.sessionStorage["user"]); 
          
           
          
      } else {
         
          
      }
    
      cart.add = function(product_id,product_qty){
          
          //var data = $.param({ item_id: 1, quantity: 5 });
          
          if($scope.info.user){
          
          var data = new FormData();
            data.append('item_id', product_id);
            data.append('quantity', product_qty);
    
    
          
           $http.post( 'cart/public/v1/cart/'+$scope.info.user.UserId+'/add', 
                                 data,
                                 
                                  { headers : { 'Content-Type' : undefined, 'X-Token' : $scope.info.token.access_token},
                                  transformRequest: angular.identity
            }).success(function(response){
                
                console.log(response);
                $notification.success("Correcto", "El articulo fue agregado al carrito de compra");
                cart.load();
            })
            .error(function(response){
                
                console.log(response.error);
                if(response.error ==='Invalid Auth'){
                    response.error = "Debe iniciar sesión para agregar articulos al carrito de compra";
                }
                  $notification.error("Error", response.error);
            });
          }
          else {
              
              $notification.error("Inicie sesión","Inicie sesión para agregar articulos al carrito");
          }
          
      };
      
      cart.update = function(item,quantity_to_add_or_delete){
          
          
          var product_id  = item.item.product_id;
          var product_qty = parseInt(item.quantity) + parseInt(quantity_to_add_or_delete);
          
          if(product_qty > 0){
          var data = new FormData();
            data.append('item_id', product_id);
            data.append('quantity', product_qty);
    
    
          
           $http.post( 'cart/public/v1/cart/'+$scope.info.user.UserId+'/update', 
                                 data,
                                 
                                  { headers : { 'Content-Type' : undefined, 'X-Token' : $scope.info.token.access_token},
                                  transformRequest: angular.identity
            }).success(function(response){
                
                item.quantity = product_qty;
                cart.load();
            })
            .error(function(error){
                
                console.log(error);
            });
          }
          else {
              
              this.remove(product_id);
          }
          
      };
      
      cart.remove = function(item){
          
          var product_id  = item.item.product_id;
            
            var data = new FormData();
            data.append('item_id', product_id);
            
    
    
          
           $http.post( 'cart/public/v1/cart/'+$scope.info.user.UserId+'/remove', 
                                 data,
                                 
                                  { headers : { 'Content-Type' : undefined, 'X-Token' : $scope.info.token.access_token},
                                  transformRequest: angular.identity
            }).success(function(response){
                
                var index = cart.items.indexOf(item,0); 
            
                if (index != -1 )
                {
    
                    cart.items.splice(index,1) ;        
                    cart.load();
                    //$notification.success(category.CategoryName, 'La categoria se elimino correctamente');
                }
                //item.quantity = parseInt(product_qty) + parseInt(qty);
                
            })
            .error(function(error){
                
                console.log(error);
            });
            
          
      };
      
      cart.load = function(){
        
        if($scope.info.user){
        
            $http({
                url: 'cart/public/v1/cart/'+$scope.info.user.UserId+'/get/items', 
                method: "GET",
                params: { },
                headers : { 'Content-Type' : undefined, 'X-Token' : $scope.info.token.access_token},
            }).success(function(response){
                
                console.log(response);
                cart.items = response.items;
                cart.shipping = response.shipping;
            })
            .error(function(error){
                
                console.log(error);
            });
        }
        
          
      };
      
     cart.calculateTotals = function(){
          
          var taxes = 0;
            var subtotal = 0;
            var discounts = 0;
				        
			if(cart.items){
          	for(var i=0, len=cart.items.length; i < len ; i ++)
						{
							

							cart.items[i].total = cart.items[i].item.product_price * cart.items[i].quantity;
                            
							var discount = 0;

						

            				var tax = 0;//total * ( cart.items[i].SaleOrderDetailTax / 100);

            				cart.items[i].taxes = tax;

							taxes = taxes + tax ;
							
							 cart.items[i].subtotal = ( cart.items[i].total + tax ) - discount;

				            if(cart.items[i].discount > 0){
				                
				               discounts = discounts + discount; 
				               
				            }
                            
                            subtotal += cart.items[i].total ;

						};

						cart.subtotal = subtotal;
				        cart.taxes = taxes;
				        cart.discounts = discounts;
						cart.total = cart.subtotal +  cart.taxes -  cart.discounts  + parseFloat(cart.shipping.Amount);

			}
        }
      
      $scope.$watch('cart.items', cart.calculateTotals , true);
      	
      //Cargar solo si estas en la pagina del carrito	
      if(window.location.pathname.indexOf("carrito-compras.html") > -1){
          
            if(!$window.sessionStorage["user"]){
         $window.location.href = 'iniciar-sesion.html'; }
         
        cart.load();    
      }
      
  })
  .controller('AccountController',function($http,$window,$scope,$location,$notification){
      
      var account = this;
      
      $scope.info = {};
      $scope.states = [{"name":"Aguascalientes","id":"AG"},{"name":"Baja   California Norte","id":"BC"},{"name":"Baja   California Sur","id":"BS"},{"name":"Campeche","id":"CM"},{"name":"Chiapas","id":"CS"},{"name":"Chihuahua","id":"CH"},{"name":"Coahuila","id":"CO"},{"name":"Colima","id":"CL"},{"name":"Distrito Federal","id":"DF"},{"name":"Durango","id":"DG"},{"name":"Guanajuato","id":"GT"},{"name":"Guerrero","id":"GR"},{"name":"Hidalgo","id":"HG"},{"name":"Jalisco","id":"JA"},{"name":"Mexico","id":"MX"},{"name":"Michoacán","id":"MI"},{"name":"Morelos","id":"MO"},{"name":"Nayarit","id":"NA"},{"name":"Nuevo Leon","id":"NL"},{"name":"Oaxaca","id":"OA"},{"name":"Puebla","id":"PU"},{"name":"Queretaro","id":"QT"},{"name":"Quintana Roo","id":"QR"},{"name":"San Luis Potosi","id":"SL"},{"name":"Sinaloa","id":"SI"},{"name":"Sonora","id":"SO"},{"name":"Tabasco","id":"TB"},{"name":"Tamaulipas","id":"TM"},{"name":"Tlaxcala","id":"TL"},{"name":"Veracruz","id":"VE"},{"name":"Yucatan","id":"YU"},{"name":"Zacatecas","id":"ZA"}];
      
      //console.log($location.absUrl());
      
      if($window.sessionStorage["user"]){
            
            $scope.info = JSON.parse($window.sessionStorage["user"]); 
          
            
        } 
        else {
        
      }
    
      account.logout = function() {
        
          $window.sessionStorage.removeItem("user");
          $window.location.href = 'index.html';
            
      };
      
      account.update = function(){
          
          
           if($scope.info.customer.customer_address === null || $scope.info.customer.customer_address === ""){
               
               $notification.error("Error", "Debe capturar una dirección valida!");
               
               return;
           }
          
          
          if($scope.info.customer.customer_delivery_place === null || $scope.info.customer.customer_delivery_place === ""){
               
               $notification.error("Error", "Debe capturar una dirección de entrega valida!");
               
               return;
           }
           
           if($scope.info.customer.customer_city === null || $scope.info.customer.customer_city === ""){
               
               $notification.error("Error", "Debe capturar una ciudad valida!");
               
               return;
           }
           
           if($scope.info.customer.customer_state === null || $scope.info.customer.customer_state === ""){
               
               $notification.error("Error", "Debe capturar un estado valido!");
               
               return;
           }
           
           if($scope.info.customer.customer_cp === null || $scope.info.customer.customer_cp === ""){
               
               $notification.error("Error", "Debe capturar un codigo postal valido!");
               
               return;
           }
           
           if($scope.info.customer.customer_cellphone === null || $scope.info.customer.customer_cellphone === ""){
               
               $notification.error("Error", "Debe capturar un numero de celular valido!");
               
               return;
           }
           
           console.log($scope.info.customer);
          
          
        
            var request = $http.post( 'backend/api/ecommerce_users/update', 
                                  { user : $scope.info.user , customer: $scope.info.customer },
                                  { 
                                      headers : 
                                        { 'Content-Type' : 'application/json', 'Authorization' : 'Bearer ' + $scope.info.token.access_token}
            });

            return request.then( 
            
                function( response ) {
                
                  
                  /*$window.sessionStorage["user"].user = JSON.stringify(response.data.user);
                  $window.sessionStorage["user"].customer = JSON.stringify(response.data.customer);
                  $window.sessionStorage["user"].token = $scope.info.token;*/
                  var user = JSON.parse($window.sessionStorage["user"]);
                  //user.user = response.data.user;
                  user.customer = response.data.customer;
                  
                  $window.sessionStorage["user"] = JSON.stringify(user);
                  
                  console.log(response);
                $notification.success("Correcto", "Se actualizo la cuenta correctamente!");
                
                    
                   
                      
                });
                                  
               
            
              //return currentUser.isAuthenticated();
            
      };
      
      account.loadOrders = function(){
        
        if($scope.info.user){
        
            $http({
                url: 'cart/public/v1/cart/'+$scope.info.user.UserId+'/get/orders', 
                method: "GET",
                params: { },
                headers : { 'Content-Type' : undefined, 'X-Token' : $scope.info.token.access_token},
            }).success(function(response){
                
                console.log(response);
                account.orders = response.orders;
            })
            .error(function(error){
                
                console.log(error);
            });
        }
        
          
      };
      

      //Cargar solo si estas en la pagina del carrito	
      if(window.location.pathname.indexOf("mi-cuenta.html") > -1){
          
             if(!$window.sessionStorage["user"]){
         $window.location.href = 'iniciar-sesion.html'; }
         
         
        account.loadOrders();    
      }
      
  })
  .controller('CheckoutController',function($http,$window,$scope){
    
    var checkout = this;    
    
    if($window.sessionStorage["user"]){
            
        $scope.info = JSON.parse($window.sessionStorage["user"]); 
        
    }else{
           if(!$window.sessionStorage["user"]){
         $window.location.href = 'iniciar-sesion.html'; }
    }
    
    checkout.load = function(){
          
       
          $http({
            url: 'cart/public/v1/cart/'+$scope.info.user.UserId+'/get/items', 
            method: "GET",
            params: { },
            headers : { 'Content-Type' : undefined, 'X-Token' : $scope.info.token.access_token},
         }).success(function(response){
                
                console.log(response);
                
                checkout.items = response.items;
                checkout.shipping = response.shipping;
            })
            .error(function(error){
                
                console.log(error);
            });
          
      };
      
      checkout.calculateTotals = function(){
          
          var taxes = 0;
            var subtotal = 0;
            var discounts = 0;
				        
			if(checkout.items){
          	for(var i=0, len=checkout.items.length; i < len ; i ++)
			{
							

							checkout.items[i].total = checkout.items[i].item.product_price * checkout.items[i].quantity;
                            
							var discount = 0;

						

            				var tax = 0;//total * ( checkout.items[i].SaleOrderDetailTax / 100);

            				checkout.items[i].taxes = tax;

							taxes = taxes + tax ;
							
							 checkout.items[i].subtotal = ( checkout.items[i].total + tax ) - discount;

				            if(checkout.items[i].discount > 0){
				                
				               discounts = discounts + discount; 
				               
				            }
                            
                            subtotal += checkout.items[i].total ;

						};

						checkout.subtotal = subtotal;
				        checkout.taxes = taxes;
				        checkout.discounts = discounts;
						//checkout.total = checkout.subtotal +  checkout.taxes -  checkout.discounts;
						checkout.total = checkout.subtotal +  checkout.taxes -  checkout.discounts  + parseFloat(checkout.shipping.Amount);

			}
        };
      
        $scope.$watch('checkout.items', checkout.calculateTotals , true);
      	

      
       if(window.location.pathname.indexOf("checkout.html") > -1){
        checkout.load();    
      }
      
  })
  .controller('MenuController',function($http,$window,$scope){
      })
  .controller('SearchController',function($window,$scope,$http,$notification) {
      
      $scope.search = function(search_string){
        
        var parm_category_id = getParameterByName('category_id');
        var parm_parent_category_id = getParameterByName('parent_category_id');
        //var parm_product_search = getParameterByName('product_search');
        var query = 'productos.html';
        
        var amp = '?';
        
        if(parm_category_id){
            
           
            query = query + amp + 'category_id='+parm_category_id;
            
             amp = '&';
            
        }
            
         if(parm_parent_category_id && parseInt(parm_parent_category_id) > 0){
             
            query = query + amp +'parent_category_id='+parm_parent_category_id;
               
         }
         
         if(search_string)
         {
              query = query + amp +'product_search='+search_string;
             
         }
   
   
    
        $window.location.href = query ;
        
        
    };
  })
  .filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace !== -1) {
                  //Also remove . and , so its gives a cleaner result.
                  if (value.charAt(lastspace-1) === '.' || value.charAt(lastspace-1) === ',') {
                    lastspace = lastspace - 1;
                  }
                  value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    })
  .directive('ngEnter', function() {
        return function(scope, elem, attrs) {
          elem.bind("keydown keypress", function(event) {
            // 13 represents enter button
            if (event.which === 13) {
              scope.$apply(function() {
                scope.$eval(attrs.ngEnter);
              });
    
              event.preventDefault();
            }
          });
        };
     });
    
  
  
  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
