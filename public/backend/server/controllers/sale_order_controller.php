<?php
    require 'server/models/folio_model.php';
    require 'server/models/sale_order_model.php';
    require 'server/reports/sale_order_report.php';
    require 'server/reports/receipt_report.php';

		//Obtener SaleOrders
	$app->get('/api/sale_orders/',$checkToken(),$checkScopes(array('sales.view')),function () use ($app){

        
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        
		$SaleOrderModel = new SaleOrderModel();

		$data = [];
		$data = $SaleOrderModel->getSaleOrders();
    
    	$json =  json_encode($data,JSON_NUMERIC_CHECK);
    	$response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

    $app->get('/api/sale_orders/approved/',$checkToken(),$checkScopes(array('sales.view')),function () use ( $app ) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        
        $SaleOrderModel = new SaleOrderModel();

        $data = [];
        $data = $SaleOrderModel->getApprovedSaleOrders();
    
        $json =  json_encode($data,JSON_NUMERIC_CHECK);
    	$response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        
    });
    
    $app->get('/api/sale_orders/facturadas/',$checkToken(),$checkScopes(array('sales.view')),function () use ( $app ) {
        
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

        $SaleOrderModel = new SaleOrderModel();

        $data = [];
        $data = $SaleOrderModel->getFacturadasSaleOrders();
  
        $json =  json_encode($data,JSON_NUMERIC_CHECK);
    	$response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

    $app->get('/api/sale_orders/canceled/',$checkToken(),$checkScopes(array('sales.view')),function () use ( $app ){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        $SaleOrderModel = new SaleOrderModel();

        $data = [];
        $data = $SaleOrderModel->getCanceledSaleOrders();
    
        $json =  json_encode($data,JSON_NUMERIC_CHECK);
    	$response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

      $app->get('/api/sale_orders/completed/',$checkToken(),$checkScopes(array('sales.view')),function () {

        $SaleOrderModel = new SaleOrderModel();

        $data = [];
        $data = $SaleOrderModel->getCompletedSaleOrders();
    
        echo json_encode($data,JSON_NUMERIC_CHECK);
    });



    $app->get('/api/sale_orders/:id',$checkToken(),$checkScopes(array('sales.view')), function ($id) {
       
       $SaleOrderModel = new SaleOrderModel();

       $SaleOrderJson = $SaleOrderModel->getSaleOrder($id);

        echo json_encode($SaleOrderJson,JSON_NUMERIC_CHECK);
    });

	$app->get('/api/sale_orders/get_by_quote/:id',$checkToken(),$checkScopes(array('sales.view')), function ($id) {
       
       $SaleOrderModel = new SaleOrderModel();

       $SaleOrderJson = $SaleOrderModel->getSaleOrderByQuoteId($id);

    	echo json_encode($SaleOrderJson,JSON_NUMERIC_CHECK);
	});


	//Crear orden de venta
	$app->post('/api/sale_orders',$checkToken(),$checkScopes(array('sales.view')),function() use ($app){
		 
        $app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$body = $request->getBody();
    	$SaleOrderReceived = json_decode($body);
    	//var_dump($SaleOrderReceived);
    	$SaleOrderModel = new SaleOrderModel();
    	$SaleOrderJson =  $SaleOrderModel->createSaleOrder($SaleOrderReceived);

    	echo json_encode($SaleOrderJson,JSON_NUMERIC_CHECK);
	});

	//Actualizar una orden de venta
	$app->post('/api/sale_orders/:id',$checkToken(),$checkScopes(array('sales.view')),function($id) use ($app){

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$body = $request->getBody();

    	$SaleOrderReceived = json_decode($body);
    	$SaleOrderReceived->SaleOrderId = $id;
    	//validaciones antes de actualizar
    
    	$SaleOrderModel = new SaleOrderModel();
    	$SaleOrderReceived=$SaleOrderModel->updateSaleOrder($SaleOrderReceived);

        echo json_encode($SaleOrderReceived,JSON_NUMERIC_CHECK);

	});

    //Eliminar una orden de venta
    $app->delete('/api/sale_orders/:id',$checkToken(),$checkScopes(array('sales.view')),function($id) use ($app){
        $SaleOrderModel = new SaleOrderModel();
        $SaleOrderModel->deleteSaleOrder($id);
    });


    //Aprobar una orden de venta
    $app->post('/api/sale_orders/:id/approve',$checkToken(),$checkScopes(array('sales.view')),function($id) use ($app){

        $app = \Slim\Slim::getInstance();
        $request = $app->request();
        $body = $request->getBody();

        $SaleOrderReceived = json_decode($body);
        $SaleOrderReceived->SaleOrderId = $id;
        //validaciones antes de actualizar
    
        $SaleOrderModel = new SaleOrderModel();
        $SaleOrderReceived = $SaleOrderModel->approveSaleOrder($SaleOrderReceived);

        echo json_encode($SaleOrderReceived,JSON_NUMERIC_CHECK);

    });


 //Aprobar una orden de venta
    $app->post('/api/sale_orders/:id/shipping',$checkToken(),$checkScopes(array('sales.view')),function($id) use ($app){

        $app = \Slim\Slim::getInstance();
        $request = $app->request();
        $body = $request->getBody();

        $SaleOrderReceived = json_decode($body);
        $SaleOrderReceived->SaleOrderId = $id;
        //validaciones antes de actualizar
    
        $SaleOrderModel = new SaleOrderModel();
        $SaleOrderReceived = $SaleOrderModel->shippingSaleOrder($SaleOrderReceived);

        echo json_encode($SaleOrderReceived,JSON_NUMERIC_CHECK);

    });

    //Cancelar una orden de venta
    $app->post('/api/sale_orders/:id/cancel',$checkToken(),$checkScopes(array('sales.view')),function($id) use ($app){

        $app = \Slim\Slim::getInstance();
        $request = $app->request();
        $body = $request->getBody();

        $SaleOrderReceived = json_decode($body);
        $SaleOrderReceived->SaleOrderId = $id;
        //validaciones antes de actualizar
        
        
        //Necesitamos validar que la orden de venta no este facturada
    
        $SaleOrderModel = new SaleOrderModel();
        $SaleOrderReceived=$SaleOrderModel->cancelSaleOrder($SaleOrderReceived);

        echo json_encode($SaleOrderReceived,JSON_NUMERIC_CHECK);
    });

    //API de pagos
     $app->get('/api/sale_orders/:id/payments',$checkToken(),$checkScopes(array('collect.view')), function ($id) {
       
       $SaleOrderModel = new SaleOrderModel();

       $SaleOrderPaymentsJson = $SaleOrderModel->getSaleOrderPayments($id);

        echo json_encode($SaleOrderPaymentsJson,JSON_NUMERIC_CHECK);
    });

    $app->get('/api/sale_orders/:id/payments/:payment_id', function ($id,$payment_id) {
       
       $SaleOrderModel = new SaleOrderModel();

       $SaleOrderPaymentsJson = $SaleOrderModel->getSaleOrderPayment($id,$payment_id);

        echo json_encode($SaleOrderPaymentsJson,JSON_NUMERIC_CHECK);
    });

    $app->get('/api/sale_orders/:id/payments/:payment_id/report', function ($id,$payment_id) {
       
       $SaleOrderModel = new SaleOrderModel();

       $SaleOrderJson =  $SaleOrderModel->getSaleOrder($id);
       $SaleOrderPaymentsJson = $SaleOrderModel->getSaleOrderPayment($id,$payment_id);

        $ReceiptReport = new ReceiptReport();
      //var_dump($SaleOrderJson);
       $ReceiptReport->printReport($SaleOrderPaymentsJson,$SaleOrderJson);

        //echo json_encode($SaleOrderPaymentsJson,JSON_NUMERIC_CHECK);
    });


    //Crear pago
    $app->post('/api/sale_orders/:id/payments',$checkToken(),$checkScopes(array('collect.edit')),function($id) use ($app){
         
        $app = \Slim\Slim::getInstance();
        $request = $app->request();
        $body = $request->getBody();
        $SaleOrderPaymentReceived = json_decode($body);
        $SaleOrderPaymentReceived->SaleOrderId = $id;

        $SaleOrderModel = new SaleOrderModel();
        $SaleOrderPaymentJson =  $SaleOrderModel->createOrUpdateSaleOrderPayment($SaleOrderPaymentReceived);

        echo json_encode($SaleOrderPaymentJson,JSON_NUMERIC_CHECK);
    });

    //Actualizar pago
    $app->post('/api/sale_orders/:id/payments/:payment_id',$checkToken(),$checkScopes(array('collect.edit')),function($id,$payment_id) use ($app){
         
        $app = \Slim\Slim::getInstance();
        $request = $app->request();
        $body = $request->getBody();

        $SaleOrderPaymentReceived = json_decode($body);
        $SaleOrderPaymentReceived->SaleOrderId = $id;
        $SaleOrderPaymentReceived->SaleOrderPaymentId = $payment_id;

        $SaleOrderModel = new SaleOrderModel();
        $SaleOrderPaymentJson =  $SaleOrderModel->createOrUpdateSaleOrderPayment($SaleOrderPaymentReceived);

        echo json_encode($SaleOrderPaymentJson,JSON_NUMERIC_CHECK);
    });

     $app->delete('/api/sale_orders/:id/payments/:payment_id',$checkToken(),$checkScopes(array('collect.edit')),function($id,$payment_id) use ($app){
         
       
        
        $SaleOrderPaymentReceived = new stdClass();
        $SaleOrderPaymentReceived->SaleOrderId = $id;
        $SaleOrderPaymentReceived->SaleOrderPaymentId = $payment_id;

        $SaleOrderModel = new SaleOrderModel();
        $SaleOrderModel->deleteSaleOrderPayment($SaleOrderPaymentReceived);

 
    });

     //Ordenes de venta pendientes de pago
     $app->get('/api/receivable/sale_orders/',$checkToken(),$checkScopes(array('collect.view')), function () {

        $SaleOrderModel = new SaleOrderModel();

        $data = [];
        $data = $SaleOrderModel->getAllReceivableSalerOrders();
    
        echo json_encode($data,JSON_NUMERIC_CHECK);
    });


     //Ordenes de venta pagadas completamente
     $app->get('/api/receivable/sale_orders/completed/',$checkToken(),$checkScopes(array('collect.view')), function () {

        $SaleOrderModel = new SaleOrderModel();

        $data = [];
        $data = $SaleOrderModel->getAllSalerOrdersPayed();
    
        echo json_encode($data,JSON_NUMERIC_CHECK);
    });

     //Crear orden de trabajo
     $app->post('/api/sale_orders/:id/create_work_order/',$checkToken(),$checkScopes(array('collect.edit')),function($id) use ($app){
         
        $app = \Slim\Slim::getInstance();
        $request = $app->request();
        $body = $request->getBody();

        $SaleOrderReceived = json_decode($body);
        $SaleOrderReceived->SaleOrderId = $id;

        $SaleOrderModel = new SaleOrderModel();
        $SaleOrderJson =  $SaleOrderModel->createWorkOrder($id);

        echo json_encode($SaleOrderJson,JSON_NUMERIC_CHECK);
    });


    //Contrato de Venta
     $app->get('/api/sale_orders/report/:id', function ($id) {
       
       $SaleOrderModel = new SaleOrderModel();

       $SaleOrderJson = $SaleOrderModel->getSaleOrder($id,true);

       $SaleOrderReport = new SaleOrderReport();
    
       $SaleOrderReport->printReport($SaleOrderJson);

        //echo json_encode($QuoteJson,JSON_NUMERIC_CHECK);
    });

    //Envio de contrato de venta
    $app->post('/api/sale_orders/email/:id',$checkToken(),$checkScopes(array('sales.view')), function ($id) use ($app){
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $res = $app->response();


       try{

           $SaleOrderModel = new SaleOrderModel();

           $SaleOrderJson = $SaleOrderModel->getSaleOrder($id,true);

           $SaleOrderReport = new SaleOrderReport();
         
           $SaleOrderReport->sendReport($SaleOrderJson);



       }catch(Exception $ex){

             $json = $ex->getMessage();//json_encode(array('error' => $ex->getMessage()));

            $res['Content-Type'] = 'application/json';
            $res->status(503);
            //$res->body($json);
       }

    });
?>