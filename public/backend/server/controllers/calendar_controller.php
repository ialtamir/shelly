<?php
	require 'server/models/calendar_model.php';


	$app->get('/api/calendar/', function () use ($app){

    	$app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();



		$CalendarModel = new CalendarModel();

		$data = [];
		//$data['visits'] = array();
		$data = $CalendarModel->getSaleOrdersDeliveryDates();
    	
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


	});


	//Obtener Events
	$app->get('/api/events/', function () use ($app){

    	$app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();


		$CalendarModel = new CalendarModel();

		$data = [];
		$data = $CalendarModel->getEvents();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


	 //Obtener un solo cliente
	$app->get('/api/events/:id', function ($id) use ($app){

    	$app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

       
       $CalendarModel = new CalendarModel();

       $data = $CalendarModel->getEvent($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});



    //Crear cliente
	$app->post('/api/events',function() use ($app){
		 
    	$app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();


    	$body = $request->getBody();
    	$EventReceived = json_decode($body);

    	$CalendarModel = new CalendarModel();
    	$data =  $CalendarModel->createEvent($EventReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar un cliente
	$app->post('/api/events/:id',function($id) use ($app){

    	$app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();


    	$body = $request->getBody();

    	$EventReceived = json_decode($body);
    	$EventReceived->EventId = $id;
    	//validaciones antes de actualizar
    
    	$CalendarModel = new CalendarModel();
    	$CalendarModel->updateEvent($EventReceived);

	});

	//Eliminar un cliente
	$app->delete('/api/events/:id',function($id) use ($app){
		$CalendarModel = new CalendarModel();
    	$CalendarModel->deleteEvent($id);
	});

?>