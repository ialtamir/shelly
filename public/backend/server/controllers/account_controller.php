<?php

    require 'server/models/account_model.php';
    require 'server/models/account_configuration_model.php';
    
    
     $app->get('/api/account/configurations/', function () use ($app){

    	$app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

        $host_name = $_SERVER["HTTP_HOST"];
        $AccountModel = new AccountModel(); 
        $account_customization = $AccountModel->getAccountCustomization();
        
        $data = $AccountModel->getAccountConfigurationsByHostName($host_name);
        $data->account_customization = $account_customization ;
        
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


    });
    
    $app->get('/api/account/:id', function ($id) use ($app){

    	$app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

        $host_name = $_SERVER["HTTP_HOST"];
        $AccountModel = new AccountModel(); 
        $data = $AccountModel->getAccount();


        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


    });
    
    $app->post('/api/account/:id', function ($id) use ($app){
        
        
        
    	$app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();
    	
        $body = $request->getBody();
        $account = json_decode($body);
        
         try {
            
            $AccountModel = new AccountModel(); 
            $account = $AccountModel->updateAccount($account);
    
    
            $json = json_encode($account,JSON_NUMERIC_CHECK);
    
            $response['Content-Type'] = 'application/json';
            $response->status(200);
            $response->body($json);
        } 
        catch(Exception $ex) {
            
            $json = json_encode(array('error' => $ex->getMessage()));

            $response['Content-Type'] = 'application/json';
            $response->status(500);
            $response->body($json);
            
        }

    });
    
    $app->post('/api/account/:id/update_pkey_password/', function ($id) use ($app){
        
        
        
    	$app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();
    	
        $body = $request->getBody();
        $account = json_decode($body);
        
         try {
            
            $AccountModel = new AccountModel(); 
            $account = $AccountModel->updateInvoiceCertPassword($account);
            $account->cert_password = "";
            $account->confirm_cert_password= "";
            $account->cert_password_is_set = true;
    
            $json = json_encode($account,JSON_NUMERIC_CHECK);
    
            $response['Content-Type'] = 'application/json';
            $response->status(200);
            $response->body($json);
            
        } 
        catch(Exception $ex) {
            
            $json = json_encode(array('error' => $ex->getMessage()));

            $response['Content-Type'] = 'application/json';
            $response->status(500);
            $response->body($json);
            
        }

    });
    
    
    
    $app->post('/api/account/upload/',function() use ($app) { 
        
        $app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();
    	
        
		if(isset($_FILES["file"])){
                 
		        
		    $document = $app->request()->post('Document');
		    
		    $AccountModel = new AccountModel(); 
		    
		    $account = $AccountModel->getAccount();
		    $account_configuration = AccountModel::getAccountConfiguration();
		    
		    
		     
		    switch ($document) {
		        
                case "cert_file":
                    
                    $filename = $account->rfc.".cer";
                    $upload_path = '../accounts_data/facturas/cerKey/';  
                    
                    break;
                case "cert_key":
                    
                    $filename = $account->rfc.".key";
                    $upload_path = '../accounts_data/facturas/cerKey/';  
                    
                    break;
                    
                case "logo_file":
                    
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
				    $filename = "logo.".$ext;
                    $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path.CONST_ACCOUNT_PUBLIC_FOLDER;
                    $AccountModel->updateAccountLogo($filename);
                    
                    break;
              
            }
            
		    
                  

        	if(move_uploaded_file($_FILES["file"]["tmp_name"],$upload_path.$filename)){

                //update the account 
                
  				$uploaded_file = $upload_path.$filename;
  				    
  				// Actualizar la informacion del certificado  
  				
  				$json = json_encode(array('url' => $filename));

                $response['Content-Type'] = 'application/json';
                $response->status(200);
                $response->body($json);

        	}

        }
	});



   

    $app->get('/api/account/configuration/', function () use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();



      

        $http_host = $_SERVER["HTTP_HOST"];
    
        //$AccountModel = new AccountModel();
        $data = [] ;//AccountModel::getAccountConfiguration();
                


        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


    });

?>