<?php

require 'server/models/contract_template_model.php';

	//Obtener contract_templates
	$app->get('/api/contract_templates/', function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$ContractTemplateModel = new ContractTemplateModel();

		$data = [];
		$data = $ContractTemplateModel->getContractTemplates();
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});



	 //Obtener una sola plantilla
	$app->get('/api/contract_templates/:id', function ($id)  use ($app) {
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       $ContractTemplateModel = new ContractTemplateModel();

       $data = $ContractTemplateModel->getContractTemplate($id);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});



    //Crear plantilla
	$app->post('/api/contract_templates',function() use ($app){
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$ContractTemplateReceived = json_decode($body);

    	$ContractTemplateModel = new ContractTemplateModel();
    	$data =  $ContractTemplateModel->creatContractTemplate($ContractTemplateReceived);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar una plantilla
	$app->post('/api/contract_templates/:id',function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();

    	$ContractTemplateReceived = json_decode($body);
    	$ContractTemplateReceived->ContractTemplateId = $id;
    	//validaciones antes de actualizar
    
    	$ContractTemplateModel = new ContractTemplateModel();
    	$ContractTemplateModel->updateContractTemplate($ContractTemplateReceived);

    	
    	$json = json_encode($ContractTemplateReceived,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


	});

	//Eliminar una plantilla
	$app->delete('/api/contract_templates/:id',function($id) use ($app){
		$ContractTemplateModel = new ContractTemplateModel();
    	$ContractTemplateModel->deleteContractTemplate($id);
	});

?>