<?php 

	require 'server/models/work_order_model.php';


	$app->get('/api/work_orders/',$checkToken(), function ()  use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();


		$WorkOrderModel = new WorkOrderModel();

		$data = [];
		$data = $WorkOrderModel->getWorkOrders();
    
    	echo json_encode($data,JSON_NUMERIC_CHECK);
	});

	$app->get('/api/work_orders/by_status/:Status',$checkToken(), function ($Status)  use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();


		$WorkOrderModel = new WorkOrderModel();

		$data = [];
		if($Status == 'Cerrada'){
		
			$data = $WorkOrderModel->getWorkOrdersByStatus($Status);
		}
		else{
			$data = $WorkOrderModel->getOpenedWorkOrders();
		}
    
    	echo json_encode($data,JSON_NUMERIC_CHECK);
	});

	$app->get('/api/work_orders/:id',$checkToken(), function ($id)  use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

		$WorkOrderModel = new WorkOrderModel();

		$data = [];
		$data = $WorkOrderModel->getWorkOrder($id);
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	$app->get('/api/work_orders/search',$checkToken(), function ()  use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

		$WorkOrderModel = new WorkOrderModel();

		$data = [];
		$data = $WorkOrderModel->getWorkOrders();
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	$app->post('/api/work_orders/',$checkToken(),function()  use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

    	$body = $request->getBody();
    	$WorkOrderReceived = json_decode($body);
    	

    	$WorkOrderModel = new WorkOrderModel();
    	$data =  $WorkOrderModel->createWorkOrder($WorkOrderReceived);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	$app->post('/api/work_orders/:id',$checkToken(),function($id) use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

    	$body = $request->getBody();
    	$WorkOrderReceived = json_decode($body);
        $WorkOrderReceived->WorkOrderId = $id;

    	$WorkOrderModel = new WorkOrderModel();
    	$data =  $WorkOrderModel->updateWorkOrder($WorkOrderReceived);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	$app->delete('/api/work_orders/:id',$checkToken(),function($id) use ($app){
        $WorkOrderModel = new WorkOrderModel();
        $WorkOrderModel->deleteWorkOrder($id);
    });



	$app->post('/api/work_orders/open/:id',$checkToken(),function($id) use ($app){

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

    	$body = $request->getBody();
    	$WorkOrderReceived = json_decode($body);
        $WorkOrderReceived->WorkOrderId = $id;
        

        $WorkOrderStatus = "Abierta";
    	$WorkOrderModel = new WorkOrderModel();
    	$data =  $WorkOrderModel->setWorkOrderStatus($WorkOrderReceived,$WorkOrderStatus);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


	$app->post('/api/work_orders/close/:id',$checkToken(),function($id) use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

    	$body = $request->getBody();
    	$WorkOrderReceived = json_decode($body);
        $WorkOrderReceived->WorkOrderId = $id;

        $WorkOrderStatus = "Cerrada";
    	$WorkOrderModel = new WorkOrderModel();
    	$data =  $WorkOrderModel->setWorkOrderStatus($WorkOrderReceived,$WorkOrderStatus);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


	$app->post('/api/work_orders/from_quote/:QuoteId',$checkToken(),function($QuoteId) use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

    	$body = $request->getBody();


        try{


	    	$WorkOrderModel = new WorkOrderModel();
	    	$data =  $WorkOrderModel->createWorkOrderFromQuote($QuoteId);

	    	$json = json_encode($data,JSON_NUMERIC_CHECK);

	        $response['Content-Type'] = 'application/json';
	        $response->status(200);
	        $response->body($json);

	    }catch(Exception $e){
	    	//ar_dump($e);
	    	$json = json_encode(array('message' => $e->getMessage()));

	        $response['Content-Type'] = 'application/json';
	        $response->status(500);
	        $response->body($json);
	    }
	});



	



	
?>