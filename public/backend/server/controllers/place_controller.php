<?php

	require 'server/models/place_model.php';

	//Obtener Places
	$app->get('/api/places/', function () use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
		$PlaceModel = new PlaceModel();

		$data = [];
		$data = $PlaceModel->getPlaces();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	 //Obtener una sola sucursal
	$app->get('/api/places/:id', function ($id) use ($app) {
       
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
       $PlaceModel = new PlaceModel();

       $data = $PlaceModel->getPlace($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

    //Crear Sucursal
	$app->post('/api/places',function() use ($app){
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
    	$body = $request->getBody();
    	$PlaceReceived = json_decode($body);

    	$PlaceModel = new PlaceModel();
    	$data =  $PlaceModel->createPlace($PlaceReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar sucursal
	$app->post('/api/places/:id',function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
    	$body = $request->getBody();

    	$PlaceReceived = json_decode($body);
    	$PlaceReceived->PlaceId = $id;
    	//validaciones antes de actualizar
    
    	$PlaceModel = new PlaceModel();
    	$PlaceModel->updatePlace($PlaceReceived);

    	$json= json_encode($PlaceReceived,JSON_NUMERIC_CHECK);
        

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);    	

	});

	//Eliminar Sucursal
	$app->delete('/api/places/:id',function($id) use ($app) {
		$PlaceModel = new PlaceModel();
    	$PlaceModel->deletePlace($id);
	});


?>