<?php

	require 'server/models/material_model.php';

	//Obtener Materials
	$app->get('/api/materials/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$MaterialModel = new MaterialModel();

		$data = [];
		$data = $MaterialModel->getMaterials();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

		//Obtener Materials
	$app->get('/api/materials/search/:query', function ($query) use ($app) {
        

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
		
		$MaterialModel = new MaterialModel();
        
		$data = [];
		$data = $MaterialModel->searchMaterials($query);
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

    	//echo '[{ "MaterialId" : 1 , "MaterialName" : "Material Ejemplo Uno" , "MaterialUM" : "PZ" , "MaterialStock" : 99 },{ "MaterialId" : 2,"MaterialName" : "Material Ejemplo 2" , "MaterialUM" : "PZ" , "MaterialStock" : 99 },{ "MaterialId" : 3 , "MaterialName" : "Material Ejemplo Tres" , "MaterialUM" : "PZ" , "MaterialStock" : 99 }]';
	});

	 //Obtener un solo Material
	$app->get('/api/materials/:id', function ($id) use ($app) {
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
		

       $MaterialModel = new MaterialModel();

       $data = $MaterialModel->getMaterial($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});



    //Crear Material
	$app->post('/api/materials',function() use ($app) {
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
		

    	$body = $request->getBody();
    	$MaterialReceived = json_decode($body);

    	$MaterialModel = new MaterialModel();
    	$data =  $MaterialModel->createMaterial($MaterialReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar un Material
	$app->post('/api/materials/:id',function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
		
		
    	$body = $request->getBody();

    	$MaterialReceived = json_decode($body);
    	$MaterialReceived->MaterialId = $id;
    	//validaciones antes de actualizar
    
    	$MaterialModel = new MaterialModel();
    	$MaterialModel->updateMaterial($MaterialReceived);

        $json = json_encode($MaterialReceived,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	//Eliminar un Material
	$app->delete('/api/materials/:id',function($id) use ($app) {
		$MaterialModel = new MaterialModel();
    	$MaterialModel->deleteMaterial($id);
	});


?>