<?php


	require 'server/models/product_model.php';
	require 'server/models/variants_model.php';

	$app->get('/api/products/by_category/:id', function ($id)  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

        
        $product_search = $request->get("product_search");
        $page = $request->get("page");
        
        
       $ProductModel = new ProductModel();

       $data = [];
       $data = $ProductModel->getProductsByCategory($id,$product_search,$page);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        
    });
    
    $app->get('/api/products/by_category/', function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

       $category_id = $request->get("category_id");
       $product_search = $request->get("product_search");
       $page = $request->get("page");
       $user_id = $request->get("user_id");
       
       $ProductModel = new ProductModel();
       
       if(!isset($user_id) || $user_id == 0){
           
           $ProductModel->hide_prices = true;
       }

       $data = [];
       $data = $ProductModel->getProductsByCategoryPaging($category_id,$product_search,$page);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });
    
    
    $app->get('/api/products/by_category/paging/', function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

       $category_id = $request->get("category_id");
       $product_search = $request->get("product_search");
       $page = $request->get("page");
       
       $ProductModel = new ProductModel();

       $data = [];
       $data = $ProductModel->getProductsByCategoryPaging($category_id,$product_search,$page);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });



		//Obtener Products
	$app->get('/api/products/',$checkToken(), function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

		$ProductModel = new ProductModel();

		$data = [];
		$data = $ProductModel->getAllProducts();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	$app->get('/api/products/import',function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

		$ProductModel = new ProductModel();

		$data = [];
		$data = $ProductModel->importProducts('');
    
    	//echo json_encode($data,JSON_NUMERIC_CHECK);
	});

	//Obtener Products
	$app->get('/api/products/variants/',$checkToken(), function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

		$ProductModel = new ProductModel();

		$data = [];
		$data = $ProductModel->getVariants();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


		//Obtener Productos
	$app->get('/api/products/search/:query',$checkToken(), function ($query)  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
		$ProductModel = new ProductModel();
        
		$data = [];
		$data = $ProductModel->searchProducts($query);
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	$app->get('/api/products/all/', function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        
        
        $request = $app->request();
        $response = $app->response();
        
        $include_only_saleables = $request->get("include_only_saleables");
        

      		$ProductModel = new ProductModel();
              
      		$data = [];
      		$data = $ProductModel->getProducts($include_only_saleables);
          
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


   $app->get('/api/products/ecommerce/', function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        
        
        $request = $app->request();
        $response = $app->response();
        
        $include_only_saleables = $request->get("include_only_saleables");
        

      		$ProductModel = new ProductModel();
              
      		$data = [];
      		$data = $ProductModel->getEcommerceProducts($include_only_saleables);
          
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


     $app->get('/api/products/featured/', function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        
        
        $request = $app->request();
        $response = $app->response();
        
        $category_id = $request->get("category_id");
        $user_id = $request->get("user_id");
        

      		$ProductModel = new ProductModel();
            
            if(!isset($user_id) ||  $user_id == 0){
      		    
      		    $ProductModel->hide_prices = true;
      		    
      		}
      		
      		
      		$data = [];
      		$data = $ProductModel->getFeaturedProducts($category_id);
          
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


 $app->get('/api/products/recommended/', function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        
        
        $request = $app->request();
        $response = $app->response();
        
        $category_id = $request->get("category_id");
        $user_id = $request->get("user_id");
        

      		$ProductModel = new ProductModel();
      		
      		if(!isset($user_id) || $user_id == 0){
      		    
      		    $ProductModel->hide_prices = true;
      		    
      		}
              
      		$data = [];
      		$data = $ProductModel->getRecommendedProducts($category_id);
          
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});




	 //Obtener un solo producto
	$app->get('/api/products/:id',$checkToken(), function ($id)  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

       $ProductModel = new ProductModel();

       $data = $ProductModel->getProduct($id,true);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	
	$app->get('/api/products/ecommerce/:id', function ($id)  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

       $ProductModel = new ProductModel();

       $data = $ProductModel->getProduct($id,true);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	$app->post('/api/products/ecommerce/:id/review', $checkToken(),function ($id)  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        
        $body = $request->getBody();
    	$review = json_decode($body);
    	
    	
        $review->product_id = $id;

        $ProductModel = new ProductModel();

        $data = $ProductModel->createProductReview($review);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


	//Crear un producto
	$app->post('/api/products',$checkToken(),function() use ($app) {
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
    	$body = $request->getBody();
    	$ProductReceived = json_decode($body);
    	//ar_dump($ProductReceived);
    	$ProductModel = new ProductModel();
    	$data =  $ProductModel->createProduct($ProductReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	
	//Crear un producto
	$app->post('/api/products/sync',function() use ($app) {
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
    	$body = $request->getBody();
    	
    	$ProductReceived = json_decode($body);
    	
    	//var_dump($ProductReceived);
    	$ProductModel = new ProductModel();
    	$data =  $ProductModel->syncProducts($ProductReceived);

        $json = json_encode($data);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar un cliente
	$app->post('/api/products/:id',$checkToken(),function($id) use ($app) {
	        
	        try {
                
                        $app = Slim\Slim::getInstance();
                        $request = $app->request();
                        $response = $app->response();
                        
                    	$body = $request->getBody();
                
                    	$ProductReceived = json_decode($body);
                    	$ProductReceived->ProductId = $id;
                    	//validaciones antes de actualizar
                    
                    	$ProductModel = new ProductModel();
                    	$data= $ProductModel->updateProduct($ProductReceived);
                
                        $json = json_encode($data,JSON_NUMERIC_CHECK);
                
                        $response['Content-Type'] = 'application/json';
                        $response->status(200);
                        $response->body($json);
	        }
	        catch(Exception $ex){
	                $response['Content-Type'] = 'application/json';
                        $response->status(200);
                        $response->body('{ error: '.$ex->getMessage().'}');  
	        }
	});

	$app->delete('/api/products/:id',$checkToken(),function($id) use ($app) {


    	$ProductModel = new ProductModel();
    	$ProductModel->deleteProduct($id);

	});

	$app->post('/api/products/upload/',$checkToken(),function() use ($app) { 

		if(isset($_FILES["file"])){

			//var_dump($_FILES);
			//echo $unique_key = League\OAuth2\Server\Util\SecureKey::make();
			//
			
            $account_configuration = AccountModel::getAccountConfiguration();
                
            $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path;
                    

        	if(move_uploaded_file($_FILES["file"]["tmp_name"],$upload_path.$_FILES['file']["name"])){

                
  				$picture = $upload_path.$_FILES['file']["name"];
  				$thumb = $upload_path.'thumb_'.$_FILES['file']["name"];
  			
                $picture_url = ABSOLUTE_BASE_URL.$_FILES['file']["name"];
                $thumb_url   = ABSOLUTE_BASE_URL.'thumb_'.$_FILES['file']["name"];
                
  				$img = Intervention\Image\Image::make($picture);

				// now you are able to resize the instance
				$img->heighten(60);


				// finally we save the image as a new image
				$img->save($thumb );

    			echo '{ "url": "'.$picture_url.'" , "thumbnail_url":"'.$thumb_url.'" }';
        	}

        }
	});
	
	
	$app->post('/api/products/:id/upload/',function($id) use ($app) { 

		if(isset($_FILES["file"])){

			//var_dump($_FILES);
			//echo $unique_key = League\OAuth2\Server\Util\SecureKey::make();
			//
			
            $account_configuration = AccountModel::getAccountConfiguration();
                
            $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path;
                    

        	if(move_uploaded_file($_FILES["file"]["tmp_name"],$upload_path.$_FILES['file']["name"])){

                
  				$picture = $upload_path.$_FILES['file']["name"];
  				$thumb = $upload_path.'thumb_'.$_FILES['file']["name"];
  			
                $picture_url = ABSOLUTE_BASE_URL.$_FILES['file']["name"];
                $thumb_url   = ABSOLUTE_BASE_URL.'thumb_'.$_FILES['file']["name"];
                
  				$img = Intervention\Image\Image::make($picture);

				// now you are able to resize the instance
				$img->heighten(60);


				// finally we save the image as a new image
				$img->save($thumb );
				
				
				$picture = new stdClass();
				//$picture->ProductPictureId 
				$picture->ProductPictureUrl = $picture_url;
				$picture->ProductPictureThumbnail = $thumb_url;
				$picture->ProductPictureDefault = 1;



				
			    $ProductModel = new ProductModel();
			    
			    $product = $ProductModel->getProductByCode($id);
			    
    	        $ProductModel->createPicture($product["ProductId"],$picture);
    	
    	
			
			
				
    			echo '{ "url": "'.$picture_url.'" , "thumbnail_url":"'.$thumb_url.'" }';
        	}

        }
	});

	//Product Attributes
	//Product Variants
	//Product Images
	$app->get('/api/products/:id/pictures/',$checkToken(),function($id) use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

		$ProductModel = new ProductModel();

       	$data = $ProductModel->getProductPictures($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	$app->get('/api/products/:id/pictures/:ProductPictureId',$checkToken(),function($id,$ProductPictureId) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

		$ProductModel = new ProductModel();

       	$data = $ProductModel->getProductPicture($id,$ProductPictureId);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

$app->get('backend/server/uploads/images/:image_name',$checkToken(),function ($image_name) use ($app){
	
       
       	try{
               
               $account_configuration = AccountModel::getAccountConfiguration();                
               $image_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path.'uploads/'.$image_name;
       
               $image = file_get_contents( $image_path );
               $finfo = new finfo(FILEINFO_MIME_TYPE);        
               $app->response()->header('Content-Type', 'content-type: ' . $finfo->buffer($image));
               $app->response()->body($image);
               
       	}
       	catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e)
       	{
       		//$app->render('../client/login.html');
       	}
       	



});

	
?>