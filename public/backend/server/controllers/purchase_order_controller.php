<?php

	require 'server/models/purchase_order_model.php';
   
  

		//Obtener Entradas
	$app->get('/api/purchase_orders/',$checkToken(),$checkScopes(array('sales.view','sales.edit')), function () use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$data = [];

        $PurchaseOrderModel = new PurchaseOrderModel();
        $data = $PurchaseOrderModel->getPendingPurchaseOrders();
        $json = json_encode($data,JSON_NUMERIC_CHECK);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
       
    
	});

    ///Ordenes de compra procesadas

    $app->get('/api/purchase_orders/processed/',$checkToken(),$checkScopes(array('sales.view','sales.edit')),  function () use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $PurchaseOrderModel = new PurchaseOrderModel();
        $data = $PurchaseOrderModel->getProcessedPurchaseOrders();

        $json = json_encode($data,JSON_NUMERIC_CHECK);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
       
    });



	 //Obtener una sola entrada
	$app->get('/api/purchase_orders/:id',$checkToken(),$checkScopes(array('sales.view','sales.edit')), function ($id) {
       
       $PurchaseOrderModel = new PurchaseOrderModel();

       $data = $PurchaseOrderModel->getPurchaseOrder($id,true);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


	//Crear una orden de entrada
	$app->post('/api/purchase_orders',$checkToken(),$checkScopes(array('sales.edit')),function() use ($app){
		 
        $app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$body = $request->getBody();

    	$PurchaseOrderReceived = json_decode($body);
    	
        $PurchaseOrderModel = new PurchaseOrderModel();
    	$data =  $PurchaseOrderModel->createPurchaseOrder($PurchaseOrderReceived);

         $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	//Actualizar una orden de entrada
	$app->post('/api/purchase_orders/:id',$checkToken(),$checkScopes(array('sales.edit')),function($id) use ($app){

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$body = $request->getBody();

    	$PurchaseOrderReceived = json_decode($body);
    	$PurchaseOrderReceived->PurchaseOrderId = $id;
    	//validaciones antes de actualizar
    
    	$PurchaseOrderModel = new PurchaseOrderModel();
    	$data = $PurchaseOrderModel->updatePurchaseOrder($PurchaseOrderReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


    //Aprobar una orden de compra
    $app->post('/api/purchase_orders/:id/approve',$checkToken(),$checkScopes(array()),function($id) use ($app){

        $app = \Slim\Slim::getInstance();
        $request = $app->request();
        $body = $request->getBody();

        $PurchaseOrderReceived = json_decode($body);
        $PurchaseOrderReceived->PurchaseOrderId = $id;
        //validaciones antes de actualizar
    
        $PurchaseOrderModel = new PurchaseOrderModel();
        $data = $PurchaseOrderModel->approvePurchaseOrder($PurchaseOrderReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });


    

    //Eliminar una orden de entrada
    $app->delete('/api/purchase_orders/:id',$checkToken(),$checkScopes(array('sales.edit')),function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

        $PurchaseOrderModel = new PurchaseOrderModel();
        $PurchaseOrderModel->deletePurchaseOrder($id);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
 
    });


    //Impresion del reporte de entrada
    $app->get('/api/purchase_orders/report/:id', function ($id) {
       
       $PurchaseOrderModel = new PurchaseOrderModel();

       $PurchaseOrderJson = $PurchaseOrderModel->getPurchaseOrder($id,true);

       $PurchaseOrderReport = new PurchaseOrderReport();
     
       $PurchaseOrderReport->printReport($PurchaseOrderJson);

       
    });


     $app->get('/api/purchase_orders/report/square/:id', function ($id) {
       

       $PurchaseOrderModel = new PurchaseOrderModel();

       $PurchaseOrderJson = $PurchaseOrderModel->getPurchaseOrder($id,true);

 
       $SquaresReport = new SquaresReport();
     
       $SquaresReport->printReport($PurchaseOrderJson);

       
    });


?>