<?php

	require 'server/models/customer_model.php';

	//Obtener Customers
	$app->get('/api/customers/', function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$CustomerModel = new CustomerModel();

		$data = [];
		$data = $CustomerModel->getCustomers();
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

		//Obtener Customers
	$app->get('/api/customers/search/:query', function ($query)  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$CustomerModel = new CustomerModel();
        
		$data = [];
		$data = $CustomerModel->searchCustomers($query);
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	 //Obtener un solo cliente
	$app->get('/api/customers/:id', function ($id)  use ($app) {
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       $CustomerModel = new CustomerModel();

       $data = $CustomerModel->getCustomer($id);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);
    	//$json = json_encode($data);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

    $app->get('/api/customers/by_code/:code', function ($code)  use ($app) {
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       $CustomerModel = new CustomerModel();

       $data = $CustomerModel->getCustomerByCode($code);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


    //Crear cliente
	$app->post('/api/customers',function() use ($app){
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$CustomerReceived = json_decode($body);

    	$CustomerModel = new CustomerModel();
    	$data =  $CustomerModel->createCustomer($CustomerReceived);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar un cliente
	$app->post('/api/customers/:id',function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();

    	$CustomerReceived = json_decode($body);
    	$CustomerReceived->CustomerId = $id;
    	//validaciones antes de actualizar
    
    	$CustomerModel = new CustomerModel();
    	$CustomerModel->updateCustomer($CustomerReceived);

    	
    	$json = json_encode($CustomerReceived,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


	});
	
	//Actualizar un cliente
	$app->post('/api/customers/:id/saveAddress',function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();

    	$CustomerReceived = json_decode($body);
    	$CustomerReceived->CustomerId = $id;
    	
    	
    	//validaciones antes de actualizar
    
    	$CustomerModel = new CustomerModel();
    	$CustomerModel->updateCustomerAddress($CustomerReceived);

    	
    	$json = json_encode($CustomerReceived,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


	});

	//Eliminar un cliente
	$app->delete('/api/customers/:id',function($id) use ($app){
		$CustomerModel = new CustomerModel();
    	$CustomerModel->deleteCustomer($id);
	});

    //Get Customer Contacts
	$app->get('/api/customers/:id/contacts/', function ($id) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
		
		$CustomerModel = new CustomerModel();

		$data = [];
		$data = $CustomerModel->getCustomerContacts($id);
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	

	 //Get Customer Contact
	$app->get('/api/customers/:id/contacts/:contact_id', function ($id,$contact_id)  use ($app) {
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
       
       $CustomerModel = new CustomerModel();

       $data = $CustomerModel->getCustomerContact($id,$contact_id);
       $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});



    //Crear contacto cliente
	$app->post('/api/customers/:id/contacts/',function($id) use ($app){
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$CustomerContactReceived = json_decode($body);
    	$CustomerContactReceived->CustomerContactId = NULL;
        $CustomerContactReceived->CustomerId = $id;


    	$CustomerModel = new CustomerModel();
    	$data =  $CustomerModel->createCustomerContact($CustomerContactReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar un cliente
	$app->post('/api/customers/:id/contacts/:contact_id',function($id,$contact_id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();

    	$CustomerContactReceived = json_decode($body);
    	$CustomerContactReceived->CustomerId = $id;
    	$CustomerContactReceived->CustomerContactId = $contact_id;
    	//validaciones antes de actualizar
    
    	$CustomerModel = new CustomerModel();
    	$CustomerModel->updateCustomerContact($CustomerContactReceived);

	});

	//Eliminar un cliente
	$app->delete('/api/customers/:id/contacts/:contact_id',function($id,$contact_id) use ($app){
		$CustomerModel = new CustomerModel();
    	$CustomerModel->deleteCustomerContact($id,$contact_id);
	});
	
	$app->delete('/api/customers/:id/sales/',$checkToken(),function($id) use ($app){
	    
		$SaleOrderModel = new SaleOrderModel();
    	$sales = $SaleOrderModel->getSaleOrdersByCustomer($id);
    	
    	$json = json_encode($sales,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    	
    	
	});
	
	$app->post('/api/customers/:id/upload/',$checkToken(),function ($id) use ($app) {
        

 

        //var_dump($_FILES);


		 if(isset($_FILES["myFile"])){

            $account_configuration = AccountModel::getAccountConfiguration();                
            $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path;
             
             
        	if(move_uploaded_file($_FILES["myFile"]["tmp_name"],$upload_path.$id.$_FILES['myFile']["name"])){

  				$Picture = $upload_path.$id.$_FILES['myFile']["name"];
        	
    			
                $Picture_URL = ABSOLUTE_BASE_URL.$id.$_FILES['myFile']["name"];
                
                if($id > 0){
                $CustomerModel = new CustomerModel();
    			$CustomerModel->updatePicture($id,$Picture_URL);
                }
    			
    			echo '{ "url": "'.$Picture_URL.'" }';
        	}

        }
        
        
        
        


	});





?>