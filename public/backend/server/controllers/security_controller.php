<?php

//development 


require '../../libraries/League/OAuth2/Server/Exception/OAuth2Exception.php';
require '../../libraries/League/OAuth2/Server/Exception/ClientException.php';
require '../../libraries/League/OAuth2/Server/Exception/InvalidAccessTokenException.php';

require '../../libraries/League/OAuth2/Server/Storage/ScopeInterface.php';
require '../../libraries/League/OAuth2/Server/Storage/SessionInterface.php';
require '../../libraries/League/OAuth2/Server/Storage/ClientInterface.php';
require '../../libraries/League/OAuth2/Server/Util/RequestInterface.php';

require '../../libraries/League/OAuth2/Server/Grant/GrantTypeInterface.php';
require '../../libraries/League/OAuth2/Server/Grant/AuthCode.php';
require '../../libraries/League/OAuth2/Server/Util/Request.php';
require '../../libraries/League/OAuth2/Server/Util/SecureKey.php';
require '../../libraries/League/OAuth2/Server/Util/RedirectUri.php';
require '../../libraries/League/OAuth2/Server/Resource.php';
require '../../libraries/League/OAuth2/Server/Authorization.php';
require '../../libraries/League/OAuth2/Server/Grant/Password.php';
require '../../libraries/League/OAuth2/Server/Grant/RefreshToken.php';

// Include the storage models
include 'server/models/client_model.php';
include 'server/models/scope_model.php';
include 'server/models/session_model.php';




//Routes
$app->post('/api/logout', function (){});

$app->post('/api/login', function () use ($app){
    
    $app = Slim\Slim::getInstance();
    $request = $app->request();
    $res = $app->response();
    $body = $request->getBody();
    $data = json_decode($body);
    
    try{


        //Obtener los parametros
        $_POST['grant_type'] = 'password';
        $_POST['client_id'] = CLIENT_ID;
        $_POST['client_secret'] = CLIENT_SECRET;
        $_POST['username'] = $data->email;
        $_POST['password'] = $data->password;
        //$_POST['scope'] = 'access'; //ir por los scopes al usuario

        $tokenInfo=PasswordGrantAuthentication($_POST);
        
      
        $sessionModel = new SessionModel();
        $sessionInfo  = $sessionModel->validateAccessToken($tokenInfo['access_token']);
        
        $userModel = new UserModel();    
        $AccountModel = new AccountModel();    

        $userInfo = $userModel->getUser($sessionInfo['owner_id']);

        $userScopes = $userModel->getUserScopes($sessionInfo['owner_id']);
        
        
        //var_dump($userInfo);
        
        $userInfo->place = new stdClass();
        
        if($userInfo->place_id){
        	
        	$PlaceModel = new PlaceModel();
        	$userInfo->place = $PlaceModel->getPlace($userInfo->place_id);
        }
        
       

        $user=array(
            'user' => array(
                'UserId' => $userInfo->user_id,
                'UserEmail' => $userInfo->user_email,
                'UserName' => $userInfo->user_name,
                'UserIsAdmin' => $userInfo->user_is_admin,
                'PlaceId' => $userInfo->place_id,
                'place' => $userInfo->place
            ),
            'token'=> array(
                'access_token'=>$tokenInfo['access_token'],
                'token_type'=> $tokenInfo['token_type'],
                'expires' => $tokenInfo['expires'],
                'expires_in' => $tokenInfo['expires_in'],
                'refresh_token'=>$tokenInfo['refresh_token'],
            )
            
        );
        
         //if(isset($userInfo->user_type)){
            
            //if($userInfo->user_type == "Customer"){
                
                $customer_model = new CustomerModel();
                $customer = $customer_model->getCustomerByUserId($userInfo->user_id);
                $user['customer'] = $customer;
            //}
            
            
        //}

        $json = json_encode($user);
        
        //var_dump($user);
echo $json;
/*
        $res['Content-Type'] = 'application/json';
        $res->status(200);
        $res->body($json);
*/
    } catch(Exception $e){
   
       $json = json_encode(array('error' => $e->getMessage()));

          $res['Content-Type'] = 'application/json';
        $res->status(401);
       $res->body($json);
    }  
});



$app->get('/api/scopes/', function () {

    });


//Implement validate refresh token
function PasswordGrantAuthentication($parameters){

    $server = new League\OAuth2\Server\Authorization(new ClientModel, new SessionModel, new ScopeModel);
				
 	$validateCredentials = function($u, $p) { 
 		
        $UserModel = new UserModel();
        
        return $UserModel->validateUser($u,$p);	 	

 	};

    $UserModel = new UserModel();        
    $UserId = $UserModel->validateUser($parameters["username"],$parameters["password"]);

    if($UserId){

        $userScopes = $UserModel->getUserScopes($UserId);
        
        foreach ($userScopes as $userScope) {
            $scopes_string .= $userScope["ScopeName"]." ";
        }

        if(trim($scopes_string) != ""){
            $parameters["scope"] = $scopes_string;
        }
    }


	$passwordGrant = new League\OAuth2\Server\Grant\Password($server);
	$passwordGrant->setVerifyCredentialsCallback($validateCredentials);
    $passwordGrant->setAccessTokenTTL(86400);

	$server->addGrantType($passwordGrant);
	//$server->addGrantType(new League\OAuth2\Server\Grant\RefreshToken($server));
    
	$request = new League\OAuth2\Server\Util\Request(array(), $parameters);
	$server->setRequest($request);
   
    $array = $server->issueAccessToken();

	return $array;

}

$resourceServer = new League\OAuth2\Server\Resource( new SessionModel, new ScopeModel);

$checkToken = function () use ($resourceServer) {

	return function() use ($resourceServer)
	{
		// Test for token existance and validity
    	try {
            
    		$resourceServer->isValid();
            
    	}
    	// The access token is missing or invalid...
    	catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e)
    	{
    		$app = Slim\Slim::getInstance();
    		$res = $app->response();
			$res['Content-Type'] = 'application/json';
			$res->status(401);

			$res->body(json_encode(array(
				'error'	=>	$e->getMessage()
			)));
            
			$app->stop();
    	}
    };

};

$checkScopes = function($userScopes) use ($resourceServer){



    return function() use ($userScopes,$resourceServer){


        try{
            
            //Obtener el usuario para saber si es admin
            $id = getOwnerId();
        
            $userModel = new UserModel();

            $user = $userModel->getUser($id);

            if( (bool) $user->user_is_admin ){
                return ;
            }
            
            //Si no es admin checar los scopes
            $found= false;

           foreach ($userScopes as $userScope) {

                if ($resourceServer->hasScope($userScope)) {
                    $found = true;
                    break;
                }
            }

            if(!$found){
                throw new Exception('No tiene privilegios para llevar a cabo esta actividad');
            }

        }
        catch(Exception $ex){
 
            $app = Slim\Slim::getInstance();
            $response = $app->response();
            $json = json_encode(array('message' => "".$ex->getMessage().""));            
            $response['Content-Type'] = 'application/json';
            $response->status(403);
            $response->body($json);

            $app->stop();

        }
    };
};

function getOwnerId()  {
		try {
            
			$resourceServer = new League\OAuth2\Server\Resource(new SessionModel, new ScopeModel);
	    	$resourceServer->isValid();
	    	return $resourceServer->getOwnerId();
            
	    }
    	// The access token is missing or invalid...
    	catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e)
    	{
            
    		$app = Slim\Slim::getInstance();
    		$res = $app->response();
			$res['Content-Type'] = 'application/json';
			$res->status(401);

			$res->body(json_encode(array(
				'error'	=>	$e->getMessage()
			)));
			$app->stop();
            
    	}
   
}
?>