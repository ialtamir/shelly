<?php
	require 'server/models/feature_model.php';

	//Obtener Features
	$app->get('/api/feature_groups/', function ()  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();		

        $FeatureModel = new FeatureModel();

		$data = [];
		$data = $FeatureModel->getFeatureGroups();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	 //Obtener una sola sucursal
	$app->get('/api/feature_groups/:id', function ($id)  use ($app) {
       

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       $FeatureModel = new FeatureModel();

       $data = $FeatureModel->getFeatureGroup($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

    //Crear Sucursal
	$app->post('/api/feature_groups',function() use ($app) {
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();
    	$FeatureReceived = json_decode($body);

    	$FeatureModel = new FeatureModel();
    	$data =  $FeatureModel->createFeatureGroup($FeatureReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar sucursal
	$app->post('/api/feature_groups/:id',function($id)   use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();

    	$FeatureReceived = json_decode($body);
    	$FeatureReceived->FeatureGroupId = $id;
    	//validaciones antes de actualizar
    
    	$FeatureModel = new FeatureModel();
    	$FeatureModel->updateFeatureGroup($FeatureReceived);

        $json = json_encode($FeatureReceived,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);        

	});

	//Eliminar Sucursal
	$app->delete('/api/feature_groups/:id',function($id)   use ($app) {
		$FeatureModel = new FeatureModel();
    	$FeatureModel->deleteFeatureGroup($id);
	});




		//Obtener Features
	$app->get('/api/feature_groups/:FeatureGroupId/features/', function ($FeatureGroupId)   use ($app) {

		
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

        $FeatureModel = new FeatureModel();

		$data = [];
		$data = $FeatureModel->getFeatures($FeatureGroupId);
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	 
	$app->get('/api/feature_groups/:FeatureGroupId/features/:id', function ($FeatureGroupId,$id)   use ($app) {
       
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


       $FeatureModel = new FeatureModel();

       $data = $FeatureModel->getFeature($FeatureGroupId,$id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

    //Crear Sucursal
	$app->post('/api/feature_groups/:FeatureGroupId/features/',function($FeatureGroupId) use ($app){
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$FeatureReceived = json_decode($body);
    	

    	$FeatureModel = new FeatureModel();
    	$data =  $FeatureModel->createFeature($FeatureGroupId,$FeatureReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar sucursal
	$app->post('/api/feature_groups/:FeatureGroupId/features/:id',function($FeatureGroupId,$id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();

    	$FeatureReceived = json_decode($body);
    	$FeatureReceived->FeatureGroupId = $FeatureGroupId;
    	$FeatureReceived->FeatureId = $id;
    	//validaciones antes de actualizar
    
    	$FeatureModel = new FeatureModel();
    	$FeatureModel->updateFeature($FeatureReceived);

	});

	//Eliminar Sucursal
	$app->delete('/api/feature_groups/:FeatureGroupId/features/:id',function($FeatureGroupId,$id) use ($app){
		$FeatureModel = new FeatureModel();
    	$FeatureModel->deleteFeatureGroup($FeatureGroupId,$id);
	});


?>