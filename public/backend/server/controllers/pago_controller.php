<?php

	require 'server/models/pago_model.php';

	 //Pago Timbres
     $app->post('/api/pago/:total/:token/',$checkToken(), function ($total,$token) use ($app) {
         
         
         
        $app = Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();
         
         try{
            
            $PagoModel = new PagoModel();
        
            $respuesta =  $PagoModel->PagoTimbrado($total,$token);
            
            $json = json_encode(array("respuesta" => $respuesta));
		    $response['Content-Type'] = 'application/json';
            $response->status(200);
            $response->body($json);
            
            
         } catch(Exception $ex){
             
            $json = json_encode(array("error" => $ex->getMessage()));
		    $response['Content-Type'] = 'application/json';
            $response->status(500);
            $response->body($json);
         }
        

    });
    
    



	


?>