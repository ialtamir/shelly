<?php

	require 'server/models/inventory_model.php';

	//Obtener Inventario
	$app->get('/api/inventories/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$InventoryModel = new InventoryModel();

		$data = [];
		$data = $InventoryModel->getInventories();

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

     $app->get('/api/inventories/:id',$checkToken(),$checkScopes(array('inventories.view')), function ($id)  use ($app) {
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       $InventoryModel = new InventoryModel();

       $data = $InventoryModel->getInventory($id);
       
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });



	//Obtener Inventarios
	$app->get('/api/inventories/search/:query', function ($query) use ($app) {

		
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $InventoryModel = new InventoryModel();
        
		$data = [];
		$data = $InventoryModel->searchInventories($query);
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});


    //Crear Captura de Inventario
	$app->post('/api/inventories',function() use ($app) {
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$InventoryReceived = json_decode($body);

    	$InventoryModel = new InventoryModel();
    	$data =  $InventoryModel->createInventory($InventoryReceived);
        
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	

	//Eliminar una captura de inventario
	$app->delete('/api/inventories/:id',function($id) use ($app) {
		$InventoryModel = new InventoryModel();
    	$InventoryModel->deleteInventory($id);
	});
	
	
	//activar folio de inventario fisico
	$app->post('/api/inventories/open/:id',function($id) use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();
    	$InventoryReceived = json_decode($body);
        $InventoryReceived->InventoryId = $id;
        

        $InventoryStatus = "Abierto";
    	$InventoryModel = new InventoryModel();
    	$data =  $InventoryModel->setInventoryStatus($InventoryReceived,$InventoryStatus);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	//cerrar folio de inventario fisico
	$app->post('/api/inventories/close/:id',function($id) use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();
    	$InventoryReceived = json_decode($body);
        $InventoryReceived->InventoryId = $id;
        

        $InventoryStatus = "Cerrado";
    	$InventoryModel = new InventoryModel();
    	$data =  $InventoryModel->setInventoryStatus($InventoryReceived,$InventoryStatus);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	
	
	//API de Inventario Fisico Detalle
     $app->get('/api/inventories/:id/inventory_details',$checkToken(),$checkScopes(array('collect.view')), function ($id) use ($app) {
       
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


       $InventoryModel = new InventoryModel();

       $data = $InventoryModel->getInventoryDetails($id);
       
       $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

    $app->get('/api/inventories/:id/inventory_details/:inventory_detail_id', function ($id,$inventory_id) use ($app) {
       
       

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


       $InventoryModel = new InventoryModel();

       $data = $InventoryModel->getInventoryDetail($id,$inventory_id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

     //Crear Inventario Fisico Parcial
    $app->post('/api/inventories/:id/inventory_details',$checkToken(),$checkScopes(array('collect.edit')),function($id) use ($app) {
         
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

        
        $body = $request->getBody();

        $InventoryDetailReceived = json_decode($body);
        $InventoryDetailReceived->InventoryDetailId = $id;

        $InventoryModel = new InventoryModel();
        $data =  $InventoryModel->createInventoryDetail($InventoryDetailReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });


	


	

?>