<?php

	require 'server/models/currency_model.php';
	require 'server/models/exchange_rate_model.php';

	//Obtener currencys
	$app->get('/api/currencies/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$CurrencyModel = new CurrencyModel();

		$data = [];
		$data = $CurrencyModel->getCurrencies();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

		//Obtener currencies
	$app->get('/api/currencies/search/:query', function ($query) use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$CurrencyModel = new CurrencyModel();
        
		$data = [];
		$data = $CurrencyModel->searchCurrencies($query);
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	 //Obtener un solo unidad
	$app->get('/api/currencies/:id', function ($id) use ($app){
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       $CurrencyModel = new CurrencyModel();

       $data = $CurrencyModel->getCurrency($id);

         $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	
	



    //Crear unidad
	$app->post('/api/currencies',function() use ($app) {
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$CurrencyReceived = json_decode($body);

    	$CurrencyModel = new CurrencyModel();
    	$data =  $CurrencyModel->createCurrency($CurrencyReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar un unidad
	$app->post('/api/currencies/:id',function($id) use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();

    	$CurrencyReceived = json_decode($body);
    	$CurrencyReceived->CurrencyId = $id;
    	//validaciones antes de actualizar
    
    	$CurrencyModel = new CurrencyModel();
    	$CurrencyModel->updateCurrency($CurrencyReceived);


        $json = json_encode($CurrencyReceived,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	//Eliminar unidad
	$app->delete('/api/currencies/:id',function($id) use ($app){
		$CurrencyModel = new CurrencyModel();
    	$CurrencyModel->deleteCurrency($id);
	});
	
	
	$app->get('/api/currencies/:id/exchange_rate/:exchange_rate_date', function ($id,$exchange_rate_date) use ($app){
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       $ExchangeRateModel = new ExchangeRateModel();

       $data = $ExchangeRateModel->getExchangeRate($id,$exchange_rate_date);

       $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	
	$app->post('/api/currencies/:id/exchange_rate/', function ($id) use ($app){
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        
        $body = $request->getBody();

    	$ExchangeRateReceived = json_decode($body);
    	
        $ExchangeRateReceived->currency_id = $id;
        
        $ExchangeRateModel = new ExchangeRateModel();

        $data = $ExchangeRateModel->setExchangeRate($ExchangeRateReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


?>