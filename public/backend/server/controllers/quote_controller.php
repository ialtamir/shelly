<?php

	require 'server/models/quote_model.php';
    require 'server/reports/quote_report.php';
    require 'server/reports/squares.php';

		//Obtener Quotes Pendientes
	$app->get('/api/quotes/', $checkToken(),$checkScopes(array('sales.view','sales.edit')),function () use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$data = [];

        $QuoteModel = new QuoteModel();
        $data = $QuoteModel->getPendingQuotes();
        $json = json_encode($data,JSON_NUMERIC_CHECK);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
       
    
	});



    //Obtener Quotes canceladas
    $app->get('/api/quotes/canceled/',$checkToken(),$checkScopes(array('sales.view','sales.edit')),  function () use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $QuoteModel = new QuoteModel();
        $data = $QuoteModel->getCanceledQuotes();

        $json = json_encode($data,JSON_NUMERIC_CHECK);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
       


    
    });

    //Obtener Quotes Procesadas 
    $app->get('/api/quotes/processed/',$checkToken(),$checkScopes(array('sales.view','sales.edit')),  function () use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $QuoteModel = new QuoteModel();
        $data = $QuoteModel->getProcessedQuotes();

        $json = json_encode($data,JSON_NUMERIC_CHECK);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    
    });


     //Obtener Quotes Completadas 
    $app->get('/api/quotes/completed/',$checkToken(),$checkScopes(array('sales.view','sales.edit')),  function () use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $QuoteModel = new QuoteModel();
        $data = $QuoteModel->getCompletedQuotes();

        $json = json_encode($data,JSON_NUMERIC_CHECK);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
       
    });

    $app->get('/api/quotes/ready_for_wo/', function () use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();



        $QuoteModel = new QuoteModel();

        $data = [];
        $data = $QuoteModel->getQuotesReadyForWorkOrders();


        $json = json_encode($data,JSON_NUMERIC_CHECK);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


    
    });
    

	 //Obtener una sola cotizacion
	$app->get('/api/quotes/:id',$checkToken(),$checkScopes(array('sales.view','sales.edit')), function ($id) {
       
       $QuoteModel = new QuoteModel();

       $QuoteJson = $QuoteModel->getQuote($id,true);

    	echo json_encode($QuoteJson,JSON_NUMERIC_CHECK);
	});


	//Crear una cotizacion
	$app->post('/api/quotes',$checkToken(),$checkScopes(array('sales.edit')),function() use ($app){
		 
        $app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$body = $request->getBody();

    	$QuoteReceived = json_decode($body);
    	
        $QuoteModel = new QuoteModel();
    	$QuoteJson =  $QuoteModel->createQuote($QuoteReceived);

    	echo json_encode($QuoteJson,JSON_NUMERIC_CHECK);

	});

	//Actualizar una cotizacion
	$app->post('/api/quotes/:id',$checkToken(),$checkScopes(array('sales.edit')),function($id) use ($app){

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$body = $request->getBody();

    	$QuoteReceived = json_decode($body);
    	$QuoteReceived->QuoteId = $id;
    	//validaciones antes de actualizar
    
    	$QuoteModel = new QuoteModel();
    	$QuoteReceived = $QuoteModel->updateQuote($QuoteReceived);

        echo json_encode($QuoteReceived,JSON_NUMERIC_CHECK);
	});

    //Aprobar una cotizacion
    $app->post('/api/quotes/:id/approve',$checkToken(),$checkScopes(array()),function($id) use ($app){

        $app = \Slim\Slim::getInstance();
        $request = $app->request();
        $body = $request->getBody();

        $QuoteReceived = json_decode($body);
        $QuoteReceived->QuoteId = $id;
        //validaciones antes de actualizar
    
        $QuoteModel = new QuoteModel();
        $QuoteReceived = $QuoteModel->approveQuote($QuoteReceived);

        echo json_encode($QuoteReceived,JSON_NUMERIC_CHECK);
    });

    //Cancelar una cotizacion
    $app->post('/api/quotes/:id/cancel',$checkToken(),$checkScopes(array()),function($id) use ($app){

        $app = \Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
   
        $body = $request->getBody();

        $QuoteReceived = json_decode($body);
        $QuoteReceived->QuoteId = $id;
      
    
        $QuoteModel = new QuoteModel();
        $QuoteReceived = $QuoteModel->cancelQuote($QuoteReceived);

        
        $json =  json_encode($QuoteReceived,JSON_NUMERIC_CHECK);            
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        

    });

     $app->post('/api/quotes/:id/create_sale_order',$checkToken(),$checkScopes(array('sales.edit')),function($id) use ($app){

        $app = \Slim\Slim::getInstance();
        $request = $app->request();
        $body = $request->getBody();

        $QuoteReceived = json_decode($body);
        $QuoteReceived->QuoteId = $id;
        //validaciones antes de actualizar
    
        $QuoteModel = new QuoteModel();
        $QuoteReceived = $QuoteModel->createSaleOrderFromQuote($QuoteReceived->QuoteId);

        echo json_encode($QuoteReceived,JSON_NUMERIC_CHECK);
    });

    //Eliminar una cotizacion
    $app->delete('/api/quotes/:id',$checkToken(),$checkScopes(array('sales.edit')),function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

        $QuoteModel = new QuoteModel();
        $QuoteModel->deleteQuote($id);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
 
    });

    //Impresion del reporte de cotizacion
    $app->get('/api/quotes/report/:id', function ($id) {
       
       $QuoteModel = new QuoteModel();

       $QuoteJson = $QuoteModel->getQuote($id,true);

       $QuoteReport = new QuoteReport();
     
       $QuoteReport->printReport($QuoteJson);

       
    });

     $app->get('/api/quotes/report/square/:id', function ($id) {
       

        $QuoteModel = new QuoteModel();

       $QuoteJson = $QuoteModel->getQuote($id,true);

 
       $SquaresReport = new SquaresReport();
     
       $SquaresReport->printReport($QuoteJson);

       
    });

    //Envio de cotizacion por correo electronico
    $app->post('/api/quotes/email/:id',$checkToken(),$checkScopes(array('sales.edit')), function ($id) use ($app){
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $res = $app->response();

        $body = $request->getBody();
        $QuoteReceived = json_decode($body);


       try{

           $QuoteModel = new QuoteModel();

           $QuoteJson = $QuoteModel->getQuote($id,true);

           $QuoteReport = new QuoteReport();
            
            //var_dump($QuoteReceived->email->EmailCC);

           if(isset($QuoteReceived->email->EmailCC)){

                 $QuoteCC = $QuoteReceived->email->EmailCC;
           }

           if(isset($QuoteReceived->email->EmailMessage)){
                $QuoteMailMessage = $QuoteReceived->email->EmailMessage;
           }


           $QuoteReport->sendReport($QuoteJson,$QuoteCC,$QuoteMailMessage);



       }catch(Exception $ex){

             $json = $ex->getMessage();

            $res['Content-Type'] = 'application/json';
            $res->status(500);
            //$res->body($json);
       }

    });

    $app->get('/api/quotes/:id/attachments/' ,$checkToken(),$checkScopes(array('sales.edit')),function($id) use ($app){
        


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

        $data = [];

        $QuoteModel = new QuoteModel();
        $data = $QuoteModel->getQuoteAttachments($id);
        $json = json_encode($data,JSON_NUMERIC_CHECK);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


    });


    $app->post('/api/quotes/:id/attachments/' ,$checkToken(),$checkScopes(array('sales.edit')),function($id) use ($app){


        //var_dump($_FILES);


         if(isset($_FILES["attachment"])){

            $account_configuration = AccountModel::getAccountConfiguration();                
            $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path;
             
             
            if(move_uploaded_file($_FILES["attachment"]["tmp_name"],$upload_path.$id.$_FILES['attachment']["name"])){

                $AttachmentFileName = $id.$_FILES['attachment']["name"];

                $QuoteAttachment = new stdClass();
                $QuoteAttachment->QuoteId = $id;
                $QuoteAttachment->QuoteAttachmentName = $AttachmentFileName;
                $QuoteAttachment->QuoteAttachmentFileName = $AttachmentFileName;
                $QuoteAttachment->QuoteAttachmentUrl = $upload_path.$id.$_FILES['attachment']["name"];                
                $QuoteModel = new QuoteModel();
                $QuoteModel->createQuoteAttachment($QuoteAttachment);

                echo json_encode($QuoteAttachment);
            }

        }


    });

     $app->post('/api/quotes/:id/attachments/:QuoteAttachmentId' ,$checkToken(),$checkScopes(array('sales.edit')),function($id,$QuoteAttachmentId) use ($app){




    });

     $app->delete('/api/quotes/:id/attachments/:QuoteAttachmentId' ,$checkToken(),$checkScopes(array('sales.edit')),function($id,$QuoteAttachmentId) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        $QuoteAttachment = new stdClass();
        $QuoteAttachment->QuoteId = $id;
        $QuoteAttachment->QuoteAttachmentId = $QuoteAttachmentId;
         
        $QuoteModel = new QuoteModel();
        $QuoteModel->deleteQuoteAttachment($QuoteAttachment);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


    });

?>