<?php
	require 'server/models/payment_model.php';
    require 'server/reports/payment_receipt_report.php';
		//Obtener Payments
	//$app->get('/api/payments/',$checkToken(),$checkScopes(array('payments.view')),function () {

		//$PaymentModel = new PaymentModel();

		//$data = [];
		//$data = $PaymentModel->getPayments();
    
    //	echo json_encode($data,JSON_NUMERIC_CHECK);
	//});
	
	//Obtener Payments
	$app->get('/api/payments/', function () use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
		$PaymentModel = new PaymentModel();

		$data = [];
		$data = $PaymentModel->getRemainingPayments();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

    $app->get('/api/payments/completed/', function () use ($app) {

        
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

        $PaymentModel = new PaymentModel();

        $data = [];
        $data = $PaymentModel->getCompletedPayments();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });
    
	


    $app->get('/api/payments/:id',$checkToken(),$checkScopes(array('payments.view')), function ($id)  use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       
       $PaymentModel = new PaymentModel();

       $data = $PaymentModel->getPayment($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });



	//Crear un Pago Total
	$app->post('/api/payments',$checkToken(),$checkScopes(array('payments.view')),function() use ($app){
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
    	$body = $request->getBody();
    	$PaymentReceived = json_decode($body);
    	//var_dump($SaleOrderReceived);
    	$PaymentModel = new PaymentModel();
    	$data =  $PaymentModel->createPayment($PaymentReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


	});

	//Actualizar un Pago Total
	$app->post('/api/payments/:id',$checkToken(),$checkScopes(array('payments.view')),function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
    	$body = $request->getBody();

    	$PaymentReceived = json_decode($body);
    	$PaymentReceived->PaymentId = $id;
    	//validaciones antes de actualizar
    
    	$PaymentModel = new PaymentModel();
    	$PaymentReceived=$PaymentModel->updatePayment($PaymentReceived);

        $json = json_encode($PaymentReceived,JSON_NUMERIC_CHECK);
        

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);        

	});

    //Eliminar un Pago Total
    $app->delete('/api/payments/:id',$checkToken(),$checkScopes(array('payments.view')),function($id) use ($app){
        $PaymentModel = new PaymentModel();
        $PaymentModel->deletePayment($id);
    });


    //API de pagos parciales
     $app->get('/api/payments/:id/payment_details',$checkToken(),$checkScopes(array('collect.view')), function ($id) use ($app) {
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

       $PaymentModel = new PaymentModel();

       $data = $PaymentModel->getPaymentDetails($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

    $app->get('/api/payments/:id/payment_details/:payment_detail_id', function ($id,$payment_id)  use ($app) {
       

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        


       $PaymentModel = new PaymentModel();

       $data = $PaymentModel->getPaymentDetail($id,$payment_id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

    $app->get('/api/payments/:id/payment_details/:payment_detail_id/report', function ($id,$payment_detail_id) use ($app) {
       

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

       $PaymentModel = new PaymentModel();

       $PaymentJson =  $PaymentModel->getPayment($id);
       $PaymentDetailJson = $PaymentModel->getPaymentDetail($id,$payment_detail_id);

       

        $PaymemtReceiptReport = new PaymemtReceiptReport();
      //var_dump($PaymentDetailJson);
       $PaymemtReceiptReport->printReport($PaymentDetailJson,$PaymentJson);


    });

	
    //Crear pago
    $app->post('/api/payments/:id/payment_details',$checkToken(),$checkScopes(array('collect.edit')),function($id) use ($app){
         
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        


        $body = $request->getBody();

        $PaymentDetailReceived = json_decode($body);
        $PaymentDetailReceived->PaymentId = $id;

        $PaymentModel = new PaymentModel();
        $data =  $PaymentModel->createOrUpdatePaymentDetail($id,$PaymentDetailReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

    //Actualizar pago
    $app->post('/api/payments/:id/payment_details/:payment_detail_id',$checkToken(),$checkScopes(array('collect.edit')),function($id,$payment_detail_id) use ($app){
         
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        $body = $request->getBody();

        $PaymentDetailReceived = json_decode($body);
        //$PaymentDetailReceived->PaymentId = $id;
        //$PaymentDetailReceived->PaymentDetailId = $payment_id;

        $PaymentModel = new PaymentModel();
        $data =  $PaymentModel->createOrUpdatePaymentDetail($id,$PaymentDetailReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

     $app->delete('/api/payments/:id/payment_details/:payment_id',$checkToken(),$checkScopes(array('collect.edit')),function($id,$payment_id) use ($app) {
         
       
        
        $PaymentDetailReceived = new stdClass();
        $PaymentDetailReceived->PaymentId = $id;
        $PaymentDetailReceived->PaymentDetailId = $payment_id;

        $PaymentModel = new PaymentModel();
        $PaymentModel->deletePaymentDetails($id,$PaymentDetailReceived);

 
    });

     //pagos pendientes
     $app->get('/api/receivable/payments/',$checkToken(),$checkScopes(array('collect.view')), function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

        $PaymentModel = new PaymentModel();

        $data = [];
        $data = $PaymentModel->getAllReceivablePayments();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });


     //pagos completos
     $app->get('/api/receivable/payments/completed/',$checkToken(),$checkScopes(array('collect.view')), function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        

        $PaymentModel = new PaymentModel();

        $data = [];
        $data = $PaymentModel->getAllPaymentsPayed();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

   

?>