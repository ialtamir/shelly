<?php

	require 'server/models/lead_model.php';

	//Obtener Prospectos 
	$app->get('/api/leads/', function () use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$LeadModel = new LeadModel();

		$data = [];
		$data = $LeadModel->getLeads();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

		//Buscar Prospectos
	$app->get('/api/leads/search/:query', function ($query) use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$LeadModel = new LeadModel();
        
		$data = [];
		$data = $LeadModel->searchLeads($query);
    
       	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	 //Obtener un solo prospecto
	$app->get('/api/leads/:id', function ($id) use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       	$LeadModel = new LeadModel();

       	$data = $LeadModel->getLead($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});



    //Crear prospecto
	$app->post('/api/leads',function() use ($app) {
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();
    	$LeadReceived = json_decode($body);

    	$LeadModel = new LeadModel();
    	$data =  $LeadModel->createLead($LeadReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar un prospecto
	$app->post('/api/leads/:id',function($id) use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();

    	$LeadReceived = json_decode($body);
    	$LeadReceived->LeadId = $id;
    	//validaciones antes de actualizar
    
    	$LeadModel = new LeadModel();
    	$LeadModel->updateLead($LeadReceived);

    	$json =  json_encode($LeadReceived,JSON_NUMERIC_CHECK);


        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);    	
	});

	//Eliminar un prospecto
	$app->delete('/api/leads/:id',function($id) use ($app){
		$LeadModel = new LeadModel();
    	$LeadModel->deleteLead($id);
	});


	//convertir a cliente
	$app->post('/api/leads/open/:id',function($id) use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();
    	$LeadReceived = json_decode($body);
        $LeadReceived->LeadId = $id;
       
    	$LeadModel = new LeadModel();
    	$data =  $LeadModel->convertLeadToCustomer($LeadReceived,$id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


  
?>