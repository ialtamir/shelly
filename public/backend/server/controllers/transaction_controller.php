<?php
	
	require 'server/models/transaction_model.php';


	$app->get('/api/transaction/update/', function () {

		$host_name = $_SERVER["HTTP_HOST"];
		$TransactionModel = new TransactionModel(); 
        $data = $TransactionModel->updateTransactionReference();

     
    	
    	echo 'DONE';//json_encode($data,JSON_NUMERIC_CHECK);
	});

	$app->get('/api/transaction/:TransactionId', function ($TransactionId) {

		$host_name = $_SERVER["HTTP_HOST"];
		$TransactionModel = new TransactionModel(); 
        $data = $TransactionModel->getTransactions($TransactionId);

     
    	
    	echo json_encode($data);//json_encode($data,JSON_NUMERIC_CHECK);
	});
?>