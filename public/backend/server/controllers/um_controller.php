<?php
require 'server/models/um_model.php';

	//Obtener Places
	$app->get('/api/ums/', function () use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

		$UMModel = new UMModel();

		$data = [];
		$data = $UMModel->getUMS();
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

		 //Obtener una sola unidad de medida
	$app->get('/api/ums/:id', function ($id) use ($app) {
       
       	$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

       $UMModel = new UMModel();

       	$data = $UMModel->getUM($id);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


	});

	 //Crear unidad de medida
	$app->post('/api/ums/',function() use ($app){
		 
		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

    	$body = $request->getBody();
    	$UMReceived = json_decode($body);

    	$UMModel = new UMModel();
    	$data =  $UMModel->createUM($UMReceived);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar unidad de medida
	$app->post('/api/ums/:id',function($id) use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();


    	$body = $request->getBody();

    	$UMReceived = json_decode($body);
    	$UMReceived->UMId = $id;
    	//validaciones antes de actualizar
    
    	$UMModel = new UMModel();
    	$UMModel->updateUM($UMReceived);

	});

    //Eliminar unidad de medida
	$app->delete('/api/ums/:id',function($id) use ($app){
		$UMModel = new UMModel();
    	$UMModel->deleteUM($id);
	});


?>