<?php
	require 'server/models/slider_model.php';

		//Obtener Categorys
	$app->get('/api/sliders/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$Slider = new SliderModel();

		$data = [];
		$data = $Slider->getAll();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	 //Obtener un solo cliente
	$app->get('/api/sliders/:id', function ($id) use ($app) {
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       $Slider = new SliderModel();

       $data = $Slider->get($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


    $app->get('/api/sliders/:id/products/',$checkToken(),$checkScopes(array('maintainance.view')), function ($id) use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


       $ProductModel = new ProductModel();

       $data = [];
       $data = $ProductModel->getProductsByCategory($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

	//Crear cliente
	$app->post('/api/sliders',function() use ($app){
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();
    	$slider = json_decode($body,true);

    	$Slider = new SliderModel();
    	
    	$data =  $Slider->create($slider);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	//Actualizar un cliente
	$app->post('/api/sliders/:id',function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();

    	$slider = json_decode($body,true);
    	$slider["id"] =  $id;
    	//validaciones antes de actualizar
    
    	$Slider = new SliderModel();
    	$Slider->update($slider);

        $json = json_encode($slider,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	//Eliminar un cliente
	$app->delete('/api/sliders/:id',function($id) use ($app){
	    

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$Slider = new SliderModel();
    	
    	$Slider->delete($id);

        $json = json_encode($slider,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        
        
	});

	$app->post('/api/sliders/:id/upload/',function ($id) use ($app) {
        

 

        //var_dump($_FILES);


		 if(isset($_FILES["myFile"])){

            $account_configuration = AccountModel::getAccountConfiguration();                
            $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path;
             
             
        	if(move_uploaded_file($_FILES["myFile"]["tmp_name"],$upload_path.$id.$_FILES['myFile']["name"])){

  				$image = $upload_path.$id.$_FILES['myFile']["name"];
        	
    			
                $image = ABSOLUTE_BASE_URL.$id.$_FILES['myFile']["name"];
                
                
                $Slider = new SliderModel();
    			$Slider->update_picture($id,$image);
    			
    			
    			echo '{ "url": "'.$image.'" }';
        	}

        }
        
        
        
        


	});

	$app->post('/api/sliders/:id/crop/',$checkToken(),$checkScopes(array('maintainance.view')),function ($id) use ($app) {

		 if(isset($_FILES["file"])){

            $account_configuration = AccountModel::getAccountConfiguration();                
            $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path;
             
             
        	if(move_uploaded_file($_FILES["file"]["tmp_name"],$upload_path.$id.$_FILES['file']["name"])){

  				$CategoryPicture = $upload_path.$id.$_FILES['file']["name"];
        		$CategoryModel = new CategoryModel();
    			$CategoryModel->updateCategoryPicture($id,$CategoryPicture);

    			echo '{ "url": "'.$CategoryPicture.'" }';
        	}

        }


	});


?>