<?php

	require 'server/models/dashboard_model.php';

	$app->get('/api/dashboard/',function() use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$dateFrom = $app->request()->get('dateFrom');
		$dateTo = $app->request()->get('dateTo');

		$DashboardModel = new DashboardModel();


		$data = [];
		
		

		$quoteCount = $DashboardModel->getCountQuotes($dateFrom,$dateTo);
		$pendingsQuotes = $DashboardModel->getCountStatusQuotes($dateFrom,$dateTo,"Pendiente");
		$approvedQuotes = $DashboardModel->getCountStatusQuotes($dateFrom,$dateTo,"Aprobada");
		$cancelledQuotes = $DashboardModel->getCountStatusQuotes($dateFrom,$dateTo,"Cancelada");

		$quoteData = array("Total" => $quoteCount , "Pendings" => $pendingsQuotes , "Approved" => $approvedQuotes,"Cancelled" => $cancelledQuotes);
		
		//Sales data
		$salesCount = $DashboardModel->getCountSales($dateFrom,$dateTo);

		$pendingsSales = $DashboardModel->getCountStatusSales($dateFrom,$dateTo,"Pendiente");
		$approvedSales = $DashboardModel->getCountStatusSales($dateFrom,$dateTo,"Aprobada");
		$cancelledSales = $DashboardModel->getCountStatusSales($dateFrom,$dateTo,"Cancelada");

		$todaySales = $DashboardModel->getAmountSales();


		$salesData = array("Total" => $salesCount , "Pendings" => $pendingsSales , "Approved" => $approvedSales,"Cancelled" => $cancelledSales,"Today" => $todaySales);


		$worksCount = $DashboardModel->getCountWorks($dateFrom,$dateTo);

		$newWorks = $DashboardModel->getCountStatusWorks($dateFrom,$dateTo,"Nueva");
		$openedWorks = $DashboardModel->getCountStatusWorks($dateFrom,$dateTo,"Abierta");
		$closedWorks = $DashboardModel->getCountStatusWorks($dateFrom,$dateTo,"Cerrada");

		$worksData = array("Total" => $worksCount , "New" => $newWorks , "Opened" => $openedWorks,"Closed" => $closedWorks);


		$paymentsCount = $DashboardModel->getCountPayments($dateFrom,$dateTo);

		$paymentMxnAmount = $DashboardModel->getAmountPaymentsMxn($dateFrom,$dateTo);
		$paymentDllsAmount = $DashboardModel->getAmountPaymentsDlls($dateFrom,$dateTo);

		$paymentsData = array("Total" => $paymentsCount , "MxnAmount" => $paymentMxnAmount, "DllsAmount" => $paymentDllsAmount  );
		

		$data = array("Quotes" => $quoteData , "Sales" => $salesData, "Works" => $worksData, "Payments" => $paymentsData );	

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);



	});

	// //Obtener Cotizaciones
	// $app->get('/api/dashboard/quotes', function () use ($app) {

	// 	$app = Slim\Slim::getInstance();
       
	// 	$dateFrom = $app->request()->get('dateFrom');
	// 	$dateTo = $app->request()->get('dateTo');

	// 	$DashboardModel = new DashboardModel();

	// 	$data = [];
	// 	$data = $DashboardModel->getCountQuotes($dateFrom,$dateTo);

    
 //    	echo json_encode($data,JSON_NUMERIC_CHECK);
	// });
	
	// //Obtener Cotizaciones por Estatus
	// $app->get('/api/dashboard/quotesbystatus', function () use ($app) {

	// 	$app = Slim\Slim::getInstance();
       
	// 	$dateFrom = $app->request()->get('dateFrom');
	// 	$dateTo = $app->request()->get('dateTo');
	// 	$statusQuote = $app->request()->get('statusQuote');

	// 	$DashboardModel = new DashboardModel();

	// 	$data = [];
	// 	$data = $DashboardModel->getCountStatusQuotes($dateFrom,$dateTo,$statusQuote);
    
 //    	echo json_encode($data,JSON_NUMERIC_CHECK);
	// });
	
	// //Obtener Ordenes de Venta
	// $app->get('/api/dashboard/sales', function () use ($app) {

	// 	$app = Slim\Slim::getInstance();
       
	// 	$dateFrom = $app->request()->get('dateFrom');
	// 	$dateTo = $app->request()->get('dateTo');


	// 	$DashboardModel = new DashboardModel();

	// 	$data = [];
	// 	$data = $DashboardModel->getCountSales($dateFrom,$dateTo);
    
 //    	echo json_encode($data,JSON_NUMERIC_CHECK);
	// });
	
	
	// //Obtener Ventas por Estatus
	// $app->get('/api/dashboard/salesbystatus', function () use ($app) {

	// 	$app = Slim\Slim::getInstance();
       
	// 	$dateFrom = $app->request()->get('dateFrom');
	// 	$dateTo = $app->request()->get('dateTo');
	// 	$statusSale = $app->request()->get('statusSale');

	// 	$DashboardModel = new DashboardModel();

	// 	$data = [];
	// 	$data = $DashboardModel->getCountStatusSales($dateFrom,$dateTo,$statusSale);
    
 //    	echo json_encode($data,JSON_NUMERIC_CHECK);
	// });
	
	
	

	//Obtener Ordenes de Trabajo
	$app->get('/api/dashboard/works', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
       
		$dateFrom = $app->request()->get('dateFrom');
		$dateTo = $app->request()->get('dateTo');

		$DashboardModel = new DashboardModel();

		$data = [];
		$data = $DashboardModel->getCountWorks($dateFrom,$dateTo);
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	//Ordenes de Trabajo por Estatus
	$app->get('/api/dashboard/worksbystatus', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
       
		$dateFrom = $app->request()->get('dateFrom');
		$dateTo = $app->request()->get('dateTo');
		$statusWork = $app->request()->get('statusWork');

		$DashboardModel = new DashboardModel();

		$data = [];
		$data = $DashboardModel->getCountStatusWorks($dateFrom,$dateTo,$statuswork);
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	

	//Obtener Pagos recibidos
	$app->get('/api/dashboard/payments', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
       
		$dateFrom = $app->request()->get('dateFrom');
		$dateTo = $app->request()->get('dateTo');

		$DashboardModel = new DashboardModel();

		$data = [];
		$data = $DashboardModel->getCountPayments($dateFrom,$dateTo);
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	//Obtener Monto de Pagos recibidos en Pesos
	$app->get('/api/dashboard/paymentsamountmxn', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$dateFrom = $app->request()->get('dateFrom');
		$dateTo = $app->request()->get('dateTo');

		$DashboardModel = new DashboardModel();

		$data = [];
		$data = $DashboardModel->getAmountPaymentsMxn($dateFrom,$dateTo);
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});
	
	
	//Obtener Monto de Pagos recibidos en Dolares
	$app->get('/api/dashboard/paymentsamountdlls', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
       
		$dateFrom = $app->request()->get('dateFrom');
		$dateTo = $app->request()->get('dateTo');

		$DashboardModel = new DashboardModel();

		$data = [];
		$data = $DashboardModel->getAmountPaymentsDlls($dateFrom,$dateTo);
    
       	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	
	
	
    //Obtener Monto de Ventas HOY
	$app->get('/api/dashboard/salesamount', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
       
  	    $dateFrom = $app->request()->get('dateFrom');
		$dateTo = $app->request()->get('dateTo');
		
		$DashboardModel = new DashboardModel();

		$data = [];
		$data = $DashboardModel->getAmountSales();
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);


	});
	


	//Obtener Grafica de Pagos en Pesos
	$app->get('/api/dashboard/chartpayments/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$year = $app->request()->get('year');


		$DashboardModel = new DashboardModel();

		$data = [];
		$data = $DashboardModel->getChartPayments($year);
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	

	//Obtener Grafica de Ventas
	$app->get('/api/dashboard/chartsaleorders/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$year = $app->request()->get('year');
		
		$DashboardModel = new DashboardModel();

		$data = [];
		$data = $DashboardModel->getChartSaleOrders($year);
    
     	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        
	});
	
	

?>