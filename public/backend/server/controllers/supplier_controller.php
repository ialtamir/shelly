<?php

	require 'server/models/supplier_model.php';

	//Obtener suppliers
	$app->get('/api/suppliers/', function () use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

		$SupplierModel = new SupplierModel();

		$data = [];
		$data = $SupplierModel->getSuppliers();
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

		//Obtener suppliers
	$app->get('/api/suppliers/search/:query', function ($query) use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

		$SupplierModel = new SupplierModel();
        
		$data = [];
		$data = $SupplierModel->searchSuppliers($query);
    
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	 //Obtener un solo proveedor
	$app->get('/api/suppliers/:id', function ($id) {
       
       	$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

      	$SupplierModel = new SupplierModel();

       	$data = $SupplierModel->getSupplier($id);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});



    //Crear proveedor
	$app->post('/api/suppliers',function() use ($app) {
		 
        $app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

    	$body = $request->getBody();
    	$SupplierReceived = json_decode($body);

    	$SupplierModel = new SupplierModel();
    	$data =  $SupplierModel->createSupplier($SupplierReceived);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Actualizar un Proveedor
	$app->post('/api/suppliers/:id',function($id) use ($app) {

		$app = \Slim\Slim::getInstance();
    	$request = $app->request();
    	$response = $app->response();

    	
    	$body = $request->getBody();

    	$SupplierReceived = json_decode($body);
    	$SupplierReceived->SupplierId = $id;
    	//validaciones antes de actualizar
    
    	$SupplierModel = new SupplierModel();
    	$data = $SupplierModel->updateSupplier(SupplierReceived);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	//Eliminar un Proveedor
	$app->delete('/api/suppliers/:id',function($id) use ($app){
		$SupplierModel = new SupplierModel();
    	$SupplierModel->deleteSupplier($id);
	});



?>