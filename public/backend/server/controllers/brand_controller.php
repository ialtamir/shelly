<?php
	require 'server/models/brand_model.php';

		//Obtener brands
	$app->get('/api/brands/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$brandModel = new brandModel();

		$data = [];
		$data = $brandModel->getbrands();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});
	
	$app->get('/api/brands/ecommerce', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$brandModel = new brandModel();

		$data = [];
		$data = $brandModel->getbrandsEcommerceEnabled();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});
	
	$app->get('/api/brands/featured/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$brandModel = new brandModel();

		$data = [];
		$data = $brandModel->getbrandsWithFeaturedProducts();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	 //Obtener un solo cliente
	$app->get('/api/brands/:id',$checkToken(),$checkScopes(array('maintainance.view')), function ($id) use ($app) {
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       $brandModel = new brandModel();

       $data = $brandModel->getbrand($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


    $app->get('/api/brands/:id/products/',$checkToken(),$checkScopes(array('maintainance.view')), function ($id) use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


       $brandModel = new brandModel();

       $data = [];
       $data = $ProductModel->getProductsBybrand($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

    
    
    //Crear un producto
	$app->post('/api/brands/sync',function() use ($app) {
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
    	$body = $request->getBody();
    	
    	$CategoriesReceived = json_decode($body);
    	
    	//var_dump($ProductReceived);
    	$brandModel = new brandModel();
    	$data =  $brandModel->syncCategories($CategoriesReceived);

        $json = '{ "Result" : "'.gettype($CategoriesReceived).'" }';

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	

	//Crear cliente
	$app->post('/api/brands',$checkToken(),$checkScopes(array('maintainance.view')),function() use ($app){
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();
    	$brandReceived = json_decode($body);

    	$brandModel = new brandModel();
    	$data =  $brandModel->createbrand($brandReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	//Actualizar un cliente
	$app->post('/api/brands/:id',$checkToken(),$checkScopes(array('maintainance.view')),function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();

    	$brandReceived = json_decode($body);
    	$brandReceived->brandId = $id;
    	//validaciones antes de actualizar
    
    	$brandModel = new brandModel();
    	$brandModel->updatebrand($brandReceived);

        $json = json_encode($brandReceived,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	//Eliminar un cliente
	$app->delete('/api/brands/:id',$checkToken(),$checkScopes(array('maintainance.view')),function($id) use ($app){
		$brandModel = new brandModel();
    	$brandModel->deletebrand($id);
	});

	$app->post('/api/brands/:id/upload/',$checkToken(),$checkScopes(array('maintainance.view')),function ($id) use ($app) {
        

 

        //var_dump($_FILES);


		 if(isset($_FILES["myFile"])){

            $account_configuration = AccountModel::getAccountConfiguration();                
            $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path;
             
             
        	if(move_uploaded_file($_FILES["myFile"]["tmp_name"],$upload_path.$id.$_FILES['myFile']["name"])){

  				$brandPicture = $upload_path.$id.$_FILES['myFile']["name"];
        	
    			
                $brandPicture_URL = ABSOLUTE_BASE_URL.$id.$_FILES['myFile']["name"];
                
                
                $brandModel = new brandModel();
    			$brandModel->updatebrandPicture($id,$brandPicture_URL);
    			
    			
    			echo '{ "url": "'.$brandPicture_URL.'" }';
        	}

        }
        
        
        
        


	});

	$app->post('/api/brands/:id/crop/',$checkToken(),$checkScopes(array('maintainance.view')),function ($id) use ($app) {

		 if(isset($_FILES["file"])){

            $account_configuration = AccountModel::getAccountConfiguration();                
            $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path;
             
             
        	if(move_uploaded_file($_FILES["file"]["tmp_name"],$upload_path.$id.$_FILES['file']["name"])){

  				$brandPicture = $upload_path.$id.$_FILES['file']["name"];
        		$brandModel = new brandModel();
    			$brandModel->updatebrandPicture($id,$brandPicture);

    			echo '{ "url": "'.$brandPicture.'" }';
        	}

        }


	});


?>