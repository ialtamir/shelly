<?php

	require 'server/models/facturas_model.php';
	require 'server/reports/factura_report.php';
	include_once "../../libraries/Cfdi/cfdi32_multifacturas.php";

        /*
	//Obtener Materials
	$app->get('/api/materials/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$FacturasModel = new FacturasModel();

		$data = [];
		$data = $FacturasModel->getMaterials();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
        */
		//Facturar
	$app->post('/api/facturar/:SaleOrderId',$checkToken(), function ($SaleOrderId) use ($app) {
        

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
		
		//VALIDAR QUE EL CLIENTE TIENE FOLIOS DISPONIBLES
		
		
		
		//PENDIENTE VALIDAR QUE LA ORDEN NO ESTE FACTURADA
		$SaleOrderModel = new SaleOrderModel();
		$sale_order = $SaleOrderModel->getSaleOrder($SaleOrderId);
		
		try {
		    
		    if((!isset($sale_order["InvoiceId"]) || $sale_order["InvoiceId"] == "") && $sale_order["SaleOrderStatus"] == "Aprobada"){
    		
        		$FacturasModel = new FacturasModel();
                
        		$data = [];
        		$data = $FacturasModel->createFactura($SaleOrderId);
                //var_dump($data);
                $json = json_encode($data,JSON_NUMERIC_CHECK);
        
                $response['Content-Type'] = 'application/json';
                $response->status(200);
                $response->body($json);
		    }
		    else {
		    
		        throw new Exception("Esta orden de venta ya fue timbrada previamente");
		    }
		}   
		catch(Exception $ex) {
		    //var_dump($e);
		    $json = json_encode(array("error" => $ex->getMessage()));
		    $response['Content-Type'] = 'application/json';
            $response->status(500);
            $response->body($json);
            
		}

    	
	});
	
	//Contrato de Venta
     $app->get('/api/facturar/delete/:SaleOrderId', function ($SaleOrderId) {
       try {
           
            $FacturasModel = new FacturasModel();
        
            $FacturasJson = $FacturasModel->deleteInvoice($SaleOrderId);
       
       }
       catch(Exception $ex) {
		    
		    $json = json_encode(array("error" => $ex->getMessage()));
		    $response['Content-Type'] = 'application/json';
            $response->status(500);
            $response->body($json);
            
		}
        
    });
	
	 //Contrato de Venta
     $app->get('/api/facturar/report/:id', function ($id) {
       
       $FacturasModel = new FacturasModel();

       $FacturasJson = (object)$FacturasModel->getInvoice($id);
        
       $FacturaReport = new FacturaReport();
    
       $FacturaReport->printReport($FacturasJson);

        //echo json_encode($QuoteJson,JSON_NUMERIC_CHECK);
    });
    
     $app->get('/api/facturar/send/:id', function ($id) {
       
       $FacturasModel = new FacturasModel();

       $FacturasJson = (object)$FacturasModel->getInvoice($id);
        
       $FacturaReport = new FacturaReport();
    
       $FacturaReport->sendReport($FacturasJson);

        //echo json_encode($QuoteJson,JSON_NUMERIC_CHECK);
    });
    
     $app->get('/api/facturar/xml/:id', function ($id) {
       
       $FacturasModel = new FacturasModel();

       $FacturasJson = $FacturasModel->getInvoice($id);
        
       /*$FacturaReport = new FacturaReport();
    
       $FacturaReport->printReport($FacturasJson);
       */

        //echo json_encode($QuoteJson,JSON_NUMERIC_CHECK);
    });



	


?>