<?php
	require 'server/models/employee_model.php';

	//Get Employees by position
	$app->get('/api/employees/by_position/:position', function ($position)  use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

        $EmployeeModel = new EmployeeModel();
        
		$data = [];
		$data = $EmployeeModel->getEmployeesByPosition($position);
    
   		$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

	//Obtener Empleados
	$app->get('/api/employees/', function () {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$EmployeeModel = new EmployeeModel();

		$data = [];
		$data = $EmployeeModel->getEmployees();
    
   		$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	


	$app->get('/api/employees/search/:query', function ($query)  use ($app) {
		

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$EmployeeModel = new EmployeeModel();
        
		$data = [];
		$data = $EmployeeModel->searchEmployees($query);
   	
   		$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	 //Obtener un solo empleado
	$app->get('/api/employees/:id', function ($id)  use ($app) {
             
		$app = Slim\Slim::getInstance();
		$request = $app->request();
		$response = $app->response();

       $EmployeeModel = new EmployeeModel();

       $data = $EmployeeModel->getEmployee($id);

  
    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	  //Crear Empleado
	$app->post('/api/employees',function() use ($app) {
		 

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$EmployeeReceived = json_decode($body);

    	$EmployeeModel = new EmployeeModel();
    	$data =  $EmployeeModel->createEmployee($EmployeeReceived);

    	//echo json_encode($EmployeeJson,JSON_NUMERIC_CHECK);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	//Actualizar un Empleado
	$app->post('/api/employees/:id',function($id) use ($app){


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();

    	$EmployeeReceived = json_decode($body);
    	$EmployeeReceived->EmployeeId = $id;
    	//validaciones antes de actualizar
    
    	$EmployeeModel = new EmployeeModel();
    	$EmployeeModel->updateEmployee($EmployeeReceived);

    	//echo json_encode($EmployeeReceived,JSON_NUMERIC_CHECK);

    	$json = json_encode($EmployeeReceived,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});
	
	
	//Eliminar un Empleado
	$app->delete('/api/employees/:id',function($id) use ($app){
		$EmployeeModel = new EmployeeModel();
    	$EmployeeModel->deleteEmployee($id);
	});
	

?>