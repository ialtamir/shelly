<?php
	require 'server/models/user_model.php';
    
    
    // New Slim app
//Implement Get Glient

    $app->get('/api/current-user',$checkToken(), function() use ($app){
        
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

        $id = getOwnerId();
        
        $userModel = new UserModel();

        $user = $userModel->getUser($id);
        
        $user->place = new stdClass();
        
        if($user->place_id && !$user->user_is_admin){
        	
        	$PlaceModel = new PlaceModel();
        	$user->place = $PlaceModel->getPlace($user->place_id);
        }



        $json = json_encode(array('user' =>array(
                'UserId' => $user->user_id,
                'UserName' => $user->user_name,
                'UserEmail' =>$user->user_email,
                'UserIsAdmin' => (bool) $user->user_is_admin,
                'PlaceId' => (bool) $user->place_id,
                'place' => $user->place
            )));



        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

    });


    //Obtener Users
	$app->get('/api/users/',$checkToken(), function ()  use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

		$UserModel = new UserModel();

		$dbusers = $UserModel->getUsers();

        $data = [];

		foreach($dbusers as $user){
               
        	array_push($data,array(
            	'UserId' => $user->user_id,
               	'UserName' => $user->user_name,
               	'UserEmail' =>$user->user_email,
                'UserIsAdmin' => (bool)$user->user_is_admin,
                'PlaceId' => $user->place_id
            ));

		}

        //sleep(5);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

     //Obtener un solo usuario
	$app->get('/api/users/:id',$checkToken(), function ($id)  use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
		$UserModel = new UserModel();

		$user = $UserModel->getUser($id);

        
        $data = array(
            	'UserId' => $user->user_id,
               	'UserName' => $user->user_name,
               	'UserEmail' =>$user->user_email,
                'UserIsAdmin' =>(bool) $user->user_is_admin,
                'PlaceId' => $user->place_id
         );

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});

    $app->get('/api/users/:id/scopes/',$checkToken(), function ($id)  use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        $UserModel = new UserModel();

        $data = $UserModel->getUserScopesSelected($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

     $app->post('/api/users/:id/scopes/',$checkToken(), function ($id)  use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

        $body = $request->getBody();
        $receivedScopes = json_decode($body);


        $UserModel = new UserModel();

        $data = $UserModel->saveUserScopes($receivedScopes);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

    //crear un usuario
	$app->post('/api/users',$checkToken(),function() use ($app){

    
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$receivedUser = json_decode($body);

       //Validaciones antes de guardar
    	if(!isset($receivedUser->UserIsAdmin)) $receivedUser->UserIsAdmin = 0 ;


    	$UserModel = new UserModel();
    	$data =  $UserModel->createUser($receivedUser);

    	$json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

   

    });
    
    
    $app->post('/api/ecommerce_users/',function() use ($app){

        try {
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$receivedUser = json_decode($body);
    	


    	$UserModel = new UserModel();
    	$data =  $UserModel->createEcommerceUser($receivedUser);

    
        
        //Obtener los parametros
        $_POST['grant_type'] = 'password';
        $_POST['client_id'] = CLIENT_ID;
        $_POST['client_secret'] = CLIENT_SECRET;
        $_POST['username'] = $data->email;
        $_POST['password'] = $data->password;
        //$_POST['scope'] = 'access'; //ir por los scopes al usuario

        $tokenInfo=PasswordGrantAuthentication($_POST);
        
       


        $sessionModel = new SessionModel();
        $sessionInfo  = $sessionModel->validateAccessToken($tokenInfo['access_token']);
        
        $userModel = new UserModel();    
        $AccountModel = new AccountModel();    

        $userInfo = $userModel->getUser($sessionInfo['owner_id']);

        $userScopes = $userModel->getUserScopes($sessionInfo['owner_id']);
        
        
        //var_dump($userInfo);
        
        $userInfo->place = new stdClass();
        
        if($userInfo->place_id){
        	
        	$PlaceModel = new PlaceModel();
        	$userInfo->place = $PlaceModel->getPlace($userInfo->place_id);
        }
        
       

        $user=array(
            'user' => array(
                'UserId' => $userInfo->user_id,
                'UserEmail' => $userInfo->user_email,
                'UserName' => $userInfo->user_name,
                'UserIsAdmin' => $userInfo->user_is_admin,
                'PlaceId' => $userInfo->place_id,
                'place' => $userInfo->place
            ),
            'token'=> array(
                'access_token'=>$tokenInfo['access_token'],
                'token_type'=> $tokenInfo['token_type'],
                'expires' => $tokenInfo['expires'],
                'expires_in' => $tokenInfo['expires_in'],
                'refresh_token'=>$tokenInfo['refresh_token'],
            )
            
        );
        
         if(isset($userInfo->user_type)){
            
            if($userInfo->user_type == "Customer"){
                
                $customer_model = new CustomerModel();
                $customer = $customer_model->getCustomerByUserId($userInfo->user_id);
                $user['customer'] = $customer;
            }
            
            
        }

        $json = json_encode($user);
        
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

        }catch(Exception $ex){
            
            $http_status = 500;
            
            if($ex->getCode()=="23000"){
                
              $json = json_encode(array("error" => "El correo electrónico ya existe"));
               $http_status = 500;
               
            }
            else {
              $json = json_encode(array("error" => $ex->getMessage()));  
            }
            
        
            $response['Content-Type'] = 'application/json';
            $response->status($http_status);
            $response->body($json);
        
        
        }
        

    });
    
    $app->post('/api/ecommerce_users/:id',$checkToken(),function($id) use ($app){



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	
    
    	
    

    	$received = json_decode($body,true);
    	
    	
    
    	$CustomerModel = new CustomerModel();
    	$received["customer"] = $CustomerModel->updateCustomerEcommerce($received["user"],$received["customer"]);

    	//echo json_encode($receivedUser);

        $json = json_encode($received,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

   
   
   
   

    });

    //actualizar un usuario
    $app->post('/api/users/:id',$checkToken(),function($id) use ($app){

    
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();

    	$receivedUser = json_decode($body);
    	$receivedUser->UserId = $id;
    	//validaciones antes de actualizar
    
    	$UserModel = new UserModel();
    	$UserModel->updateUser($receivedUser);

    	//echo json_encode($receivedUser);

        $json = json_encode($receivedUser,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

   

    });

    //eliminar un usuario
    $app->delete('/api/users/:id',$checkToken(),function($id) use ($app){

    
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
        
        
    	$receivedUser =(object) array();
    	$receivedUser->UserId = $id;
    
    	$UserModel = new UserModel();
    	$UserModel->deleteUser($receivedUser);

    	//echo json_encode($receivedUser);

   

    });


    //setear el password de un usuario
    $app->post('/api/users/:id/changepassword',$checkToken(),function($id) use ($app){

        

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$receivedUser = json_decode($body);

        //Validate that the password doesn't comes empty
    	$receivedUser->UserId = $id;
    

    	$UserModel = new UserModel();
    	$data =  $UserModel->setUserPassword($receivedUser);
       
    	//echo json_encode($jsonUser);

        $json = json_encode($data);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

   

    });
    
    //setear el password de un usuario
    $app->post('/api/users/setnewpassword/',function() use ($app){

        

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

    	$body = $request->getBody();
    	$receivedUser = json_decode($body);

        //Validate that the password doesn't comes empty
    
    

    	$UserModel = new UserModel();
    	$data =  $UserModel->setUserNewPassword($receivedUser);
       
    	//echo json_encode($jsonUser);

        $json = json_encode($data);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

   

    });
    
    
	
	
    






?>