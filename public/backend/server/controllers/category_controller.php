<?php
	require 'server/models/category_model.php';

		//Obtener Categorys
	$app->get('/api/categories/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$CategoryModel = new CategoryModel();

		$data = [];
		$data = $CategoryModel->getCategories();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});
	
	$app->get('/api/categories/ecommerce', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$CategoryModel = new CategoryModel();

		$data = [];
		$data = $CategoryModel->getCategoriesEcommerceEnabled();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});
	
	$app->get('/api/categories/featured/', function () use ($app) {

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


		$CategoryModel = new CategoryModel();

		$data = [];
		$data = $CategoryModel->getCategoriesWithFeaturedProducts();
    
        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	 //Obtener un solo cliente
	$app->get('/api/categories/:id',$checkToken(),$checkScopes(array('maintainance.view')), function ($id) use ($app) {
       
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();

       $CategoryModel = new CategoryModel();

       $data = $CategoryModel->getCategory($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});


    $app->get('/api/categories/:id/products/',$checkToken(),$checkScopes(array('maintainance.view')), function ($id) use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


       $ProductModel = new ProductModel();

       $data = [];
       $data = $ProductModel->getProductsByCategory($id);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    });

    
    
    //Crear un producto
	$app->post('/api/categories/sync',function() use ($app) {
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
    	$body = $request->getBody();
    	
    	$CategoriesReceived = json_decode($body);
    	
    	//var_dump($ProductReceived);
    	$CategoryModel = new CategoryModel();
    	$data =  $CategoryModel->syncCategories($CategoriesReceived);

        $json = '{ "Result" : "'.gettype($CategoriesReceived).'" }';

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
	});
	
	

	//Crear cliente
	$app->post('/api/categories',$checkToken(),$checkScopes(array('maintainance.view')),function() use ($app){
		 
        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();
    	$CategoryReceived = json_decode($body);

    	$CategoryModel = new CategoryModel();
    	$data =  $CategoryModel->createCategory($CategoryReceived);

        $json = json_encode($data,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	//Actualizar un cliente
	$app->post('/api/categories/:id',$checkToken(),$checkScopes(array('maintainance.view')),function($id) use ($app){

        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


    	$body = $request->getBody();

    	$CategoryReceived = json_decode($body);
    	$CategoryReceived->CategoryId = $id;
    	//validaciones antes de actualizar
    
    	$CategoryModel = new CategoryModel();
    	$CategoryModel->updateCategory($CategoryReceived);

        $json = json_encode($CategoryReceived,JSON_NUMERIC_CHECK);

        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);

	});

	//Eliminar un cliente
	$app->delete('/api/categories/:id',$checkToken(),$checkScopes(array('maintainance.view')),function($id) use ($app){
		$CategoryModel = new CategoryModel();
    	$CategoryModel->deleteCategory($id);
	});

	$app->post('/api/categories/:id/upload/',$checkToken(),$checkScopes(array('maintainance.view')),function ($id) use ($app) {
        

 

        //var_dump($_FILES);


		 if(isset($_FILES["myFile"])){

            $account_configuration = AccountModel::getAccountConfiguration();                
            $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path;
             
             
        	if(move_uploaded_file($_FILES["myFile"]["tmp_name"],$upload_path.$id.$_FILES['myFile']["name"])){

  				$CategoryPicture = $upload_path.$id.$_FILES['myFile']["name"];
        	
    			
                $CategoryPicture_URL = ABSOLUTE_BASE_URL.$id.$_FILES['myFile']["name"];
                
                
                $CategoryModel = new CategoryModel();
    			$CategoryModel->updateCategoryPicture($id,$CategoryPicture_URL);
    			
    			
    			echo '{ "url": "'.$CategoryPicture_URL.'" }';
        	}

        }
        
        
        
        


	});

	$app->post('/api/categories/:id/crop/',$checkToken(),$checkScopes(array('maintainance.view')),function ($id) use ($app) {

		 if(isset($_FILES["file"])){

            $account_configuration = AccountModel::getAccountConfiguration();                
            $upload_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path;
             
             
        	if(move_uploaded_file($_FILES["file"]["tmp_name"],$upload_path.$id.$_FILES['file']["name"])){

  				$CategoryPicture = $upload_path.$id.$_FILES['file']["name"];
        		$CategoryModel = new CategoryModel();
    			$CategoryModel->updateCategoryPicture($id,$CategoryPicture);

    			echo '{ "url": "'.$CategoryPicture.'" }';
        	}

        }


	});


?>