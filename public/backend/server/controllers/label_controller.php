<?php
	
	//require 'server/models/folio_model.php';
	require 'server/models/label_model.php';

	$app->get('/api/labels/:LabelId',$checkToken(), function ($LabelId) use ($app) {
	//$app->get('/api/labels/:LabelId', function ($LabelId) use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        
        

		$data = [];

        $LabelModel = new LabelModel();
        $data = $LabelModel->getLabel($LabelId);
        
        $tracking_process =  $request->get("tracking_process");
        $tracking_mode    =  $request->get("tracking_mode");
        $include_child_labels    =  $request->get("include_child_labels");
        
        
        if(isset($tracking_process) && isset($data["LabelId"])){
            
            //Guardar un modelo para sesiones de captura
            
            //Obtener el almacen al que se esta dando entrada a la etiqueta
            $warehouse_id =  $request->get("warehouse_id");
            //$user_id = 2;//getOwnerId();
            $user_id = getOwnerId();
            //
            $LabelTrackingStatus = $LabelModel->getLabelTrackingStatus($warehouse_id,$data["LabelId"],$tracking_process,$user_id,$tracking_mode);
            
            $data["LabelTrackingStatus"] =  $LabelTrackingStatus  ;
            
            
        }
        
        //----------------------------------------------------------------------
        //* Preguntar si el tipo de etiqueta es de tipo contenedor
        //* de ser asi regresaremos las etiquetas que le corresponden
        //
        //----------------------------------------------------------------------
        if(isset($data["BundleTypeId"]) && $data["BundleTypeId"] > 0 && isset($include_child_labels) && $include_child_labels=="true")
        {
            $data["labels"] = $LabelModel->getBundleLabels($data["LabelId"]);
            
        }
        
        
        
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
       
    
	});
	
	$app->get('/api/labels/:LabelId/open', function ($LabelId) use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        
        

		$data = [];

        $LabelModel = new LabelModel();
        $data = $LabelModel->getLabel($LabelId);
        
        $tracking_process =  $request->get("tracking_process");
        $tracking_mode    =  $request->get("tracking_mode");
        $include_child_labels    =  $request->get("include_child_labels");
        /*
        
        if(isset($tracking_process) && isset($data["LabelId"])){
            
            //Guardar un modelo para sesiones de captura
            
            //Obtener el almacen al que se esta dando entrada a la etiqueta
            $warehouse_id =  $request->get("warehouse_id");
            //$user_id = 2;//getOwnerId();
            $user_id = getOwnerId();
            //
            $LabelTrackingStatus = $LabelModel->getLabelTrackingStatus($warehouse_id,$data["LabelId"],$tracking_process,$user_id,$tracking_mode);
            
            $data["LabelTrackingStatus"] =  $LabelTrackingStatus  ;
            
            
        }
        */
        //----------------------------------------------------------------------
        //* Preguntar si el tipo de etiqueta es de tipo contenedor
        //* de ser asi regresaremos las etiquetas que le corresponden
        //
        //----------------------------------------------------------------------
        if(isset($data["BundleTypeId"]) && $data["BundleTypeId"])
        {
            $data["labels"] = $LabelModel->getBundleLabels($data["LabelId"]);
            
        }
        
        
        
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
       
    
	});
	
	$app->post('/api/labels/:LabelId/reset_tracking',$checkToken(), function ($LabelId) use ($app) {
	//$app->get('/api/labels/:LabelId', function ($LabelId) use ($app) {


        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();
        
        $body = $request->getBody();
        $LabelReceived = json_decode($body);
        
        $warehouse_id          =  $request->get("warehouse_id");
        $label_tracking_process =  $request->get("label_tracking_process");
        
        if(!isset($warehouse_id)) { 
           $warehouse_id          =  $LabelReceived->warehouse_id;
        }
        
        if(!isset($label_tracking_process)) { 
            $label_tracking_process =  $LabelReceived->label_tracking_process;
        }
        

		$data = [];

        $LabelModel = new LabelModel();
        $data = $LabelModel->resetLabelTrackingStatus($warehouse_id,$LabelId,$label_tracking_process);
        
        

        
        
        
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
       
    
	});
	
	$app->post('/api/labels/',$checkToken(), function () use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $body = $request->getBody();
        $LabelReceived = json_decode($body);


		$data = [];

        $LabelModel = new LabelModel();


        $data = $LabelModel->createLabel($LabelReceived);
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    	

	});
	
	$app->post('/api/labels/create_bundle_label',$checkToken(), function () use ($app) {
	//$app->post('/api/labels/create_bundle_label', function () use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $body = $request->getBody();
        $LabelsReceived = json_decode($body);

        
		$data = [];
        
        
        $LabelModel = new LabelModel();
        
        $bundle_label = new stdClass();
        
        $bundle_label->BundleTypeId = 1;

        $data = $LabelModel->createBundleLabel($bundle_label,$LabelsReceived);
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    	

	});

    $app->get('/api/labels/search/prototypes/:search',$checkToken(), function ($search) use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $LabelModel = new LabelModel();


        $data = $LabelModel->getLabelPrototypes($search);
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        

    });

    $app->get('/api/labels/search/headers/:search',$checkToken(), function ($search) use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $LabelModel = new LabelModel();


        $data = $LabelModel->getLabelHeaders($search);
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        

    });
    
    $app->get('/api/labels/search/headers/batch/:label_batch',$checkToken(), function ($label_batch) use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $LabelModel = new LabelModel();


        $data = $LabelModel->getLabelHeader($label_batch);
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        

    });

    $app->get('/api/labels/',$checkToken(), function () use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $LabelModel = new LabelModel();

        $label_batch = $request->get("label_batch");
        $label_number_from = $request->get("label_number_from");
        $label_number_to = $request->get("label_number_to");
        $label_date_from = $request->get("label_date_from");
        $label_date_to = $request->get("label_date_to");
        $include_printed_disabled_labels = $request->get("include_printed_disabled_labels");
        $include_only_prototypes = $request->get("include_only_prototypes");

        $data = $LabelModel->getLabels($label_batch,$label_number_from,$label_number_to,$label_date_from,$label_date_to,$include_printed_disabled_labels,$include_only_prototypes);
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        

    });
    
    $app->get('/api/tracking/labels',$checkToken(), function () use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $LabelModel = new LabelModel();

        $label_batch = $request->get("label_batch");
        $label_number_from = $request->get("label_number_from");
        $label_number_to = $request->get("label_number_to");
        $label_date_from = $request->get("label_date_from");
        $label_date_to = $request->get("label_date_to");
        $include_printed_disabled_labels = $request->get("include_printed_disabled_labels");
        $include_only_prototypes = $request->get("include_only_prototypes");
        
        
        $data = $LabelModel->getLabelTracking($warehouse_id,$data["LabelId"],$tracking_process,$user_id,$tracking_mode);
        
        

        //$data = $LabelModel->getLabels($label_batch,$label_number_from,$label_number_to,$label_date_from,$label_date_to,$include_printed_disabled_labels,$include_only_prototypes);
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        

    });
    
    $app->get('/api/tracking_detailed/labels',$checkToken(), function () use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $LabelModel = new LabelModel();

        $label_batch = $request->get("label_batch");
        $label_id = $request->get("label_id");
        //$label_number_from = $request->get("label_number_from");
        //$label_number_to = $request->get("label_number_to");
        $label_date_from = $request->get("label_date_from");
        $label_date_to = $request->get("label_date_to");
        //$include_printed_disabled_labels = $request->get("include_printed_disabled_labels");
        //$include_only_prototypes = $request->get("include_only_prototypes");
        
        
        $data = $LabelModel->getLabelTrackingDetailed($warehouse_id,$label_batch,$label_id,$label_date_from,$label_date_to);
        
        

        //$data = $LabelModel->getLabels($label_batch,$label_number_from,$label_number_to,$label_date_from,$label_date_to,$include_printed_disabled_labels,$include_only_prototypes);
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        

    });
    
     $app->get('/api/tracking_anomalies/labels',$checkToken(), function () use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $data = [];

        $LabelModel = new LabelModel();

        $label_batch = $request->get("label_batch");
        $label_id = $request->get("label_id");
        //$label_number_from = $request->get("label_number_from");
        //$label_number_to = $request->get("label_number_to");
        $label_date_from = $request->get("label_date_from");
        $label_date_to = $request->get("label_date_to");
        //$include_printed_disabled_labels = $request->get("include_printed_disabled_labels");
        //$include_only_prototypes = $request->get("include_only_prototypes");
        
        
        $data = $LabelModel->getLabelTrackingAnomalies($warehouse_id,$label_batch,$label_id,$label_date_from,$label_date_to);
        
        

        //$data = $LabelModel->getLabels($label_batch,$label_number_from,$label_number_to,$label_date_from,$label_date_to,$include_printed_disabled_labels,$include_only_prototypes);
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        

    });
    
	$app->post('/api/labels/bulk/',$checkToken(), function () use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $body = $request->getBody();
        $LabelReceived = json_decode($body);

        $label_qty   = $request->get('qty');
        $label_start_to_print = $request->get('start_to_print');
        $label_customer_po= $request->get('customer_po');
        $label_product_id = $request->get('product_id');
        $LabelCustomerItem = $request->get("customer_item");
        $LabelBoxSize = $request->get("box_size");
        $LabelBoxWeight = $request->get("box_weight");
        $LabelPartNumber = $request->get("part_number");
        $LabelJobNo = $request->get("job_no");
        $LabelRackId = $request->get("rack_id");
        $LabelPerCtn = $request->get("per_ctn");
        $LabelIsPrototype = $request->get("is_prototype");
        $label_batch = $request->get("label_batch");
        $LabelReference = $request->get("label_reference");
        $LabelHeader = $request->get("label_header");
        $LabelCustomerItemTitle = $request->get("customer_item_title");


        $data = [];
        $LabelModel = new LabelModel();

        if(!isset($label_start_to_print)){
            $label_start_to_print = 1;
        }

        if(isset($label_qty) && $label_qty > 0){

                $FolioModel = new FolioModel();

                if(empty($label_batch)){

                    $batch_folio = $FolioModel->getLastFolioByCode('BATCH');            
                    $label_batch =  $batch_folio; //str_pad($batch_folio, 8, "0", STR_PAD_LEFT);

                }

                $first_print = 1 ; 
                
        		for ($i=$label_qty; $i >= $label_start_to_print; $i--) { 

        			$label = new stdClass();
                    $label->LabelBatch = $label_batch;
        			$label->LabelCustomerPO = $label_customer_po;
        			$label->CustomerPO = $label_customer_po;
        			$label->LabelNumber = $i;
        			$label->LabelTotal  = $label_qty;
        			$label->ProductId = $label_product_id;

                    $label->LabelCustomerItem=$LabelCustomerItem;
                    $label->LabelBoxSize=$LabelBoxSize;
                    $label->LabelBoxWeight=$LabelBoxWeight;
                    $label->LabelPartNumber=$LabelPartNumber;
                    $label->LabelJobNo=$LabelJobNo;
                    $label->LabelRackId=$LabelRackId;
                    $label->LabelPerCtn = $LabelPerCtn;
                    $label->LabelIsPrototype = $LabelIsPrototype;
                    $label->LabelHeader = $LabelHeader;
                    $label->LabelReference = $LabelReference;
                    $label->LabelCustomerItemTitle = $LabelCustomerItemTitle;

        			$label = $LabelModel->createLabel($label);
        			
        			
        			if($first_print == 1){
        			
        			    $ProductModel = new ProductModel();
        			    
        			    
    					$current_product = $ProductModel->getProduct($label->ProductId);
    					
    					
    					if(isset($label->LabelBatch) ){
        				
        				    $batch_label = $LabelModel->getLabelHeader($label->LabelBatch);
        				    
        				    if(isset($batch_label["LabelTotal"])){
        				    
        				        $current_label_total = $batch_label["LabelTotal"];
        				        
        				    }
        				    
        				}
        				
    				
    					
    					$first_print = 0;
        			}
        			
        			
    			    if(isset($current_product)){
    			
					    $label->product = $current_product;
					    $label->ItemId = $label->ProductId;
					    $label->ItemDescription = $current_product["ProductDescription"];
					
				    }
				    
				    
				    
				    if(isset($current_label_total )){

				        $label->LabelTotal  = $current_label_total;
				    }
				            			        		        				
				
        			array_push($data, $label);
        			
        			if($LabelIsPrototype==1)
        			{
                        $label->LabelNumber = 0;
        			    break;
        			}
        			
        			if($LabelHeader == 1){
        			    break;
        			}
        		}
        }



        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
    	

	});
	
    $app->delete('/api/labels/:id',$checkToken(), function ($id) use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $body = $request->getBody();
        $LabelReceived = json_decode($body);


        $data = [];

        $LabelModel = new LabelModel();


        $data = $LabelModel->deleteLabel($LabelReceived);
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        

    });
    
    $app->post('/api/labels/:id/enable_disable_printing',$checkToken(), function ($id) use ($app) {



        $app = Slim\Slim::getInstance();
        $request = $app->request();
        $response = $app->response();


        $disable_by_batch = $request->get('disable_by_batch');

        $body = $request->getBody();
        $LabelReceived = json_decode($body);


        $data = [];

        $LabelModel = new LabelModel();


        $data = $LabelModel->enableDisableLabel($LabelReceived);
        
        $json = json_encode($data);
        $response['Content-Type'] = 'application/json';
        $response->status(200);
        $response->body($json);
        

    });
	

?>