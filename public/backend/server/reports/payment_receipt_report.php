<?php




class PaymemtReceiptReport {

    public $app_configuration=NULL;
    public $account_customization = NULL;

	public function __construct(){

		$host_name = $_SERVER["HTTP_HOST"];
		$AccountModel = new AccountModel(); 
		$this->account_customization = $AccountModel->getAccountCustomization(true);
		$this->app_configuration = $AccountModel->getAccountConfigurationsByHostName($host_name);


	}

	public function getHTML($payment_detail,$payment){

		$PaymentModel = new PaymentModel();

		$payment_details = $PaymentModel->getPaymentDetails($payment["PaymentId"]);
        
         $logo = $this->account_customization->logo_absolute_path;
		
        //var_dump($payment_detail[0]);
		$saldo = $payment["PaymentTotal"];

		$html_payment_detail = '
        	  	<tr>
        	  		<td colspan="3" style="text-align:right;font-size:10px;">Total:</td>        	  		
        	  		<td style="width:100px;text-align:right;font-size:10px;">$ '.number_format($saldo, 2, '.', '').'</td>
				</tr>
			';

		foreach ($payment_details as $single_payment_detail) {
            if($single_payment_detail["PaymentDetailId"] <= $payment_detail["PaymentDetailId"] ){
				$saldo = $saldo -  $single_payment_detail["PaymentDetailAmount"];

	        	$html_payment_detail = $html_payment_detail.'
	        	  	<tr>
	        	  		<td style="font-size:10px;" >'.$single_payment_detail["PaymentDetailReceiptNumber"].'</td>
	        	  		<td style="width:80px;text-align:right;font-size:10px;">'.date('d/m/Y',strtotime($single_payment_detail["PaymentDetailDate"])).'</td>
	        	  		<td style="width:100px;text-align:right;font-size:10px;">$ '.number_format($single_payment_detail["PaymentDetailAmount"], 2, '.', '') .'</td>
	        	  		<td style="width:100px;text-align:right;font-size:10px;">$ '.number_format($saldo, 2, '.', '').'</td>
					</tr>
				';
			}
        }

        $html_payment_details_table = '<table style="width:350px;">'.$html_payment_detail.'</table>';

    	$ValorLetras = num2letras($payment_detail["PaymentDetailAmount"], false, true,$payment["CurrencyName"]) ; 


		$html_head = '

			<head>
				<style type="text/css">

					  
					  .font-maroon {
						color: #BF0000;
						font-family : Arial;
					  }
					  
					   .small{
						font-size: 10px;
					  }
					  
					  .lines {
						
						
						background-color: #fff; 
						height: 20px;
						width:100%
						
						
					  }
					  
					  .lines-maroon {
						display:table-cell;

						border-bottom:solid 1px #BF0000;
						
					  }
    			</style>
  			</head>
  		';
  
        //$logo = 'http://'.$_SERVER["HTTP_HOST"].'/'.$this->app_configuration->CompanyLogo;
        
 		$html_receipt ='
		    <table style="width:100%" >      
		        <tr>
		          	<td>
		            	<table style="width:100%">
			                <tr>
			                  	<td style="width:30%">
			                    	<img src="'.$logo.'" style="width:180px; height:160px">
			                  	</td>
			                  	<td style="width:45%; text-align:center">
			                    	<table class="font-maroon">
			                      
				                        <tr>
				                          	<td  style="text-align:center">
				                            <p>'.$this->app_configuration->CompanyAddress.'</p>
				                          	</td>
				                        </tr>
				                        <tr>
											<td style="text-align:center">
												<h4>RECIBO DE PAGO</h4>
											</td>
				                        </tr>                      		
			                    	</table>
			                  	</td>
			                  	<td style="width:25%;text-align:right;vertical-align:top;">
									<table style="width:100%">
										<tr>
											<td style="text-align:center">
												<strong>'.$payment_detail["PaymentDetailReceiptNumber"].'</strong>
											</td>
										</tr>
										<tr>
											<td>
												<table style="border-spacing:0;border-collapse:collapse;width:100%">
													<tr>
														<td colspan="3" style="border: solid 1px #BF0000; text-align:center">FECHA</td>

													</tr>
												    <tr>
														<td style="border-left: solid 1px #BF0000;border-right:solid 1px #BF0000;  text-align:center">'.date('d',strtotime($payment_detail["PaymentDetailDate"])).'</td>
														<td style="border-left: solid 1px #BF0000;border-right:solid 1px #BF0000;  text-align:center">'.date('m',strtotime($payment_detail["PaymentDetailDate"])).'</td>
														<td style="border-left: solid 1px #BF0000;border-right:solid 1px #BF0000;  text-align:center">'.date('y',strtotime($payment_detail["PaymentDetailDate"])).'</td>
													</tr>
													 <tr>
														<td style="border-bottom: solid 1px #BF0000; border-left:solid 1px #BF0000;border-right:solid 1px #BF0000; text-align:center">DIA</td>
														<td style="border-bottom: solid 1px #BF0000; border-left:solid 1px #BF0000;border-right:solid 1px #BF0000;text-align:center">MES</td>
														<td style="border-bottom: solid 1px #BF0000; border-left:solid 1px #BF0000;border-right:solid 1px #BF0000;text-align:center">AÑO</td>
													</tr>

												</table>
											</td>
										</tr>
									</table>
							  	</td>
			                </tr>            
		            	</table>
		          	</td>
		        </tr>
				<tr>
		         	<td style="">
			          	<table style="width:700px">
			          		<tr>
								<td style="background-color: #B0B0B0; height: 5px"></td>
							</tr>
							<tr>
								<td style="background-color: #666666; height: 10px"></td>
							</tr>
						</table>
				  	</td>
		        </tr>
		        <tr>
					<td>
						<table class="lines"> 
							<tr>
								<td style="width:100px"> RECIBI DE:</td>
								<td style="width:600px" class="lines-maroon">'.$payment["suppliet"]["SupplierName"].'</td> 
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table class="lines"> 
							<tr>
								<td style="width:150px"> LA CANTIDAD DE:</td>
								<td style="width:150px" class="lines-maroon" >&nbsp;$ '.$payment_detail["PaymentDetailAmount"].'</td>
								<td style="width:400px" class="lines-maroon" >'.$ValorLetras.'</td> 
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table class="lines"> 
							<tr>
								<td style="width:180px"> POR CONCEPTO DE:</td>
								<td style="width:520px" class="lines-maroon">'.$payment_detail["PaymentDetailText"].'</td> 
							</tr>
						</table>							
					</td>
		        </tr>
				<tr>
					<td>
						<table class="lines"> 
							<tr>						
								<td style="width:700px" class="lines-maroon">&nbsp;</td> 
							</tr>
						</table>							
					</td>
		        </tr>
				
		        <tr>
		          <td>
						<table style="width:100%; ">
							<tr>
								<td style="width:350px; text-align:center">
									<table style="border: solid 2px #BF0000;width:600px">
										<tr>
											<td style="text-align:center"> Historial de pagos</td>
										</tr>
										<tr>
											<td>
												'.$html_payment_details_table.'
											</td>
										</tr>
									</table>
									
								</td>
								<td style="width:350px; text-align:center;vertical-align:middle">
								______________________________
								<br>
								FIRMA DE RECIBIDO
								 
								</td>
							</tr>
						</table>
				  </td>
				  
		        </tr>
				<tr>
					<td style="padding:10px 0 10px 0; text-align:center">
						"ESTE RECIBO ES VALIDO UNICAMENTE PORTANDO SELLO ORIGINAL"
					</td>
				</tr>
		    </table>
 

		';

    $html= $html_head.'
    	<body>
    		<table class="font-maroon">
    			<tr>
    				<td style="vertical-align:top">'.$html_receipt.'</td>
    			</tr>
    			<tr>
    				<td style="text-align:right">ORIGINAL: PROVEEDOR</td>
    			</tr>
    			<tr>
    				<td style="border-bottom: dotted 1px #BF0000"></td>
    			</tr>
    			<tr>
    				<td style="vertical-align:top">'.$html_receipt.'</td>
    			</tr>
    			<tr>
    				<td style="text-align:right">COPIA: EMPRESA</td>
    			</tr>
    		</table>
    	</body>
    ';
		//$logo = 'http://'.$_SERVER["SERVER_NAME"].'/LogoReportesMerofa.png';
		$header = '<div align="center" style="height:135px;background-color: #f0f2ff;background: transparent url(\''.$logo.'\') no-repeat scroll left top;border-radius: 4mm;font-size:18pt;font-weight:bold;font-style:italic;vertical-align:middle;">'.$title.'</div>';

		
 

		    return $html;

	}

	public function printReport($payment_detail,$payment,$filename=''){

       

		 $html = $this->getHTML($payment_detail,$payment);
		
		
 

			$mpdf=new mPDF('c','A4','','',8,15,10,0,20,17);
			$mpdf->displayDefaultOrientation = true;
			$mpdf->defaultfooterfontsize = 10;	/* in pts */
			$mpdf->defaultfooterfontstyle = B;	/* blank, B, I, or BI */
			$mpdf->defaultfooterline = 0; 	/* 1 to include line below header/above footer */



			

           

			$mpdf->WriteHTML($html);
		

			

			if($filename == ''){
				$mpdf->Output();
				exit;
			} else {
				$mpdf->Output($filename,'F');
			}
			//exit;

			//return $html;


	}

	public function sendReport($quote){



		$mail             = new PHPMailer(); // defaults to using php "mail()"

		//$body             = file_get_contents('contents.html');
		//$body             = eregi_replace("[\]",'',$body);
		//$body ="&nbsp;This is a test of PHPMailer.<br>";

        //echo $body;
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host          = "smtpout.secureserver.net";
		$mail->SMTPAuth      = true;                  // enable SMTP authentication
		$mail->SMTPKeepAlive = false;                  // SMTP connection will not close after each email sent
		$mail->Host          = "smtpout.secureserver.net"; // sets the SMTP server
		$mail->Port          = 25;                    // set the SMTP port for the GMAIL server
		$mail->Username      = "ventas@merofa.com"; // SMTP account username
		$mail->Password      = "G3nerico01";        // SMTP account password
		//$mail->SMTPDebug  = 2; 

		

        $mail->AddReplyTo("ventas@merofa.com","Ventas Merofa");
		$mail->SetFrom('ventas@merofa.com', 'Ventas Merofa');
		

		$address = $quote["customer"]["CustomerEmail"];
		$name = $quote["customer"]["CustomerName"];

		$mail->AddAddress($address, $name);
		$mail->AddReplyTo("ventas@merofa.com","Ventas Merofa");

		$mail->Subject = "Cotizacion: ".$quote["PaymentFolio"]." , ".$quote["PaymentTitle"]."";

		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

		

		$filename = $quote["PaymentFolio"]."test.pdf";

		$body = $this->getHtml($quote);

		$mail->MsgHTML($body);
		


		if(!$mail->Send()) {

		  
		  throw new Exception($mail->ErrorInfo);

		}

      


	}

}




?>