<?php




class QuoteReport {

    public $app_configuration=NULL;
    public $account_customization = NULL;

	public function __construct(){

		$host_name = $_SERVER["HTTP_HOST"];
		
		$AccountModel = new AccountModel();
		
		
		
        $this->account_customization = $AccountModel->getAccountCustomization(true);
        
        $this->app_configuration = $AccountModel->getAccountConfigurationsByHostName($host_name,true);
        
	}
	

	public function getHTML($quote){


		$html = "";

         //$logo = 'http://'.$_SERVER["SERVER_NAME"].'/'.$this->app_configuration->CompanyLogo;
         //$logo = '../public/'.$this->app_configuration->CompanyLogo;
         
        $logo = $this->account_customization->logo_absolute_path;
         
         
		 $title = '
			<table width="100%">
				<tr>
					<td width="33%"><img src="'.$logo.'"></td>
					<td width="33%" align="center" style="font-size:24px;font-weight:bold;font-family:Arial;color:#666666;">Cotización</td>
					<td width="33%"></td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="33%" align="center"></td>
					<td width="33%" >
						<span style="font-size:18px;">
						#<span style="font-weight:bold">'.$quote["QuoteFolio"].'</span></span> <br>
						'.$quote["QuoteDate"].'
					</td>
				</tr>
			</table>
		';

		

		$header = '<div align="center" style="height:135px;background-color: #f0f2ff;background: transparent no-repeat scroll left top;border-radius: 4mm;font-size:18pt;font-weight:bold;font-style:italic;vertical-align:middle;">'.$title.'</div>';

		$html=$header.'
				<table width="100%">
					<tr>
						<td width="33%">
							<p>'.$this->app_configuration->CompanyAddress.'</p>
						</td>
						<td width="18%"></td>
						<td width="48%">
							<p>
								<span style="font-weight: bold">Atn:'.$quote["customer"]["CustomerName"].'</span><br>
								'.$quote["customer"]["CustomerAddress"].'<br>
				  				'.$quote["customer"]["CustomerCity"].', '.$quote["customer"]["CustomerCountry"].'<br>
			       				Tel:'.$quote["customer"]["CustomerTelephone"].'<br>
			       				Cel:'.$quote["customer"]["CustomerCellphone"].'<br>
			       				Entregar en:'.$quote["customer"]["CustomerDeliveryPlace"].'<br>
							</p>
						</td>
					</tr>
					<tr>
						<td width="80%" colspan="2">Titulo: '.$quote["QuoteTitle"].'</td>
						
						<td width="20%" style="text-align:right"><br>Tipo de cambio: $'.$quote["QuoteExchangeRate"].'</td>
					</tr>
				</table>
				
			';


			
			$data_table_header = '<br>
				<table width="100%" style="border-top: 1mm solid #666666;">
					<tr style="background-color:#B0B0B0; color:#ffffff;">
						<td width="40%" >Descripcion</td>
						<td width="8%">UM</td>
						<td width="6%">Cant.</td>
						<td width="10%">Precio</td>
						<td width="12%" style="text-align:center">Desc.</td>
						<td width="12%" style="text-align:center">IVA</td>
						<td width="15%" >Total</td>
					</tr>';


			$data_table_data='		<tr >';

			foreach ($quote["items"] as $item) {

				$Description = "";


				$DiscountAmountText = "";
                if( $item["QuoteDetailDiscount"] > 0 ){

					$DiscountAmount =  ( $item["QuoteDetailDiscount"] / 100) * $item["QuoteDetailTotal"];
					$DiscountAmount =  number_format((float) $DiscountAmount, 2, '.', '');
					$DiscountAmountText = ''.$item["QuoteDetailDiscount"].'% <span style="color: #444444" >($'.$DiscountAmount.')</span>';
				}

				$TaxAmount="";
				if($item["QuoteDetailTax"] > 0){

					$TaxAmount = ( $item["QuoteDetailTax"] / 100) * $item["QuoteDetailTotal"];
					$TaxAmount =  number_format((float) $TaxAmount, 2, '.', '');
					$TaxAmount = ''.$item["QuoteDetailTax"].'% <span style="color: #444444" >($'.$TaxAmount.')</span>';


				}

				if(isset($item["Product"]["ProductName"])){

					
					$Description = $item["Product"]["ProductName"];
				}else
				{
					$Description = $item["QuoteDetailDescription"];
				}

				$data_table_data .='
					<tr >
						<td style="font-size:12px" >'.$Description.'</td>
						<td style="text-align:center">'.$item["QuoteDetailUM"].'</td>
						<td style="text-align:center">'.$item["QuoteDetailQTY"].'</td>
						<td style="text-align:right">'.$item["QuoteDetailPrice"].'</td>
						<td style="text-align:center; font-size:12px">'.$DiscountAmountText.'</td>
						<td style="text-align:center; font-size:12px">'.$TaxAmount.'</td>
						<td style="text-align:right">'.$item["QuoteDetailTotal"].'</td>
					</tr>
				';
				
			}

			if( $quote["CurrencyId"] == 1 ){
				$totales = '
				<tr>
									<td width="60%">Total MXN</td>
									<td width="40%" style="text-align:right;border-top: 1px solid #B0B0B0">$ '.$quote["QuoteTotal"].'</td>
								</tr>
								<tr>
									<td width="60%">Total Dlls</td>
									<td width="40%" style="text-align:right">$ '.$quote["QuoteTotalDlls"].'</td>
								</tr>';
			}
			else
			{
				$totales = '
				<tr>
									<td width="60%">Total DLLS</td>
									<td width="40%" style="text-align:right;border-top: 1px solid #B0B0B0">$ '.$quote["QuoteTotal"].'</td>
								</tr>
								<tr>
									<td width="60%">Total MXN</td>
									<td width="40%" style="text-align:right">$ '.$quote["QuoteTotalDlls"].'</td>
								</tr>';

			}



			$ValorLetras = num2letras($quote["QuoteTotal"], false, true,$quote["CurrencyName"]) ; 
			
			$data_table_footer = '
				<tr><td colspan="7" style="border-top: 1px solid #B0B0B0"><br><br></td>
				</tr>
				<tr>
						<td width="60%" colspan="4" style="vertical-align: top" >Descripci&oacute;n, comentarios o notas:<br><br>

						'.$quote["QuoteTerms"].'
						</td>
						<td width="40%" colspan="3"  style="text-align:right">
							<table width="100%">
								<tr>
									<td width="60%">Subtotal</td>
									<td width="40%" style="text-align:right">$ '.$quote["QuoteSubtotal"].'</td>
								</tr>
								<tr>
									<td width="60%">IVA</td>
									<td width="40%" style="text-align:right">$ '.$quote["QuoteTaxes"].'</td>
								</tr>
								<tr>
									<td width="60%">Descuentos</td>
									<td width="40%" style="color:#0000FF;text-align:right">-$ '.$quote["QuoteDiscounts"].'</td>
								</tr>
								'.$totales.'
							</table>
						</td>
						
					</tr>
					<tr><td colspan="7" style="text-align: center; font-size:13px"><br><br>'.$ValorLetras.'</td>
				</tr>
			';


			


		    $data_table = $data_table_header.$data_table_data.$data_table_footer.'</table>';

		    $html = $html.$data_table;

		    return $html;

	}

	public function printReport($quote,$filename=''){

       

		$html = $this->getHTML($quote);
		
		




			$mpdf=new mPDF('c','A4','','',8,15,10,57,20,17);
			$mpdf->displayDefaultOrientation = true;
			$mpdf->defaultfooterfontsize = 12;	/* in pts */
			$mpdf->defaultfooterfontstyle = B;	/* blank, B, I, or BI */
			$mpdf->defaultfooterline = 0; 	/* 1 to include line below header/above footer */


			//$mpdf->SetFooter('{PAGENO}');


			$mpdf->SetFooter(array(
					'L' => array(
						'content' => '{PAGENO}',
						'font-family' => 'monotipe',
						'font-style' => 'BI',	/* blank, B, I, or BI */
						'font-size' => '8',	/* in pts */
					),
					'C' => array(
						'content' => $this->app_configuration->CompanyWebUrl,
						'font-family' => 'serif',
						'font-style' => 'BI',
						'font-size' => '16',	/* gives default */
					),
					'R' => array(
						'content' => $quote["UserName"].' @ {DATE j-m-Y H:m}',
						'font-family' => 'arial',
						'font-style' => '',
						'font-size' => '8',
					),
					'line' => 1,		/* 1 to include line below header/above footer */
				), 'O'	/* defines footer for Even Pages */
			);



            

			$mpdf->WriteHTML($html);
		

			

			if($filename == ''){
				$mpdf->Output();
				exit;
			} else {
				$mpdf->Output($filename,'F');
			}
			//exit;

			//return $html;


	}

	public function sendReport($quote,$QuoteCC='',$QuoteMailMessage=''){

        /*
        
                $this->app_configuration->CompanySmtpHost = $account_configurations_data["company_smtp_host"];
                $this->app_configuration->CompanySmtpPort = $account_configurations_data["company_smtp_port"];
                $this->app_configuration->CompanySmtpUserName = $account_configurations_data["company_smtp_user_name"];
                $this->app_configuration->CompanySmtpPassword = $account_configurations_data["company_smtp_password"];
                $this->app_configuration->CompanyAddReplyTo = $account_configurations_data["company_add_reply_to"];
                $this->app_configuration->CompanyAddReplyToName = $account_configurations_data["company_add_reply_to_name"];
        
        */


		$mail             = new PHPMailer(); // defaults to using php "mail()"

		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host          = $this->app_configuration->CompanySmtpHost;
		$mail->SMTPAuth      = true;                  // enable SMTP authentication
		$mail->SMTPKeepAlive = false;                  // SMTP connection will not close after each email sent
		$mail->Host          = $this->app_configuration->CompanySmtpHost;
		$mail->Port          = $this->app_configuration->CompanySmtpPort ;
		$mail->Username      = $this->app_configuration->CompanySmtpUserName;
		$mail->Password      = $this->app_configuration->CompanySmtpPassword;
		$mail->SMTPSecure = "ssl";
		//$mail->SMTPDebug  = 2; 

		$mail->SetFrom( $this->app_configuration->CompanySmtpUserName  ,$this->app_configuration->CompanyAddReplyToName);
		
		

        $mail->AddReplyTo($this->app_configuration->CompanyAddReplyTo,$this->app_configuration->CompanyAddReplyToName);
		
		

		$address = $quote["customer"]["CustomerEmail"];
		$name = $quote["customer"]["CustomerName"];

		$mail->AddAddress($address, $name);
		$mail->AddReplyTo($this->app_configuration->CompanyAddReplyTo, $this->app_configuration->CompanyAddReplyToName);

         
        if(is_array($QuoteCC)){
        	
        	foreach ($QuoteCC as $CCAddress) {
        		$mail->AddCC($CCAddress, '');
        	}
        }
		
	    

		$mail->Subject = "Cotizacion: ".$quote["QuoteFolio"]." , ".$quote["QuoteTitle"]."";

		

		

		$filename = $quote["QuoteFolio"]."test.pdf";

		$body = $this->getHtml($quote);


        if(isset($QuoteMailMessage)){
        	
        	$body = '<br><br><strong>Mensaje : </strong>'.$QuoteMailMessage .'<br><br><div style="display:block; border:dashed 1px #ddd"></div><br><br>' .$body;
        }



		$mail->MsgHTML($body);
		


		if(!$mail->Send()) {

		  
		  throw new Exception($mail->ErrorInfo);

		}

      


	}

}




?>