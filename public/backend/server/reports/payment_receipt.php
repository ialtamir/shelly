<?php




class ReceiptReport {
	
	public $app_configuration=NULL;
    public $account_customization = NULL;

	public function __construct(){

		$host_name = $_SERVER["HTTP_HOST"];
		$AccountModel = new AccountModel(); 
		$this->account_customization = $AccountModel->getAccountCustomization(true);
		$this->app_configuration = $AccountModel->getAccountConfigurationsByHostName($host_name);


	}
	
	

	public function getHTML($payment,$sale_order){


   $logo = $this->account_customization->logo_absolute_path;
		$SaleOrderModel = new SaleOrderModel();

		$payments = $SaleOrderModel->getSaleOrderPayments($sale_order["SaleOrderId"]);

		
      
		$saldo = $sale_order["SaleOrderTotal"];

		$html_payment = '
        	  	<tr>
        	  		<td colspan="3" style="text-align:right;font-size:10px;">Total:</td>        	  		
        	  		<td style="width:100px;text-align:right;font-size:10px;">$ '.number_format($saldo, 2, '.', '').'</td>
				</tr>
			';

		foreach ($payments as $single_payment) {
            if($single_payment["SaleOrderPaymentId"] <= $payment["SaleOrderPaymentId"] ){
				$saldo = $saldo -  $single_payment["SaleOrderPaymentAmount"];

	        	$html_payment = $html_payment.'
	        	  	<tr>
	        	  		<td style="font-size:10px;" >'.$single_payment["SaleOrderPaymentReceiptNumber"].'</td>
	        	  		<td style="width:80px;text-align:right;font-size:10px;">'.date('d/m/Y',strtotime($single_payment["SaleOrderPaymentDate"])).'</td>
	        	  		<td style="width:100px;text-align:right;font-size:10px;">$ '.number_format($single_payment["SaleOrderPaymentAmount"], 2, '.', '') .'</td>
	        	  		<td style="width:100px;text-align:right;font-size:10px;">$ '.number_format($saldo, 2, '.', '').'</td>
					</tr>
				';
			}
        }

        $html_payments_table = '<table style="width:350px;">'.$html_payment.'</table>';

    	$ValorLetras = num2letras($payment["SaleOrderPaymentAmount"], false, true,$sale_order["CurrencyName"]) ; 


		$html_head = '

			<head>
				<style type="text/css">

					  
					  .font-maroon {
						color: #BF0000;
						font-family : Arial;
					  }
					  
					   .small{
						font-size: 10px;
					  }
					  
					  .lines {
						
						
						background-color: #fff; 
						height: 20px;
						width:100%
						
						
					  }
					  
					  .lines-maroon {
						display:table-cell;

						border-bottom:solid 1px #BF0000;
						
					  }
    			</style>
  			</head>
  		';
  
  
 		$html_receipt ='
		    <table style="width:100%" >      
		        <tr>
		          	<td>
		            	<table style="width:100%">
			                <tr>
			                  	<td style="width:30%">
			                    	<img src="'.$logo.'" style="width:180px; height:160px">
			                  	</td>
			                  	<td style="width:45%; text-align:center">
			                    	<table class="font-maroon">
			                      
				                        <tr>
				                          	<td  style="text-align:center">
				                            	<p >Alba Roja No. 75, Fracc. Hipodromo II
				                              	<br>Tijuana, B.C. Tel. 660-9449
				                              	<br>e-mail: merofa_designs@hotmail.com</p>
				                          	</td>
				                        </tr>
				                        <tr>
											<td style="text-align:center">
												<h4>RECIBO DE DINERO</h4>
											</td>
				                        </tr>                      		
			                    	</table>
			                  	</td>
			                  	<td style="width:25%;text-align:right;vertical-align:top;">
									<table style="width:100%">
										<tr>
											<td style="text-align:center">
												<strong>'.$payment["SaleOrderPaymentReceiptNumber"].'</strong>
											</td>
										</tr>
										<tr>
											<td>
												<table style="border-spacing:0;border-collapse:collapse;width:100%">
													<tr>
														<td colspan="3" style="border: solid 1px #BF0000; text-align:center">FECHA</td>

													</tr>
												    <tr>
														<td style="border-left: solid 1px #BF0000;border-right:solid 1px #BF0000;  text-align:center">'.date('d',strtotime($payment["SaleOrderPaymentDate"])).'</td>
														<td style="border-left: solid 1px #BF0000;border-right:solid 1px #BF0000;  text-align:center">'.date('m',strtotime($payment["SaleOrderPaymentDate"])).'</td>
														<td style="border-left: solid 1px #BF0000;border-right:solid 1px #BF0000;  text-align:center">'.date('y',strtotime($payment["SaleOrderPaymentDate"])).'</td>
													</tr>
													 <tr>
														<td style="border-bottom: solid 1px #BF0000; border-left:solid 1px #BF0000;border-right:solid 1px #BF0000; text-align:center">DIA</td>
														<td style="border-bottom: solid 1px #BF0000; border-left:solid 1px #BF0000;border-right:solid 1px #BF0000;text-align:center">MES</td>
														<td style="border-bottom: solid 1px #BF0000; border-left:solid 1px #BF0000;border-right:solid 1px #BF0000;text-align:center">AÑO</td>
													</tr>

												</table>
											</td>
										</tr>
									</table>
							  	</td>
			                </tr>            
		            	</table>
		          	</td>
		        </tr>
				<tr>
		         	<td style="">
			          	<table style="width:700px">
			          		<tr>
								<td style="background-color: #FFCC00; height: 5px"></td>
							</tr>
							<tr>
								<td style="background-color: #993300; height: 10px"></td>
							</tr>
						</table>
				  	</td>
		        </tr>
		        <tr>
					<td>
						<table class="lines"> 
							<tr>
								<td style="width:100px"> RECIBI DE:</td>
								<td style="width:600px" class="lines-maroon">'.$sale_order["customer"]["CustomerName"].'</td> 
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table class="lines"> 
							<tr>
								<td style="width:150px"> LA CANTIDAD DE:</td>
								<td style="width:150px" class="lines-maroon" >&nbsp;$ '.$payment["SaleOrderPaymentAmount"].'</td>
								<td style="width:400px" class="lines-maroon" >'.$ValorLetras.'</td> 
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table class="lines"> 
							<tr>
								<td style="width:180px"> POR CONCEPTO DE:</td>
								<td style="width:520px" class="lines-maroon">'.$payment["SaleOrderPaymentText"].'</td> 
							</tr>
						</table>							
					</td>
		        </tr>
				<tr>
					<td>
						<table class="lines"> 
							<tr>						
								<td style="width:700px" class="lines-maroon">&nbsp;</td> 
							</tr>
						</table>							
					</td>
		        </tr>
				
		        <tr>
		          <td>
						<table style="width:100%; ">
							<tr>
								<td style="width:350px; text-align:center">
									<table style="border: solid 2px #BF0000;width:600px">
										<tr>
											<td style="text-align:center"> Historial de pagos</td>
										</tr>
										<tr>
											<td>
												'.$html_payments_table.'
											</td>
										</tr>
									</table>
									
								</td>
								<td style="width:350px; text-align:center;vertical-align:middle">
								______________________________
								<br>
								FIRMA DE RECIBIDO
								 
								</td>
							</tr>
						</table>
				  </td>
				  
		        </tr>
				<tr>
					<td style="padding:10px 0 10px 0; text-align:center">
						"ESTE RECIBO ES VALIDO UNICAMENTE PORTANDO SELLO ORIGINAL"
					</td>
				</tr>
		    </table>
 

		';

    $html= $html_head.'
    	<body>
    		<table class="font-maroon">
    			<tr>
    				<td style="vertical-align:top">'.$html_receipt.'</td>
    			</tr>
    			<tr>
    				<td style="text-align:right">ORIGINAL: CLIENTE</td>
    			</tr>
    			<tr>
    				<td style="border-bottom: dotted 1px #BF0000"></td>
    			</tr>
    			<tr>
    				<td style="vertical-align:top">'.$html_receipt.'</td>
    			</tr>
    			<tr>
    				<td style="text-align:right">COPIA: EMPRESA</td>
    			</tr>
    		</table>
    	</body>
    ';
		$logo = 'http://'.$_SERVER["SERVER_NAME"].'/LogoReportesMerofa.png';
		$header = '<div align="center" style="height:135px;background-color: #f0f2ff;background: transparent url(\''.$logo.'\') no-repeat scroll left top;border-radius: 4mm;font-size:18pt;font-weight:bold;font-style:italic;vertical-align:middle;">'.$title.'</div>';

		
 

		    return $html;

	}

	public function printReport($payment,$sale_order,$filename=''){

       

		$html = $this->getHTML($payment,$sale_order);
		
		




			$mpdf=new mPDF('c','A4','','',8,15,10,0,20,17);
			$mpdf->displayDefaultOrientation = true;
			$mpdf->defaultfooterfontsize = 10;	/* in pts */
			$mpdf->defaultfooterfontstyle = B;	/* blank, B, I, or BI */
			$mpdf->defaultfooterline = 0; 	/* 1 to include line below header/above footer */

           

			$mpdf->WriteHTML($html);
		

			

			if($filename == ''){
				$mpdf->Output();
				exit;
			} else {
				$mpdf->Output($filename,'F');
			}
			//exit;

			//return $html;


	}

	public function sendReport($quote){



		$mail             = new PHPMailer(); // defaults to using php "mail()"

		//$body             = file_get_contents('contents.html');
		//$body             = eregi_replace("[\]",'',$body);
		//$body ="&nbsp;This is a test of PHPMailer.<br>";

        //echo $body;
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host          = "smtpout.secureserver.net";
		$mail->SMTPAuth      = true;                  // enable SMTP authentication
		$mail->SMTPKeepAlive = false;                  // SMTP connection will not close after each email sent
		$mail->Host          = "smtpout.secureserver.net"; // sets the SMTP server
		$mail->Port          = 25;                    // set the SMTP port for the GMAIL server
		$mail->Username      = "ventas@merofa.com"; // SMTP account username
		$mail->Password      = "G3nerico01";        // SMTP account password
		//$mail->SMTPDebug  = 2; 

		

        $mail->AddReplyTo("ventas@merofa.com","Ventas Merofa");
		$mail->SetFrom('ventas@merofa.com', 'Ventas Merofa');
		

		$address = $quote["customer"]["CustomerEmail"];
		$name = $quote["customer"]["CustomerName"];

		$mail->AddAddress($address, $name);
		$mail->AddReplyTo("ventas@merofa.com","Ventas Merofa");

		$mail->Subject = "Cotizacion: ".$quote["SaleOrderFolio"]." , ".$quote["SaleOrderTitle"]."";

		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

		

		$filename = $quote["SaleOrderFolio"]."test.pdf";

		$body = $this->getHtml($quote);

		$mail->MsgHTML($body);
		


		if(!$mail->Send()) {

		  
		  throw new Exception($mail->ErrorInfo);

		}

      


	}

}




?>