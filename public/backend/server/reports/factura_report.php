<?php
require_once "contract_1.php";
require_once "contract_2.php";
require_once "contract_3.php";


class FacturaReport {

    public $app_configuration=NULL;

	public function __construct(){

		$host_name = $_SERVER->HTTP_HOST;
		$AccountModel = new AccountModel(); 
		$this->app_configuration = $AccountModel->getAccountConfigurationsByHostName($host_name,true);


	}

	public function getHTML($sale_order){

		
		$AccountModel = new AccountModel(); 
		    
		$account = $AccountModel->getAccount();
		
		    
		$logo = '../public/'.$this->app_configuration->CompanyLogo;
		$qr = '../accounts_data/facturas/qr/'.$account->rfc.'-'.$sale_order->SaleOrderId.'.png';
		$html = "";
		
		$customer = (object)$sale_order->customer; 
		
		 $title = '
			<table width="100%">
			<tr>
					<td width="33%"><img src="'.$logo.'"></td>
					<td width="33%" align="center" style="font-size:24px;font-weight:bold;font-family:Arial;color:#666666;">Factura</td>
					<td width="33%"></td>
			</tr>
			<tr>
				<td width="33%">
					<p>
						<span style="font-weight: bold">'.$customer->CustomerName.'</span><br>
						'.$customer->CustomerAddress.'<br>
						'.$customer->CustomerCity.', '.$customer->CustomerCountry.'<br>
						'.$customer->CustomerRFC.' '.$customer->CustomerCP.'<br>
			    		'.$customer->CustomerTelephone.'
					</p>
				</td>
				<td width="33%"></td>
				<td width="33%">
					<p style="font-size:10px;">
						FECHA EXPEDICIÓN : '.$sale_order->InvoiceDateExpedition.' <br>
						CONDICIONES DE PAGO : '.$sale_order->InvoiceConditionsPayment.' <br>
						FORMA DE PAGO : '.$sale_order->InvoiceMethodPayment.' <br>
						FACTURA : '.$sale_order->InvoiceFolio.'<br>
						LUGAR EXPEDICIÓN : '.$sale_order->InvoicePlaceExpeditions.'<br>
						TIPO DE CAMBIO :  '.$sale_order->InvoiceTypeExchange.' 
					</p>
				</td>
			</tr>
			</table>
		';

		
		$header = '<div align="center" style="height:135px;background-color: #f0f2ff;background: transparent scroll left top;border-radius: 4mm;font-size:18pt;font-weight:bold;font-style:italic;vertical-align:middle;">'.$title.'</div>';

		$html=$header; 


			$data_table_header = '<br>
				<table width="100%" style="border-top: 1mm solid #666666;">
					<tr style="background-color:#B0B0B0; color:#ffffff;">
						<td width="40%" >Descripcion</td>
						<td width="8%">UM</td>
						<td width="6%">Cant.</td>
						<td width="10%">Precio</td>
						<td width="12%" style="text-align:center">Desc. %</td>
						<td width="12%" style="text-align:center">IVA %</td>
						<td width="15%" >Total</td>
					</tr>';


			$data_table_data='		<tr >';
			$items = (object) $sale_order->items ;
			foreach ($items as $item) {

				$Description = "";



				$DiscountAmountText = "";
                if( $item["SaleOrderDetailDiscount"] > 0 ){

					$DiscountAmount =  ( $item["SaleOrderDetailDiscount"] / 100) * $item["SaleOrderDetailTotal"];
					$DiscountAmount =  number_format((float) $DiscountAmount, 2, '.', '');
					$DiscountAmountText = ''.$item["SaleOrderDetailDiscount"].'% <span style="color: #444444" >($'.$DiscountAmount.')</span>';
				}

				$TaxAmount="";
				if($item["SaleOrderDetailTax"] > 0){

					$TaxAmount = ( $item["SaleOrderDetailTax"] / 100) * $item["SaleOrderDetailTotal"];
					$TaxAmount =  number_format((float) $TaxAmount, 2, '.', '');
					$TaxAmount = ''.$item["SaleOrderDetailTax"].'% <span style="color: #444444" >($'.$TaxAmount.')</span>';


				}


				if(isset($item["Product"]["ProductName"])){

					
					$Description = $item["Product"]["ProductName"];
				}else
				{
					$Description = $item["SaleOrderDetailDescription"];
				}

				$data_table_data .='
					<tr >
						<td style="font-size:12px" >'.$Description.'</td>
						<td style="text-align:center">'.$item["SaleOrderDetailUM"].'</td>
						<td style="text-align:center">'.$item["SaleOrderDetailQTY"].'</td>
						<td style="text-align:right">'.$item["SaleOrderDetailPrice"].'</td>
						<td style="text-align:center; font-size:12px">'.$DiscountAmountText .'</td>
						<td style="text-align:center; font-size:12px">'.$TaxAmount.'</td>
						<td style="text-align:right">'.$item["SaleOrderDetailTotal"].'</td>
					</tr>
				';
				
			}

			if( $sale_order->CurrencyId == 1 ){
				$totales = '
				<tr>
									<td width="60%">Total MXN</td>
									<td width="40%" style="text-align:right;border-top: 1px solid #666666">$ '.$sale_order->SaleOrderTotal.'</td>
								</tr>
								<tr>
									<td width="60%">Total Dlls</td>
									<td width="40%" style="text-align:right">$ '.$sale_order->SaleOrderTotalDlls.'</td>
								</tr>';
			}
			else
			{
				$totales = '
				<tr>
									<td width="60%">Total DLLS</td>
									<td width="40%" style="text-align:right;border-top: 1px solid #666666">$ '.$sale_order->SaleOrderTotal.'</td>
								</tr>
								<tr>
									<td width="60%">Total MXN</td>
									<td width="40%" style="text-align:right">$ '.$sale_order->SaleOrderTotalDlls.'</td>
								</tr>';

			}



			$ValorLetras = num2letras($sale_order->SaleOrderTotal, false, true,$sale_order->CurrencyName) ; 
			
			$data_table_footer = '
				<tr><td colspan="7" style="border-top: 1px solid #666666"><br><br></td></tr>
				
				<tr><td colspan="7" style="text-align: center; font-size:13px">'.$ValorLetras.'</td></tr>
				<tr>
						<td width="60%" colspan="4" style="vertical-align: top" >
						<p style="font-size:10px;">
						Regimen Fiscal Emisor: '.$sale_order->InvoiceRegimenFiscal.'<br>
						Forma de pago Hacienda: NO IDENTIFICADO <br>
						Cuenta de pago: '.$sale_order->InvoiceAccountPayment.' <br>
						Numero de Aprobación: '.$sale_order->InvoiceNumberApproval.' <br>
						Año de Aprobación: '.$sale_order->InvoiceYearApproval.' Numero de Certificado: '.$sale_order->InvoiceNumberCertificate.' <br>
						</p>
						</td>
						
						<td width="40%" colspan="3"  style="text-align:right">
							<table width="100%">
								<tr>
									<td width="60%">Subtotal</td>
									<td width="40%" style="text-align:right">$ '.$sale_order->SaleOrderSubtotal.'</td>
								</tr>
								<tr>
									<td width="60%">IVA</td>
									<td width="40%" style="text-align:right">$ '.$sale_order->SaleOrderTaxes.'</td>
								</tr>
								<tr>
									<td width="60%">Descuentos</td>
									<td width="40%" style="color:#0000FF;text-align:right">-$ '.$sale_order->SaleOrderDiscounts.'</td>
								</tr>
								'.$totales.'
							</table>
						</td>
						
				</tr>
				
				<tr>
						<td width="60%" colspan="4" style="vertical-align: top" >
						<p style="font-size:10px;">
						Sello Digital  : '.$sale_order->InvoiceSeal.'<br><br>
						Cadena Original :  '.$sale_order->InvoiceChain.' <br><br>
						Numero de Certificado SAT:  '.$sale_order->InvoiceCertificate_SAT.'<br>
						Folio Fiscal:  '.$sale_order->InvoiceFolioFiscal.' <br>
						Fecha y hora de Certifiación:  '.$sale_order->InvoiceDateCertificate.' <br>
						Sello Digital SAT:  '.$sale_order->InvoiceSealSAT.' <br>
						</p>
						</td>
						<td width="40%" colspan="3"  style="text-align:right">
						<img src="'.$qr.'">
						</td>
				</tr>
			';

		    $data_table = $data_table_header.$data_table_data.$data_table_footer.'</table>';

		    $html = $html.$data_table;

		    return $html;

	}
	

	public function getHTMLInvoice($sale_order){
		
		$logo = '../public/'.$this->app_configuration->CompanyLogo;
		$pdf = 'http://merofa-ialtamirano.c9.io/server/uploads/images/descargarpdf.png';
		$xml = 'http://merofa-ialtamirano.c9.io/server/uploads/images/descargarxml.png';
		$titulo = 'http://merofa-ialtamirano.c9.io/server/uploads/images/titulomail.png';

		$html = '<div style="text-align:center; background-color:#B8E4F1;">
		  <img src="'.$titulo.'">
		  </div>
		  	<div style="text-align:center; background-color:white; width:590px; margin: 0px auto 15px auto">
		    <a href=\'https://merofa-ialtamirano.c9.io/api/facturar/report/'.$this->filename.'\' style="margin-left:0px;"><img src="'.$pdf.'"></a>
		    <a href=\'https://merofa-ialtamirano.c9.io/api/facturar/xml/'.$this->filename.'\' style="margin-left:55px;"><img src="'.$xml.'"></a>
		    </div>';

		    return $html;
		
		}
		

	public function printReport($sale_order,$filename=''){


		$html = $this->getHTML($sale_order);
		
		




			$mpdf=new mPDF('c','A4','','',8,15,10,20,20,17);
			$mpdf->displayDefaultOrientation = true;
			$mpdf->defaultfooterfontsize = 12;	/* in pts */
			$mpdf->defaultfooterfontstyle = B;	/* blank, B, I, or BI */
			$mpdf->defaultfooterline = 0; 	/* 1 to include line below header/above footer */


			//$mpdf->SetFooter('{PAGENO}');



            

			$mpdf->WriteHTML($html);
			
			if($filename == ''){
				$mpdf->Output();
				exit;
			} else {
				$mpdf->Output($filename,'F');
			}
			//exit;

			//return $html;


	}

	public function sendReport($sale_order){

		//var_dump($sale_order);
		$order = (object)$sale_order;
		
	    $mail             = new PHPMailer(); // defaults to using php "mail()"

		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host          = $this->app_configuration->CompanySmtpHost;
		$mail->SMTPAuth      = true;                  // enable SMTP authentication
		$mail->SMTPKeepAlive = false;                  // SMTP connection will not close after each email sent
		$mail->Port          = $this->app_configuration->CompanySmtpPort ;
		$mail->Username      = $this->app_configuration->CompanySmtpUserName;
		$mail->Password      = $this->app_configuration->CompanySmtpPassword;
		$mail->SMTPSecure = "ssl";


		

        
		$mail->SetFrom($this->app_configuration->CompanySmtpUserName, 'Cuatroo Servicio de Facturación');
		
		$customer = (object)$order->customer; 

		$address = $customer->CustomerEmail;
		$name = $customer->CustomerName;

		$mail->AddAddress($address, $name);
		$mail->AddReplyTo($this->app_configuration->CompanyAddReplyTo,$this->app_configuration->CompanyAddReplyToName);

		$mail->Subject = "Factura: ".$order->SaleOrderFolio." , ".$order->SaleOrderTitle."";

		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

		

		//$filename = $order->SaleOrderFolio."test.pdf";

		$body = $this->getHTMLInvoice($order);

		$mail->MsgHTML($body);
		


		if(!$mail->Send()) {

		  
		  throw new Exception($mail->ErrorInfo);

		}

      


	}

}




?>