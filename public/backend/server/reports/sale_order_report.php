<?php
require_once "contract_1.php";
require_once "contract_2.php";
require_once "contract_3.php";


class SaleOrderReport {

    public $app_configuration=NULL;
    public $account_customization = NULL;

	public function __construct(){

		$host_name = $_SERVER["HTTP_HOST"];
		
		$AccountModel = new AccountModel(); 
        
        $this->account_customization = $AccountModel->getAccountCustomization(true);
        
		$this->app_configuration = $AccountModel->getAccountConfigurationsByHostName($host_name,true);


	}

	public function getHTML($sale_order){


        
		
		$logo = $this->account_customization->logo_absolute_path;
		
		$html = "";

		 $title = '
			<table width="100%">
				<tr>
					<td width="33%"><img src="'.$logo.'"></td>
					<td width="33%" align="center" style="font-size:24px;font-weight:normal;font-family:Arial;color:#666666;">Orden de Venta</td>
					<td width="33%"></td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="33%" align="center"></td>
					<td width="33%" >
						<span style="font-size:18px;">
						#<span style="font-weight:bold">'.$sale_order["SaleOrderFolio"].'</span></span> <br>
						'.$sale_order["SaleOrderDate"].'
					</td>
				</tr>
			</table>
		';

		
		$header = '<div align="center" style="height:135px;background-color: #f0f2ff;background: transparent scroll left top;border-radius: 4mm;font-size:18pt;font-weight:bold;font-style:italic;vertical-align:middle;">'.$title.'</div>';

		$html=$header.'
				<table width="100%">
					<tr>
						<td width="33%">
						    <p>'.$this->app_configuration->CompanyAddress.'</p>
						</td>
						<td width="18%"></td>
						<td width="48%">
							<p>
								<span style="font-weight: bold">Atn:'.$sale_order["customer"]["CustomerName"].'</span><br>
								'.$sale_order["customer"]["CustomerAddress"].'<br>
				  				'.$sale_order["customer"]["CustomerCity"].', '.$sale_order["customer"]["CustomerCountry"].'<br>
			       				'.$sale_order["customer"]["CustomerTelephone"].'
							</p>
						</td>
					</tr>
					<tr>
						<td width="80%" colspan="2">Titulo: '.$sale_order["SaleOrderTitle"].'</td>
						
						<td width="20%" style="text-align:right"><br>Tipo de cambio: $'.$sale_order["SaleOrderExchangeRate"].'</td>
					</tr>
				</table>
				
			';


			
			$data_table_header = '<br>
				<table width="100%" style="border-top: 1mm solid #666666;">
					<tr style="background-color:#B0B0B0; color:#ffffff;">
						<td width="40%" >Descripcion</td>
						<td width="8%">UM</td>
						<td width="6%">Cant.</td>
						<td width="10%">Precio</td>
						<td width="12%" style="text-align:center">Desc. %</td>
						<td width="12%" style="text-align:center">IVA %</td>
						<td width="15%" >Total</td>
					</tr>';


			$data_table_data='		<tr >';

			foreach ($sale_order["items"] as $item) {

				$Description = "";



				$DiscountAmountText = "";
                if( $item["SaleOrderDetailDiscount"] > 0 ){

					$DiscountAmount =  ( $item["SaleOrderDetailDiscount"] / 100) * $item["SaleOrderDetailTotal"];
					$DiscountAmount =  number_format((float) $DiscountAmount, 2, '.', '');
					$DiscountAmountText = ''.$item["SaleOrderDetailDiscount"].'% <span style="color: #444444" >($'.$DiscountAmount.')</span>';
				}

				$TaxAmount="";
				if($item["SaleOrderDetailTax"] > 0){

					$TaxAmount = ( $item["SaleOrderDetailTax"] / 100) * $item["SaleOrderDetailTotal"];
					$TaxAmount =  number_format((float) $TaxAmount, 2, '.', '');
					$TaxAmount = ''.$item["SaleOrderDetailTax"].'% <span style="color: #444444" >($'.$TaxAmount.')</span>';


				}


				if(isset($item["Product"]["ProductName"])){

					
					$Description = $item["Product"]["ProductName"];
				}else
				{
					$Description = $item["SaleOrderDetailDescription"];
				}

				$data_table_data .='
					<tr >
						<td style="font-size:12px" >'.$Description.'</td>
						<td style="text-align:center">'.$item["SaleOrderDetailUM"].'</td>
						<td style="text-align:center">'.$item["SaleOrderDetailQTY"].'</td>
						<td style="text-align:right">'.$item["SaleOrderDetailPrice"].'</td>
						<td style="text-align:center; font-size:12px">'.$DiscountAmountText .'</td>
						<td style="text-align:center; font-size:12px">'.$TaxAmount.'</td>
						<td style="text-align:right">'.$item["SaleOrderDetailTotal"].'</td>
					</tr>
				';
				
			}

			if( $sale_order["CurrencyId"] == 1 ){
				$totales = '
				<tr>
									<td width="60%">Total MXN</td>
									<td width="40%" style="text-align:right;border-top: 1px solid #666666">$ '.$sale_order["SaleOrderTotal"].'</td>
								</tr>
								<tr>
									<td width="60%">Total Dlls</td>
									<td width="40%" style="text-align:right">$ '.$sale_order["SaleOrderTotalDlls"].'</td>
								</tr>';
			}
			else
			{
				$totales = '
				<tr>
									<td width="60%">Total DLLS</td>
									<td width="40%" style="text-align:right;border-top: 1px solid #666666">$ '.$sale_order["SaleOrderTotal"].'</td>
								</tr>
								<tr>
									<td width="60%">Total MXN</td>
									<td width="40%" style="text-align:right">$ '.$sale_order["SaleOrderTotalDlls"].'</td>
								</tr>';

			}



			$ValorLetras = num2letras($sale_order["SaleOrderTotal"], false, true,$sale_order["CurrencyName"]) ; 
			
			$data_table_footer = '
				<tr><td colspan="7" style="border-top: 1px solid #666666"><br><br></td>
				</tr>
				<tr>
						<td width="60%" colspan="4" style="vertical-align: top" >Descripci&oacute;n, comentarios o notas:<br><br>

						'.$sale_order["SaleOrderDescription"].'
						</td>
						<td width="40%" colspan="3"  style="text-align:right">
							<table width="100%">
								<tr>
									<td width="60%">Subtotal</td>
									<td width="40%" style="text-align:right">$ '.$sale_order["SaleOrderSubtotal"].'</td>
								</tr>
								<tr>
									<td width="60%">IVA</td>
									<td width="40%" style="text-align:right">$ '.$sale_order["SaleOrderTaxes"].'</td>
								</tr>
								<tr>
									<td width="60%">Descuentos</td>
									<td width="40%" style="color:#0000FF;text-align:right">-$ '.$sale_order["SaleOrderDiscounts"].'</td>
								</tr>
								'.$totales.'
							</table>
						</td>
						
					</tr>
					<tr><td colspan="7" style="text-align: center; font-size:13px"><br><br>'.$ValorLetras.'</td>
				</tr>
			';


			


		    $data_table = $data_table_header.$data_table_data.$data_table_footer.'</table>';

		    $html = $html.$data_table;

		    return $html;

	}

	public function printReport($sale_order,$filename=''){

       

		$html = $this->getHTML($sale_order);
		
		




			$mpdf=new mPDF('c','A4','','',8,15,10,20,20,17);
			$mpdf->displayDefaultOrientation = true;
			$mpdf->defaultfooterfontsize = 12;	/* in pts */
			$mpdf->defaultfooterfontstyle = B;	/* blank, B, I, or BI */
			$mpdf->defaultfooterline = 0; 	/* 1 to include line below header/above footer */


			//$mpdf->SetFooter('{PAGENO}');



            

			$mpdf->WriteHTML($html);

			if($sale_order["ContractId"] == 1)
			{
				$mpdf->AddPage();

				$contract_1 = Contrato1();
          		$contract_1 = str_replace("{{CustomerName}}", strtoupper($sale_order["customer"]["CustomerName"]), $contract_1);
                $contract_1 = str_replace("{{CustomerAddress}}", $sale_order["customer"]["CustomerAddress"], $contract_1);
                $contract_1 = str_replace("{{CustomerCP}}", $sale_order["customer"]["CustomerCP"], $contract_1);
                $contract_1 = str_replace("{{CustomerCity}}", $sale_order["customer"]["CustomerCity"], $contract_1);
                $contract_1 = str_replace("{{CustomerTelephone}}", $sale_order["customer"]["CustomerTelephone"], $contract_1);
                $contract_1 = str_replace("{{CustomerEmail}}", $sale_order["customer"]["CustomerEmail"], $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal}}", $sale_order["SaleOrderTotal"], $contract_1);
                $contract_1 = str_replace("{{SaleOrderDeliveryDate}}", date('d/m/Y',strtotime($sale_order["SaleOrderDeliveryDate"])) , $contract_1);

                $SaleOrderTotalText = num2letras($sale_order["SaleOrderTotal"], false, true,$sale_order["CurrencyName"]) ; 

                $contract_1 = str_replace("{{SaleOrderTotalText}}", strtoupper($SaleOrderTotalText), $contract_1);

                $SaleOrderTotal50 = ($sale_order["SaleOrderTotal"] * 0.5);
                $SaleOrderTotal50Text=num2letras($SaleOrderTotal50, false, true,$sale_order["CurrencyName"]) ; 


                $contract_1 = str_replace("{{SaleOrderTotal50}}", $SaleOrderTotal50, $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal50Text}}", strtoupper($SaleOrderTotal50Text), $contract_1);

                $SaleOrderTotal35 =  ($sale_order["SaleOrderTotal"] * 0.35);
                $SaleOrderTotal35Text=num2letras($SaleOrderTotal35, false, true,$sale_order["CurrencyName"]) ; 

                $contract_1 = str_replace("{{SaleOrderTotal35}}", $SaleOrderTotal35, $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal35Text}}", strtoupper($SaleOrderTotal35Text), $contract_1);

                $SaleOrderTotal15=($sale_order["SaleOrderTotal"] * 0.15);
                $SaleOrderTotal15Text = num2letras($SaleOrderTotal15, false, true,$sale_order["CurrencyName"]) ; 


                $contract_1 = str_replace("{{SaleOrderTotal15}}", $SaleOrderTotal15, $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal15Text}}", strtoupper($SaleOrderTotal15), $contract_1);

                $contract_1 = str_replace("{{CurrencyName}}", $sale_order["CurrencyName"], $contract_1);

                $contract_1 = str_replace("{{day}}", date('d'), $contract_1);
                $contract_1 = str_replace("{{month}}", date('m'), $contract_1);
                $contract_1 = str_replace("{{year}}",  date('y'), $contract_1);


				$mpdf->WriteHTML($contract_1);
			}

			if($sale_order["ContractId"] == 2)
			{
				$mpdf->AddPage();

				$contract_1 = Contrato2();
				$contract_1 = str_replace("{{CustomerName}}", strtoupper($sale_order["customer"]["CustomerName"]), $contract_1);
                $contract_1 = str_replace("{{CustomerAddress}}", $sale_order["customer"]["CustomerAddress"], $contract_1);
                $contract_1 = str_replace("{{CustomerCP}}", $sale_order["customer"]["CustomerCP"], $contract_1);
                $contract_1 = str_replace("{{CustomerCity}}", $sale_order["customer"]["CustomerCity"], $contract_1);
                $contract_1 = str_replace("{{CustomerTelephone}}", $sale_order["customer"]["CustomerTelephone"], $contract_1);
                $contract_1 = str_replace("{{CustomerEmail}}", $sale_order["customer"]["CustomerEmail"], $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal}}", $sale_order["SaleOrderTotal"], $contract_1);
                $contract_1 = str_replace("{{SaleOrderDeliveryDate}}", date('d/m/Y',strtotime($sale_order["SaleOrderDeliveryDate"])) , $contract_1);

                $SaleOrderTotalText = num2letras($sale_order["SaleOrderTotal"], false, true,$sale_order["CurrencyName"]) ; 

                $contract_1 = str_replace("{{SaleOrderTotalText}}", $SaleOrderTotalText, $contract_1);

                $SaleOrderTotal50 = ($sale_order["SaleOrderTotal"] * 0.5);
                $SaleOrderTotal50Text=num2letras($SaleOrderTotal50, false, true,$sale_order["CurrencyName"]) ; 


                $contract_1 = str_replace("{{SaleOrderTotal50}}", $SaleOrderTotal50, $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal50Text}}", $SaleOrderTotal50Text, $contract_1);

                $SaleOrderTotal35 =  ($sale_order["SaleOrderTotal"] * 0.35);
                $SaleOrderTotal35Text=num2letras($SaleOrderTotal35, false, true,$sale_order["CurrencyName"]) ; 

                $contract_1 = str_replace("{{SaleOrderTotal35}}", $SaleOrderTotal35, $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal35Text}}", $SaleOrderTotal35Text, $contract_1);

                $SaleOrderTotal15=($sale_order["SaleOrderTotal"] * 0.15);
                $SaleOrderTotal15Text = num2letras($SaleOrderTotal15, false, true,$sale_order["CurrencyName"]) ; 


                $contract_1 = str_replace("{{SaleOrderTotal15}}", $SaleOrderTotal15, $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal15Text}}", $SaleOrderTotal15, $contract_1);

                $contract_1 = str_replace("{{CurrencyName}}", $sale_order["CurrencyName"], $contract_1);

                $contract_1 = str_replace("{{day}}", date('d'), $contract_1);
                $contract_1 = str_replace("{{month}}", date('m'), $contract_1);
                $contract_1 = str_replace("{{year}}",  date('y'), $contract_1);
               



				$mpdf->WriteHTML($contract_1);
			}

			if($sale_order["ContractId"] == 3)
			{
				$mpdf->AddPage();

				$contract_1 = Contrato3();
				$contract_1 = str_replace("{{CustomerName}}", strtoupper($sale_order["customer"]["CustomerName"]), $contract_1);
                $contract_1 = str_replace("{{CustomerAddress}}", $sale_order["customer"]["CustomerAddress"], $contract_1);
                $contract_1 = str_replace("{{CustomerCP}}", $sale_order["customer"]["CustomerCP"], $contract_1);
                $contract_1 = str_replace("{{CustomerCity}}", $sale_order["customer"]["CustomerCity"], $contract_1);
                $contract_1 = str_replace("{{CustomerTelephone}}", $sale_order["customer"]["CustomerTelephone"], $contract_1);
                $contract_1 = str_replace("{{CustomerEmail}}", $sale_order["customer"]["CustomerEmail"], $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal}}", $sale_order["SaleOrderTotal"], $contract_1);

                $SaleOrderTotalText = num2letras($sale_order["SaleOrderTotal"], false, true,$sale_order["CurrencyName"]) ; 

                $contract_1 = str_replace("{{SaleOrderTotalText}}", $SaleOrderTotalText, $contract_1);

                $SaleOrderTotal50 = ($sale_order["SaleOrderTotal"] * 0.5);
                $SaleOrderTotal50Text=num2letras($SaleOrderTotal50, false, true,$sale_order["CurrencyName"]) ; 


                $contract_1 = str_replace("{{SaleOrderTotal50}}", $SaleOrderTotal50, $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal50Text}}", $SaleOrderTotal50Text, $contract_1);

                $SaleOrderTotal35 =  ($sale_order["SaleOrderTotal"] * 0.35);
                $SaleOrderTotal35Text=num2letras($SaleOrderTotal35, false, true,$sale_order["CurrencyName"]) ; 

                $contract_1 = str_replace("{{SaleOrderTotal35}}", $SaleOrderTotal35, $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal35Text}}", $SaleOrderTotal35Text, $contract_1);

                $SaleOrderTotal15=($sale_order["SaleOrderTotal"] * 0.15);
                $SaleOrderTotal15Text = num2letras($SaleOrderTotal15, false, true,$sale_order["CurrencyName"]) ; 


                $contract_1 = str_replace("{{SaleOrderTotal15}}", $SaleOrderTotal15, $contract_1);
                $contract_1 = str_replace("{{SaleOrderTotal15Text}}", $SaleOrderTotal15, $contract_1);

                $contract_1 = str_replace("{{CurrencyName}}", $sale_order["CurrencyName"], $contract_1);

                $contract_1 = str_replace("{{day}}", date('d'), $contract_1);
                $contract_1 = str_replace("{{month}}", date('m'), $contract_1);
                $contract_1 = str_replace("{{year}}",  date('y'), $contract_1);
               



				$mpdf->WriteHTML($contract_1);
			}
		

			

			if($filename == ''){
				$mpdf->Output();
				exit;
			} else {
				$mpdf->Output($filename,'F');
			}
			//exit;

			//return $html;


	}

	public function sendReport($quote){



	    $mail             = new PHPMailer(); // defaults to using php "mail()"

		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host          = CONST_SMTP_HOST;
		$mail->SMTPAuth      = true;                  // enable SMTP authentication
		$mail->SMTPKeepAlive = false;                  // SMTP connection will not close after each email sent
		$mail->Port          = CONST_SMTP_PORT;
		$mail->Username      = CONST_SMTP_USER;
		$mail->Password      = CONST_SMTP_PASSWORD;



        $mail->AddReplyTo($this->app_configuration->CompanyAddReplyTo,$this->app_configuration->CompanyAddReplyToName);
		

		$address = $quote["customer"]["CustomerEmail"];
		$name = $quote["customer"]["CustomerName"];

		$mail->AddAddress($address, $name);
		$mail->AddReplyTo($this->app_configuration->CompanyAddReplyTo,$this->app_configuration->CompanyAddReplyToName);

		$mail->Subject = "Venta: ".$quote["SaleOrderFolio"]." , ".$quote["SaleOrderTitle"]."";

		$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

		

		$filename = $quote["SaleOrderFolio"]."test.pdf";

		$body = $this->getHtml($quote);

		$mail->MsgHTML($body);
		


		if(!$mail->Send()) {

		  
		  throw new Exception($mail->ErrorInfo);

		}

      


	}

}




?>