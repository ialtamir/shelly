<?php
class SquaresReport {

    public $app_configuration=NULL;
    public $account_customization = NULL;

	public function __construct(){

		$host_name = $_SERVER["HTTP_HOST"];
		$AccountModel = new AccountModel(); 
		
		$this->account_customization = $AccountModel->getAccountCustomization(true);
		$this->app_configuration = $AccountModel->getAccountConfigurationsByHostName($host_name);


	}
	

	private function getHTML($quote){

       
         
        $logo = $this->account_customization->logo_absolute_path;
        

		$server_url = 'http://'.$_SERVER["SERVER_NAME"].'/';
		
		//$logo = 'http://'.$_SERVER["HTTP_HOST"].'/'.$this->app_configuration->CompanyLogo;

		$html ='
		<head>
		
			<title>Cuatroo</title>
			
			</head>
			<style>
			#header{
			text-align: left;
			width: 750px;
			margin: auto;
			}

			#lateral{
			width: 180px; 
			 
			float:left; 
			}

			#derecha{
			margin-left:190px; 


			}

			body{
			font-family: "Open Sans", sans-serif;
			font-size: small;
			}


			#caja {
			background-color: #eaeaea;
			height: 25px;
			margin: 0 auto;
			width: 750px;
			/*para Firefox*/
			-moz-border-radius: 5px 5px 5px 5px;
			/*para Safari y Chrome*/
			-webkit-border-radius: 5px 5px 5px 5px;
			/* para Opera */
			border-radius: 5px 5px 5px 5px;
			/* para IE */
			behavior:url(border.htc);
			}



			</style>
			<body>

			<div id="header">

				<div id="lateral">
					<img src="'.$logo.'" />
				</div>

				<div id="derecha">
					<br>Fecha : '.$quote["QuoteDate"].'
					<br>Nombre: '.$quote["customer"]["CustomerName"].'
					<br>Direccion: '.$quote["customer"]["CustomerAddress"].'
					<br>'.$quote["customer"]["CustomerCity"].', '.$quote["customer"]["CustomerCountry"].'
					<br>Tel: '.$quote["customer"]["CustomerTelephone"].'
			       	<br>Cel: '.$quote["customer"]["CustomerCellphone"].'
				</div>


			</div>


			<div style="width: 750px;text-align:center"> 
			<img src="cuadricula.jpg"  style="width: 750px;" />
			<p  style="text-align:center;"><b></b></p>
			</div>

			<div id="caja">
				<p  style="text-align:center;font-size: x-small; padding-top:5px;color:#E5C98F">'.$this->app_configuration->CompanyAddress.'</p>
			</div>


			</body>
		';

		return $html;
	}

	public function printReport($quote){
		$html = $this->getHTML($quote);
		
		




			$mpdf=new mPDF('c','A4','','',15,15,5,5,20,17);
			$mpdf->displayDefaultOrientation = true;
			$mpdf->defaultfooterfontsize = 12;	/* in pts */
			$mpdf->defaultfooterfontstyle = B;	/* blank, B, I, or BI */
			$mpdf->defaultfooterline = 0; 	/* 1 to include line below header/above footer */


			//$mpdf->SetFooter('{PAGENO}');




            

			$mpdf->WriteHTML($html);
		

			

			if($filename == ''){
				$mpdf->Output();
				exit;
			} else {
				$mpdf->Output($filename,'F');
			}
	}
}
?>