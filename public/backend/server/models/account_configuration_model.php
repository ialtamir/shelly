<?php
class AccountConfigurationModel {
    
	//AccountConfigurations

	private $db;
	private $account_configuration_select_sql = " SELECT `account_config_id`, `account_config_code`, `account_config_value` FROM `account_configurations` ";
    private $account_configuration_insert_sql = "";
	private $account_configuration_update_sql = "";
    
    
    private $statements = array( "SELECT" =>
                            "SELECT `account_id`, `account_config_code`, `account_config_value` FROM `account_configurations` ",
                            
                        );

	public function __construct(){

        $absolute_configuration = new stdClass();
        $absolute_configuration->database_host = ABSOLUTE_DB_HOST;
        $absolute_configuration->database_name = ABSOLUTE_DB_NAME;
        $absolute_configuration->database_user = ABSOLUTE_DB_USER;
        $absolute_configuration->database_password = ABSOLUTE_DB_PASSWORD;
                
    
        
        $this->db = DB::withAccount($absolute_configuration);

	}

	
	/*
		This document returns a key value array;
	*/
	public function getAccountConfigurations($include_server_configurations=false){

        $this->db = DB::withAccount(AccountModel::getAccountConfiguration());
        
		$sql = $this->account_configuration_select_sql;	
		$parameters = array();



		$result = $this->db->query($sql,$parameters);

		$account_configurations_records = $result->fetchAll(PDO::FETCH_OBJ);
        
        $account_configurations_data = new stdClass();

        foreach ($account_configurations_records as $account_configurations_record) {

        	$account_configurations_data[$account_configurations_record->account_config_code] = $account_configurations_record->account_config_value;

        	

        }

		return $account_configurations_data;

	}

	public function getAccountConfigurationsByHostName($host_name = "",$include_server_configurations=false){


		$sql = $this->account_configuration_select_sql;	
		$parameters = array();

		/*if(isset($host_name)){
			$sql=$sql." WHERE `account_config_host`= :AccountConfigurationHost ";
			$parameters[":AccountConfigurationHost"] = $host_name;
		}*/
		
		

		$result = $this->db->query($sql,$parameters);

		$account_configurations_records = $result->fetchAll(PDO::FETCH_OBJ);
        
        $account_configurations_data = [];

        foreach ($account_configurations_records as $account_configurations_record) {

        	$account_configurations_data[$account_configurations_record->account_config_code] = $account_configurations_record->account_config_value;

        	


        }

		return $this->convertAccountConfigurations($account_configurations_data,$include_server_configurations);

	}
    
    
  
    
    public function getAccountConfiguration($accound_id){
        
        $sql = $this->statements["SELECT"]." WHERE account_id = :account_id";
        
        
		$result = $this->db->query($sql,array(":account_id" => $accound_id));

		$configurations_records = $result->fetchAll(PDO::FETCH_OBJ);
        
        $configurations_data = [];

        foreach ($configurations_records as $configurations_record) {

        	$configurations_data[$configurations_record->account_config_code] = $configurations_record->account_config_value;

        }

		return (object) $configurations_data;
        
    }



	private function convertAccountConfigurations($account_configurations_data, $include_server_configurations=false){



          
			$account_configuration_obj = new stdClass();

           
           $account_configuration_obj->CompanyName = $account_configurations_data["company_name"];
           $account_configuration_obj->CompanyFullName = $account_configurations_data["company_full_name"];
           $account_configuration_obj->CompanyLogo = $account_configurations_data["company_logo_path"];
           $account_configuration_obj->CompanyLogoSmall = $account_configurations_data["company_logo_small_path"];
           $account_configuration_obj->CompanyAddress = $account_configurations_data["company_address"];
           $account_configuration_obj->CompanyWebUrl = $account_configurations_data["company_web_url"];
           
           if($include_server_configurations){
           
                $account_configuration_obj->CompanySmtpHost = $account_configurations_data["company_smtp_host"];
                $account_configuration_obj->CompanySmtpPort = intval($account_configurations_data["company_smtp_port"]);
                $account_configuration_obj->CompanySmtpUserName = $account_configurations_data["company_smtp_user_name"];
                $account_configuration_obj->CompanySmtpPassword = $account_configurations_data["company_smtp_password"];
                $account_configuration_obj->CompanyAddReplyTo = $account_configurations_data["company_add_reply_to"];
                $account_configuration_obj->CompanyAddReplyToName = $account_configurations_data["company_add_reply_to_name"];
                
           }
           
           
             


          return $account_configuration_obj;

	}

}


	

?>