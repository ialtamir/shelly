<?php
class QuoteModel {

	private $db;
	
	private $statements = array( "SELECT" => "SELECT * FROM erp_quotes WHERE ( quote_deleted = 0 OR quote_deleted IS NULL ) " , 
								 "DEFAULT_ORDER" =>" ORDER BY `quote_date` DESC , `quote_id` DESC");
	
	
	
	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getQuotesReadyForWorkOrders() {

		$result = $this->db->query(" 
			SELECT 	q.`quote_id`, `quote_folio`,`quote_title`  
			FROM `erp_quotes` q  , `erp_sale_orders` s Where quote_status = 'Aprobada'
			and s.quote_id = q.quote_id
			and s.sale_order_status = 'Aprobada'
			and q.`quote_deleted` = 0
            and s.sale_order_id NOT IN ( SELECT sale_order_id FROM erp_work_orders WHERE sale_order_id = s.sale_order_id)
            ORDER BY q.`quote_date` DESC
			",
			array()
		);

		$quotes = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($quotes as $quote) {

        	array_push($data, $this->getQuote($quote->quote_id));
        }

		return $data;

	}

	public function getQuotes(){

		$sql = $this->statements["SELECT"];
		
		$result = $this->checkRowLevelPermissions($sql,array());

		$quotes = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($quotes as $quote) {

        	array_push($data, $this->convertQuote($quote));
        }

		return $data;
	}
	
	private function checkRowLevelPermissions($sql,$parameters){
		
		$CurrentUserId = getOwnerId();
		
		
		$UserModel = new UserModel();
		$current_user = $UserModel->getUser($CurrentUserId );
		
		
		if(!((bool)$current_user->user_is_admin)){
			
			
			//El usuario solo podra ver las ventas de sucursal
			if( $current_user->place_id > 0){
				
				$sql = $sql." AND place_id =:PlaceId";
				$parameters[":PlaceId"] = $current_user->place_id;
				
			}
			else {
				
				$sql = $sql . " AND created_by = :UserId";
				$parameters[":UserId"] = getOwnerId();
				
			}
			
		}
		
		return $this->db->query($sql.$this->statements["DEFAULT_ORDER"], $parameters);
		
		
		
	}
	


	public function getPendingQuotes(){

		$sql = $this->statements["SELECT"] ." AND `quote_status` = 'Pendiente' ";

		
		$result = $this->checkRowLevelPermissions($sql,array());

		$quotes = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($quotes as $quote) {

        	array_push($data, $this->convertQuote($quote));
        }

		return $data;

	}


    public function getCanceledQuotes(){

    	$sql = $this->statements["SELECT"] ." AND `quote_status` = 'Cancelada'  ";

		$result = $this->checkRowLevelPermissions($sql,array());

		$quotes = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($quotes as $quote) {

        	array_push($data, $this->convertQuote($quote));
        }

		return $data;

	}


	public function getProcessedQuotes(){

		$sql = $this->statements["SELECT"] ." AND `quote_status` = 'Aprobada' ";

		$result = $this->checkRowLevelPermissions($sql,array());

		$quotes = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($quotes as $quote) {

        	array_push($data, $this->convertQuote($quote));
        }

		return $data;


	}



	public function getCompletedQuotes(){

		$sql = $this->statements["SELECT"] ." AND `quote_status` = 'Aprobada' 
				AND `quote_id` IN ( 
					
						SELECT q1.quote_id FROM (  SELECT s.quote_id, s.sale_order_total,
													 sum(p.sale_order_payment_amount) AS sale_order_payments_total,
													(s.sale_order_total - sum(p.sale_order_payment_amount)) AS sale_order_payments_remaining
													FROM erp_sale_orders s 
			LEFT OUTER JOIN erp_sale_order_payments p ON p.sale_order_id = s.sale_order_id 
			LEFT OUTER JOIN erp_customers c ON c.customer_id = s.customer_id
			WHERE s.sale_order_status = 'Aprobada'
			GROUP BY s.sale_order_id
			HAVING sum(p.sale_order_payment_amount) >= s.sale_order_total 
			ORDER BY s.sale_order_date desc
      ) AS q1
                     
                     )
      
		";


		$result = $this->checkRowLevelPermissions($sql,array());

		$quotes = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($quotes as $quote) {

        	array_push($data, $this->convertQuote($quote));
        }

		return $data;


	}

	public function getQuote($QuoteId,$detailed = false, $status=""){
		
		$sql = $this->statements["SELECT"]." AND quote_id = :QuoteId " ; 
		
		$result = $this->checkRowLevelPermissions($sql,array(':QuoteId' => $QuoteId ));
		

		$quote=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertQuote($quote,$detailed);
	}



	public function getQuoteByFolio($QuoteFolio){
		
		$sql = $this->statements["SELECT"]." AND quote_folio = :QuoteFolio" ; 

		$result = $this->db->query($sql,array(':QuoteFolio' => $QuoteFolio ));

		$quote=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertQuote($quote,true);
	}


	public function getQuoteByTransactionId($TransactionId){
		
		$sql = $this->statements["SELECT"]." AND transaction_id = :TransactionId" ; 
		
		$result = $this->checkRowLevelPermissions($sql,array(':TransactionId' => $TransactionId ));


		$quote=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertQuote($quote,true);
	}
	
	

	public function createQuote($quote){

 		try{

 				$this->db->conn->beginTransaction();
 			  
				$sql = " 
					INSERT INTO `erp_quotes`(
						`quote_id`, 
						`quote_folio`, 
						`quote_title`, 
						`quote_date`, 
						`customer_id`, 
						`quote_status`,
						`quote_subtotal`,
						`quote_taxes`, 
						`quote_discounts`,
						`quote_total`,
						`quote_total_dlls`,
						`quote_cdate`,
						`quote_exchange_rate`,
						`quote_terms`,
						`user_id`,
						`currency_id`,
        				`transaction_id`,
        				`created_by`,
        				`place_id`

					) VALUES (

						:QuoteId,
						:QuoteFolio,
						:QuoteTitle,
						:QuoteDate,
						:CustomerId,
						:QuoteStatus,
						:QuoteSubtotal,
						:QuoteTaxes, 
						:QuoteDiscounts,
						:QuoteTotal,
						:QuoteTotalDlls,
						NOW(),
						:QuoteExchangeRate,
						:QuoteTerms,
						:UserId,
						:CurrencyId,
						:TransactionId,
						:UserId,
						:PlaceId
				)";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($quote));
		
				$quote->QuoteId = (int)$this->db->conn->lastInsertId();
				$quote->QuoteFolio = $quote->QuoteId ;

				if(isset($quote->items)){
  					$this->createQuoteDetails($quote);
				}

				$this->db->conn->commit();

				return $quote;

		}catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}
	}

	public function updateQuote($quote){

		try{

 				if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();

				$sql = " 
					UPDATE `erp_quotes` SET 

						`quote_folio` = :QuoteFolio, 
						`quote_title`  = :QuoteTitle,
						`quote_date` = :QuoteDate,
						`customer_id` = :CustomerId,
						`quote_status` = :QuoteStatus,
						`quote_subtotal` = :QuoteSubtotal,
						`quote_taxes` = :QuoteTaxes, 
						`quote_discounts` = :QuoteDiscounts,
						`quote_total` = :QuoteTotal,
						`quote_total_dlls` = :QuoteTotalDlls,						
						`user_id` = :UserId,
						`quote_lmdate` = NOW(),
						`quote_exchange_rate` = :QuoteExchangeRate,
						`quote_terms` = :QuoteTerms,
						`currency_id` = :CurrencyId,
        				`transaction_id`= :TransactionId,
        				`place_id` =:PlaceId

						WHERE `quote_id` = :QuoteId 
					";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($quote));
		
			

				if(isset($quote->deleted_items)){
  					$this->deleteQuoteDetails($quote);
				}



				if(isset($quote->items)){
  					$this->createQuoteDetails($quote);
				}

				

				$this->db->conn->commit();

				return $quote;

		}catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}

	}

	public function deleteQuote($QuoteId){

		//try{

 				$this->db->conn->beginTransaction();

				/*$sql = " 
					DELETE FROM `erp_quote_details` 
					WHERE quote_id= :QuoteId
				" ;
					
				
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array( ':QuoteId' => $QuoteId ));
				*/

				$sql = " 
					UPDATE `erp_quotes` SET `quote_deleted` = 1
					WHERE quote_id=:QuoteId
				" ;

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array( ':QuoteId' => $QuoteId ));




				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array( ':QuoteId' => $QuoteId ));

					

				$this->db->conn->commit();
		/*}
		catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}*/
	}

	public function createQuoteDetails($quote){

		$sql_insert = "
			INSERT INTO `erp_quote_details`(
				`quote_id`, 
				`quote_detail_id`, 
				`quote_detail_description`, 
				`product_id`, 
				`quote_detail_qty`, 
				`quote_detail_um`, 
				`quote_detail_price`,
				`quote_detail_discount`,
				`quote_detail_tax`,
				`quote_detail_total`
			) 
			VALUES (
				:QuoteId,
				:QuoteDetailId,
				:QuoteDetailDescription,
				:ProductId,
				:QuoteDetailQTY,
				:QuoteDetailUM,
				:QuoteDetailPrice,
				:QuoteDetailDiscount,
				:QuoteDetailTax,
				:QuoteDetailTotal
			)";

		$sql_update = "
			UPDATE `erp_quote_details` SET
						
				`quote_detail_description`=:QuoteDetailDescription,
				`product_id` =:ProductId, 
				`quote_detail_qty`=:QuoteDetailQTY, 
				`quote_detail_um`=:QuoteDetailUM, 
				`quote_detail_price`=:QuoteDetailPrice,
				`quote_detail_discount`=:QuoteDetailDiscount,
				`quote_detail_tax` =:QuoteDetailTax,
				`quote_detail_total` = :QuoteDetailTotal
				WHERE `quote_id` = :QuoteId and `quote_detail_id` = :QuoteDetailId";


		foreach ($quote->items as $quote_item){

			$sql = $sql_insert;

			if(isset($quote_item->QuoteDetailId)){ 
				$sql = $sql_update; 
			}else{
				$quote_item->QuoteDetailId = $this->getQuoteDetailIdentity($quote->QuoteId);	
			}


			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute($this->fillDetailParams($quote->QuoteId,$quote_item));

			
			
		}		
	}

	public function deleteQuoteDetails($quote){

		$sql = "
			DELETE FROM `erp_quote_details` WHERE
				`quote_id` = :QuoteId and `quote_detail_id` = :QuoteDetailId
				";



			foreach ($quote->deleted_items as $quote_item){

				

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(':QuoteId' => $quote->QuoteId, ':QuoteDetailId' => $quote_item->QuoteDetailId));
				
				
			}		
	}

	private function getQuoteDetails($quote_id){
		$result = $this->db->query("
			SELECT  
				`quote_id`, 
				`quote_detail_id`, 
				`quote_detail_description`, 
				`product_id`, 
				`quote_detail_qty`, 
				`quote_detail_um`, 
				`quote_detail_price`,
				`quote_detail_discount`,
				`quote_detail_tax`,
				`quote_detail_total`
			FROM `erp_quote_details`
			WHERE `quote_id` = :QuoteId"
			, array( ':QuoteId' => $quote_id));

			$quote_details = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($quote_details as $quote_detail) {

        	array_push($data, $this->convertQuoteDetail($quote_detail));
        }

		return $data;


	}

	public function getQuoteAttachments($quote_id){
		$result = $this->db->query("
			SELECT  
				`quote_id`, 
				`quote_attachment_id`, 
				`quote_attachment_name`, 
				`quote_attachment_description`, 
				`quote_attachment_file_name`
			FROM `erp_quote_attachments`
			WHERE `quote_id` = :QuoteId"
			, array( ':QuoteId' => $quote_id));

			$quote_attachments = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($quote_attachments as $quote_attachment) {

        	array_push($data, $this->convertQuoteAttachment($quote_attachment));
        }

		return $data;
	}

	public function createQuoteAttachment($quote_attachment){


		$sql_insert = "
			INSERT INTO `erp_quote_attachments`(
				`quote_id`, 
				`quote_attachment_id`, 
				`quote_attachment_name`, 
				`quote_attachment_description`, 
				`quote_attachment_file_name`
			) 
			VALUES (
				:QuoteId,
				:QuoteAttachmentId,
				:QuoteAttachmentName,
				:QuoteAttachmentDescription,
				:QuoteAttachmentFileName
			)";




		    //var_dump($quote_attachment);
			$sql = $sql_insert;

		
			$quote_attachment->QuoteAttachmentId = $this->getQuoteAttachmentIdentity($quote_attachment->QuoteId);	
			


			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute($this->fillAttachmentParams($quote_attachment));

			
			
			

	}

	public function updateQuoteAttachment($quote_attachment){




		$sql_update = "
			UPDATE `erp_quote_details` SET
				`quote_attachment_description`=:QuoteAttachmentDescription,
				`quote_attachment_name` =:QuoteAttachmentName, 
				`quote_attachment_file_name`=:QuoteAttachmentFileName, 
			WHERE `quote_id` = :QuoteId and `quote_attachment_id` = :QuoteAttachmentId
		";


		
			$sql = $sql_update;



			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute($this->fillAttachmentParams($quote->QuoteId,$quote_attachment));

			
			
			
	}

	public function deleteQuoteAttachment($quote_attachment){
		$sql = "
			DELETE FROM `erp_quote_attachments` WHERE
				`quote_id` = :QuoteId and `quote_attachment_id` = :QuoteAttachmentId
				";


		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':QuoteId' => $quote_attachment->QuoteId, ':QuoteAttachmentId' => $quote_attachment->QuoteAttachmentId));
		
				
	}

	private function getQuoteAttachmentIdentity($quote_id){

		$quote_attachment_id=1;

		$sql = "
			SELECT  MAX(`quote_attachment_id`)+1 as quote_attachment_id FROM `erp_quote_attachments` WHERE `quote_id` = :QuoteId
		";

		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':QuoteId' => $quote_id));


 		$result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->quote_attachment_id)){
		 	$quote_attachment_id = $result->quote_attachment_id;
		 }

		
		return $quote_attachment_id;
    }

	private function getQuoteDetailIdentity($quote_id){

		$quote_detail_id=1;

		$sql = "
			SELECT  MAX(`quote_detail_id`)+1 as identity FROM `erp_quote_details` WHERE `quote_id` = :QuoteId
		";

		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':QuoteId' => $quote_id));


 		$result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->identity)){
		 	$quote_detail_id = $result->identity;
		 }

		
		return $quote_detail_id;
    }

    

    public function approveQuote($quote){

    	if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();

    	$quote = $this->updateQuote($quote);

    	$quote=$this->createSaleOrderFromQuote($quote->QuoteId);
        
        if($this->db->conn->inTransaction()) $this->db->conn->commit();

      	return $quote;

    }


    public function createSaleOrderFromQuote($QuoteId){

    	$QuoteModel = new QuoteModel();

    	$quote = $this->getQuote($QuoteId,true);
    	
    	//Create Sale Order        

       if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();


    	$SaleOrderModel = new SaleOrderModel();


    	$SO = new stdClass();
    	$SO->SaleOrderId = null;
    	$SO->QuoteId = $quote["QuoteId"];
		//if(isset($quote["QuoteFolio"])) $SO->SaleOrderFolio=$quote["QuoteFolio"];
		if(isset($quote["QuoteTitle"])) $SO->SaleOrderTitle=$quote["QuoteTitle"];
		if(isset($quote["QuoteTerms"])) $SO->SaleOrderDescription =$quote["QuoteTerms"];
		if(isset($quote["QuoteTerms"])) $SO->SaleOrderTerms=$quote["QuoteTerms"];
		$SO->SaleOrderDate=$quote["QuoteDate"];
		$SO->CustomerId=$quote["CustomerId"];
		$SO->SaleOrderStatus="Aprobada";
		if(isset($quote["QuoteSubtotal"])) $SO->SaleOrderSubtotal=$quote["QuoteSubtotal"];
		if(isset($quote["QuoteTaxes"])) $SO->SaleOrderTaxes=$quote["QuoteTaxes"];
		if(isset($quote["QuoteDiscounts"])) $SO->SaleOrderDiscounts=$quote["QuoteDiscounts"];
		$SO->SaleOrderTotal=$quote["QuoteTotal"];

		if(isset($quote["QuoteTotalDlls"])) $SO->SaleOrderTotalDlls=$quote["QuoteTotalDlls"];
        if(isset($quote["UserId"])) $SO->UserId=$quote["UserId"];
        if(isset($quote["CurrencyId"])) $SO->CurrencyId = $quote["CurrencyId"];
        if(isset($quote["PlaceId"])) $SO->PlaceId = $quote["PlaceId"];
        $SO->SaleOrderExchangeRate = $quote["QuoteExchangeRate"];
        $SO->TransactionId = $quote["TransactionId"];

       
        if(isset($quote["items"]))
        {
        	$SO->items = array();

			foreach ($quote["items"] as $quote_item){

				$item = new stdClass();

				if(isset($quote_item["QuoteDetailDescription"])) $item->SaleOrderDetailDescription=$quote_item["QuoteDetailDescription"];
				$item->SaleOrderDetailQTY= $quote_item["QuoteDetailQTY"];
				if(isset($quote_item["QuoteDetailUM"])) $item->SaleOrderDetailUM = $quote_item["QuoteDetailUM"];				
				if(isset($quote_item["QuoteDetailDiscount"])) $item->SaleOrderDetailDiscount = $quote_item["QuoteDetailDiscount"];
				if(isset($quote_item["QuoteDetailTax"])) $item->SaleOrderDetailTax = $quote_item["QuoteDetailTax"];
				if(isset($quote_item["QuoteDetailPrice"])) $item->SaleOrderDetailPrice = $quote_item["QuoteDetailPrice"];

				if(isset($quote_item["ProductId"])) $item->ProductId = $quote_item["ProductId"];

				array_push($SO->items, $item);
			}		

			
        }

      
        $SO = $SaleOrderModel->createSaleOrder($SO);
         //var_dump($SO);
        $quote["SaleOrderId"] = $SO->SaleOrderId;

         if($this->db->conn->inTransaction()) $this->db->conn->commit();

         return $quote;

    }

    public function cancelQuote($quote){

    	$quote->QuoteStatus = "Cancelada";

    	$this->db->conn->beginTransaction();

		$sql = " 
			UPDATE `erp_sale_orders` SET
			`sale_order_status` = :SaleOrderStatus
			WHERE quote_id= :QuoteId
		" ;
			
		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':QuoteId' => $quote->QuoteId ,':SaleOrderStatus' => $quote->QuoteStatus));

		$sql = " 
			UPDATE `erp_quotes` SET
				`quote_status` = :QuoteStatus
			WHERE quote_id=:QuoteId
		" ;

		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':QuoteId' => $quote->QuoteId , ':QuoteStatus' => $quote->QuoteStatus ));
		

		$this->db->conn->commit();

		return $quote;
    }

	private function fillParams($quote){

		$params = [];


		if(isset($quote->QuoteId)){  $params[':QuoteId'] = $quote->QuoteId; } else {  $params[':QuoteId'] = NULL; };
		if(isset($quote->QuoteFolio)){   $params[':QuoteFolio'] = $quote->QuoteFolio; } else {    $params[':QuoteFolio'] = NULL; };
		if(isset($quote->QuoteTitle)){   $params[':QuoteTitle'] = $quote->QuoteTitle; } else {   $params[':QuoteTitle'] = ""; };
		if(isset($quote->QuoteDate)){   $params[':QuoteDate'] = $quote->QuoteDate; } else {   $params[':QuoteDate'] = NULL; };
		if(isset($quote->CustomerId)){   $params[':CustomerId'] = $quote->CustomerId; } else {   $params[':CustomerId'] = NULL; };
		if(isset($quote->QuoteStatus)){   $params[':QuoteStatus'] = $quote->QuoteStatus; } else {   $params[':QuoteStatus'] = NULL;};
		if(isset($quote->QuoteSubtotal)){   $params[':QuoteSubtotal'] = $quote->QuoteSubtotal; } else {   $params[':QuoteSubtotal'] = NULL;};
		if(isset($quote->QuoteTaxes)){   $params[':QuoteTaxes'] = $quote->QuoteTaxes; } else {   $params[':QuoteTaxes'] = NULL;};
		if(isset($quote->QuoteDiscounts)){   $params[':QuoteDiscounts'] = $quote->QuoteDiscounts; } else {   $params[':QuoteDiscounts'] = NULL;};
		if(isset($quote->QuoteTotal)){   $params[':QuoteTotal'] = $quote->QuoteTotal; } else {   $params[':QuoteTotal'] = NULL;};
		if(isset($quote->QuoteTotalDlls)){   $params[':QuoteTotalDlls'] = $quote->QuoteTotalDlls; } else {   $params[':QuoteTotalDlls'] = NULL;};
		if(isset($quote->QuoteExchangeRate)){   $params[':QuoteExchangeRate'] = $quote->QuoteExchangeRate; } else {   $params[':QuoteExchangeRate'] = NULL;};
		if(isset($quote->QuoteTerms)){   $params[':QuoteTerms'] = $quote->QuoteTerms; } else {   $params[':QuoteTerms'] = NULL;};
		if(isset($quote->CurrencyId)){   $params[':CurrencyId'] = $quote->CurrencyId; } else {   $params[':CurrencyId'] = NULL;};
		if(isset($quote->TransactionId)){   $params[':TransactionId'] = $quote->TransactionId; } else {   $params[':TransactionId'] = uniqid();};
		
		if(isset($quote->PlaceId)){   $params[':PlaceId'] = $quote->PlaceId; } else {   
			
			$UserModel = new UserModel();
			$user = $UserModel->getUser(getOwnerId());
			
			if( $user->place_id > 0){ $params[":PlaceId"] = $user->place_id; } else {$params[':PlaceId'] = NULL;}
			
		};
								
        $params[':UserId']  = getOwnerId();
		return $params;
	}

	private function fillDetailParams($quote_id , $quote_detail){
		$params=[];
		$params[':QuoteId'] = $quote_id ;
		if(isset($quote_detail->QuoteDetailId)){   $params[':QuoteDetailId'] = $quote_detail->QuoteDetailId ; } else { $params[':QuoteDetailId'] = NULL ; }
		
		if(isset($quote_detail->QuoteDetailDescription)){   $params[':QuoteDetailDescription'] = $quote_detail->QuoteDetailDescription ; } else { $params[':QuoteDetailDescription'] = NULL ; }
		if(isset($quote_detail->QuoteDetailQTY)){   $params[':QuoteDetailQTY'] = $quote_detail->QuoteDetailQTY ; } else { $params[':QuoteDetailQTY'] = NULL ; }
		if(isset($quote_detail->QuoteDetailUM)){   $params[':QuoteDetailUM'] = $quote_detail->QuoteDetailUM ; } else { $params[':QuoteDetailUM'] = NULL ; }
		if(isset($quote_detail->QuoteDetailDiscount)){   $params[':QuoteDetailDiscount'] = $quote_detail->QuoteDetailDiscount ; } else { $params[':QuoteDetailDiscount'] = NULL ; }
		if(isset($quote_detail->QuoteDetailTax)){   $params[':QuoteDetailTax'] = $quote_detail->QuoteDetailTax ; } else { $params[':QuoteDetailTax'] = NULL ; }
		if(isset($quote_detail->QuoteDetailPrice)){   $params[':QuoteDetailPrice'] = $quote_detail->QuoteDetailPrice ; } else { $params[':QuoteDetailPrice'] = NULL ; }
		if(isset($quote_detail->QuoteDetailTotal)){   $params[':QuoteDetailTotal'] = $quote_detail->QuoteDetailTotal ; } else { $params[':QuoteDetailTotal'] = NULL ; }
        

        
		if(isset($quote_detail->Product->ProductId)){   

			$params[':ProductId'] = $quote_detail->Product->ProductId ; 

		} else { 
			$params[':ProductId'] = NULL ; 

			if(isset($quote_detail->Product) && is_string($quote_detail->Product)){

				 $params[':QuoteDetailDescription'] = $quote_detail->Product ; 
			}
		}


		return $params;
	}

	private function fillAttachmentParams($quote_attachment){

			$params =[];
		    $params[':QuoteId'] = ( isset($quote_attachment->QuoteId) ?  $quote_attachment->QuoteId : NULL );
			$params[':QuoteAttachmentId'] = ( isset($quote_attachment->QuoteAttachmentId) ? $quote_attachment->QuoteAttachmentId : NULL );
			$params[':QuoteAttachmentName'] = ( isset($quote_attachment->QuoteAttachmentName) ?  $quote_attachment->QuoteAttachmentName : NULL );
    		$params[':QuoteAttachmentDescription'] = ( isset($quote_attachment->QuoteAttachmentDescription) ?  $quote_attachment->QuoteAttachmentDescription : NULL );
			$params[':QuoteAttachmentFileName'] = ( isset($quote_attachment->QuoteAttachmentFileName) ?  $quote_attachment->QuoteAttachmentFileName :  NULL );
			
            
			return $params;
	}

	private function convertQuote($quote,$detailed=false){

        //var_dump($quote);

		$QuoteData = array(
			"QuoteId" => $quote->quote_id,
			//"QuoteFolio" => $quote->quote_folio,
			"QuoteFolio" => 'COT-'.$quote->quote_id,
			"QuoteTitle" => $quote->quote_title,
			"QuoteDate" => $quote->quote_date,
			"CustomerId" => $quote->customer_id,
			"QuoteStatus" => $quote->quote_status,
			"QuoteSubtotal" => $quote->quote_subtotal,
			"QuoteTaxes" => $quote->quote_taxes,
			"QuoteDiscounts" => $quote->quote_discounts,
			"QuoteTotal" => $quote->quote_total,
			"QuoteTotalDlls" => $quote->quote_total_dlls,
			"QuoteTerms" => $quote->quote_terms,
			"CurrencyId" => $quote->currency_id,
			"QuoteCDate" => $quote->quote_cdate,
			"QuoteLMDate" =>  $quote->quote_lmdate,
			"QuoteExchangeRate" => $quote->quote_exchange_rate,
			"UserId" => $quote->user_id,
			"CurrencyId" => $quote->currency_id,
			"TransactionId" => $quote->transaction_id,
			"CreatedBy" => $quote->created_by,
			"PlaceId" => $quote->place_id
		);


		if(isset($quote->customer_id)){

			$CustomerModel = new CustomerModel();
			$customer = $CustomerModel->getCustomer($quote->customer_id);
			$QuoteData["customer"] = $customer;

		}

		if(isset($quote->user_id)){

			$UserModel = new UserModel();
			$user = $UserModel->getUser($quote->user_id);
			$QuoteData["UserName"] = $user->user_name;

		}
		
		if(isset($quote->created_by)){

			$UserModel = new UserModel();
			$user = $UserModel->getUser($quote->created_by);
			$QuoteData["CreatedByName"] = $user->user_name;

		}

		if(isset($quote->currency_id)){
			$CurrencyModel = new CurrencyModel();
			$Currency = $CurrencyModel->getCurrency($quote->currency_id);
			$QuoteData["CurrencyName"]= $Currency["CurrencyName"];
			$QuoteData["CurrencySymbol"]= $Currency["CurrencySymbol"];
		}
        
		if($detailed) {

			$SaleOrderModel = new SaleOrderModel();
            $SO = $SaleOrderModel->getSaleOrderByQuoteId($quote->quote_id,false);
            if(isset($SO["SaleOrderId"])){
            	$QuoteData["SaleOrderId"] = $SO["SaleOrderId"] ;

            	$WorkOrderModel = new WorkOrderModel();
            	$WO = $WorkOrderModel->getWorkOrderBySale($SO["SaleOrderId"],false);
            	if(isset($WO["WorkOrderId"])){
            		$QuoteData["WorkOrderId"]=$WO["WorkOrderId"];
            	}
            }
 		    
			$QuoteData["items"] = $this->getQuoteDetails($quote->quote_id);

		}
    
		return $QuoteData ;
	}

	private function convertQuoteDetail($quote_detail){


		$QuoteDetailData = array(
			"QuoteDetailId" => $quote_detail->quote_detail_id,
			"QuoteDetailDescription" => $quote_detail->quote_detail_description,
			"ProductId" => $quote_detail->product_id,
			"QuoteDetailQTY" => $quote_detail->quote_detail_qty,
			"QuoteDetailUM" => $quote_detail->quote_detail_um,
			"QuoteDetailPrice" => $quote_detail->quote_detail_price,
			"QuoteDetailDiscount" => $quote_detail->quote_detail_discount,
			"QuoteDetailTax" => $quote_detail->quote_detail_tax,
            "QuoteDetailTotal" => $quote_detail->quote_detail_total
		);

		if(!isset($quote_detail->product_id)){
			$QuoteDetailData["Product"] =  $quote_detail->quote_detail_description;


			if(strlen($quote_detail->quote_detail_description) > 0)
				$QuoteDetailData["IsManualItem"] =  true;
		}
		else{
			$ProductModel = new ProductModel();
			$product = $ProductModel->getProduct($quote_detail->product_id);
			$QuoteDetailData["Product"] = $product;
		}





		
        


		return $QuoteDetailData ;
	}

	private function convertQuoteAttachment($quote_attachment){


		$QuoteAttachmentData = array(
			"QuoteId" => $quote_attachment->quote_id,
			"QuoteAttachmentId" => $quote_attachment->quote_attachment_id,
			"QuoteAttachmentName" => $quote_attachment->quote_attachment_name,
			"QuoteAttachmentDescription" => $quote_attachment->quote_attachment_description,
			"QuoteAttachmentFileName" => $quote_attachment->quote_attachment_file_name
			
		);


        if(isset($quote_attachment->quote_attachment_file_name)){
			$QuoteAttachmentData["QuoteAttachmentUrl"] =  ABSOLUTE_BASE_URL.$quote_attachment->quote_attachment_file_name;
        }

		return $QuoteAttachmentData ;
	}
}
?>