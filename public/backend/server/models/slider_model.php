<?php


class SliderModel  {

	private $pdo;
	private $db; //Less

	public function __construct(){
        
 
		$this->pdo = new DB();
		
		$this->db = new LessQL\Database( $this->pdo->conn );
	    
	}

	public function getAll(){
		
		$result = $this->db->slider();
	
		$data = $result->fetchAll();  // fetch all rows
	
		return $data;

	}

	public function get($id){
		
		$result = $this->db->slider( $id );

		return  $result;

	}

	public function create($slider) {
		
		$slider = $this->db->slider()->createRow($slider);
		
		$slider->save();


		return $slider;
	}

	public function update($slider) {
		
		$row = $this->get($slider["id"]);
		
		$slider = $row->update($slider);
		
		//$slider->save();


		return $slider;
	}

	public function delete($id){
		
		$row = $this->get($slider["id"]);
		
		$slider = $row->delete($slider);
		
		//$slider->save();


		return $slider;
		
		

		
	}

	public function update_picture($id,$image){
		
			$row = $this->get($id);
		
		$slider = $row->update(array("image" => $image));
		
	
		
		
	}







}
?>