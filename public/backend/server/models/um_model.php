<?php
	class UMModel {
	
	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getUMS(){

		$result = $this->db->query("
			SELECT 
				`um_id`,
				`um_name`,
				`um_abbreviation`
			FROM `erp_ums`"
			,
			array()
		);

		$ums = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($ums as $um) {

        	array_push($data, $this->convertUM($um));
        }

		return $data;

	}

	public function getUM($UMId){
		$result = $result = $this->db->query("
			SELECT 
				`um_id`,
				`um_name`,
				`um_abbreviation`
			FROM `erp_ums` Where `um_id` = :UMId"
			,
			array(':UMId' => $UMId)
		);

		$um = $result->fetch(PDO::FETCH_OBJ);
        

        return $this->convertUM($um);


	}

	public function createUM($um){
		$this->db->query("
			INSERT INTO `erp_ums` (
				`um_id`,
				`um_name`, 
				`um_abbreviation`
			) 
			VALUES ( 
				:UMId ,
				:UMName, 
				:UMAbbreviation
			)",
			$this->fillParams($um)
		);	

		$um->UMId = $this->db->getInsertId();

		return $um;	
	}

	public function updateUM($um){
		
		$this->db->query("
			UPDATE `erp_ums` SET 
				`um_name`=:UMName, 
				`um_abbreviation`=:UMAbbreviation
		 WHERE `um_id` = :UMId",
		   $this->fillParams($um)
		 );
		
	}


	public function deleteUM($UMId){
		
		$this->db->query("
			DELETE FROM `erp_ums` 
			WHERE `um_id` = :UMId",
		array(':UMId' => $UMId ));
		
	}

    private function fillParams($um) {

		$params = [];

		if(isset($um->UMId))	 { $params[':UMId'] = $um->UMId; } else { $params[':UMId'] = NULL; }; 
		if(isset($um->UMName))	 { $params[':UMName'] = $um->UMName; } else { $params[':UMName'] = NULL; }; 
		if(isset($um->UMAbbreviation))	 { $params[':UMAbbreviation'] = $um->UMAbbreviation; } else { $params[':UMAbbreviation'] = NULL; }; 
	
		return $params;
			
	}


	private function convertUM($um)
	{
		return array(
			'UMId' => $um->um_id,
			'UMName' => $um->um_name, 
			'UMDescription' => $um->um_abbreviation
    	);
	}
}
?>