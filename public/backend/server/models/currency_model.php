<?php
class CurrencyModel {

	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}
	
	public function searchCurrencies($query){

		$result = $this->db->query("
			SELECT 
				`currency_id`,
				`currency_name`, 
				`currency_symbol`,
                `currency_default`				
			FROM `erp_currencies`
			WHERE currency_name like :CurrencyName ORDER BY `currency_default` DESC"
			,
			array(':CurrencyName' => $query.'%')
		);

		$currencies = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($currencies as $currency) {

        	array_push($data, $this->convertCurrency($currency));
        }

		return $data;

	}


	public function getCurrencies()
	{
	$result = $this->db->query("
			SELECT 
				`currency_id`,
				`currency_name`, 
				`currency_symbol`,
                `currency_default`				
			FROM `erp_currencies`
			ORDER BY currency_default DESC
			"
			,
			array()
		);

		$currencies = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($currencies as $currency) {

        	array_push($data, $this->convertCurrency($currency));
        }

		return $data;
	
	
	}

	public function getCurrency($CurrencyId){
	$result = $this->db->query("
			SELECT 
				`currency_id`,
				`currency_name`, 
				`currency_symbol`,
                `currency_default`				
			FROM `erp_currencies`
			WHERE `currency_id` = :CurrencyId ORDER BY currency_default DESC"
			,
			array( ':CurrencyId' => $CurrencyId )
		);

		$currency=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertCurrency($currency);
	}

	public function createCurrency($currency){
	$this->db->query("
			INSERT INTO `erp_currencies` (
				`currency_id`,
				`currency_name`, 
				`currency_symbol`, 
				`currency_default`
			) 
			VALUES ( 
				:CurrencyId ,
				:CurrencyName, 
				:CurrencySymbol, 
				:CurrencyDefault 
			)",
			$this->fillParams($currency)
		);	

		$currency->CurrencyId = $this->db->getInsertId();

		return $currency;
	}

	public function updateCurrency($currency){
	$this->db->query("
			UPDATE `erp_currencies` SET 
				`currency_id`=:CurrencyId, 
				`currency_name`=:CurrencyName,
				`currency_symbol`=:CurrencySymbol,
				`currency_default`=:CurrencyDefault
		WHERE `currency_id` = :CurrencyId",
		   $this->fillParams($currency)
		 );
	}

	
	public function deleteCurrency($CurrencyId){
	$this->db->query("
			DELETE FROM `erp_currencies` 
			WHERE `currency_id` = :CurrencyId",
		array(':CurrencyId' => $CurrencyId ));
	}
	
	private function fillParams($currency) {

		$params = [];

		if(isset($currency->CurrencyId))	 { $params[':CurrencyId'] = $currency->CurrencyId; } else { $params[':CurrencyId'] = NULL; }; 
		if(isset($currency->CurrencyName))	 { $params[':CurrencyName'] = $currency->CurrencyName; } else { $params[':CurrencyName'] = NULL; }; 
		if(isset($currency->CurrencySymbol))	 { $params[':CurrencySymbol'] = $currency->CurrencySymbol; } else { $params[':CurrencySymbol'] = NULL; }; 
		if(isset($currency->CurrencyDefault))	 { $params[':CurrencyDefault'] = $currency->CurrencyDefault; } else { $params[':CurrencyDefault'] = NULL;};  
		
		return $params;
			
	}
	

	private function convertCurrency($currency){
	return array(
			'CurrencyId' => $currency->currency_id,
			'CurrencyName' => $currency->currency_name, 
			'CurrencySymbol' => $currency->currency_symbol,
    		'CurrencyDefault' => $currency->currency_default 
		);
	}

	private function assignParams($currency){}
	
	
	 

}
?>