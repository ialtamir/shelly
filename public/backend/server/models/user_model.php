<?php
class UserModel {
	
	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getUsers(){

        $result = $this->db->query("
			SELECT 
				user_id,
				user_name,
				user_email,
				user_is_admin,
				place_id
			FROM oauth_users"
			,
			array()
		);

		$users = $result->fetchAll(PDO::FETCH_OBJ);

		return $users;
	}

	public function getUser($userId){

		$result = $this->db->query("
			SELECT 
				user_id,
				user_name,
				user_email,
				user_is_admin,
				place_id,
				user_type
			FROM oauth_users  WHERE 
				user_id = :UserId
			"
			,
			array
			(
				':UserId' => $userId
			)
		);

		 $user=$result->fetch(PDO::FETCH_OBJ);
		 
		 

		 return $user;
	}
	
	public function getUserByEmail($userEmail){

		$result = $this->db->query("
			SELECT 
				user_id,
				user_name,
				user_email,
				user_is_admin,
				place_id,
				user_type
			FROM oauth_users  WHERE 
				user_email = :user_email
			"
			,
			array
			(
				':user_email' => $userEmail
			)
		);

		 $user=$result->fetch(PDO::FETCH_OBJ);
		 
		 

		 return $user;
	}

	public function createUser($user){

		 $this->db->query('
		 	INSERT INTO  oauth_users (
				user_id ,
				user_name ,
				user_email, 
				user_is_admin,
				place_id
			)
			VALUES (
				NULL ,  
				:UserName,  
				:UserEmail,
				:UserIsAdmin,
				:PlaceId
			)'
			,
			array(
				':UserName' => $user->UserName,
				':UserEmail'=> $user->UserEmail,
				':UserIsAdmin'=> $user->UserIsAdmin,
				':PlaceId'=> $user->PlaceId

			) 
		);

		 $user->UserId = $this->db->getInsertId();

		 return $user;
	}
	
	public function createEcommerceUser($user){

		 $this->db->query('
		 	INSERT INTO  oauth_users (
			
				user_name ,
				user_email,
				user_type,
				place_id
				
			)
			VALUES (
			
				:user_name,  
				:user_email,
				"Customer",
				2
			
			)'
			,
			array(
				':user_name' => $user->name,
				':user_email'=> $user->email

			) 
		);

		 $user->id = $this->db->getInsertId();
		 $user->UserId=$user->id;
		 $user->UserPassword = $user->password;
		 
		 $this->setUserPassword($user);
		 
		 
		 
		$this->db->query("
			INSERT INTO erp_customers (
			
				customer_name, 
				customer_email, 
				user_id
			) 
			VALUES ( 

				:CustomerName, 
			
				:CustomerEmail,
				:UserId
			)",
			[":CustomerName"=>$user->name,":CustomerEmail"=>$user->email,":UserId" => $user->id]
		);	

		$user->CustomerId = $this->db->getInsertId();

		return $user;	
		
		

		 //return $user;
	}

	public function updateUser($user){
    	$this->db->query('
			UPDATE oauth_users SET  
				user_name = :UserName,
				user_email = :UserEmail,
				user_is_admin = :UserIsAdmin,
				place_id = :PlaceId
			WHERE  user_id = :UserId
			',
			array(
				':UserId'   => $user->UserId,
				':UserName' => $user->UserName,
				':UserEmail'=> $user->UserEmail,
				':UserIsAdmin'=> $user->UserIsAdmin,
				':PlaceId' => $user->PlaceId
			)
		);
	}

	public function deleteUser($user){
		$this->db->query('
			DELETE FROM oauth_users 
			WHERE  user_id = :UserId
			',
			array(
				':UserId'   => $user->UserId
			)
		);
	}

	public function setUserPassword($user){
		
        require_once '../../libraries/PasswordLib/PasswordLib.php';

        $PasswordLib = new \PasswordLib\PasswordLib;

        $Password = $PasswordLib->createPasswordHash($user->UserPassword );


		$this->db->query('
			UPDATE oauth_users SET  
				user_password = :UserPassword
			WHERE  user_id = :UserId
			',
			array(
				':UserId'   => $user->UserId,
				':UserPassword' => $Password
			)
		);

		$user->UserPassword = null;

		return $user;
	}
	
	public function setUserNewPassword($user){
		
		
		$sql = "SELECT * FROM oauth_session_email_token WHERE token=:Token and status=1";
		
		
		
			$result = $this->db->query($sql,
			array
			(
				':Token' => $user->user->recovery_token
			)
		);
		

		 $token_data=$result->fetch(PDO::FETCH_OBJ);
		 
		 
		 var_dump($token_data);
		 
		 if($token_data){
		 	
		 date_default_timezone_set('America/Tijuana');
		 
		 
        $date1 = new DateTime("now");
        $date2 = new DateTime($token_data->date_creation);

        $date2->add(new DateInterval('P01D'));

	        if ( ($date1->format('dMY') <= $date2->format('dMY')) and isset($token_data->id_user))
	        {
	            //$app->render('/recovery_user.php',$token);
	            $password1 = $user->user->new_password;
	            $password2 = $user->user->confirm_new_password;
	            
	            //$UserModel = new UserModel();
	            
	            $receivedUser = new StdClass();
	            $receivedUser->UserId = $token_data->id_user;
	            $receivedUser->UserPassword = $password1;
	    
	
	    
	    	    
	    	    $data =  $this->setUserPassword($receivedUser);
	            
	            
	            
	        }
	        else {
	            throw new Exception("El enlace ha expirado!");
	        }
		}else{
			
			    throw new Exception("El enlace es invalido!");
		}
		 
                
                
		 
		 
		
	}
	
	

	public function setUserScopes(){
	

	}

	public function validateUser($UserEmail,$UserPassword){
			
			$result = $this->db->query('

				SELECT 
					user_id,
					user_name,
					user_email,
					user_password

				FROM oauth_users  WHERE 
					user_email = :UserEmail 
			'
			,
			array
			(
				':UserEmail' => $UserEmail
			)

		);

		 $row=$result->fetch(PDO::FETCH_OBJ);

		 if($row){

		 	require_once '../../libraries/PasswordLib/PasswordLib.php';
             $PasswordLib = new \PasswordLib\PasswordLib;

            $password=$UserPassword;
		 	$isValid = $PasswordLib->verifyPasswordHash($password, $row->user_password);

             if($isValid){
             	return $row->user_id;
             }
        	
        }


		return false;
	}

	public function getUserScopes($UserId){

		
		$sql = "
			SELECT  
				`user_id`, 
				`scope_id` 
			FROM `oauth_user_scopes` 
			WHERE 
				`user_id` = :UserId
		";

		$result = $this->db->query($sql,
        			array( 
        				":UserId" => $UserId 
        			)
        		);

		$scopes = $result->fetchAll(PDO::FETCH_OBJ);

		$data = [];

		foreach ($scopes as $scope) {
			array_push($data, $this->convertUserScope($scope));
		}


		return $data;

	}


	public function getUserScopesSelected($UserId){

		$ScopeModel = new ScopeModel();

		$scopes = $ScopeModel->getScopes();

		
		$sql = "SELECT  1 as `scope_selected` FROM `oauth_user_scopes` WHERE `user_id` = :UserId AND `scope_id` = :ScopeId";
		foreach ($scopes as $scope) {


			$result = $this->db->query($sql,
        			array( 
        				":UserId" => $UserId , 
        				":ScopeId" => $scope->id
        			)
        		);

        		$object = $result->fetch(PDO::FETCH_OBJ);
        		

        		if(isset($object->scope_selected)) {
        			
                    $scope->UserId = $UserId;
        			$scope->scope_selected = (boolean)$object->scope_selected;

        		} 

		}

		return $scopes;

	}

	public function saveUserScopes($user){

		$sql_insert = "INSERT INTO `oauth_user_scopes`(`user_id`, `scope_id`) VALUES (:UserId,:ScopeId)";
		$sql_delete = "DELETE FROM `oauth_user_scopes` WHERE `user_id`=:UserId and `scope_id` = :ScopeId ";


		foreach ($user->user_scopes as $user_scope) {
			$sql = null;

			if($user_scope->scope_selected){

				if(!isset($user_scope->UserId)){

					$sql = $sql_insert;
				}

			}else{

				if(isset($user_scope->UserId)){
					$sql = $sql_delete;
				}
			}

            if(isset($sql)){
            	var_dump($sql);
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(":UserId" => $user->UserId,":ScopeId" => $user_scope->id));
			}

		}

	}



	public function convertUser($user,$includeScopes = false){

		$data = array(
            	'UserId' => $user->user_id,
               	'UserName' => $user->user_name,
               	'UserEmail' =>$user->user_email,
                'UserIsAdmin' => (bool)$user->user_is_admin,
                'PlaceId' => $user->place_id
            );
        
        if($data["PlaceId"]){
        	
        	$PlaceModel = new PlaceModel();
        	$data["place"] = $PlaceModel->getPlace($data["PlaceId"]);
        }
            
        return $data;

		if($includeScopes)
		{

		}
	}

	public function convertUserScope($scope){

		$scope_data = array(
				"UserId" => $scope->user_id,
				"ScopeId" => $scope->scope_id,

			);

		$ScopeModel = new ScopeModel();
		$Scope = $ScopeModel->getScopeById($scope->scope_id);

		$scope_data["ScopeName"] = $Scope["scope"];

		return $scope_data ;
	}

	

}
?>