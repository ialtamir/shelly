<?php
/**
* 
*/
class VariantModel
{

	private $db;
	private $sql_select = "SELECT `variant_id`, `variant_name` FROM `erp_variants` ";
	

	function __construct()
	{
		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());
	}

	public function getVariants(){

		$sql = $this->sql_select; 

		$statement = $this->db->query( $sql , array() );
		
		$results = $statement->fetchAll(PDO::FETCH_OBJ);
        
        $variants = [];

        foreach ($results as $variant) {

        	array_push($variants, $this->convertVariant($variant));

        }

		return $variants;


	}

	public function getVariant($variant){

		$sql = $this->sql_select;
		$filter = "  WHERE `variant_id` = :VariantId ";

		$sql = $sql.$filter;
		
		$statement = $this->db->query( $sql , array(':VariantId' => $variant->VariantId) );
		
		$result = $statement->fetch(PDO::FETCH_OBJ);
        
        $variant =  $this->convertVariant($result);
        
		return $variant;

	}

	public function getVariantByName($variant_name){
		
		$sql = $this->sql_select;
		$filter = "  WHERE `variant_name` = :VariantName ORDER BY `variant_id` LIMIT 1";

		$sql = $sql.$filter;
		
		$statement = $this->db->query( $sql , array(':VariantName' => $variant_name) );
		
		$result = $statement->fetch(PDO::FETCH_OBJ);
        
        $variant =  $this->convertVariant($result);
        
		return $variant;

	}

	public function createVariant($variant){

		$sql = "INSERT INTO `erp_variants`(`variant_id`, `variant_name`) 
			VALUES 	(:VariantId,:VariantName)";

		$this->db->query($sql,
			$this->fillVariantParameters($variant)
		);

		$variant->VariantId = $this->db->getInsertId();

		return $variant;


	}

	public function updateVariant($variant){

		$sql = "UPDATE `erp_variants` SET 			
	
				`variant_name`=:VariantName,

			WHERE `variant_id`=:VariantId
		";

		$this->db->query($sql,$this->fillVariantParameters($variant));
		

	}

	public function deleteVariant($variant){

		$sql = "DELETE FROM `variants` 			
			WHERE `variant_id`=:VariantId
		";

		$this->db->query($sql, array(':VariantId' => $variant->VariantId));
	}


	private function fillVariantParameters($variant){
		
		$parameters = [];

		$parameters[':VariantId']			= ( isset($variant->VariantId) 			?  $variant->VariantId 			: NULL );
		
		$parameters[':VariantName']			= ( isset($variant->VariantName) 		?  $variant->VariantName 		: NULL );
		

		return $parameters;
	}

	private function convertVariant($variant){

		$data = [];

		
		$data['VariantId']			= ( isset($variant->variant_id) 		  ?  $variant->variant_id 			: NULL );		
		$data['VariantName']		= ( isset($variant->variant_name) 		  ?  $variant->variant_name 		: NULL );		

		return $data;
	}


	//VariantValues

	public function getVariantValues($variant_id){

		$sql = "SELECT `variant_id`, `variant_value_id`, `variant_value_name`, `variant_value_picture_url` FROM `erp_variant_values` WHERE `variant_id` = :VariantId"; 

		$statement = $this->db->query( $sql , array(':VariantId' => $variant_id) );
		
		$results = $statement->fetchAll(PDO::FETCH_OBJ);
        
        $variant_values = [];

        foreach ($results as $variant_value) {

        	array_push($variant_values, $this->convertVariantValue($variant_value));

        }

		return $variant_values;
	}

	public function createVariantValue($variant_value){

		

		$sql = " UPDATE `erp_variant_values` SET 			
			`variant_value_name`=:VariantValueName,
			`variant_value_picture_url`=:VariantValuePictureUrl WHERE 
			`variant_id`=:VariantId AND `variant_value_id`=:VariantValueId
			";

		if(!isset($variant_value->VariantValueId))
		{
			$sql = "INSERT INTO `erp_variant_values`(`variant_id`, `variant_value_id`,`variant_value_name`,`variant_value_picture_url` ) 
			VALUES 	(:VariantId,:VariantValueId,:VariantValueName,:VariantValuePictureUrl)";
	    	$variant_value->VariantValueId = $this->createVariantValueIdentity($variant_value);
		}


		$this->db->query($sql,
			$this->fillVariantValueParameters($variant_value)
		);

		
		return $variant;


	}

	public function createVariantValueIdentity($variant_value){

		$VariantValueIdentity = 1;

		$sql = "SELECT MAX(  `variant_value_id` )+1 as identity
			FROM  `erp_variant_values` 
			WHERE  `variant_id` =:VariantId 
		";

		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':VariantId' => $variant_value->VariantId));

         $result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->identity)){
		 	$VariantValueIdentity = $result->identity;
		 }



		return $VariantValueIdentity;
		
			
	}

	public function deleteVariantValue($variant_value){
		$sql = "DELETE FROM `erp_variant_values` 			
			WHERE `variant_id`=:VariantId
			AND `variant_value_id` = :VariantValueId
		";

		$this->db->query($sql, array(':VariantId' => $variant_value->VariantId, ":VariantValueId" => $variant_value->VariantValueId ));
	}

	private function  fillVariantValueParameters($variant_value){
		$parameters = [];

		$parameters[':VariantId']				= ( isset($variant_value->VariantId) 			?  $variant_value->VariantId 			: NULL );
		$parameters[':VariantValueId']			= ( isset($variant_value->VariantValueId) 		?  $variant_value->VariantValueId 		: NULL );
		$parameters[':VariantValueName']		= ( isset($variant_value->VariantValueName) 		?  $variant_value->VariantValueName 		: NULL );
		$parameters[':VariantValuePictureUrl']	= ( isset($variant_value->VariantValuePictureUrl) 		?  $variant_value->VariantValuePictureUrl 		: NULL );
		
		

		return $parameters;
	}

	private function convertVariantValue($variant_value){

		$data = [];

		$data['VariantId']				= ( isset($variant_value->variant_id) 					?  $variant_value->variant_id 				: NULL );
		$data['VariantValueId']			= ( isset($variant_value->variant_value_id) 		?  $variant_value->variant_value_id 		: NULL );
		$data['VariantValueName']		= ( isset($variant_value->variant_value_name) 		?  $variant_value->variant_value_name 		: NULL );
		$data['VariantValuePictureUrl']	= ( isset($variant_value->variant_value_picture_url)?  $variant_value->variant_value_picture_url: NULL );
		
		

		return $data;
	}
}
?>