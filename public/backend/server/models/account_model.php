<?php
class AccountModel {


	//AccountConfigurations

	private $db;
	private $account_configuration_select_sql = " SELECT `account_config_id`, `account_config_code`, `account_config_value` FROM `account_configurations` ";
    private $account_configuration_insert_sql = "";
	private $account_configuration_update_sql = "";

    
    private $statements = array( 
                            "SELECT" => "SELECT * FROM `accounts` ",
                            "SELECT_CUSTOMIZATION" => "SELECT * FROM `account_customization` WHERE account_id = :account_id ",
                            "UPDATE_INFO" => "UPDATE `accounts` SET `company_name`=:company_name, `country` = :country, `address`=:address, `city`=:city, `state`=:state, `business_phone`=:business_phone, `mobile`= :mobile, `rfc`= :rfc, `web_url`=:web_url WHERE `id`=:id",
                            "UPDATE_INVOICE_INFO" => "UPDATE `accounts` SET `company_name`=:company_name,`trade_name`=:trade_name,`trade_regimen`=:trade_regimen,`email`=:email, `country` = :country, `street`=:street,`numExt`=:numExt,`numInt`=:numInt,`colony`=:colony,`localidad`=:localidad,`cp`=:cp, `city`=:city, `state`=:state, `business_phone`=:business_phone, `mobile`= :mobile, `rfc`= :rfc, `web_url`=:web_url,`updated` = NOW() WHERE `id`=:id",
                            "UPDATE_TIMBRES_DES" => "UPDATE `accounts` SET `invoice_folios`=invoice_folios-1 WHERE `id`=:id",
                            "UPDATE_TIMBRES_ADD" => "UPDATE `accounts` SET `invoice_folios`=invoice_folios+:total WHERE `id`=:id",
                            "UPDATE_INVOICE_CERT_PASSWORD" => "UPDATE `accounts` SET `cert_password`=:cert_password WHERE `id`=:id",
                            "UPDATE_LOGO" => "UPDATE `account_customization` SET `logo_path`=:logo_path WHERE `account_id`=:id"
                            
                          );
    
	public function __construct(){

	
        
        $absolute_configuration = new stdClass();
        $absolute_configuration->database_host = ABSOLUTE_DB_HOST;
        $absolute_configuration->database_name = ABSOLUTE_DB_NAME;
        $absolute_configuration->database_user = ABSOLUTE_DB_USER;
        $absolute_configuration->database_password = ABSOLUTE_DB_PASSWORD;
                
    
        
        $this->db = DB::withAccount($absolute_configuration);

	}

    

	
	/*
		This document returns a key value array;
	*/
	public function getAccountConfigurations($include_server_configurations=false){

        //$this->db = DB::withAccount(AccountModel::getAccountConfiguration());
        
		$sql = $this->account_configuration_select_sql;	
		$parameters = array();



		$result = $this->db->query($sql,$parameters);

		$account_configurations_records = $result->fetchAll(PDO::FETCH_OBJ);
        
        $account_configurations_data = new stdClass();

        foreach ($account_configurations_records as $account_configurations_record) {

        	$account_configurations_data[$account_configurations_record->account_config_code] = $account_configurations_record->account_config_value;

        	


        }

		return $account_configurations_data;

	}

	public function getAccountConfigurationsByHostName($host_name = "",$include_server_configurations=false){
        
        $this->db = DB::withAccount(AccountModel::getAccountConfiguration());

		$sql = $this->account_configuration_select_sql;	
		$parameters = array();


		$result = $this->db->query($sql,$parameters);

		$account_configurations_records = $result->fetchAll(PDO::FETCH_OBJ);
        
        $account_configurations_data = [];

        foreach ($account_configurations_records as $account_configurations_record) {

        	$account_configurations_data[$account_configurations_record->account_config_code] = $account_configurations_record->account_config_value;

        	


        }
        
        

		return $this->convertAccountConfigurations($account_configurations_data,$include_server_configurations);

	}



	private function convertAccountConfigurations($account_configurations_data, $include_server_configurations=false){



          
			$account_configuration_obj = new stdClass();

           
           $account_configuration_obj->CompanyName = $account_configurations_data["company_name"];
           $account_configuration_obj->CompanyFullName = $account_configurations_data["company_full_name"];
           $account_configuration_obj->CompanyLogo = ABSOLUTE_BASE_URL.$account_configurations_data["company_logo_path"];
           $account_configuration_obj->CompanyLogoSmall = ABSOLUTE_BASE_URL.$account_configurations_data["company_logo_small_path"];
           $account_configuration_obj->CompanyAddress = $account_configurations_data["company_address"];
           $account_configuration_obj->CompanyWebUrl = $account_configurations_data["company_web_url"];
           
           if($include_server_configurations){
           
                $account_configuration_obj->CompanySmtpHost = $account_configurations_data["company_smtp_host"];
                $account_configuration_obj->CompanySmtpPort = intval($account_configurations_data["company_smtp_port"]);
                $account_configuration_obj->CompanySmtpUserName = $account_configurations_data["company_smtp_user_name"];
                $account_configuration_obj->CompanySmtpPassword = $account_configurations_data["company_smtp_password"];
                $account_configuration_obj->CompanyAddReplyTo = $account_configurations_data["company_add_reply_to"];
                $account_configuration_obj->CompanyAddReplyToName = $account_configurations_data["company_add_reply_to_name"];
                
           }
           
           
             


          return $account_configuration_obj;

	}
    
    
    /*NEW SECTION*/
    
    
    public  function getAccount($include_passwords = false){
        
        $http_host = $_SERVER["HTTP_HOST"];
        
        
        $sql = $this->statements["SELECT"];	
		$parameters = array();

		if(isset($http_host)){
			$sql=$sql." WHERE `domain` = :domain ";
			$parameters[":domain"] = $http_host;
		}
		
       
		$result = $this->db->query($sql,$parameters);

		$account_data = $result->fetch(PDO::FETCH_OBJ);
        
        if(!$include_passwords)
        {
            if(isset($account_data->cert_password) && $account_data->cert_password != ""){
                
                $account_data->cert_password = "";
                $account_data->cert_password_is_set = true;
            }
        }
        
		return $account_data;

    }
        
    public  function getAccountCustomization($include_server_settings=false){
        
        $account = $this->getAccount();
        
        $sql = $this->statements["SELECT_CUSTOMIZATION"];	
		$parameters = array();


		$parameters[":account_id"] = $account->id;
			
		

		$result = $this->db->query($sql,$parameters);

		$dbobject = $result->fetch(PDO::FETCH_OBJ);
        
        if($include_server_settings){
            
            $account_configuration = AccountModel::getAccountConfiguration();  
            
            $dbobject->logo_absolute_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path.CONST_ACCOUNT_PUBLIC_FOLDER.$dbobject->logo_path;
            
        }
        
        //var_dump($dbobject);
        //exit();
		return $dbobject;

    }
    
   public  function getAccountByHost($http_host){
        
        $sql = $this->statements["SELECT"];	
		$parameters = array();

		if(isset($http_host)){
		    
			$sql=$sql." WHERE `domain` = :domain ";
			$parameters[":domain"] = $http_host;
			
		}
		

		$result = $this->db->query($sql,$parameters);

		$account_data = $result->fetch(PDO::FETCH_OBJ);
        
		return $account_data;

    }
    
    public function updateAccount($account){
        
        
        
        $current_account = $this->getAccount();
        
        $sql = $this->statements["UPDATE_INVOICE_INFO"];	
        $parameters = [];
         
         
         
        $parameters[":id"] = ( isset($current_account->id) ?  $current_account->id : NULL );
        $parameters[":company_name"] = ( isset($account->company_name) ?  $account->company_name : NULL );
        $parameters[":trade_name"] = ( isset($account->trade_name) ?  $account->trade_name : NULL );
        $parameters[":trade_regimen"] = ( isset($account->trade_regimen) ?  $account->trade_regimen : NULL );
        $parameters[":email"] = ( isset($account->email) ?  $account->email : NULL );
        $parameters[":country"] = ( isset($account->country) ?  $account->country : NULL );
        $parameters[":street"] = ( isset($account->street) ?  $account->street : NULL );
        $parameters[":numExt"] = ( isset($account->numExt) ?  $account->numExt : NULL );
        $parameters[":numInt"] = ( isset($account->numInt) ?  $account->numInt : NULL );
        $parameters[":colony"] = ( isset($account->colony) ?  $account->colony : NULL );
        $parameters[":localidad"] = ( isset($account->localidad) ?  $account->localidad : NULL );
        $parameters[":cp"] = ( isset($account->cp) ?  $account->cp : NULL );
        $parameters[":city"] = ( isset($account->city) ?  $account->city : NULL );
        $parameters[":state"] = ( isset($account->state) ?  $account->state : NULL );
        $parameters[":business_phone"] = ( isset($account->business_phone) ?  $account->business_phone : NULL );
        $parameters[":mobile"] = ( isset($account->mobile) ?  $account->mobile : NULL );
        $parameters[":rfc"] = ( isset($account->rfc) ?  $account->rfc : NULL );
        $parameters[":web_url"] = ( isset($account->web_url) ?  $account->web_url : NULL );
          
          
         
         
         



	    $this->db->query($sql,$parameters);
	    
        return $account;
    }
    
    public function updateInvoiceCertPassword($account){
        
        $current_account = $this->getAccount();
        
        $sql = $this->statements["UPDATE_INVOICE_CERT_PASSWORD"];	
        $parameters = [];
     
        $parameters[":id"] = ( isset($current_account->id) ?  $current_account->id : NULL );
        
        $parameters[":cert_password"]   = $account->cert_password;
        
	    $this->db->query($sql,$parameters);
	    
        return $account;
        
        
    }
    
    public function updateTimbresAccount(){
        
        $current_account = $this->getAccount();
        
        $sql = $this->statements["UPDATE_TIMBRESDES"];	
        $parameters = [];
     
        $parameters[":id"] = ( isset($current_account->id) ?  $current_account->id : NULL );
          
	    $this->db->query($sql,$parameters);
	    
        return $account;
    }
    
     public function updateTimbresAccountAdd($total){
        
        $current_account = $this->getAccount();
        
        $sql = $this->statements["UPDATE_TIMBRES_ADD"];	
        $parameters = [];
     
        $parameters[":id"] = ( isset($current_account->id) ?  $current_account->id : NULL );
        $parameters[":total"] = ( isset($total) ?  $total : NULL );
          
	    $this->db->query($sql,$parameters);
	    
        return $account;
    }
    
    

    
    public static function getAccountConfiguration(){
        
        $http_host = $_SERVER["HTTP_HOST"];
         
        $instance = new self();
        $account = $instance->getAccountByHost($http_host);
        
        $AccountConfigurationModel = new AccountConfigurationModel();
        
        $account_configuration = $AccountConfigurationModel->getAccountConfiguration($account->id);
        
        return $account_configuration;
        
        
    }
    
    public function updateAccountConfiguration($logo_path){
        
    }
    
    public function updateAccountLogo($logo_path){
        
        $current_account = $this->getAccount();
        
        $sql = $this->statements["UPDATE_LOGO"];	
        $parameters = [];
     
        $parameters[":id"] = ( isset($current_account->id) ?  $current_account->id : NULL );
        
        $parameters[":logo_path"]   = $logo_path;
        
	    $this->db->query($sql,$parameters);
	    
        return $account;
        
    }

	
}
?>