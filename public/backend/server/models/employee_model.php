<?php

class EmployeeModel {
	

	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}
	
	public function searchEmployees($query){

		$result = $this->db->query("
			SELECT 
				`employee_id`, 
				`employee_name`, 
				`employee_last_name`, 
				`employee_last_name2`, 
				`employee_initials`, 
				`employee_position` 
			FROM `erp_employees` WHERE employee_name like :EmployeeName  "
			,
			array(':EmployeeName' => $query.'%')
		);

		$employees = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($employees as $employe) {

        	array_push($data, $this->convertEmployee($employee));
        }

		return $data;

	}

	public function getEmployeesByPosition($EmployeePosition){

		$result = $this->db->query("
			SELECT 
				`employee_id`, 
				`employee_name`, 
				`employee_last_name`, 
				`employee_last_name2`, 
				`employee_initials`, 
				`employee_position` 
			FROM `erp_employees` WHERE `employee_position` = :EmployeePosition  "
			,
			array(':EmployeePosition' => $EmployeePosition)
		);

		$employees = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($employees as $employee) {

        	array_push($data, $this->convertEmployee($employee));
        }

		return $data;
	}
	
	public function getEmployees(){

		$result = $this->db->query("
			SELECT 
				`employee_id`, 
				`employee_name`, 
				`employee_last_name`, 
				`employee_last_name2`, 
				`employee_initials`, 
				`employee_position` 
			FROM `erp_employees`
			"
			,
			array()
		);

		$employees = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($employees as $employee) {

        	array_push($data, $this->convertEmployee($employee));
        }

		return $data;

	}
	
	public function getEmployee($EmployeeId){

		$result = $this->db->query("
			SELECT 
				`employee_id`, 
				`employee_name`, 
				`employee_last_name`, 
				`employee_last_name2`, 
				`employee_initials`, 
				`employee_position` 
			FROM `erp_employees`
			WHERE `employee_id` = :EmployeeId"
			,
			array( ':EmployeeId' => $EmployeeId )
		);

		$employee=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertEmployee($employee);

	}
	
	public function createEmployee($employee){


		$this->db->query("
			INSERT INTO erp_employees (
				`employee_id`, 
				`employee_name`, 
				`employee_last_name`, 
				`employee_last_name2`, 
				`employee_initials`, 
				`employee_position` 
			) 
			VALUES ( 
				:EmployeeId ,
				:EmployeeName, 
				:EmployeeLastName, 
				:EmployeeLastName2, 
				:EmployeeInitials, 
				:EmployeePosition
			)",
			$this->fillParams($employee)
		);	

		$employee->EmployeeId = $this->db->getInsertId();

		return $employee;	
	}
	
	public function updateEmployee($employee){
		
		$this->db->query("
			UPDATE `erp_employees` SET 
				`employee_name`=:EmployeeName, 
				`employee_last_name`=:EmployeeLastName,
				`employee_last_name2`=:EmployeeLastName2,
				`employee_initials`=:EmployeeInitials,
				`employee_position`=:EmployeePosition
		 WHERE `employee_id` = :EmployeeId",
		   $this->fillParams($employee)
		 );
		
	}
	
	public function deleteEmployee($EmployeeId){
		
		$this->db->query("
			DELETE FROM `erp_employees` 
			WHERE `employee_id` = :EmployeeId",
		array(':EmployeeId' => $EmployeeId ));
		
	}
	
	private function fillParams($employee) {

		$params = [];

		if(isset($employee->EmployeeId))	 { $params[':EmployeeId'] = $employee->EmployeeId; } else { $params[':EmployeeId'] = NULL; }; 
		if(isset($employee->EmployeeName))	 { $params[':EmployeeName'] = $employee->EmployeeName; } else { $params[':EmployeeName'] = NULL; }; 
		if(isset($employee->EmployeeLastName))	 { $params[':EmployeeLastName'] = $employee->EmployeeLastName; } else { $params[':EmployeeLastName'] = NULL; }; 
		if(isset($employee->EmployeeLastName2))	 { $params[':EmployeeLastName2'] = $employee->EmployeeLastName2; } else { $params[':EmployeeLastName2'] = NULL;};  
		if(isset($employee->EmployeeInitials)){ $params[':EmployeeInitials'] = $employee->EmployeeInitials; } else { $params[':EmployeeInitials'] = NULL; };  
		if(isset($employee->EmployeePosition)){ $params[':EmployeePosition'] = $employee->EmployeePosition; } else { $params[':EmployeePosition'] = NULL; };  
		
		return $params;
			
	}
	
	


	private function convertEmployee($employee)
	{
		return array(
			"EmployeeId" => $employee->employee_id, 
			"EmployeeName" => $employee->employee_name, 
			"EmployeeLastName" => $employee->employee_last_name, 
			"EmployeeLastName2" => $employee->employee_last_name2, 
			"EmployeeInitials" => $employee->employee_initials, 
			"EmployeePosition" => $employee->employee_position
		);
	}
}
?>