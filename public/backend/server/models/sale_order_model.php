<?php
class SaleOrderModel {

	private $db;
	
	private $statements = array(
		"SELECT" => "SELECT * FROM erp_sale_orders WHERE ( sale_order_deleted = 0 OR sale_order_deleted IS NULL) ",
		"DEFAULT_ORDER" => " ORDER BY `sale_order_date` DESC, `sale_order_id` DESC"
	);

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getSaleOrders(){
		
		$sql = $this->statements["SELECT"]. " AND sale_order_status = 'Pendiente' ";
		
		$result = $this->checkRowLevelPermissions($sql,array());

		$sale_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

	        foreach ($sale_orders as $sale_order) {
	
	        	array_push($data, $this->convertSaleOrder($sale_order));
	        }

		return $data;

	}
	
	public function getSaleOrdersByCustomer($cusomer_id){
		
		$sql = $this->statements["SELECT"]. " AND customer_id =:CustomerId ";
		
		$result = $this->checkRowLevelPermissions($sql,array(":CustomerId" => $cusomer_id));

		$sale_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

	        foreach ($sale_orders as $sale_order) {
	
	        	array_push($data, $this->convertSaleOrder($sale_order));
	        }

		return $data;

	}
	
	private function checkRowLevelPermissions($sql,$parameters){
		
		$CurrentUserId = getOwnerId();
		
		
		$UserModel = new UserModel();
		$current_user = $UserModel->getUser($CurrentUserId);
		
	//var_dump($current_user);
		
		if(!((bool)$current_user->user_is_admin)){
			
			//El usuario solo podra ver las ventas de sucursal
			if( $current_user->place_id > 0){
				
				$sql = $sql." AND place_id =:PlaceId";
				$parameters[":PlaceId"] = $current_user->place_id;
				
			}
			else {
				
				$sql = $sql . " AND created_by = :UserId";
				$parameters[":UserId"] = getOwnerId();
				
			}
			
			
		}
		
		return $this->db->query($sql.$this->statements["DEFAULT_ORDER"], $parameters);
	
		
	}
	
	public function getApprovedSaleOrders(){
		
		$sql = $this->statements["SELECT"]. " AND `sale_order_status` = 'Aprobada' ";
		
		$result = $this->checkRowLevelPermissions($sql,array());

		$sale_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($sale_orders as $sale_order) {

        	array_push($data, $this->convertSaleOrder($sale_order));
        }

		return $data;

	}
	
	public function getFacturadasSaleOrders(){
		
		$sql = $this->statements["SELECT"]. " AND `invoice_id` <> '' ";
	
		$result = $this->checkRowLevelPermissions($sql,array());

		$sale_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($sale_orders as $sale_order) {

        	array_push($data, $this->convertSaleOrder($sale_order));
        }

		return $data;

	}

	public function getCanceledSaleOrders(){
		
		$sql = $this->statements["SELECT"]. " AND `sale_order_status` = 'Cancelada' ";
		
		$result = $this->checkRowLevelPermissions($sql,array());

		$sale_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($sale_orders as $sale_order) {

        	array_push($data, $this->convertSaleOrder($sale_order));
        }

		return $data;

	}

	public function getCompletedSaleOrders(){
		
		$sql = $this->statements["SELECT"]. " AND `sale_order_status` = 'Aprobada' 
			AND `sale_order_id` in ( 
				SELECT q1.sale_order_id FROM ( 
					SELECT s.sale_order_id, s.sale_order_total, SUM(p.sale_order_payment_amount) AS sale_order_payments_total,
						  (s.sale_order_total - sum(p.sale_order_payment_amount)) AS sale_order_payments_remaining
					FROM erp_sale_orders s 
					LEFT OUTER JOIN erp_sale_order_payments p ON p.sale_order_id = s.sale_order_id 
					LEFT OUTER JOIN erp_customers c ON c.customer_id = s.customer_id
					WHERE s.sale_order_status = 'Aprobada'
					GROUP BY s.sale_order_id
					HAVING sum(p.sale_order_payment_amount) >= s.sale_order_total 
					ORDER BY s.sale_order_date desc
      			) AS q1
      			
      		)";
		
		$result = $this->checkRowLevelPermissions($sql,array());



		$sale_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($sale_orders as $sale_order) {

        	array_push($data, $this->convertSaleOrder($sale_order));
        }

		return $data;

	}

	public function getSaleOrder($SaleOrderId){
		
		$sql = $this->statements["SELECT"]. " AND `sale_order_id` = :SaleOrderId ";
		
		$result = $this->checkRowLevelPermissions($sql,array(':SaleOrderId' => $SaleOrderId ));

		$sale_order=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertSaleOrder($sale_order,true);

	}

	public function getSaleOrderByQuote($QuoteId){
		
		$sql = $this->statements["SELECT"]. " AND `quote_id` = :QuoteId ";
		
		$result = $this->checkRowLevelPermissions($sql,array(':QuoteId' => $QuoteId ));
		

		$sale_order=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertSaleOrder($sale_order,true);

	}

	public function getSaleOrderByTransactionId($TransactionId){


		$sql = $this->statements["SELECT"]. " transaction_id = :TransactionId ";
		
		$result = $this->checkRowLevelPermissions($sql,array(':TransactionId' => $TransactionId ));

		$sale_order=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertSaleOrder($sale_order,true);

	}

	public function createSaleOrder($sale_order){

 		try{

 				$this->db->conn->beginTransaction();

				$sql = " 
					INSERT INTO `erp_sale_orders`(
						`sale_order_id`, 
						`quote_id`,
						`sale_order_folio`, 
						`sale_order_title`, 
						`sale_order_description`,
						`sale_order_date`, 
						`customer_id`, 
						`sale_order_status`, 
        				`sale_order_subtotal`, 
        				`sale_order_taxes`, 
        				`sale_order_discounts`, 
        				`sale_order_total`, 
        				`sale_order_total_dlls`,
        					`sale_order_terms`, 
						`sale_order_cdate`,
						`user_id`,
						`sale_order_exchange_rate`,
						`place_id`,
						`currency_id`,
						`contract_id`,
						`sale_order_delivery`,
						`sale_order_delivery_date`,
						`sale_order_type`,
        				`transaction_id`,
        				`created_by`,
        				sale_order_payment_method, 
        				sale_order_payment_form , 
        				sale_order_proof_type
					) VALUES (
						:SaleOrderId,
						:QuoteId,
						:SaleOrderFolio,
						:SaleOrderTitle,
						:SaleOrderDescription,
						:SaleOrderDate,
						:CustomerId,
						:SaleOrderStatus,
						:SaleOrderSubtotal, 
        					:SaleOrderTaxes, 
        					:SaleOrderDiscounts, 
        					:SaleOrderTotal, 
        				:SaleOrderTotalDlls,
        				:SaleOrderTerms,
						NOW(),
						:UserId,
						:SaleOrderExchangeRate,
						:PlaceId,
						:CurrencyId,
						:ContractId,
						:SaleOrderDelivery,
						:SaleOrderDeliveryDate,
						:SaleOrderType,
        				:TransactionId,
        				:UserId,
        				:SaleOrderPaymentMethod, 
        				:SaleOrderPaymentForm , 
        				:SaleOrderProofType
					)";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($sale_order));
		
				$sale_order->SaleOrderId = (int) $this->db->conn->lastInsertId();
				$sale_order->SaleOrderFolio = 'SO'.$sale_order->SaleOrderId;

				if( isset($sale_order->items) ){

  					$this->createSaleOrderDetails($sale_order->SaleOrderId, $sale_order->items);

				}

				if(isset($sale_order->SaleOrderCash)){

					if($sale_order->SaleOrderType == "POS" || $sale_order->SaleOrderType == "Online"){
						
						$payment = new stdClass();
						$payment->SaleOrderId = $sale_order->SaleOrderId;
						$payment->SaleOrderPaymentText = "Punto de Venta";
						$payment->SaleOrderPaymentDate = date("Y-m-d H:i:s");
						$payment->SaleOrderPaymentMethod = 0;
						$payment->SaleOrderPaymentAmount = $sale_order->SaleOrderTotal;

						$payment = $this->createOrUpdateSaleOrderPayment($payment);
					}
				}

				$this->db->conn->commit();

				return $sale_order;

		}catch(Exception $ex){
			
			$this->db->conn->rollback();
			
			throw new Exception($ex->getMessage());
		}
	}

	public function updateSaleOrder($sale_order){

		try{

 				$this->db->conn->beginTransaction();

				$sql = " 
					UPDATE `erp_sale_orders` SET 
						`quote_id` = :QuoteId,
						`sale_order_folio` = :SaleOrderFolio, 
						`sale_order_title`  = :SaleOrderTitle,
						`sale_order_description`  = :SaleOrderDescription,
						`sale_order_date` = :SaleOrderDate,
						`customer_id` = :CustomerId,
						`sale_order_status` = :SaleOrderStatus,
						`sale_order_subtotal` = :SaleOrderSubtotal, 
        				`sale_order_taxes` = :SaleOrderTaxes, 
        				`sale_order_discounts` = :SaleOrderDiscounts, 
        				`sale_order_total` = :SaleOrderTotal, 
        				`sale_order_total_dlls` = :SaleOrderTotalDlls,
        				`sale_order_terms` = :SaleOrderTerms,
						`user_id` = :UserId,
						`sale_order_lmdate` = NOW(),
						`sale_order_exchange_rate` = :SaleOrderExchangeRate,
						`place_id` = :PlaceId,
						`currency_id`=:CurrencyId,
						`contract_id`=:ContractId,
						`sale_order_delivery` = :SaleOrderDelivery,
						`sale_order_delivery_date` = :SaleOrderDeliveryDate,
						`sale_order_type` = :SaleOrderType,
						`transaction_id` = :TransactionId,
						`sale_order_payment_method` = :SaleOrderPaymentMethod, 
        					`sale_order_payment_form` = :SaleOrderPaymentForm , 
        					`sale_order_proof_type` = :SaleOrderProofType	
					WHERE `sale_order_id` = :SaleOrderId 
				";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($sale_order));
		
				//$sale_order->SaleOrderId = (int)$this->db->conn->lastInsertId();

				if(isset($sale_order->items)){
  					$this->createSaleOrderDetails($sale_order->SaleOrderId,$sale_order->items);
				}

				if(isset($sale_order->deleted_items)){
  					$this->deleteSaleOrderDetails($sale_order->SaleOrderId,$sale_order->deleted_items);
				}

				$this->db->conn->commit();

				return $sale_order;

		}catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}

	}

	public function deleteSaleOrder( $SaleOrderId ){

		$this->db->conn->beginTransaction();

		
		
		$sql = " 
			UPDATE `erp_sale_orders` SET `sale_order_deleted` = 1
			WHERE sale_order_id=:SaleOrderId
		" ;

		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':SaleOrderId' => $SaleOrderId ));

		$this->db->conn->commit();
	
	}

	public function approveSaleOrder($sale_order){

		$sale_order->SaleOrderStatus = "Aprobada";

		$sql = " 
			UPDATE `erp_sale_orders` 
			SET `sale_order_status` = :SaleOrderStatus
			WHERE sale_order_id= :SaleOrderId
		" ;
			
		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':SaleOrderId' => $sale_order->SaleOrderId , ':SaleOrderStatus' => $sale_order->SaleOrderStatus , ));

		return $sale_order;

	}

	public function completeSaleOrder($sale_order){


 		if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();

		$sale_order->SaleOrderStatus = "Completada";

		$sql = " 
			UPDATE `erp_sale_orders` 
			SET `sale_order_status` = :SaleOrderStatus
			WHERE sale_order_id= :SaleOrderId
		" ;
			
		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':SaleOrderId' => $sale_order->SaleOrderId , ':SaleOrderStatus' => $sale_order->SaleOrderStatus , ));


		if(isset($sale_order->items)){
  			$this->createOutMovement($sale_order);
		}


		if($this->db->conn->inTransaction()) $this->db->conn->commit();

		return $sale_order;

	}
	

	public function shippingSaleOrder($sale_order){



		$sql = " 
			UPDATE erp_sale_orders 
			SET sale_order_shipping = :sale_order_shipping,
			sale_order_carrier = :sale_order_carrier
			
			WHERE sale_order_id= :sale_order_id
		" ;
			
		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':sale_order_id' => $sale_order->SaleOrderId , ':sale_order_shipping' => $sale_order->SaleOrderShipping , ':sale_order_carrier' => $sale_order->SaleOrderCarrier));

		return $sale_order;

	}

	public function createOutMovement($sale_order){

		try{


		$sql_insert = "INSERT INTO `erp_movements`(
				`purchase_order_id`,
				`sale_order_id`, 
				`product_id`, 
				`um_id`,
				`movement_um`,
				`movement_qty`, 
				`movement_cdate`, 
				`user_id`, 
				`movement_type`, 
				`movement_active`
			) 
			VALUES (
				0,
				:SaleOrderId,
				:ProductId,
				0,
				:SaleOrderDetailUM,
				:SaleOrderDetailQTY,
				NOW(),
				:UserId,
				2,
				1
			)";

		foreach ($sale_order->items as $sale_order_item){
            
          	if(isset($sale_order_item->Product->ProductId)){   
			    $sql = $sql_insert;

    			
    		 
    			$parameters = array(
    			                ':SaleOrderId' => $purchase_order->SaleOrderId,
    			                ':ProductId'=>$sale_order_item->Product->ProductId,
    			                ':SaleOrderDetailQTY'=> $sale_order_item->SaleOrderDetailQTY,
    			                ':SaleOrderDetailUM' => $sale_order_item->SaleOrderDetailUM,
    			                ':UserId' => getOwnerId()
    			                );
    			                
    			$statement = $this->db->conn->prepare($sql);
    			$statement->setFetchMode(PDO::FETCH_OBJ);
    			$statement->execute($parameters);
          	}
		}
		}catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}	
	}

	public function cancelSaleOrder($sale_order){

		$sale_order->SaleOrderStatus = "Cancelada";

		$sql = " 
			UPDATE `erp_sale_orders` 
			SET `sale_order_status` = :SaleOrderStatus
			WHERE sale_order_id= :SaleOrderId
		" ;
			
		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':SaleOrderId' => $sale_order->SaleOrderId , ':SaleOrderStatus' => $sale_order->SaleOrderStatus ));

		return $sale_order;
	}

	public function printSaleOrderContract($sale_order){

		$sale_order->ContractPrinted = true;

		$sql = " 
			UPDATE `erp_sale_orders` 
			SET `contract_printed` = :ContractPrinted
			WHERE sale_order_id= :SaleOrderId
		" ;
			
		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':SaleOrderId' => $sale_order->SaleOrderId , ':ContractPrinted' => $sale_order->ContractPrinted ));

		return $sale_order;
	}


	public function createSaleOrderDetails($sale_order_id,$sale_order_items){

		$sql_insert = "
			INSERT INTO `erp_sale_order_details`(
				`sale_order_id`, 
				`sale_order_detail_id`, 
				`sale_order_detail_description`, 
				`product_id`, 
				`sale_order_detail_qty`, 
				`sale_order_detail_um`, 
				`sale_order_detail_price`,
				`sale_order_detail_discount`,
				`sale_order_detail_tax`,
				`sale_order_detail_total`
			) 
			VALUES (
				:SaleOrderId,
				:SaleOrderDetailId,
				:SaleOrderDetailDescription,
				:ProductId,
				:SaleOrderDetailQTY,
				:SaleOrderDetailUM,
				:SaleOrderDetailPrice,
				:SaleOrderDetailDiscount,
				:SaleOrderDetailTax,
				:SaleOrderDetailTotal
			)";

		$sql_update = "
			UPDATE `erp_sale_order_details` SET
						
				`sale_order_detail_description`=:SaleOrderDetailDescription,
				`product_id` =:ProductId, 
				`sale_order_detail_qty`=:SaleOrderDetailQTY, 
				`sale_order_detail_um`=:SaleOrderDetailUM, 
				`sale_order_detail_price`=:SaleOrderDetailPrice,
				`sale_order_detail_discount`=:SaleOrderDetailDiscount,
				`sale_order_detail_tax` =:SaleOrderDetailTax,
				`sale_order_detail_total` = :SaleOrderDetailTotal
				WHERE `sale_order_id` = :SaleOrderId and `sale_order_detail_id` = :SaleOrderDetailId
			";


			foreach ($sale_order_items as $sale_order_item){

				$sql = $sql_insert;

				if(isset($sale_order_item->SaleOrderDetailId)){ 
					$sql = $sql_update; 
				}
				else{
					$sale_order_item->SaleOrderDetailId = $this->getSaleOrderDetailIdentity($sale_order_id);
				}

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillDetailParams($sale_order_id,$sale_order_item));

				
				
			}		
	}

	
	public function deleteSaleOrderDetails($sale_order_id,$sale_order_deleted_items){

		$sql = "
			DELETE FROM `erp_sale_order_details` 
			WHERE `sale_order_id` = :SaleOrderId and `sale_order_detail_id` = :SaleOrderDetailId
		";



		foreach ($sale_order_deleted_items as $sale_order_item){

			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute(array(':SaleOrderId' => $sale_order_id, ':SaleOrderDetailId' => $sale_order_item->SaleOrderDetailId));
							
		}		
	}

	private function getSaleOrderDetails($sale_order_id){
		$result = $this->db->query("
			SELECT  
				`sale_order_id`, 
				`sale_order_detail_id`, 
				`sale_order_detail_description`, 
				`product_id`, 
				`sale_order_detail_qty`, 
				`sale_order_detail_um`, 
				`sale_order_detail_price`,
				`sale_order_detail_discount`,
				`sale_order_detail_tax`,
				`sale_order_detail_total`
			FROM `erp_sale_order_details`
			WHERE `sale_order_id` = :SaleOrderId"
			, array( ':SaleOrderId' => $sale_order_id));

			$sale_order_details = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($sale_order_details as $sale_order_detail) {

        	array_push($data, $this->convertSaleOrderDetail($sale_order_detail));
        }

		return $data;


	}

	private function getSaleOrderDetailIdentity($sale_order_id){
		

        $sale_order_detail_id=1;

		$sql = "
			SELECT  MAX(`sale_order_detail_id`)+1 as identity FROM `erp_sale_order_details` WHERE `sale_order_id` = :SaleOrderId
		";

		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':SaleOrderId' => $sale_order_id));


 		$result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->identity)){
		 	$sale_order_detail_id = $result->identity;
		 }

		
		return $sale_order_detail_id;	
			
    }

    public function createWorkOrder($SaleOrderId){

    	$SaleOrder= $this->getSaleOrder($SaleOrderId);

    	//var_dump($SaleOrderId);
    	$WorkOrder = new stdClass();
    	//$WorkOrder->WorkOrderId = NULL;
    	//$WorkOrder->WorkOrderNumber = NULL;
    	$WorkOrder->WorkOrderTitle = $SaleOrder["SaleOrderTitle"];
    	$WorkOrder->WorkOrderCreationDate = date("Y/m/d");
    	$WorkOrder->WorkOrderDeliveryDate = NULL;
    	$WorkOrder->WorkOrderSupervisor = NULL;
    	$WorkOrder->WorkOrderStatus = "Nueva";
        $WorkOrder->WorkOrderPriority = NULL;
        $WorkOrder->WorkOrderNotes = "" ; //$SaleOrder->SaleOrderDescription;
        $WorkOrder->SaleOrderId = $SaleOrder["SaleOrderId"];
        $WorkOrder->TransactionId = $SaleOrder["TransactionId"];
    	$WorkOrderModel = new WorkOrderModel();
    	$WorkOrder = $WorkOrderModel->createWorkOrder($WorkOrder);

    	return $WorkOrder;

    }

	private function fillParams($sale_order){

		$params = [];


		if(isset($sale_order->SaleOrderId)){  $params[':SaleOrderId'] = $sale_order->SaleOrderId; } else {  $params[':SaleOrderId'] = NULL; };
		if(isset($sale_order->QuoteId)){  $params[':QuoteId'] = $sale_order->QuoteId; } else {  $params[':QuoteId'] = NULL; };
		if(isset($sale_order->SaleOrderFolio)){   $params[':SaleOrderFolio'] = $sale_order->SaleOrderFolio; } else {    $params[':SaleOrderFolio'] = NULL; };
		if(isset($sale_order->SaleOrderTitle)){   $params[':SaleOrderTitle'] = $sale_order->SaleOrderTitle; } else {   $params[':SaleOrderTitle'] = NULL; };
		if(isset($sale_order->SaleOrderDescription)){   $params[':SaleOrderDescription'] = $sale_order->SaleOrderDescription; } else {   $params[':SaleOrderDescription'] = NULL; };
		if(isset($sale_order->SaleOrderDate)){   $params[':SaleOrderDate'] = $sale_order->SaleOrderDate; } else {   $params[':SaleOrderDate'] = NULL; };
		if(isset($sale_order->CustomerId)){   $params[':CustomerId'] = $sale_order->CustomerId; } else {   $params[':CustomerId'] = NULL; };
		if(isset($sale_order->SaleOrderStatus)){   $params[':SaleOrderStatus'] = $sale_order->SaleOrderStatus; } else {   $params[':SaleOrderStatus'] = NULL;};
		if(isset($sale_order->SaleOrderSubtotal)){   $params[':SaleOrderSubtotal'] = $sale_order->SaleOrderSubtotal; } else {   $params[':SaleOrderSubtotal'] = NULL;};
		if(isset($sale_order->SaleOrderDiscounts)){   $params[':SaleOrderDiscounts'] = $sale_order->SaleOrderDiscounts; } else {   $params[':SaleOrderDiscounts'] = NULL;};
		if(isset($sale_order->SaleOrderTaxes)){   $params[':SaleOrderTaxes'] = $sale_order->SaleOrderTaxes; } else {   $params[':SaleOrderTaxes'] = NULL;};
		if(isset($sale_order->SaleOrderTotal)){   $params[':SaleOrderTotal'] = $sale_order->SaleOrderTotal; } else {   $params[':SaleOrderTotal'] = NULL;};
		if(isset($sale_order->SaleOrderTotalDlls)){   $params[':SaleOrderTotalDlls'] = $sale_order->SaleOrderTotalDlls; } else {   $params[':SaleOrderTotalDlls'] = NULL;};
		if(isset($sale_order->SaleOrderExchangeRate)){   $params[':SaleOrderExchangeRate'] = $sale_order->SaleOrderExchangeRate; } else {   $params[':SaleOrderExchangeRate'] = NULL;};
		if(isset($sale_order->SaleOrderTerms)){   $params[':SaleOrderTerms'] = $sale_order->SaleOrderTerms; } else {   $params[':SaleOrderTerms'] = NULL;};
	        
	        if(isset($sale_order->CurrencyId)){   $params[':CurrencyId'] = $sale_order->CurrencyId; } else {   $params[':CurrencyId'] = NULL;}; 
	        if(isset($sale_order->ContractId)){   $params[':ContractId'] = $sale_order->ContractId; } else {   $params[':ContractId'] = NULL;}; 
	        if(isset($sale_order->SaleOrderDelivery)){   $params[':SaleOrderDelivery'] = $sale_order->SaleOrderDelivery; } else {   $params[':SaleOrderDelivery'] = NULL;}; 
	        if(isset($sale_order->SaleOrderDeliveryDate)){   $params[':SaleOrderDeliveryDate'] = $sale_order->SaleOrderDeliveryDate; } else {   $params[':SaleOrderDeliveryDate'] = NULL;}; 
	        if(isset($sale_order->SaleOrderType)){   $params[':SaleOrderType'] = $sale_order->SaleOrderType; } else {   $params[':SaleOrderType'] = "Normal";}; 
	        if(isset($sale_order->TransactionId)){   $params[':TransactionId'] = $sale_order->TransactionId; } else {   $params[':TransactionId'] = uniqid();}; 
	        if(isset($sale_order->SaleOrderPaymentForm)){   $params[':SaleOrderPaymentForm'] = $sale_order->SaleOrderPaymentForm; } else {   $params[':SaleOrderPaymentForm'] = "NULL" ;}; 
	        if(isset($sale_order->SaleOrderPaymentMethod)){   $params[':SaleOrderPaymentMethod'] = $sale_order->SaleOrderPaymentMethod; } else {   $params[':SaleOrderPaymentMethod'] = "NULL";}; 
	        if(isset($sale_order->SaleOrderProofType)){   $params[':SaleOrderProofType'] = $sale_order->SaleOrderProofType; } else {   $params[':SaleOrderProofType'] = "NULL";}; 
	        
	        if(isset($sale_order->PlaceId)){   $params[':PlaceId'] = $sale_order->PlaceId; } else {   
			
			$UserModel = new UserModel();
			$user = $UserModel->getUser(getOwnerId());
			
			if( $user->place_id > 0){ $params[":PlaceId"] = $user->place_id; } else {$params[':PlaceId'] = NULL;}
			
		};
	        	

        $params[':UserId']  = getOwnerId();
		return $params;
	}

	private function fillDetailParams($sale_order_id , $sale_order_detail){

		$params=[];
		$params[':SaleOrderId'] = $sale_order_id ;
		if(isset($sale_order_detail->SaleOrderDetailId)){   $params[':SaleOrderDetailId'] = $sale_order_detail->SaleOrderDetailId ; } else { $params[':SaleOrderDetailId'] = NULL ; }
		
		if(isset($sale_order_detail->SaleOrderDetailDescription)){   $params[':SaleOrderDetailDescription'] = $sale_order_detail->SaleOrderDetailDescription ; } else { $params[':SaleOrderDetailDescription'] = NULL ; }


		if(isset($sale_order_detail->SaleOrderDetailQTY)){   $params[':SaleOrderDetailQTY'] = $sale_order_detail->SaleOrderDetailQTY ; } else { $params[':SaleOrderDetailQTY'] = NULL ; }
		if(isset($sale_order_detail->SaleOrderDetailUM)){   $params[':SaleOrderDetailUM'] = $sale_order_detail->SaleOrderDetailUM ; } else { $params[':SaleOrderDetailUM'] = NULL ; }
		if(isset($sale_order_detail->SaleOrderDetailDiscount)){   $params[':SaleOrderDetailDiscount'] = $sale_order_detail->SaleOrderDetailDiscount ; } else { $params[':SaleOrderDetailDiscount'] = NULL ; }
		if(isset($sale_order_detail->SaleOrderDetailTax)){   $params[':SaleOrderDetailTax'] = $sale_order_detail->SaleOrderDetailTax ; } else { $params[':SaleOrderDetailTax'] = NULL ; }
		if(isset($sale_order_detail->SaleOrderDetailPrice)){   $params[':SaleOrderDetailPrice'] = $sale_order_detail->SaleOrderDetailPrice ; } else { $params[':SaleOrderDetailPrice'] = NULL ; }
		if(isset($sale_order_detail->SaleOrderDetailTotal)){   $params[':SaleOrderDetailTotal'] = $sale_order_detail->SaleOrderDetailTotal ; } else { $params[':SaleOrderDetailTotal'] = NULL ; }

		if(isset($sale_order_detail->Product->ProductId)){   

			$params[':ProductId'] = $sale_order_detail->Product->ProductId ; 

		} else { 
			$params[':ProductId'] = NULL ; 

			if(isset($sale_order_detail->Product) && is_string($sale_order_detail->Product)){

				 $params[':SaleOrderDetailDescription'] = $sale_order_detail->Product ; 

			}
			else{
				if(isset($sale_order_detail->ProductId)){
					$params[':ProductId'] = $sale_order_detail->ProductId ; 
				}
			}
		}


		return $params;
	}

    public function getSaleOrderPayments($SaleOrderId){
    	$result = $this->db->query(" 
			SELECT 	
				`sale_order_id`, 
				`sale_order_payment_id`,
				`sale_order_payment_receipt_number`,
				`sale_order_payment_text`,  
				`sale_order_payment_date`, 
				`sale_order_payment_amount`,
				`sale_order_payment_method`, 
				`sale_order_payment_cdate`, 
				`sale_order_payment_lmdate`, 
				`user_id` 
			FROM 
				`erp_sale_order_payments`
			WHERE
				`sale_order_id` = :SaleOrderId
			",
			array(':SaleOrderId' => $SaleOrderId)
		);

		$payments = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($payments as $payment) {

        	array_push($data, $this->convertSaleOrderPayment($payment));
        }

		return $data;
    }

    public function getSaleOrderPayment($SaleOrderId,$SaleOrderPaymentId){
    	$result = $this->db->query(" 
			SELECT 	
				`sale_order_id`, 
				`sale_order_payment_id`,
				`sale_order_payment_receipt_number`,
				`sale_order_payment_text`,  
				`sale_order_payment_date`, 
				`sale_order_payment_amount`, 
				`sale_order_payment_method`,
				`sale_order_payment_cdate`, 
				`sale_order_payment_lmdate`, 
				`user_id` 
			FROM 
				`erp_sale_order_payments`
			WHERE
				`sale_order_id` = :SaleOrderId
			AND
				`sale_order_payment_id` = :SaleOrderPaymentId
			",
			array(
				':SaleOrderId' => $SaleOrderId,
				':SaleOrderPaymentId' => $SaleOrderPaymentId
			)
		);

		$payment = $result->fetch(PDO::FETCH_OBJ);
        
     

        return  $this->convertSaleOrderPayment($payment);
  
    }

	public function createOrUpdateSaleOrderPayment($payment){

		$sql_insert = "
			INSERT INTO `erp_sale_order_payments`(
				`sale_order_id`, 
				`sale_order_payment_id`,
				`sale_order_payment_receipt_number`,
				`sale_order_payment_text`, 
				`sale_order_payment_date`, 
				`sale_order_payment_amount`,		
				`sale_order_payment_method`, 
				`sale_order_payment_cdate`, 
				`sale_order_payment_lmdate`, 
				`user_id`
			) VALUES (
				:SaleOrderId,
				:SaleOrderPaymentId,
				:SaleOrderPaymentReceiptNumber,
				:SaleOrderPaymentText,
				:SaleOrderPaymentDate,
				:SaleOrderPaymentAmount,
				:SaleOrderPaymentMethod,
				NOW(),
				NOW(),
				:UserId
			)
		";

		$sql_update = "
			UPDATE `erp_sale_order_payments` SET 
				`sale_order_payment_receipt_number` =:SaleOrderPaymentReceiptNumber,
			    `sale_order_payment_text`=:SaleOrderPaymentText, 
				`sale_order_payment_date`= :SaleOrderPaymentDate,
				`sale_order_payment_amount`= :SaleOrderPaymentAmount,
				`sale_order_payment_method`= :SaleOrderPaymentMethod,
				`sale_order_payment_lmdate`= NOW(),
				`user_id`=:UserId
			WHERE 
				`sale_order_id`= :SaleOrderId 
			AND 
				`sale_order_payment_id`= :SaleOrderPaymentId
		";

		if(isset($payment->SaleOrderPaymentId)){

			$sql = $sql_update;
		}
		else{
			$sql = $sql_insert;
			$payment->SaleOrderPaymentId = $this->getSaleOrderPaymentIdentity($payment->SaleOrderId);
			$FolioModel = new FolioModel();
			$FolioNumber = $FolioModel->getLastFolioByCode('RECEIPT');
			$FolioNumber = str_pad($FolioNumber, 7, "0", STR_PAD_LEFT);
			$payment->SaleOrderPaymentReceiptNumber = "A-".$FolioNumber;

		}
        //var_dump($payment);
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute($this->fillPaymentParams($payment));

		return $payment;
	}


	public function deleteSaleOrderPayment($payment){

		$sql = "
			DELETE FROM `erp_sale_order_payments` 
			WHERE 
				`sale_order_id`= :SaleOrderId 
			AND 
				`sale_order_payment_id`= :SaleOrderPaymentId
		";


		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':SaleOrderId' => $payment->SaleOrderId, ':SaleOrderPaymentId' => $payment->SaleOrderPaymentId));

	}

	public function getAllReceivableSalerOrders(){
		$sql = "
			SELECT 
				s.sale_order_id,
				s.sale_order_folio,
				s.sale_order_title,
				s.customer_id,
				c.customer_name, 
				s.sale_order_total,
				s.`currency_id`,
				s.sale_order_type,
				sum(p.sale_order_payment_amount) AS sale_order_payments_total,
				(s.sale_order_total - sum(p.sale_order_payment_amount)) AS sale_order_payments_remaining
			FROM erp_sale_orders s 
			LEFT OUTER JOIN erp_sale_order_payments p ON p.sale_order_id = s.sale_order_id 
			LEFT OUTER JOIN erp_customers c ON c.customer_id = s.customer_id
			WHERE s.sale_order_status = 'Aprobada'
			GROUP BY s.sale_order_id
			HAVING s.sale_order_total > sum(p.sale_order_payment_amount)
			ORDER BY s.sale_order_date desc
		";

		$results = $this->db->query($sql,array());

		$receivables = $results->fetchAll(PDO::FETCH_OBJ);
        
     	$data = [];

     	foreach ($receivables as $receivable) {
     		array_push($data, $this->convertReceivableSaleOrder($receivable));
     	}

        return  $data;


	}

	public function getAllSalerOrdersPayed(){
		$sql = "
			SELECT 
				s.sale_order_id,
				s.sale_order_folio,
				s.sale_order_title,
				s.customer_id,
				c.customer_name, 
				s.sale_order_total,
				s.currency_id,
				s.sale_order_type,
				sum(p.sale_order_payment_amount) AS sale_order_payments_total,
				(s.sale_order_total - sum(p.sale_order_payment_amount)) AS sale_order_payments_remaining
			FROM erp_sale_orders s 
			LEFT OUTER JOIN erp_sale_order_payments p ON p.sale_order_id = s.sale_order_id 
			LEFT OUTER JOIN erp_customers c ON c.customer_id = s.customer_id
			WHERE s.sale_order_status = 'Aprobada'
			GROUP BY s.sale_order_id
			HAVING sum(p.sale_order_payment_amount) >= s.sale_order_total 
			ORDER BY s.sale_order_date desc
		";

		$results = $this->db->query($sql,array());

		$receivables = $results->fetchAll(PDO::FETCH_OBJ);
        
     	$data = [];

     	foreach ($receivables as $receivable) {
     		array_push($data, $this->convertReceivableSaleOrder($receivable));
     	}

        return  $data;


	}

	private function convertReceivableSaleOrder($receivable)
	{

		$data = array(
			"SaleOrderId" => $receivable->sale_order_id,
			"SaleOrderTitle" => $receivable->sale_order_title, 
			"SaleOrderFolio" => "SO-".$receivable->sale_order_id, 
			"CustomerId" => $receivable->customer_id,
			"CustomerName" => $receivable->customer_name,
			"SaleOrderTotal" => $receivable->sale_order_total,
			"SaleOrderPaymentsTotal" => $receivable->sale_order_payments_total,
			"SaleOrderPaymentsRemaining" => $receivable->sale_order_payments_remaining
		);

		if(isset($receivable->currency_id)){
			
			$CurrencyModel = new CurrencyModel();
			$Currency = $CurrencyModel->getCurrency($receivable->currency_id);
			$data["CurrencyName"]= $Currency["CurrencyName"];
			$data["CurrencySymbol"]=$Currency["CurrencySymbol"];

		}
		
		return $data;

	}

	private function fillPaymentParams($payment){

		$params= [];
		$params[':SaleOrderId'] = $payment->SaleOrderId;
		$params[':SaleOrderPaymentId'] = $payment->SaleOrderPaymentId;

		if(isset($payment->SaleOrderPaymentText)){   $params[':SaleOrderPaymentText'] = $payment->SaleOrderPaymentText ; } else { $params[':SaleOrderPaymentText'] = NULL ; }
		if(isset($payment->SaleOrderPaymentDate)){   $params[':SaleOrderPaymentDate'] = $payment->SaleOrderPaymentDate ; } else { $params[':SaleOrderPaymentDate'] = NULL ; }
		if(isset($payment->SaleOrderPaymentAmount)){   $params[':SaleOrderPaymentAmount'] = $payment->SaleOrderPaymentAmount ; } else { $params[':SaleOrderPaymentAmount'] = NULL ; }
		if(isset($payment->SaleOrderPaymentMethod)){   $params[':SaleOrderPaymentMethod'] = $payment->SaleOrderPaymentMethod ; } else { $params[':SaleOrderPaymentMethod'] = NULL ; }
		if(isset($payment->SaleOrderPaymentReceiptNumber)){   $params[':SaleOrderPaymentReceiptNumber'] = $payment->SaleOrderPaymentReceiptNumber ; } else { $params[':SaleOrderPaymentReceiptNumber'] = NULL ; }

		$params[':UserId']  = getOwnerId();

		return $params;

	}

	private function getSaleOrderPaymentIdentity($SaleOrderId){

		$SaleOrderPaymentId=1;

		$sql = "
			SELECT  MAX(`sale_order_payment_id`)+1 as identity FROM `erp_sale_order_payments` WHERE `sale_order_id` = :SaleOrderId
		";

		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':SaleOrderId' => $SaleOrderId));


 		$result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->identity)){
		 	$SaleOrderPaymentId = $result->identity;
		 }

		
		return $SaleOrderPaymentId;
	}

	private function convertSaleOrderPayment($payment){
		$PaymentData = array(
			'SaleOrderId' => $payment->sale_order_id, 
			'SaleOrderPaymentId' => $payment->sale_order_payment_id,
			'SaleOrderPaymentReceiptNumber' => $payment->sale_order_payment_receipt_number,  
			'SaleOrderPaymentText' => $payment->sale_order_payment_text,
			'SaleOrderPaymentDate' => date('Y-m-d',strtotime($payment->sale_order_payment_date)), 
			'SaleOrderPaymentAmount' => $payment->sale_order_payment_amount, 
			'SaleOrderPaymentMethod' => $payment->sale_order_payment_method,
			'SaleOrderPaymentCDate' => $payment->sale_order_payment_cdate, 
			'SaleOrderPaymentLMDate' => $payment->sale_order_payment_lmdate, 
			`UserId` => $payment->user_id
		);


		if(isset($payment->user_id)){

			$UserModel = new UserModel();
			$user = $UserModel->getUser($payment->user_id);
			$PaymentData["UserName"] = $user->user_name;

		}

		return $PaymentData;
	}

	private function convertSaleOrder($sale_order,$detailed=false){


		$SaleOrderData = array(
			"SaleOrderId" => $sale_order->sale_order_id,
			"SaleOrderFolio" => 'SO-'.$sale_order->sale_order_id,
			"SaleOrderTitle" => $sale_order->sale_order_title,
			"SaleOrderDescription" => $sale_order->sale_order_description,
			"SaleOrderDate" => $sale_order->sale_order_date,
			"CustomerId" => $sale_order->customer_id,
			"SaleOrderStatus" => $sale_order->sale_order_status,
			"SaleOrderSubtotal" => $sale_order->sale_order_subtotal,
			"SaleOrderTaxes" => $sale_order->sale_order_taxes,
			"SaleOrderDiscounts" => $sale_order->sale_order_discounts,
			"SaleOrderTotal" => $sale_order->sale_order_total,
			"SaleOrderTotalDlls" => $sale_order->sale_order_total_dlls,
			"SaleOrderExchangeRate" => $sale_order->sale_order_exchange_rate,
			"SaleOrderTerms" => $sale_order->sale_order_terms,
			"CurrencyId" => $sale_order->currency_id,
			"SaleOrderCDate" => $sale_order->sale_order_cdate,
			"SaleOrderLMDate" => $sale_order->sale_order_lmdate,
			"UserId" => $sale_order->user_id,
			"QuoteId" => $sale_order->quote_id,
			"PlaceId" => $sale_order->place_id,
			"ContractId" => $sale_order->contract_id,
			"ContractPrinted" => $sale_order->contract_printed,
			"SaleOrderDelivery" => $sale_order->sale_order_delivery,
			"SaleOrderDeliveryDate" => $sale_order->sale_order_delivery_date,
			"SaleOrderType" => $sale_order->sale_order_type,
			"TransactionId" => $sale_order->transaction_id,
			"CreatedBy" => $sale_order->created_by,
			"InvoiceId" => $sale_order->invoice_id,
			"SaleOrderPaymentMethod" => $sale_order->sale_order_payment_method,
			"SaleOrderPaymentForm" => $sale_order->sale_order_payment_form,
			"SaleOrderProofType" => $sale_order->sale_order_proof_type,
			"InvoiceId" => $sale_order->invoice_id,
			"SaleOrderShipping" => $sale_order->sale_order_shipping,
			"SaleOrderCarrier" => $sale_order->sale_order_carrier
			
			


		);


		if(isset($sale_order->currency_id)){
			$CurrencyModel = new CurrencyModel();
			$Currency = $CurrencyModel->getCurrency($sale_order->currency_id);
			$SaleOrderData["CurrencyName"]= $Currency["CurrencyName"];
			$SaleOrderData["CurrencySymbol"]=$Currency["CurrencySymbol"];

		}

		if(isset($sale_order->customer_id)){
			$CustomerModel = new CustomerModel();
			$customer = $CustomerModel->getCustomer($sale_order->customer_id);
			$SaleOrderData["customer"] = $customer;
		}
        
		if($detailed) {
			
			$SaleOrderData["items"] = $this->getSaleOrderDetails($sale_order->sale_order_id);

			$WorkOrderModel = new WorkOrderModel();
	            	$WO = $WorkOrderModel->getWorkOrderBySale($sale_order->sale_order_id,false);
	            	
	            	if(isset($WO["WorkOrderId"])){
	            		$SaleOrderData["WorkOrderId"]=$WO["WorkOrderId"];
	            	}
	            		
	            		$CustomerModel = new CustomerModel();
				$customer = $CustomerModel->getCustomer($sale_order->customer_id);
				$SaleOrderData["customer"] = $customer;
            	
		}
		

		return $SaleOrderData ;
	}

	public function getSaleOrderByQuoteId($QuoteId,$detailed = true){

		$result = $this->db->query("
			SELECT 
				`sale_order_id`, 
	        	`sale_order_folio`, 
	        	`sale_order_title`, 
	        	`sale_order_date`, 
	        	`customer_id`, 
	        	`sale_order_status`, 
	        	`sale_order_subtotal`, 
        		`sale_order_taxes`, 
        		`sale_order_discounts`, 
        		`sale_order_total`, 
        		`sale_order_total_dlls`,
        		`sale_order_exchange_rate`, 
	        	`sale_order_cdate`, 
	        	`sale_order_terms`,
	        	`currency_id`, 
	        	`user_id`,
	        	`sale_order_lmdate`,
	        	`quote_id`,
	        	`place_id`,
	        	`contract_id`,
	        	`contract_printed`,
	        	`transaction_id`
			FROM `erp_sale_orders` 
			WHERE `quote_id` = :QuoteId
			AND `sale_order_status` = 'Aprobada'
			ORDER BY `sale_order_id` DESC
			LIMIT 1;
		",
			array(':QuoteId' => $QuoteId )
		);

		$sale_order=$result->fetch(PDO::FETCH_OBJ);
        //var_dump($sale_order);
		return $this->convertSaleOrder($sale_order,$detailed);


	}

	private function convertSaleOrderDetail($sale_order_detail){


		$SaleOrderDetailData = array(
			"SaleOrderDetailId" => $sale_order_detail->sale_order_detail_id,
			"SaleOrderDetailDescription" => $sale_order_detail->sale_order_detail_description,
			"ProductId" => $sale_order_detail->product_id,
			"SaleOrderDetailQTY" => $sale_order_detail->sale_order_detail_qty,
			"SaleOrderDetailUM" => $sale_order_detail->sale_order_detail_um,
			"SaleOrderDetailPrice" => $sale_order_detail->sale_order_detail_price,
			"SaleOrderDetailDiscount" => $sale_order_detail->sale_order_detail_discount,
			"SaleOrderDetailTax" => $sale_order_detail->sale_order_detail_tax,
			"SaleOrderDetailTotal" => $sale_order_detail->sale_order_detail_total
		);

		if(!isset($sale_order_detail->product_id)){
			$SaleOrderDetailData["Product"] =  $sale_order_detail->sale_order_detail_description;
		}
		else{
			$ProductModel = new ProductModel();
			$product = $ProductModel->getProduct($sale_order_detail->product_id);
			$SaleOrderDetailData["Product"] = $product;
		}

		return $SaleOrderDetailData ;
	}
	
	
	private function writeShellyXML(){
		
		
		
		$xmlString = '<?xml version="1.0" encoding="UTF-8"?>
<DATAPACKET Version="2.0">
   <METADATA>
      <FIELDS>
         <FIELD attrname="CVE_CLPV" fieldtype="string" WIDTH="10" />
         <FIELD attrname="NUM_ALMA" fieldtype="i4" />
         <FIELD attrname="CVE_PEDI" fieldtype="string" WIDTH="20" />
         <FIELD attrname="ESQUEMA" fieldtype="i4" />
         <FIELD attrname="DES_TOT" fieldtype="r8" />
         <FIELD attrname="DES_FIN" fieldtype="r8" />
         <FIELD attrname="CVE_VEND" fieldtype="string" WIDTH="5" />
         <FIELD attrname="COM_TOT" fieldtype="r8" />
         <FIELD attrname="NUM_MONED" fieldtype="i4" />
         <FIELD attrname="TIPCAMB" fieldtype="r8" />
         <FIELD attrname="STR_OBS" fieldtype="string" WIDTH="255" />
         <FIELD attrname="ENTREGA" fieldtype="string" WIDTH="25" />
         <FIELD attrname="SU_REFER" fieldtype="string" WIDTH="20" />
         <FIELD attrname="TOT_IND" fieldtype="r8" />
         <FIELD attrname="MODULO" fieldtype="string" WIDTH="4" />
         <FIELD attrname="CONDICION" fieldtype="string" WIDTH="25" />
         <FIELD attrname="dtfield" fieldtype="nested">
            <FIELDS>
               <FIELD attrname="CANT" fieldtype="r8" />
               <FIELD attrname="CVE_ART" fieldtype="string" WIDTH="20" />
               <FIELD attrname="DESC1" fieldtype="r8" />
               <FIELD attrname="DESC2" fieldtype="r8" />
               <FIELD attrname="DESC3" fieldtype="r8" />
               <FIELD attrname="IMPU1" fieldtype="r8" />
               <FIELD attrname="IMPU2" fieldtype="r8" />
               <FIELD attrname="IMPU3" fieldtype="r8" />
               <FIELD attrname="IMPU4" fieldtype="r8" />
               <FIELD attrname="COMI" fieldtype="r8" />
               <FIELD attrname="PREC" fieldtype="r8" />
               <FIELD attrname="NUM_ALM" fieldtype="i4" />
               <FIELD attrname="STR_OBS" fieldtype="string" WIDTH="255" />
               <FIELD attrname="REG_GPOPROD" fieldtype="i4" />
               <FIELD attrname="REG_KITPROD" fieldtype="i4" />
               <FIELD attrname="NUM_REG" fieldtype="i4" />
               <FIELD attrname="COSTO" fieldtype="r8" />
               <FIELD attrname="TIPO_PROD" fieldtype="string" WIDTH="1" />
               <FIELD attrname="TIPO_ELEM" fieldtype="string" WIDTH="1" />
               <FIELD attrname="MINDIRECTO" fieldtype="r8" />
               <FIELD attrname="TIP_CAM" fieldtype="r8" />
               <FIELD attrname="FACT_CONV" fieldtype="r8" />
               <FIELD attrname="UNI_VENTA" fieldtype="string" WIDTH="10" />
               <FIELD attrname="IMP1APLA" fieldtype="i4" />
               <FIELD attrname="IMP2APLA" fieldtype="i4" />
               <FIELD attrname="IMP3APLA" fieldtype="i4" />
               <FIELD attrname="IMP4APLA" fieldtype="i4" />
               <FIELD attrname="PREC_SINREDO" fieldtype="r8" />
               <FIELD attrname="COST_SINREDO" fieldtype="r8" />
               <FIELD attrname="LINK_FIELD" fieldtype="ui4" hidden="true" linkfield="true" />
            </FIELDS>
            <PARAMS CHANGE_LOG="1 0 4" />
         </FIELD>
      </FIELDS>
      <PARAMS CHANGE_LOG="1 0 4 1 1 64" />
   </METADATA>
   <ROWDATA>
      <ROW RowState="4" CVE_CLPV="         1" NUM_ALMA="1" CVE_PEDI="" ESQUEMA="1" DES_TOT="0" DES_FIN="0" CVE_VEND="    1" COM_TOT="0" NUM_MONED="1" TIPCAMB="1" STR_OBS="" MODULO="FACT" CONDICION="">
         <dtfield>
            <ROWdtfield RowState="4" CANT="4" CVE_ART="2262" DESC1="0" DESC2="0" DESC3="0" IMPU1="0" IMPU2="0" IMPU3="0" IMPU4="16" COMI="0" PREC="211.2068" NUM_ALM="1" STR_OBS="" REG_GPOPROD="0" COSTO="53.505" TIPO_PROD="P" TIPO_ELEM="N" TIP_CAM="1" UNI_VENTA="pz" IMP1APLA="0" IMP2APLA="0" IMP3APLA="0" IMP4APLA="1" PREC_SINREDO="211.2068" COST_SINREDO="53.505" LINK_FIELD="1" />
         </dtfield>
      </ROW>
   </ROWDATA>
</DATAPACKET>';
		
		$dom = new DOMDocument;
		$dom->preserveWhiteSpace = FALSE;
		$dom->loadXML($xmlString);
		
		//Save XML as a file
		$dom->save('xml/sitemap.xml');


	}
}
?>