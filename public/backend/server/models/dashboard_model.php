<?php
class DashboardModel {
	
	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	//Cotizaciones
	
	public function getCountQuotes($dateFrom = '',$dateTo=''){
            $result = $this->db->query("
			Select count(*)  as total
			from erp_quotes where DATE(quote_date) between :DateFrom and :DateTo
			"
			,
			array(':DateFrom' => $dateFrom, ':DateTo' => $dateTo)
		);

		$data = $result->fetch(PDO::FETCH_OBJ);
        
		return $data->total;

	}
	
	//Cotizaciones Por Estatus
	public function getCountStatusQuotes($dateFrom = '',$dateTo='',$statusQuote=''){
            $result = $this->db->query("
			Select count(*)  as total
			from erp_quotes where DATE(quote_date) between :DateFrom and :DateTo and quote_status= :StatusQuote
			"
			,
			array(':DateFrom' => $dateFrom, ':DateTo' => $dateTo, ':StatusQuote' => $statusQuote)
		);

		$data = $result->fetch(PDO::FETCH_OBJ);
        
		return $data->total;

	}
	
	
	
	//Ordenes de Venta
	
	public function getCountSales($dateFrom = '',$dateTo=''){
            $result = $this->db->query("
			Select count(*)  as total
			from erp_sale_orders where DATE(sale_order_date) between :DateFrom and :DateTo
			"
			,
			array(':DateFrom' => $dateFrom, ':DateTo' => $dateTo)
		);

		$data = $result->fetch(PDO::FETCH_OBJ);
        
		return  $data->total;

	}
	
	//Ventas Por Estatus
	public function getCountStatusSales($dateFrom = '',$dateTo='',$statusSale=''){
            $result = $this->db->query("
			Select count(*)  as total
			from erp_sale_orders where DATE(sale_order_date) between :DateFrom and :DateTo and sale_order_status= :StatusSale
			"
			,
			array(':DateFrom' => $dateFrom, ':DateTo' => $dateTo, ':StatusSale' => $statusSale)
		);

		$data = $result->fetch(PDO::FETCH_OBJ);
        
		return  $data->total;
	}
	
	//Ordenes de Trabajo
	public function getCountWorks($dateFrom = '',$dateTo=''){
            $result = $this->db->query("
			Select count(*)  as total
			from erp_work_orders where DATE(work_order_creation_date) between :DateFrom and :DateTo
			"
			,
			array(':DateFrom' => $dateFrom, ':DateTo' => $dateTo)
		);

		$data = $result->fetch(PDO::FETCH_OBJ);

		return  $data->total;

	}
	
	//Ordenes de Trabajo Por Estatus
	public function getCountStatusWorks($dateFrom = '',$dateTo='',$statusWork=''){
            $result = $this->db->query("
			Select count(*)  as total
			from erp_work_orders where DATE(work_order_creation_date) between :DateFrom and :DateTo and work_order_status= :StatusWork
			"
			,
			array(':DateFrom' => $dateFrom, ':DateTo' => $dateTo, ':StatusWork' => $statusWork)
		);

		$data = $result->fetch(PDO::FETCH_OBJ);
        
		return  $data->total;

	}
	
	
	
	//Pagos
	public function getCountPayments($dateFrom = '',$dateTo=''){
            $result = $this->db->query("
			Select Count(*) as total
			from erp_sale_order_payments where DATE(sale_order_payment_date) between :DateFrom and :DateTo
			"
			,
			array(':DateFrom' => $dateFrom, ':DateTo' => $dateTo)
		);

		$data = $result->fetch(PDO::FETCH_OBJ);
        
		return  $data->total ;

	}
	
    //Pagos En Pesos
		public function getAmountPaymentsMxn($dateFrom = '',$dateTo=''){
            $result = $this->db->query("
			Select Sum(a.sale_order_payment_amount) as total
			from erp_sale_order_payments a
            inner join erp_sale_orders b on a.sale_order_id = b.sale_order_id 
            where DATE(sale_order_payment_date) between :DateFrom and :DateTo and b.currency_id = 1
			"
			,
			array(':DateFrom' => $dateFrom, ':DateTo' => $dateTo)
		);

		$data = $result->fetch(PDO::FETCH_OBJ);
        
		return   $data->total ;
    }
	
	//Pagos En Dolares
		public function getAmountPaymentsDlls($dateFrom = '',$dateTo=''){
            $result = $this->db->query("
			Select Sum(a.sale_order_payment_amount) as total
			from erp_sale_order_payments a
            inner join erp_sale_orders b on a.sale_order_id = b.sale_order_id 
            where DATE(sale_order_payment_date) between :DateFrom and :DateTo and b.currency_id = 2
			"
			,
			array(':DateFrom' => $dateFrom, ':DateTo' => $dateTo)
		);

		$data = $result->fetch(PDO::FETCH_OBJ);
        
		return  $data->total;
    }
	
	
	//Monto de Ventas por Dia
		public function getAmountSales(){
            $result = $this->db->query("
			Select SUM(sale_order_total_dlls) as total from erp_sale_orders where DATE(sale_order_date) = DATE(Now()) 
			"
			,
			array(':DateFrom' => $dateFrom, ':DateTo' => $dateTo)
			
		);

		$data = $result->fetch(PDO::FETCH_OBJ);
        
		return $data->total;
    }
	
	
	//Pagos para grafica en Dolares
	public function getChartPaymentsDlls()
	{
	$result = $this->db->query("
			select DATE_FORMAT(sale_order_payment_date,'%M %Y') as payment_date, c.currency_name, SUM( a.sale_order_payment_amount) as payment_sum_amount  from erp_sale_order_payments a
            inner join erp_sale_orders b on a.sale_order_id = b.sale_order_id 
            inner join erp_currencies c on b.currency_id = c.currency_id 
			where b.currency_id=2
            group by DATE_FORMAT(sale_order_payment_date,'%M %Y'), c.currency_name
            order by sale_order_payment_date DESC
			"
			,
			array()
		);

		$payments = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($payments as $payment) {

        	array_push($data, $this->convertPayments($payment));
        }

		return $data;
	
	}
	
	//Pagos para grafica en Pesos
	public function getChartPayments($year=0)
	{


		$year_months_sql = "
			SELECT DISTINCT MONTH( sale_order_payment_date ) AS 
			month , MONTHNAME( sale_order_payment_date ) AS month_name, YEAR( sale_order_payment_date ) AS year
			FROM erp_sale_order_payments
			WHERE YEAR( sale_order_payment_date ) =:Year
			GROUP BY MONTH( sale_order_payment_date ) , MONTHNAME( sale_order_payment_date ) , YEAR( sale_order_payment_date ) 
			ORDER BY YEAR( sale_order_payment_date ) , MONTH( sale_order_payment_date ) 
		";


		$year_months_result = $this->db->query($year_months_sql,
			array(':Year' => $year)
		);

		$year_months_data = $year_months_result->fetchAll(PDO::FETCH_OBJ);

		$payments_data=array();


		foreach ($year_months_data as $year_month) {		

			$sql_mxn="
				SELECT SUM( sop.sale_order_payment_amount ) AS total
				FROM erp_sale_orders so
				INNER JOIN erp_sale_order_payments sop ON sop.sale_order_id = so.sale_order_id
				WHERE so.currency_id =1
				AND MONTH( sop.sale_order_payment_date ) =:month
				AND YEAR( sop.sale_order_payment_date ) =:year
				GROUP BY MONTH( sop.sale_order_payment_date ) , YEAR( sop.sale_order_payment_date ) 
            ";

            $mxn_result = $this->db->query($sql_mxn,
				array(':month' => $year_month->month , ':year' => $year_month->year)
			);

            $mxn_data = $mxn_result->fetch(PDO::FETCH_OBJ);

            $sql_dlls="
				SELECT SUM( sop.sale_order_payment_amount ) AS total
				FROM erp_sale_orders so
				INNER JOIN erp_sale_order_payments sop ON sop.sale_order_id = so.sale_order_id
				WHERE so.currency_id =2
				AND MONTH( sop.sale_order_payment_date ) =:month
				AND YEAR( sop.sale_order_payment_date ) =:year
				GROUP BY MONTH( sop.sale_order_payment_date ) , YEAR( sop.sale_order_payment_date ) 
            ";

            $dlls_result = $this->db->query($sql_dlls,
				array(':month' => $year_month->month , ':year' => $year_month->year)
			);

			$dlls_data = $dlls_result->fetch(PDO::FETCH_OBJ);

			$mxn_total =  isset($mxn_data->total) ? $mxn_data->total : 0; 
			$dlls_total = isset($dlls_data->total) ? $dlls_data->total :  0;

			//array_push($payments_data["months"],  $year_month->month_name.' ' . $year_month->year );
			//array_push($payments_data["mxn"],  $mxn_total);
			//array_push($payments_data["dlls"],  $dlls_total);

			$data = array('y' => $year_month->year.'-'.str_pad($year_month->month, 2, '0', STR_PAD_LEFT) , 'a' => $mxn_total, 'b' => $dlls_total );

			array_push($payments_data,  $data);
		}



		return $payments_data;
	
	}
	

	
	//Ventas para grafica
	public function getChartSaleOrders( $year=0)	
	{

		$year_months_sql = "
			SELECT DISTINCT 
				MONTH( sale_order_date ) AS month , 
				MONTHNAME( sale_order_date ) AS month_name, 
				YEAR( sale_order_date ) AS year
			FROM erp_sale_orders
			WHERE YEAR(sale_order_date) = :Year
			GROUP BY MONTH( sale_order_date ) , MONTHNAME( sale_order_date ) , YEAR( sale_order_date ) 
			ORDER BY YEAR( sale_order_date ) , MONTH( sale_order_date ) ";


		$year_months_result = $this->db->query($year_months_sql,
			array(':Year' => $year)
		);

		$year_months_data = $year_months_result->fetchAll(PDO::FETCH_OBJ);

		$sales_data=array(     );


		foreach ($year_months_data as $year_month) {			


			$sql_mxn="
				SELECT SUM( sale_order_total) as total
            	FROM erp_sale_orders
            	WHERE currency_id=1
            	AND MONTH(sale_order_date) = :month
            	AND YEAR(sale_order_date) = :year
            ";

            $mxn_result = $this->db->query($sql_mxn,
				array(':month' => $year_month->month , ':year' => $year_month->year)
			);

            $mxn_data = $mxn_result->fetch(PDO::FETCH_OBJ);

            $sql_dlls="
				SELECT SUM( sale_order_total) as total
            	FROM erp_sale_orders
            	WHERE currency_id=2
            	AND MONTH(sale_order_date) = :month
            	AND YEAR(sale_order_date) = :year
            ";

            $dlls_result = $this->db->query($sql_dlls,
				array(':month' => $year_month->month , ':year' => $year_month->year)
			);

			$dlls_data = $dlls_result->fetch(PDO::FETCH_OBJ);

			$mxn_total =  isset($mxn_data->total) ? $mxn_data->total : 0; 
			$dlls_total = isset($dlls_data->total) ? $dlls_data->total :  0;
            

			//array_push($sales_data["months"],  $year_month->month_name.' ' . $year_month->year );
			//array_push($sales_data["mxn"],  $mxn_total);
			//array_push($sales_data["dlls"],  $dlls_total);

			$data = array('y' => $year_month->year.'-'.str_pad($year_month->month, 2, '0', STR_PAD_LEFT) , 'a' => $mxn_total, 'b' => $dlls_total );

			array_push($sales_data,  $data);
		}



		return $sales_data;
	
	}
	
	
	
	
	

	
	private function convertPayments($payment){
	return array(
			'PaymentDate' => $payment->payment_date,
			'CurrencyName' => $payment->currency_name, 
			'PaymentSumAmount' => $payment->payment_sum_amount
    	);
	}
	
	private function convertSaleOrders($saleorder){
	return array(
			'SaleOrderDate' => $saleorder->sale_order_date,
			'CurrencyName' => $saleorder->currency_name, 
			'SaleOrderTotal' => $saleorder->sale_order_total
    	);
	}

	


	


	
}
?>