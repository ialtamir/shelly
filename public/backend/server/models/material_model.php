<?php
class MaterialModel {
	
	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function searchMaterials($query){

		$result = $this->db->query("
			SELECT          
			    `product_id` as `material_id`,
			    `product_name` as	`material_name`,
			    `category_id` as	`material_category_id`,
				`product_stock` as `material_stock`
			FROM `erp_products`
			WHERE product_name like :MaterialName and (item_id = 2)  "
			,
			array(':MaterialName' => $query.'%')
		);

		$materials = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($materials as $material) {

        	array_push($data, $this->convertMaterial($material));
        }

		return $data;

	}

	public function getMaterials(){

		$result = $this->db->query("
			SELECT          
			    `product_id` as `material_id`,
			    `product_name` as	`material_name`,
			    `category_id` as	`material_category_id`,
				`product_stock` as `material_stock`
			FROM `erp_products`
			WHERE item_id = 2
			"
			,
			array()
		);

		$materials = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($materials as $material) {

        	array_push($data, $this->convertMaterial($material));
        }

		return $data;

	}

	public function getMaterial($MaterialId){

		$result = $this->db->query("
			SELECT          
			    `product_id` as `material_id`,
			    `product_name` as	`material_name`,
			    `category_id` as	`material_category_id`,
				`product_stock` as `material_stock`
			FROM `erp_products`
			WHERE `product_id` = :MaterialId"
			,
			array( ':MaterialId' => $MaterialId )
		);

		$material=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertMaterial($material);

	}

	public function createMaterial($material){


		$this->db->query("
			INSERT INTO `erp_products` (
				`product_id`,
				`product_name`, 
				`category_id`,
				`product_stock`,
				`item_id`
			) 
			VALUES ( 
				:MaterialId ,
				:MaterialName, 
				:MaterialCategoryId,
                :MaterialStock,
                2				
			)",
			$this->fillParams($material)
		);	

		$material->MaterialId = $this->db->getInsertId();

		return $material;	
	}

	public function updateMaterial($material){
		
		$this->db->query("
			UPDATE `erp_products` SET 
				`product_name`=:MaterialName, 
				`category_id`=:MaterialCategoryId,
				`product_stock`=:MaterialStock
		WHERE `product_id` = :MaterialId",
		   $this->fillParams($material)
		 );
		
	}

	public function deleteMaterial($MaterialId){
		
		$this->db->query("
			DELETE FROM `erp_products` 
			WHERE `product_id` = :MaterialId",
		array(':MaterialId' => $MaterialId ));
		
	}

	private function fillParams($material) {

		$params = [];

		if(isset($material->MaterialId))	 { $params[':MaterialId'] = $material->MaterialId; } else { $params[':MaterialId'] = NULL; }; 
		if(isset($material->MaterialName))	 { $params[':MaterialName'] = $material->MaterialName; } else { $params[':MaterialName'] = NULL; }; 
		if(isset($material->MaterialCategoryId))	 { $params[':MaterialCategoryId'] = $material->MaterialCategoryId; } else { $params[':MaterialCategoryId'] = NULL; }; 
		if(isset($material->MaterialStock))	 { $params[':MaterialStock'] = $material->MaterialStock; } else { $params[':MaterialStock'] = NULL; }; 
		
		return $params;
			
	}

	private function convertMaterial($material)
	{
		return array(
			'MaterialId' => $material->material_id,
			'MaterialName' => $material->material_name, 
			'MaterialCategoryId' => $material->material_category_id,
			'MaterialStock' => $material->material_stock
    	);
	}


	//Material Categories
	public function getMaterialCategories(){
		$sql = "SELECT `material_category_id`, `material_category_name`, `material_category_show_in_products` FROM `erp_material_categories`";
	}

	public function getMaterialCategory($material_category){

	}

	public function createMaterialCategory($material_category){

	}

	public function updateMaterialCategory($material_category){

	}

	public function deleteMaterialCategory($material_category){

	}
}
?>