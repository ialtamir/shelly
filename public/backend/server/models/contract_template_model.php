<?php

/*
 * To change this template use Tools | Templates.
 */
class ContractTemplateModel {
    
   private $statements = array("SELECT" => "SELECT `contract_template_id`, `contract_template_name`, `contract_template_body` FROM `erp_contract_templates`",
                               "INSERT" => "INSERT INTO `erp_contract_templates`(`contract_template_id`, `contract_template_name`, `contract_template_body`) VALUES (:ContractTemplateId,:ContractTemplateName,:ContractTemplateBody)",
                               "UPDATE" => "UPDATE `erp_contract_templates` SET `contract_template_name`=:ContractTemplateName,`contract_template_body`=:ContractTemplateBody WHERE  `contract_template_id`=:ContractTemplateId",
                               "DELETE" => "DELETE FROM `erp_contract_templates` WHERE  `contract_template_id`=:ContractTemplateId"
                              );
   private $db;
   

   public function __construct(){

        $this->db = DB::withAccount(AccountModel::getAccountConfiguration());
   } 
    
   public function getContractTemplates(){
       
		$result = $this->db->query($this->statements["SELECT"],array());

		$contract_templates = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($contract_templates as $contract_template) {

        	array_push($data, $this->convert($contract_template,false));
        }

		return $data;

   }
    
   public function getContractTemplate($ContractTemplateId){
       
        $result = $this->db->query($this->statements["SELECT"],array( ':ContractTemplateId' => $ContractTemplateId ));

	    $contract_template=$result->fetch(PDO::FETCH_OBJ);

		return $this->convert($contract_template);

   }
   
   public function createContractTemplate($contract_template){
        
        $this->db->query($this->statements["INSERT"],$this->fillParams($contract_template));	

	    $contract_template->ContractTemplateId = $this->db->getInsertId();

		return $contract_template;	
   }
    
   public function updateContractTemplate($contract_template){
       
		$this->db->query($this->statements["UPDATE"],$this->fillParams($contract_template));	
       
       return $contract_template;
   }
    
   public function deleteContractTemplate($ContractTemplateId){
       
       $this->db->query($this->statements["DELETE"],array(':ContractTemplateId' => $ContractTemplateId ));
		
   }
    
   private function fillParams($contract_template){
       
       $params =[];
       $params[':ContractTemplateId']   = ( isset($contract_template->ContractTemplateId)   ?  $contract_template->ContractTemplateId : NULL );
       $params[':ContractTemplateName'] = ( isset($contract_template->ContractTemplateName) ?  $contract_template->ContractTemplateName : NULL );
       $params[':ContractTemplateBody'] = ( isset($contract_template->ContractTemplateBody) ?  $contract_template->ContractTemplateBody : NULL );
       
    	return $params;

   }
    
   private function convert($contract_template,$include_template_body = true){
       
       $data = [];
       $data["ContractTemplateId"] = $contract_template->contract_template_id;
       $data["ContractTemplateName"] = $contract_template->contract_template_name;
                                      
       
       if($include_body){
           $data[ "ContractTemplateBody"] = $contract_template->contract_template_body;
       }
       
       return $data;
   }
    
}

?>