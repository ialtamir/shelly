<?php
class PagoModel {
	
	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}
	
	public function PagoTimbrado($total,$token){
		
		
		Conekta::setApiKey("key_JqjFbwxbEdsShmCgJUxe5g");

		try{
			
			  //Calcular monto de acuerdo al paquete
			  $cobro  = 0;
			  $cantidad = 0;
			  switch ($total) {
				    case "PAQ50":
				        //echo "i es igual a 0";
				        $cobro=29000;
				        $cantidad=50;
				        break;
				    case "PAQ200":
				        //echo "i es igual a 1";
				        $cobro=104400;
				        $cantidad=200;
				        break;
				    case "PAQ500":
				        //echo "i es igual a 2";
				        $cobro=232000;
				        $cantidad=500;
				        break;
				    case "PAQ1000":
				        //echo "i es igual a 2";
				        $cobro=406000;
				        $cantidad=1000;
				        break;
				    default:
				    	throw new Exception("Paquete no encontrado");
				    	break;
				}

			  $Id=1;
			  
			  
			  $charge = Conekta_Charge::create(array(
			    "amount"=> $cobro, //51000,
			    "currency"=> "MXN",
			    "description"=> "Pago de Timbrado",
			    "reference_id"=> $Id,//"orden_de_id_interno",
			    "card"=> $token//$_POST['conektaTokenId']
			 //"tok_a4Ff0dD2xYZZq82d9"
			  ));
			  
			  
			}
			catch (Conekta_Error $e){
				
			  throw new Exception($e->getMessage());
			  
		
			}
			
		$res = $charge->status;
		
		if($res=='paid')
	    {
	    	 $AccountModel = new AccountModel(); 
	    	 $AccountModel->updateTimbresAccountAdd($cantidad);
	    }

	    return $res;

	}
	
}
?>