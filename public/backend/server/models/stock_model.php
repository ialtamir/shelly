<?php
class StockModel {
	
	private $db;
	
	private $statements = [];

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function createMovement($stock_movement){
		
		try {

 			if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();

			$sql = "INSERT INTO erp_stock_movements 
				( document_id, document_detail_id,  document_type,  place_id,  product_id,  um_id,  uom,  sign,  quantity,  user_id,  type ) VALUES 
				(:document_id,:document_detail_id, :document_type, :place_id, :product_id, :um_id, :uom, :sign, :quantity, :user_id, :type)";
			
			
			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			
			$params = [];
			$params[':document_id'] 		= ( isset($stock_movement->document_id) ? $stock_movement->document_id : NULL);
			$params[':document_detail_id']	= ( isset($stock_movement->document_detail_id) ? $stock_movement->document_detail_id : NULL);
			$params[':document_type']		= ( isset($stock_movement->document_type) ? $stock_movement->document_type : NULL);
			$params[':place_id']			= ( isset($stock_movement->place_id) ? $stock_movement->place_id : NULL);
			$params[':product_id']			= ( isset($stock_movement->product_id) ? $stock_movement->product_id : NULL);
			$params[':um_id'] 				= ( isset($stock_movement->um_id) ? $stock_movement->um_id : NULL);
			$params[':uom'] 				= ( isset($stock_movement->uom) ? $stock_movement->uom : NULL);
			$params[':sign']				= ( isset($stock_movement->sign) ? $stock_movement->sign : NULL);
			$params[':quantity']			= ( isset($stock_movement->quantity) ? $stock_movement->quantity : NULL);
			$params[':user_id']				= ( isset($stock_movement->user_id) ? $stock_movement->user_id : NULL);
			$params[':type']				= ( isset($stock_movement->type) ? $stock_movement->type : NULL);
			
			
			
			$statement->execute($params);
		
			$stock_movement->id = (int) $this->db->conn->lastInsertId();


			
			if($this->db->conn->inTransaction()) $this->db->conn->commit();

			return $stock_movement;


		
		} catch(Exception $ex){
			
			echo $ex;
			$this->db->conn->rollback();
			
		}

	
	}
	
	public function cancelStockMovement($id){
		
		
	}
	
	
	public function getStock(){

		$result = $this->db->query("
			SELECT movement_id, erp_products.product_id, erp_products.product_name, movement_um, SUM(movement_qty) as movement_qty 
			FROM erp_stock_movements LEFT OUTER JOIN erp_ums on erp_movements.um_id =  erp_ums.um_id INNER JOIN erp_products on erp_movements.product_id =  erp_products.product_id
			group by  erp_products.product_id,erp_ums.um_id

			"
			,
			array()
		);

		$stocks = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($stocks as $stock) {

        	array_push($data, $this->convertStock($stock));
        }

		return $data;

	}


	private function convertStock($stock)
	{
		return array(
			'MovementId' => $stock->movement_id,
			'ProductId' => $stock->product_id,
			'ProductName' => $stock->product_name,  
			'MovementQty' => $stock->movement_qty,
			'UmName' => $stock->movement_um
			
    	);
	}
}
?>