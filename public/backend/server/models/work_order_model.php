<?php
	class WorkOrderModel {

		private $db;
		
		private $statements = array(
		"SELECT" => "SELECT * FROM erp_work_orders",
		"SELECT_WITH_JOINS", "		SELECT 
					`work_order_id`, 
                    `work_order_number`, 
                    `erp_customers`.`customer_name`,
					`work_order_title`, 
					`work_order_creation_date`, 
					`work_order_delivery_date`, 
					`work_order_supervisor`, 
					`work_order_status`, 
					`work_order_priority`, 
					`work_order_notes`, 				
					`erp_work_orders`.`transaction_id`,										
					`erp_work_orders`.`sale_order_id`,
					`erp_work_orders`.`place_id`
				  FROM `erp_work_orders`
          INNER JOIN `erp_sale_orders` ON `erp_work_orders`.`sale_order_id` = `erp_sale_orders`.`sale_order_id`
          INNER JOIN `erp_customers` ON `erp_sale_orders`.`customer_id` = `erp_customers`.`customer_id` ",
		"DEFAULT_ORDER" => " ORDER BY `work_order_creation_date` DESC , `work_order_id` DESC"
	);

		public function __construct(){

			$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

		}

		public function getWorkOrders(){

			$sql = $this->statements["SELECT_WITH_JOINS"].$this->statements["DEFAULT_ORDER"];
			
			$result = $this->db->query($sql,array());

			$work_orders = $result->fetchAll(PDO::FETCH_OBJ);
	        
	        $data = [];

	        foreach ($work_orders as $work_order) {

	        	array_push($data, $this->convertWorkOrder($work_order));
	        }

			return $data;

		}

		public function getWorkOrdersByStatus($WorkOrderStatus){
			
			$sql = $this->statements["SELECT_WITH_JOINS"];
			$sql = $sql. " WHERE `work_order_status` = :WorkOrderStatus";
			$sql = $sql.$this->statements["DEFAULT_ORDER"];

			$result = $this->db->query($sql,array(':WorkOrderStatus' => $WorkOrderStatus));

			$work_orders = $result->fetchAll(PDO::FETCH_OBJ);
	        
	        $data = [];

	        foreach ($work_orders as $work_order) {

	        	array_push($data, $this->convertWorkOrder($work_order));
	        }

			return $data;

		}

		public function getOpenedWorkOrders(){

			$sql = $this->statements["SELECT_WITH_JOINS"];
			$sql = $sql. " WHERE `work_order_status` <> :WorkOrderStatus";
			$sql = $sql.$this->statements["DEFAULT_ORDER"];

			$result = $this->db->query($sql,array(':WorkOrderStatus' => 'Cerrada'));


			$work_orders = $result->fetchAll(PDO::FETCH_OBJ);
	        
	        $data = [];

	        foreach ($work_orders as $work_order) {

	        	array_push($data, $this->convertWorkOrder($work_order));
	        }

			return $data;

		}

		public function getWorkOrder($WorkOrderId){
			
			$sql = $this->statements["SELECT"];
			$sql = $sql." WHERE `work_order_id` = :WorkOrderId";
			
			$result = $this->db->query($sql,
				array(':WorkOrderId' => $WorkOrderId )
			);

			$work_order=$result->fetch(PDO::FETCH_OBJ);

			return $this->convertWorkOrder($work_order,true);
		}

		public function getWorkOrderByTransactionId($TransactionId){
			
			
			$sql = $this->statements["SELECT"];
			$sql = $sql." WHERE `transaction_id` = :TransactionId";
			
			
			$result = $this->db->query($sql,
				array(':TransactionId' => $TransactionId )
			);

			$work_order=$result->fetch(PDO::FETCH_OBJ);

			return $this->convertWorkOrder($work_order,true);
		}

		public function getWorkOrderBySale($SaleOrderId,$detailed=true){
			
			$sql = $this->statements["SELECT"];
			$sql = $sql." WHERE `sale_order_id` = :SaleOrderId";
			
			$result = $this->db->query($sql,
				array(':SaleOrderId' => $SaleOrderId )
			);

			$work_order=$result->fetch(PDO::FETCH_OBJ);

			return $this->convertWorkOrder($work_order,$detailed);
		}


		public function createWorkOrder($work_order){

			try{

					$this->db->conn->beginTransaction();
					
					$sql="
						INSERT INTO `erp_work_orders`(
							`work_order_id`, 
							`work_order_number`, 
							`work_order_title`, 
							`work_order_creation_date`, 
							`work_order_delivery_date`, 
							`work_order_supervisor`, 
							`work_order_status`, 
							`work_order_priority`, 
							`work_order_notes`, 
							`sale_order_id`,
							`transaction_id`,
							`place_id`
						) VALUES (
							:WorkOrderId,
							:WorkOrderNumber,
							:WorkOrderTitle,
							:WorkOrderCreationDate,
							:WorkOrderDeliveryDate,
							:WorkOrderSupervisor,
							:WorkOrderStatus,
							:WorkOrderPriority,
							:WorkOrderNotes,
							:SaleOrderId,
							:TransactionId,
							:PlaceId
						)
					";

					$statement = $this->db->conn->prepare($sql);
					$statement->setFetchMode(PDO::FETCH_OBJ);
					$statement->execute($this->fillParams($work_order));
				
					$work_order->WorkOrderId = (int) $this->db->conn->lastInsertId();

					if(isset($work_order->materials)){ 
						$this->createOrUpdateWorkOrderMaterials($work_order);
					}

					if(isset($work_order->employees)){
						$this->createOrUpdateWorkOrderEmployees($work_order);
					}

					if(isset($work_order->notes)){
						$this->createOrUpdateWorkOrderNotes($work_order);
					}

					if(isset($work_order->expenses)){
						$this->createOrUpdateWorkOrderExpenses($work_order);
					}

					$this->db->conn->commit();

					return $work_order;
			}
			catch(Exception $ex){
				$this->db->conn->rollback();
				throw new Exception($ex->getMessage());
			}
		}

		public function updateWorkOrder($work_order){

			$this->db->conn->beginTransaction();
 			$sql = "
 				UPDATE `erp_work_orders` SET 
 				
 					`work_order_number`=:WorkOrderNumber,
 					`work_order_title`=:WorkOrderTitle,
 					`work_order_creation_date`=:WorkOrderCreationDate,
 					`work_order_delivery_date`=:WorkOrderDeliveryDate,
 					`work_order_supervisor`=:WorkOrderSupervisor,
 					`work_order_status`=:WorkOrderStatus,
 					`work_order_priority`=:WorkOrderPriority,
 					`work_order_notes`= :WorkOrderNotes,
 					`sale_order_id`=:SaleOrderId,
 					`transaction_id` =:TransactionId,
 					`place_id` = :PlaceId
 				WHERE 	`work_order_id`=:WorkOrderId
 				 ";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($work_order));


                if(isset($work_order->deleted_materials)){ 
					$this->deleteWorkOrderMaterials($work_order);
				}


				if(isset($work_order->materials)){ 
					$this->createOrUpdateWorkOrderMaterials($work_order);
				}

				if(isset($work_order->deleted_employees)){ 
					$this->deleteWorkOrderEmployees($work_order);
				}

				if(isset($work_order->employees)){
					$this->createOrUpdateWorkOrderEmployees($work_order);
				}

				if(isset($work_order->notes)){
					$this->createOrUpdateWorkOrderNotes($work_order);
				}

				if(isset($work_order->deleted_expenses)){
						$this->deleteWorkOrderExpenses($work_order);
					}


				if(isset($work_order->expenses)){
						$this->createOrUpdateWorkOrderExpenses($work_order);
				}







		    $this->db->conn->commit();

			return $work_order;



		}


		public function closeWorkOrder($work_order){

			$this->db->conn->beginTransaction();
			
			$work_order->WorkOrderStatus = 'Cerrada';
 			$sql = "
 				UPDATE `erp_work_orders` SET 
 				
 					
 					`work_order_status`= :WorkOrderStatus

 				WHERE 	`work_order_id`=:WorkOrderId
 				 ";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(':WorkOrderId' => $work_order->WorkOrderId, ':WorkOrderStatus'=>$work_order->WorkOrderStatus));


		    $this->db->conn->commit();

			return $work_order;



		}

		public function setWorkOrderStatus($work_order,$WorkOrderStatus){

			$this->db->conn->beginTransaction();

			

			
 			$sql = "
 				UPDATE `erp_work_orders` SET 
 				
 					
 					`work_order_status`= :WorkOrderStatus

 				WHERE 	`work_order_id`=:WorkOrderId
 				 ";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(':WorkOrderId' => $work_order->WorkOrderId, ':WorkOrderStatus'=>$WorkOrderStatus));


		    $this->db->conn->commit();

		    $work_order->WorkOrderStatus = $WorkOrderStatus;


			return $work_order;



		}


		public function deleteWorkOrder($WorkOrderId){
			$this->db->conn->beginTransaction();


			$sql = "DELETE FROM `erp_work_order_materials` WHERE `work_order_id`=:WorkOrderId";

			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
		    $statement->execute(array(':WorkOrderId' => $WorkOrderId));

			$sql = "DELETE FROM `erp_work_order_notes` WHERE `work_order_id`=:WorkOrderId";

			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
		    $statement->execute(array(':WorkOrderId' => $WorkOrderId));

			$sql = "DELETE FROM `erp_work_order_technicians` WHERE `work_order_id`=:WorkOrderId";
			
			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
		    $statement->execute(array(':WorkOrderId' => $WorkOrderId));


		    $sql = "DELETE FROM `erp_work_order_expenses` WHERE `work_order_id`=:WorkOrderId";
			
			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
		    $statement->execute(array(':WorkOrderId' => $WorkOrderId));

			$sql = "DELETE FROM `erp_work_orders` WHERE `work_order_id`=:WorkOrderId";


			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
		    $statement->execute(array(':WorkOrderId' => $WorkOrderId));


			$this->db->conn->commit();

		}

		private function createOrUpdateWorkOrderMaterials($work_order){

			$sql_insert="
				INSERT INTO `erp_work_order_materials`(
					`work_order_id`, 
					`material_id`, 
					`material_qty`
				) VALUES (
					:WorkOrderId,
					:MaterialId,
					:MaterialQty
				)
			";

			$sql_update="
				UPDATE `erp_work_order_materials` 
				SET `material_qty`=:MaterialQty
				WHERE `work_order_id`=:WorkOrderId 
				AND `material_id`=:MaterialId
			";

			foreach ($work_order->materials as $material) {


				$sql = $sql_update;

				if(isset($material->IsNew)){ 
					if($material->IsNew) $sql = $sql_insert; 
				}

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array( ":WorkOrderId" => $work_order->WorkOrderId , ":MaterialId" => $material->MaterialId , ":MaterialQty" => $material->MaterialQty));

				$material->IsNew = false;
			}

		}

		private function deleteWorkOrderMaterials($work_order){

			$sql_delete="
				DELETE FROM `erp_work_order_materials`
				WHERE `work_order_id`=:WorkOrderId 
				AND `material_id`=:MaterialId
			";



			foreach ($work_order->deleted_materials as $material) {


				$sql = $sql_delete;

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array( ":WorkOrderId" => $work_order->WorkOrderId , ":MaterialId" => $material->MaterialId));

				$material->IsNew = false;
			}

		}

		private function deleteWorkOrderExpenses($work_order){

			$sql_delete="
				DELETE FROM `erp_work_order_expenses`
				WHERE `work_order_id`=:WorkOrderId 
				AND `work_order_expense_id`=:WorkOrderExpenseId
			";



			foreach ($work_order->deleted_expenses as $expense) {


				$sql = $sql_delete;

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array( ":WorkOrderId" => $work_order->WorkOrderId , ":WorkOrderExpenseId" => $expense->WorkOrderExpenseId));

				$expense->IsNew = false;
			}

		}

		private function createOrUpdateWorkOrderEmployees($work_order){

			$sql_insert = "
				INSERT INTO `erp_work_order_technicians`(
					`work_order_id`, 
					`employee_id`
				) VALUES (:WorkOrderId,:EmployeeId)
			";

			$sql_delete = "
				DELETE FROM `erp_work_order_technicians` 
				WHERE `work_order_id` = :WorkOrderId AND `employee_id` = :EmployeeId
			";

			$new_employees = [];

			for ($count = 0 ; $count < count($work_order->employees); $count++) {

				$employee = $work_order->employees[$count];
				
				$Add = true;

				if(!isset($employee->WorkOrderId)){	

					$sql = $sql_insert;

	                if(isset($employee->IsMarkForDelete))
	                {
	                	if($employee->IsMarkForDelete){

	                		$sql = $sql_delete;

	                	}                

	                }
					
					//echo $sql;
					//var_dump($employee); 
					$statement = $this->db->conn->prepare($sql);
					$statement->setFetchMode(PDO::FETCH_OBJ);
					$statement->execute(array( ":WorkOrderId" => $work_order->WorkOrderId , ":EmployeeId" => $employee->EmployeeId));

					if(isset($employee->IsMarkForDelete))
	                {
	                	if($employee->IsMarkForDelete){
	                		$Add = false;
	                	}
	                }
	                else{

	                }
					$employee->WorkOrderId = $work_order_id;
				}

				if($Add){
					array_push($new_employees,$employee);	
				}
				
			}

			$work_order->employees = $new_employees;


		}

		private function deleteWorkOrderEmployees($work_order){

	
			$sql_delete = "
				DELETE FROM `erp_work_order_technicians` 
				WHERE `work_order_id` = :WorkOrderId AND `employee_id` = :EmployeeId
			";

			$new_employees = [];

			for ($count = 0 ; $count < count($work_order->deleted_employees); $count++) {

				$employee = $work_order->deleted_employees[$count];
				
				//$Add = true;

				//if(!isset($employee->WorkOrderId)){	

					$sql = $sql_delete;

	    
					
					//echo $sql;
					//var_dump($employee); 
					$statement = $this->db->conn->prepare($sql);
					$statement->setFetchMode(PDO::FETCH_OBJ);
					$statement->execute(array( ":WorkOrderId" => $work_order->WorkOrderId , ":EmployeeId" => $employee->EmployeeId));

		
				//}


				
			}

	


		}

		private function createOrUpdateWorkOrderNotes($work_order){

			$sql_insert="
				INSERT INTO `erp_work_order_notes`(
					`work_order_id`, 
					`work_order_note_id`, 
					`work_order_note_text`, 
					`user_id`, 
					`work_order_note_cdate`
				) VALUES (
					:WorkOrderId,
					:WorkOrderNoteId,
					:WorkOrderNoteText,
					:UserId,
					Now()
				)
			";

			$sql_update="
				UPDATE `erp_work_order_notes` SET 
					`work_order_note_text`=:WorkOrderNoteText,
					`user_id`=:UserId,
					`work_order_note_cdate`= Now() 
				WHERE 					
					`work_order_id`=:WorkOrderId
				AND
					`work_order_note_id`=:WorkOrderNoteId
			";

			foreach ($work_order->notes as $note) {


				$sql = $sql_insert;


				if(isset($note->WorkOrderNoteId)){
					$sql = $sql_update;
				}
				else{
					
					$note->WorkOrderNoteId = $this->createWorkOrderNoteIdentity($work_order->WorkOrderId);
				}

                //var_dump($note);
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillWorkOrderNoteParams($work_order,$note));

			}

		}

		private function createOrUpdateWorkOrderExpenses($work_order){

			$sql_insert="
				INSERT INTO `erp_work_order_expenses`(
					`work_order_id`, 
					`work_order_expense_id`, 
					`work_order_expense_text`,
					`work_order_expense_date`,
					`work_order_expense_amount`,
					`work_order_expense_method`,
					`work_order_expense_cdate`,
					`work_order_expense_lmdate`,
					`currency_id`,
					`user_id`
		
				) VALUES (
					:WorkOrderId,
					:WorkOrderExpenseId,
					:WorkOrderExpenseText,
					:WorkOrderExpenseDate,
					:WorkOrderExpenseAmount,
					:WorkOrderExpenseMethod,
					Now(),
					Now(),
					:CurrencyId,
					:UserId
				)
			";

			$sql_update="
				UPDATE `erp_work_order_expenses` SET 
					`work_order_expense_text` = :WorkOrderExpenseText,
					`work_order_expense_date` = :WorkOrderExpenseDate,
					`work_order_expense_amount` = :WorkOrderExpenseAmount,
					`work_order_expense_method` = :WorkOrderExpenseMethod,
					`work_order_expense_lmdate` = Now(),
					`currency_id` = :CurrencyId,
					`user_id` = :UserId
				WHERE `work_order_id`=:WorkOrderId 
				AND `work_order_expense_id` = :WorkOrderExpenseId
			";

			foreach ($work_order->expenses as $expense) {


				$sql = $sql_update;

				if(isset($expense->IsNew)){ 
					if($expense->IsNew) {

						$sql = $sql_insert; 
						$expense->WorkOrderId = $work_order->WorkOrderId;
						$expense->WorkOrderExpenseId = $this->createWorkOrderExpenseIdentity($work_order->WorkOrderId);
					}

				}
				//echo $sql;
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillWorkOrderExpensesParams($work_order,$expense));

				$expense->IsNew = false;
			}

		}

		private function fillWorkOrderExpensesParams($work_order,$expense){


				$params =[];



				if(isset($expense->WorkOrderId)){  $params[':WorkOrderId'] = $expense->WorkOrderId; } else {  $params[':WorkOrderId'] = NULL; };
				if(isset($expense->WorkOrderExpenseId)){  $params[':WorkOrderExpenseId'] = $expense->WorkOrderExpenseId; } else {  $params[':WorkOrderExpenseId'] = NULL; };
				if(isset($expense->WorkOrderExpenseText)){  $params[':WorkOrderExpenseText'] = $expense->WorkOrderExpenseText; } else {  $params[':WorkOrderExpenseText'] = NULL; };
				if(isset($expense->WorkOrderExpenseDate)){  $params[':WorkOrderExpenseDate'] = $expense->WorkOrderExpenseDate; } else {  $params[':WorkOrderExpenseDate'] = NULL; };
				if(isset($expense->WorkOrderExpenseAmount)){  $params[':WorkOrderExpenseAmount'] = $expense->WorkOrderExpenseAmount; } else {  $params[':WorkOrderExpenseAmount'] = NULL; };
				if(isset($expense->WorkOrderExpenseMethod)){  $params[':WorkOrderExpenseMethod'] = $expense->WorkOrderExpenseMethod; } else {  $params[':WorkOrderExpenseMethod'] = NULL; };
				
				if(isset($expense->CurrencyId)){  $params[':CurrencyId'] = $expense->CurrencyId; } else {  $params[':CurrencyId'] = NULL; };
				if(isset($expense->UserId)){  $params[':UserId'] = $expense->UserId; } else {  $params[':UserId'] = NULL; };

				//var_dump($params);
				return $params;


		}

		private function fillWorkOrderNoteParams($work_order, $note){

			return array( 
				":WorkOrderId" => $work_order->WorkOrderId , 
				":WorkOrderNoteId" => $note->WorkOrderNoteId , 
				":WorkOrderNoteText" => $note->WorkOrderNoteText,
				":UserId" => getOwnerId()
				);

		}



		private function createWorkOrderExpenseIdentity($work_order_id){

			$work_order_expense_id = 1;

			$sql = "
				SELECT  MAX(`work_order_expense_id`) + 1 as identity FROM `erp_work_order_expenses` WHERE `work_order_id` = :WorkOrderId
			";

			
			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute(array(':WorkOrderId' => $work_order_id));


	 		$result = $statement->fetch(PDO::FETCH_OBJ);
			 
			 if(isset($result->identity)){
			 	$work_order_expense_id = $result->identity;
			 }

			 return $work_order_expense_id;
		
		}


		private function createWorkOrderNoteIdentity($work_order_id){

			$work_order_note_id = 1;

			$sql = "
				SELECT  MAX(`work_order_note_id`)+1 as identity FROM `erp_work_order_notes` WHERE `work_order_id` = :WorkOrderId
			";

			
			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute(array(':WorkOrderId' => $work_order_id));


	 		$result = $statement->fetch(PDO::FETCH_OBJ);
			 
			 if(isset($result->identity)){
			 	$work_order_note_id = $result->identity;
			 }

			 return $work_order_note_id;
		
		}

		 public function createWorkOrderFromQuote($QuoteId){

		 	$QuoteModel = new QuoteModel();
		 	$Quote = $QuoteModel->getQuote($QuoteId);

		 	
		 	if($Quote["QuoteId"]){

			 	$SaleOrderModel = new SaleOrderModel();
	    		$SaleOrder= $SaleOrderModel->getSaleOrderByQuote($Quote["QuoteId"]);

	    		if($SaleOrder["SaleOrderId"]){
		    	   

		    	    $WorkOrder = $this->getWorkOrderBySale($SaleOrder["SaleOrderId"]);

		    	    if($WorkOrder["WorkOrderId"] == NULL){

				    	$WorkOrder = new stdClass();
				    	//$WorkOrder->WorkOrderId = NULL;
				    	//$WorkOrder->WorkOrderNumber = NULL;
				    	$WorkOrder->WorkOrderTitle = $SaleOrder["SaleOrderTitle"]."xx";
				    	$WorkOrder->WorkOrderCreationDate = date("Y/m/d");
				    	$WorkOrder->WorkOrderDeliveryDate = NULL;
				    	$WorkOrder->WorkOrderSupervisor = NULL;
				    	$WorkOrder->WorkOrderStatus = "Nueva";
				        $WorkOrder->WorkOrderPriority = NULL;
				        $WorkOrder->WorkOrderNotes = "" ; //$SaleOrder->SaleOrderDescription;
				        $WorkOrder->SaleOrderId = $SaleOrder["SaleOrderId"];
				        $WorkOrder->TransactionId = $SaleOrder["TransactionId"];

				    	$WorkOrderModel = new WorkOrderModel();
				    	$WorkOrder = $WorkOrderModel->createWorkOrder($WorkOrder);
				    }

				    return $WorkOrder;
		    	}
		    	else{

		    		throw new Exception("La cotizacion no esta asociada a ninguna orden de venta");
		    		
		    	}
		    }
		    else{

		    	throw new Exception("No existe la cotizacion");
		    	
		    }
    	}

		private function fillParams($work_order){

			$params =[];

			if(isset($work_order->WorkOrderId)){  $params[':WorkOrderId'] = $work_order->WorkOrderId; } else {  $params[':WorkOrderId'] = NULL; };
			if(isset($work_order->WorkOrderNumber)){  $params[':WorkOrderNumber'] = $work_order->WorkOrderNumber; } else {  $params[':WorkOrderNumber'] = NULL; };
			if(isset($work_order->WorkOrderTitle)){  $params[':WorkOrderTitle'] = $work_order->WorkOrderTitle; } else {  $params[':WorkOrderTitle'] = NULL; };
			if(isset($work_order->WorkOrderCreationDate)){  $params[':WorkOrderCreationDate'] = $work_order->WorkOrderCreationDate; } else {  $params[':WorkOrderCreationDate'] = NULL; };
			if(isset($work_order->WorkOrderDeliveryDate)){  $params[':WorkOrderDeliveryDate'] = $work_order->WorkOrderDeliveryDate; } else {  $params[':WorkOrderDeliveryDate'] = NULL; };
			if(isset($work_order->WorkOrderSupervisor)){  $params[':WorkOrderSupervisor'] = $work_order->WorkOrderSupervisor; } else {  $params[':WorkOrderSupervisor'] = NULL; };
			if(isset($work_order->WorkOrderStatus)){  $params[':WorkOrderStatus'] = $work_order->WorkOrderStatus; } else {  $params[':WorkOrderStatus'] = NULL; };
			if(isset($work_order->WorkOrderPriority)){  $params[':WorkOrderPriority'] = $work_order->WorkOrderPriority; } else {  $params[':WorkOrderPriority'] = NULL; };
			if(isset($work_order->WorkOrderNotes)){  $params[':WorkOrderNotes'] = $work_order->WorkOrderNotes; } else {  $params[':WorkOrderNotes'] = NULL; };
			if(isset($work_order->SaleOrderId)){  $params[':SaleOrderId'] = $work_order->SaleOrderId; } else {  $params[':SaleOrderId'] = NULL; };
			if(isset($work_order->TransactionId)){  $params[':TransactionId'] = $work_order->TransactionId; } else {  $params[':TransactionId'] = NULL; };
			if(isset($work_order->PlaceId)){   $params[':PlaceId'] = $work_order->PlaceId; } else {   
				
				$UserModel = new UserModel();
				$user = $UserModel->getUser(getOwnerId());
				
				if( $user->place_id > 0){ $params[":PlaceId"] = $user->place_id; } else {$params[':PlaceId'] = NULL;}
				
			};
			return $params;
		}

		private function convertWorkOrder($work_order,$detailed=false){


			$data =  array(
				    "WorkOrderId" => $work_order->work_order_id, 
					"WorkOrderNumber" => "OT-".$work_order->work_order_id,//$work_order->work_order_number, 
					"CustomerName" => $work_order->customer_name, 
					"WorkOrderTitle" => $work_order->work_order_title, 
					"WorkOrderCreationDate" => $work_order->work_order_creation_date, 
					"WorkOrderDeliveryDate" => $work_order->work_order_delivery_date, 
					"WorkOrderSupervisor" => $work_order->work_order_supervisor, 
					"WorkOrderStatus" => $work_order->work_order_status, 
					"WorkOrderPriority" => $work_order->work_order_priority, 
					"WorkOrderNotes" => $work_order->work_order_notes, 
					"SaleOrderId" => $work_order->sale_order_id,
					"TransactionId" => $work_order->transaction_id,
					"PlaceId" => $work_order->place_id

				);



			if(isset($work_order->work_order_delivery_date) && $work_order->work_order_status!= "Cerrada" ){
				if(  date('d-m-Y',strtotime($work_order->work_order_delivery_date)) < date('d-m-Y')){
            			$data["Overdue"] = true;
            	}
            }

			if(isset($work_order->work_order_supervisor)){
				$EmployeeModel = new EmployeeModel();
				
			}

            if($detailed){

            	if(isset($work_order->sale_order_id)){

            		$SaleOrderModel = new SaleOrderModel();
                    $SaleOrder = $SaleOrderModel->getSaleOrder($work_order->sale_order_id);
            		$data["SaleOrder"] = $SaleOrder;

            		if(isset($SaleOrder["QuoteId"])){
            			$QuoteModel = new QuoteModel();
            			$Quote = $QuoteModel->getQuote($SaleOrder["QuoteId"],true);
            			$data["Quote"] = $Quote;
            		}




            	}

				$data["employees"] = $this->getWorkOrderEmployees($work_order);
				$data["materials"] = $this->getWorkOrderMaterials($work_order);
				$data["notes"] = $this->getWorkOrderNotes($work_order);
				$data["expenses"] = $this->getWorkOrderExpenses($work_order);
			}


			return $data ;
		}

		private function getWorkOrderMaterials($work_order){
			$result=$this->db->query("
				SELECT `work_order_id`, `material_id`, `material_qty` FROM `erp_work_order_materials` 
				WHERE `work_order_id` = :WorkOrderId
			",
			array(':WorkOrderId' => $work_order->work_order_id )
			);

			$work_order_materials = $result->fetchAll(PDO::FETCH_OBJ);

			$data= [];


			$MaterialModel = new MaterialModel();

			 foreach ($work_order_materials as $work_order_material) {

	        	$material =  $MaterialModel->getMaterial($work_order_material->material_id);
	        	$material["WorkOrderId"] =$work_order->work_order_id ;
	        	$material["MaterialQty"] = $work_order_material->material_qty;

	        	array_push($data,$material);
	        }

	        return $data;

		}

		private function getWorkOrderEmployees($work_order){

			$result = $this->db->query("
				SELECT 
					`work_order_id`, 
					`employee_id` 
				FROM `erp_work_order_technicians`
				WHERE `work_order_id` = :WorkOrderId
			",
			array(':WorkOrderId' => $work_order->work_order_id )
			);

			//var_dump($work_order);

			$work_order_employees = $result->fetchAll(PDO::FETCH_OBJ);
	        
	        $data = [];

	        $EmployeeModel = new EmployeeModel();

	        foreach ($work_order_employees as $work_order_employee) {

	        	$employee =  $EmployeeModel->getEmployee($work_order_employee->employee_id);
	        	$employee["WorkOrderId"] =$work_order->work_order_id ; 

	        	array_push($data,$employee);
	        }






			return $data;
		}

		private function getWorkOrderNotes($work_order){

			$result = $this->db->query("
				SELECT 
					`work_order_id`, 
					`work_order_note_id`, 
					`work_order_note_text`, 
					`user_id`, 
					`work_order_note_cdate` 
				FROM `erp_work_order_notes` 
				WHERE `work_order_id` = :WorkOrderId
			",
			array(':WorkOrderId' => $work_order->work_order_id )
			);

			//var_dump($work_order);

			$work_order_notes = $result->fetchAll(PDO::FETCH_OBJ);
	        
	        $data = [];

	        foreach ($work_order_notes as $work_order_note) {

	        	array_push($data, $this->convertWorkOrderNote($work_order_note));
	        }




			return $data;

		}

		private function getWorkOrderExpenses($work_order){

			$result = $this->db->query("
				SELECT
					`work_order_id`, 
					`work_order_expense_id`, 
					`work_order_expense_receipt_number`, 
					`work_order_expense_receipt_prefix`, 
					`work_order_expense_text`, 
					`work_order_expense_date`, 
					`work_order_expense_time`, 
					`work_order_expense_amount`, 
					`work_order_expense_method`, 
					`work_order_expense_cdate`, 
					`work_order_expense_lmdate`, 
					`currency_id`, 
					`user_id` 
				FROM `erp_work_order_expenses` 
				WHERE `work_order_id` = :WorkOrderId
			",
			array(':WorkOrderId' => $work_order->work_order_id )
			);

			//var_dump($work_order);

			$work_order_expenses = $result->fetchAll(PDO::FETCH_OBJ);
	        
	        $data = [];

	        foreach ($work_order_expenses as $work_order_expense) {

	        	array_push($data, $this->convertWorkOrderExpense($work_order_expense));
	        }


			return $data;

		}



		private function convertWorkOrderNote($work_order_note){

			$data = array(
				"WorkOrderId" => $work_order_note->work_order_id, 
				"WorkOrderNoteId" => $work_order_note->work_order_note_id,
				"WorkOrderNoteText" => $work_order_note->work_order_note_text, 
				"UserId" => $work_order_note->user_id, 
				"WorkOrderNoteCDate" => $work_order_note->work_order_note_cdate
			);



			if(isset($work_order_note->user_id)){

				$UserModel = new UserModel();
				$user = $UserModel->getUser($work_order_note->user_id);
				$data["UserName"] = $user->user_name;

			}


			return $data;


		}

		private function convertWorkOrderExpense($expense){

			$data = array(
				"WorkOrderId" => $expense->work_order_id, 
				"WorkOrderExpenseId" => $expense->work_order_expense_id,
				"WorkOrderExpenseText" => $expense->work_order_expense_text, 
				"WorkOrderExpenseDate" => $expense->work_order_expense_date, 
				"WorkOrderExpenseAmount" => $expense->work_order_expense_amount,
				"WorkOrderExpenseMethod" => $expense->work_order_expense_method,
				"CurrencyId" => $expense->currency_id,
				"UserId" => $expense->user_id
			);


			if(isset($expense->user_id)){

				$UserModel = new UserModel();
				$user = $UserModel->getUser($expense->user_id);
				$data["UserName"] = $user->user_name;

			}


			return $data;

		}





	}
?>