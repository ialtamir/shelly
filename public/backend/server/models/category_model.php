<?php
class CategoryModel {

	private $db;
	
	

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getCategories(){

		$result = $this->db->query("
			SELECT  category_id , category_code ,  category_parent_id ,  category_name ,  category_description ,  category_picture , category_external_id ,  category_external_code ,  category_external_name ,  category_ecommerce_enabled,category_external_link,category_external_image_site,category_order
			FROM  erp_product_categories 
			Order by category_order
			",
			array()
		);

		$categories = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($categories as $category) {

        	array_push($data, $this->convertCategory($category));
        }

		return $data;

	}
	
	public function getCategoriesEcommerceEnabled(){

	/*	$result = $this->db->query("
			SELECT  category_id , category_code ,  category_parent_id ,  category_name ,  category_description ,  category_picture , category_external_id ,  category_external_code ,  category_external_name ,  category_ecommerce_enabled 
			FROM  erp_product_categories
			WHERE category_ecommerce_enabled = 1
			",
			array()
		);*/
		
		/*$result = $this->db->query("
		SELECT * FROM erp_product_categories where category_id in (
			SELECT distinct category_parent_id 
			FROM erp_product_categories
		)
		AND category_ecommerce_enabled = 1
			",
			array()
		);*/
		
			$result = $this->db->query("
		SELECT * FROM erp_product_categories where category_parent_id IS NULL
			
		
		AND category_ecommerce_enabled = 1
		Order by category_order
			",
			array()
		);
		
		
		/*$result = $this->db->query("
		SELECT * FROM erp_product_categories where category_id in (
			SELECT distinct category_parent_id 
			FROM erp_product_categories
			WHERE category_id
			IN (
			
				SELECT DISTINCT category_id
				FROM  `erp_products` 
				WHERE product_ecommerce_enabled =1
			))
			",
			array()
		);*/
		
		

		$categories = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($categories as $category) {
			
			
			$category = $this->convertCategory($category);
			
			$category["childs"] = $this->getChildCategories($category["CategoryId"]);
			
			
        	array_push($data, $category);
        }
        
        

		return $data;

	}
	public function getCategoriesWithFeaturedProducts(){


		

		
		
		$result = $this->db->query("
		SELECT * FROM erp_product_categories where category_id
			
			IN (
			
				SELECT DISTINCT category_id
				FROM  erp_products
				WHERE product_featured =1
			)
		Order by category_order
			",
			array()
		);
		
		

		$categories = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($categories as $category) {
			
			
			$category = $this->convertCategory($category);
			
			$ProductModel = new ProductModel();
			$category["products"] = $ProductModel->getFeaturedProducts($category["CategoryId"]);
        	array_push($data, $category);
        }
        
        

		return $data;

	}
	
	
	public function getChildCategories($CategoryId){

		$result = $this->db->query("
			SELECT  *
			FROM  erp_product_categories
			WHERE category_parent_id = :category_parent_id
			Order by category_order
			",
			array(":category_parent_id" => $CategoryId)
		);

		$categories = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($categories as $category) {

			
		
			
			$category = $this->convertCategory($category);
			
			$category["childs"] = $this->getChildCategories($category["CategoryId"]);
			
			
        	array_push($data, $category);
        }

		return $data;

	}
	
	
	

	public function getCategory($CategoryId){
		$result = $this->db->query(" 
			SELECT  *
			FROM  erp_product_categories
			WHERE `category_id` = :CategoryId Order by category_order",
			array( ':CategoryId' => $CategoryId )
		);


		$category=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertCategory($category);

	}
	
	public function getCategoryByCode($CategoryCode){
		$result = $this->db->query(" 
			SELECT  *
			FROM  erp_product_categories
			WHERE `category_code` = :CategoryCode Order by category_order",
			array( ':CategoryCode' => $CategoryCode )
		);


		$category=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertCategory($category);

	}

	public function createCategory($category) {

		$this->db->query("
			INSERT INTO erp_product_categories(
				category_id,
				category_code,
				category_parent_id,
				category_name, 
				category_description,
				category_picture,
				category_external_id,
				category_external_code,
				category_external_name,
				category_ecommerce_enabled,
				category_external_link,
				category_external_image_site,
				category_order
			)
			VALUES (
				:CategoryId,
				:CategoryCode,
				:CategoryParentId,
				:CategoryName,
				:CategoryDescription,
				:CategoryPicture,
				:CategoryExternalId,
				:CategoryExternalCode,
				:CategoryExternalName,
				:CategoryEcommerceEnabled,
				:CategoryExternalLink,
				:CategoryExternalImageSite,
				:CategoryOrder
			)",
			$this->fillParams($category)
		);

		$category->CategoryId = $this->db->getInsertId();

		return $category;
	}

	public function updateCategory($category) {

		$this->db->query("
			UPDATE erp_product_categories SET 
				category_id=:CategoryId,
				category_code=:CategoryCode,
				category_parent_id=:CategoryParentId,
				category_name=:CategoryName,
				category_description = :CategoryDescription,
				category_picture=:CategoryPicture,
				category_external_id=:CategoryExternalId,
				category_external_code=:CategoryExternalCode,
				category_external_name=:CategoryExternalName,
				category_ecommerce_enabled=:CategoryEcommerceEnabled,
				category_external_link = :CategoryExternalLink,
				category_external_image_site =:CategoryExternalImageSite,
				category_order =:CategoryOrder
			WHERE category_id=:CategoryId",
			$this->fillParams($category)
		);
	}
	
	public function syncCategories($products){

		try {
		//		$this->db->conn->beginTransaction();

				$sql = "
					INSERT INTO erp_product_categories(
						category_id,
						category_code,
						category_parent_id,
						category_name, 
						
						category_external_code,
						category_external_name,
						category_ecommerce_enabled
					)
					VALUES (
						:CategoryId,
						:CategoryCode,
						:CategoryParentId,
						:CategoryName,
						
						:CategoryExternalCode,
						:CategoryExternalName,
						1
					)
				";
		
				$sql_search = "SELECT * FROM erp_product_categories where category_code=:CategoryCode";
				
				foreach ($products as $product) {
					
					//var_dump($product);
					
					//Verificar si el producto ya existe en la base de datos
					$statement = $this->db->conn->prepare($sql_search);
					$statement->setFetchMode(PDO::FETCH_OBJ);
					$statement->execute(array(':CategoryCode' => $product->CategoryCode));
			
			        $result = $statement->fetch(PDO::FETCH_OBJ);
					 
					if(isset($result->category_id) && $result->category_id > 0){
					 	
					 	$product->CategoryId = $result->category_id;
					 	
					 	if(isset($result->category_parent_id)){
					 		
					 		$product->CategoryParentId = $result->category_parent_id;
					 		
					 	}
					 	else {
					 		
					 		$parent_category  = $this->getCategoryByCode($product->CategoryParentCode);
					 			
						 	if(isset($parent_category["CategoryId"])){
						 		
						 		$product->CategoryParentId = $parent_category["CategoryId"];
						 	
						 	}
						 	else {
						 	
						 			$parent_category = new StdClass();
						 			$parent_category->CategoryCode = $product->CategoryParentCode;
						 			$parent_category->CategoryName = $product->CategoryParentCode;
						 		
						 			$parent_category = $this->createCategory($parent_category);
						 		
						 			$product->CategoryParentId = $parent_category->CategoryId;
						 		
						 		}
					 		}
					 		
					 }
					 else {
					 	
					 	$parent_category  = $this->getCategoryByCode($product->CategoryParentCode);
					 	//var_dump($parent_category);
					 	
					 	if(isset($parent_category["CategoryId"]) && $parent_category["CategoryId"] > 0 ){
					 		
					 		$product->CategoryParentId = $parent_category["CategoryId"];
					 		
					 	}
					 	else{
	
					 		$parent_category = new StdClass();
					 		$parent_category->CategoryCode = $product->CategoryParentCode;
					 		$parent_category->CategoryName = $product->CategoryParentCode;
					 		
					 		$parent_category = $this->createCategory($parent_category);
					 		
					 		$product->CategoryParentId = $parent_category->CategoryId;
					 		
					 	}
					 	

		 
						//var_dump($product);
						$params =[];
	
						$params = [];

						if(isset($product->CategoryId)){ $params[':CategoryId'] = $product->CategoryId; } else { $params[':CategoryId'] = NULL; }; 
						if(isset($product->CategoryParentId)){ $params[':CategoryParentId'] = $product->CategoryParentId; } else { $params[':CategoryParentId'] = NULL; }; 
						if(isset($product->CategoryCode)){ $params[':CategoryCode'] = $product->CategoryCode; } else { $params[':CategoryCode'] = NULL; }; 
						if(isset($product->CategoryCode)){ $params[':CategoryName'] = $product->CategoryCode; } else { $params[':CategoryName'] = NULL; }; 
					    //if(isset($category->CategoryExternalId)){ $params[':CategoryExternalId'] = $category->CategoryExternalId; } else { $params[':CategoryExternalId'] = NULL; }; 
					    if(isset($product->CategoryCode)){ $params[':CategoryExternalCode'] = $product->CategoryCode; } else { $params[':CategoryExternalCode'] = NULL; }; 
					    if(isset($product->CategoryCode)){ $params[':CategoryExternalName'] = $product->CategoryCode; } else { $params[':CategoryExternalName'] = NULL; }; 
					    //if(isset($category->CategoryEcommerceEnabled)){ $params[':CategoryEcommerceEnabled'] = $category->CategoryEcommerceEnabled; } else { $params[':CategoryEcommerceEnabled'] = NULL; }; 
						
						
			
						
						$statement = $this->db->conn->prepare($sql);
						$statement->setFetchMode(PDO::FETCH_OBJ);
						$statement->execute($params);
	
						$product->CategoryId =(int) $this->db->conn->lastInsertId();
					 }
					 
					$sql_update_products = " UPDATE erp_products SET category_id=:CategoryId WHERE product_code = :ProductCode";
			
					$params = [];
					$params[":CategoryId"] = $product->CategoryId;
					$params[":ProductCode"] = $product->ProductCode;
						
									
					$statement = $this->db->conn->prepare($sql_update_products);
					$statement->setFetchMode(PDO::FETCH_OBJ);
					$statement->execute($params);
					
					var_dump($product);

				}
				
			
		

	


		//$this->db->conn->commit();


		return $products;
		
		} 
		catch(Exception $ex){
			
			//$this->db->conn->rollback();
			throw new Exception($ex);
			  
		}	
	}

	public function deleteCategory($CategoryId){
		$this->db->query("
			DELETE FROM `erp_product_categories` 
			WHERE `category_id`=:CategoryId",
			array(':CategoryId' => $CategoryId)
		);

		
	}

	public function updateCategoryPicture($CategoryId,$CategoryPicture){
		$this->db->query("
			UPDATE `erp_product_categories` SET 
				`category_picture`=:CategoryPicture 
			WHERE `category_id`=:CategoryId",
			array( 
				':CategoryId' => $CategoryId,
				':CategoryPicture' => $CategoryPicture)
		);
	}

	private function fillParams($category) {

        $params = [];

		if(isset($category->CategoryId)){ $params[':CategoryId'] = $category->CategoryId; } else { $params[':CategoryId'] = NULL; }; 
		if(isset($category->CategoryParentId)){ $params[':CategoryParentId'] = $category->CategoryParentId; } else { $params[':CategoryParentId'] = NULL; }; 
		if(isset($category->CategoryCode)){ $params[':CategoryCode'] = $category->CategoryCode; } else { $params[':CategoryCode'] = NULL; }; 
		if(isset($category->CategoryName)){ $params[':CategoryName'] = $category->CategoryName; } else { $params[':CategoryName'] = NULL; }; 
		if(isset($category->CategoryDescription)){ $params[':CategoryDescription'] = $category->CategoryDescription; } else { $params[':CategoryDescription'] = NULL; }; 
	    if(isset($category->CategoryPicture)){ $params[':CategoryPicture'] = $category->CategoryPicture; } else { $params[':CategoryPicture'] = NULL; }; 
	    if(isset($category->CategoryExternalId)){ $params[':CategoryExternalId'] = $category->CategoryExternalId; } else { $params[':CategoryExternalId'] = NULL; }; 
	    if(isset($category->CategoryExternalCode)){ $params[':CategoryExternalCode'] = $category->CategoryExternalCode; } else { $params[':CategoryExternalCode'] = NULL; }; 
	    if(isset($category->CategoryExternalName)){ $params[':CategoryExternalName'] = $category->CategoryExternalName; } else { $params[':CategoryExternalName'] = NULL; }; 
	    if(isset($category->CategoryEcommerceEnabled)){ $params[':CategoryEcommerceEnabled'] = $category->CategoryEcommerceEnabled; } else { $params[':CategoryEcommerceEnabled'] = NULL; }; 
		if(isset($category->CategoryExternalLink)){ $params[':CategoryExternalLink'] = $category->CategoryExternalLink; } else { $params[':CategoryExternalLink'] = NULL; }; 
		if(isset($category->CategoryExternalImageSite)){ $params[':CategoryExternalImageSite'] = $category->CategoryExternalImageSite; } else { $params[':CategoryExternalImageSite'] = NULL; }; 
		if(isset($category->CategoryOrder)){ $params[':CategoryOrder'] = $category->CategoryOrder; } else { $params[':CategoryOrder'] = NULL; }; 
		
			
			
			
	

				
				
		return $params;

	}



	private function convertCategory($category){
		
		return array(
			'CategoryId' => $category->category_id,
			'CategoryCode' =>  $category->category_code,
			'CategoryName' =>  $category->category_name,
			'CategoryDescription' =>  $category->category_description,
			'CategoryPicture' => $category->category_picture,
			'CategoryParentId' => $category->category_parent_id,
			'CategoryExternalId' => $category->category_external_id,  
			'CategoryExternalName' => $category->category_external_name,  
			'CategoryExternalCode' => $category->category_external_code,  
			'CategoryEcommerceEnabled' => $category->category_ecommerce_enabled,
			'CategoryExternalLink' => $category->category_external_link,
			'CategoryExternalImageSite'=> $category->category_external_image_site,
			'CategoryOrder'=> $category->category_order
			
		

		);
	}
}
?>