<?php
class ProductModel {
		
	private $db;
	
	private $product_select_sql = "SELECT product_id, product_code, product_name,product_description,erp_products.category_id, 
		product_cost_price,product_price,product_stock,product_um, product_has_variants,product_variant_product_id,
		product_is_saleable,product_external_id,product_external_code,product_external_name,product_featured,product_ecommerce_enabled,erp_product_categories.category_external_link,erp_product_categories.category_external_image_site
		FROM erp_products left outer join erp_product_categories ON erp_product_categories.category_id = erp_products.category_id  ";
		
		private $product_select_count_sql = "SELECT count(*) as product_count FROM erp_products left outer join erp_product_categories ON erp_product_categories.category_id = erp_products.category_id  ";

	public $hide_prices = false; 
	
	public function __construct(){

        $this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}


	public function searchProducts($query){

		$sql = $this->product_select_sql;

		$result = $this->db->query( $sql. " WHERE  ( product_name like :ProductName OR product_code like :ProductName ) and ( product_has_variants = 0 or product_has_variants is null ) AND product_weight > 0",
			array( ":ProductName" => ''.$query.'%')
		);

		$products = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($products as $product) {

        	array_push($data, $this->convertProduct($product,false,true));
        }

		return $data;
	}

	public function getProducts($include_only_saleables=false){

		$sql = $this->product_select_sql;
		$sql = $sql." WHERE ( product_has_variants = 0 or product_has_variants is null ) AND ( item_id = 1 ) AND product_weight > 0";
		
		if($include_only_saleables){
			$sql = $sql." AND product_is_saleable = 1 ";
		}
		
        //echo$sql;
        
		$result = $this->db->query($sql, array());
		

		$products = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($products as $product) {

        	array_push($data, $this->convertProduct($product,false,true));
        }

		return $data;
	}
	
	public function getEcommerceProducts($include_only_saleables=false){

		$sql = $this->product_select_sql;
		$sql = $sql." WHERE ( product_has_variants = 0 or product_has_variants is null ) AND ( item_id = 1 )   AND (product_ecommerce_enabled = 1) AND product_weight > 0";
		
		if($include_only_saleables){
			$sql = $sql." AND product_is_saleable = 1 ";
		}
		
        //echo$sql;
        
		$result = $this->db->query($sql, array());
		

		$products = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($products as $product) {

        	array_push($data, $this->convertProduct($product,false,true));
        }

		return $data;
	}

    public function getFeaturedProducts($category_id=0){

		$sql = $this->product_select_sql;
		$sql = $sql." WHERE ( product_has_variants = 0 or product_has_variants is null ) AND ( item_id = 1 )   AND ( product_featured = 1) AND product_weight > 0";
		$params = [];
		
		if($category_id > 0){
			$sql = $sql." AND erp_products.category_id = :category_id ";
			$params[":category_id"] = $category_id;
		}
		
        //echo$sql;
        
		$result = $this->db->query($sql, $params);
		

		$products = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($products as $product) {

        	array_push($data, $this->convertProduct($product,false,true));
        }

		return $data;
	}
	
	public function getRecommendedProducts($category_id=0){

		$sql = $this->product_select_sql;
		$sql = $sql." WHERE ( product_has_variants = 0 or product_has_variants is null ) AND ( item_id = 1 )   AND ( product_recommended = 1) AND product_weight > 0";
		$params = [];
		
		if($category_id > 0){
			$sql = $sql." AND erp_products.category_id = :category_id ";
			$params[":category_id"] = $category_id;
		}
		
        //echo$sql;
        
		$result = $this->db->query($sql, $params);
		

		$products = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($products as $product) {

        	array_push($data, $this->convertProduct($product,false,true));
        }

		return $data;
	}

	public function getProductVariants($ProductId){

		$sql = $this->product_select_sql;

		$result = $this->db->query($sql." WHERE `product_variant_product_id` = :ProductId  AND product_weight > 0",
			array(':ProductId' => $ProductId)
		);

		$products = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($products as $product) {

        	array_push($data, $this->convertProduct($product,false));
        }

		return $data;
	}

	public function getProductsByCategory($CategoryId=0,$product_search="",$page=0){


        $limit = 12;
        
		$params = [];
		
		$sql_count = $this->product_select_count_sql."  WHERE ( product_variant_product_id = 0 or product_variant_product_id is null ) AND product_weight > 0";
		
		$sql = $this->product_select_sql."  WHERE ( product_variant_product_id = 0 or product_variant_product_id is null ) AND product_weight > 0";

        if($CategoryId > 0){
			$sql = $sql." AND erp_products.category_id = :category_id ";
			$sql_count = $sql_count." AND erp_products.category_id = :category_id ";
			$params[":category_id"] = $CategoryId;
		}
		
		if($product_search <> ""){
			$sql = $sql." AND ( product_code like :product_search OR product_name like :product_search OR product_description like :product_search ) ";
			
			$sql_count = $sql_count." AND ( product_code like :product_search OR product_name like :product_search OR product_description like :product_search ) ";
			
			$params[":product_search"] = "%".$product_search."%";
		}
		
		$result_count = $this->db->query($sql_count,$params);
		$products_count = $result_count->fetch(PDO::FETCH_OBJ);
		
		if($page > 0){
			
			$page   = $page - 1;
            $offset = $limit * $page ;
            
		}
		else{
			
			$offset = 0;
		}
		
		
		$sql = $sql." LIMIT $offset, $limit";
		
		$products_result = new stdClass();
		$products_result->count = $products_count->product_count;
		
		
		
		$result = $this->db->query($sql,$params);
		
		
		
		

		$products = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($products as $product) {

        	array_push($data, $this->convertProduct($product,false));
        }

		$products_result->data = $data;
		return $products_result;

	}
	
	public function getProductsByCategoryPaging($CategoryId=0,$product_search="",$page=0){
		
		$limit = 12;

		

		$params = [];
		
		
		$sql_count = $this->product_select_count_sql."  WHERE ( product_variant_product_id = 0 or product_variant_product_id is null ) AND product_weight > 0";
		
		$sql       = $this->product_select_sql      ."  WHERE ( product_variant_product_id = 0 or product_variant_product_id is null ) AND product_weight > 0";

        if($CategoryId > 0){
        	
			$sql = $sql." AND erp_products.category_id = :category_id ";
			$sql_count = $sql_count." AND erp_products.category_id = :category_id ";
			
			$params[":category_id"] = $CategoryId;
		}
		
		if($product_search <> ""){
			
			$sql       = $sql      ." AND ( product_code like :product_search OR product_name like :product_search OR product_description like :product_search ) ";
			$sql_count = $sql_count." AND ( product_code like :product_search OR product_name like :product_search OR product_description like :product_search ) ";
			
			
			$params[":product_search"] = "%".$product_search."%";
		}
		
		//var_dump($params);
		//var_dump($sql);
		
		$result_count = $this->db->query($sql_count,$params);
		$products_count = $result_count->fetch(PDO::FETCH_OBJ);
		
		if($page > 0){
			
			$page   = $page - 1;
            $offset = $limit * $page ;
            
		}
		else{
			
			$offset = 0;
		}
		
		
		$sql = $sql." LIMIT $offset, $limit";
		
		
		
		
		$products_result = new stdClass();
		$products_result->count = $products_count->product_count;
		
		
		
		
		$result = $this->db->query($sql,$params);
		
		

		$products = $result->fetchAll(PDO::FETCH_OBJ);
        
        
		$data = [];
        foreach ($products as $product) {

        	array_push($data, $this->convertProduct($product,false));
        }

		$products_result->data = $data;
		return $products_result;

	}

	public function getAllProducts(){

		$sql = $this->product_select_sql;

		$result = $this->db->query($sql." WHERE ( product_variant_product_id = 0 or product_variant_product_id is null ) and ( item_id = 1 ) ",
			array( )
		);

		$products = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($products as $product) {

        	array_push($data, $this->convertProduct($product,false,true));
        }

		return $data;

	}

	public function getVariants(){
		$result = $this->db->query(" 
			SELECT `variant_id`, `variant_name` FROM `erp_variants`",
			array()
		);

		$variants = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($variants as $variant) {

        	

        	array_push($data, $this->convertVariant($variant));
        }

		return $data;
	}

	public function getVariantByName($VariantName){

		$result = $this->db->query(" 
			SELECT `variant_id`, `variant_name` FROM `erp_variants` WHERE `variant_name` = :VariantName",
			array(':VariantName' => $VariantName)
		);

		$variant = $result->fetchAll(PDO::FETCH_OBJ);
        

        	

        return $this->convertVariant($variant);
        

		
	}

	public function getVariantValues($VariantId) {
		$result = $this->db->query("
			SELECT 
				`variant_id`, 
				`variant_value_id`, 
				`variant_value_name`, 
				`variant_value_picture_url`
			FROM `erp_variant_values` 
			WHERE `variant_id` = :VariantId
		", array(':VariantId'=> $VariantId));

		$results = $result->fetchAll(PDO::FETCH_OBJ);

		$variant_values =[];

		foreach ($results as $variant_value){
			array_push($variant_values, $this->convertVariantValue($variant_value));
		}

		return $variant_values;


	}

	private function convertVariantValue($variant_value){
		return array(
			"VariantId"  => $variant_value->variant_id, 
			"VariantValueId" => $variant_value->variant_value_id, 
			"VariantValueName" => $variant_value->variant_value_name,
			"VariantValuePictureUrl" => $variant_value->variant_value_picture_url,
		);
	}

	public function createProduct($product){

		try{
				$this->db->conn->beginTransaction();


				$sql = "
					INSERT INTO `erp_products`(
						`product_id`, 
						`product_code`, 
						`product_name`, 
						`product_description`, 
						`category_id`, 
						`product_cost_price`, 
						`product_price`, 
						`product_stock`, 
						`product_um`, 
						`product_has_variants`, 
						`product_variant_product_id`,
						`item_id`,
						`product_is_saleable`,
						product_featured,
						product_ecommerce_enabled
					) VALUES (	
						:ProductId, 
						:ProductCode, 
						:ProductName, 
						:ProductDescription, 
						:CategoryId, 
						:ProductCostPrice, 
						:ProductPrice, 
						:ProductStock, 
						:ProductUM, 
						:ProductHasVariants, 
						:ProductVariantProductId,
						1,
						:ProductIsSaleable,
						:ProductFeatured,
						:ProductEcommerceEnabled
					)";
		
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($product));

				$product->ProductId =(int) $this->db->conn->lastInsertId();

		
				//Si el producto tiene variantes definidas
				if(isset($product->product_variants)){
  			
  					$this->createOrUpdateProductVariants($product,$product->product_variants);

				}

				if(isset($product->variants)){

					$this->createVariants($product->ProductId,$product->variants);

				}

				if(isset($product->feature_groups)){

					$this->createFeatures($product,$product->feature_groups);

				}

				if(isset($product->pictures)){
					$this->createPictures($product);
				}

	


		$this->db->conn->commit();


		return $product;
		} 
		catch(Exception $ex){
			
			$this->db->conn->rollback();
			throw new Exception($ex);
			  
		}	
	}
	
	public function createProductReview($review){

		try{
		
				$params = [];

				$sql = "INSERT INTO product_reviews ( product_id, user_id, comment, rating) VALUES ( :product_id, :user_id,:comment,:rating);";
				$params[":product_id"] = $review->product_id;
				$params[":user_id"]    = getOwnerId();
				$params[":comment"] = $review->comment;
				$params[":rating"] = $review->rating;
				
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($params);

				$review->id =(int) $this->db->conn->lastInsertId();


		return $review;
		} 
		catch(Exception $ex){
			
			
			throw new Exception($ex);
			  
		}	
	}

	public function syncProducts($products){

		try{
				$this->db->conn->beginTransaction();


				$sql = "
					INSERT INTO erp_products(
						
						product_code, 
						product_name, 
						product_description, 
						
						item_id, 
						currency_id, 
						product_cost_price, 
						product_price, 
						product_stock, 
						product_um, 
						product_last_sale_date,
					 
						
					
					
						product_ecommerce_enabled, 
						
						product_external_code, 
						product_external_name
					) VALUES (	
						
						:ProductCode, 
						:ProductName, 
						:ProductDescription, 
						
						1,
						:CurrencyId,
						:ProductCostPrice, 
						:ProductPrice, 
						:ProductStock, 
						:ProductUM,
						:ProductLastSaleDate,
					
						
						1,
						
						:ProductExternalCode, 
						:ProductExternalName
					)";
		
				$sql_search = "SELECT * FROM erp_products where product_code=:ProductCode";
			
				$CategoryModel = new CategoryModel();
				
				foreach ($products as $product) {
					
					
					
					//$category = $CategoryModel->getCategoryByCode($product->ProductLineCode);
					
					//$product->CategoryId = $category["CategoryId"];
					
					
					
					
					
					//Verificar si el producto ya existe en la base de datos
					$statement = $this->db->conn->prepare($sql_search);
					$statement->setFetchMode(PDO::FETCH_OBJ);
					$statement->execute(array(':ProductCode' => $product->ProductCode));
			
			        $result = $statement->fetch(PDO::FETCH_OBJ);
			         
			         
					 
					 if(isset($result->product_id)){
					 	
					 	$product->ProductId = $result->product_id;
					 	
					 	
					 	//Si el producto ya existe entonces verificar el campo de la ultima venta, si el campo de la ultima venta es mayor
						//al que tenemos en la base de datos entonces hay que actualizar el stock
						
						
					 	$product_last_sale_date_db = new DateTime($result->product_last_sale_date);
					 	$product_last_sale_date = new DateTime($product->ProductLastSaleDate) ; 
					 	
					 	if( ($product_last_sale_date > $product_last_sale_date_db) ){
					 		
					 		 $sql_update = "UPDATE erp_products SET product_stock = :ProductStock, product_last_sale_date =:ProductLastSaleDate, product_lm_date = CURRENT_TIMESTAMP WHERE product_id = :ProductId ";
					 		//Verificar si el producto ya existe en la base de datos
							$statement = $this->db->conn->prepare($sql_update);
							$statement->setFetchMode(PDO::FETCH_OBJ);
							$statement->execute(array(':ProductId' => $product->ProductId, ':ProductStock' => $product->ProductStock, ':ProductLastSaleDate' => $product->ProductLastSaleDate));
					
					 	}
					 
					 	if($result->product_price <> $product->ProductPrice ){
					 		
					 		$sql_update = "UPDATE erp_products SET product_price = :ProductPrice WHERE product_id = :ProductId ";
					 		//Verificar si el producto ya existe en la base de datos
							$statement = $this->db->conn->prepare($sql_update);
							$statement->setFetchMode(PDO::FETCH_OBJ);
							$statement->execute(array(':ProductId' => $product->ProductId, ':ProductPrice' => $product->ProductPrice));
					 	}
					 	
					 	if($result->product_weight <> $product->ProductWeight ){
					 		
					 		$sql_update = "UPDATE erp_products SET product_weight = :ProductWeight WHERE product_id = :ProductId ";
					 		//Verificar si el producto ya existe en la base de datos
							$statement = $this->db->conn->prepare($sql_update);
							$statement->setFetchMode(PDO::FETCH_OBJ);
							$statement->execute(array(':ProductId' => $product->ProductId, ':ProductWeight' => $product->ProductWeight));
					 	}
					 	
					 	if($result->product_length <> $product->ProductLength	 ){
					 		
					 		 $sql_update = "UPDATE erp_products SET product_length = :ProductLength	 WHERE product_id = :ProductId ";
					 		//Verificar si el producto ya existe en la base de datos
							$statement = $this->db->conn->prepare($sql_update);
							$statement->setFetchMode(PDO::FETCH_OBJ);
							$statement->execute(array(':ProductId' => $product->ProductId, ':ProductLength' => $product->ProductLength	));
					 	}
					 	
					 	if($result->product_width <> $product->ProductWidth	 ){
					 		
					 		$sql_update = "UPDATE erp_products SET product_width = :ProductWidth	 WHERE product_id = :ProductId ";
					 		//Verificar si el producto ya existe en la base de datos
							$statement = $this->db->conn->prepare($sql_update);
							$statement->setFetchMode(PDO::FETCH_OBJ);
							$statement->execute(array(':ProductId' => $product->ProductId, ':ProductWidth' => $product->ProductWidth	));
					 	}
					 	
					 	if($result->product_height <> $product->ProductHeight	 ){
					 		
					 		$sql_update = "UPDATE erp_products SET product_height = :ProductHeight	 WHERE product_id = :ProductId ";
					 		//Verificar si el producto ya existe en la base de datos
							$statement = $this->db->conn->prepare($sql_update);
							$statement->setFetchMode(PDO::FETCH_OBJ);
							$statement->execute(array(':ProductId' => $product->ProductId, ':ProductHeight' => $product->ProductHeight	));
					 	}
					 	
					 	if($result->product_name <> $product->ProductName	 ){
					 		
					 		$sql_update = "UPDATE erp_products SET product_name = :ProductName	 WHERE product_id = :ProductId ";
					 		//Verificar si el producto ya existe en la base de datos
							$statement = $this->db->conn->prepare($sql_update);
							$statement->setFetchMode(PDO::FETCH_OBJ);
							$statement->execute(array(':ProductId' => $product->ProductId, ':ProductName' => $product->ProductName	));
					 	}
					 	
					 	
					 	
					 	
					 	
					 	/*if( $result->category_id <> $category["CategoryId"] ){
					 		
					 		 $sql_update = "UPDATE erp_products SET category_id = :CategoryId WHERE product_id = :ProductId ";
					 		//Verificar si el producto ya existe en la base de datos
							$statement = $this->db->conn->prepare($sql_update);
							$statement->setFetchMode(PDO::FETCH_OBJ);
							$statement->execute(array(':ProductId' => $product->ProductId,':CategoryId' => $category["CategoryId"]));
					
					 	}
					 	*/
					 	
					 	
					 	
					 	
					 	
					 }
					 else{
		 
		 
					
						
						
						//var_dump($product);
						$params =[];
	
						//if(isset($product->ProductId))	 { $params[':ProductId'] = $product->ProductId; } else { $params[':ProductId'] = NULL; }; 
						if(isset($product->ProductCode))	 { $params[':ProductCode'] = $product->ProductCode; } else { $params[':ProductCode'] = NULL; }; 
						if(isset($product->ProductName))	 { $params[':ProductName'] = $product->ProductName; } else { $params[':ProductName'] = NULL; }; 
						if(isset($product->ProductDescription))	 { $params[':ProductDescription'] = $product->ProductDescription; } else { $params[':ProductDescription'] = NULL; }; 
						
						if(isset($product->CurrencyId))	 { $params[':CurrencyId'] = $product->CurrencyId; } else { $params[':CurrencyId'] = NULL; }; 
						if(isset($product->ProductCostPrice))	 { $params[':ProductCostPrice'] = $product->ProductCostPrice; } else { $params[':ProductCostPrice'] = NULL; }; 
						if(isset($product->ProductPrice))	 { $params[':ProductPrice'] = $product->ProductPrice; } else { $params[':ProductPrice'] = NULL; }; 
						if(isset($product->ProductStock))	 { $params[':ProductStock'] = $product->ProductStock; } else { $params[':ProductStock'] = NULL; }; 
						if(isset($product->ProductUM))	 { $params[':ProductUM'] = $product->ProductUM; } else { $params[':ProductUM'] = NULL; }; 
						if(isset($product->ProductLastSaleDate))	 { $params[':ProductLastSaleDate'] = $product->ProductLastSaleDate; } else { $params[':ProductLastSaleDate'] = NULL; }; 
						//if(isset($product->ProductHasVariants))	 { $params[':ProductHasVariants'] = $product->ProductHasVariants; } else { $params[':ProductHasVariants'] = NULL; }; 
						//if(isset($product->ProductVariantProductId))	 { $params[':ProductVariantProductId'] = $product->ProductVariantProductId; } else { $params[':ProductVariantProductId'] = NULL; }; 
						
						if(isset($product->ProductExternalCode))	 { $params[':ProductExternalCode'] = $product->ProductExternalCode; } else { $params[':ProductExternalCode'] = NULL; }; 
						if(isset($product->ProductExternalName))	 { $params[':ProductExternalName'] = $product->ProductExternalName; } else { $params[':ProductExternalName'] = NULL; }; 
						
						
						
						$statement = $this->db->conn->prepare($sql);
						$statement->setFetchMode(PDO::FETCH_OBJ);
						$statement->execute($params);
	
						$product->ProductId =(int) $this->db->conn->lastInsertId();
					 }
					 
				}
				
		

	


		$this->db->conn->commit();


		return $products;
		} 
		catch(Exception $ex){
			
			$this->db->conn->rollback();
			throw new Exception($ex);
			  
		}	
	}

    public function createVariants($ProductId,$variants)  {
    	$sql = "
    		INSERT INTO erp_product_variants(product_id, variant_id) VALUES (:ProductId,:VariantId)
    	";

    	foreach ($variants as $variant) {

    		$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute(array( ":ProductId" => $ProductId, ":VariantId" => $variant->VariantId));

			if(isset($variant->values)){

				$sql_values ="
				INSERT INTO `erp_product_variant_details`(`product_id`, `variant_id`, `variant_detail_value`) VALUES (:ProductId,:VariantId,:VariantDetailValue)
				";
				foreach ($variant->values as $value) {
					$statement = $this->db->conn->prepare($sql_values);
					$statement->setFetchMode(PDO::FETCH_OBJ);
					$statement->execute(array(":ProductId" => $ProductId, ":VariantId"=>$variant->VariantId,":VariantDetailValue" => $value));
				}
			}

    	}


    }


    public function createPictures($product){

    	$sql_insert ="
    		INSERT INTO `erp_product_pictures`(
    			`product_id`, 
    			`product_picture_id`, 
    			`product_picture_url`,
    			`product_picture_default`,
    			`product_picture_thumbnail`
    		) VALUES (
    			:ProductId,
    			:ProductPictureId,
    			:ProductPictureUrl,
    			:ProductPictureDefault,
    			:ProductPictureThumbnail
    		)";

    	$sql_update="
    		UPDATE `erp_product_pictures` SET 
    			`product_picture_url`=:ProductPictureUrl ,
    			`product_picture_default` = :ProductPictureDefault,
    			`product_picture_thumbnail` = :ProductPictureThumbnail
    		WHERE `product_id`=:ProductId and `product_picture_id`=:ProductPictureId
    	";

    	foreach($product->pictures as $picture){
    		if(isset($picture->ProductPictureUrl)){

    			$sql = $sql_insert;

	    		if(isset($picture->ProductPictureId)){ 
	    			$sql = $sql_update; 
	    		} else { 

	    			$picture->ProductPictureId = $this->getProductPictureIdentity($product->ProductId); 

	    		}
	    		

	    		$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(":ProductId" => $product->ProductId,":ProductPictureId" => $picture->ProductPictureId ,":ProductPictureUrl"=> $picture->ProductPictureUrl,":ProductPictureThumbnail"=> $picture->ProductPictureThumbnail,':ProductPictureDefault' => $picture->ProductPictureDefault));
    		}
    		

			
    	}
    }
    
    public function createPicture($ProductId,$picture){

    	$sql_insert ="
    		INSERT INTO `erp_product_pictures`(
    			`product_id`, 
    			`product_picture_id`, 
    			`product_picture_url`,
    			`product_picture_default`,
    			`product_picture_thumbnail`
    		) VALUES (
    			:ProductId,
    			:ProductPictureId,
    			:ProductPictureUrl,
    			:ProductPictureDefault,
    			:ProductPictureThumbnail
    		)";

    	$sql_update="
    		UPDATE `erp_product_pictures` SET 
    			`product_picture_url`=:ProductPictureUrl ,
    			`product_picture_default` = :ProductPictureDefault,
    			`product_picture_thumbnail` = :ProductPictureThumbnail
    		WHERE `product_id`=:ProductId and `product_picture_id`=:ProductPictureId
    	";

    
    		if(isset($picture->ProductPictureUrl)){

    			$sql = $sql_insert;

	    		if(isset($picture->ProductPictureId)){ 
	    			$sql = $sql_update; 
	    		} else { 

	    			$picture->ProductPictureId = $this->getProductPictureIdentity($ProductId); 

	    		}
	    		

	    		$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(":ProductId" => $ProductId,":ProductPictureId" => $picture->ProductPictureId ,":ProductPictureUrl"=> $picture->ProductPictureUrl,":ProductPictureThumbnail"=> $picture->ProductPictureThumbnail,':ProductPictureDefault' => $picture->ProductPictureDefault));
    		}
    		

			
    	
    }

    private function removePictures($product){



    	$sql = "
    		DELETE FROM  `erp_product_pictures`
    		WHERE `product_id`=:ProductId and `product_picture_id`=:ProductPictureId
    	";

    	foreach($product->deleted_pictures as $picture){
    		
	    		

	    		$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(":ProductId" => $product->ProductId,":ProductPictureId" => $picture->ProductPictureId ));		
    	}
    }

    private function getProductPictureIdentity($product_id){
		
		$ProductPictureId = 1;

		$sql = "
			SELECT MAX(  `product_picture_id` )+1 as identity
			FROM  `erp_product_pictures` 
			WHERE  `product_id` =:ProductId ";

		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':ProductId' => $product_id));

         $result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->identity)){
		 	$ProductPictureId = $result->identity;
		 }



		return $ProductPictureId;
		
			
    }

    private function createFeatures($product,$feature_groups){


		$sql_insert = "INSERT INTO `erp_product_features`(`product_id`, `feature_group_id`, `feature_id`) VALUES (:ProductId,:FeatureGroupId,:FeatureId)
    		";

    	$sql_delete="DELETE FROM `erp_product_features` WHERE `product_id`=:ProductId AND `feature_group_id`=:FeatureGroupId AND `feature_id`=:FeatureId ";

        $sql = "";
        foreach ($feature_groups as $feature_group) {
        
	    	if($feature_group->features){

	    		foreach ($feature_group->features as $feature) {

	    			if(isset($feature->FeatureSelected)){

	    				if($feature->FeatureSelected && !isset($feature->ProductId) ){
	    					$sql=$sql_insert;
	    				}
	    				else{
	    					if(!$feature->FeatureSelected){
	    						$sql=$sql_delete;
	    					}
	    					
	    				}

                        if($sql != ""){
                        	//echo $sql;
	    					$statement = $this->db->conn->prepare($sql);
							$statement->setFetchMode(PDO::FETCH_OBJ);
							$statement->execute(array(":ProductId" => $product->ProductId,":FeatureGroupId" => $feature_group->FeatureGroupId ,":FeatureId"=> $feature->FeatureId));
							$feature->ProductId = $product->ProductId;
						}

	    			}
	    		}
	    	}


        }
    }

	public function createOrUpdateProductVariants($product,$product_variants){

		//var_dump($product);

		$sql_insert="
			INSERT INTO `erp_products`(
			`product_id`, 
			`product_code`, 
			`product_name`, 
			`product_description`, 
			`category_id`, 
			`product_cost_price`, 
			`product_price`, 
			`product_stock`, 
			`product_um`, 
			`product_has_variants`, 
			`product_variant_product_id`,
			`item_id`,
			`product_is_saleable`
		) VALUES (
			:ProductId, 
			:ProductCode, 
			:ProductName, 
			:ProductDescription, 
			:CategoryId, 
			:ProductCostPrice, 
			:ProductPrice, 
			:ProductStock, 
			:ProductUM, 
			:ProductHasVariants, 
			:ProductVariantProductId,
			1,
			:ProductIsSaleable
			
		)	
		";

		$sql_update = "

			UPDATE `erp_products` SET 			
				`product_code` = :ProductCode,
				`product_name` =:ProductName, 
				`product_description` =:ProductDescription,  
				`category_id` = :CategoryId, 
				`product_cost_price`= :ProductCostPrice,  
				`product_price`=:ProductPrice,  
				`product_stock` = :ProductStock,  
				`product_um` = :ProductUM, 
				`product_has_variants`  = :ProductHasVariants, 
				`product_variant_product_id`  = :ProductVariantProductId,
				`product_is_saleable` = :ProductIsSaleable,
				product_featured = :ProductFeatured,
				product_ecommerce_enabled = :ProductEcommerceEnabled
			WHERE `product_id` = :ProductId			
		";


			foreach ($product_variants as $product_variant){

				if(!isset($product_variant->ProductId))
				{
					$sql = $sql_insert;
				}
				else{
					$sql = $sql_update;
				}
								
				$product_variant->ProductVariantProductId = $product->ProductId;

				if(isset($product->CategoryId )) { $product_variant->CategoryId = $product->CategoryId;};
				if(isset($product->ProductUM )) { $product_variant->ProductUM = $product->ProductUM ;};

				$product_variant->ProductHasVariants = 0;
				
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($product_variant));

				if(!isset($product_variant->ProductId)){
					$product_variant->ProductId = (int) $this->db->conn->lastInsertId();
				}

				if(isset($product_variant->Combinations)){
					$this->createProductVariantCombinations($product_variant->ProductId,$product_variant->Combinations);
				}


				if(isset($product_variant->pictures)){
					$this->createPictures($product_variant,$product_variant->pictures);
				}
				
			}		

	}

	private function createProductVariantCombinations($ProductId,$combinations){
		$sql_insert="
			INSERT INTO `erp_product_variant_values`(
				`product_id`, 
				`variant_id`, 
				`product_variant_value`
			) VALUES (
				:ProductId,
				:VariantId,
				:VariantValueName
			)";

		$sql_update ="
			UPDATE `erp_product_variant_values` SET 
	
				`product_variant_value`=:VariantValueName
			WHERE `product_id`=:ProductId AND `variant_id` = :VariantId AND `variant_value_id` = :VariantValueId
			";

		foreach ($combinations as $combination) {

			if(!isset($combination->VariantValueId)){

				$combination->VariantValueId = 0 ;
				$sql = $sql_insert;

			}else{
				$sql = $sql_update;
			}

			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute(array(":ProductId" => $ProductId,":VariantId" => $combination->VariantId,":VariantValueName" => $combination->VariantValueName));

			
		}

	}

	public function getProduct($ProductId,$detailed=false){

		$sql = $this->product_select_sql;

		$result = $this->db->query($sql." WHERE `product_id` = :ProductId",
			array(':ProductId' => $ProductId )
		);

		$product = $result->fetch(PDO::FETCH_OBJ);
        
       return $this->convertProduct($product,$detailed);

	}

	public function getProductByCode($ProductCode,$detailed=false){

		$sql = $this->product_select_sql;

		$result = $this->db->query($sql." WHERE `product_code` = :ProductCode ORDER BY `product_id` LIMIT 1",
			array(':ProductCode' => $ProductCode )
		);

		$product = $result->fetch(PDO::FETCH_OBJ);
        
       return $this->convertProduct($product,$detailed);

	}

	public function updateProduct($product){


		if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();


		$sql = "

			UPDATE erp_products SET 					
				product_code=:ProductCode,
				product_name=:ProductName,
				product_description=:ProductDescription,
				category_id=:CategoryId,
				product_cost_price=:ProductCostPrice,
				product_price=:ProductPrice,
				product_stock=:ProductStock,
				product_um=:ProductUM,
				product_has_variants=:ProductHasVariants,
				product_variant_product_id=:ProductVariantProductId,
				product_is_saleable=:ProductIsSaleable,
				product_featured =:ProductFeatured,
				product_ecommerce_enabled= :ProductEcommerceEnabled
			WHERE product_id=:ProductId";

			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute($this->fillParams($product));



			if(isset($product->product_variants)){
  				
  					$this->createOrUpdateProductVariants($product,$product->product_variants);
  			}

  			if(isset($product->deleted_pictures)){

					$this->removePictures($product);

			}

  			if(isset($product->pictures)){
					$this->createPictures($product,$product->pictures);
				}

			if(isset($product->feature_groups)){

					$this->createFeatures($product,$product->feature_groups);

				}


			

			$this->db->conn->commit();

			return $product;
	}

	public function deleteProduct($ProductId) {


		$this->db->conn->beginTransaction();

		$params = array(":ProductId" => $ProductId);



		$sql="DELETE FROM `erp_product_features` WHERE `product_id` = :ProductId";

		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute($params);

		$sql="DELETE FROM `erp_product_variant_details` WHERE `product_id` = :ProductId"; 

		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute($params);

		$sql="DELETE FROM `erp_product_variant_values` WHERE `product_id` = :ProductId";

		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute($params);

		$sql="DELETE FROM `erp_product_variants` WHERE `product_id` = :ProductId";

		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute($params);


		$sql="DELETE FROM `erp_products` WHERE `product_id` = :ProductId OR `product_variant_product_id`=:ProductId";

		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute($params);


		$this->db->conn->commit();

	}

	private function fillParams($product){
		$params =[];

		if(isset($product->ProductId))	 { $params[':ProductId'] = $product->ProductId; } else { $params[':ProductId'] = NULL; }; 
		if(isset($product->ProductCode))	 { $params[':ProductCode'] = $product->ProductCode; } else { $params[':ProductCode'] = NULL; }; 
		if(isset($product->ProductName))	 { $params[':ProductName'] = $product->ProductName; } else { $params[':ProductName'] = NULL; }; 
		if(isset($product->ProductDescription))	 { $params[':ProductDescription'] = $product->ProductDescription; } else { $params[':ProductDescription'] = NULL; }; 
		if(isset($product->CategoryId))	 { $params[':CategoryId'] = $product->CategoryId; } else { $params[':CategoryId'] = NULL; }; 
		if(isset($product->ProductCostPrice))	 { $params[':ProductCostPrice'] = $product->ProductCostPrice; } else { $params[':ProductCostPrice'] = NULL; }; 
		if(isset($product->ProductPrice))	 { $params[':ProductPrice'] = $product->ProductPrice; } else { $params[':ProductPrice'] = NULL; }; 
		if(isset($product->ProductStock))	 { $params[':ProductStock'] = $product->ProductStock; } else { $params[':ProductStock'] = NULL; }; 
		if(isset($product->ProductUM))	 { $params[':ProductUM'] = $product->ProductUM; } else { $params[':ProductUM'] = NULL; }; 
		if(isset($product->ProductHasVariants))	 { $params[':ProductHasVariants'] = $product->ProductHasVariants; } else { $params[':ProductHasVariants'] = NULL; }; 
		if(isset($product->ProductVariantProductId))	 { $params[':ProductVariantProductId'] = $product->ProductVariantProductId; } else { $params[':ProductVariantProductId'] = NULL; }; 
		if(isset($product->ProductIsSaleable))	 { $params[':ProductIsSaleable'] = $product->ProductIsSaleable; } else { $params[':ProductIsSaleable'] = NULL; }; 
		if(isset($product->ProductFeatured))	 { $params[':ProductFeatured'] = $product->ProductFeatured; } else { $params[':ProductFeatured'] = NULL; }; 
		if(isset($product->ProductEcommerceEnabled))	 { $params[':ProductEcommerceEnabled'] = $product->ProductEcommerceEnabled; } else { $params[':ProductEcommerceEnabled'] = NULL; }; 
		

		return $params;
	
	}

	private function convertVariant($variant){

		$array =  array(
			"VariantId" => $variant->variant_id,
			"VariantName" => $variant->variant_name
		);

		$array["VariantValues"] = $this->getVariantValues($variant->variant_id);

		return $array;
	}


	private function convertProduct($product,$detailed=false,$search_result = false){


	
		if(isset($product->category_external_link)){
			
			
			$product->category_external_link  = str_replace("{{product_external_id}}",$product->product_external_id,$product->category_external_link);
			
		} else{
			
			$product->category_external_link = null;
		}
		
			
		$ProductArray = array(
				"ProductId" => $product->product_id, 
				"ProductCode" => $product->product_code, 
				"ProductName" => $product->product_name, 
				"ProductDescription" => $product->product_description, 
				"CategoryId" => $product->category_id, 
				"ProductCostPrice" => $product->product_cost_price, 
				"ProductPrice" => $product->product_price, 
				"ProductStock" => $product->product_stock, 
				"ProductUM" => $product->product_um, 
				"ProductHasVariants" => (bool )$product->product_has_variants, 
				"ProductVariantProductId" => $product->product_variant_product_id,
				"ProductFeatured" => (boolean)$product->product_featured,
				"ProductEcommerceEnabled" => (boolean)$product->product_ecommerce_enabled,
				
				"ProductExtendedDescription" => $product->product_code." ".$product->product_name,
				"ProductIsSaleable" => (boolean) $product->product_is_saleable,
				"ProductExternalId" => $product->product_external_id,
				"ProductExternalCode" => $product->product_external_code,
				"ProductExternalName" => $product->product_external_name,
				"CategoryExternalLink" => $product->category_external_link
		);

		if($detailed){

			$ProductArray["feature_groups"] = $this->getProductFeatures($product->product_id);

			if($product->product_has_variants>0){
				$ProductArray["product_variants"] = $this->getProductVariants($product->product_id);
			}

			$ProductArray["pictures"] = $this->getProductPictures($product->product_id);
		
			$ProductArray["reviews"] = $this->getProductReviews($product->product_id);
		

		}
		
		if($this->hide_prices){
			
			$ProductArray["ProductPrice"] = 0;
			
		}

		$ProductArray["default_picture"] = $this->getProductDefaultPicture($product->product_id);
		
		if(isset($product->category_external_image_site)){
			
			$ProductArray["default_picture"]["ProductPictureId"] = 99;
			$ProductArray["default_picture"]["ProductPictureUrl"] = str_replace("{{product_external_id}}",$product->product_external_id,$product->category_external_image_site);
			$ProductArray["default_picture"]["ProductPictureThumbnail"] = str_replace("{{product_external_id}}",$product->product_external_id,$product->category_external_image_site);

			
		} else{
			
			$product->category_external_image_site = null;
		}
		
		

		return $ProductArray;

	}

public function getProductReviews($ProductId) {

		$sql ="
			SELECT r.*, (UNIX_TIMESTAMP(r.created) * 1000) as created_date ,u.user_name
			FROM `product_reviews`  r ,oauth_users u
			WHERE 
				`product_id` = :ProductId
				AND u.user_id = r.user_id
		";

		$result = $this->db->query($sql,array( ":ProductId" => $ProductId));
    	$reviews = $result->fetchAll(PDO::FETCH_OBJ);

   

		return $reviews;
	}

	//Product Pictures
	public function getProductPictures($ProductId) {

		$sql ="
			SELECT 
				`product_id`, 
				`product_picture_id`, 
				`product_picture_url`,
				`product_picture_default`,
				`product_picture_thumbnail`
			FROM `erp_product_pictures` 
			WHERE 
				`product_id` = :ProductId
		";

		$result = $this->db->query($sql,array( ":ProductId" => $ProductId));
    	$product_pictures = $result->fetchAll(PDO::FETCH_OBJ);

    	$pictures =[];

		foreach ($product_pictures as $product_picture){
			array_push($pictures, $this->convertProductPicture($product_picture));
		}

		return $pictures;
	}

	public function getProductPicture($ProductId,$ProductPictureId) {

		$sql ="
			SELECT 
				`product_id`, 
				`product_picture_id`, 
				`product_picture_url`,
				`product_picture_default`,
				`product_picture_thumbnail`
			FROM `erp_product_pictures` 
			WHERE 
				`product_id` = :ProductId
			AND `product_picture_id` = :ProductPictureId
		";

		$result = $this->db->query($sql,array( ":ProductId" => $ProductId , ":ProductPictureId" => $ProductPictureId));
    	$product_picture = $result->fetch(PDO::FETCH_OBJ);

    	$picture = $this->convertProductPicture($product_picture);
		

		return $picture;
	}

	public function getProductDefaultPicture($ProductId) {

		$sql ="
			SELECT 
				`product_id`, 
				`product_picture_id`, 
				`product_picture_url`,
				`product_picture_default`,
				`product_picture_thumbnail`
			FROM `erp_product_pictures` 
			WHERE 
				`product_id` = :ProductId
			
			AND `product_picture_default` = 1
		";

		$result = $this->db->query($sql,array( ":ProductId" => $ProductId ));

		$product_picture = $result->fetch(PDO::FETCH_OBJ);

		//var_dump($product_picture);

		if(!isset($product_picture->product_picture_id)){

			$sql ="
			SELECT 
				`product_id`, 
				`product_picture_id`, 
				`product_picture_url`,
				`product_picture_default`,
				`product_picture_thumbnail`
			FROM `erp_product_pictures` 
			WHERE 
				`product_id` = :ProductId			
			LIMIT 1
			";

			$result = $this->db->query($sql,array( ":ProductId" => $ProductId ));

			$product_picture = $result->fetch(PDO::FETCH_OBJ);

		}

    	//$product_picture = $result->fetch(PDO::FETCH_OBJ);

		//var_dump($product_picture);
    	$picture = $this->convertProductPicture($product_picture);
		

		return $picture;
	}

	private function convertProductPicture($product_picture){

		return array(
			'ProductPictureId' => $product_picture->product_picture_id,
			'ProductPictureUrl' => '/backend/' . $product_picture->product_picture_url,
			'ProductPictureDefault'=> $product_picture->product_picture_default,
			'ProductPictureThumbnail'=> '/backend/' .$product_picture->product_picture_thumbnail
			
			);
	}

	private function getProductFeatures($ProductId){
 

		//Selecccionamos los diferentes grupos de features en la tabla de productos
    	$sql = "SELECT DISTINCT  `product_id` ,  `feature_group_id` FROM  `erp_product_features` WHERE `product_id` = :ProductId";


    	$result = $this->db->query($sql,array( ":ProductId" => $ProductId));
    	$product_feature_groups = $result->fetchAll(PDO::FETCH_OBJ);
        
        $feature_groups = [];

        $FeatureModel = new FeatureModel();


        //Con este query marcaremos el feature como seleccionado si es que existe una asociacion con un articulo
        $sql = "SELECT  1  as `FeatureSelected`  FROM `erp_product_features` WHERE `product_id` = :ProductId AND `feature_group_id` =:FeatureGroupId AND `feature_id`=:FeatureId";

        foreach ($product_feature_groups as $product_feature_group) {

        	$feature_group = $FeatureModel->getFeatureGroup($product_feature_group->feature_group_id);

        	
            
        	for( $i=0; $i < count($feature_group["features"]);$i++) {

        		 $feature = $feature_group["features"][$i];
        		
        		$result = $this->db->query($sql,
        			array( 
        				":ProductId" => $ProductId , 
        				":FeatureGroupId" => $feature_group["FeatureGroupId"],
        				":FeatureId" => $feature["FeatureId"]
        			)
        		);

        		$object = $result->fetch(PDO::FETCH_OBJ);
        		//var_dump($object);

        		if(isset($object->FeatureSelected)) { 	


        			

        			$feature["FeatureSelected"] = (boolean)$object->FeatureSelected;

        			if((boolean)$object->FeatureSelected){
        				$feature["ProductId"] = $ProductId;
        			}
        			
        		} 

                $feature_group["features"][$i] = $feature;
        		//var_dump($feature);
        	}

        	array_push($feature_groups,$feature_group);
        
        }



		return $feature_groups;

	}

    public function importProducts($file){
    	//Leer el archivo
     $file = "c:/temp/products.csv";

		  
		$importer = new CsvImporter($file,true); 
		$data = $importer->get();

		$line_counter = 1;
		//print_r($data); 
        foreach ($data as $item) {
        	
        	$line =  $item["ProductCode,ProductName,ProductDescription,ProductCostPrice,ProductPrice,ProductManageStock,VariantName,VariantValue"];
        	$array = str_getcsv($line);
        	echo $line_counter ." ".$line."<br>";
        	$line_counter++;

        	if(isset($array[ProductFields::ProductCode])){

        		//Crear un producto
        			//echo "Hay codigo". $array[ProductFields::ProductName];

    			$NewProduct = new stdClass();
    			$NewProduct->ProductCode = $array[ProductFields::ProductCode];
    			$NewProduct->ProductName = $array[ProductFields::ProductName];
    			$NewProduct->ProductDescription =$array[ProductFields::ProductDescription];
    			$NewProduct->ProductCostPrice = $array[ProductFields::ProductCostPrice];
    			$NewProduct->ProductPrice= $array[ProductFields::ProductPrice];
    			$NewProduct->ProductManageStock = $array[ProductFields::ProductManageStock];
    			$NewProduct->VariantName = $array[ProductFields::VariantName];
    			$NewProduct->VariantValueName = $array[ProductFields::VariantValue];
                ;


        		//Buscar si el producto existe,
        		$Product = json_decode(json_encode($this->getProductByCode($NewProduct->ProductCode)),False);

        		//var_dump($Product);


        		//Si el codigo del producto no existe, entonces insertamos el registro
        		if(!isset($Product->ProductId)){
        			
                    $NewProduct->ProductVariantProductId = 0;
        			$this->createProduct($NewProduct);
        			
        		}
        		else {
        			//Si el codigo ya existen entonces entonces revisamos si el producto
        			//se va a guardar como una variante
        			//echo "----".$NewProduct->VariantName."----";

        			if($NewProduct->VariantName=="")
        			{
                        
                           
                        $Product->ProductName  = $NewProduct->ProductName;
                        $Product->ProductDescription =$NewProduct->ProductDescription;
                        $Product->ProductPrice = $NewProduct->ProductPrice;
                        $Product->ProductCostPrice = $NewProduct->ProductPrice;
                        $Product->ProductManageStock = $NewProduct->ProductManageStock;
        				$this->updateProduct($Product);

        			}
        			else{

        				 $VariantModel = new VariantModel();
        				 $variant = new stdClass();
        				 $variant = json_decode(json_encode($VariantModel->getVariantByName($NewProduct->VariantName)),False);

        				
        				if(!isset($variant->VariantId)){
      					
        					$variant = new stdClass();
        					$variant->VariantName = $NewProduct->VariantName;

        					$variant = $VariantModel->createVariant($variant);

        					$variant_value = new stdClass();
        					$variant_value->VariantId = $variant->VariantId;
        					$variant_value->VariantValueName = $NewProduct->VariantValueName;
        					$variant_value = $VariantModel->createVariantValue($variant_value);

        					$variant->variant_values = array( $variant_value);
        					
						}
        				else{

        					$variant_found = 0;
        					$variant_values = json_decode(json_encode($VariantModel->getVariantValues($variant->VariantId)),false);
        				
        					$variant_value = new stdClass();
        					$variant_pictures = [];
                            $variant_combinations = [];

        					foreach ($variant_values as $variant_value_item) {

        						//var_dump($variant_value_item);
        						
        						if( $variant_value_item->VariantValueName == $NewProduct->VariantValueName )
        						{
        							
        							$variant_value->VariantId 		 = $variant_value_item->VariantId;
        							$variant_value->VariantValueId   = $variant_value_item->VariantValueId;
        							$variant_value->VariantValueName = $variant_value_item->VariantValueName;
                                    
                                    if(isset($variant_value_item->VariantValuePictureUrl)){
                                        
                                        $product_picture = new stdClass();
                                        $product_picture->ProductPictureUrl = $variant_value_item->VariantValuePictureUrl;
                                        $product_picture->ProductPictureThumbnail = $variant_value_item->VariantValuePictureUrl;
                                        
                                        array_push($variant_pictures,$product_picture);
                                    }
                                    
                                    $combination = new stdClass();
                                    $combination->VariantId =$variant_value_item->VariantId; 
                                    $combination->VariantValueName =$variant_value_item->VariantValueName; 


                                    array_push($variant_combinations,$combination);

                                       
                                     
                                    
                                    
        							$variant_found = 1;
        							break;
        						}

        					}

        					//If the variant wasnt found then we create the record
        					if($variant_found==0){
        						
        						$variant_value->VariantId = $variant->VariantId;
        						$variant_value->VariantValueName = $NewProduct->VariantValueName;
        						$variant_value= $VariantModel->createVariantValue($variant_value);
                                
                                
                                 $combination = new stdClass();
                                 $combination->VariantId =$variant->VariantId; 
                                 $combination->VariantValueName =$NewProduct->VariantValueName; 
                                        
                                        
                                 array_push($variant_combinations,$combination);
        					
        					}

        					$variant->variant_values = array($variant_value);                            
                            $variant->pictures = $variant_pictures;
                            $variant->combinations = $variant_combinations;

        				}

        				
        				$Product->product_variants =[];

        				foreach ($variant->variant_values  as $variant_value) {
        					
	        				
	        				$NewProductVariant = new stdClass();
	    					$NewProductVariant->ProductCode = $Product->ProductCode;
	    					$NewProductVariant->ProductName = $array[ProductFields::ProductName].", ".$variant_value->VariantValueName;
	    					$NewProductVariant->ProductDescription =$array[ProductFields::ProductDescription];
	    					$NewProductVariant->ProductCostPrice = $array[ProductFields::ProductCostPrice];
	    					$NewProductVariant->ProductPrice= $array[ProductFields::ProductPrice];
	    					$NewProductVariant->ProductManageStock = $array[ProductFields::ProductManageStock];

    						$NewProductVariant->ProductVariantProductId = $Product->ProductId;
                             
                            if(isset( $variant->pictures )){
                                $NewProductVariant->pictures = $variant->pictures;
                            }
                            
                            if(isset($variant->combinations)){
                                $NewProductVariant->Combinations = $variant->combinations;
                            }

    						array_push($Product->product_variants,$NewProductVariant);


						}
    					

    					
    					$Product->ProductHasVariants= 1;
                        $Product->ProductVariantProductId = 0;
    					$this->updateProduct($Product);

    					




    					
        			}
        		}
        		
        		
        		
        	}
           
        	//array str_getcsv ( string $input [, string $delimiter = "," [, string $enclosure = '"' [, string $escape = "\\" ]]] )
        }


        
				

    	
    	//Ver si el primer campo tiene id
    	//Ver si
    	//
    }
}

abstract class ProductFields
{
    const ProductCode  = 0;
    const ProductName  = 1;
    const ProductDescription = 2;
  
    const ProductCostPrice = 3;
    const ProductPrice = 4;
    const ProductManageStock = 5;
    const VariantName = 6;
    const VariantValue = 7;
    
}
?>