<?php
class brandModel {

	private $db;
	
	

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getbrands(){

		$result = $this->db->query("
			SELECT  brand_id , brand_code ,  brand_parent_id ,  brand_name ,  brand_description ,  brand_picture , brand_external_id ,  brand_external_code ,  brand_external_name ,  brand_ecommerce_enabled 
			FROM  erp_product_brands ",
			array()
		);

		$brands = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($brands as $brand) {

        	array_push($data, $this->convertbrand($brand));
        }

		return $data;

	}
	
	public function getbrandsEcommerceEnabled(){

	/*	$result = $this->db->query("
			SELECT  brand_id , brand_code ,  brand_parent_id ,  brand_name ,  brand_description ,  brand_picture , brand_external_id ,  brand_external_code ,  brand_external_name ,  brand_ecommerce_enabled 
			FROM  erp_product_brands
			WHERE brand_ecommerce_enabled = 1
			",
			array()
		);*/
		
		$result = $this->db->query("
		SELECT * FROM erp_product_brands where brand_id in (
			SELECT distinct brand_parent_id 
			FROM erp_product_brands
		)
		AND brand_ecommerce_enabled = 1
			",
			array()
		);
		
		
		/*$result = $this->db->query("
		SELECT * FROM erp_product_brands where brand_id in (
			SELECT distinct brand_parent_id 
			FROM erp_product_brands
			WHERE brand_id
			IN (
			
				SELECT DISTINCT brand_id
				FROM  `erp_products` 
				WHERE product_ecommerce_enabled =1
			))
			",
			array()
		);*/
		
		

		$brands = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($brands as $brand) {
			
			
			$brand = $this->convertbrand($brand);
			
			$brand["childs"] = $this->getChildbrands($brand["brandId"]);
        	array_push($data, $brand);
        }
        
        

		return $data;

	}
	public function getbrandsWithFeaturedProducts(){


		

		
		
		$result = $this->db->query("
		SELECT * FROM erp_product_brands where brand_id
			
			IN (
			
				SELECT DISTINCT brand_id
				FROM  erp_products
				WHERE product_featured =1
			)
			",
			array()
		);
		
		

		$brands = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($brands as $brand) {
			
			
			$brand = $this->convertbrand($brand);
			
			$ProductModel = new ProductModel();
			$brand["products"] = $ProductModel->getFeaturedProducts($brand["brandId"]);
        	array_push($data, $brand);
        }
        
        

		return $data;

	}
	
	
	public function getChildbrands($brandId){

		$result = $this->db->query("
			SELECT  brand_id , brand_code ,  brand_parent_id ,  brand_name ,  brand_description ,  brand_picture , brand_external_id ,  brand_external_code ,  brand_external_name ,  brand_ecommerce_enabled 
			FROM  erp_product_brands
			WHERE brand_parent_id = :brand_parent_id
			",
			array(":brand_parent_id" => $brandId)
		);

		$brands = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($brands as $brand) {

        	array_push($data, $this->convertbrand($brand));
        }

		return $data;

	}
	
	
	

	public function getbrand($brandId){
		$result = $this->db->query(" 
			SELECT  brand_id , brand_code ,  brand_parent_id ,  brand_name ,  brand_description ,  brand_picture , brand_external_id ,  brand_external_code ,  brand_external_name ,  brand_ecommerce_enabled 
			FROM  erp_product_brands
			WHERE `brand_id` = :brandId",
			array( ':brandId' => $brandId )
		);


		$brand=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertbrand($brand);

	}
	
	public function getbrandByCode($brandCode){
		$result = $this->db->query(" 
			SELECT  brand_id , brand_code ,  brand_parent_id ,  brand_name ,  brand_description ,  brand_picture , brand_external_id ,  brand_external_code ,  brand_external_name ,  brand_ecommerce_enabled 
			FROM  erp_product_brands
			WHERE `brand_code` = :brandCode",
			array( ':brandCode' => $brandCode )
		);


		$brand=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertbrand($brand);

	}

	public function createbrand($brand) {

		$this->db->query("
			INSERT INTO erp_product_brands(
				brand_id,
				brand_code,
				brand_parent_id,
				brand_name, 
				brand_description,
				brand_picture,
				brand_external_id,
				brand_external_code,
				brand_external_name,
				brand_ecommerce_enabled
			)
			VALUES (
				:brandId,
				:brandCode,
				:brandParentId,
				:brandName,
				:brandDescription,
				:brandPicture,
				:brandExternalId,
				:brandExternalCode,
				:brandExternalName,
				:brandEcommerceEnabled
			)",
			$this->fillParams($brand)
		);

		$brand->brandId = $this->db->getInsertId();

		return $brand;
	}

	public function updatebrand($brand) {

		$this->db->query("
			UPDATE erp_product_brands SET 
				brand_id=:brandId,
				brand_code=:brandCode,
				brand_parent_id=:brandParentId,
				brand_name=:brandName,
				brand_description = :brandDescription,
				brand_picture=:brandPicture,
				brand_external_id=:brandExternalId,
				brand_external_code=:brandExternalCode,
				brand_external_name=:brandExternalName,
				brand_ecommerce_enabled=:brandEcommerceEnabled
			WHERE brand_id=:brandId",
			$this->fillParams($brand)
		);
	}
	
	public function syncbrands($products){

		try {
		//		$this->db->conn->beginTransaction();

				$sql = "
					INSERT INTO erp_product_brands(
						brand_id,
						brand_code,
						brand_parent_id,
						brand_name, 
						
						brand_external_code,
						brand_external_name,
						brand_ecommerce_enabled
					)
					VALUES (
						:brandId,
						:brandCode,
						:brandParentId,
						:brandName,
						
						:brandExternalCode,
						:brandExternalName,
						1
					)
				";
		
				$sql_search = "SELECT * FROM erp_product_brands where brand_code=:brandCode";
				
				foreach ($products as $product) {
					
					//var_dump($product);
					
					//Verificar si el producto ya existe en la base de datos
					$statement = $this->db->conn->prepare($sql_search);
					$statement->setFetchMode(PDO::FETCH_OBJ);
					$statement->execute(array(':brandCode' => $product->brandCode));
			
			        $result = $statement->fetch(PDO::FETCH_OBJ);
					 
					if(isset($result->brand_id) && $result->brand_id > 0){
					 	
					 	$product->brandId = $result->brand_id;
					 	
					 	if(isset($result->brand_parent_id)){
					 		
					 		$product->brandParentId = $result->brand_parent_id;
					 		
					 	}
					 	else {
					 		
					 		$parent_brand  = $this->getbrandByCode($product->brandParentCode);
					 			
						 	if(isset($parent_brand["brandId"])){
						 		
						 		$product->brandParentId = $parent_brand["brandId"];
						 	
						 	}
						 	else {
						 	
						 			$parent_brand = new StdClass();
						 			$parent_brand->brandCode = $product->brandParentCode;
						 			$parent_brand->brandName = $product->brandParentCode;
						 		
						 			$parent_brand = $this->createbrand($parent_brand);
						 		
						 			$product->brandParentId = $parent_brand->brandId;
						 		
						 		}
					 		}
					 		
					 }
					 else {
					 	
					 	$parent_brand  = $this->getbrandByCode($product->brandParentCode);
					 	//var_dump($parent_brand);
					 	
					 	if(isset($parent_brand["brandId"]) && $parent_brand["brandId"] > 0 ){
					 		
					 		$product->brandParentId = $parent_brand["brandId"];
					 		
					 	}
					 	else{
	
					 		$parent_brand = new StdClass();
					 		$parent_brand->brandCode = $product->brandParentCode;
					 		$parent_brand->brandName = $product->brandParentCode;
					 		
					 		$parent_brand = $this->createbrand($parent_brand);
					 		
					 		$product->brandParentId = $parent_brand->brandId;
					 		
					 	}
					 	

		 
						//var_dump($product);
						$params =[];
	
						$params = [];

						if(isset($product->brandId)){ $params[':brandId'] = $product->brandId; } else { $params[':brandId'] = NULL; }; 
						if(isset($product->brandParentId)){ $params[':brandParentId'] = $product->brandParentId; } else { $params[':brandParentId'] = NULL; }; 
						if(isset($product->brandCode)){ $params[':brandCode'] = $product->brandCode; } else { $params[':brandCode'] = NULL; }; 
						if(isset($product->brandCode)){ $params[':brandName'] = $product->brandCode; } else { $params[':brandName'] = NULL; }; 
					    //if(isset($brand->brandExternalId)){ $params[':brandExternalId'] = $brand->brandExternalId; } else { $params[':brandExternalId'] = NULL; }; 
					    if(isset($product->brandCode)){ $params[':brandExternalCode'] = $product->brandCode; } else { $params[':brandExternalCode'] = NULL; }; 
					    if(isset($product->brandCode)){ $params[':brandExternalName'] = $product->brandCode; } else { $params[':brandExternalName'] = NULL; }; 
					    //if(isset($brand->brandEcommerceEnabled)){ $params[':brandEcommerceEnabled'] = $brand->brandEcommerceEnabled; } else { $params[':brandEcommerceEnabled'] = NULL; }; 
						
						
			
						
						$statement = $this->db->conn->prepare($sql);
						$statement->setFetchMode(PDO::FETCH_OBJ);
						$statement->execute($params);
	
						$product->brandId =(int) $this->db->conn->lastInsertId();
					 }
					 
					$sql_update_products = " UPDATE erp_products SET brand_id=:brandId WHERE product_code = :ProductCode";
			
					$params = [];
					$params[":brandId"] = $product->brandId;
					$params[":ProductCode"] = $product->ProductCode;
						
									
					$statement = $this->db->conn->prepare($sql_update_products);
					$statement->setFetchMode(PDO::FETCH_OBJ);
					$statement->execute($params);
					
					var_dump($product);

				}
				
			
		

	


		//$this->db->conn->commit();


		return $products;
		
		} 
		catch(Exception $ex){
			
			//$this->db->conn->rollback();
			throw new Exception($ex);
			  
		}	
	}

	public function deletebrand($brandId){
		$this->db->query("
			DELETE FROM `erp_product_brands` 
			WHERE `brand_id`=:brandId",
			array(':brandId' => $brandId)
		);

		
	}

	public function updatebrandPicture($brandId,$brandPicture){
		$this->db->query("
			UPDATE `erp_product_brands` SET 
				`brand_picture`=:brandPicture 
			WHERE `brand_id`=:brandId",
			array( 
				':brandId' => $brandId,
				':brandPicture' => $brandPicture)
		);
	}

	private function fillParams($brand) {

        $params = [];

		if(isset($brand->brandId)){ $params[':brandId'] = $brand->brandId; } else { $params[':brandId'] = NULL; }; 
		if(isset($brand->brandParentId)){ $params[':brandParentId'] = $brand->brandParentId; } else { $params[':brandParentId'] = NULL; }; 
		if(isset($brand->brandCode)){ $params[':brandCode'] = $brand->brandCode; } else { $params[':brandCode'] = NULL; }; 
		if(isset($brand->brandName)){ $params[':brandName'] = $brand->brandName; } else { $params[':brandName'] = NULL; }; 
		if(isset($brand->brandDescription)){ $params[':brandDescription'] = $brand->brandDescription; } else { $params[':brandDescription'] = NULL; }; 
	    if(isset($brand->brandPicture)){ $params[':brandPicture'] = $brand->brandPicture; } else { $params[':brandPicture'] = NULL; }; 
	    if(isset($brand->brandExternalId)){ $params[':brandExternalId'] = $brand->brandExternalId; } else { $params[':brandExternalId'] = NULL; }; 
	    if(isset($brand->brandExternalCode)){ $params[':brandExternalCode'] = $brand->brandExternalCode; } else { $params[':brandExternalCode'] = NULL; }; 
	    if(isset($brand->brandExternalName)){ $params[':brandExternalName'] = $brand->brandExternalName; } else { $params[':brandExternalName'] = NULL; }; 
	    if(isset($brand->brandEcommerceEnabled)){ $params[':brandEcommerceEnabled'] = $brand->brandEcommerceEnabled; } else { $params[':brandEcommerceEnabled'] = NULL; }; 
		
		
			
			
			
	

				
				
		return $params;

	}



	private function convertbrand($brand){
		
		return array(
			'brandId' => $brand->brand_id,
			'brandCode' =>  $brand->brand_code,
			'brandName' =>  $brand->brand_name,
			'brandDescription' =>  $brand->brand_description,
			'brandPicture' => $brand->brand_picture,
			'brandParentId' => $brand->brand_parent_id,
			'brandExternalId' => $brand->brand_external_id,  
			'brandExternalName' => $brand->brand_external_name,  
			'brandExternalCode' => $brand->brand_external_code,  
			'brandEcommerceEnabled' => $brand->brand_ecommerce_enabled 
		

		);
	}
}
?>