<?php
class PurchaseOrderModel {

	private $db;
    private $purchase_order_select_sql = "SELECT `purchase_order_id`, `purchase_order_folio`, `purchase_order_title`, `purchase_order_date`, `supplier_id`, `purchase_order_status`, `purchase_order_total`, `purchase_order_cdate`, `purchase_order_terms`, `user_id`, `purchase_order_detail_last_id`, `currency_id`, `purchase_order_lmdate`, `purchase_order_exchange_rate`, `purchase_order_subtotal`, `purchase_order_taxes`, `purchase_order_discounts`, `purchase_order_total_dlls`, `purchase_order_deleted` FROM `erp_purchase_orders`";

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	
	public function getPurchaseOrders(){
        
        $sql = $this->purchase_order_select_sql;
        
   
		$result = $this->db->query($sql." WHERE `purchase_order_deleted` = 0",array());

		$purchase_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($purchase_orders as $purchase_order) {

        	array_push($data, $this->convertPurchaseOrder($purchase_order));
        }

		return $data;
	}


	public function getPurchaseOrder($PurchaseOrderId,$detailed = false, $status=""){
        
        $sql = $this->purchase_order_select_sql;
        
		$result = $this->db->query($sql." WHERE purchase_order_id = :PurchaseOrderId AND `purchase_order_deleted` = 0",array(':PurchaseOrderId' => $PurchaseOrderId ));

		$purchase_order=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertPurchaseOrder($purchase_order,$detailed);
	}


	public function getPendingPurchaseOrders(){
        
        $sql = $this->purchase_order_select_sql;

		$result = $this->db->query($sql." WHERE `purchase_order_status` = 'Pendiente' AND `purchase_order_deleted` = 0 ORDER BY `purchase_order_date` DESC , `purchase_order_id` DESC",array());

		$purchase_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($purchase_orders as $purchase_order) {

        	array_push($data, $this->convertPurchaseOrder($purchase_order));
        }

		return $data;

	}


	public function getProcessedPurchaseOrders(){

        $sql = $this->purchase_order_select_sql;
        
		$result = $this->db->query($sql." WHERE `purchase_order_status` <> 'Pendiente' AND `purchase_order_deleted` = 0 ORDER BY `purchase_order_date` DESC , `purchase_order_id` DESC",array());

		$purchase_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($purchase_orders as $purchase_order) {

        	array_push($data, $this->convertPurchaseOrder($purchase_order));
        }

		return $data;


	}




	public function getPurchaseOrderByFolio($PurchaseOrderFolio){
        
        $sql = $this->purchase_order_select_sql;

		$result = $this->db->query($sql." WHERE purchase_order_folio = :PurchaseOrderFolio	AND `purchase_order_deleted` = 0",array(':PurchaseOrderFolio' => $PurchaseOrderFolio ));

		$purchase_order=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertPurchaseOrder($purchase_order,true);
	}

	public function createPurchaseOrder($purchase_order){

 		try{

 			$this->db->conn->beginTransaction();
 			  
				$sql = " 
					INSERT INTO `erp_purchase_orders`(
						`purchase_order_id`, 
						`purchase_order_folio`, 
						`purchase_order_title`, 
						`purchase_order_date`, 
						`supplier_id`, 
						`purchase_order_status`,
						`purchase_order_subtotal`,
						`purchase_order_taxes`, 
						`purchase_order_discounts`,
						`purchase_order_total`,
						`purchase_order_total_dlls`,
						`purchase_order_cdate`,
						`purchase_order_exchange_rate`,
						`purchase_order_terms`,
						`user_id`,
						`currency_id`

					) VALUES (

						:PurchaseOrderId,
						:PurchaseOrderFolio,
						:PurchaseOrderTitle,
						:PurchaseOrderDate,
						:SupplierId,
						:PurchaseOrderStatus,
						:PurchaseOrderSubtotal,
						:PurchaseOrderTaxes, 
						:PurchaseOrderDiscounts,
						:PurchaseOrderTotal,
						:PurchaseOrderTotalDlls,
						NOW(),
						:PurchaseOrderExchangeRate,
						:PurchaseOrderTerms,
						:UserId,
						:CurrencyId
				)";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($purchase_order));
		
				$purchase_order->PurchaseOrderId = (int)$this->db->conn->lastInsertId();
				$purchase_order->PurchaseOrderFolio = $purchase_order->PurchaseOrderId ;

				if(isset($purchase_order->items)){
  					$this->createPurchaseOrderDetails($purchase_order);
				}

				$this->db->conn->commit();

				return $purchase_order;

		}catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}
	}

	public function updatePurchaseOrder($purchase_order){

		try{

 				if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();

				$sql = " 
					UPDATE `erp_purchase_orders` SET 

						`purchase_order_folio` = :PurchaseOrderFolio, 
						`purchase_order_title`  = :PurchaseOrderTitle,
						`purchase_order_date` = :PurchaseOrderDate,
						`supplier_id` = :SupplierId,
						`purchase_order_status` = :PurchaseOrderStatus,
						`purchase_order_subtotal` = :PurchaseOrderSubtotal,
						`purchase_order_taxes` = :PurchaseOrderTaxes, 
						`purchase_order_discounts` = :PurchaseOrderDiscounts,
						`purchase_order_total` = :PurchaseOrderTotal,
						`purchase_order_total_dlls` = :PurchaseOrderTotalDlls,						
						`user_id` = :UserId,
						`purchase_order_lmdate` = NOW(),
						`purchase_order_exchange_rate` = :PurchaseOrderExchangeRate,
						`purchase_order_terms` = :PurchaseOrderTerms,
						`currency_id` = :CurrencyId
						WHERE `purchase_order_id` = :PurchaseOrderId 
					";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($purchase_order));
		
			

				if(isset($purchase_order->deleted_items)){
  					$this->deletePurchaseOrderDetails($purchase_order);
				}



				if(isset($purchase_order->items)){
  					$this->createPurchaseOrderDetails($purchase_order);
				}

				

				$this->db->conn->commit();

				return $purchase_order;

		}catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}

	}


	public function setPurchaseOrderToMovements($purchase_order){

		try{

 				if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();

				$sql = " 
					UPDATE `erp_purchase_orders` SET 

						`purchase_order_folio` = :PurchaseOrderFolio, 
						`purchase_order_title`  = :PurchaseOrderTitle,
						`purchase_order_date` = :PurchaseOrderDate,
						`supplier_id` = :SupplierId,
						`purchase_order_status` = :PurchaseOrderStatus,
						`purchase_order_subtotal` = :PurchaseOrderSubtotal,
						`purchase_order_taxes` = :PurchaseOrderTaxes, 
						`purchase_order_discounts` = :PurchaseOrderDiscounts,
						`purchase_order_total` = :PurchaseOrderTotal,
						`purchase_order_total_dlls` = :PurchaseOrderTotalDlls,						
						`user_id` = :UserId,
						`purchase_order_lmdate` = NOW(),
						`purchase_order_exchange_rate` = :PurchaseOrderExchangeRate,
						`purchase_order_terms` = :PurchaseOrderTerms,
						`currency_id` = :CurrencyId
						WHERE `purchase_order_id` = :PurchaseOrderId 
					";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($purchase_order));
		
			

				if(isset($purchase_order->deleted_items)){
  					$this->deletePurchaseOrderDetails($purchase_order);
				}

				if(isset($purchase_order->items)){
  					$this->createPurchaseOrderDetails($purchase_order);
				}

                // Se planea hacer el insert en la base de movimientos 
				if(isset($purchase_order->items)){
  					$this->createPurchaseOrderDetailsToMovements($purchase_order);
				}

				

				$this->db->conn->commit();

				return $purchase_order;

		}catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}

	}


	public function deletePurchaseOrder($PurchaseOrderId){

		//try{

 				$this->db->conn->beginTransaction();

				/*$sql = " 
					DELETE FROM `erp_purchase_order_details` 
					WHERE purchase_order_id= :PurchaseOrderId
				" ;
					
				
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array( ':PurchaseOrderId' => $PurchaseOrderId ));
				*/

				$sql = " 
					UPDATE `erp_purchase_orders` SET `purchase_order_deleted` = 1
					WHERE purchase_order_id=:PurchaseOrderId
				" ;

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array( ':PurchaseOrderId' => $PurchaseOrderId ));




				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array( ':PurchaseOrderId' => $PurchaseOrderId ));

					

				$this->db->conn->commit();
		/*}
		catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}*/
	}


	public function approvePurchaseOrder($purchase_order){

    	if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();

    	$purchase_order = $this->setPurchaseOrderToMovements($purchase_order);

        if($this->db->conn->inTransaction()) $this->db->conn->commit();

        //insertar el contenido de la orden de compra en movimientos




      	return $purchase_order;

    }



	public function createPurchaseOrderDetails($purchase_order){

		$sql_insert = "
			INSERT INTO `erp_purchase_order_details`(
				`purchase_order_id`, 
				`purchase_order_detail_id`, 
				`purchase_order_detail_description`, 
				`product_id`, 
				`purchase_order_detail_qty`, 
				`purchase_order_detail_um`, 
				`purchase_order_detail_cost`,
				`purchase_order_detail_discount`,
				`purchase_order_detail_tax`,
				`purchase_order_detail_total`
			) 
			VALUES (
				:PurchaseOrderId,
				:PurchaseOrderDetailId,
				:PurchaseOrderDetailDescription,
				:ProductId,
				:PurchaseOrderDetailQTY,
				:PurchaseOrderDetailUM,
				:PurchaseOrderDetailCost,
				:PurchaseOrderDetailDiscount,
				:PurchaseOrderDetailTax,
				:PurchaseOrderDetailTotal
			)";

		$sql_update = "
			UPDATE `erp_purchase_order_details` SET
						
				`purchase_order_detail_description`=:PurchaseOrderDetailDescription,
				`product_id` =:ProductId, 
				`purchase_order_detail_qty`=:PurchaseOrderDetailQTY, 
				`purchase_order_detail_um`=:PurchaseOrderDetailUM, 
				`purchase_order_detail_cost`=:PurchaseOrderDetailCost,
				`purchase_order_detail_discount`=:PurchaseOrderDetailDiscount,
				`purchase_order_detail_tax` =:PurchaseOrderDetailTax,
				`purchase_order_detail_total` = :PurchaseOrderDetailTotal
				WHERE `purchase_order_id` = :PurchaseOrderId and `purchase_order_detail_id` = :PurchaseOrderDetailId";


		foreach ($purchase_order->items as $purchase_order_item){

			$sql = $sql_insert;

			if(isset($purchase_order_item->PurchaseOrderDetailId)){ 
				$sql = $sql_update; 
			}else{
				$purchase_order_item->PurchaseOrderDetailId = $this->getPurchaseOrderDetailIdentity($purchase_order->PurchaseOrderId);	
			}


			$statement = $this->db->conn->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute($this->fillDetailParams($purchase_order->PurchaseOrderId,$purchase_order_item));

			
			
		}		
	}



	public function createPurchaseOrderDetailsToMovements($purchase_order){

		$sql_insert = "
			INSERT INTO `erp_movements`(
				`purchase_order_id`,
				`sale_order_id`, 
				`product_id`, 
				`um_id`,
				`movement_um`,
				`movement_qty`, 
				`movement_cdate`, 
				`user_id`, 
				`movement_type`, 
				`movement_active`
			) 
			VALUES (
				:PurchaseOrderId,
				 0,
				:ProductId,
				0,
				:PurchaseOrderDetailUM,
				:PurchaseOrderDetailQTY,
				NOW(),
				:UserId,
				1,
				1
			)";

		foreach ($purchase_order->items as $purchase_order_item){
            
          	if(isset($purchase_order_item->Product->ProductId)){   
			    $sql = $sql_insert;

    			//$purchase_order_item->PurchaseOrderDetailId = $this->getPurchaseOrderDetailIdentity($purchase_order->PurchaseOrderId);
    		 
    			$parameters = array(
    			                ':PurchaseOrderId' => $purchase_order->PurchaseOrderId,
    			                ':ProductId'=>$purchase_order_item->Product->ProductId,
    			                ':PurchaseOrderDetailQTY'=> $purchase_order_item->PurchaseOrderDetailQTY,
    			                ':PurchaseOrderDetailUM' => $purchase_order_item->PurchaseOrderDetailUM,
    			                ':UserId' => getOwnerId()
    			                );
    			                
    			$statement = $this->db->conn->prepare($sql);
    			$statement->setFetchMode(PDO::FETCH_OBJ);
    			$statement->execute($parameters);
          	}
		}		
	}


	public function deletePurchaseOrderDetails($purchase_order){

		$sql = "
			DELETE FROM `erp_purchase_order_details` WHERE
				`purchase_order_id` = :PurchaseOrderId and `purchase_order_detail_id` = :PurchaseOrderDetailId
				";



			foreach ($purchase_order->deleted_items as $purchase_order_item){

				

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(':PurchaseOrderId' => $purchase_order->PurchaseOrderId, ':PurchaseOrderDetailId' => $purchase_order_item->PurchaseOrderDetailId));
				
				
			}		
	}

	private function getPurchaseOrderDetails($purchase_order_id){
		$result = $this->db->query("
			SELECT  
				`purchase_order_id`, 
				`purchase_order_detail_id`, 
				`purchase_order_detail_description`, 
				`product_id`, 
				`purchase_order_detail_qty`, 
				`purchase_order_detail_um`, 
				`purchase_order_detail_cost`,
				`purchase_order_detail_discount`,
				`purchase_order_detail_tax`,
				`purchase_order_detail_total`
			FROM `erp_purchase_order_details`
			WHERE `purchase_order_id` = :PurchaseOrderId"
			, array( ':PurchaseOrderId' => $purchase_order_id));

			$purchase_order_details = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($purchase_order_details as $purchase_order_detail) {

        	array_push($data, $this->convertPurchaseOrderDetail($purchase_order_detail));
        }

		return $data;

	}

	private function getPurchaseOrderDetailIdentity($purchase_order_id){

		$purchase_order_detail_id=1;

		$sql = "
			SELECT  MAX(`purchase_order_detail_id`)+1 as identity FROM `erp_purchase_order_details` WHERE `purchase_order_id` = :PurchaseOrderId
		";

		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':PurchaseOrderId' => $purchase_order_id));


 		$result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->identity)){
		 	$purchase_order_detail_id = $result->identity;
		 }

		
		return $purchase_order_detail_id;
    }

     
    public function cancelPurchaseOrder($purchase_order){

    	$purchase_order->PurchaseOrderStatus = "Cancelada";

    	$this->db->conn->beginTransaction();

		$sql = " UPDATE `erp_purchase_orders` SET `purchase_order_status` = :PurchaseOrderStatus WHERE purchase_order_id=:PurchaseOrderId " ;

		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':PurchaseOrderId' => $purchase_order->PurchaseOrderId , ':PurchaseOrderStatus' => $purchase_order->PurchaseOrderStatus ));
		

		$this->db->conn->commit();

		return $PurchaseOrder;
    }

	private function fillParams($purchase_order){

		$params = [];

        $params[':PurchaseOrderId']			 = ( isset($purchase_order->PurchaseOrderId) 		   ?  $purchase_order->PurchaseOrderId 			 : NULL );
        $params[':PurchaseOrderFolio']		 = ( isset($purchase_order->PurchaseOrderFolio) 	   ?  $purchase_order->PurchaseOrderFolio 		 : NULL );
        $params[':PurchaseOrderTitle']		 = ( isset($purchase_order->PurchaseOrderTitle)        ?  $purchase_order->PurchaseOrderTitle 		 : NULL );
        $params[':PurchaseOrderDate']		 = ( isset($purchase_order->PurchaseOrderDate) 		   ?  $purchase_order->PurchaseOrderDate 		 : NULL );
        $params[':SupplierId']				 = ( isset($purchase_order->SupplierId) 			   ?  $purchase_order->SupplierId 			     : NULL );
        $params[':PurchaseOrderStatus']		 = ( isset($purchase_order->PurchaseOrderStatus) 	   ?  $purchase_order->PurchaseOrderStatus 		 : NULL );
        $params[':PurchaseOrderSubtotal']	 = ( isset($purchase_order->PurchaseOrderSubtotal) 	   ?  $purchase_order->PurchaseOrderSubtotal 	 : NULL );
        $params[':PurchaseOrderTaxes']		 = ( isset($purchase_order->PurchaseOrderTaxes) 	   ?  $purchase_order->PurchaseOrderTaxes 		 : NULL );
        $params[':PurchaseOrderDiscounts']	 = ( isset($purchase_order->PurchaseOrderDiscounts)    ?  $purchase_order->PurchaseOrderDiscounts 	 : NULL );
        $params[':PurchaseOrderTotal']		 = ( isset($purchase_order->PurchaseOrderTotal) 	   ?  $purchase_order->PurchaseOrderTotal 		 : NULL );
        $params[':PurchaseOrderTotalDlls']	 = ( isset($purchase_order->PurchaseOrderTotalDlls)    ?  $purchase_order->PurchaseOrderTotalDlls 	 : NULL );
        $params[':PurchaseOrderExchangeRate']= ( isset($purchase_order->PurchaseOrderExchangeRate) ?  $purchase_order->PurchaseOrderExchangeRate : NULL );
        $params[':PurchaseOrderTerms']		 = ( isset($purchase_order->PurchaseOrderTerms) 	   ?  $purchase_order->PurchaseOrderTerms 		 : NULL );
        $params[':CurrencyId']				 = ( isset($purchase_order->CurrencyId) 			   ?  $purchase_order->CurrencyId 			     : NULL );
        $params[':UserId']  = getOwnerId();
        
		
		return $params;
	}


	private function fillDetailParams($purchase_order_id , $purchase_order_detail){
		$params=[];
		$params[':PurchaseOrderId'] = $purchase_order_id ;
		if(isset($purchase_order_detail->PurchaseOrderDetailId)){   $params[':PurchaseOrderDetailId'] = $purchase_order_detail->PurchaseOrderDetailId ; } else { $params[':PurchaseOrderDetailId'] = NULL ; }
		
		if(isset($purchase_order_detail->PurchaseOrderDetailDescription)){   $params[':PurchaseOrderDetailDescription'] = $purchase_order_detail->PurchaseOrderDetailDescription ; } else { $params[':PurchaseOrderDetailDescription'] = NULL ; }
		if(isset($purchase_order_detail->PurchaseOrderDetailQTY)){   $params[':PurchaseOrderDetailQTY'] = $purchase_order_detail->PurchaseOrderDetailQTY ; } else { $params[':PurchaseOrderDetailQTY'] = NULL ; }
		if(isset($purchase_order_detail->PurchaseOrderDetailUM)){   $params[':PurchaseOrderDetailUM'] = $purchase_order_detail->PurchaseOrderDetailUM ; } else { $params[':PurchaseOrderDetailUM'] = NULL ; }
		if(isset($purchase_order_detail->PurchaseOrderDetailDiscount)){   $params[':PurchaseOrderDetailDiscount'] = $purchase_order_detail->PurchaseOrderDetailDiscount ; } else { $params[':PurchaseOrderDetailDiscount'] = NULL ; }
		if(isset($purchase_order_detail->PurchaseOrderDetailTax)){   $params[':PurchaseOrderDetailTax'] = $purchase_order_detail->PurchaseOrderDetailTax ; } else { $params[':PurchaseOrderDetailTax'] = NULL ; }
		if(isset($purchase_order_detail->PurchaseOrderDetailCost)){   $params[':PurchaseOrderDetailCost'] = $purchase_order_detail->PurchaseOrderDetailCost ; } else { $params[':PurchaseOrderDetailCost'] = NULL ; }
		if(isset($purchase_order_detail->PurchaseOrderDetailTotal)){   $params[':PurchaseOrderDetailTotal'] = $purchase_order_detail->PurchaseOrderDetailTotal ; } else { $params[':PurchaseOrderDetailTotal'] = NULL ; }
        

        
		if(isset($purchase_order_detail->Product->ProductId)){   

			$params[':ProductId'] = $purchase_order_detail->Product->ProductId ; 

		} else { 
			$params[':ProductId'] = NULL ; 

			if(isset($purchase_order_detail->Product) && is_string($purchase_order_detail->Product)){

				 $params[':PurchaseOrderDetailDescription'] = $purchase_order_detail->Product ; 
			}
		}


		return $params;
	}



	private function convertPurchaseOrder($purchase_order,$detailed=false){

        //var_dump($PurchaseOrder);

		$PurchaseOrderData = array(
			"PurchaseOrderId" => $purchase_order->purchase_order_id,
			"PurchaseOrderFolio" => 'OC-'.$purchase_order->purchase_order_id,
			"PurchaseOrderTitle" => $purchase_order->purchase_order_title,
			"PurchaseOrderDate" => $purchase_order->purchase_order_date,
			"SupplierId" => $purchase_order->supplier_id,
			"PurchaseOrderStatus" => $purchase_order->purchase_order_status,
			"PurchaseOrderSubtotal" => $purchase_order->purchase_order_subtotal,
			"PurchaseOrderTaxes" => $purchase_order->purchase_order_taxes,
			"PurchaseOrderDiscounts" => $purchase_order->purchase_order_discounts,
			"PurchaseOrderTotal" => $purchase_order->purchase_order_total,
			"PurchaseOrderTotalDlls" => $purchase_order->purchase_order_total_dlls,
			"PurchaseOrderTerms" => $purchase_order->purchase_order_terms,
			"CurrencyId" => $purchase_order->currency_id,
			"PurchaseOrderCDate" => $purchase_order->purchase_order_cdate,
			"PurchaseOrderLMDate" =>  $purchase_order->purchase_order_lmdate,
			"PurchaseOrderExchangeRate" => $purchase_order->purchase_order_exchange_rate,
			"UserId" => $purchase_order->user_id,
			"CurrencyId" => $purchase_order->currency_id
		);


		if(isset($purchase_order->supplier_id)){

			$SupplierModel = new SupplierModel();
			$supplier = $SupplierModel->getSupplier($purchase_order->supplier_id);
			$PurchaseOrderData["supplier"] = $supplier;

		}

		if(isset($purchase_order->user_id)){

			$UserModel = new UserModel();
			$user = $UserModel->getUser($purchase_order->user_id);
			$PurchaseOrderData["UserName"] = $user->user_name;

		}

		if(isset($purchase_order->currency_id)){
			$CurrencyModel = new CurrencyModel();
			$Currency = $CurrencyModel->getCurrency($purchase_order->currency_id);
			$PurchaseOrderData["CurrencyName"]= $Currency["CurrencyName"];
			$PurchaseOrderData["CurrencySymbol"]= $Currency["CurrencySymbol"];
		}
        
		if($detailed) {

			$PurchaseOrderData["items"] = $this->getPurchaseOrderDetails($purchase_order->purchase_order_id);

		}
    
		return $PurchaseOrderData ;
	}

	private function convertPurchaseOrderDetail($purchase_order_detail){


		$PurchaseOrderDetailData = array(
			"PurchaseOrderDetailId" => $purchase_order_detail->purchase_order_detail_id,
			"PurchaseOrderDetailDescription" => $purchase_order_detail->purchase_order_detail_description,
			"ProductId" => $purchase_order_detail->product_id,
			"PurchaseOrderDetailQTY" => $purchase_order_detail->purchase_order_detail_qty,
			"PurchaseOrderDetailUM" => $purchase_order_detail->purchase_order_detail_um,
			"PurchaseOrderDetailCost" => $purchase_order_detail->purchase_order_detail_cost,
			"PurchaseOrderDetailDiscount" => $purchase_order_detail->purchase_order_detail_discount,
           
			"PurchaseOrderDetailTax" => $purchase_order_detail->purchase_order_detail_tax,
            "PurchaseOrderDetailTotal" => $purchase_order_detail->purchase_order_detail_total
		);

		if(!isset($purchase_order_detail->product_id)){
			$PurchaseOrderDetailData["Product"] =  $purchase_order_detail->purchase_order_detail_description;


			if(strlen($purchase_order_detail->purchase_order_detail_description) > 0)
				$PurchaseOrderDetailData["IsManualItem"] =  true;
		}
		else{
			$ProductModel = new ProductModel();
			$product = $ProductModel->getProduct($purchase_order_detail->product_id);
			$PurchaseOrderDetailData["Product"] = $product;
		}



		return $PurchaseOrderDetailData ;
	}

	
}
?>