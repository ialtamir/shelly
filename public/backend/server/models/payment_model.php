<?php
class PaymentModel {

	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getCompletedPayments(){

		$result = $this->db->query(" 
			SELECT 
				p.`payment_id`, 
        		p.`payment_total`, 
        		p.`payment_title`, 
        		p.`payment_date`, 
        		p.`supplier_id`,
        		`supplier_name`,
        		p.`currency_id`,
				`currency_name`,
				`currency_symbol`,
        		p.`user_id`,
        		SUM(pd.payment_detail_amount) AS payment_details_total,
        		(payment_total - sum(pd.payment_detail_amount)) AS payment_remaining
        	FROM `erp_payments` as p        	
            LEFT OUTER JOIN `erp_suppliers` on erp_suppliers.Supplier_Id = p.Supplier_id
            LEFT OUTER JOIN `erp_currencies` on erp_currencies.currency_Id = p.currency_id
            LEFT OUTER JOIN `erp_payment_details`  as pd on pd.payment_id = p.payment_id
            GROUP BY p.payment_id
            HAVING SUM(pd.payment_detail_amount) >= p.`payment_total`
			ORDER BY `payment_date` DESC, `payment_id` DESC",
			array()
		);

		$payments = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($payments as $payment) {

        	array_push($data, $this->convertPayment($payment));
        }

		return $data;

	}


	public function getRemainingPayments(){

		$result = $this->db->query(" 
			SELECT 
				p.`payment_id`, 
        		p.`payment_total`, 
        		p.`payment_title`, 
        		p.`payment_date`, 
        		p.`supplier_id`,
        		`supplier_name`,
        		p.`currency_id`,
				`currency_name`,
				`currency_symbol`,
        		p.`user_id`,
        		SUM(pd.payment_detail_amount) AS payment_details_total,
        		IFNULL((payment_total - sum(pd.payment_detail_amount)),payment_total) AS payment_remaining
        	FROM `erp_payments` as p        	
            LEFT OUTER JOIN `erp_suppliers` on erp_suppliers.Supplier_Id = p.Supplier_id
            LEFT OUTER JOIN `erp_currencies` on erp_currencies.currency_Id = p.currency_id
            LEFT OUTER JOIN `erp_payment_details` as pd on pd.payment_id = p.payment_id
            GROUP BY p.payment_id
            HAVING payment_remaining > 0
			ORDER BY `payment_date` DESC, `payment_id` DESC",
			array()
		);

		$payments = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($payments as $payment) {

        	array_push($data, $this->convertPayment($payment));
        }

		return $data;

	}

	public function getPayment($PaymentId){

		$result = $this->db->query("
		SELECT 	
				`payment_id`, 
        		`payment_total`, 
        		`payment_title`, 
        		`payment_date`, 
        		`erp_payments`.`supplier_id`,
        		`supplier_name`,
				`erp_payments`.`currency_id`,
				`currency_name`,
				`currency_symbol`,
                `payment_exchange_rate`,
        		`user_id`
        	FROM `erp_payments`
        	 LEFT OUTER JOIN `erp_suppliers` on erp_suppliers.Supplier_Id = erp_payments.Supplier_id
        	 LEFT OUTER JOIN `erp_currencies` on erp_currencies.currency_Id = erp_payments.currency_id
		WHERE payment_id = :PaymentId
		",
			array(':PaymentId' => $PaymentId )
		);

		$payment=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertPayment($payment,true);

	}


	public function createPayment($payment){

 		try{

 				$this->db->conn->beginTransaction();

				$sql = " 
					INSERT INTO `erp_payments`(
					    `payment_id`,
						`payment_total`, 
        		        `payment_title`, 
        	        	`payment_date`, 
        		        `supplier_id`, 
        		        `user_id`,
						`currency_id`,
						`payment_exchange_rate`
					) VALUES (
					    :PaymentId,
						:PaymentTotal,
						:PaymentTitle,
						:PaymentDate,						
						:SupplierId,
						:UserId,
						:CurrencyId,
						:PaymentExchangeRate
					)";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($payment));
		
				$payment->PaymentId = (int) $this->db->conn->lastInsertId();
				
				if( isset($payment->items) ){

  					$this->createPaymentDetails($payment->PaymentId, $payment->items);

				}

				$this->db->conn->commit();

				return $payment;

		}catch(Exception $ex){
			
			$this->db->conn->rollback();
			
			throw new Exception($ex->getMessage());
		}
	}

	public function updatePayment($payment){

		try{

 				$this->db->conn->beginTransaction();

				$sql = " 
					UPDATE `erp_payments` SET 
					    `payment_total`= :PaymentTotal,  
        		        `payment_title`= :PaymentTitle,  
        	        	`payment_date`=  :PaymentDate , 
        		        `supplier_id`= :SupplierId, 
        		        `user_id` = :UserId,
        		        `currency_id` = :CurrencyId,
						`payment_exchange_rate` = :PaymentExchangeRate
					WHERE `payment_id` = :PaymentId 
				";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($payment));
		
				//$sale_order->SaleOrderId = (int)$this->db->conn->lastInsertId();

				if(isset($payment->items)){
  					$this->createPaymentDetails($payment->PaymentId,$payment->items);
				}

				if(isset($payment->deleted_items)){
  					$this->deletePaymentDetails($payment->PaymentId,$payment->deleted_items);
				}

				$this->db->conn->commit();

				return $payment;

		}catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}

	}

	public function deletePayment($PaymentId){

		$this->db->conn->beginTransaction();

		$sql = " 
			DELETE FROM `erp_payments` 
			WHERE payment_id= :PaymentId
		" ;
			
		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':PaymentId' => $PaymentId ));


		
	

		$this->db->conn->commit();
	
	}



	public function createOrUpdatePaymentDetail($payment_id,$payment_item){

		$sql_insert = "
			INSERT INTO `erp_payment_details`(
			  payment_id,
			  payment_detail_id,
			  payment_detail_receipt_number,
			  payment_detail_receipt_prefix,
			  payment_detail_text,
			  payment_detail_date,
			  payment_detail_time,
			  payment_detail_amount,
			  payment_detail_method,
			  payment_detail_cdate,
			  payment_detail_lmdate,
			  user_id	
			) 
			VALUES (
			  :PaymentId,
			  :PaymentDetailId,
			  :PaymentDetailReceiptNumber,
			  :PaymentDetailReceiptPrefix,
			  :PaymentDetailText,
			  :PaymentDetailDate,
			  :PaymentDetailTime,
			  :PaymentDetailAmount,
			  :PaymentDetailMethod,
			   Now(),
			   Now(),
			  :UserId
			)";

		$sql_update = "
			UPDATE `erp_payment_details` SET
			  payment_detail_receipt_number=:PaymentDetailReceiptNumber,
			  payment_detail_receipt_prefix=:PaymentDetailReceiptPrefix,
			  payment_detail_text=:PaymentDetailText,
			  payment_detail_date=:PaymentDetailDate,
			  payment_detail_time=:PaymentDetailTime,
			  payment_detail_amount=:PaymentDetailAmount,
			  payment_detail_method=:PaymentDetailMethod,
			  payment_detail_lmdate=Now(),
			  user_id=:UserId
			WHERE `payment_id` = :PaymentId and `payment_detail_id` = :PaymentDetailId
			";



				$sql = $sql_insert;

				if(isset($payment_item->PaymentDetailId)){ 

					$sql = $sql_update; 

				}
				else{

					$payment_item->PaymentDetailId = $this->getPaymentDetailIdentity($payment_id);

				}

		
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);

				$statement->execute($this->fillDetailParams($payment_id,$payment_item));
		
				return $payment_item;
				
	}

	
	public function deletePaymentDetails($payment_id,$payment_item){

		$sql = "
			DELETE FROM `erp_payment_details` 
			WHERE `payment_id` = :PaymentId and `payment_detail_id` = :PaymentDetailId
		";



		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':PaymentId' => $payment_id, ':PaymentDetailId' => $payment_item->PaymentDetailId));
							

	}
	
	
	

	public function getPaymentDetails($payment_id){
		$result = $this->db->query("
			SELECT  
			  payment_id,
			  payment_detail_id,
			  payment_detail_receipt_number,
			  payment_detail_receipt_prefix,
			  payment_detail_text,
			  payment_detail_date,
			  payment_detail_time,
			  payment_detail_amount,
			  payment_detail_method,
			  payment_detail_cdate,
			  payment_detail_lmdate,
			  user_id			   
			FROM `erp_payment_details`
			WHERE `payment_id` = :PaymentId"
			, array( ':PaymentId' => $payment_id));

			$payment_details = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($payment_details as $payment_detail) {

        	array_push($data, $this->convertPaymentDetail($payment_detail));
        }

		return $data;


	}

	public function getPaymentDetail($payment_id,$payment_detail_id){
		$result = $this->db->query("
			SELECT  
			  payment_id,
			  payment_detail_id,
			  payment_detail_receipt_number,
			  payment_detail_receipt_prefix,
			  payment_detail_text,
			  payment_detail_date,
			  payment_detail_time,
			  payment_detail_amount,
			  payment_detail_method,
			  payment_detail_cdate,
			  payment_detail_lmdate,
			  user_id			   
			FROM `erp_payment_details`
			WHERE `payment_id` = :PaymentId AND `payment_detail_id`=:PaymentDetailId"
			, array( ':PaymentId' => $payment_id, ':PaymentDetailId' => $payment_detail_id));

			$payment_detail= $result->fetch(PDO::FETCH_OBJ);
        
        

        

        	return $this->convertPaymentDetail($payment_detail);
        

		


	}

	private function getPaymentDetailIdentity($payment_id){
		

        $payment_detail_id = 1;

		$sql = "
			SELECT  MAX(`payment_detail_id`)+1 as identity FROM `erp_payment_details` WHERE `payment_id` = :PaymentId
		";

		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':PaymentId' => $payment_id));


 		$result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->identity)){
		 	$payment_detail_id = $result->identity;
		 }

		
		return $payment_detail_id;	
			
    }

	private function fillParams($payment){

		$params = [];
		if(isset($payment->PaymentId)){  $params[':PaymentId'] = $payment->PaymentId; } else {  $params[':PaymentId'] = NULL; };
		if(isset($payment->PaymentTotal)){  $params[':PaymentTotal'] = $payment->PaymentTotal; } else {  $params[':PaymentTotal'] = NULL; };
		if(isset($payment->PaymentTitle)){   $params[':PaymentTitle'] = $payment->PaymentTitle; } else {    $params[':PaymentTitle'] = NULL; };
		if(isset($payment->SupplierId)){   $params[':SupplierId'] = $payment->SupplierId; } else {   $params[':SupplierId'] = NULL; };
		if(isset($payment->PaymentDate)){  $params[':PaymentDate'] = $payment->PaymentDate; } else {  $params[':PaymentDate'] = NULL; };
		if(isset($payment->CurrencyId)){   $params[':CurrencyId'] = $payment->CurrencyId; } else {   $params[':CurrencyId'] = NULL; };
		if(isset($payment->PaymentExchangeRate)){   $params[':PaymentExchangeRate'] = $payment->PaymentExchangeRate; } else {   $params[':PaymentExchangeRate'] = NULL; };
		$params[':UserId']  = getOwnerId();
		return $params;
	}

	private function fillDetailParams($payment_id , $payment_detail){

		$params=[];
				
		$params[':PaymentId'] = $payment_id ;
		if(isset($payment_detail->PaymentDetailId)){   $params[':PaymentDetailId'] = $payment_detail->PaymentDetailId ; } else { $params[':PaymentDetailId'] = NULL ; }
		if(isset($payment_detail->PaymentDetailReceiptNumber)){   $params[':PaymentDetailReceiptNumber'] = $payment_detail->PaymentDetailReceiptNumber ; } else { $params[':PaymentDetailReceiptNumber'] = NULL ; }
		if(isset($payment_detail->PaymentDetailReceiptPrefix)){   $params[':PaymentDetailReceiptPrefix'] = $payment_detail->PaymentDetailReceiptPrefix ; } else { $params[':PaymentDetailReceiptPrefix'] = NULL ; }
		if(isset($payment_detail->PaymentDetailText)){   $params[':PaymentDetailText'] = $payment_detail->PaymentDetailText ; } else { $params[':PaymentDetailText'] = NULL ; }
		if(isset($payment_detail->PaymentDetailDate)){   $params[':PaymentDetailDate'] = $payment_detail->PaymentDetailDate ; } else { $params[':PaymentDetailDate'] = NULL ; }
		if(isset($payment_detail->PaymentDetailTime)){   $params[':PaymentDetailTime'] = $payment_detail->PaymentDetailTime ; } else { $params[':PaymentDetailTime'] = NULL ; }
		if(isset($payment_detail->PaymentDetailAmount)){   $params[':PaymentDetailAmount'] = $payment_detail->PaymentDetailAmount ; } else { $params[':PaymentDetailAmount'] = NULL ; }
		if(isset($payment_detail->PaymentDetailMethod)){   $params[':PaymentDetailMethod'] = $payment_detail->PaymentDetailMethod ; } else { $params[':PaymentDetailMethod'] = NULL ; }

		
		
		
		$params[':UserId']  = getOwnerId();




		return $params;
	}

 

	private function convertPayment($payment,$detailed=false){


		$PaymentData = array(
			"PaymentId" => $payment->payment_id,
			"PaymentTotal" => $payment->payment_total,
			"PaymentTitle" => $payment->payment_title,
			"PaymentDate" => $payment->payment_date,

			"SupplierId" => $payment->supplier_id,
			"SupplierName" => $payment->supplier_name,
            "CurrencyId" => $payment->currency_id,
			"CurrencyName" => $payment->currency_name,
			"CurrencySymbol" => $payment->currency_symbol,
            "PaymentExchangeRate" => $payment->payment_exchange_rate,
            "PaymentsDetailTotal" => $payment->payment_details_total,
            "PaymentRemaining" => $payment->payment_remaining
		);
		
	

		if(isset($payment->user_id)){

			$UserModel = new UserModel();
			$user = $UserModel->getUser($payment->user_id);
			$PaymentData["UserName"] = $user->user_name;
		}
		
		
	
		
		return $PaymentData ;
	}

	

	private function convertPaymentDetail($payment_detail){

		


			$PaymentDetailData = array(
						  "PaymentId" => $payment_detail->payment_id,
						  "PaymentDetailId" => $payment_detail->payment_detail_id,
						  "PaymentDetailReceiptNumber" => $payment_detail->payment_detail_receipt_number,
						  "PaymentDetailReceiptPrefix" => $payment_detail->payment_detail_receipt_prefix,
						  "PaymentDetailText" => $payment_detail->payment_detail_text,
						  "PaymentDetailDate" => date('Y-m-d',strtotime($payment_detail->payment_detail_date)),
						  "PaymentDetailTime" => $payment_detail->payment_detail_time,
						  "PaymentDetailAmount" => $payment_detail->payment_detail_amount,
						  "PaymentDetailMethod" => $payment_detail->payment_detail_method,
						  "PaymentDetailCDate" => $payment_detail->payment_detail_cdate,
						  "PaymentDetailLMDate" => $payment_detail->payment_detail_lmdate,
						  "UserId" => $payment_detail->user_id
			);


		return $PaymentDetailData ;
	}
}
?>