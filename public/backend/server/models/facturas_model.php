<?php
class FacturasModel {
	
	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}
	
	public function getInvoice($InvoiceId){

		$result = $this->db->query("SELECT * FROM `erp_invoice` WHERE `invoice_id` = :invoice_id",
			array( ':invoice_id' => $InvoiceId )
		);

		$invoice=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertInvoice($invoice);

	}
	
	private function convertInvoice($invoice)
	{
			$InvoiceData = array(
			'InvoiceId' => $invoice->invoice_id,
			'InvoiceOrderId' => $invoice->invoice_order_id, 
			'InvoiceDateExpedition' => $invoice->invoice_date_expedition,
    		'InvoiceConditionsPayment' => $invoice->invoice_conditions_payment,
    		'InvoiceMethodPayment' => $invoice->invoice_method_payment,
    		'InvoiceFolio' => $invoice->invoice_folio,
    		'InvoicePlaceExpeditions' => $invoice->invoice_place_expeditions,
    		'InvoiceTypeExchange' => $invoice->invoice_type_exchange,
    		'InvoiceRegimenFiscal' => $invoice->invoice_regimen_fiscal,
			//'CustomerAddress' => $invoice->invoice_account_payment, 
			'InvoiceAccountPayment' => $invoice->invoice_account_payment, //$invoice->invoice_number_approval, 
			'InvoiceYearApproval' => $invoice->invoice_year_approval, 
			'InvoiceNumberCertificate' => $invoice->invoice_number_certificate,
			'InvoiceSeal' => $invoice->invoice_seal, 
	 		'InvoiceChain' => $invoice->invoice_chain, 
	 		'InvoiceCertificateSAT' => $invoice->invoice_certificate_SAT, 
	 		'InvoiceFolioFiscal' => $invoice->invoice_folio_fiscal,
	 		'InvoiceDateCertificate' => $invoice->invoice_date_certificate,
	 		'InvoiceSealSAT' => $invoice->invoice_seal_SAT

	 	);
	 	
	 	
       
	 	if(isset($invoice->invoice_order_id)){
	 		
	 		$SaleOrderModel = new SaleOrderModel();

       		$SaleOrder = $SaleOrderModel->getSaleOrder($invoice->invoice_order_id,true);
       		
       		$InvoiceData+=$SaleOrder;
		}
		
			return $InvoiceData;
	}
	
	
	private function createInvoice($Invoice){

		
		$this->db->query("
			INSERT INTO `erp_invoice` (
			    `invoice_id`,
			    `invoice_order_id`,
			    `invoice_date_expedition`,
			    `invoice_conditions_payment`,
			    `invoice_method_payment`,
			    `invoice_folio`,
			    `invoice_place_expeditions`,
			    `invoice_type_exchange`,
			    `invoice_regimen_fiscal`,
			    `invoice_account_payment`,
			    `invoice_number_approval`,
			    `invoice_year_approval`,
			    `invoice_number_certificate`,
			    `invoice_seal`, 
			    `invoice_chain`,
			    `invoice_certificate_SAT`,
			    `invoice_folio_fiscal`,
			    `invoice_date_certificate`,
			    `invoice_seal_SAT`
			) 
			VALUES ( 
				:InvoiceId,
				:InvoiceOrderId, 
				:InvoiceDateExpedition, 
				:InvoiceConditionsPayment, 
				:InvoiceMethodPayment, 
				:InvoiceFolio,  
				:InvoicePlaceExpeditions,
				:InvoiceTypeExchange, 
				:InvoiceRegimenFiscal, 
				:InvoiceAccountPayment, 
				:InvoiceNumberApproval, 
				:InvoiceYearApproval, 
				:InvoiceNumberCertificate, 
				:InvoiceSeal, 
				:InvoiceChain, 
				:InvoiceCertificateSAT, 
				:InvoiceFolioFiscal, 
				:InvoiceDateCertificate, 
				:InvoiceSealSAT 
			)",
			$this->fillInvoiceParams($Invoice)
		);	
		
		
		$this->db->query("UPDATE `erp_sale_orders` set   `invoice_id` = :InvoiceId where sale_order_id = :InvoiceOrderId",array(":InvoiceId" => $Invoice->InvoiceId,":InvoiceOrderId" =>$Invoice->InvoiceOrderId));	
		
		
	
		

		return $Invoice;	
	}
	
	private function fillInvoiceParams($invoice){
	
		
			if(!isset($invoice->InvoiceId)){ 
				
				$factory = new \PasswordLib\PasswordLib;
				$invoice->InvoiceId = base64_encode ($factory->createPasswordHash($invoice->InvoiceFolioFiscal,"",array()));
			}
		
 			$params =[];
		    
		    $params[':InvoiceId'] = ( isset($invoice->InvoiceId) ?  $invoice->InvoiceId : NULL );
		    
			$params[':InvoiceOrderId'] = ( isset($invoice->InvoiceOrderId) ? $invoice->InvoiceOrderId : NULL );
			$params[':InvoiceDateExpedition'] = ( isset($invoice->InvoiceDateExpedition) ?  $invoice->InvoiceDateExpedition : NULL );
    		$params[':InvoiceConditionsPayment'] = ( isset($invoice->InvoiceConditionsPayment) ?  $invoice->InvoiceConditionsPayment : NULL );
			$params[':InvoiceMethodPayment'] = ( isset($invoice->InvoiceMethodPayment) ?  $invoice->InvoiceMethodPayment :  NULL );
			$params[':InvoiceFolio'] = ( isset($invoice->InvoiceFolio) ? $invoice->InvoiceFolio : NULL );
			$params[':InvoicePlaceExpeditions'] = ( isset($invoice->InvoicePlaceExpeditions) ?  $invoice->InvoicePlaceExpeditions : NULL ) ;
			$params[':InvoiceTypeExchange'] = ( isset($invoice->InvoiceTypeExchange) ?  $invoice->InvoicePlaceExpeditions : NULL ) ;
			$params[':InvoiceRegimenFiscal'] = ( isset($invoice->InvoiceRegimenFiscal) ?  $invoice->InvoiceRegimenFiscal : NULL ) ;
			$params[':InvoiceAccountPayment'] = ( isset($invoice->InvoiceAccountPayment) ?  $invoice->InvoiceAccountPayment : NULL ) ;
			$params[':InvoiceNumberApproval'] = ( isset($invoice->InvoiceNumberApproval) ?  $invoice->InvoiceNumberApproval : NULL ) ;
			$params[':InvoiceYearApproval'] = ( isset($invoice->InvoiceYearApproval) ?  $invoice->InvoiceYearApproval : NULL ) ;
			$params[':InvoiceNumberCertificate'] = ( isset($invoice->InvoiceNumberCertificate) ?  $invoice->InvoiceNumberCertificate : NULL ) ;
			$params[':InvoiceSeal'] = ( isset($invoice->InvoiceSeal) ?  $invoice->InvoiceSeal : NULL ) ;
			$params[':InvoiceChain'] = ( isset($invoice->InvoiceChain) ?  $invoice->InvoiceChain : NULL ) ;
			$params[':InvoiceCertificateSAT'] = ( isset($invoice->InvoiceCertificateSAT) ?  $invoice->InvoiceCertificateSAT : NULL ) ;
			$params[':InvoiceFolioFiscal'] = ( isset($invoice->InvoiceFolioFiscal) ?  $invoice->InvoiceFolioFiscal : NULL ) ;
			$params[':InvoiceDateCertificate'] = ( isset($invoice->InvoiceDateCertificate) ?  $invoice->InvoiceDateCertificate : NULL ) ;
			$params[':InvoiceSealSAT'] = ( isset($invoice->InvoiceSealSAT) ?  $invoice->InvoiceSealSAT : NULL ) ;
			
			//print_r($params);
			return $params;

	}
	
	
	private function convertInvoiceSave($NumOrden,$Comprobante,$Timbre,$Regimen,$Cadena)
	{
			$invoice_Data = array(
			//'InvoiceId' => $NumOrden, //CAMBIAR POR RANDOM
			'InvoiceOrderId' => $NumOrden, 
			'InvoiceDateExpedition' => $Comprobante->fecha,
    		'InvoiceConditionsPayment' => $Comprobante->formaDePago,
    		'InvoiceMethodPayment' => $Comprobante->metodoDePago,
    		'InvoiceFolio' => $Comprobante->folio,
    		'InvoicePlaceExpeditions' => $Comprobante->LugarExpedicion,
    		'InvoiceTypeExchange' => $Comprobante->TipoCambio,
    		'InvoiceRegimenFiscal' => $Regimen->Regimen,
			//'CustomerAddress' => $Timbre->invoice_account_payment, 
			'InvoiceAccountPayment' => 'NO IDENTIFICADO', //$Timbre->invoice_number_approval, 
			'InvoiceYearApproval' => '', //$Timbre->invoice_year_approval, 
			'InvoiceNumberCertificate' => $Comprobante->noCertificado,
			'InvoiceSeal' => $Comprobante->sello, 
	 		'InvoiceChain' => $Cadena,//$Timbre->invoice_chain, 
	 		'InvoiceCertificateSAT' => $Comprobante->certificado, 
	 		'InvoiceFolioFiscal' => $Timbre->UUID,
	 		'InvoiceDateCertificate' => $Timbre->FechaTimbrado,
	 		'InvoiceSealSAT' => $Timbre->selloSAT

	 	);
	 	//print_r($invoice_Data);
	 	return (object)$invoice_Data;
	}
	
	
	public function deleteInvoice($NumOrden){
		
		$AccountModel=new AccountModel();
		$Emisor=(object)$AccountModel->getAccount();
		
		$SaleOrderModel=new SaleOrderModel();
		$Orden=(object)$SaleOrderModel->getSaleOrder($NumOrden);
		
		$Cliente=(object)$Orden->customer;
		
		$passCerEmisor=$Emisor->cert_password; //'12345678a';
		
		$noTimbresEmisor=$Emisor->invoice_folios;
		
		$statusEmisor=$Emisor->active;
		
		if($statusEmisor==1)
		$datos['PAC']['produccion'] = 'SI'; //   [SI|NO]
		else
		$datos['PAC']['produccion'] = 'NO'; //   [SI|NO]
		
		
		$datos['rfcEmisor']=$Emisor->rfc; //$emi[0]['rfc'];

		$datos['conf']['cer'] = '../accounts_data/facturas/cerKey/'.$Emisor->rfc.'.cer.pem';

		$datos['conf']['key'] = '../accounts_data/facturas/cerKey/'.$Emisor->rfc.'.enc.key';

		$datos['conf']['pass'] =$passCerEmisor;

		$datos['cfdi']='../accounts_data/facturas/timbrados/'.$Emisor->rfc.'-'.$Orden->SaleOrderId.'.xml';
		
		
		if($statusEmisor==1 and $noTimbresEmisor<=0)
		{
			//return "Cantidad Insuficiente de Timbres";
			throw new Exception('Cantidad Insuficiente de Timbres');
		}
		else
		{
			$res= cfdi_cancelar($Orden->SaleOrderId,$datos);
			
			//var_dump($res);
			if($statusEmisor==1)
			{
				//Descontar un timbre de la base de datos
				$AccountModel->updateTimbresAccount();
			}
			return $res;
		}
	}

	public function createFactura($NumOrden){
		
		$AccountModel=new AccountModel();
		$Emisor=(object)$AccountModel->getAccount();

		$SaleOrderModel=new SaleOrderModel();
		$Orden=(object)$SaleOrderModel->getSaleOrder($NumOrden);
		
		$Cliente=(object)$Orden->customer;
		
		$Productos=(object)$Orden->items;
		
		$rfcEmisor=$Emisor->rfc;//"AAD990814BP7";
		$nombre=$Emisor->trade_name; //"jesus antonio gonzalez palma";
		
		$regimenFiscal=$Emisor->trade_regimen; //"sistemas computaciones";
		
		$emailEmisor=$Emisor->email; //"jesus.palma@hotmail.com";
		
		$noTimbresEmisor=$Emisor->invoice_folios; //"100";
		
		$passCerEmisor=$Emisor->cert_password;//'12345678a';
		
		$statusEmisor=$Emisor->active;
		if($statusEmisor==1)
		$datos['PAC']['produccion'] = 'SI'; //   [SI|NO]
		else
		$datos['PAC']['produccion'] = 'NO'; //   [SI|NO]
				
		$datos['conf']['cer'] = '../accounts_data/facturas/cerKey/'.$rfcEmisor.'.cer.pem';
		$datos['conf']['key'] = '../accounts_data/facturas/cerKey/'.$rfcEmisor.'.key.pem';
		
		$datos['conf']['pass'] =$passCerEmisor;
	
		//OPCIONAL, ACTIVAR SOLO EN CASO DE CONFLICTOS
		$datos['remueve_acentos']='SI';

		//OPCIONAL, UTILIZAR LA LIBRERIA PHP DE OPENSSL, DEFAULT SI
		$datos['php_openssl']='SI';

		$serie='';
		$folio=$Orden->SaleOrderId; //'123';

		$metodo_pago=$Orden->SaleOrderPaymentMethod; //'Efectivo';
		$forma_pago=$Orden->SaleOrderPaymentForm; //'PAGO EN UNA SOLA EXHIBICION';
		$tipocomprobante=$Orden->SaleOrderProofType; //'ingreso';
		
		$tipoCambio=$Orden->SaleOrderExchangeRate;//'1';
		
		$LugarExpedicion=$Emisor->localidad; //'Empalme Sonora';
		
		$moneda=$Orden->CurrencySymbol; //'MXN';
		
		$subtotal=$Orden->SaleOrderSubtotal; //'178.00';
		
		$descuento=$Orden->SaleOrderDiscounts; //'0';
		
		$total=$Orden->SaleOrderTotal; //'116';
		
		$datos['factura']['serie'] = $serie; //opcional
		$datos['factura']['folio'] = $folio; //opcional
		$datos['factura']['fecha_expedicion'] = date('Y-m-d H:i:s',time()-120);// Opcional  "time()-120" para retrasar la hora 2 minutos para evitar falla de error en rango de fecha
		$datos['factura']['metodo_pago'] = $metodo_pago; // EFECTIV0, CHEQUE, TARJETA DE CREDITO, TRANSFERENCIA BANCARIA, NO IDENTIFICADO
		$datos['factura']['forma_pago'] = $forma_pago;  //PAGO EN UNA SOLA EXHIBICION, CREDITO 7 DIAS, CREDITO 15 DIAS, CREDITO 30 DIAS, ETC
		$datos['factura']['tipocomprobante'] = $tipocomprobante; 
		$datos['factura']['moneda'] = $moneda; // MXN USD EUR
		$datos['factura']['tipocambio'] = $tipoCambio; // OPCIONAL (MXN = 1.00, OTRAS EJ: USD = 13.45; EUR = 16.86)
		$datos['factura']['LugarExpedicion'] = $LugarExpedicion;
		//$datos['factura']['NumCtaPago'] = '0234'; //opcional; 4 DIGITOS pero obligatorio en transferencias y cheques
		$datos['factura']['RegimenFiscal'] = $regimenFiscal;

		$calleDomFisEmisor=$Emisor->street; //'calle';
		$noExteriorDomFisEmisor= $Emisor->numExt; //'15';
		$noInteriorDomFisEmisor=$Emisor->numInt; //'A';
		$coloniaDomFisEmisor=$Emisor->colony; //'Moderna';
		$localidadDomFisEmisor=$Emisor->localidad; //'Empalme';
		$municipioDomFisEmisor=$Emisor->city; //'Empalme';
		$estadoDomFisEmisor=$Emisor->state; //'Sonora';
		$paisDomFisEmisor=$Emisor->country; //'Mexico';
		$CodigoPostalDomFisEmisor=$Emisor->cp; //'85330';

		$datos['emisor']['rfc'] = $rfcEmisor; //RFC DE PRUEBA 
		$datos['emisor']['nombre'] = $nombre;  // EMPRESA DE PRUEBA
		$datos['emisor']['DomicilioFiscal']['calle'] = $calleDomFisEmisor;
		$datos['emisor']['DomicilioFiscal']['noExterior'] = $noExteriorDomFisEmisor;
		$datos['emisor']['DomicilioFiscal']['noInterior'] = $noInteriorDomFisEmisor; //(opcional)
		$datos['emisor']['DomicilioFiscal']['colonia'] = $coloniaDomFisEmisor;
		$datos['emisor']['DomicilioFiscal']['localidad'] = $localidadDomFisEmisor;
		$datos['emisor']['DomicilioFiscal']['municipio'] = $municipioDomFisEmisor; // o delegacion
		$datos['emisor']['DomicilioFiscal']['estado'] = $estadoDomFisEmisor;
		$datos['emisor']['DomicilioFiscal']['pais'] = $paisDomFisEmisor;
		$datos['emisor']['DomicilioFiscal']['CodigoPostal'] = $CodigoPostalDomFisEmisor; // 5 digitos

		/*
		$calleDomExpEmisor=$rowDomExpEmisor['calle'];
		$noExteriorDomExpEmisor=$rowDomExpEmisor['noExt'];
		$noInteriorDomExpEmisor=$rowDomExpEmisor['noInt'];
		$coloniaDomExpEmisor=$rowDomExpEmisor['col'];
		$localidadDomExpEmisor=$rowDomExpEmisor['localidad'];
		$municipioDomExpEmisor=$rowDomExpEmisor['municipio'];
		$estadoDomExpEmisor=$rowDomExpEmisor['estado'];
		$paisDomExpEmisor=$rowDomExpEmisor['pais'];
		$CodigoPostalDomExpEmisor=$rowDomExpEmisor['cp'];
		*/
		
		$calleDomExpEmisor=$calleDomFisEmisor;
		$noExteriorDomExpEmisor=$noExteriorDomFisEmisor;
		$noInteriorDomExpEmisor=$noInteriorDomFisEmisor;
		$coloniaDomExpEmisor=$coloniaDomFisEmisor;
		$localidadDomExpEmisor=$localidadDomFisEmisor;
		$municipioDomExpEmisor=$municipioDomFisEmisor;
		$estadoDomExpEmisor=$estadoDomFisEmisor;
		$paisDomExpEmisor=$paisDomFisEmisor;
		$CodigoPostalDomExpEmisor=$CodigoPostalDomFisEmisor;
		
		//SI EX EXPEDIDO EN SUCURSAL CAMBIA EL DOMICILIO
		//SI ES EN EL MISMO DOMICILIO REPETIR INFORMACION
		$datos['emisor']['ExpedidoEn']['calle'] = $calleDomExpEmisor; 
		$datos['emisor']['ExpedidoEn']['noExterior'] = $noExteriorDomExpEmisor;
		$datos['emisor']['ExpedidoEn']['noInterior'] = $noInteriorDomExpEmisor; //(opcional)
		$datos['emisor']['ExpedidoEn']['colonia'] = $coloniaDomExpEmisor;
		$datos['emisor']['ExpedidoEn']['localidad'] = $localidadDomExpEmisor;
		$datos['emisor']['ExpedidoEn']['municipio'] = $municipioDomExpEmisor; // O DELEGACION
		$datos['emisor']['ExpedidoEn']['estado'] = $estadoDomExpEmisor;
		$datos['emisor']['ExpedidoEn']['pais'] = $paisDomExpEmisor;
		$datos['emisor']['ExpedidoEn']['CodigoPostal'] = $CodigoPostalDomExpEmisor; // 5 digitos

		$rfcRecep=$Cliente->CustomerRFC;//'GOPJ880228N52';
		$nombreRecep=$Cliente->CustomerName; //'Jesus Gonzalez Palma';
		$emailRecep=$Cliente->CustomerEmail; //'jesus.palma@hotmail.com';
					
				
		// IMPORTANTE PROBAR CON NOMBRE Y RFC REAL O GENERARA ERROR DE XML MAL FORMADO
		$datos['receptor']['rfc'] = $rfcRecep;
		$datos['receptor']['nombre'] = $nombreRecep;



		$calleDomFisRec= $Cliente->CustomerStreet; //'Melchor O.';
		$noExteriorDomFisRec=$Cliente->CustomerNumExt; //'23';
		$noInteriorDomFisRec=$Cliente->CustomerNumInt; //'';
		$coloniaDomFisRec=$Cliente->CustomerColony; //'Moderna';
		$localidadDomFisRec=$Cliente->CustomerLocalidad; //'Empalme';
		$municipioDomFisRec=$Cliente->CustomerCity; //'Empalme';
		$estadoDomFisRec=$Cliente->CustomerState; //'Sonora';
		$paisDomFisRec=$Cliente->CustomerCountry; //'Mexico';
		$CodigoPostalDomFisRec=$Cliente->CustomerCP; //'85333';
				
			//opcional
		$datos['receptor']['Domicilio']['calle'] = $calleDomFisRec;
		$datos['receptor']['Domicilio']['noExterior'] = $noExteriorDomFisRec;
		$datos['receptor']['Domicilio']['noInterior'] = $noInteriorDomFisRec;
		$datos['receptor']['Domicilio']['colonia'] = $coloniaDomFisRec;
		$datos['receptor']['Domicilio']['localidad'] = $localidadDomFisRec;
		$datos['receptor']['Domicilio']['municipio'] = $municipioDomFisRec;
		$datos['receptor']['Domicilio']['estado'] = $estadoDomFisRec;
		$datos['receptor']['Domicilio']['pais'] = $paisDomFisRec;
		$datos['receptor']['Domicilio']['CodigoPostal'] = $CodigoPostalDomFisRec; // 5 digitos
		
		foreach($Productos as $Producto )
		{
		$concepto['cantidad'] =$Producto['SaleOrderDetailQTY']; //'5';
		$concepto['unidad']=$Producto['SaleOrderDetailUM']; //'unidad';
		$concepto['ID']=$Producto['ProductId']; //'1234';
		$concepto['descripcion']=$Producto['Product']['ProductName']; //'descripcion de producto';
		$concepto['valorunitario']=$Producto['SaleOrderDetailPrice']; //'20';
		$concepto['importe']=$Producto['SaleOrderDetailTotal']; //'100';

		$datos['conceptos'][] = $concepto;
		}
				
		$datos['factura']['subtotal'] = $subtotal; // sin impuestos
		$datos['factura']['descuento'] = $descuento; // descuento sin impuestos
		$datos['factura']['total'] = $total; // total incluyendo impuestos

		
		$translado1['impuesto'] = 'IVA';
		$translado1['tasa'] = '16';
			//$translado1['importe'] = 160.00; // iva de los productos facturados
		$translado1['importe'] = ($subtotal-$descuento)*(.16); // iva de los productos facturados

		$datos['impuestos']['translados'][0] = $translado1;

		/*
		//EJEMPLO DE TRANSLADO IMPUESTO LOCAL
		$transladolocal['ImpLocTrasladado']='ISH';
		$transladolocal['TasadeTraslado']='3';  //varia 2 o 3% segun el tipo de cliente
		$transladolocal['Importe']=10.00;
		$datos['ImpuestosLocales']['TrasladosLocales'][]=$transladolocal;
		*/

		//SE CREA UN NOMBRE CON EL RFC DEL USUARIO Y CON EL FOLIO DE LA FACTURA
		$identificadorfac=$rfcEmisor.'-'.$folio;

		//RUTA DONDE ALMACENARA EL CFDI
		$datos['cfdi']='../accounts_data/facturas/timbrados/'.$identificadorfac.'.xml';
		// OPCIONAL GUARDAR EL XML GENERADO ANTES DE TIMBRARLO
		$datos['xml_debug']='../accounts_data/facturas/timbrados/'.$identificadorfac.'_debug.xml';
		//print_r($datos);
		
		$datos['SDK']['ruta']='../accounts_data/facturas/';
		
		
		if($statusEmisor==1 and $noTimbresEmisor<=0)
			$timbrar=0;
		else
			$timbrar=1;
		
		if ($timbrar==1)
		{
			$res= cfdi_generar_xml($datos);
			
			//if($res=='Timbrado')
			if($res->status=='Timbrado')
			{
				if($statusEmisor==1)
				{
					//Descontar un timbre de la base de datos
					$AccountModel->updateTimbresAccount();
				}
				
				$xml = simplexml_load_file($datos['cfdi']);
	        	$ns = $xml->getNamespaces(true);
	        	$xml->registerXPathNamespace('c', $ns['cfdi']);
	        	$xml->registerXPathNamespace('t', $ns['tfd']);
	        	
	       
	        	$json_string_Comprobante = json_encode($xml->xpath('//c:Comprobante'));
	        	
	        	$result_array_Comprobante = json_decode($json_string_Comprobante, TRUE);
				
				$Comprobante=$result_array_Comprobante['0']['@attributes'];
				
				$json_string_Regimen = json_encode($xml->xpath('//c:RegimenFiscal'));
	        	
	        	$result_array_Regimen = json_decode($json_string_Regimen, TRUE);
				
				$Regimen=$result_array_Regimen['0']['@attributes'];
	
				$json_string_Timbre = json_encode($xml->xpath('//t:TimbreFiscalDigital'));
				
				$result_array_Timbre = json_decode($json_string_Timbre, TRUE);
				
				$Timbre=$result_array_Timbre['0']['@attributes'];
				
				$invoice_Data = $this->convertInvoiceSave($NumOrden,(object)$Comprobante,(object)$Timbre,(object)$Regimen,$res->cadena);
				$this->createInvoice($invoice_Data);
				
				$FacturaReport = new FacturaReport();
				 
	       		$FacturaReport->sendReport($Orden);
				
				return $Orden;
			}
			throw new Exception('Verificar Datos de Factura y/o Cliente.');
		}
		else
		{
			throw new Exception('Cantidad Insuficiente de Timbres');
		}		
	}
}
?>