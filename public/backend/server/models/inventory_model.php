<?php
class InventoryModel {
	
	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getInventories(){

		$result = $this->db->query("
			SELECT  
`inventory_id` , 
`inventory_start_date` ,  
`inventory_end_date` ,  
`inventory_folio` ,  
`inventory_notes` ,  
`inventory_status` , 
`erp_inventory`.`place_id`,
`erp_places`.`place_name`
FROM  `erp_inventory` 
INNER JOIN  `erp_places` ON `erp_places`.`place_id` = `erp_inventory`.`place_id`
			"
			,
			array()
		);

		$inventories = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($inventories as $inventory) {

        	array_push($data, $this->convertInventory($inventory));
        }

		return $data;
	}


	public function getInventory($InventoryId){

		$result = $this->db->query("
		SELECT 	
				*
        	FROM `erp_inventory`
        	 
		WHERE inventory_id = :InventoryId
		",
			array(':InventoryId' => $InventoryId )
		);

		$inventory=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertInventory($inventory,true);

	}

	
	public function createInventory($inventory){

		$this->db->query("
			INSERT INTO `erp_inventory` (
				`inventory_id`, 
				`inventory_cdate`, 
				`inventory_start_date`, 
                `inventory_end_date`, 
                `inventory_folio`,
				`inventory_notes`,
				`inventory_status`,
				`place_id`, 
				`user_id`
			) 
			VALUES ( 
				:InventoryId ,
				NOW(),
				:InventoryStartDate,
				:InventoryEndDate,
				0, 
				:InventoryNotes,
                'Nuevo',				
				:PlaceId, 
				:UserId
			)",
			$this->fillParams($inventory)
		);	

		$inventory->InventoryId = $this->db->getInsertId();
        $inventory->InventoryStatus = 'Nuevo';
		
		return $inventory;	
	}


	public function deleteInventory($InventoryId){
		
		$this->db->query("
			DELETE FROM `erp_inventory` 
			WHERE `inventory_id` = :InventoryId",
		array(':InventoryId' => $InventoryId ));
		
	}
	
	public function setInventoryStatus($inventory,$InventoryStatus){

			$this->db->conn->beginTransaction();
 			$sql = "
 				UPDATE `erp_inventory` SET 
 					`inventory_status`= :InventoryStatus
 				WHERE 	`inventory_id`=:InventoryId
 				 ";

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(':InventoryId' => $inventory->InventoryId, ':InventoryStatus'=>$InventoryStatus));
		    $this->db->conn->commit();
		    $inventory->InventoryStatus = $InventoryStatus;
			return $inventory;
	}
	
	
	



	private function fillParams($inventory) {

		$params = [];

		if(isset($inventory->InventoryId))	 { $params[':InventoryId'] = $inventory->InventoryId; } else { $params[':InventoryId'] = NULL; }; 
		if(isset($inventory->InventoryStartDate))	 { $params[':InventoryStartDate'] = $inventory->InventoryStartDate; } else { $params[':InventoryStartDate'] = NULL; }; 
		if(isset($inventory->InventoryEndDate))	 { $params[':InventoryEndDate'] = $inventory->InventoryEndDate; } else { $params[':InventoryEndDate'] = NULL; }; 
		//if(isset($inventory->InventoryCode))	 { $params[':InventoryCode'] = $inventory->InventoryCode; } else { $params[':InventoryCode'] = NULL;};  
        if(isset($inventory->InventoryNotes))	 { $params[':InventoryNotes'] = $inventory->InventoryNotes; } else { $params[':InventoryNotes'] = NULL;};  
		if(isset($inventory->place->PlaceId))	 { $params[':PlaceId'] = $inventory->place->PlaceId; } else { $params[':PlaceId'] = NULL;}; 
        $params[':UserId']  = getOwnerId();
		return $params;
	}

	private function convertInventory($inventory)
	{
       $data =  array(
			"InventoryId" => $inventory->inventory_id,
			"InventoryCode" => $inventory->inventory_folio, 
			"InventoryStartDate" => $inventory->inventory_start_date,
			"InventoryEndDate" => $inventory->inventory_end_date,
			"InventoryNotes" => $inventory->inventory_notes,
			"InventoryStatus" => $inventory->inventory_status,
			"PlaceName" => $inventory->place_name, 
			"PlaceId" => $inventory->place_id
		);
		
         if(isset($inventory->place_id)){
         
            $data["place"] = array('PlaceId' => $inventory->place_id, 'PlaceName' => $inventory->place_name );
           
         
         }
         
        return $data;
       
		if(isset($inventory->place_id))
		{
        $InventoryData["place"] = array('PlaceId' => $inventory->place_id, 'PlaceName' => $inventory->place_name );
		}
    
    	return $InventoryData;
	}


    /* Inventario Detalle */

	public function getInventoryDetails($inventory_id){
		$result = $this->db->query("
			SELECT  
			      `inventory_id`,
                  `inventory_detail_id`,
                  `erp_products`.`product_id`,
                  `erp_products`.`product_name`,
                  `inventory_detail_qty`,
                  `inventory_cdate`,
                  `erp_ums`.`um_name`,
                  `erp_inventory_details`.`user_id`,
                  `oauth_users`.`user_name`

			FROM `erp_inventory_details`
            INNER JOIN  `oauth_users` ON `erp_inventory_details`.`user_id` = `oauth_users`.`user_id`
            INNER JOIN  `erp_products` ON `erp_inventory_details`.`product_id` = `erp_products`.`product_id`
            INNER JOIN  `erp_ums` ON `erp_inventory_details`.`um_id` = `erp_ums`.`um_id`

			WHERE `inventory_id` = :InventoryId"
			, array( ':InventoryId' => $inventory_id));

			$inventory_details = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($inventory_details as $inventory_detail) {

        	array_push($data, $this->convertInventoryDetail($inventory_detail));
        }

		return $data;


	}

	private function getInventoryDetailIdentity($inventory_id){
		

        $inventory_detail_id = 1;

		$sql = "
			SELECT  MAX(`inventory_detail_id`)+1 as identity FROM `erp_inventory_details` WHERE `inventory_id` = :InventoryId
		";

		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':InventoryId' => $inventory_id));


 		$result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->identity)){
		 	$inventory_detail_id = $result->identity;
		 }

		
		return $inventory_detail_id;	
			
    }

	private function convertInventoryDetail($inventory_detail){

			$InventoryDetailData = array(
						  "InventoryId" => $inventory_detail->inventory_id,
						  "InventoryDetailId" => $inventory_detail->inventory_detail_id,
						  "ProductId" => $inventory_detail->product_id,
						  "ProductName" => $inventory_detail->product_name,
						  "InventoryDetailQty" => $inventory_detail->inventory_detail_qty,
						  "InventoryDate" => $inventory_detail->inventory_cdate,
                          "UmId" => $inventory_detail->um_id,
						  "UmName" => $inventory_detail->um_name,
						  "UserId" => $inventory_detail->user_id,
						  "UserName" => $inventory_detail->user_name
						
			);

		
		return $InventoryDetailData ;
	}


	public function createInventoryDetail($inventory_detail){


		$this->db->query("
			INSERT INTO `erp_inventory_details` (
				`inventory_detail_id`,
				`inventory_id`, 
				`product_id`,
				`um_id`,
				`inventory_detail_qty`,
				`inventory_cdate`,
				`user_id`
			) 
			VALUES ( 
				:InventoryDetailId ,
				:InventoryId, 
				:ProductId,
                :UmId,
                :InventoryDetailQty,
                NOW(),
                :UserId
			)",
			$this->fillParams($inventory_detail)
		);	

		$inventory_detail->InventoryDetailId = $this->db->getInsertId();

		return $inventory_detail;	
	}



	private function fillParamsDetails($inventory_detail) {

		$params = [];
		if(isset($inventory_detail->InventoryDetailId))	 { $params[':InventoryDetailId'] = $inventory_detail->InventoryDetailId; } else { $params[':InventoryDetailId'] = NULL; }; 
		if(isset($inventory_detail->InventoryId))	 { $params[':InventoryId'] = $inventory_detail->InventoryId; } else { $params[':InventoryId'] = NULL; }; 
		if(isset($inventory_detail->ProductId))	 { $params[':ProductId'] = $inventory_detail->ProductId; } else { $params[':ProductId'] = NULL; }; 
		if(isset($inventory_detail->UmId))	 { $params[':UmId'] = $inventory_detail->UmId; } else { $params[':UmId'] = NULL; }; 
	    if(isset($inventory_detail->InventoryDetailQty))	 { $params[':InventoryDetailQty'] = $inventory_detail->InventoryDetailQty; } else { $params[':InventoryDetailQty'] = NULL;};  
		$params[':UserId']  = getOwnerId();
		return $params;
	}




   






}
?>