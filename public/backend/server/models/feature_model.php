<?php
class FeatureModel {

	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getFeatureGroups(){

		$result = $this->db->query("
			SELECT 
				`feature_group_id`,
				`feature_group_name`,
				`feature_group_description`
			FROM `erp_feature_groups`"
			,
			array()
		);

		$feature_groups = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($feature_groups as $feature_group) {

        	array_push($data, $this->convertFeatureGroup($feature_group,false));
        }

		return $data;
	}

	public function getFeatureGroup($FeatureGroupId){


		$result = $this->db->query("
			SELECT 
				`feature_group_id`,
				`feature_group_name`, 
				`feature_group_description`
			FROM `erp_feature_groups` 
			WHERE `feature_group_id` = :FeatureGroupId"
			,
			array( ':FeatureGroupId' => $FeatureGroupId )
		);

		$feature_group=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertFeatureGroup($feature_group,true);

	}

	public function createFeatureGroup($feature_group){

			$this->db->query("
			INSERT INTO `erp_feature_groups` (
				`feature_group_id`,
				`feature_group_name`, 
				`feature_group_description`
			) 
			VALUES ( 
				:FeatureGroupId ,
				:FeatureGroupName, 
				:FeatureGroupDescription
			)",
			$this->featureGroupParams($feature_group)
		);	

		$feature_group->FeatureGroupId = $this->db->getInsertId();

		return $feature_group;	


	}

	public function updateFeatureGroup($feature_group){

			$this->db->query("
			UPDATE `erp_feature_groups` SET 
				`feature_group_name`=:FeatureGroupName, 
				`feature_group_description`=:FeatureGroupDescription
		 WHERE `feature_group_id` = :FeatureGroupId",
		   $this->featureGroupParams($feature_group)
		 );
	}

	public function deleteFeatureGroup($FeatureGroupId){


		$this->db->query("
			DELETE FROM `erp_feature_groups` 
			WHERE `feature_group_id` = :FeatureGroupId",
		array(':FeatureGroupId' => $FeatureGroupId ));


	}

	private function featureGroupParams($feature_group) {

		$params = [];

		if(isset($feature_group->FeatureGroupId))	 { $params[':FeatureGroupId'] = $feature_group->FeatureGroupId; } else { $params[':FeatureGroupId'] = NULL; }; 
		if(isset($feature_group->FeatureGroupName))	 { $params[':FeatureGroupName'] = $feature_group->FeatureGroupName; } else { $params[':FeatureGroupName'] = NULL; }; 
		if(isset($feature_group->FeatureGroupDescription))	 { $params[':FeatureGroupDescription'] = $feature_group->FeatureGroupDescription; } else { $params[':FeatureGroupDescription'] = NULL; }; 
	
		return $params;
	}

	private function convertFeatureGroup($feature_group,$detailed=false) {

        $FeatureGroupArray =array(
			'FeatureGroupId' => $feature_group->feature_group_id,
			'FeatureGroupName' => $feature_group->feature_group_name, 
			'FeatureGroupDescription' => $feature_group->feature_group_description
    	);

    	if($detailed){

    		$FeatureGroupArray["features"] = $this->getFeatures($feature_group->feature_group_id);
    	}



    	return $FeatureGroupArray;


	}



	public function getFeatures($FeatureGroupId){

		$result = $this->db->query("
			SELECT 
				`feature_group_id`,
				`feature_id`,
				`feature_name`,
				`feature_description`
			FROM `erp_features` 
			WHERE `feature_group_id` = :FeatureGroupId"
			,
			array(':FeatureGroupId' => $FeatureGroupId)
		);

		$features = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($features as $feature) {

        	array_push($data, $this->convertFeature($feature));
        }

		return $data;
	}

	public function getFeature($FeatureGroupId,$FeatureId){


		$result = $this->db->query("
			SELECT 
				`feature_group_id`,
				`feature_id`,
				`feature_name`, 
				`feature_description`
			FROM `erp_features` 
			WHERE `feature_group_id` = :FeatureGroupId and `feature_id` = :FeatureId"
			,
			array( ':FeatureGroupId' => $FeatureGroupId,':FeatureId' => $FeatureId )
		);

		$feature=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertFeature($feature);

	}

	public function createFeature($FeatureGroupId,$feature){

		   $result=$this->db->query("
		   		SELECT IFNULL(MAX( `feature_id` ) , 0 ) + 1  as FeatureId
		   		FROM  `erp_features`  
		   		WHERE  `feature_group_id`= :FeatureGroupId
		   		",
		   		array(":FeatureGroupId" => $FeatureGroupId)
		   	);
            $obj = $result->fetch(PDO::FETCH_OBJ);

            $feature->FeatureId = $obj->FeatureId;

			$this->db->query("
			INSERT INTO `erp_features` (
				`feature_group_id`,
				`feature_id`,
				`feature_name`, 
				`feature_description`
			) 
			VALUES ( 
				:FeatureGroupId,
				:FeatureId ,
				:FeatureName, 
				:FeatureDescription
			)",
			$this->featureParams($feature)
		);	

		//$feature->FeatureId = $this->db->getInsertId();

		return $feature;	


	}

	public function updateFeature($feature){

			$this->db->query("
			UPDATE `erp_features` SET 
				`feature_name`=:FeatureName, 
				`feature_description`=:FeatureDescription
		 WHERE `feature_group_id`=:FeatureGroupId and `feature_id` = :FeatureId",
		   $this->featureParams($feature)
		 );
	}

	public function deleteFeature($FeatureGroupId,$FeatureId){


		$this->db->query("
			DELETE FROM `erp_features` 
			WHERE `feature_group_id`=:FeatureGroupId and `feature_id` = :FeatureId",
		array(':FeatureGroupId' => $FeatureGroupId ,':FeatureId' => $FeatureId ));


	}

	private function featureParams($feature) {

		$params = [];
		if(isset($feature->FeatureGroupId))	 { $params[':FeatureGroupId'] = $feature->FeatureGroupId; } else { $params[':FeatureGroupId'] = NULL; }; 
		if(isset($feature->FeatureId))	 { $params[':FeatureId'] = $feature->FeatureId; } else { $params[':FeatureId'] = NULL; }; 
		if(isset($feature->FeatureName))	 { $params[':FeatureName'] = $feature->FeatureName; } else { $params[':FeatureName'] = NULL; }; 
		if(isset($feature->FeatureDescription))	 { $params[':FeatureDescription'] = $feature->FeatureDescription; } else { $params[':FeatureDescription'] = NULL; }; 
	
		return $params;
	}

	private function convertFeature($feature) {


		return array(
			'FeatureGroupId' => $feature->feature_group_id,
			'FeatureId' => $feature->feature_id,
			'FeatureName' => $feature->feature_name, 
			'FeatureDescription' => $feature->feature_description,
			'FeatureSelected' => false
    	);


	}

}
?>