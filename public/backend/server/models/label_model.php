<?php
class LabelModel {


	private $label_select_sql = "SELECT `label_id`, `label_batch`,`label_customer_po`, `label_customer_item`, `label_box_size`, `label_box_weight`, `label_part_number`, `label_job_no`, `label_rack_id`, `label_number`, `label_total`, `product_id`,`label_per_ctn`,  `label_cdate`, `erp_labels`.`user_id`,`oauth_users`.`user_name`	,`label_is_prototype`,`label_reference`,`label_header`,`label_deleted`,`bundle_type_id`,`label_print_disabled`, `label_customer_item_title` FROM `erp_labels` LEFT JOIN `oauth_users` ON ( `oauth_users`.`user_id` = `erp_labels`.`user_id`)";
	
	private $db;

	public function __construct(){

		
		 $this->db = DB::withAccount(AccountModel::getAccountConfiguration());
	}

	public function getLabelPrototypes($search){

		$sql = $this->label_select_sql." WHERE (`label_deleted` = 0 OR `label_deleted` IS NULL) AND label_is_prototype = 1 AND  (label_batch like :search OR label_part_number like :search OR label_reference like :search) AND ( label_print_disabled <> 1 OR label_print_disabled IS NULL) ";
		$result = $this->db->query($sql
			,
			array(':search' => '%'.$search.'%')
		);

		$labels = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($labels as $label) {

        	array_push($data, $this->convertLabel($label));
        }

		return $data;
	}
	
	public function getLabelHeaders($search){

		$sql = $this->label_select_sql." WHERE label_header = 1 AND  (label_batch like :search OR label_part_number like :search OR label_reference like :search)";
		$result = $this->db->query($sql
			,
			array(':search' => '%'.$search.'%')
		);

		$labels = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($labels as $label) {

        	array_push($data, $this->convertLabel($label));
        }

		return $data;
		
	}
	
	public function getLabelHeader($batch){
	
	    $sql = $this->label_select_sql." WHERE label_header = 1 AND  label_batch = :batch";
		$result = $this->db->query($sql
			,
			array(':batch' => $batch)
		);

		$label=$result->fetch(PDO::FETCH_OBJ);
        


		return $this->convertLabel($label);
	}
	
 	public function getLabels($label_batch,$label_number_from,$label_number_to,$label_date_from,$label_date_to,$include_printed_disabled_labels=false,$include_only_prototypes=false){

		$sql = $this->label_select_sql;
		
		
		if($include_only_prototypes){
			
			$sql = $sql." WHERE  label_is_prototype = 1";
		}
		else {
			
			$sql = $sql." WHERE (label_header <> 1 or label_header IS NULL)";
			
		}
		
		$params = [];
		
        
        if(isset($label_batch) && $label_batch != ""){
        	
        	$sql = $sql." AND label_batch = :label_batch  ";
        	$params[":label_batch"] = $label_batch ;
        	
        }
        
		if($label_number_from > 0){
			
			$sql = $sql." AND label_number >= :label_number_from  ";
			$params[":label_number_from"] = $label_number_from;	
			
		}

		if($label_number_to > 0){
			
			$sql = $sql." AND  label_number <= :label_number_to";
			$params[":label_number_to"] = $label_number_to;	
			
		}
		
		if(isset($label_date_from)){
			
			$sql = $sql." AND  label_cdate >= :label_date_from";
			$params[":label_date_from"] = $label_date_from;	
			
			
		}
		
		if(isset($label_date_to)){
			
			$sql = $sql." AND  label_cdate <= :label_date_to";
			$params[":label_date_to"] = $label_date_to;	
			
			
		}
		
		if(!$include_printed_disabled_labels){
			
			$sql = $sql." AND  label_print_disabled = 0";
			
			
		}
		
		/*if($include_only_prototypes){
			
			$sql = $sql." AND  label_is_prototype = 1";
		}*/
		


	   $sql.= " ORDER BY label_cdate DESC  LIMIT 1000";
		
		$result = $this->db->query($sql	,$params);

		$labels = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($labels as $label) {

        	array_push($data, $this->convertLabel($label));
        }

		return $data;
	}
	
    public function getLabel($LabelId){

    	$sql = $this->label_select_sql." WHERE label_id = :LabelId";
		
		$result = $this->db->query($sql,array(':LabelId' => $LabelId ));

		$label=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertLabel($label);
		

    }
    
    public function getLabelTrackingStatus($warehouse_id,$label_id,$label_tracking_process,$user_id,$tracking_mode="read"){
	
		
		$tracking_found = "Pending";
		
	    $sql = "SELECT `warehouse_id`, `label_id`, `label_tracking_process`,`label_tracking_datetime`, `user_id` from erp_label_tracking WHERE warehouse_id = :warehouse_id AND label_id = :label_id AND  label_tracking_process = :label_tracking_process AND label_tracking_released = 0";
	    
		$result = $this->db->query($sql
			,
			array(':warehouse_id' => $warehouse_id, ':label_id' => $label_id,':label_tracking_process' => $label_tracking_process)
		);

        
		$tracking = $result->fetch(PDO::FETCH_OBJ);
		
		
		if(!isset($tracking->label_id) )
		{
			
			if($tracking_mode == "write"){
			
			
			$sql = "
			
 			  	INSERT INTO `erp_label_tracking`( `warehouse_id`, `label_id`, `label_tracking_process`,	`label_tracking_datetime`, `user_id`,label_tracking_released	) VALUES (
 			  		:warehouse_id,:label_id,:label_tracking_process,Now(), :user_id	,0)";
 			  	
 			  	
 			  	//$user_id = getOwnerId();
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(':warehouse_id' => $warehouse_id, ':label_id' => $label_id, ':label_tracking_process' => $label_tracking_process,':user_id'=>$user_id));
			}
			
			$tracking_found = "New";
			
			
		}
		else {
			
			$tracking_found = "Processed";
			
		}
		
		return $tracking_found;
	}
	
	public function createLabelTrackingStatus($warehouse_id,$label_id,$label_tracking_process,$user_id){
		
		
			if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();
		
		
	
			date_default_timezone_set("America/Tijuana");
			$today_date = date("Y-m-d");
			
			$sql = "
			
 			  	INSERT INTO `erp_label_tracking`( `warehouse_id`, `label_id`, `label_tracking_process`,	`label_tracking_datetime`, `user_id`,label_tracking_released	) VALUES (
 			  		:warehouse_id,:label_id,:label_tracking_process,:today_date, :user_id,0	)";
 			  	
 			  	
 			  	//$user_id = getOwnerId();
				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(':warehouse_id' => $warehouse_id, ':label_id' => $label_id, ':label_tracking_process' => $label_tracking_process,':user_id'=>$user_id,':today_date' => $today_date));
			
			
			$tracking_found = "New";
			
		  if($this->db->conn->inTransaction()) $this->db->conn->commit();
		
		return $tracking_found;
	}
	
	
	public function getLabelTracking($warehouse_id,$label_id,$label_tracking_process,$user_id,$tracking_mode="read"){
	
		
		$tracking_found = "Pending";
		
	    $sql = "SELECT erp_label_tracking.`warehouse_id`,erp_warehouses.warehouse_name,erp_warehouses.warehouse_code, erp_label_tracking.`label_id`, `label_tracking_process`,`label_tracking_datetime`, erp_label_tracking.`user_id` as scanned_by,oauth_users.user_name as scanned_by_name, erp_labels.* FROM erp_label_tracking INNER JOIN erp_labels ON erp_labels.label_id = erp_label_tracking.label_id LEFT JOIN `oauth_users` ON ( `oauth_users`.`user_id` = `erp_label_tracking`.`user_id`) LEFT JOIN `erp_warehouses` ON (`erp_warehouses`.warehouse_id = erp_label_tracking.warehouse_id) ";// WHERE DATE(label_tracking_datetime) = DATE(NOW()) ";
	    
	    //$sql = $sql.  " AND warehouse_id = :warehouse_id AND label_id = :label_id AND  label_tracking_process = :label_tracking_process ";
	    
	    
		/*$result = $this->db->query($sql
			,
			array(':warehouse_id' => $warehouse_id, ':label_id' => $label_id,':label_tracking_process' => $label_tracking_process)
		);*/
		
		$result = $this->db->query($sql,array());

        
		$traking_data = $result->fetchAll(PDO::FETCH_OBJ);
		
		$data = [];
		
		
		foreach ($traking_data as $label) {
			
			$converted_label = $this->convertLabel($label);
			
			$converted_label["WarehouseId"] = $label->warehouse_id;
			$converted_label["WarehouseCode"] = $label->warehouse_code;
			$converted_label["WarehouseName"] = $label->warehouse_name;
			$converted_label["ScannedBy"] = $label->scanned_by;
			$converted_label["ScannedByName"] = $label->scanned_by_name;
			
			$converted_label["LabelTrackingProcess"] = $label->label_tracking_process;
			$converted_label["LabelTrackingDatetime"] = $label->label_tracking_datetime;
			
        	array_push($data, $converted_label);
        }
		
		
		return $data;
	}
	
	
	public function getLabelTrackingDetailed($warehouse_id,$label_batch,$label_id,$label_date_from,$label_date_to){
	
		$params = [];
		
		//$sql_and ='';
		
		
		if(isset($warehouse_id)){
			$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
			$sql_and  .= $sql_where.'  warehouse_id = :warehouse_id ';
			$params[":warehouse_id"] = $warehouse_id;
		}
		
		if(isset($label_id)){
			if($label_id != ""){
				
				$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
				$sql_and  .= $sql_where.'  erp_label_tracking.label_id = :label_id ';
				$params[":label_id"] = $label_id;
				
			}
		}
		
		if(isset($label_batch)){
			if($label_batch != ""){
				
				$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
				$sql_and  .= $sql_where.'  label_batch = :label_batch ';
				$params[":label_batch"] = $label_batch;
				
			}
		}
		
		if(isset($label_date_from)){
			$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
			$sql_and .= $sql_where.'  label_tracking_datetime > :label_date_from ';
			$params[":label_date_from"] = $label_date_from;
		}
		
		if(isset($label_date_to)){
			$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
			$sql_and  .= $sql_where.'  label_tracking_datetime < DATE_ADD(:label_date_to, INTERVAL 1 DAY)  ';
			$params[":label_date_to"] = $label_date_to;
		}
		
		
	     $sql = "SELECT erp_label_tracking.`warehouse_id`,erp_warehouses.warehouse_code,erp_warehouses.warehouse_name, erp_label_tracking.`label_id`, `label_tracking_process`,`label_tracking_released`,`label_tracking_datetime`, erp_label_tracking.`user_id` as scanned_by,oauth_users.user_name as scanned_by_name, erp_labels.* 
	    	FROM erp_label_tracking 
	    	INNER JOIN erp_labels ON erp_labels.label_id = erp_label_tracking.label_id 
	    	INNER JOIN `oauth_users` ON ( `oauth_users`.`user_id` = `erp_label_tracking`.`user_id`) 
	    	INNER JOIN `erp_warehouses` ON (`erp_warehouses`.warehouse_id = erp_label_tracking.warehouse_id) ";
	    
	        $sql = $sql. $sql_and.' ORDER BY label_tracking_datetime desc ';
	    //var_dump($params);
	    
		$result = $this->db->query($sql,$params);
		
		//$result = $this->db->query($sql,array());

        
		$traking_data = $result->fetchAll(PDO::FETCH_OBJ);
		
		$data = [];
		
		
		foreach ($traking_data as $label) {
			
			$converted_label = $this->convertLabel($label);
			
			$converted_label["WarehouseId"] = $label->warehouse_id;
			$converted_label["WarehouseName"] = $label->warehouse_name;
			$converted_label["WarehouseCode"] = $label->warehouse_code;
			$converted_label["ScannedBy"] = $label->scanned_by;
			$converted_label["ScannedByName"] = $label->scanned_by_name;
			$converted_label["LabelTrackingProcess"] = $label->label_tracking_process;
			$converted_label["LabelTrackingReleased"] = $label->label_tracking_released;
			$converted_label["LabelTrackingDatetime"] = $label->label_tracking_datetime;
			
        	array_push($data, $converted_label);
        }
		
		
		return $data;
	}
	
	public function getLabelTrackingAnomalies($warehouse_id,$label_batch,$label_id,$label_date_from,$label_date_to){
	
		$params = [];
		
		//$sql_and ='';
		
		
		if(isset($warehouse_id)){
			$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
			$sql_and  .= $sql_where.'  warehouse_id = :warehouse_id ';
			$params[":warehouse_id"] = $warehouse_id;
		}
		
		if(isset($label_id)){
			if($label_id != ""){
				
				$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
				$sql_and  .= $sql_where.'  erp_label_tracking.label_id = :label_id ';
				$params[":label_id"] = $label_id;
				
			}
		}
		
		if(isset($label_batch)){
			if($label_batch != ""){
				
				$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
				$sql_and  .= $sql_where.'  label_batch = :label_batch ';
				$params[":label_batch"] = $label_batch;
				
			}
		}
		
		if(isset($label_date_from)){
			$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
			$sql_and .= $sql_where.'  label_tracking_datetime > :label_date_from ';
			$params[":label_date_from"] = $label_date_from;
		}
		
		if(isset($label_date_to)){
			$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
			$sql_and  .= $sql_where.'  label_tracking_datetime < DATE_ADD(:label_date_to, INTERVAL 1 DAY)  ';
			$params[":label_date_to"] = $label_date_to;
		}
		
		
	
			$sql_where = !isset($sql_where)? ' WHERE ': 'AND' ;
			$sql_and  .= $sql_where."   erp_label_tracking.label_id in (
					SELECT labels.label_id FROM (
						SELECT e.label_id, COUNT( * ) no_entradas, IFNULL(s.no_salidas,0) as no_salidas
						FROM  `erp_label_tracking` e LEFT OUTER JOIN ( 
																	SELECT label_id, COUNT( * ) AS no_salidas
																	FROM  `erp_label_tracking` 
																	WHERE label_tracking_process =  'salida'
																	GROUP BY label_id, label_tracking_process
																)s ON s.label_id = e.label_id
						AND e.label_tracking_process =  'entrada'
						GROUP BY e.label_id, label_tracking_process, s.no_salidas
						HAVING ( no_entradas - IFNULL(s.no_salidas,0)  ) > 1
					) as labels
				
				)  " ;
		
	
		
	     $sql = "SELECT erp_label_tracking.`warehouse_id`,erp_warehouses.warehouse_code,erp_warehouses.warehouse_name, erp_label_tracking.`label_id`, `label_tracking_process`,`label_tracking_released`,`label_tracking_datetime`, erp_label_tracking.`user_id` as scanned_by,oauth_users.user_name as scanned_by_name, erp_labels.* 
	    	FROM erp_label_tracking 
	    	INNER JOIN erp_labels ON erp_labels.label_id = erp_label_tracking.label_id 
	    	INNER JOIN `oauth_users` ON ( `oauth_users`.`user_id` = `erp_label_tracking`.`user_id`) 
	    	INNER JOIN `erp_warehouses` ON (`erp_warehouses`.warehouse_id = erp_label_tracking.warehouse_id) 
	    	";
	    
	        $sql = $sql. $sql_and.' AND label_tracking_released = 0 ORDER BY label_tracking_datetime desc ';
	    //var_dump($params);
	    
		$result = $this->db->query($sql,$params);
		
		//$result = $this->db->query($sql,array());

        
		$traking_data = $result->fetchAll(PDO::FETCH_OBJ);
		
		$data = [];
		
		
		foreach ($traking_data as $label) {
			
			$converted_label = $this->convertLabel($label);
			
			$converted_label["WarehouseId"] = $label->warehouse_id;
			$converted_label["WarehouseName"] = $label->warehouse_name;
			$converted_label["WarehouseCode"] = $label->warehouse_code;
			$converted_label["ScannedBy"] = $label->scanned_by;
			$converted_label["ScannedByName"] = $label->scanned_by_name;
			$converted_label["LabelTrackingProcess"] = $label->label_tracking_process;
			$converted_label["LabelTrackingReleased"] = $label->label_tracking_released;
			$converted_label["LabelTrackingDatetime"] = $label->label_tracking_datetime;
			
        	array_push($data, $converted_label);
        }
		
		
		return $data;
	}
	
	public function resetLabelTrackingStatus($warehouse_id,$label_id,$label_tracking_process){
	
		if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();
		
		
	    $sql = "UPDATE erp_label_tracking SET label_tracking_released=1 WHERE warehouse_id = :WarehouseId AND  label_id = :label_id AND   label_tracking_process = :LabelTrackingProcess ";
	    
	    
	    $statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(":WarehouseId" => $warehouse_id,  ':label_id' => $label_id , ":LabelTrackingProcess" => $label_tracking_process));
		
		 if($this->db->conn->inTransaction()) $this->db->conn->commit();
				
		return array("Result" => "OK");
				
		
		
	}
	
	public function resetLabelTrackingStatusByWarehouse($warehouse_id,$label_id){
	
		
		if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();
		
	    $sql = "UPDATE erp_label_tracking SET label_tracking_released=1 WHERE warehouse_id = :WarehouseId and  label_id = :LabelId ";
	    
	    
	    $statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(":WarehouseId" => $warehouse_id,":LabelId" => $label_id));
				
		return array("Result" => "OK");
		
		 if($this->db->conn->inTransaction()) $this->db->conn->commit();
				
		
		
	}

    public function createLabel($label){

		//try{

 			$this->db->conn->beginTransaction();

 				$label->LabelId = $this->createLabelFolio();
 			  
 			  $sql = "
 			  	INSERT INTO `erp_labels`(
 			  		`label_id`, 
 			  		`label_batch`,
 			  		`label_customer_po`, 
 			  		`label_customer_item`, 
 			  		`label_box_size`, 
 			  		`label_box_weight`, 
 			  		`label_part_number`, 
 			  		`label_job_no`, 
 			  		`label_rack_id`, 
 			  		`label_number`, 
 			  		`label_total`, 
 			  		`product_id`, 
 					`label_per_ctn`,
 			  		`label_cdate`, 
 			  		`user_id`,
 			  		`label_is_prototype`,
 			  		`label_reference`,
 			  		`label_header`,
 			  		`bundle_type_id`,
 			  		`label_customer_item_title`
 			  		
 			  	
 			  	) VALUES (
 			  		:LabelId,
 			  		:LabelBatch,
 			  		:LabelCustomerPO,
 			  		:LabelCustomerItem, 
 			  		:LabelBoxSize, 
 			  		:LabelBoxWeight, 
 			  		:LabelPartNumber, 
 			  		:LabelJobNo, 
 			  		:LabelRackId, 
 			  		:LabelNumber,
 			  		:LabelTotal,
 			  		:ProductId,
 			  		:LabelPerCtn,
 			  		Now(),
 			  		:UserId,
 			  		:LabelIsPrototype,
 			  		:LabelReference,
 			  		:LabelHeader,
 			  		:BundleTypeId,
 			  		:LabelCustomerItemTitle
 			  	)";

	

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($label));
		
				//(int)$this->db->conn->lastInsertId();
				//$inbound_order->InboundOrderFolio = $inbound_order->InboundOrderId ;


				$this->db->conn->commit();

                /*
				if(isset($label->ProductId)){
					$ProductModel = new ProductModel();
					$label->Product = $ProductModel->getProduct($label->ProductId);
					$label->ItemId = $label->Product["ProductId"];
					$label->ItemDescription = $label->Product["ProductDescription"];
				}
				
				
				if(isset($label->LabelBatch) ){
				
				    $batch_label = $this->getLabelHeader($label->LabelBatch);
				    
				    if(isset($batch_label["LabelTotal"])){
				    
				        $label->LabelTotal = $batch_label["LabelTotal"];
				        
				    }
				    
				    
				}
				*/

				return $label;

		//}catch(Exception $ex){
		//	echo $ex;
			//$this->db->conn->rollback();
		//}
	}
	
	public function updateLabel($label){


		try{

 				if(!$this->db->conn->inTransaction()) $this->db->conn->beginTransaction();

 	



 				$sql = "
 					UPDATE `erp_labels` SET  	
 						`label_batch` = :LabelBatch,
 						`label_customer_po`= :LabelCustomerPO,
 						`label_customer_item` = :LabelCustomerItem, 
	 			  		`label_box_size` = :LabelBoxSize, 
	 			  		`label_box_weight` = :LabelBoxWeight, 
	 			  		`label_part_number` = :LabelPartNumber, 
	 			  		`label_job_no` = :LabelJobNo, 
	 			  		`label_rack_id = :LabelRackId, 
 						`label_number` =:LabelNumber,
 						`label_total`  =:LabelTotal,					
 						`product_id`=:ProductId,
 						`label_per_ctn` = :LabelPerCtn,
 						`user_id`=:UserId,
 						`label_is_prototype` = :LabelIsPrototype,
 						`label_reference` = :LabelReference,
 						`label_header` = :LabelHeader,
 						`bundle_type_id` = :BundleTypeId,
 						`label_customer_item_title` = :LabelCustomerItemTitle
 						
 					WHERE `label_id`=:LabelId ";
				

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($label));
		
			

				

				

				$this->db->conn->commit();

				return $label;

		}catch(Exception $ex){
			echo $ex;
			$this->db->conn->rollback();
		}


	}

	public function deleteLabel($label){




 				$sql = "
 					UPDATE `erp_labels` SET  	
 						`label_deleted` = 1 
 					WHERE `label_id`=:LabelId ";
				

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute(array(":LabelId" => $label->LabelId));
				
				$label->LabelDeleted = 1;


				return $label;



	}
	
	public function enableDisableLabel($label){



    	$label->LabelPrintDisabled = !$label->LabelPrintDisabled;
    	
 		$sql = "
 			UPDATE `erp_labels` SET  `label_print_disabled` = :LabelPrintDisabled
 			WHERE `label_id`=:LabelId ";
 			
 		
 			
 		$params = array(":LabelId" => $label->LabelId, ":LabelPrintDisabled" => $label->LabelPrintDisabled);
 		
 					
 		if( $label->DisableByBatch == true && isset($label->LabelBatch)){
 			
 			$sql = $sql. " OR `label_batch` = :LabelBatch";
 			$params[":LabelBatch"] = $label->LabelBatch;
 			
 		}
				
        //var_dump($params);
        
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute($params);;
		
		//$label->LabelPrintDisabled = 1;


		return $label;



	}

	private function createLabelFolio(){

		$FolioModel = new FolioModel();

		$FolioNumber = $FolioModel->getLastFolioByCode('LABEL');
		$LabelFolio = str_pad($FolioNumber, 8, "0", STR_PAD_LEFT);

		return $LabelFolio;
	}
	
	private function createBundleLabelFolio($prefix=""){

		$FolioModel = new FolioModel();

		$FolioNumber = $FolioModel->getLastFolioByCode('BUNDLE_LABEL');
		$LabelFolio = str_pad($FolioNumber, 8, "0", STR_PAD_LEFT);
		
		if(isset($prefix) && $prefix != ""){
			
			$LabelFolio = $prefix."-".$LabelFolio;
			
		}

		return $LabelFolio;
	}
	
	public function getBundleLabels($BundleId){

		$sql = " SELECT `erp_labels`.`label_id`, `label_batch`, `label_customer_po`, `label_customer_item`, `label_box_size`, `label_box_weight`, `label_part_number`, `label_job_no`, `label_rack_id`, `label_number`, `label_total`, `product_id`, `inbound_order_id`, `inbound_order_detail_id`, `outbound_order_id`, `outbound_order_detail_id`, `label_cdate`, `user_id`, `label_per_ctn`, `label_is_prototype`, `label_reference`, `label_header`, `label_deleted`, `bundle_type_id`, `label_customer_item_title`
				 FROM  `erp_labels` ,  `erp_bundle_labels` 
				 WHERE  `erp_labels`.`label_id` =  `erp_bundle_labels`.`label_id` AND `erp_bundle_labels`.`bundle_id` = :BundleId
		";
		
		$result = $this->db->query($sql
			,
			array(":BundleId"=>$BundleId)
		);

		$labels = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($labels as $label) {

        	array_push($data, $this->convertLabel($label));
        }

		return $data;
	}
	
	public function getBundleLabelsCount($BundleId){

		$sql = " SELECT count(*) as `total`
				 FROM  `erp_labels` ,  `erp_bundle_labels` 
				 WHERE  `erp_labels`.`label_id` =  `erp_bundle_labels`.`label_id` AND `erp_bundle_labels`.`bundle_id` = :BundleId
		";
		
		$result = $this->db->query($sql
			,
			array(":BundleId"=>$BundleId)
		);

		$labels = $result->fetch(PDO::FETCH_OBJ);
        
		return $labels->total;
	}
	
	public function createBundleLabel($bundle_label,$labels){
		
		$BundleModel = new BundleModel();
		
		$bundle_type = $BundleModel->getBundleType($bundle_label->BundleTypeId);
		
		$bundle_type_label_prefix = "";
		
		if(isset($bundle_type->bundle_type_label_prefix)) {
			
			$bundle_type_label_prefix = $bundle_type->bundle_type_label_prefix;
			
		}
		
		if(count($labels) > 0){
			
			$first_label = (object) $this->getLabel($labels[0]->LabelId);
			
			$bundle_label->CustomerPO = $first_label->CustomerPO;
			$bundle_label->CustomerItem = $first_label->CustomerItem;
			$bundle_label->LabelCustomerPO = $first_label->LabelCustomerPO;
			$bundle_label->LabelCustomerItem = $first_label->LabelCustomerItem;
			$bundle_label->LabelBoxSize = $first_label->LabelBoxSize;
			$bundle_label->LabelBoxWeight= $first_label->LabelBoxWeight;
			$bundle_label->LabelPartNumber = $first_label->LabelPartNumber;
			$bundle_label->LabelJobNo = $first_label->LabelJobNo;
			$bundle_label->LabelRackId = $first_label->LabelRackId;
			//$bundle_label->LabelNumber = $first_label.LabelNumber;
			//$bundle_label->LabelTotal = $first_label.LabelTotal;
			$bundle_label->LabelBatch = $first_label->LabelBatch;
			$bundle_label->ProductId = $first_label->ProductId;
			$bundle_label->LabelPerCtn = $first_label->LabelPerCtn;
			$bundle_label->LabelReference = $first_label->LabelReference;
			$bundle_label->LabelsOnPallet = count($labels);
			
			$bundle_label->CustomerName = $first_label->Product["CustomerName"];
			$bundle_label->ItemDescription = $first_label->Product["ProductDescription"];
			
			$bundle_label->LabelCustomerItemTitle = $first_label->LabelCustomerItemTitle;
			
		//	$bundle_label->CustomerName = $first_label->product->CustomerName;
			
			
		
		}
		
		
		$this->db->conn->beginTransaction();

 				$bundle_label->LabelId = $this->createBundleLabelFolio($bundle_type_label_prefix);
 			  
 			  $sql = "
 			  	INSERT INTO `erp_labels`(
 			  		`label_id`, 
 			  		`label_batch`,
 			  		`label_customer_po`, 
 			  		`label_customer_item`, 
 			  		`label_box_size`, 
 			  		`label_box_weight`, 
 			  		`label_part_number`, 
 			  		`label_job_no`, 
 			  		`label_rack_id`, 
 			  		`label_number`, 
 			  		`label_total`, 
 			  		`product_id`, 
 					`label_per_ctn`,
 			  		`label_cdate`, 
 			  		`user_id`,
 			  		`label_is_prototype`,
 			  		`label_reference`,
 			  		`label_header`,
 			  		`bundle_type_id`,
 			  		`label_customer_item_title`
 			  		
 			  	
 			  	) VALUES (
 			  		:LabelId,
 			  		:LabelBatch,
 			  		:LabelCustomerPO,
 			  		:LabelCustomerItem, 
 			  		:LabelBoxSize, 
 			  		:LabelBoxWeight, 
 			  		:LabelPartNumber, 
 			  		:LabelJobNo, 
 			  		:LabelRackId, 
 			  		:LabelNumber,
 			  		:LabelTotal,
 			  		:ProductId,
 			  		:LabelPerCtn,
 			  		Now(),
 			  		:UserId,
 			  		:LabelIsPrototype,
 			  		:LabelReference,
 			  		:LabelHeader,
 			  		:BundleTypeId,
 			  		:LabelCustomerItemTitle
 			  		
 			  	
 			  	)";

	

				$statement = $this->db->conn->prepare($sql);
				$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($bundle_label));
		
				//(int)$this->db->conn->lastInsertId();
				//$inbound_order->InboundOrderFolio = $inbound_order->InboundOrderId ;

                //CreateRelationBetweenBundleAndLabels
                
                
                $sql = " INSERT INTO `erp_bundle_labels`( `bundle_id`, `label_id` ) VALUES ( :BundleId, :LabelId )";
                
                $bundle_label->labels = [];
                
                foreach ($labels as $label) {

        			
        			
        			$statement = $this->db->conn->prepare($sql);
					$statement->setFetchMode(PDO::FETCH_OBJ);
					$statement->execute(array(":BundleId"=>$bundle_label->LabelId,":LabelId"=>$label->LabelId));
					
					$label->BundleLabelId = $bundle_label->LabelId;
					
					array_push($bundle_label->labels, $label);
					
        		}
        
        
				
				
		
                
                
				$this->db->conn->commit();

                /*
				if(isset($label->ProductId)){
					$ProductModel = new ProductModel();
					$label->Product = $ProductModel->getProduct($label->ProductId);
					$label->ItemId = $label->Product["ProductId"];
					$label->ItemDescription = $label->Product["ProductDescription"];
				}
				
				
				if(isset($label->LabelBatch) ){
				
				    $batch_label = $this->getLabelHeader($label->LabelBatch);
				    
				    if(isset($batch_label["LabelTotal"])){
				    
				        $label->LabelTotal = $batch_label["LabelTotal"];
				        
				    }
				    
				    
				}
				*/

				return $bundle_label;

		//}catch(Exception $ex){
		//	echo $ex;
			//$this->db->conn->rollback();
		//}
		
		
	}

	private function fillParams($label){

		$params = [];
		$params[':LabelId']					= (isset($label->LabelId) ? $label->LabelId : NULL ) ;
		$params[':LabelBatch']				= (isset($label->LabelBatch) ? $label->LabelBatch : NULL ) ;
		$params[':LabelCustomerPO']			= (isset($label->LabelCustomerPO) ? $label->LabelCustomerPO : NULL ) ;
		$params[':LabelCustomerItem']		= (isset($label->LabelCustomerItem) ? $label->LabelCustomerItem : NULL ) ;
		$params[':LabelBoxSize']			= (isset($label->LabelBoxSize) ? $label->LabelBoxSize : NULL ) ;
		$params[':LabelBoxWeight']			= (isset($label->LabelBoxWeight) ? $label->LabelBoxWeight : NULL ) ;
		$params[':LabelPartNumber']			= (isset($label->LabelPartNumber) ? $label->LabelPartNumber : NULL ) ;
		$params[':LabelJobNo']				= (isset($label->LabelJobNo) ? $label->LabelJobNo : NULL ) ;
		$params[':LabelRackId']				= (isset($label->LabelRackId) ? $label->LabelRackId : NULL ) ;		
		$params[':LabelNumber']				= (isset($label->LabelNumber) ? $label->LabelNumber : NULL ) ;
		$params[':LabelTotal']				= (isset($label->LabelTotal) ? $label->LabelTotal : NULL ) ;
		$params[':ProductId']				= (isset($label->ProductId) ? $label->ProductId : NULL ) ;
		$params[':LabelPerCtn']				= (isset($label->LabelPerCtn) ? $label->LabelPerCtn : NULL ) ;
		$params[':LabelIsPrototype']	    = (isset($label->LabelIsPrototype) ? $label->LabelIsPrototype : NULL ) ;
		$params[':LabelReference']	        = (isset($label->LabelReference) ? $label->LabelReference : NULL ) ;
		$params[':LabelHeader']	            = (isset($label->LabelHeader) ? $label->LabelHeader : NULL ) ;
		$params[':BundleTypeId']	        = (isset($label->BundleTypeId) ? $label->BundleTypeId : NULL ) ;
		$params[':UserId']					= getOwnerId();
		$params[':LabelCustomerItemTitle']	= (isset($label->LabelCustomerItemTitle) ? $label->LabelCustomerItemTitle : NULL ) ;
		//$params[':UserId']					= 1;

		return $params;
		
	}

	private function convertLabel($label,$detailed=false,$bundle_id=NULL){

		$data = array(
			"LabelId" => $label->label_id , 
			"LabelBatch" => $label->label_batch,
			"LabelCustomerPO" => $label->label_customer_po,
			"CustomerPO" => $label->label_customer_po,
			"LabelCustomerItem"=> $label->label_customer_item,
			"LabelBoxSize"=> $label->label_box_size,
			"LabelBoxWeight"=> $label->label_box_weight,
			"LabelPartNumber"=> $label->label_part_number,
			"LabelJobNo"=> $label->label_job_no,
			"LabelRackId"=> $label->label_rack_id,
			"LabelNumber" => $label->label_number,
			"LabelTotal" => $label->label_total,
			"ProductId" => $label->product_id,
			"LabelPerCtn" => (double)$label->label_per_ctn,
			"UserId" => $label->user_id,
			"UserName" => $label->user_name,
			"LabelIsPrototype" => $label->label_is_prototype,
			"LabelReference" => $label->label_reference,
			"LabelHeader" => $label->label_header,
			"LabelDeleted" => $label->label_deleted,
			"BundleTypeId" => $label->bundle_type_id,
			"LabelPrintDisabled"=> (bool)$label->label_print_disabled,
			"LabelCDate"=> $label->label_cdate,
			"LabelCustomerItemTitle"=> $label->label_customer_item_title,
		);

		if(isset($label->product_id)){
			
			$ProductModel = new ProductModel();
			$data["Product"] = $ProductModel->getProduct($label->product_id);
			$data["ItemId"] = $data["Product"]["ProductId"];
			$data["CustomerId"] = $data["Product"]["CustomerId"];
			$data["CustomerName"] = $data["Product"]["CustomerName"];
			$data["ItemDescription"] = $data["Product"]["ProductDescription"];



		}
		
		if(isset($label->bundle_type_id) && $label->bundle_type_id > 0){
			
			$data["LabelsOnPallet"] = $this->getBundleLabelsCount($label->label_id);
		}
		
		if(isset($bundle_id)){
			
			$data["BundleId"] = $bundle_id;
			
		}
		
		if($detailed){
			//Revisar si la etiqueta ya esta en la orden
		}
		
		

		return $data;
	}
}
?>