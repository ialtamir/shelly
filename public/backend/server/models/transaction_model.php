<?php
class TransactionModel{

	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function updateTransactionReference(){

		return ;

		$sql_quotes = " SELECT  `quote_id` ,  `quote_folio` ,  `quote_title` ,  `transaction_id` FROM erp_quotes WHERE transaction_id IS NULL OR transaction_id =  '' ";
		$sql_update_quotes = " UPDATE erp_quotes SET transaction_id = :TransactionId where quote_id = :QuoteId ";


		$result = $this->db->query($sql_quotes,array());

		$quotes = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($quotes as $quote) {

        	

			echo 'Quote:'.$quote->quote_id.'  TransactionId:'.uniqid();

        	$statement = $this->db->conn->prepare($sql_update_quotes);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute(array(':QuoteId' => $quote->quote_id,":TransactionId" => uniqid()));

			//$sql_sale_orders = "SELECT sale_order_id,quote_id FROM erp_sale_orders WHERE quote_id=:QuoteId";
            $sql_update_sale_orders  = " UPDATE erp_sale_orders INNER JOIN erp_quotes ON erp_sale_orders.quote_id = erp_quotes.quote_id SET erp_sale_orders.transaction_id = erp_quotes.transaction_id  where erp_sale_orders.quote_id = :QuoteId";


            $statement = $this->db->conn->prepare($sql_update_sale_orders);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute(array(':QuoteId' => $quote->quote_id));


			//$sql_sale_orders = "SELECT sale_order_id,quote_id FROM erp_sale_orders WHERE quote_id=:QuoteId";
            $sql_update_work_orders  = " UPDATE erp_work_orders INNER JOIN erp_sale_orders ON erp_work_orders.sale_order_id = erp_sale_orders.sale_order_id SET erp_work_orders.transaction_id = erp_sale_orders.transaction_id";


            $statement = $this->db->conn->prepare($sql_update_work_orders);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute(array());



        }


        $sql_sale_orders = " SELECT  `sale_order_id` , `transaction_id` FROM erp_sale_orders WHERE transaction_id IS NULL OR transaction_id =  '' ";

        $result = $this->db->query($sql_sale_orders,array());

		$sale_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        foreach ($sale_orders as $sale_order) {


        	echo 'SaleOrder:'.$sale_order->sale_order_id.'  TransactionId:'.uniqid();

        	$statement = $this->db->conn->prepare(" UPDATE erp_sale_orders SET transaction_id = :TransactionId where sale_order_id = :SaleOrderId");
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute(array(':SaleOrderId' => $sale_order->sale_order_id,":TransactionId" => uniqid()));

			//$sql_sale_orders = "SELECT sale_order_id,quote_id FROM erp_sale_orders WHERE quote_id=:QuoteId";
            $sql_update_work_orders  = " UPDATE erp_work_orders INNER JOIN erp_sale_orders ON erp_work_orders.sale_order_id = erp_sale_orders.sale_order_id SET erp_work_orders.transaction_id = erp_sale_orders.transaction_id";


            $statement = $this->db->conn->prepare($sql_update_work_orders);
			$statement->setFetchMode(PDO::FETCH_OBJ);
			$statement->execute(array());




        }


	}

	public function getTransactions($TransactionId){

		$QuoteModel = new QuoteModel();
		$quote = $QuoteModel->getQuoteByTransactionId($TransactionId);

		$SaleOrderModel = new SaleOrderModel();
		$sale_order = $SaleOrderModel->getSaleOrderByTransactionId($TransactionId);


		$SaleOrderPayments = $SaleOrderModel->getSaleOrderPayments($sale_order["SaleOrderId"]);

		$WorkOrderModel = new WorkOrderModel();
		$work_order = $WorkOrderModel->getWorkOrderByTransactionId($TransactionId);

		return array('Quote' => $quote, 'SaleOrder' => $sale_order,'SaleOrderPayments' => $SaleOrderPayments, 'WorkOrder' => $work_order);
	}
}
?>