<?php
class FolioModel {

	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getFolios(){

		$result = $this->db->query(" 
			SELECT 
				`folio_id`,
				`folio_serie`,
				`folio_code`
			FROM `erp_folios`",
			array()
		);

		$folios = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($folios as $folio) {

        	array_push($data, $this->convertFolio($folio));
        }

		return $data;

	}

	public function getFolio($FolioId,$FolioSerie=''){
		$result = $this->db->query(" 
			SELECT 
				`folio_id`, 
				`folio_serie`,
				`folio_code`
			FROM `erp_folios`
			WHERE `folio_id` = :FolioId
			AND `folio_serie` = :FolioSerie",
			array( ':FolioId' => $FolioId , ':FolioSerie' => $FolioSerie )
		);


		$folio=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertFolio($folio);

	}

	public function getLastFolioByCode($FolioCode,$FolioSerie='',$AutoIncrement=true){

		$folio_number = 1;


		$sql = " 
			SELECT MAX(`folio_number`)+1 as `folio_number` 
			FROM `erp_folios`
			WHERE `folio_code` = :FolioCode 
			AND `folio_serie` = :FolioSerie
		";


		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array( ':FolioCode' => $FolioCode ,':FolioSerie' => $FolioSerie ));


 		$result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->folio_number)){
		 	
		 	$folio_number = $result->folio_number;
		 	
		 }
		

		if($AutoIncrement){
			if($folio_number == 1){
				
       			$folio_id = $this->createFolio($FolioCode,$FolioSerie,$folio_number);
	       	}
	       	else{
	       		
	       		$this->updateFolio($FolioCode,$FolioSerie,$folio_number);
	       	}
		}
       	
       		

       //$folio = array('folio_code' => $folio_code, 'folio_number' => $folio_number );

		return $folio_number;

	}

	public function createFolio($folio_code,$folio_serie='',$folio_number) {

		$this->db->query("
			INSERT INTO `erp_folios`(
				`folio_code`,
				`folio_serie`,
				`folio_number`
			)
			VALUES (
				
				:FolioCode,
				:FolioSerie,
				:FolioNumber
			)",
			array(':FolioCode' => $folio_code,':FolioSerie' => $folio_serie,':FolioNumber' => $folio_number)
		);

		$folio_id = $this->db->getInsertId();

		return $folio_id;
	}

	public function updateFolio($folio_code,$folio_serie='',$folio_number) {

		$this->db->query("
			UPDATE `erp_folios` SET 
				`folio_number`=:FolioNumber
			WHERE `folio_code`=:FolioCode
			AND `folio_serie`=:FolioSerie
			",
			array(':FolioNumber' => $folio_number ,':FolioSerie' => $folio_serie , ':FolioCode' => $folio_code)
		);
	}

	public function deleteFolio($FolioId){
		$this->db->query("
			DELETE FROM `erp_folios` 
			WHERE `folio_id`=:FolioId",
			array(':FolioId' => $FolioId)
		);

		
	}

}