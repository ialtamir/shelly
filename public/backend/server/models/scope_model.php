<?php
class ScopeModel implements League\OAuth2\Server\Storage\ScopeInterface {

	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}


	 /**
     * Return information about a scope
     *
     * Example SQL query:
     *
     * <code>
     * SELECT * FROM oauth_scopes WHERE scope = :scope
     * </code>
     *
     * Response:
     *
     * <code>
     * Array
     * (
     *     [id] => (int) The scope's ID
     *     [scope] => (string) The scope itself
     *     [name] => (string) The scope's name
     *     [description] => (string) The scope's description
     * )
     * </code>
     *
     * @param  string     $scope     The scope
     * @param  string     $clientId  The client ID (default = "null")
     * @param  string     $grantType The grant type used in the request (default = "null")
     * @return bool|array If the scope doesn't exist return false
     */

    public function getScope($scope, $clientId = null, $grantType = null){
     $result = $this->db->query('SELECT * FROM oauth_scopes WHERE scope = :scope',array(':scope'=>$scope));

        $row = $result->fetch();
        
        if($row){
        	return array(
        		'id' => $row->id,
        		'scope' => $row->scope,
        		'name' => $row->name,
        		'description' => $row->description
			);	
        }else{
        	return false;
        }
		
	}



    public function getScopeById($scopeId, $clientId = null, $grantType = null){
     $result = $this->db->query('SELECT * FROM oauth_scopes WHERE id = :scopeid',array(':scopeid'=>$scopeId));

        $row = $result->fetch();
        
        if($row){
            return array(
                'id' => $row->id,
                'scope' => $row->scope,
                'name' => $row->name,
                'description' => $row->description
            );  
        }else{
            return false;
        }
        
    }



    public function getScopes($scopeId=null, $clientId = null, $grantType = null){
     $result = $this->db->query('SELECT * FROM oauth_scopes',array());

     $scopes = $result->fetchAll(PDO::FETCH_OBJ);
      
      return $scopes;
        
       
        
    }

    

   


}
?>