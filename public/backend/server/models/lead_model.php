<?php
class LeadModel {
	
	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function searchLeads($query){

		$result = $this->db->query("
			SELECT 
				`lead_id`,
				`lead_name`, 
				`lead_address`, 
				`lead_city`, 
				`lead_country`, 
				`lead_telephone`, 
				`lead_cellphone`, 
				`lead_email`,
				`lead_notes`,
				`lead_date`,
				`lead_title`
			FROM `erp_leads`
			WHERE lead_name like :LeadName  "
			,
			array(':LeadName' => $query.'%')
		);

		$leads = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($leads as $lead) {

        	array_push($data, $this->convertLead($lead));
        }

		return $data;

	}

	public function getLeads(){

		$result = $this->db->query("
			SELECT 
				`lead_id`,
				`lead_name`, 
				`lead_address`, 
				`lead_city`, 
				`lead_country`, 
				`lead_telephone`, 
				`lead_cellphone`, 
				`lead_email`,
				`lead_notes`,
				`lead_date`,
				`lead_title`
			FROM `erp_leads`
			"
			,
			array()
		);

		$leads = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($leads as $lead) {

        	array_push($data, $this->convertLead($lead));
        }

		return $data;

	}

	public function getLead($LeadId){

		$result = $this->db->query("
			SELECT 
				`lead_id`,
				`lead_name`, 
				`lead_address`, 
				`lead_city`, 
				`lead_country`, 
				`lead_telephone`, 
				`lead_cellphone`, 
				`lead_email`,
				`lead_notes`,
				`lead_date`,
				`lead_title`
			FROM `erp_leads`
			WHERE `lead_id` = :LeadId"
			,
			array( ':LeadId' => $LeadId )
		);

		$lead=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertLead($lead);

	}

	public function createLead($lead){


		$this->db->query("
			INSERT INTO `erp_leads` (
				`lead_id`,
				`lead_name`, 
				`lead_address`, 
				`lead_city`, 
				`lead_country`, 
				`lead_telephone`, 
				`lead_cellphone`, 
				`lead_email`,
				`lead_notes`,
				`lead_date`,
				`lead_title`,
				`user_id`
			) 
			VALUES ( 
				:LeadId ,
				:LeadName, 
				:LeadAddress, 
				:LeadCity, 
				:LeadCountry, 
				:LeadTelephone,
				:LeadCellphone, 
				:LeadEmail,
				:LeadNotes,
				:LeadDate,
				:LeadTitle,
				:UserId
			)",
			$this->fillParams($lead)
		);	

		$lead->LeadId = $this->db->getInsertId();

		return $lead;	
	}

	public function updateLead($lead){
		
		$this->db->query("
			UPDATE `erp_leads` SET 
				`lead_name`=:LeadName,
				`lead_address`=:LeadAddress,
				`lead_city`=:LeadCity,
				`lead_country`=:LeadCountry,
				`lead_telephone`=:LeadTelephone,
				`lead_cellphone`=:LeadCellphone,
				`lead_email`=:LeadEmail,
				`lead_notes`=:LeadNotes,
				`lead_date`=:LeadDate,
				`lead_title`=:LeadTitle,
				`user_id` = :UserId
		 WHERE `lead_id` = :LeadId",
		   $this->fillParams($lead)
		 );
		
	}

	public function deleteLead($LeadId){
		
		$this->db->query("
			DELETE FROM `erp_leads` 
			WHERE `lead_id` = :LeadId",
		array(':LeadId' => $LeadId ));
		
	}

	

	public function convertLeadToCustomer($lead,$LeadId){
		$this->db->query("
			INSERT INTO `erp_customers` (
				`customer_id`,
				`customer_name`, 
				`customer_address`, 
			    `customer_city`, 
				`customer_country`, 
				`customer_telephone`, 
				`customer_cellphone`, 
				`customer_email`, 
				`customer_notes`
			) 
            VALUES ( 
            	0,
            	:LeadName, 
                :LeadAddress,
                :LeadCity,
				:LeadCountry,
				:LeadTelephone,
				:LeadCellphone,
				:LeadEmail,
				:LeadNotes
			)",
		   $this->fillParamsCustomer($lead)
		 );

		$this->db->query("
			DELETE FROM `erp_leads` 
			WHERE `lead_id` = :LeadId",
		array(':LeadId' => $LeadId ));
	

	}




	private function fillParams($lead) {

		$params = [];

		if(isset($lead->LeadId))	 { $params[':LeadId'] = $lead->LeadId; } else { $params[':LeadId'] = NULL; }; 
		if(isset($lead->LeadName))	 { $params[':LeadName'] = $lead->LeadName; } else { $params[':LeadName'] = NULL; }; 
		if(isset($lead->LeadAddress)){ $params[':LeadAddress'] = $lead->LeadAddress; } else { $params[':LeadAddress'] = NULL; };  
		if(isset($lead->LeadCity))   { $params[':LeadCity'] = $lead->LeadCity; } else { $params[':LeadCity'] = NULL; }; 
		if(isset($lead->LeadCountry)){ $params[':LeadCountry'] = $lead->LeadCountry; } else { $params[':LeadCountry'] = NULL; }; 
		if(isset($lead->LeadTelephone)){ $params[':LeadTelephone'] = $lead->LeadTelephone; } else { $params[':LeadTelephone'] = NULL; }; 
		if(isset($lead->LeadCellphone)){ $params[':LeadCellphone'] = $lead->LeadCellphone; } else { $params[':LeadCellphone'] = NULL; };  
		if(isset($lead->LeadEmail))  { $params[':LeadEmail'] = $lead->LeadEmail; } else { $params[':LeadEmail']  = NULL; }; 
		if(isset($lead->LeadNotes))  { $params[':LeadNotes'] = $lead->LeadNotes; } else {  $params[':LeadNotes'] = NULL; }; 
		if(isset($lead->LeadDate))  { $params[':LeadDate'] = $lead->LeadDate; } else {  $params[':LeadDate'] = NULL; }; 
		if(isset($lead->LeadTitle))  { $params[':LeadTitle'] = $lead->LeadTitle; } else {  $params[':LeadTitle'] = NULL; }; 
		$params[':UserId']  = getOwnerId();

		return $params;
			
	}


	private function fillParamsCustomer($lead) {

		$params = [];

		if(isset($lead->LeadName))	 { $params[':LeadName'] = $lead->LeadName; } else { $params[':LeadName'] = NULL; }; 
		if(isset($lead->LeadAddress)){ $params[':LeadAddress'] = $lead->LeadAddress; } else { $params[':LeadAddress'] = NULL; }; 
		if(isset($lead->LeadCity))   { $params[':LeadCity'] = $lead->LeadCity; } else { $params[':LeadCity'] = NULL; }; 
		if(isset($lead->LeadCountry)){ $params[':LeadCountry'] = $lead->LeadCountry; } else { $params[':LeadCountry'] = NULL; }; 
		if(isset($lead->LeadTelephone)){ $params[':LeadTelephone'] = $lead->LeadTelephone; } else { $params[':LeadTelephone'] = NULL; }; 
		if(isset($lead->LeadCellphone)){ $params[':LeadCellphone'] = $lead->LeadCellphone; } else { $params[':LeadCellphone'] = NULL; };  
		if(isset($lead->LeadEmail))  { $params[':LeadEmail'] = $lead->LeadEmail; } else { $params[':LeadEmail']  = NULL; }; 
		if(isset($lead->LeadNotes))  { $params[':LeadNotes'] = $lead->LeadNotes; } else {  $params[':LeadNotes'] = NULL; };
		return $params;
			
	}





	


	private function convertLead($lead)
	{
		return array(
			'LeadId' => $lead->lead_id,
			'LeadName' => $lead->lead_name,
    		'LeadAddress' => $lead->lead_address, 
			'LeadCity' => $lead->lead_city, 
			'LeadCountry' => $lead->lead_country, 
	 		'LeadTelephone' => $lead->lead_telephone, 
	 		'LeadCellphone' => $lead->lead_cellphone, 
	 		'LeadEmail' => $lead->lead_email,
	 		'LeadNotes' => $lead->lead_notes,
	 		'LeadDate' => $lead->lead_date,
	 		'LeadTitle' => $lead->lead_title
	 		
	 	);
	}


	
}
?>