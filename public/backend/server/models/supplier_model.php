<?php
class SupplierModel {

	private $db;
    private $supplier_select_sql = "SELECT `supplier_id`, `supplier_name`, `supplier_rfc`, `supplier_telephone`, `supplier_email`, `supplier_address`, `supplier_cp`, `supplier_city`, `supplier_country`, `supplier_notes`,`supplier_web` FROM `erp_suppliers`";

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}
	
	
	public function searchSuppliers($query){

        $sql = $this->supplier_select_sql;
        $parameters = array();
        
        if(isset($query)){
            
            $sql = $sql." WHERE supplier_name like :SupplierName";
            $parameters[":SupplierName"] = $query."%";
            
        }

        
        
		$result = $this->db->query($sql,$parameters);
			  

		$suppliers = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($suppliers as $supplier) {

        	array_push($data, $this->convertSupplier($supplier));
        }

		return $data;

	}

	
	

	public function getSuppliers(){

        $sql = $this->supplier_select_sql;
        
		$result = $this->db->query($sql,array());


		$suppliers = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($suppliers as $supplier) {

        	array_push($data, $this->convertSupplier($supplier));
        }

		return $data;
	}
	
	

	public function getSupplier($SupplierId){
        
        $sql = $this->supplier_select_sql." WHERE `supplier_id` = :SupplierId";
        $parameters[":SupplierId"] = $SupplierId;
        
		$result = $this->db->query($sql,$parameters);

		$supplier=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertSupplier($supplier);

	}
	
	
	
	public function createSupplier($supplier){


		$this->db->query("
			INSERT INTO `erp_suppliers`(
				`supplier_id`, 
				`supplier_name`, 
				`supplier_rfc`, 
				`supplier_telephone`, 
				`supplier_email`, 
				`supplier_address`, 
				`supplier_cp`, 
				`supplier_city`, 
				`supplier_country`, 
				`supplier_notes`,
                `supplier_web`
				) 
			VALUES (
				:SupplierId,
				:SupplierName,
				:SupplierRFC,
				:SupplierTelephone,
				:SupplierEmail,
				:SupplierAddress,
				:SupplierCP,
				:SupplierCity,
				:SupplierCountry,
				:SupplierNotes,
				:SupplierWeb
			)",
			$this->fillParams($supplier)
		);	

		$supplier->SupplierId = $this->db->getInsertId();

		return $supplier;	
	}
	
	
	public function updateSupplier($supplier){
		
		$this->db->query("
			UPDATE `erp_suppliers` SET 
			`supplier_name`=:SupplierName,
			`supplier_rfc`=:SupplierRFC,
			`supplier_telephone`= :SupplierTelephone,
			`supplier_email`= :SupplierEmail,
			`supplier_address`=:SupplierAddress,
			`supplier_cp`= :SupplierCP,
			`supplier_city`=:SupplierCity,
			`supplier_country`=:SupplierCountry,
			`supplier_notes`=:SupplierNotes,
            `supplier_web` = :SupplierWeb
			WHERE `supplier_id` = :SupplierId",
		   $this->fillParams($supplier)
		 );
		
	}
	
	
	public function deleteSupplier($SupplierId){
		
		$this->db->query("
			DELETE FROM `erp_suppliers` WHERE `supplier_id` = :SupplierId",
		array(':SupplierId' => $SupplierId ));
		
	}
	

	private function fillParams($supplier){

	$params = [];

		if(isset($supplier->SupplierId))	 { $params[':SupplierId'] = $supplier->SupplierId; } else { $params[':SupplierId'] = NULL; }; 
		if(isset($supplier->SupplierName))	 { $params[':SupplierName'] = $supplier->SupplierName; } else { $params[':SupplierName'] = NULL; }; 
		if(isset($supplier->SupplierRFC))	 { $params[':SupplierRFC'] = $supplier->SupplierRFC; } else { $params[':SupplierRFC'] = NULL; }; 
		if(isset($supplier->SupplierTelephone))	 { $params[':SupplierTelephone'] = $supplier->SupplierTelephone; } else { $params[':SupplierTelephone'] = NULL;};  
		if(isset($supplier->SupplierEmail)){ $params[':SupplierEmail'] = $supplier->SupplierEmail; } else { $params[':SupplierEmail'] = NULL; };  
		if(isset($supplier->SupplierAddress))	 { $params[':SupplierAddress'] = $supplier->SupplierAddress; } else { $params[':SupplierAddress'] = NULL; };   
		if(isset($supplier->SupplierCP))   { $params[':SupplierCP'] = $supplier->SupplierCP; } else { $params[':SupplierCP'] = NULL; }; 
		if(isset($supplier->SupplierCity)){ $params[':SupplierCity'] = $supplier->SupplierCity; } else { $params[':SupplierCity'] = NULL; }; 
		if(isset($supplier->SupplierCountry)){ $params[':SupplierCountry'] = $supplier->SupplierCountry; } else { $params[':SupplierCountry'] = NULL; }; 
		if(isset($supplier->SupplierNotes)){ $params[':SupplierNotes'] = $supplier->SupplierNotes; } else { $params[':SupplierNotes'] = NULL; };  
        if(isset($supplier->SupplierWeb)){ $params[':SupplierWeb'] = $supplier->SupplierWeb; } else { $params[':SupplierWeb'] = NULL; };  
		
		return $params;
		
	}
	
	private function convertSupplier($supplier)
	{
	return array(
			'SupplierId' => $supplier->supplier_id,
			'SupplierName' => $supplier->supplier_name, 
			'SupplierRFC' => $supplier->supplier_rfc,
    		'SupplierTelephone' => $supplier->supplier_telephone, 
			'SupplierEmail' => $supplier->supplier_email, 
			'SupplierAddress' => $supplier->supplier_address, 
			'SupplierCP' => $supplier->supplier_cp, 
			'SupplierCity' => $supplier->supplier_city, 
	 		'SupplierCountry' => $supplier->supplier_country, 
	 		'SupplierNotes' => $supplier->supplier_notes,
            'SupplierWeb' => $supplier->supplier_web
	 	);

	}

}
?>