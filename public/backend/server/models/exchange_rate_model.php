<?php
class ExchangeRateModel {
    
    private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}
	
	
	public function getExchangeRate($currency_id,$exchange_rate_date){
	    
	    $result = $this->db->query("
			SELECT 
				`currency_id`,
				`exchange_rate_date`, 
				`exchange_rate_amount`			
			FROM `erp_exchange_rate`
			WHERE `currency_id` = :CurrencyId 
			AND `exchange_rate_date` = :ExchangeRateDate
			"
			,
			array( ':CurrencyId' => $currency_id , ':ExchangeRateDate' => $exchange_rate_date )
		);

		$exchange_rate=$result->fetch(PDO::FETCH_OBJ);

		return $exchange_rate;
	    
	    
	    
	}
	
	
	public function setExchangeRate($exchange_rate){
	    
	    $sql = "INSERT INTO  `erp_exchange_rate` (`currency_id`,`exchange_rate_date`, `exchange_rate_amount`) VALUES (:CurrencyId,:ExchangeRateDate,:ExchangeRateAmount)";
	    
	    
	    if(isset($exchange_rate->currency_id) && isset($exchange_rate->exchange_rate_date))
	    {
	        
    	    $sql = "UPDATE  `erp_exchange_rate`  SET  `exchange_rate_amount` = :ExchangeRateAmount WHERE `currency_id` =:CurrencyId AND `exchange_rate_date` =:ExchangeRateDate";
	        
	    }
	    
	    $result = $this->db->query($sql,array( ':CurrencyId' => $exchange_rate->currency_id , ':ExchangeRateDate' => $exchange_rate->exchange_rate_date , ':ExchangeRateAmount' => $exchange_rate->exchange_rate_amount  )
		);

		

		return $exchange_rate;
	    
	    
	    
	}
	
	
}
?>