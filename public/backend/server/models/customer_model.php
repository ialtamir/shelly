<?php
class CustomerModel {
	
	private $db;
	private $statements = array("SELECT" => "SELECT *	FROM `erp_customers` ");

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function searchCustomers($query){
		
		$sql = $this->statements["SELECT"]." WHERE customer_name like :CustomerName  ";

		$result = $this->db->query($sql	,array(':CustomerName' => $query.'%'));

		$customers = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($customers as $customer) {

        	array_push($data, $this->convertCustomer($customer));
        }

		return $data;

	}

	public function getCustomers(){
		
		$sql = $this->statements["SELECT"];

		$result = $this->db->query($sql
			,
			array()
		);

		$customers = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($customers as $customer) {

        	array_push($data, $this->convertCustomer($customer));
        }

		return $data;

	}

	public function getCustomer($CustomerId){
		$sql = $this->statements["SELECT"];
		$sql = $sql." WHERE `customer_id` = :CustomerId";

		$result = $this->db->query($sql
			,
			array( ':CustomerId' => $CustomerId )
		);

		$customer=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertCustomer($customer);

	}
	
	public function getCustomerByUserId($user_id){
		$sql = $this->statements["SELECT"];
		$sql = $sql." WHERE `user_id` = :user_id";

		$result = $this->db->query($sql
			,
			array( ':user_id' => $user_id )
		);

		$customer=$result->fetch(PDO::FETCH_OBJ);

		return $customer;

	}
	
	public function getCustomerByCode($CustomerCode){
		
		$sql = $this->statements["SELECT"];
		$sql = $sql." WHERE `customer_code` = :CustomerCode";

		$result = $this->db->query($sql	,array( ':CustomerCode' => $CustomerCode )
		);

		$customer=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertCustomer($customer);

	}
	
	public function createCustomer($customer){


		$this->db->query("
			INSERT INTO erp_customers (
				customer_id,
				customer_code, 
				customer_name, 
				customer_rfc, 
				customer_street,
				customer_numExt,
				customer_numInt,
				customer_colony,
				customer_localidad,
				customer_address, 
				customer_cp, 
				customer_city, 
				customer_state,
				customer_country, 
				customer_telephone, 
				customer_cellphone, 
				customer_email, 
				customer_web,
				customer_references,
				customer_notes,
				customer_delivery_place,
				customer_rating ,
				customer_picture 
			) 
			VALUES ( 
				:CustomerId ,
				:CustomerCode, 
				:CustomerName, 
				:CustomerRFC, 
				:CustomerStreet,
				:CustomerNumExt,
				:CustomerNumInt,
				:CustomerColony,
				:CustomerLocalidad,
				:CustomerAddress, 
				:CustomerCP,  
				:CustomerCity, 
				:CustomerState,
				:CustomerCountry, 
				:CustomerTelephone,
				:CustomerCellphone, 
				:CustomerEmail,
				:CustomerWeb,
				:CustomerReferences,
				:CustomerNotes,
				:CustomerDeliveryPlace,
				:CustomerRating,
				:CustomerPicture
			)",
			$this->fillParams($customer)
		);	

		$customer->CustomerId = $this->db->getInsertId();

		return $customer;	
	}

	public function updateCustomer($customer){
		
		
		
		
		$this->db->query("
			UPDATE `erp_customers` SET 
				`customer_code`=:CustomerCode, 
				`customer_name`=:CustomerName,
				`customer_rfc`=:CustomerRFC,
				`customer_street`=:CustomerStreet,
				`customer_numExt`=:CustomerNumExt,
				`customer_numInt`=:CustomerNumInt,
				`customer_colony`=:CustomerColony,
				`customer_localidad`=:CustomerLocalidad,
				`customer_address`=:CustomerAddress,
				`customer_cp`=:CustomerCP,
				`customer_city`=:CustomerCity,
				`customer_state`=:CustomerState,
				`customer_country`=:CustomerCountry,
				`customer_telephone`=:CustomerTelephone,
				`customer_cellphone`=:CustomerCellphone,
				`customer_email`=:CustomerEmail,
				`customer_web` =:CustomerWeb,
				`customer_references`= :CustomerReferences,
				`customer_notes`=:CustomerNotes,
				`customer_delivery_place` = :CustomerDeliveryPlace,
				`customer_rating` = :CustomerRating,
				`customer_picture` = :CustomerPicture
		 WHERE `customer_id` = :CustomerId",
		   $this->fillParams($customer)
		 );
		
	}
	
	public function updateCustomerEcommerce($user_id,$customer){
		
		
		$parameters = [];
		$parameters[':customer_name']  = $customer['customer_name'];
		$parameters[':customer_last_name'] =  $customer['customer_last_name'];
		$parameters[':customer_rfc'] =  $customer['customer_rfc'];
		$parameters[':customer_street']=  $customer['customer_street'];
		$parameters[':customer_numExt']=  $customer['customer_numExt'];
		$parameters[':customer_numInt']= $customer['customer_numInt'];
		$parameters[':customer_colony']= $customer['customer_colony'];
		$parameters[':customer_localidad']=$customer['customer_localidad'];
		$parameters[':customer_address']= $customer['customer_address'];
		$parameters[':customer_cp']= $customer['customer_cp'];
		$parameters[':customer_city']= $customer['customer_city'];
		$parameters[':customer_country']= $customer['customer_country'];
		$parameters[':customer_state']= $customer['customer_state'];
		$parameters[':customer_telephone']= $customer['customer_telephone'];
		$parameters[':customer_cellphone']= $customer['customer_cellphone'];
		$parameters[':customer_web']= $customer['customer_web'];
		$parameters[':customer_references']= $customer['customer_references'];
		$parameters[':customer_notes']= $customer['customer_notes'];
		$parameters[':customer_delivery_place']= $customer['customer_delivery_place'];
		
		
		 if(!isset($customer['customer_id']) ){
		 
		 
		
		$sql = "
		
			INSERT INTO erp_customers (
				
				customer_name, 
				customer_last_name, 
				customer_rfc, 
				customer_street,
				customer_numExt,
				customer_numInt,
				customer_colony,
				customer_localidad,
				customer_address, 
				customer_cp, 
				customer_city, 
				customer_state,
				customer_country, 
				customer_telephone, 
				customer_cellphone, 
				
				customer_web,
				customer_references,
				customer_notes,
				customer_delivery_place,
				user_id,
				customer_email

			) 
			VALUES ( 
			
		
			:customer_name, 
				:customer_last_name, 
				:customer_rfc, 
				:customer_street,
				:customer_numExt,
				:customer_numInt,
				:customer_colony,
				:customer_localidad,
				:customer_address, 
				:customer_cp, 
				:customer_city, 
				:customer_state,
				:customer_country, 
				:customer_telephone, 
				:customer_cellphone, 
			
				:customer_web,
				:customer_references,
				:customer_notes,
				:customer_delivery_place;
				:user_id,
				:customer_email

			)
		";
		
		$parameters[":user_id"] = $user["UserId"];
		$parameters[":customer_email"] = $user["UserEmail"];
		
		$this->db->query($sql, $parameters);
		
		$customer["customer_id"] = $this->db->getInsertId();
		
		 }else {
		$sql = "
			UPDATE erp_customers SET 
				customer_name=:customer_name,
				customer_last_name=:customer_last_name,
				customer_rfc=:customer_rfc,
				customer_street=:customer_street,
				customer_numExt=:customer_numExt,
				customer_numInt=:customer_numInt,
				customer_colony=:customer_colony,
				customer_localidad=:customer_localidad,
				customer_address=:customer_address,
				customer_cp=:customer_cp,
				customer_city=:customer_city,
				customer_state=:customer_state,
				customer_country=:customer_country,
				customer_telephone=:customer_telephone,
				customer_cellphone=:customer_cellphone,
			
				customer_web =:customer_web,
				customer_references= :customer_references,
				customer_notes=:customer_notes,
				customer_delivery_place = :customer_delivery_place
		 WHERE customer_id = :customer_id";
		 $parameters[':customer_id']  = $customer['customer_id'];
		  $this->db->query($sql, $parameters);
		 }
		
		 
		 
		 
		
		

		 
		 
		 
		 
		 
		 
		 return $customer;
		 
		 

            
            
		
	}
	
	public function updateCustomerAddress($customer){
		
		$this->db->query("
			UPDATE erp_customers SET 
				customer_address=:CustomerAddress,
				customer_cp=:CustomerCP,
				customer_city=:CustomerCity,
				customer_state=:CustomerState,
				customer_country=:CustomerCountry,
				customer_latitude=:CustomerLatitude,
				customer_longitude=:CustomerLongitude,
				`customer_delivery_place` = :CustomerDeliveryPlace
		 WHERE customer_id = :CustomerId",
		   array(
		   	":CustomerAddress" => $customer->CustomerAddress,
			":CustomerCP" => $customer->CustomerCP,
			":CustomerCity" => $customer->CustomerCity,
			":CustomerState" => $customer->CustomerState,
			":CustomerCountry" => $customer->CustomerCountry,
			":CustomerLatitude" => $customer->CustomerLatitude,
			":CustomerLongitude" => $customer->CustomerLongitude,
			":CustomerDeliveryPlace" => $customer->CustomerDeliveryPlace,
			":CustomerId" => $customer->CustomerId
			
			
		   )
		 );
		
	}

	public function deleteCustomer($CustomerId){
		
		$this->db->query("
			DELETE FROM `erp_customers` 
			WHERE `customer_id` = :CustomerId",
		array(':CustomerId' => $CustomerId ));
		
	}
	
	public function updatePicture($CustomerId,$Picture){
		$this->db->query("
			UPDATE `erp_customers` SET 
				`customer_picture`=:Picture 
			WHERE `customer_id`=:CustomerId",
			array( 
				':CustomerId' => $CustomerId,
				':Picture' => $Picture)
		);
	}



	private function fillParams($customer) {

		$params = [];

		if(isset($customer->CustomerId))	 { $params[':CustomerId'] = $customer->CustomerId; } else { $params[':CustomerId'] = NULL; }; 
		if(isset($customer->CustomerCode))	 { $params[':CustomerCode'] = $customer->CustomerCode; } else { $params[':CustomerCode'] = NULL; }; 
		if(isset($customer->CustomerName))	 { $params[':CustomerName'] = $customer->CustomerName; } else { $params[':CustomerName'] = NULL; }; 
		if(isset($customer->CustomerRFC))	 { $params[':CustomerRFC'] = $customer->CustomerRFC; } else { $params[':CustomerRFC'] = NULL;};  
		if(isset($customer->CustomerStreet)){ $params[':CustomerStreet'] = $customer->CustomerStreet; } else { $params[':CustomerStreet'] = NULL; };  
		if(isset($customer->CustomerNumExt)){ $params[':CustomerNumExt'] = $customer->CustomerNumExt; } else { $params[':CustomerNumExt'] = NULL; };  
		if(isset($customer->CustomerNumInt)){ $params[':CustomerNumInt'] = $customer->CustomerNumInt; } else { $params[':CustomerNumInt'] = NULL; };  
		if(isset($customer->CustomerColony)){ $params[':CustomerColony'] = $customer->CustomerColony; } else { $params[':CustomerColony'] = NULL; };  
		if(isset($customer->CustomerLocalidad)){ $params[':CustomerLocalidad'] = $customer->CustomerLocalidad; } else { $params[':CustomerLocalidad'] = NULL; };  
		if(isset($customer->CustomerAddress)){ $params[':CustomerAddress'] = $customer->CustomerAddress; } else { $params[':CustomerAddress'] = NULL; };  
		if(isset($customer->CustomerCP))	 { $params[':CustomerCP'] = $customer->CustomerCP; } else { $params[':CustomerCP'] = NULL; };   
		if(isset($customer->CustomerCity))   { $params[':CustomerCity'] = $customer->CustomerCity; } else { $params[':CustomerCity'] = NULL; }; 
		if(isset($customer->CustomerState)){ $params[':CustomerState'] = $customer->CustomerState; } else { $params[':CustomerState'] = NULL; }; 
		if(isset($customer->CustomerCountry)){ $params[':CustomerCountry'] = $customer->CustomerCountry; } else { $params[':CustomerCountry'] = NULL; }; 
		if(isset($customer->CustomerTelephone)){ $params[':CustomerTelephone'] = $customer->CustomerTelephone; } else { $params[':CustomerTelephone'] = NULL; }; 
		if(isset($customer->CustomerCellphone)){ $params[':CustomerCellphone'] = $customer->CustomerCellphone; } else { $params[':CustomerCellphone'] = NULL; };  
		if(isset($customer->CustomerEmail))  { $params[':CustomerEmail'] = $customer->CustomerEmail; } else { $params[':CustomerEmail']  = NULL; }; 
		if(isset($customer->CustomerWeb))    { $params[':CustomerWeb'] = $customer->CustomerWeb; } else { $params[':CustomerWeb']  = NULL; }; 
		if(isset($customer->CustomerReferences))    { $params[':CustomerReferences'] = $customer->CustomerReferences; } else { $params[':CustomerReferences']  = NULL; }; 
		if(isset($customer->CustomerNotes))  { $params[':CustomerNotes'] = $customer->CustomerNotes; } else {  $params[':CustomerNotes'] = NULL; }; 
		if(isset($customer->CustomerDeliveryPlace))  { $params[':CustomerDeliveryPlace'] = $customer->CustomerDeliveryPlace; } else {  $params[':CustomerDeliveryPlace'] = NULL; }; 
		
		if(isset($customer->CustomerRating))  { $params[':CustomerRating'] = $customer->CustomerRating; } else {  $params[':CustomerRating'] = NULL; }; 
		if(isset($customer->CustomerPicture))  { $params[':CustomerPicture'] = $customer->CustomerPicture; } else {  $params[':CustomerPicture'] = NULL; }; 

		return $params;
			
	}

	private function convertCustomer($customer)
	{
		return array(
			'CustomerId' => $customer->customer_id,
			'CustomerCode' => $customer->customer_code, 
			'CustomerName' => $customer->customer_name,
    			'CustomerRFC' => $customer->customer_rfc,
    			'CustomerStreet' => $customer->customer_street,
    			'CustomerNumExt' => $customer->customer_numExt,
    			'CustomerNumInt' => $customer->customer_numInt,
    			'CustomerColony' => $customer->customer_colony,
    			'CustomerLocalidad' => $customer->customer_localidad,
			'CustomerAddress' => $customer->customer_address, 
			'CustomerCP' => $customer->customer_cp, 
			'CustomerCity' => $customer->customer_city, 
			'CustomerState' => $customer->customer_state,
			'CustomerCountry' => $customer->customer_country, 
	 		'CustomerTelephone' => $customer->customer_telephone, 
	 		'CustomerCellphone' => $customer->customer_cellphone, 
	 		'CustomerEmail' => $customer->customer_email,
	 		'CustomerWeb' => $customer->customer_web,
	 		'CustomerReferences' => $customer->customer_references,
	 		'CustomerNotes' => $customer->customer_notes,
	 		'CustomerDeliveryPlace' => $customer->customer_delivery_place,
	 		'CustomerRating' => $customer->customer_rating,
	 		'CustomerPicture' => $customer->customer_picture,
	 		'CustomerLatitude' => $customer->customer_latitude,
	 		'CustomerLongitude' => $customer->customer_longitude
	 		
	 		
	 		
	 		

	 	);
	}


	public function getCustomerContacts($CustomerId){

		$result = $this->db->query("
			SELECT 
				`customer_id`,
				`customer_contact_id`,
				`customer_contact_name`, 
				`customer_contact_description`, 
				`customer_contact_telephone`, 
				`customer_contact_telephone2`, 
				`customer_contact_email`
			FROM `erp_customer_contacts`
			WHERE `customer_id` = :CustomerId
			"
			,
			array(':CustomerId' => $CustomerId)
		);

		$customer_contacts = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($customer_contacts as $customer_contact) {

        	array_push($data, $this->convertCustomerContact($customer_contact));
        }

		return $data;



	}

	public function getCustomerContact($CustomerId, $CustomerContactId){

		$result = $this->db->query("
			SELECT 
				`customer_id`,
				`customer_contact_id`,
				`customer_contact_name`, 
				`customer_contact_description`, 
				`customer_contact_telephone`, 
				`customer_contact_telephone2`, 
				`customer_contact_email`
			FROM `erp_customer_contacts`
			WHERE `customer_id` = :CustomerId
			AND `customer_contact_id` = :CustomerContactId
			"
			,
			array(':CustomerId' => $CustomerId, ':CustomerContactId' => $CustomerContactId)
		);

		$customer_contact=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertCustomerContact($customer_contact);


	}

	public function createCustomerContact($customer_contact){


        if(!isset($customer_contact->CustomerContactId)){
        	$customer_contact->CustomerContactId = $this->getLastCustomerContactIdentity($customer_contact->CustomerId);
        }

		$this->db->query("
			INSERT INTO `erp_customer_contacts` (
			    `customer_id`,
				`customer_contact_id`,
				`customer_contact_name`, 
				`customer_contact_description`, 
				`customer_contact_telephone`, 
				`customer_contact_telephone2`, 
				`customer_contact_email`
			) 
			VALUES ( 
				:CustomerId ,
				:CustomerContactId, 
				:CustomerContactName, 
				:CustomerContactDescription, 
				:CustomerContactTelephone, 
				:CustomerContactTelephone2,  
				:CustomerContactEmail
			)",
			$this->fillCustomerContactParams($customer_contact)
		);	

		

		return $customer_contact;	
	}


	public function updateCustomerContact($customer_contact){
		$this->db->query("
			UPDATE `erp_customer_contacts` SET 
				
				`customer_contact_name` = :CustomerContactName, 
				`customer_contact_description` = :CustomerContactDescription, 
				`customer_contact_telephone` = :CustomerContactTelephone, 
				`customer_contact_telephone2` = :CustomerContactTelephone2, 
				`customer_contact_email` = :CustomerContactEmail
		 WHERE `customer_id` = :CustomerId
		 AND `customer_contact_id` = :CustomerContactId",
		   $this->fillCustomerContactParams($customer_contact)
		 );
	}

	private function getLastCustomerContactIdentity($CustomerId){


		$customer_contact_id=1;

		$sql = "
			SELECT  MAX(`customer_contact_id`)+1 as `customer_contact_id` FROM `erp_customer_contacts` 
			WHERE `customer_id` = :CustomerId
		";

		
		$statement = $this->db->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute(array(':CustomerId' => $CustomerId));


 		$result = $statement->fetch(PDO::FETCH_OBJ);
		 
		 if(isset($result->customer_contact_id)){
		 	$customer_contact_id = $result->customer_contact_id;
		 }

		
		return $customer_contact_id;
    
	}

	public function deleteCustomerContact($customer_contact){
		$this->db->query("
			DELETE FROM `erp_customer_contacts` 
			WHERE `customer_id` = :CustomerId
			AND `customer_contact_id` = :CustomerContactId",
		array(':CustomerId' => $customer_contact->CustomerId, ':CustomerContactId'  => $customer_contact->CustomerContactId));
	}

	public function fillCustomerContactParams($customer_contact){

			
 			$params =[];
		    $params[':CustomerId'] = ( isset($customer_contact->CustomerId) ?  $customer_contact->CustomerId : NULL );
			$params[':CustomerContactId'] = ( isset($customer_contact->CustomerContactId) ? $customer_contact->CustomerContactId : NULL );
			$params[':CustomerContactName'] = ( isset($customer_contact->CustomerContactName) ?  $customer_contact->CustomerContactName : NULL );
    		$params[':CustomerContactEmail'] = ( isset($customer_contact->CustomerContactEmail) ?  $customer_contact->CustomerContactEmail : NULL );
			$params[':CustomerContactDescription'] = ( isset($customer_contact->CustomerContactDescription) ?  $customer_contact->CustomerContactDescription :  NULL );
			$params[':CustomerContactTelephone'] = ( isset($customer_contact->CustomerContactTelephone) ? $customer_contact->CustomerContactTelephone : NULL );
			$params[':CustomerContactTelephone2'] = ( isset($customer_contact->CustomerContactTelephone2) ?  $customer_contact->CustomerContactTelephone2 : NULL ) ;
            
			return $params;

	}

	private function convertCustomerContact($customer_contact){

		return array(
			'CustomerId' => $customer->customer_id,
			'CustomerContactId' => $customer_contact->customer_contact_id, 
			'CustomerContactName' => $customer_contact->customer_contact_name,
    		'CustomerContactEmail' => $customer_contact->customer_contact_email, 
			'CustomerContactDescription' => $customer_contact->customer_contact_description, 
			'CustomerContactTelephone' => $customer_contact->customer_contact_telephone, 
			'CustomerContactTelephone2' => $customer_contact->customer_contact_telephone2
	 	);

	}
}
?>