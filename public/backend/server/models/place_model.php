<?php
class PlaceModel {
	
	private $db;

	public function __construct(){

		$this->db = DB::withAccount(AccountModel::getAccountConfiguration());

	}

	public function getPlaces(){

		$result = $this->db->query("
			SELECT place_id, place_name, place_description, place_street, place_numExt, place_numInt, place_colony, place_localidad, place_city, place_state, place_country, place_cp, place_external_id, place_external_code, place_external_name
			FROM erp_places"
			,
			array()
		);

		$places = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($places as $place) {

        	array_push($data, $this->convertPlace($place));
        }

		return $data;

	}

	public function getPlace($PlaceId){

		$result = $this->db->query("
			SELECT 
				`place_id`,
				`place_name`, 
				`place_description`,
				`place_street`, 
				`place_numExt`, 
				`place_numInt`, 
				`place_colony`, 
				`place_localidad`, 
				`place_city`,
				`place_state`, 
				`place_country`, 
				`place_cp`
			FROM `erp_places` 
			WHERE `place_id` = :PlaceId"
			,
			array( ':PlaceId' => $PlaceId )
		);

		$place=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertPlace($place);

	}

	public function createPlace($place){


		$this->db->query("
			INSERT INTO `erp_places` (
				`place_id`,
				`place_name`, 
				`place_description`,
				`place_street`, 
				`place_numExt`, 
				`place_numInt`, 
				`place_colony`, 
				`place_localidad`, 
				`place_city`,
				`place_state`, 
				`place_country`, 
				`place_cp`
			) 
			VALUES ( 
				:PlaceId ,
				:PlaceName, 
				:PlaceDescription,
				:PlaceStreet, 
				:PlaceNumExt, 
				:PlaceNumInt, 
				:PlaceColony, 
				:PlaceLocalidad, 
				:PlaceCity,
				:PlaceState, 
				:PlaceCountry, 
				:PlaceCp
			)",
			$this->fillParams($place)
		);	

		$place->PlaceId = $this->db->getInsertId();

		return $place;	
	}

	public function updatePlace($place){
		
		$this->db->query("
			UPDATE `erp_places` SET 
				`place_name`=:PlaceName, 
				`place_description`=:PlaceDescription,
				`place_street`=:PlaceStreet, 
				`place_numExt`=:PlaceNumExt, 
				`place_numInt`=:PlaceNumInt, 
				`place_colony`=:PlaceColony, 
				`place_localidad`=:PlaceLocalidad, 
				`place_city`=:PlaceCity,
				`place_state`=:PlaceState, 
				`place_country`=:PlaceCountry, 
				`place_cp`=:PlaceCp
		 WHERE `place_id` = :PlaceId",
		   $this->fillParams($place)
		 );
		
	}

	public function deletePlace($PlaceId){
		
		$this->db->query("
			DELETE FROM `erp_places` 
			WHERE `place_id` = :PlaceId",
		array(':PlaceId' => $PlaceId ));
		
	}

	private function fillParams($place) {

		$params = [];

		if(isset($place->PlaceId))	 { $params[':PlaceId'] = $place->PlaceId; }           else { $params[':PlaceId'] = NULL; }; 
		if(isset($place->PlaceName))	 { $params[':PlaceName'] = $place->PlaceName; }       else { $params[':PlaceName'] = NULL; }; 
		if(isset($place->PlaceDescription))	 { $params[':PlaceDescription'] = $place->PlaceDescription; } else { $params[':PlaceDescription'] = NULL; }; 
		if(isset($place->PlaceStreet))	 { $params[':PlaceStreet'] = $place->PlaceStreet; } else { $params[':PlaceStreet'] = NULL; }; 
		if(isset($place->PlaceNumExt))	 { $params[':PlaceNumExt'] = $place->PlaceNumExt; } else { $params[':PlaceNumExt'] = NULL; }; 
		if(isset($place->PlaceNumInt))	 { $params[':PlaceNumInt'] = $place->PlaceNumInt; } else { $params[':PlaceNumInt'] = NULL; }; 
		if(isset($place->PlaceColony))	 { $params[':PlaceColony'] = $place->PlaceColony; } else { $params[':PlaceColony'] = NULL; }; 
		if(isset($place->PlaceLocalidad))	 { $params[':PlaceLocalidad'] = $place->PlaceLocalidad; } else { $params[':PlaceLocalidad'] = NULL; }; 
		if(isset($place->PlaceCity))	 { $params[':PlaceCity'] = $place->PlaceCity; }     else { $params[':PlaceCity'] = NULL; }; 
		if(isset($place->PlaceState))	 { $params[':PlaceState'] = $place->PlaceState; }   else { $params[':PlaceState'] = NULL; }; 
		if(isset($place->PlaceCountry)) { $params[':PlaceCountry'] = $place->PlaceCountry; } else { $params[':PlaceCountry'] = NULL; }; 
		if(isset($place->PlaceCp))	 { $params[':PlaceCp'] = $place->PlaceCp; }         else { $params[':PlaceCp'] = NULL; }; 
	
		return $params;
			
	}

	private function convertPlace($place)
	{
		return array(
			'PlaceId' => $place->place_id,
			'PlaceDescription' => $place->place_description, 
			'PlaceName' => $place->place_name, 
			'PlaceStreet' => $place->place_street,
			'PlaceNumExt' => $place->place_numExt,
			'PlaceNumInt' => $place->place_numInt,
			'PlaceColony' => $place->place_colony,
			'PlaceLocalidad' => $place->place_localidad,
			'PlaceCity' => $place->place_city,
			'PlaceState' => $place->place_state,
			'PlaceCountry' => $place->place_country,
			'PlaceCp' => $place->place_cp,
			'PlaceExternalId' => $place->place_external_id,
			'PlaceExternalCode' => $place->place_external_code,
			'PlaceExternalName' => $place->place_external_name
		
			
			
    	);
	}
}
?>