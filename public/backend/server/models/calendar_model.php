<?php
class CalendarModel {

		private $db;

	public function __construct(){

		
        $this->db = DB::withAccount(AccountModel::getAccountConfiguration());
        

	}

	public function getSaleOrdersDeliveryDates(){

		$result = $this->db->query(" 
			SELECT 	
				`sale_order_id`, 
        		`sale_order_folio`, 
        		`sale_order_title`, 
        		`sale_order_description`, 
        		`sale_order_delivery_date`,
        		`quote_id`
			FROM `erp_sale_orders`
			WHERE `sale_order_status` = 'Aprobada'
			AND `sale_order_delivery_date`  IS NOT NULL
			ORDER BY `sale_order_date` DESC, `sale_order_id` DESC",
			array()
		);

		$sale_orders = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($sale_orders as $sale_order) {

        	array_push($data, $this->convertSaleOrderEvent($sale_order));
        }

		return $data;

	}

	public function getEvents(){
		
		$result = $this->db->query(" 
			SELECT 
				`event_id`, 
				`event_title`, 
				`event_type`, 
				`event_date`, 
				`event_time`, 
				`customer_id`, 
				`employee_id`, 
				`event_notes`, 
				`event_cdate`, 
				`user_id` 
			FROM `erp_events`",
			array()
		);

		$events = $result->fetchAll(PDO::FETCH_OBJ);
        
        $data = [];

        foreach ($events as $event) {

        	array_push($data, $this->convertEvent($event));
        }

		return $data;
	}


	public function getEvent($EventId){
		
		$result = $this->db->query(" 
			SELECT 
				`event_id`, 
				`event_title`, 
				`event_type`, 
				`event_date`, 
				`event_time`, 
				`customer_id`, 
				`employee_id`, 
				`event_notes`, 
				`event_cdate`, 
				`user_id` 
			FROM `erp_events`
			Where `event_id` = :EventId",
			array(':EventId' => $EventId)
		);


		$event=$result->fetch(PDO::FETCH_OBJ);

		return $this->convertEvent($event);

		
	}


	public function createEvent($event){
		$sql = "
			INSERT INTO `erp_events`(
				`event_id`, 
				`event_title`, 
				`event_type`, 
				`event_date`, 
				`event_time`, 
				`customer_id`, 
				`employee_id`, 
				`event_notes`, 
				`event_cdate`, 
				`user_id`
			) VALUES (
				:EventId,
				:EventTitle,
				:EventType,
				:EventDate,
				:EventTime,
				:CustomerId,
				:EmployeeId,
				:EventNotes,
				Now(),
				:UserId)
		";

		$this->db->query($sql,
			$this->eventParams($event)
		);	

		$event->EventId = $this->db->getInsertId();


		return $event;	


	}	

	public function updateEvent($event){

		$sql ="
			UPDATE `erp_events` SET 
				
				`event_title`=:EventTitle,
				`event_type`=:EventType,
				`event_date`=:EventDate,
				`event_time`=:EventTime,
				`customer_id`=:CustomerId,
				`employee_id`= :EmployeeId,
				`event_notes`=:EventNotes,
				`event_cdate`= Now(),
				`user_id`=:UserId 
			WHERE 
				`event_id`=:EventId
		";

		
		$this->db->query($sql,
		   $this->eventParams($event)
		 );
		
	}


	public function deleteEvent($event){

		$sql ="
			DELETE FROM `erp_events` 
			WHERE 
				`event_id`=:EventId
		";

		
		$this->db->query($sql,
		   array(':EventId' => $event->EventId)
		 );
		
	}



	private function eventParams($event){



		$params = [];
		$params[':EventId'] = isset($event->EventId) ? $event->EventId : NULL;
		$params[':EventTitle'] = isset($event->EventTitle) ? $event->EventTitle : NULL;
		$params[':EventType'] = isset($event->EventType) ? $event->EventType : NULL;
		$params[':EventDate'] = isset($event->EventDate) ? $event->EventDate : NULL;
		$params[':EventTime'] = isset($event->EventTime) ? $event->EventTime : NULL;
		$params[':CustomerId'] = isset($event->CustomerId) ? $event->CustomerId : NULL;
		$params[':EmployeeId'] = isset($event->EmployeeId) ? $event->EmployeeId : NULL;
		$params[':EventNotes'] = isset($event->EventNotes) ? $event->EventNotes : NULL;
		$params[':UserId']  = getOwnerId();

		
		return $params;

	}

	private function convertSaleOrderEvent($sale_order){
		$sale_order_delivery_date = new DateTime($sale_order->sale_order_delivery_date);


		return $events = array(
			'title' => $sale_order->sale_order_folio.': '.$sale_order->sale_order_title , 
			'year' => date_format($sale_order_delivery_date,'Y') ,
			'month' => date_format($sale_order_delivery_date,'m')-1 ,
			'day' => date_format($sale_order_delivery_date,'d'),
			'url' => "#/ventas/quote/".$sale_order->quote_id
		);


	}

	private function convertEvent($event)
	{

		$event_date = new DateTime($event->event_date);

		$data = $events = array(
			    'EventId' => $event->event_id, 
				'EventTitle' => $event->event_title, 
				'EventType' => $event->event_type, 
				'EventDate' => $event->event_date, 
				'EventTime' => $event->event_time, 
				'CustomerId' => $event->customer_id, 
				'EmployeeId' => $event->employee_id, 
				'EventNotes' => $event->event_notes, 
				`EventCDate` => $event->event_cdate,
				`UserId` =>  $event->User,
				'year' => date_format($event_date,'Y') ,
				'month' => date_format($event_date,'m') - 1 ,
				'day' => date_format($event_date,'d') ,
				'url' => 'http://google.com'
		);

		/*if(isset($event->customer_id)){
			$CustomerModel = new CustomerModel();
			$data["Customer"] = $CustomerModel->getCustomer($event->customer_id);
		}*/

		return $data;

	}
}
?>