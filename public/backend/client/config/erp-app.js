'use strict';


// Declare app level module which depends on filters, and services
var mainmodule = angular.module('erp', [
	'ngResource',
    'ngRoute',
    'ngCookies',
    'erp.filters', 
    'erp.services', 
    'erp.directives',
    'ui',
    'ui.bootstrap',
    'ui.calendar',
    'services.authentication',
    'services.httpRequestTracker',
    'services.notifications',
    'services.i18nNotifications',
    'services.localizedMessages',
    'login',
    'ngTable',
    'notifications',
    'ngMap',
    'tags-input',
    'ngMorrisChart',
    'angularFileUpload',
    'snap',
    'angular-input-stars'
    ]);



 mainmodule.
    config(
        function($routeProvider,$locationProvider,$httpProvider,snapRemoteProvider) {
   
            snapRemoteProvider.globalOptions.disable = 'right';
            var self = this;

            $routeProvider.
            when('/home', { 
                templateUrl: 'client/partials/dashboard.html', 
                controller: HomeCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }
            }).   
            when('/cuenta', { 
                templateUrl: 'client/partials/account/account-form.html', 
                controller: AccountCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }
            }). 
            when('/cuenta/facturacion', { 
                templateUrl: 'client/partials/account/account-facturacion-form.html', 
                controller: AccountCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }
            }). 
            when('/inbox', {templateUrl: 'client/partials/inbox.html', controller: InboxCtrl}).
            when('/usuarios', { 
                templateUrl: 'client/partials/users.html', 
                controller: UserCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }
            }).
            when('/ventas', { 
                templateUrl: 'client/partials/sale_orders/sale-orders.html', 
                controller: SaleOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/ventas/procesadas', { 
                templateUrl: 'client/partials/sale_orders/sale-orders.html', 
                controller: SaleOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/ventas/canceladas', { 
                templateUrl: 'client/partials/sale_orders/sale-orders.html', 
                controller: SaleOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/ventas/completadas', { 
                templateUrl: 'client/partials/sale_orders/sale-orders.html', 
                controller: SaleOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/ventas/nueva', { 
                templateUrl: 'client/partials/sale_orders/sale-orders-form.html', 
                controller: SaleOrderDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/ventas/:SaleOrderId', { 
                templateUrl: 'client/partials/sale_orders/sale-orders-form.html', 
                controller: SaleOrderDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
		     when('/pagos', { 
                templateUrl: 'client/partials/payments/payments.html', 
                controller: PaymentCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
             when('/pagos/completados/', { 
                templateUrl: 'client/partials/payments/payments.html', 
                controller: PaymentCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/pagos/nuevo', { 
                templateUrl: 'client/partials/payments/payments-form.html', 
                controller: PaymentDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/pagos/:PaymentId', { 
                templateUrl: 'client/partials/payments/payments-form.html', 
                controller: PaymentDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
             when('/resumen/pagos/:PaymentId', { 
                templateUrl: 'client/partials/payments/payment-detail-form-template.html', 
                controller: PaymentSummaryCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/cobranza/', { 
                templateUrl: 'client/partials/receivable/accounts-receivable.html', 
                controller: AccountsReceivableCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
             when('/cobranza/pagadas/', { 
                templateUrl: 'client/partials/receivable/accounts-receivable.html', 
                controller: AccountsReceivableCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/cobranza/resumen/:SaleOrderId', { 
                templateUrl: 'client/partials/receivable/accounts-receivable-summary.html', 
                controller: AccountsReceivableDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).

        	when('/cotizaciones', { 
                templateUrl: 'client/partials/quotes/quotes.html', 
                controller: QuoteCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/cotizaciones/procesadas', { 
                templateUrl: 'client/partials/quotes/quotes.html', 
                controller: QuoteCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/cotizaciones/canceladas', { 
                templateUrl: 'client/partials/quotes/quotes.html', 
                controller: QuoteCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).

            when('/cotizaciones/completadas', { 
                templateUrl: 'client/partials/quotes/quotes.html', 
                controller: QuoteCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).

            when('/cotizaciones/nueva', { 
                templateUrl: 'client/partials/quotes/quotes-form.html', 
                controller: QuoteDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/cotizaciones/:QuoteId', { 
                templateUrl: 'client/partials/quotes/quotes-form.html', 
                controller: QuoteDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/cobros/cotizaciones/:QuoteId', { 
                templateUrl: 'client/partials/quotes/quote-payments-form.html', 
                controller: QuoteSaleOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/cobros/ventas/:SaleOrderId', { 
                templateUrl: 'client/partials/sale_orders/sale-orders-payment-form.html', 
                controller: SaleOrderDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
			when('/adjuntos/cotizaciones/:QuoteId', { 
                templateUrl: 'client/partials/quotes/quote-attachments.html', 
                controller: QuoteAttachmentsCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
              }}).
            when('/ventas/quote/:QuoteId', { 
                templateUrl: 'client/partials/quotes/quotes-sale-order.html', 
                controller: QuoteSaleOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).           
            when('/compras', { 
                templateUrl: 'client/partials/purchase_orders/purchase-orders.html', 
                controller: PurchaseOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/compras/procesadas', { 
                templateUrl: 'client/partials/purchase_orders/purchase-orders.html', 
                controller:PurchaseOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/compras/nueva', { 
                templateUrl: 'client/partials/purchase_orders/purchase-orders-form.html', 
                controller: PurchaseOrderDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/compras/:PurchaseOrderId', { 
                templateUrl: 'client/partials/purchase_orders/purchase-orders-form.html', 
                controller: PurchaseOrderDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
             when('/clientes', { 
                templateUrl: 'client/partials/customers.html', 
                controller: CustomerCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/clientes/:CustomerId', { 
                templateUrl: 'client/partials/customer/customer-form.html', 
                controller: CustomerDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/clientes/:CustomerId/historial', { 
                templateUrl: 'client/partials/customer/customer-form.html', 
                controller: CustomerDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
                 when('/clientes/:CustomerId/mapa', { 
                templateUrl: 'client/partials/customer/customer-map.html', 
                controller: CustomerMapCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
                
                 when('/clientes/:CustomerId/creditos', { 
                templateUrl: 'client/partials/customer/customer-map.html', 
                controller: CustomerMapCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
        	when('/empleados', { 
                templateUrl: 'client/partials/employees.html', 
                controller: EmployeeCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
        	 when('/materiales', {
               templateUrl: 'client/partials/materials.html',
               controller: MaterialCtrl,
               resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                   return AuthenticationService.requireAuthenticatedUser();
                 }]
               }}).
			when('/prospectos', {
               templateUrl: 'client/partials/leads.html',
               controller: LeadCtrl,
               resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                  return AuthenticationService.requireAuthenticatedUser();
                }]
              }}).
        	 when('/unidades', { 
                templateUrl: 'client/partials/currencies.html', 
                controller: CurrencyCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
			when('/inventarios', { 
                templateUrl: 'client/partials/inventory/inventory.html', 
                controller: InventoryCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
			
            when('/inventarios/:InventoryId', { 
                templateUrl: 'client/partials/inventory/inventory-form.html', 
                controller: InventoryDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).

            when('/resumen/inventarios/:InventoryId', { 
                templateUrl: 'client/partials/inventory/inventory-detail-form-template.html', 
                controller: InventorySummaryCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).

            when('/existencias', {
                templateUrl: 'client/partials/inventory/stock.html',
                controller: StockCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                return AuthenticationService.requireAuthenticatedUser();
                }]
                }}).
        	 when('/proveedores', { 
                templateUrl: 'client/partials/suppliers.html', 
                controller: SupplierCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
        	 when('/sucursales', { 
                templateUrl: 'client/partials/places.html', 
                controller: PlaceCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/marcas', { 
                templateUrl: 'client/partials/categories.html', 
                controller: CategoryCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/categorias', { 
                templateUrl: 'client/partials/categories.html', 
                controller: CategoryCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/categorias/:CategoryId', { 
                templateUrl: 'client/partials/products/products.html', 
                controller: ProductCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/categorias/:CategoryId/nuevo', { 
                templateUrl: 'client/partials/products/product-form.html', 
                controller: ProductDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/categorias/:CategoryId/producto/:ProductId', { 
                templateUrl: 'client/partials/products/product-form.html', 
                controller: ProductDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/trabajos/', { 
                templateUrl: 'client/partials/work_orders/work-orders.html', 
                controller: WorkOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/trabajos/cerrados/', { 
                templateUrl: 'client/partials/work_orders/work-orders.html', 
                controller: WorkOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/trabajos/nuevo', { 
                templateUrl: 'client/partials/work_orders/work-orders-quotes-ready.html', 
                controller: WorkOrderQuoteCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/trabajos/:WorkOrderId', { 
                templateUrl: 'client/partials/work_orders/work-order-form.html', 
                controller: WorkOrderDetailCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/trabajos/cotizaciones/:QuoteId', { 
                templateUrl: 'client/partials/quotes/quote-work-order-form.html', 
                controller: QuoteSaleOrderCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/calendario/', { 
                templateUrl: 'client/partials/calendar/calendar.html', 
                controller: CalendarCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/productos/', {
                templateUrl: 'client/partials/products/products.html',
                controller: ProductCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                        return AuthenticationService.requireAuthenticatedUser();
                    }]
                }}).
            when('/productos-ecommerce/', {
                templateUrl: 'client/partials/products/products-ecommerce.html',
                controller: ProductEcommerceCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                        return AuthenticationService.requireAuthenticatedUser();
                    }]
                }}).
                
            when('/productos-destacados/', {
            templateUrl: 'client/partials/products/products-featured.html',
            controller: ProductFeaturedCtrl,
            resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                }]
            }}).
            when('/productos/categorias/', {
                templateUrl: 'client/partials/products/product-categories.html',
                controller: CategoryCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                        return AuthenticationService.requireAuthenticatedUser();
                    }]
                }}).
            when('/productos/marcas/', {
                templateUrl: 'client/partials/products/product-brands.html',
                controller: BrandCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                        return AuthenticationService.requireAuthenticatedUser();
                    }]
                }}).
            when('/productos/nuevo/', {
                templateUrl: 'client/partials/products/product-form.html',
                controller: ProductDetailCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                        return AuthenticationService.requireAuthenticatedUser();
                    }]
                }}).
            when('/productos/:ProductId', {
                templateUrl: 'client/partials/products/product-form.html',
                controller: ProductDetailCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                        return AuthenticationService.requireAuthenticatedUser();
                    }]
                }}).
            when('/contract_templates/', {
                templateUrl: 'client/partials/contract_templates/contract-templates.html',
                controller: ContractTemplateCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                        return AuthenticationService.requireAuthenticatedUser();
                    }]
                }}).
            when('/pos/', {
                templateUrl: 'client/partials/pos/pos.html',
                controller: POSCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                        return AuthenticationService.requireAuthenticatedUser();
                    }]
                }}).
            when('/sliders/', {
                templateUrl: 'client/partials/sliders.html',
                controller: SliderCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                        return AuthenticationService.requireAuthenticatedUser();
                    }]
                }}).
            when('/transaccion/:TransactionId', {
                templateUrl: 'client/partials/transaction/dashboard.html',
                controller: TransactionCtrl,
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                        return AuthenticationService.requireAuthenticatedUser();
                    }]
                }}).
            when('/chat/', { 
                templateUrl: 'client/partials/chat.html', 
                controller: ChatCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/facturas/', { 
                templateUrl: 'client/partials/facturas.html', 
                controller: FacturasCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }}).
            when('/pago/', { 
                templateUrl: 'client/partials/pago.html', 
                controller: PagoCtrl, 
                resolve: { mainmodule : ['AuthenticationService',function(AuthenticationService){
                    return AuthenticationService.requireAuthenticatedUser();
                  }]
                }})
            
            
             ;

            $routeProvider.otherwise({redirectTo: '/home'}); 

            /*
            $routeProvider.when('/configuracion', {templateUrl: 'client/partials/configuracion/configuracion.html', controller: TiendaCtrl});
            $routeProvider.when('/mistiendas', {templateUrl: 'client/partials/configuracion/tiendas.html', controller: TiendaCtrl});
            $routeProvider.when('/usuarios', {templateUrl: 'client/partials/configuracion/usuarios.html', controller: TiendaCtrl});
        */
            


     
  }).run(function($location,$rootScope,AuthenticationService,currentUser ){
        


        var history = [];

        $rootScope.$on('$routeChangeSuccess', function() {
            history.push($location.$$path);
        });
    
        $rootScope.back = function () {
            var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
            $location.path(prevUrl);
        };

        /*var userInfo = currentUser.info();

        // register listener to watch route changes
        $rootScope.$on( "$routeChangeStart", function(event, next, current) {
          if (currentUser.isAuthenticated() == false ) {
            // no logged user, we should be going to #login
            if ( next.templateUrl == "client/partials/dashboard.html" ) {
                    $location.path( "/login/" );
            } else {
              // not going to #login, we should redirect now
              $location.path( "/login/" );
            }
          }         
        });*/


        FastClick.attach(document.body);

        return AuthenticationService.requireAuthenticatedUser();
            
  });