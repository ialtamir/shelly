'use strict';

//TODO: move those messages to a separate module
angular.module('erp').constant('I18N.MESSAGES', {
  'errors.route.changeError':'Route change error',
  'crud.user.save.success':"El usuario '{{name}}' fue guardado correctamente.",
  'crud.user.remove.success':"El usuario '{{name}}' fue eliminado correctamente.",
  'crud.user.remove.error':"Ocurrio un error al tratar de eliminar el usuario '{{name}}'...",
  'crud.user.save.error':"Ocurrio un error al tratar de guardar el usuario...",
  'venta.save.success':"La venta del '{{date}}' fue agregada correctamente.",
  'login.error.notAuthorized':"No tiene permisos para realizar esta acción.",
  'login.error.notAuthenticated':"Inicie sesión para acceder a esta aplicación.",
  'crud.customer.save.success':"El cliente '{{name}}' fue guardado correctamente.",
  'crud.customer.remove.success':"El cliente '{{name}}' fue eliminado correctamente.",
  'crud.customer.remove.error':"Ocurrio un error al tratar de eliminar el cliente '{{name}}'...",
  'crud.customer.save.error':"Ocurrio un error al tratar de guardar el cliente...",
  'crud.category.save.success':"La categoria '{{name}}' fue guardada correctamente.",
  'crud.category.remove.success':"La categoria  '{{name}}' fue eliminada correctamente.",
  'crud.category.remove.error':"Ocurrio un error al tratar de eliminar la categoria  '{{name}}'...",
  'crud.category.save.error':"Ocurrio un error al tratar de guardar la categoria ..."
});
