'use strict';

function MaterialCtrl($scope,$filter,MaterialService,CategoryService,currentUser,ngTableParams,$notification) {

	var data = [];
    
	$scope.categories =[];
	$scope.material = { IsNew : true };
	$scope.materials = [];
    $scope.userInfo = currentUser;
   
	CategoryService.query(function (response){
        $scope.categories = response;       
    });
	
	MaterialService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.materials = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.materials = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.Materials.length;

    });

    $scope.refresh = function(){
        MaterialService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };

   

    $scope.add = function(){
        
        $scope.material = new MaterialService();
    	$scope.material.IsNew = true;

     	$scope.showMaterialModal = true;
         
    };

    $scope.edit = function(user){
       
    	$scope.material = user;
        $scope.showMaterialModal = true;
        
    };

    $scope.cancel = function(){
        
        $scope.material = {};
        $scope.showMaterialModal = false;

    };

    $scope.save = function(){

        $scope.material.$save({ MaterialId:$scope.material.MaterialId },function(){

        if($scope.material.MaterialId   >   0   &&  $scope.material.IsNew){
               
            data.push($scope.material);

            $notification.success($scope.material.MaterialName, 'El Material se guardo correctamente');
            
            $scope.material = {};
            $scope.tableParams.total = data.length;
        }
            
    	$scope.showMaterialModal = false;
            
        },function(){        	 
             $notification.error('Funcion incorrecta', 'El Material no se pudo guardar');
        });
        
    };

    $scope.delete = function(material){

        material.$remove({ MaterialId : material.MaterialId },function (){
            
            var index = data.indexOf(material,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
                $notification.success($scope.material.MaterialName, 'El Material se elimino correctamente');
                
                $scope.tableParams.total = data.length;
            }
            
        },function (){
             
     
              $notification.error('Eliminación incorrecta', 'El Material no se pudo eliminar');
        });
    };


}