'use strict';

function SaleOrderDetailCtrl ($scope,$http,$routeParams,$resource,tokenHandler,SaleOrderService){

    $scope.currentTokenInfo = tokenHandler.info();
    
	if($routeParams.SaleOrderId >0 ){ 

        $scope.SaleOrderId = $routeParams.SaleOrderId ;

        SaleOrderService.get({SaleOrderId:$scope.SaleOrderId},function (response){
            $scope.sale_order = response;
        });
    	
   	}

    
}

function SaleOrderCtrl ($scope,$resource,$http,$location,tokenHandler,SaleOrderService,ngTableParams,$filter,$notification){

	var data =[];
	$scope.customers = [];
	$scope.sale_orders = [];
    $scope.currentPath = $location.path();
    $scope.currentTokenInfo = tokenHandler.info();


    if($scope.currentPath === "/ventas/procesadas") {

          SaleOrderService.approved(function(response){

            data = response;
            $scope.tableParams.total = data.length;
                   
        });


    }
    else if($scope.currentPath === "/ventas/canceladas") {

          SaleOrderService.canceladas(function(response){

            data = response;
            $scope.tableParams.total = data.length;
                   
        });


    }
    else if($scope.currentPath === "/ventas/completadas") {

          SaleOrderService.completadas(function(response){

            data = response;
            $scope.tableParams.total = data.length;
                   
        });


    }
    else {
            SaleOrderService.query(function(response){

                data = response;
                $scope.tableParams.total = data.length;
                       
            });

        
    }



	

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.sale_orders = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.sale_orders = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.sale_orders.length;

    });

    $scope.delete = function(sale_order){

        sale_order.$remove({ SaleOrderId : sale_order.SaleOrderId },function (){
            
            var index = data.indexOf(sale_order,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
               
                $notification.success(sale_order.SaleOrderTitle, 'La venta se elimino correctamente');
                $scope.tableParams.total = data.length;
            }
            
        },function (){
              $notification.error('Eliminación incorrecta', 'La venta no se pudo eliminar');
        });
    };
    
    $scope.facturar = function( sale_order ){
        sale_order.$facturar({ SaleOrderId : sale_order.SaleOrderId },function (){
            
            $notification.success(sale_order.SaleOrderTitle, 'Se genero la factura correctamente');
            
            
        },function(response){
            $notification.error("Ocurrió un error", response.data.error);
        });
    };
    
    
    
    

	



}