


function ProductDetailCtrl($scope,$resource,$http,$timeout,$routeParams,$filter,$upload,ProductService,FeatureService,CategoryService,limitToFilter,$notification,tokenHandler){

    $scope.categories =[];
    $scope.feature_groups = [];
    $scope.unidades=[];
    $scope.variants = [];
    $scope.selected_feature_group = [];
    $scope.tokenInfo = tokenHandler;
    //var image_ext_white_list = ["JPG","JPEG","PNG","GIF"];
    


    $scope.fileReaderSupported = window.FileReader != null;
    $scope.uploadRightAway = true;
    $scope.howToSend = 1;

  

    CategoryService.query(function (response){
        $scope.categories = response;       
    });



	$scope.product = new ProductService({
        CategoryId : parseInt($routeParams.CategoryId),        
		IsNew : true,
		variants : [],
		product_variants: [],
        feature_groups: [],
        pictures : [],
        deleted_pictures:[]
    });

    if($routeParams.ProductId > 0)
    {
        ProductService.get({ProductId : $routeParams.ProductId},function(response){
            $scope.product = response;
            $scope.product.deleted_pictures = [];
        });
    }

 
    ProductService.variants(function(response){

    	$scope.variants = response;
               
    });

    ProductService.unidades(function(response){

        $scope.unidades = response;
               
    });



    FeatureService.query(function(response){
        $scope.feature_groups = response;
    });

   
    $scope.$watch('selected_feature_group', function (selected){

        if(selected){
            
            selected.features =  FeatureService.features({ FeatureGroupId : selected.FeatureGroupId });
            $scope.product.feature_groups.push(selected);
            
        }

    });

    $scope.addFeature = function(feature_group,feature){

        var newFeature = {
            FeatureGroupId : feature_group.FeatureGroupId,
            FeatureName : feature,
            FeatureSelected : true
        };

        FeatureService.saveFeature({FeatureGroupId : feature_group.FeatureGroupId },newFeature,function(){
             
             feature_group.features.push(newFeature);
             feature = "";
        });
 
    };
    
    $scope.selectFeature = function(feature_group){
        
        $scope.selected_feature_group = feature_group;
        
    };
    

    $scope.$watch('product.variants',function(){

        var arreglo=[];


        if($scope.product.variants){ arreglo = $scope.product.variants;  }

            var combinations = [], arg = arreglo || [], max= arreglo.length-1;
            
            function helper(arr,i){
          
                if(arg[i]){
                	for (var j=0, l=arg[i].VariantValues.length; j< l; j++)
                	{
                		var a = arr.slice(0);

                        a.push( { 'VariantId': arg[i].VariantId,'VariantValueName' : arg[i].VariantValues[j].VariantValueName, 'VariantValuePictureUrl': arg[i].VariantValues[j].VariantValuePictureUrl})
                        
                		if(i==max){

                			combinations.push(a);

                		} else {

                			helper(a, i+1);
                		}        			
                	}
                }
            }


        	helper([], 0);
            

            if($scope.product.IsNew){
            var newVariants=[]; 

            //Creamos las variantes dadas las  combinacones
            for(var c = 0, len = combinations.length; c < len ; c++){
               
                    var name="",code="";
                    var pictures = [];
                    
                   for(var d= 0, largo = combinations[c].length; d< largo;d++)
                   {
                        var  separator = "";

                        if(d > 0) separator = "/" ; 

                        name += separator+combinations[c][d].VariantValueName ;
                        code += separator+combinations[c][d].VariantValueName.substring(0,1);
                        

                        var picture = { ProductPictureUrl: combinations[c][d].VariantValuePictureUrl, ProductPictureThumbnail : combinations[c][d].VariantValuePictureUrl ,IsNew : true , ProductPictureDefault : false };


                        pictures.push(picture);


                   }
          
                 var newVariant = {
                     
                    "ProductName" : $scope.product.ProductName + ', ' + name,
                    "ProductCode" : $scope.product.ProductCode,
                    "Combinations" : combinations[c],
                    "ProductPrice" : $scope.product.ProductPrice,
                    "ProductCostPrice" : $scope.product.ProductCostPrice,
                    "ProductManageStock" : $scope.product.ProductManageStock


                 }

                 newVariant.pictures = pictures;


                 newVariants.push(newVariant);
              

            }

            $scope.product.product_variants = newVariants;
        }
        
       
        
    },true);

	$scope.saveProduct = function() {

		$scope.product.$save({ ProductId : $scope.product.ProductId} , function (){
				$notification.success($scope.product.ProductName, 'El Producto se guardo correctamente');
		});
	};

	$scope.addVariant = function(variant){

        
		var index = $scope.product.variants.indexOf(variant,0);

		if(index === -1){
			variant.values = [];
			$scope.product.variants.push(variant);
            $scope.showVariantsModal = false;
		}
	};

    $scope.removeProductVariant = function(variant)
    {
        var index = $scope.product.variants.indexOf(variant);

        if(index === -1){
             $scope.product.variants.splice(index,1);

        }
    };


    $scope.hasUploader = function(index) {
        return $scope.upload[index] != null;
   
    }
    
    $scope.abort = function(index) {
        
        $scope.upload[index].abort(); 
        $scope.upload[index] = null;
        
    };

    $scope.addProductPicture = function(data){

        var picture = { ProductPictureUrl: data.url , ProductPictureThumbnail : data.thumbnail_url ,IsNew : true , ProductPictureDefault : false };

        $scope.product.pictures.push(picture);

       
    };

    $scope.removeProductPicture = function(picture){


        if(picture.IsNew) {

            var index = $scope.product.pictures.indexOf(picture,0); 
            
            if (index != -1 )
            {

                $scope.product.pictures.splice(index,1) ;
                
            }  
        }
        else {

            $scope.product.deleted_pictures.push(picture);

            var index = $scope.product.pictures.indexOf(picture,0); 
            
            if (index != -1 )
            {
                $scope.product.pictures.splice(index,1) ;
            }  

        }
    };

    $scope.setProductPictureDefault = function(picture){



         for(var i=0;i< $scope.product.pictures.length;i++){
            
                $scope.product.pictures[i].ProductPictureDefault = false;
         }
        
        picture.ProductPictureDefault = true;
           
    };

    $scope.onFileSelect = function($files) {
        
        //console.log($files);
        
        
        $scope.selectedFiles = [];
        $scope.progress = [];

        if ($scope.upload && $scope.upload.length > 0) {
            for (var i = 0; i < $scope.upload.length; i++) {
                if ($scope.upload[i] != null) {
                    $scope.upload[i].abort();
                }
            }
        }

        $scope.upload = [];
        $scope.uploadResult = [];
        $scope.selectedFiles = $files;
        $scope.dataUrls = [];


        for ( var i = 0; i < $files.length; i++) {
            
            var $file = $files[i];
            
            if($file.type.indexOf('image') < 0){ 
                alert("Ha seleccionado archivos que no son una imagen")
                break;
            };
            
            if (window.FileReader && $file.type.indexOf('image') > -1) {
                
                var fileReader = new FileReader();
                
                fileReader.readAsDataURL($files[i]);
                
                function setPreview(fileReader, index) {
                    
                    fileReader.onload = function(e) {
                        $timeout(function() {
                            $scope.dataUrls[index] = e.target.result;
                        });
                    }
                }
                
                setPreview(fileReader, i);
                
            } else{
                
            }
            
            $scope.progress[i] = -1;
            
            if ($scope.uploadRightAway) {
                $scope.start(i);
            }
        }
    };



    $scope.start = function(index) {
        $scope.progress[index] = 0;
        if ($scope.howToSend == 1) {
            $scope.upload[index] = $upload.upload({
                url : '/backend/api/products/upload/',
                method: $scope.httpMethod,
                headers: {'d': 'myHeaderVal'},
                data : {
                    myModel : $scope.myModel
                },
                /* formDataAppender: function(fd, key, val) {
                    if (angular.isArray(val)) {
                        angular.forEach(val, function(v) {
                          fd.append(key, v);
                        });
                      } else {
                        fd.append(key, val);
                      }
                }, */
                file: $scope.selectedFiles[index],
                fileFormDataName: 'file'
            }).then(function(response) {
                $scope.uploadResult.push(response.data.result);
                $scope.addProductPicture(response.data)
    

            }, null, function(evt) {
                $scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
            });
        } else {
            var fileReader = new FileReader();
            fileReader.readAsArrayBuffer($scope.selectedFiles[index]);
            fileReader.onload = function(e) {
                $scope.upload[index] = $upload.http({
                    url: '/backend/api/products/upload/',
                    headers: {'Content-Type': $scope.selectedFiles[index].type },
                    data: e.target.result
                }).then(function(response) {
                    $scope.uploadResult.push(response.data.result);
                    $scope.addProductPicture(response.data)
    
                    //$scope.product.pictures.push(response.data.result);
                }, null, function(evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        }
    }
    

    
}

function ProductVariantsCtrl($scope,$resource,ProductService){

}

function ProductFeatureCtrl(){

}

function ProductPictureCtrl(){

}

function ProductCtrl($scope,$resource,$http,ProductService,CategoryService,limitToFilter,$notification,$routeParams,ngTableParams,$filter){


    var data =[];
    $scope.products = [];
    $scope.CategoryId = parseInt($routeParams.CategoryId);


    if($scope.CategoryId > 0)
        ProductService.productsByCategory({CategoryId : $scope.CategoryId },function(response){

            data = response;
            $scope.tableParams.total = data.length;
                   
        });
    else{
       ProductService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
                   
        });
    }



    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.products = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);


    $scope.$watch('search',function(query){

         $scope.products = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.products.length;

    });
    


    $scope.deleteProduct = function(product){

         product.$remove({ ProductId : product.ProductId },function (){
            
            var index = data.indexOf(product,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
               
                $notification.success(product.ProductName, 'Se elimino correctamente');
                $scope.tableParams.total = data.length;
            }
            
        },function (){
              $notification.error('Eliminación incorrecta', 'No se pudo eliminar');
        });

    }





}

function ProductEcommerceCtrl($scope,$resource,$http,ProductService,CategoryService,limitToFilter,$notification,$routeParams,ngTableParams,$filter){


    var data =[];
    $scope.products = [];
    $scope.CategoryId = parseInt($routeParams.CategoryId);


    if($scope.CategoryId > 0)
    
        ProductService.productsByCategory({CategoryId : $scope.CategoryId },function(response){

            data = response;
            $scope.tableParams.total = data.length;
                   
        });
    else{
        
       ProductService.ecommerce(function(response){

            data = response;
            $scope.tableParams.total = data.length;
                   
        });
    }



    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.products = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);


    $scope.$watch('search',function(query){

         $scope.products = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.products.length;

    });
    


    $scope.deleteProduct = function(product){

         product.$remove({ ProductId : product.ProductId },function (){
            
            var index = data.indexOf(product,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
               
                $notification.success(product.ProductName, 'Se elimino correctamente');
                $scope.tableParams.total = data.length;
            }
            
        },function (){
              $notification.error('Eliminación incorrecta', 'No se pudo eliminar');
        });

    }





}

function ProductFeaturedCtrl($scope,$resource,$http,ProductService,CategoryService,limitToFilter,$notification,$routeParams,ngTableParams,$filter){


    var data =[];
    $scope.products = [];
    $scope.CategoryId = parseInt($routeParams.CategoryId);


    if($scope.CategoryId > 0)
    
        ProductService.productsByCategory({CategoryId : $scope.CategoryId },function(response){

            data = response;
            $scope.tableParams.total = data.length;
                   
        });
    else{
        
       ProductService.featured(function(response){

            data = response;
            $scope.tableParams.total = data.length;
                   
        });
    }



    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.products = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);


    $scope.$watch('search',function(query){

         $scope.products = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.products.length;

    });
    


    $scope.deleteProduct = function(product){

         product.$remove({ ProductId : product.ProductId },function (){
            
            var index = data.indexOf(product,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
               
                $notification.success(product.ProductName, 'Se elimino correctamente');
                $scope.tableParams.total = data.length;
            }
            
        },function (){
              $notification.error('Eliminación incorrecta', 'No se pudo eliminar');
        });

    }





}