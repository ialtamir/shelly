function AccountCtrl($scope,$notification,$http,$timeout,$upload,AccountService){
    
    $scope.account = {};
    $scope.fileReaderSupported = window.FileReader != null;
    $scope.uploadRightAway = true;
    $scope.howToSend = 1;
    
    AccountService.get({ id :'default'},function(response){
        $scope.account = response;
    });
    
    $scope.save = function(){
        $scope.account.$save({ id:"default"},function(){

            $notification.success("Correcto","La información se guardo correctamente");
            
            
        },function(){
          
             $notification.error('Error', 'No se pudo guardar la informacion');
        });  
    };
    
    $scope.updatePrivateKeyPassword = function(){
        
        if($scope.account.cert_password !== $scope.account.confirm_cert_password){
            
            
             $notification.error('Error', 'Las contraseñas no concuerdan, favor de verificar!');
             return;
        }
        
        $scope.account.$update_pkey_password({ id: 'default'},function(){
            //Success
            
            
            
            $notification.success("Correcto","La contraseña se modifico correctamente");
            $scope.showChangePasswordModal = false;
         
            
        },function(){
            //Error
            
        });
    };
    
     $scope.hasUploader = function(index) {
        return $scope.upload[index] != null;

    };
    
    $scope.abort = function(index) {
        $scope.upload[index].abort(); 
        $scope.upload[index] = null;
    };



    $scope.onFileSelect = function($files,FileName,$document) {
        
        $scope.selectedFiles = [];
        $scope.progress = [];

        if ($scope.upload && $scope.upload.length > 0) {
            for (var i = 0; i < $scope.upload.length; i++) {
                if ($scope.upload[i] != null) {
                    $scope.upload[i].abort();
                }
            }
        }

        $scope.upload = [];
        $scope.uploadResult = [];
        $scope.selectedFiles = $files;
        $scope.dataUrls = [];


        for ( var i = 0; i < $files.length; i++) {
            var $file = $files[i];
            if (window.FileReader && $file.type.indexOf('image') > -1) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL($files[i]);
                function setPreview(fileReader, index) {
                    fileReader.onload = function(e) {
                        $timeout(function() {
                            $scope.dataUrls[index] = e.target.result;
                        });
                    }
                }
                setPreview(fileReader, i);
            }
            $scope.progress[i] = -1;
            if ($scope.uploadRightAway) {
                $scope.start(i,FileName,$document);
            }
        }
    };
    
    
    
    $scope.start = function(index,FileName, $document) {
        $scope.progress[index] = 0;
        if ($scope.howToSend == 1) {
            $scope.upload[index] = $upload.upload({
                url : '/api/account/upload/',
                method: $scope.httpMethod,
                headers: {'myHeaderKey': 'myHeaderVal'},
                data : {
                    Document : FileName,
                    myModel : $scope.myModel
                    
                },
                /* formDataAppender: function(fd, key, val) {
                    if (angular.isArray(val)) {
                        angular.forEach(val, function(v) {
                          fd.append(key, v);
                        });
                      } else {
                        fd.append(key, val);
                      }
                }, */
                file: $scope.selectedFiles[index],
                fileFormDataName: 'file'
            }).then(function(response) {
                $scope.uploadResult.push(response.data.result);
               
               $document = response.data.url;

            }, null, function(evt) {
                $scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
            });
        } 
        else {
            var fileReader = new FileReader();
            fileReader.readAsArrayBuffer($scope.selectedFiles[index]);
            fileReader.onload = function(e) {
                $scope.upload[index] = $upload.http({
                    url: '/api/account/upload/',
                    headers: {'Content-Type': $scope.selectedFiles[index].type },
                    data: e.target.result
                }).then(function(response) {
                    $scope.uploadResult.push(response.data.result);
                    //$scope.addPicture(response.data)
                    $document = response.data.url;
                    //$scope.product.pictures.push(response.data.result);
                }, null, function(evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        }
    };
    
}