

function SliderCtrl($scope,$filter,$notification,SliderService,currentUser,ngTableParams,i18nNotifications,$http, $timeout, $upload){

	var data = [];
    
	$scope.slider = { IsNew : true };
	$scope.sliders = [];


    $scope.fileReaderSupported = window.FileReader != null;
    $scope.uploadRightAway = true;
    $scope.howToSend = 1;

	SliderService.query(function(response){

        data = response;
        $scope.sliders = data;
        
               
    });

    $scope.simpleThumb = 'http://placehold.it/160x120';


    $scope.hasUploader = function(index) {
        return $scope.upload[index] != null;
    };
    $scope.abort = function(index) {
        $scope.upload[index].abort(); 
        $scope.upload[index] = null;
    };

    $scope.onFileSelect = function($files,slider) {
        $scope.selectedFiles = [];
        $scope.progress = [];

        if ($scope.upload && $scope.upload.length > 0) {
            for (var i = 0; i < $scope.upload.length; i++) {
                if ($scope.upload[i] != null) {
                    $scope.upload[i].abort();
                }
            }
        }

        $scope.upload = [];
        $scope.uploadResult = [];
        $scope.selectedFiles = $files;
        $scope.dataUrls = [];


        for ( var i = 0; i < $files.length; i++) {
            var $file = $files[i];
            if (window.FileReader && $file.type.indexOf('image') > -1) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL($files[i]);
                function setPreview(fileReader, index) {
                    fileReader.onload = function(e) {
                        $timeout(function() {
                            $scope.dataUrls[index] = e.target.result;
                        });
                    }
                }
                setPreview(fileReader, i);
            }
            $scope.progress[i] = -1;
            if ($scope.uploadRightAway) {
                $scope.start(i,slider);
            }
        }
    };


    $scope.start = function(index,slider) {
        $scope.progress[index] = 0;
        if ($scope.howToSend == 1) {
            $scope.upload[index] = $upload.upload({
                url : '/api/sliders/' + slider.id + '/upload',
                method: $scope.httpMethod,
                headers: {'myHeaderKey': 'myHeaderVal'},
                data : {
                    myModel : $scope.myModel
                },
                /* formDataAppender: function(fd, key, val) {
                    if (angular.isArray(val)) {
                        angular.forEach(val, function(v) {
                          fd.append(key, v);
                        });
                      } else {
                        fd.append(key, val);
                      }
                }, */
                file: $scope.selectedFiles[index],
                fileFormDataName: 'myFile'
            }).then(function(response) {
                $scope.uploadResult.push(response.data.result);
                //alert(response.data.url);
                slider.image = response.data.url;
            }, null, function(evt) {
                $scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
            });
        } else {
            var fileReader = new FileReader();
            fileReader.readAsArrayBuffer($scope.selectedFiles[index]);
            fileReader.onload = function(e) {
                $scope.upload[index] = $upload.http({
                    url: '/api/sliders/' + slider.id + '/upload',
                    headers: {'Content-Type': $scope.selectedFiles[index].type },
                    data: e.target.result
                }).then(function(response) {
                    $scope.uploadResult.push(response.data.result);
                    //alert(response.data.url);
                    slider.sliderPicture = response.data.url;
                }, null, function(evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        }
    }


    $scope.refresh = function(){
        SliderService.query(function(response){

            data = response;
            $scope.sliders = data;
            $scope.search = '';

        });
    };


    $scope.add = function(){
        
        $scope.slider = new SliderService();
    	$scope.slider.IsNew = true;
        $scope.slider.sliderPicture = 'http://placehold.it/160x120';

     	$scope.showModal = true;
         
    };

    $scope.edit = function(slider){
       
    	$scope.slider = slider;
        $scope.showModal = true;
        
    };

    $scope.cancel = function(){
        
        $scope.slider = {};
        $scope.showModal = false;

    };



     $scope.save = function(){

        $scope.slider.$save({ id:$scope.slider.id },function(){

        if($scope.slider.id   >   0   &&  $scope.slider.IsNew){
               
            data.push($scope.slider);            
            $notification.success($scope.slider.title, 'El slider se guardo correctamente');
            $scope.slider = {};
            
        }
            
    	$scope.showModal = false;
            
        },function(){
        	 
             $notification.error($scope.slider.title, 'El slider no pudo guardar');
        });
        
    };

    $scope.delete = function(slider){

        slider.$remove({ id : slider.id },function (){
            
            var index = data.indexOf(slider,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;                
                $notification.success(slider.title, 'El slider se elimino correctamente');
            }
            
        },function (){             
             $notification.error('Eliminación incorrecta', 'El slider no se pudo eliminar');
        });
    };


}