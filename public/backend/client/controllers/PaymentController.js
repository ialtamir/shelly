'use strict';

function PaymentDetailCtrl ($scope,$http,$location,$routeParams,$filter,CurrencyService,SupplierService,PaymentService,limitToFilter,$resource,$notification,ngTableParams){

    $scope.payment = new PaymentService({});
   
    var data = [];
   
    $scope.payments = [];
    $scope.suppliers = [];
   
   PaymentService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });

   SupplierService.query(function(response){

        $scope.suppliers=response;
   });

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

   
    if($routeParams.PaymentId > 0 ){ 
        
        $scope.payment.PaymentId = $routeParams.PaymentId;

        PaymentService.get({PaymentId:$routeParams.PaymentId},function(response) {
            $scope.payment = response;
        });

    }
    
   
    $scope.suppliers = [];
    $scope.currencies =[];
     
    $scope.selectAnotherSupplier = function (){
        $scope.selectedSupplier = '';
    };
    
    $scope.supplierSearch = function (viewValue) {
        return $http.get('api/suppliers/search/'+viewValue).then(function(response){
            return limitToFilter(response.data,15);
        });
    };
    

    CurrencyService.query(function(response){
        $scope.currencies = response;
    });
    
        ///--> Service API Calls Start 
    
    
    
    $scope.payment = new PaymentService({
        IsNew : true, 
        PaymentDate : new Date().yyyymmdd(),
        CurrencyId : 2
    });
    
    
    $scope.savePayment = function() {

        /*if($scope.selectedSupplier){
          
          if($scope.selectedSupplier.SupplierId){

            $scope.payment.SupplierId = $scope.selectedSupplier.SupplierId;

          };

        }*/

        $scope.payment.$save({ PaymentId : $scope.payment.PaymentId} , function (){
                
                
                $notification.success($scope.payment.PaymentId, 'El Pago se guardo correctamente');
                if($scope.payment.IsNew)
                {
                    $location.path('/pagos/'+$scope.payment.PaymentId);
                    //$scope.$apply();
                    //alert("Es nuevo");
                }
        });
    };
    
    
}

function PaymentSummaryCtrl($scope,$routeParams,$notification,PaymentService){

    $scope.newpayment = { PaymentDetailDate : new Date().yyyymmdd(), PaymentDetailAmount : 0 };
    $scope.paymentDetails = [];
    $scope.currentProgress = 0;
    $scope.PaymentTotalAmount = 0;
    $scope.paymentMethods = [{ PaymentMethodId: 1, PaymentMethodName: "Efectivo"},{ PaymentMethodId: 2, PaymentMethodName: "Tarjeta"},{ PaymentMethodId: 3, PaymentMethodName: "Cheque"}];
            
    
    if($routeParams.PaymentId > 0 ){ 
        
        $scope.PaymentId = $routeParams.PaymentId;

        PaymentService.get({PaymentId:$routeParams.PaymentId},function(response) {
            $scope.payment = response;

            PaymentService.paymentDetails({PaymentId:$scope.PaymentId },function(response){
                $scope.PaymentDetails = response;
            });
        });

    }


    var calculateBalance = function (){

        if($scope.payment){

            var Balance = $scope.payment.PaymentTotal;
            var PaymentTotal = 0;
            $scope.currentProgress = 0;


            if($scope.PaymentDetails.length > 0){
                
                for(var i=0, len=$scope.PaymentDetails.length; i < len ; i ++)
                {

                        Balance -= parseFloat($scope.PaymentDetails[i].PaymentDetailAmount);
                        PaymentTotal += parseFloat($scope.PaymentDetails[i].PaymentDetailAmount);

                        $scope.PaymentDetails[i].PaymentDetailBalance = Balance;

                };
                
                $scope.currentProgress =Math.floor((PaymentTotal / $scope.payment.PaymentTotal)  * 100);
                $scope.PaymentTotalAmount = PaymentTotal;

            };
        };
        
    };

    $scope.$watch('PaymentDetails',calculateBalance,true);

    $scope.addPayment = function(){
      
        if ( ($scope.PaymentTotalAmount + parseFloat($scope.newpayment.PaymentDetailAmount) ) <= $scope.payment.PaymentTotal) {

            PaymentService.savePaymentDetails({ PaymentId: $scope.payment.PaymentId },$scope.newpayment,function(response){
                
                $scope.PaymentDetails.push(response);
                $scope.newpayment = { PaymentDetailDate : new Date() , PaymentDetailAmount : 0 };
                $notification.success("Pago correcto", 'El pago se guardo correctamente!');
                
            });
        }
        else{
            
            $notification.error("Pago invalido", "La cantidad del pago excede el monto total a pagar");
            return false;
        };

    };

    $scope.updatePayment = function(payment){

                    if ( ($scope.PaymentTotalAmount ) <= $scope.payment.PaymentTotal) {

                        PaymentService.savePaymentDetails({ PaymentId: payment.PaymentId , PaymentDetailId : payment.PaymentDetailId},payment,function(){

                            $notification.success("Pago", 'El pago se actualizo correctamente!');
                            
                        });

                    }else{
                        
                        $notification.error("Pago invalido", "La cantidad del pago excede el monto total a pagar");
                        return false;
                    };;
                };


                $scope.editPayment = function(payment){
        //original = angular.copy(payment);
        //original = angular.copy(payment);                   
        payment.inEditMode=true;

    };

    $scope.cancelEditPayment = function(payment){
        //payment.SaleOrderPaymentAmount = original.SaleOrderPaymentAmount;
        payment.inEditMode = false;
    };

    $scope.removePayment = function(payment){
                    PaymentService.deletePaymentDetails({ PaymentId: payment.PaymentId , PaymentDetailId : payment.PaymentDetailId},function(){

                        var index = $scope.PaymentDetails.indexOf(payment,0); 
                            
                            if (index != -1 ) $scope.PaymentDetails.splice(index,1) ;   
     
                       
                        $notification.success("Pago", 'El pago se elimino correctamente!');

                    });
                };

}

function PaymentCtrl ($scope,$resource,$http,$location,PaymentService,ngTableParams,$filter,$notification){

    var data =[];
    $scope.customers = [];
    $scope.payments = [];
     $scope.currentPath = $location.path();


    if($scope.currentPath === "/pagos/completados/") {

        PaymentService.completedPayments(function(response){

                data = response;
                $scope.tableParams.total = data.length;
                       
            });
        
      

    }
    else {
            PaymentService.query(function(response){


                data = response;
                $scope.tableParams.total = data.length;
                       
            });
        
    }





    


    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.payments = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);
    

    $scope.$watch('search',function(query){

         $scope.payments = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.payments.length;

    });
    
    $scope.edit = function(payment){
       
        $scope.payment = payment;
    };
    

    $scope.delete = function(payment){

        payment.$remove({ PaymentId : payment.PaymentId },function (){
            
            var index = data.indexOf(payment,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
               
                $notification.success(payment.PaymentTitle, 'El pago se elimino correctamente');
                $scope.tableParams.total = data.length;
            }
            
        },function (){
              $notification.error('Eliminación incorrecta', 'El pago no se pudo eliminar');
        });
    };
    
    
    
    
    
    

    



}