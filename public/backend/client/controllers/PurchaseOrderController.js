
function PurchaseOrderDetailCtrl ($scope,$resource,$http,$location,currentUser,PurchaseOrderService,CurrencyService,SupplierService,ProductService,limitToFilter,$notification,$routeParams,$position){

    $scope.suppliers = [];
    $scope.purchase_orders = [];
    $scope.currencies =[];
    $scope.currentItem = {};

    $scope.userInfo = currentUser;

    $scope.dateOptions = {
        changeYear:true,
        changeMonth:true,
        dateformat: "yy-mm-dd"
    }

    $scope.purchase_order = new PurchaseOrderService({
        PurchaseOrderFolio: "OC-##",
        IsNew : true, 
        PurchaseOrderStatus : "Pendiente",
        PurchaseOrderDate : new Date().yyyymmdd(),
        items: [{ "PurchaseOrderDetailCode" : "", "PurchaseOrderDetailDescripton" : "","PurchaseOrderDetailQTY" : 1, "PurchaseOrderDetailCost" : 0,"PurchaseOrderDetailTax": 0 }],
        deleted_items : [],
        CurrencyId : 2
    });



    $scope.showSupplierModal = function (){
        $scope.showSupplierModal = true;
    };

    if($routeParams.PurchaseOrderId > 0 ){ 
        
        $scope.PurchaseOrderId = $routeParams.PurchaseOrderId;

        PurchaseOrderService.get({PurchaseOrderId:$routeParams.PurchaseOrderId},function(response) {
            $scope.purchase_order = response;
            $scope.purchase_order.deleted_items= [];
            if($scope.purchase_order.supplier){
                $scope.selectedSupplier = $scope.purchase_order.supplier;
            };
        });

    }


    $scope.removePurchaseOrderItem = function(item){

        if(item.IsNew) {

            var index = $scope.purchase_order.items.indexOf(item,0); 
            
            if (index != -1 )
            {

                $scope.purchase_order.items.splice(index,1) ;
                
            }  
        }
        else {

            $scope.purchase_order.deleted_items.push(item);

            var index = $scope.purchase_order.items.indexOf(item,0); 
            
            if (index != -1 )
            {
                $scope.purchase_order.items.splice(index,1) ;
            }  

        }
    };

    $scope.removeProductObject = function(item){
        item.Product = "";
    };

   $scope.onProductSelection = function(item){    
        if(item.Product){

            item.PurchaseOrderDetailCost = item.Product.ProductCost;
            item.PurchaseOrderDetailUM = item.Product.ProductUM;

        }
   };

    $scope.selectAnotherSupplier = function (){
        $scope.selectedSupplier = '';
    };

    $scope.addItem = function (){
        var item = { "PurchaseOrderDetailCode" : "", "PurchaseOrderDetailDescripton" : "","PurchaseOrderDetailQTY" : 1, "PurchaseOrderDetailCost" : 0,"PurchaseOrderDetailTax": 0 , "IsNew" : true };
        $scope.purchase_order.items.push(item);
    };


    var calculateTotals = function(){

        var taxes = 0;
        var subTotal = 0;
        var discounts = 0;

        for(var i=0, len=$scope.purchase_order.items.length; i < len ; i ++)
        {
            var total = $scope.purchase_order.items[i].PurchaseOrderDetailCost * $scope.purchase_order.items[i].PurchaseOrderDetailQTY;
            
            $scope.purchase_order.items[i].PurchaseOrderDetailTotal=total ;

            var discount = total * ( $scope.purchase_order.items[i].PurchaseOrderDetailDiscount / 100);

            $scope.purchase_order.items[i].PurchaseOrderDetailDiscountAmount = discount;

            var tax = total * ( $scope.purchase_order.items[i].PurchaseOrderDetailTax / 100);

            $scope.purchase_order.items[i].PurchaseOrderDetailTaxAmount = tax;

            subTotal += total;

            taxes = taxes + tax ;

            if($scope.purchase_order.items[i].PurchaseOrderDetailDiscount > 0){
               discounts = discounts + discount;                 
            }
            
        };

        $scope.purchase_order.PurchaseOrderSubtotal = subTotal;
        $scope.purchase_order.PurchaseOrderTaxes = taxes;
        $scope.purchase_order.PurchaseOrderDiscounts = discounts;
        $scope.purchase_order.PurchaseOrderTotal = $scope.purchase_order.PurchaseOrderSubtotal +  $scope.purchase_order.PurchaseOrderTaxes - $scope.purchase_order.PurchaseOrderDiscounts ;
        
        if($scope.purchase_order.PurchaseOrderExchangeRate){
            
            if($scope.purchase_order.PurchaseOrderExchangeRate > 0){
                if($scope.purchase_order.CurrencyId == 1) {
                    $scope.purchase_order.PurchaseOrderTotalDlls = $scope.purchase_order.PurchaseOrderTotal/$scope.purchase_order.PurchaseOrderExchangeRate;
                }else{
                    $scope.purchase_order.PurchaseOrderTotalDlls = $scope.purchase_order.PurchaseOrderTotal * $scope.purchase_order.PurchaseOrderExchangeRate;
                }
                
            }

        }        
    };

    //temporalmente deshabilitado para no marcar errores
    $scope.$watch('purchase_order.items', calculateTotals , true);

    $scope.$watch('purchase_order.PurchaseOrderExchangeRate',calculateTotals,true);
 
    $scope.$watch('purchase_order.CurrencyId', calculateTotals,true);

    /// Start Interface && controls handling

    $scope.showItemExtendEditModal = function(item){

        $scope.showEditModal = true;
        $scope.editPurchaseOrderItem = item;

    };

    $scope.hideItemExtendEditWindow = function(){
        $scope.showEditModal = false;
    };

     $scope.onNewProductSelection = function(item){
        $scope.currentItem = item;
        $scope.product = new ProductService({ ProductName : item.Product});
             
        $scope.showProductModal = true;
     };

    $scope.selectProduct = function(item){
        
        $scope.currentItem.Product = $scope.product;
         $scope.onProductSelection($scope.currentItem);  
        $scope.showProductModal = false;
    }; 

    $scope.onItemNotFound = function(item){

        item.notFound = true;
        item.IsManualItem = false;
        
    };

    $scope.onItemMatchesFound = function(item){
        item.notFound = false;
    };
    
    $scope.itIsManualItem = function(item){
        return item.IsManualItem && item.Product.length > 0;
    };

    $scope.setAsManualItem = function(item){
        item.IsManualItem = true;
    };

    

    /// End Interface && controls handling

    ///--> Service API Calls Start 
    
    $scope.savePurchaseOrder = function() {

        if($scope.selectedSupplier){
          
          if($scope.selectedSupplier.SupplierId){

            $scope.purchase_order.SupplierId = $scope.selectedSupplier.SupplierId;
           
          };

        }

        $scope.purchase_order.$save({ PurchaseOrderId : $scope.purchase_order.PurchaseOrderId} , function (){
                $notification.success($scope.purchase_order.PurchaseOrderFolio, 'La Orden de Entrada se guardo correctamente');
        });
    };

    $scope.delete = function(){

        if($scope.purchase_order.PurchaseOrderDeleteWord === "ELIMINAR"){

            $scope.purchase_order.$remove({ PurchaseOrderId : $scope.purchase_order.PurchaseOrderId },function (){
                
                
                    $scope.showRemoveModal = false;
                    $notification.success('Eliminación correcta', 'La Orden de Entrada se elimino correctamente');
                    $location.path("/compras/");
                
                
            },function (response){

                
                  $notification.error('Eliminación incorrecta', 'La Orden de Entrada no se pudo eliminar');
                
            });
        }    
        else {
            $notification.error('Captura incorrecta', 'Intente capturar nuevamente');
        }    
    };


   
    $scope.cancelPurchaseOrder = function () {
        
        if($scope.purchase_order.PurchaseOrderStatus != 'Cancelada'){
            
            if(confirm("Tambien se cancelaran las existencias ingresadas, desea continuar?")){

             

                $scope.purchase_order.$cancelPurchaseOrder({PurchaseOrderId : $scope.purchase_order.PurchaseOrderId },function(response){
                    
                    $notification.success("Orden de Entrada cancelada","La Orden de Entrada fue cancelada");

                },function(response){
                    $notification.error("Error al cancelar","La Orden de Entrada no pudo ser cancelada");
                });
            };
        }
        else
        {
            $notification.error("Error al cancelar","La Orden de Entrada ya esta cancelada");
        };

    };


    $scope.removePurchaseOrder = function(purchase_order){
        if(purchase_order.PurchaseOrderStatus !== "Aprobada"){
            $scope.showRemoveModal = true;
        }
        else{
            $notification.error("Error al eliminar","No se puede eliminar una Orden de Entrada Aprobada")
        }
    }


    $scope.approvePurchaseOrder = function () {

        if($scope.purchase_order.PurchaseOrderStatus != 'Aprobada'){

            $scope.purchase_order.PurchaseOrderStatus = 'Aprobada';

            $scope.purchase_order.$approvePurchaseOrder({PurchaseOrderId : $scope.purchase_order.PurchaseOrderId },function(response){

                $notification.success("Orden de Compra aprobada","La orden de compra se aprobo correctamente");

            });
        }
        else
        {
            $notification.error("Error al aprobar","La orden de compra ya ha sido aprobada anteriormente");
        }
    };

    $scope.supplierSearch = function (viewValue) {
        return $http.get('api/suppliers/search/'+viewValue).then(function(response){
            return limitToFilter(response.data,15);
        });
    };

 

    $scope.productSearch = function (viewValue) {
        return $http.get('api/products/search/'+viewValue).then(function(response){
            return limitToFilter(response.data, 15);
        });
    };

    CurrencyService.query(function(response){
        $scope.currencies = response;
    });


    ProductService.unidades(function(response){

        $scope.unidades = response;
               
    });
    

    /// < Service API calls End 

}



function PurchaseOrderCtrl ($scope,$resource,$location,$http,PurchaseOrderService,SupplierService,CurrencyService,ngTableParams,$filter,$notification){

	var data =[];
	$scope.suppliers = [];
	$scope.purchase_orders = [];
    $scope.currentPath = $location.path();


    if($scope.currentPath === "/compras/procesadas") {
       
        PurchaseOrderService.procesadas(function(response){
             data = response;
            $scope.tableParams.total = data.length;
        },function(response){
           
        });        

    }
    else {
        PurchaseOrderService.query(function(response){
        data = response;
        $scope.tableParams.total = data.length;
               
        },function(response){
           //Bad request handler
            $notification.error("Error","");
        });
    }






	

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.purchase_orders = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.purchase_orders = $filter("filter")(data, query);

         $scope.tableParams.total = $scope.purchase_orders.length;
         $scope.tableParams.count = 100;

    });


}






