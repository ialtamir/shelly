'use strict';

function StockCtrl($scope,$filter,StockService,currentUser,ngTableParams,$notification) {

	var data = [];
    
	$scope.stock = [];
    $scope.userInfo = currentUser;

	StockService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.stocks = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.stock = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.stocks.length;

    });

    $scope.refresh = function(){
        StockService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };


  

}