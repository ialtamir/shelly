function CalendarCtrl($scope,$http,limitToFilter,CalendarService,EventService,CustomerService,EmployeeService,$notification){

  
  /*/*/
 var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    $scope.events = [];
    $scope.visits = [];
    $scope.eventSource ={};
    $scope.eventSources2 = {};

    $scope.customers = [];
    $scope.employees = [];


    $scope.dateOptions = {
        changeYear:true,
        changeMonth:true,
        dateformat: "yy-mm-dd"
    }

    $scope.customerSearch = function (viewValue) {
        return $http.get('api/customers/search/'+viewValue).then(function(response){
            return limitToFilter(response.data,15);
        });
    };
    

    CalendarService.query(function(response){
    	//console.log(response);

    	if(response.length){
    		for(var i=0, len=response.length; i < len ; i ++)
  			{
  				var evt = response[i];

  				$scope.events.push({ title: evt.title, start: new Date(evt.year, evt.month, evt.day),url:evt.url});
  			}
    	}


    });



    EventService.query(function(response){
      //console.log(response);

      if(response.length){
          for(var i=0, len=response.length; i < len ; i ++)
          {
            var evt = response[i];

            $scope.visits.push({ id: evt.EventId, title: evt.EventTitle, start: new Date(evt.year, evt.month, evt.day),type:'agenda'});

          }
      }

      // $scope.eventSources2.push($scope.visits);
       //$apply();
    });

    CustomerService.query(function(response){
      $scope.customers = response;
    });

    EmployeeService.query(function(response){
      $scope.employees = response;
    });


    /* event source that pulls from google.com */
    /*$scope.eventSource = {
            url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago' // an option!
    };*/


    /* event source that contains custom events on the scope */
   $scope.visits= [
      {
        title: 'Open Sesame',
        start: new Date(y, m, 28),
        end: new Date(y, m, 29),
        className: ['openSesame']
      }
    ];


    /* event source that calls a function on every view switch */
    $scope.eventsF = function (start, end, callback) {
     /* var s = new Date(start).getTime() / 1000;
      var e = new Date(end).getTime() / 1000;
      var m = new Date(start).getMonth();
      var events = [];//[{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
      callback(events);*/
    };

    $scope.calEventsExt = {
       color: '#f00',
       textColor: 'yellow',
       events: [ 
          {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
        ]
    };
    /* alert on eventClick */
    $scope.alertOnEventClick = function( event, allDay, jsEvent, view ){

        $scope.alertMessage = (event.title + ' was clicked ');

        if(event.type === "agenda"){
        

          EventService.get({EventId:event.id},function(response) {
            $scope.event = response;
            $scope.showEventModal = true;
          });


          
        
        }
    };
    /* alert on Drop */
     $scope.alertOnDrop = function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view){
       $scope.alertMessage = ('Event Droped to make dayDelta ' + dayDelta);
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ){
       $scope.alertMessage = ('Event Resized to make dayDelta ' + minuteDelta);
    };
    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function(sources,source) {
      var canAdd = 0;
      angular.forEach(sources,function(value, key){
        if(sources[key] === source){
          sources.splice(key,1);
          canAdd = 1;
        }
      });
      if(canAdd === 0){
        sources.push(source);
      }
    };
    /* add custom event*/
    $scope.addEvent = function() {
      $scope.events.push({
        title: 'Open Sesame',
        start: new Date(y, m, 28),
        end: new Date(y, m, 29),
        className: ['openSesame']
      });
    };
    /* remove event */
    $scope.remove = function(index) {
      $scope.events.splice(index,1);
    };
    /* Change View */
    $scope.changeView = function(view,calendar) {
      //  calendar.fullCalendar('changeView',view);
    };
    /* Change View */
    $scope.renderCalender = function(calendar) {
      calendar.fullCalendar('render');
    };

    $scope.alertEventOnDayClick = function(date){
        $scope.event = new EventService({IsNew:true});
        $scope.event.day = date.getDate();
        $scope.event.month = date.getMonth();
        $scope.event.year  = date.getFullYear();

        $scope.event.EventDate = $scope.event.year + '-'+($scope.event.month+1)+'-'+$scope.event.day ;
        $scope.showEventModal = true;
        $scope.$apply();
    };


    /* config object */
    $scope.uiConfig = {
      calendar:{
        height: 450,
        editable: true,
        header:{
          left: 'title',
          center: '',
          right: 'today prev,next'
        },
        dayClick: $scope.alertEventOnDayClick,
        eventClick: $scope.alertOnEventClick,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize
      }
    };

    $scope.uiConfig.calendar.dayNames = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
    $scope.uiConfig.calendar.dayNamesShort = ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"];

    
    /* event sources array*/
    $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
    $scope.eventSources2 = [$scope.visits, $scope.eventSource, $scope.eventsF];


    $scope.cancel = function(){
      $scope.showEventModal = false;
    }

    $scope.save = function(){

      $scope.event.$save({EventId: $scope.event.EventId }, function(response){

        $notification.success('Evento guardado','Se guardo correctamente');
          if($scope.event.IsNew){

             $scope.visits.push({ id: $scope.event.EventId, title: $scope.event.EventTitle, start: new Date($scope.event.year,$scope.event.month, $scope.event.day),type:'agenda'});

             $scope.showEventModal = false;
          }
      });
    }
   
}