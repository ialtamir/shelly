function AppCtrl($scope, $location, $resource, $rootScope,i18nNotifications,localizedMessages,AccountService,currentUser)
{
  
    $scope.notifications = i18nNotifications;
    $scope.configuration = { CompanyName : "Bienvenido..." };
   // $scope.userInfo = currentUser;
    
    //console.log($scope.userInfo.information());


    AccountService.configuration(function(response){
        console.log(response);
        
        $scope.configuration = response;
    });
   

    $scope.$on('$routeChangeError', function(event, current, previous, rejection){

      i18nNotifications.pushForCurrentRoute('errors.route.changeError', 'error', {}, {rejection: rejection});
    
    });

    $scope.account = function (){
      //return $scope.userInfo.information().account;
    };

    $scope.$location = $location;

    $scope.$watch('$location.path()', function(path) {
        return $scope.activeNavId = path || '/';
    });
    
    return $scope.getClass = function(id,completeRoute) {

      if($scope.activeNavId != undefined ){

          if(completeRoute){
            
            if ($scope.activeNavId === id) {
              return 'active';
            } 
            else {
                return '';
            }
          }
          else{

            if ($scope.activeNavId.substring(0, id.length) === id) {
              return 'active';
            } 
            else {
              return '';
            }
          }
        }
      };
    }