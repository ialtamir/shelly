'use strict';

function SupplierCtrl($scope,$filter,SupplierService,currentUser,ngTableParams,$notification) {

	var data = [];
    
	$scope.supplier = { IsNew : true };
	$scope.suppliers = [];
    $scope.userInfo = currentUser;

	SupplierService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.suppliers = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.suppliers = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.suppliers.length;

    });

    $scope.refresh = function(){
        SupplierService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };


    $scope.add = function(){
        
        $scope.supplier = new SupplierService();
    	$scope.supplier.IsNew = true;

     	$scope.showSupplierModal = true;
         
    };

    $scope.edit = function(user){
       
    	$scope.supplier = user;
        $scope.showSupplierModal = true;
        
    };

    $scope.cancel = function(){
        
        $scope.supplier = {};
        $scope.showSupplierModal = false;

    };

    $scope.save = function(){

        $scope.supplier.$save({ SupplierId:$scope.supplier.SupplierId },function(){

        if($scope.supplier.SupplierId   >   0   &&  $scope.supplier.IsNew){
               
            data.push($scope.supplier);

            $notification.success($scope.supplier.SupplierName, 'El Proveedor se guardo correctamente');
            
            $scope.supplier = {};
            $scope.tableParams.total = data.length;
        }
            
    	$scope.showSupplierModal = false;
            
        },function(){        	 
             $notification.error('Guardado incorrecto', 'El Proveedor no se pudo guardar');
        });
        
    };

    $scope.delete = function(supplier){

        supplier.$remove({ SupplierId : supplier.SupplierId },function (){
            
            var index = data.indexOf(supplier,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
                $notification.success($scope.supplier.SupplierName, 'El Proveedor se elimino correctamente');
                
                $scope.tableParams.total = data.length;
            }
            
        },function (){
             
     
              $notification.error('Eliminación incorrecta', 'El Proveedor no se pudo eliminar');
        });
    };


}