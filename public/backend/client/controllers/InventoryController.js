function InventoryDetailCtrl ($scope,$http,$routeParams,$filter,InventoryService,limitToFilter,$resource,$notification,ngTableParams,ProductService){

   $scope.inventory = new InventoryService({});
 

   var data = [];
   
   	$scope.inventories = [];
    
   InventoryService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });

   
    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.inventories = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);
	
	$scope.$watch('search',function(query){

         $scope.inventories = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.inventories.length;

    });

    $scope.refresh = function(){
        InventoryService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };
	
 	
	if($routeParams.InventoryId > 0 ){ 
        
        $scope.inventory.InventoryId = $routeParams.InventoryId;

    	InventoryService.get({InventoryId:$routeParams.InventoryId},function(response) {
    		$scope.inventory = response;
        });

   	}
	 
        ///--> Service API Calls Start 
    
    
	
    $scope.inventory = new InventoryService({
        IsNew : true, 
        InventoryDate : new Date()
        
    });


    ProductService.unidades(function(response){

        $scope.unidades = response;
               
    });

 
    $scope.productSearch = function (viewValue) {
        return $http.get('api/products/search/'+viewValue).then(function(response){
            return limitToFilter(response.data, 15);
        });
    };

    $scope.selectAnotherProduct = function (){
		$scope.selectedProduct = '';
	};


   

}


function InventorySummaryCtrl($scope,$http,$routeParams,$filter,InventoryService,limitToFilter,$resource,$notification,ngTableParams,CategoryService){
    
    $scope.InventoryDetails = [];
    $scope.inventoryDetail = { IsNew : true };

    if($routeParams.InventoryId > 0 ){ 
        
        $scope.InventoryId = $routeParams.InventoryId;

        InventoryService.get({InventoryId:$routeParams.InventoryId},function(response) {
            $scope.inventory = response;

            InventoryService.inventoryDetails({InventoryId:$scope.InventoryId },function(response){
                $scope.InventoryDetails = response;
            });
        });

    }

   
    

    
 
}



function InventoryCtrl($scope,$filter,InventoryService,PlaceService,currentUser,ngTableParams,$notification) {
	
	var data = [];
	
	$scope.unidades=[];
	$scope.inventory = { IsNew : true };
	$scope.inventories = [];
	$scope.userInfo = currentUser;
	$scope.places =[];
	
	
	PlaceService.query(function(response){
		$scope.places = response;
	});
	
	
	InventoryService.query(function(response){

		data = response;
		$scope.tableParams.total = data.length;
			   
	});

	$scope.tableParams = new ngTableParams({
		   
		page: 1,            // show first page
		total: data.length , // length of data
		count: 10          // count per page

	});

	$scope.$watch('tableParams', function(params) {
		// slice array data on pages

		$scope.inventories = data.slice(
			( params.page - 1) * params.count,
			params.page * params.count
		);

	}, true);

	$scope.$watch('search',function(query){

		 $scope.inventories = $filter("filter")(data, query);
		 $scope.tableParams.total = $scope.inventories.length;

	});

	$scope.refresh = function(){
		InventoryService.query(function(response){

			data = response;
			$scope.tableParams.total = data.length;
			$scope.search = '';

		});
	};


	$scope.add = function(){
		
		$scope.inventory = new InventoryService();
		$scope.inventory.IsNew = true;
		$scope.showInventoryModal = true;
		 
	};

	$scope.cancel = function(){
		
		$scope.inventory = {};
		$scope.showInventoryModal = false;

	};

	$scope.save = function(){

		$scope.inventory.$save({ InventoryId:$scope.inventory.InventoryId },function(){

		if($scope.inventory.InventoryId   >   0   &&  $scope.inventory.IsNew){
			   
			data.push($scope.inventory);

			$notification.success($scope.inventory.InventoryId, 'El Folio de inventario se guardo correctamente');
			
			$scope.inventory = {};
			$scope.tableParams.total = data.length;
		}
			
		$scope.showInventoryModal = false;
			
		},function(){        	 
			 $notification.error('Funcion incorrecta', 'El inventario no se pudo guardar');
		});
		
	};

	$scope.delete = function(inventory){

		inventory.$remove({ InventoryId : inventory.InventoryId },function (){
			
			var index = data.indexOf(inventory,0); 
			
			if (index != -1 )
			{

				data.splice(index,1) ;
				$notification.success($scope.inventory.InventoryId, 'El inventario se elimino correctamente');
				
				$scope.tableParams.total = data.length;
			}
			
		},function (){
			 
	 
			  $notification.error('Eliminación incorrecta', 'El inventario no se pudo eliminar');
		});
	};
	
	$scope.openInventoryCode = function(inventory){
		inventory.$openCode({ InventoryId : inventory.InventoryId},function(){
				$notification.success( inventory.InventoryCode, 'El Inventario Fisico se ha iniciado correctamente');
			},
			function(){

			}
		);
	};
	
	$scope.reopenInventoryCode = function(inventory){
		inventory.$openCode({ InventoryId : inventory.InventoryId},function(){
				$notification.success( inventory.InventoryCode, 'El Inventario Fisico se ha iniciado correctamente');
			},
			function(){

			}
		);
	};
	
	$scope.closeInventoryCode = function(inventory){
		inventory.$closeCode({ InventoryId : inventory.InventoryId},function(){
				$notification.succsess( inventory.InventoryCode, 'El Inventario Fisico se ha cerrado correctamente');
			},
			function(){

			}
		);
	};


}



