'use strict';

function UserCtrl($scope,$filter,UserService,PlaceService,ngTableParams,i18nNotifications,$notification) {
     
    var data = [];

	$scope.user = { UserIsNew : true };
	$scope.users = [];
    $scope.selectedUser = {};

    UserService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });
    
    PlaceService.query(function(response){
        $scope.places = response;
    });

    
    


    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.users = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.users = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.users.length;

    });


    $scope.refresh = function(){
        UserService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };

    $scope.add = function(){
        
        $scope.user = new UserService();
        $scope.user.UserIsAdmin = false;
    	$scope.user.UserIsNew = true;
     	$scope.showModal = true;
         
    };

    $scope.edit = function(user){
        
    	$scope.user = user;
        $scope.showModal = true;
        
    };

    $scope.cancel = function(){
        
        $scope.user = {};
        $scope.showModal = false;
        $scope.showPasswordModal = false;     

    };

    $scope.editPassword = function(user){

    	$scope.user = user;
    	$scope.showPasswordModal = true;

    }

    $scope.save = function(){

        $scope.user.$save({ UserId:$scope.user.UserId   },function(){

        if($scope.user.UserId   >   0   &&  $scope.user.UserIsNew){
               
            data.push($scope.user);
           
            $notification.success($scope.user.UserName , 'El usuario se guardo correctamente');
            $scope.user = {};
            $scope.tableParams.total = data.length;
        }
            
    	$scope.showModal = false;
            
        },function(){
        	 $notification.error($scope.user.UserName , 'Ocurrio un error al tratar de guardar el usuario');
        });
        
    };

    $scope.savePassword = function (){
        $scope.user.$savePassword({ UserId:$scope.user.UserId },function(){
            $scope.showPasswordModal = false;
            
             $notification.success($scope.user.UserName , 'El password se guardo correctamente');
        });
    };
    
    $scope.delete = function(user){

    	user.$remove({ UserId : user.UserId },function (){
            
            var index = data.indexOf(user,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
            
                $notification.success(user.UserName , 'El usuario se elimino se guardo correctamente');
                $scope.tableParams.total = data.length;
            }
            
    	},function (){
             i18nNotifications.pushForCurrentRoute('crud.user.remove.error', 'error', {name : user.UserName  });
             $notification.success('Eliminacion incorrecta' , 'Ocurrio un error al tratar de eliminar el usuario');
        });
    };

    $scope.showScopes = function(user){
        
        angular.copy(user, $scope.selectedUser ); 
        $scope.showUserScopesModal = true;

        UserService.userScopes({ UserId : $scope.selectedUser.UserId },function(response){
            $scope.selectedUser.user_scopes = response;
        });
    };

    $scope.saveScopes = function(){
        if($scope.selectedUser){
            UserService.saveScopes({ UserId : $scope.selectedUser.UserId }, $scope.selectedUser ,function(response){
                $notification.success("Permisos","Los permisos fueron guardados correctamente");
            });
        }
    };

    $scope.hideScopes = function(user){
        $scope.selectedUser = {};
        $scope.showUserScopesModal = false;
    };

    
    
}