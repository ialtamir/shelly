function EmployeeCtrl($scope,$filter,EmployeeService,currentUser,ngTableParams,$notification){

	var data = [];
    
	$scope.employee = { IsNew : true };
	$scope.employees = [];
    $scope.userInfo = currentUser;
	
	EmployeeService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });
	
	  $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });
	
	 $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.employees = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);
	
	
	$scope.$watch('search',function(query){

         $scope.employees = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.employees.length;

    });
	
	$scope.refresh = function(){
        EmployeeService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };
	
	 $scope.add = function(){
        
        $scope.employee = new EmployeeService();
    	$scope.employee.IsNew = true;

     	$scope.showEmployeeModal = true;
         
    };
	
	 $scope.edit = function(user){
       
    	$scope.employee = user;
        $scope.showEmployeeModal = true;
        
    };
	
	 $scope.cancel = function(){
        
        $scope.employee = {};
        $scope.showEmployeeModal = false;

    };
	
	 $scope.save = function(){

        $scope.employee.$save({ EmployeeId:$scope.employee.EmployeeId },function(){

        if($scope.employee.EmployeeId   >   0   &&  $scope.employee.IsNew){
               
            data.push($scope.employee);

            $notification.success($scope.employee.EmployeeName, 'El empleado se guardo correctamente');
            
            $scope.employee = {};
            $scope.tableParams.total = data.length;
        }
            
    	$scope.showEmployeeModal = false;
            
        },function(){        	 
             $notification.error('Funcion incorrecta', 'El empleado no se pudo guardar');
        });
        
    };
	
	
	 $scope.delete = function(employee){

        employee.$remove({ EmployeeId : employee.EmployeeId },function (){
            
            var index = data.indexOf(employee,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
                $notification.success($scope.employee.EmployeeName, 'El empleado se elimino correctamente');
                
                $scope.tableParams.total = data.length;
            }
            
        },function (){
             
     
              $notification.error('Eliminación incorrecta', 'El empleado no se pudo eliminar');
        });
    };
	

}