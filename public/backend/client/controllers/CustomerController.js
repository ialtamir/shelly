'use strict';

function CustomerCtrl($scope,$filter,CustomerService,currentUser,ngTableParams,$notification) {

	var data = [];
    
	$scope.customer = { IsNew : true };
	$scope.customers = [];
    $scope.userInfo = currentUser;

	CustomerService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.customers = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.customers = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.customers.length;
         $scope.tableParams.page = 1;

    });

    $scope.refresh = function(){
        CustomerService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };


    $scope.add = function(){
        
        $scope.customer = new CustomerService();
    	$scope.customer.IsNew = true;

     	$scope.showCustomerModal = true;
         
    };

    $scope.edit = function(user){
       
    	$scope.customer = user;
        $scope.showCustomerModal = true;
        
    };

    $scope.cancel = function(){
        
        $scope.customer = {};
        $scope.showCustomerModal = false;

    };

    $scope.save = function(){

        $scope.customer.$save({ CustomerId:$scope.customer.CustomerId },function(){

        if($scope.customer.CustomerId   >   0   &&  $scope.customer.IsNew){
               
            data.push($scope.customer);

            $notification.success($scope.customer.CustomerName, 'El cliente se guardo correctamente');
            
            $scope.customer = {};
            $scope.tableParams.total = data.length;
        }
            
    	$scope.showCustomerModal = false;
            
        },function(){        	 
             $notification.error('Informacion incorrecta', 'El cliente no se pudo guardar');
        });
        
    };

    $scope.delete = function(customer){

        customer.$remove({ CustomerId : customer.CustomerId },function (){
            
            var index = data.indexOf(customer,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
                $notification.success($scope.customer.CustomerName, 'El cliente se elimino correctamente');
                
                $scope.tableParams.total = data.length;
            }
            
        },function (){
             
     
              $notification.error('Eliminación incorrecta', 'El cliente no se pudo eliminar');
        });
    };

    $scope.editContacts = function(customer){
        $scope.customer = customer;
        $scope.CustomerId = customer.CustomerId;
        $scope.showCustomerContactModal = true;
    };


}

function CustomerDetailCtrl($scope,$filter,CustomerService,currentUser,$notification,$routeParams,$parse,$http, $timeout, $upload){
    
 
    $scope.fileReaderSupported = window.FileReader != null;
    $scope.uploadRightAway = true;
    $scope.howToSend = 1;
    $scope.simpleThumb = 'https://placehold.it/160x120';
    $scope.customer = new CustomerService({
        CustomerId : 0,  
		IsNew : true,
		CustomerPicture : 'https://placehold.it/160x120',
		CustomerRating : 5
    });
    
    if($routeParams.CustomerId > 0)
    {
        CustomerService.get({CustomerId : $routeParams.CustomerId},function(response){
            $scope.customer = response;
            $scope.customer.deleted_pictures = [];
        });
    }

    $scope.hasUploader = function(index) {
        return $scope.upload[index] != null;
    };
    $scope.abort = function(index) {
        $scope.upload[index].abort(); 
        $scope.upload[index] = null;
    };

    $scope.onFileSelect = function($files) {
        $scope.selectedFiles = [];
        $scope.progress = [];

        if ($scope.upload && $scope.upload.length > 0) {
            for (var i = 0; i < $scope.upload.length; i++) {
                if ($scope.upload[i] != null) {
                    $scope.upload[i].abort();
                }
            }
        }

        $scope.upload = [];
        $scope.uploadResult = [];
        $scope.selectedFiles = $files;
        $scope.dataUrls = [];


        for ( var i = 0; i < $files.length; i++) {
            var $file = $files[i];
            if (window.FileReader && $file.type.indexOf('image') > -1) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL($files[i]);
                function setPreview(fileReader, index) {
                    fileReader.onload = function(e) {
                        $timeout(function() {
                            $scope.dataUrls[index] = e.target.result;
                        });
                    }
                }
                setPreview(fileReader, i);
            }
            $scope.progress[i] = -1;
            if ($scope.uploadRightAway) {
                $scope.start(i);
            }
        }
    };


    $scope.start = function(index) {
        $scope.progress[index] = 0;
        if ($scope.howToSend == 1) {
            $scope.upload[index] = $upload.upload({
                url : '/api/customers/' + $scope.customer.CustomerId + '/upload',
                method: $scope.httpMethod,
                headers: {'myHeaderKey': 'myHeaderVal'},
                data : {
                    myModel : $scope.myModel
                },
                /* formDataAppender: function(fd, key, val) {
                    if (angular.isArray(val)) {
                        angular.forEach(val, function(v) {
                          fd.append(key, v);
                        });
                      } else {
                        fd.append(key, val);
                      }
                }, */
                file: $scope.selectedFiles[index],
                fileFormDataName: 'myFile'
            }).then(function(response) {
                $scope.uploadResult.push(response.data.result);
                //alert(response.data.url);
                $scope.customer.CustomerPicture = response.data.url;
            }, null, function(evt) {
                $scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
            });
        } else {
            var fileReader = new FileReader();
            fileReader.readAsArrayBuffer($scope.selectedFiles[index]);
            fileReader.onload = function(e) {
                $scope.upload[index] = $upload.http({
                    url: '/api/customers/' + $scope.customer.CustomerId + '/upload',
                    headers: {'Content-Type': $scope.selectedFiles[index].type },
                    data: e.target.result
                }).then(function(response) {
                    $scope.uploadResult.push(response.data.result);
                    //alert(response.data.url);
                    $scope.customer.CustomerPicture = response.data.url;
                }, null, function(evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        }
    }


	
	
	 $scope.saveCustomer = function(){

		        $scope.customer.$save({ CustomerId:$scope.customer.CustomerId },function(){

			        	//if($scope.customer.CustomerId > 0 &&  $scope.customer.IsNew){

			            	$notification.success($scope.customer.CustomerName, 'El cliente se guardo correctamente');	
			            	//$scope.selectedCustomer = $scope.customer;
			            	//onAddCallback( $scope, { $customer: $scope.customer });
			        	//}
		            
			    		//$scope.showCustomerModal = false;
			        	
			        },
			        function(){        	 
			             $notification.error('Eliminación', 'El cliente no se pudo guardar');
			       	});		       
	};

    
   /* 
    
    $scope.showCustomerModal = false;  
			$scope.customerAdded = {};
			
			var onAddCallback  = $parse($attrs.customerOnAdd);

			$scope.clearCustomerForm = function() {
        		$scope.customer = {};
      		};

      		$scope.addCustomer = function(){
        
        		$scope.customer = new CustomerService();
    			$scope.customer.IsNew = true;
     			$scope.showCustomerModal = true;  

   			 };

		    $scope.editCustomer = function(user){
		       
		    	$scope.customer = user;
		        $scope.showCustomerModal = true;
		        
		    };

		    $scope.cancelCustomer = function(){
		        
		        $scope.customer = {};
		        $scope.showCustomerModal = false;

		    };

		    $scope.saveCustomer = function(){

		        $scope.customer.$save({ CustomerId:$scope.customer.CustomerId },function(){

			        	if($scope.customer.CustomerId > 0 &&  $scope.customer.IsNew){

			            	$notification.success($scope.customer.CustomerName, 'El cliente se guardo correctamente');	
			            	$scope.selectedCustomer = $scope.customer;
			            	onAddCallback( $scope, { $customer: $scope.customer });
			        	}
		            
			    		$scope.showCustomerModal = false;
			        	
			        },
			        function(){        	 
			             $notification.error('Eliminación incorrecta', 'El cliente no se pudo guardar');
			       	});		       
		    };*/
}

function CustomerMapCtrl($scope,$filter,CustomerService,currentUser,$notification,$routeParams,$parse,$http, $timeout, $upload,NgMap){
    
    
  

    $scope.readyForMap = false;

    $scope.customer = new CustomerService({
        
		IsNew : true,
	
		CustomerRating : 5
    });
    
    
    
     NgMap.getMap().then(function(map) {
         map.getCenter();

     $scope.customer.CustomerLatitude=map.markers[0].position.lat();
     $scope.customer.CustomerLongitude=map.markers[0].position.lng();
     
     
    });
    
    if($routeParams.CustomerId > 0)
    {
        CustomerService.get({CustomerId : $routeParams.CustomerId},function(response){
            
            
            $scope.customer = response;
          
            
            $scope.readyForMap = true;
        });
    }
    
    
  
  
  
   $scope.marker=function(e){
       $scope.customer.CustomerLatitude=e.latLng.lat();
       $scope.customer.CustomerLongitude=e.latLng.lng();
   }
   
   
    $scope.saveCustomer = function(){


        $scope.customer.$saveAddress({ CustomerId:$scope.customer.CustomerId },function(){

	            	$notification.success($scope.customer.CustomerName, 'El cliente se guardo correctamente');	
	 
	        	
	        },
	        function(){        	 
	             $notification.error('Error al Guardar', 'El cliente no se pudo guardar');
	       	});		       
	};
    
}