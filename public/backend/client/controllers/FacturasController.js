

function FacturasCtrl($scope,$resource,$http,$location,$notification,tokenHandler,SaleOrderService,ngTableParams,$filter,$notification){

	var data =[];
	$scope.customers = [];
	$scope.sale_orders = [];
    $scope.currentPath = $location.path();
    $scope.currentTokenInfo = tokenHandler.info();
    
    $scope.paquetes = [
        {paq: 'PAQ50', data: 'Paquete   50 - MXN $250 + IVA ' },
        {paq: 'PAQ200', data: 'Paquete   200 - MXN $900 + IVA ' },
        {paq: 'PAQ500', data: 'Paquete   500 - MXN $2000 + IVA ' },
        {paq: 'PAQ1000', data: 'Paquete   1000 - MXN $3500 + IVA ' }
        ];

    SaleOrderService.facturadas(function(response){
        
        data = response;
        $scope.tableParams.total = data.length;
                   
    });
    
    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.sale_orders = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.sale_orders = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.sale_orders.length;

    });
    
    $scope.delete = function(sale_order){

          //Grafica de ventas
        $http.get('/api/facturar/delete/'+sale_order.SaleOrderId).then(function(response){
            var saleOrderCharData = response.data;

            $notification.success('Cancelacion corecta',"Se cancelo correctamente");
            
         },function (response){
             
             $notification.success('Error al cancelar',response.data.error);
             
         });
    };
    
     $scope.add = function(){
        $scope.Total = 'PAQ50';
     	$scope.showPagoModal = true;
         
    };
    
    $scope.cancel = function(){
        
        $scope.employee = {};
        $scope.showPagoModal = false;

    };
    
    $scope.pagar = function(){
        
        $scope.payment_errors = [];
         
        Conekta.setPublishableKey('key_OYxgE4DKo438t1zczR7LDzw');
        
        
        var $form = $("#card-form");
        /* Previene hacer submit más de una vez */
 
        $scope.PaymentInProcess = true;
            


        
        Conekta.token.create($form, function(token){
            
            $http.post('/api/pago/'+$scope.Total+'/'+token.id+'/').then(function(response){
               
                $notification.success('Pago corecto',"Se realizo el pago correctamente");
                $scope.showPagoModal = false;
                
            },function (response){
                
                $notification.error('Error al processar el pago',response.data.error);
                
            });
            
        }, function(response){
            
            $scope.payment_errors.push({ error_description: response.message_to_purchaser });
             $scope.PaymentInProcess = false;
             $scope.$apply();
        });
        
    }

    

}