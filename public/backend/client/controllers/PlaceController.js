'use strict';

function PlaceCtrl($scope,$filter,PlaceService,currentUser,ngTableParams,$notification) {

	var data = [];
    
	$scope.place = { IsNew : true };
	$scope.places = [];
    $scope.userInfo = currentUser;

	PlaceService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.places = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.place = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.places.length;

    });

    $scope.refresh = function(){
        PlaceService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };


    $scope.add = function(){
        
        $scope.place = new PlaceService();
    	$scope.place.IsNew = true;

     	$scope.showModal = true;
         
    };

    $scope.edit = function(user){
       
    	$scope.place = user;
        $scope.showModal = true;
        
    };

    $scope.cancel = function(){
        
        $scope.place = {};
        $scope.showModal = false;

    };

    $scope.save = function(){

        $scope.place.$save({ PlaceId:$scope.place.PlaceId },function(){

        if($scope.place.PlaceId   >   0   &&  $scope.place.IsNew){
               
            data.push($scope.place);
			 $notification.success($scope.place.PlaceName, 'La Sucursal se guardo correctamente');
		
            $scope.place = {};
            $scope.tableParams.total = data.length;
        }
            
    	$scope.showModal = false;
            
        },function(){
        	  $notification.error('Funcion incorrecta', 'La Sucursal no se pudo guardar');
        });
        
    };

    $scope.delete = function(place){

        place.$remove({ PlaceId : place.PlaceId },function (){
            
            var index = data.indexOf(place,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
             	$notification.success($scope.place.PlaceName, 'La Sucursal se elimino correctamente');
                $scope.tableParams.total = data.length;
            }
            
        },function (){
             $notification.error('Funcion incorrecta', 'La Sucursal no se pudo eliminar');
        });
    };


}