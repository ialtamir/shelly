function TransactionCtrl($scope,$http,$routeParams){


	//$scope.quote = {};


	if($routeParams.TransactionId){

		$http.get('api/transaction/'+$routeParams.TransactionId).then(function(response){
            

            $scope.transaction_data = response.data;
            
            if($scope.transaction_data.Quote){
            	$scope.quote = $scope.transaction_data.Quote;
            }

            
            if($scope.transaction_data.SaleOrder){
            	$scope.sale_order = $scope.transaction_data.SaleOrder;
            }

            if($scope.transaction_data.SaleOrderPayments){
                $scope.sale_order_payments = $scope.transaction_data.SaleOrderPayments;

                //var calculateBalance = function (){

                    if($scope.sale_order){

                        var Balance = $scope.sale_order.SaleOrderTotal;
                        var PaymentTotal = 0;
                        $scope.currentProgress = 0;


                        if($scope.sale_order_payments.length > 0){
                            
                            for(var i=0, len=$scope.sale_order_payments.length; i < len ; i ++)
                            {

                                    Balance -= parseFloat($scope.sale_order_payments[i].SaleOrderPaymentAmount);
                                    PaymentTotal += parseFloat($scope.sale_order_payments[i].SaleOrderPaymentAmount);

                                    $scope.sale_order_payments[i].SaleOrderPaymentBalance = Balance;

                            };
                            
                            $scope.currentProgress =Math.floor((PaymentTotal / $scope.sale_order.SaleOrderTotal)  * 100);
                            $scope.PaymentTotalAmount = PaymentTotal;

                        };
                    };
                    
                //};


            }

            if($scope.transaction_data.WorkOrder){
            	$scope.work_order = $scope.transaction_data.WorkOrder;
            }
            //console.log($scope.transaction_data);
                //$scope.barChartData = saleOrderCharData;
        });
	};
};