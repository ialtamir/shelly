'use strict';

function  POSCtrl($scope,$location,$notification,$http,$filter,CategoryService,CurrencyService,SaleOrderService,ProductService,PlaceService,CustomerService,limitToFilter,tokenHandler)
{
    $scope.categories = []
    var products_data = [];
    $scope.products  = [];
    $scope.places   = [];
    $scope.sale_order = {};
    $scope.selected_product = {};
    $scope.backup_selected_product = {};
    
    $scope.selected_item = {};
    $scope.backup_selected_item = {};

    $scope.totalItems = 0;
      
      
    $scope.currentTokenInfo = tokenHandler.info();

    $scope.currentPage = 1;
    $scope.pageSize = 50;
    
    $scope.selected_customer = {};
    
    
    
    $scope.numberOfPages=function(){
        
        return Math.ceil($scope.totalItems/$scope.pageSize);   
        
        
    }


    $scope.monedas    = [{MonedaId: 20 , MonedaImporte : 20, MonedaTipo: 'Billete' },
    {MonedaId: 50 , MonedaImporte : 50, MonedaTipo: 'Billete' },
    {MonedaId: 100 , MonedaImporte : 100, MonedaTipo: 'Billete' },
    {MonedaId: 200 , MonedaImporte : 200, MonedaTipo: 'Billete' },
    {MonedaId: 500 , MonedaImporte : 500, MonedaTipo: 'Billete' }];

    $scope.monedasSeleccionadas = [];
    
    CustomerService.getByCode({CustomerCode: "DEFAULT"},function(response){
        
        $scope.selected_customer = response;
        
        
    });

     CategoryService.query(function(data){
        $scope.categories = data;
     });

     ProductService.All({ ProductId : '',include_only_saleables:true},function(data){
        
        products_data = data;
        $scope.products = data;
        $scope.totalItems = data.length;
        $scope.currentPage = 1;

    });
    
    PlaceService.query(function(response){
				   		$scope.places = response;
	});

    CurrencyService.query(function(response){
                        $scope.currencies = response;
    });


    $scope.limpiarVenta = function(){
        
        CustomerService.getByCode({CustomerCode: "DEFAULT"},function(response){
            
            
            $scope.selected_customer = response;
            
            
        });
    
    
        $scope.sale_order = new SaleOrderService({
                        SaleOrderStatus : "Pendiente",
                        SaleOrderDate : new Date(),
                        SaleOrderFolio : 'SO-##',
                        SaleOrderTitle : 'Punto de venta',
                        CurrencyId: 1,
                        items: [],
                        deleted_items : [],
                        SaleOrderCash : 0,
                        SaleOrderType : "POS"

                    });
        
    };
    
    $scope.increaseCash = function (moneda){
        $scope.sale_order.SaleOrderCash += moneda.MonedaImporte;
    };

    $scope.increaseQty = function(item){
        item.SaleOrderDetailQTY += 1;
    };

    $scope.decreaseQty = function(item){

         item.SaleOrderDetailQTY -= 1;


        if (item.SaleOrderDetailQTY <= 0){

            var index = $scope.sale_order.items.indexOf(item,0); 

            if(index >= 0){
                $scope.removeItem(index);
            }
                
        }
        
    };

    $scope.mostrarPago = function(){

            $scope.showPaymentForm = true;
     
    };
    
    $scope.mostrarCredito = function(){
        
        $scope.sale_order.SaleOrderCreditPeriodicity = "weekly";
        $scope.sale_order.SaleOrderCreditInitialDate = new Date().yyyymmdd();
        $scope.sale_order.SaleOrderDownpayment = 0;
        $scope.sale_order.SaleOrderNumberOfPayments = 0;
        $scope.showCreditForm = true;
     
    };



    $scope.cancelPayment = function(){
        $scope.sale_order.SaleOrderCash = 0;
        $scope.showPaymentForm = false;
    };


    $scope.cancelCredit = function(){
        $scope.sale_order.SaleOrderCash = 0;
        $scope.showCreditForm = false;
    };
    
    $scope.remaining = function(){
        var restante = 0;


        restante = $scope.sale_order.SaleOrderTotal - $scope.sale_order.SaleOrderCash;

        if(restante < 0)
        {
            restante = 0;
        }
        return restante;
    };
    
    $scope.creditRemaining = function(){
        
       var creditRemaining = 0;
       
       creditRemaining = $scope.sale_order.SaleOrderTotal - $scope.sale_order.SaleOrderDownpayment;
       
       if(creditRemaining < 0){
           
           creditRemaining = 0;
       }
       
       return creditRemaining;
       
    };
    
    $scope.creditEndDate = function (){
        
        if($scope.sale_order.SaleOrderCreditInitialDate){
         
            var year = $scope.sale_order.SaleOrderCreditInitialDate.substring(0,4);
            var month =  $scope.sale_order.SaleOrderCreditInitialDate.substring(5,7);
            var day =  $scope.sale_order.SaleOrderCreditInitialDate.substring(8,10);
            
      
            var initialDate =new Date(year, month-1, day);
            
    
            
            var days = 0;
            
            if($scope.sale_order.SaleOrderCreditPeriodicity ==="weekly"){
                
                days = $scope.sale_order.SaleOrderNumberOfPayments * 7;
            }
            else {
                days = $scope.sale_order.SaleOrderNumberOfPayments * 15;
            }
           
           return new Date(initialDate.getTime()+(days*24*60*60*1000));
           
        
       
        //return days;
        }

    };

    $scope.change = function(){

        var cambio = 0 ;

        if( $scope.sale_order.SaleOrderTotal < $scope.sale_order.SaleOrderCash ){
            cambio = $scope.sale_order.SaleOrderCash - $scope.sale_order.SaleOrderTotal;
        }

        return cambio;

    };

    $scope.addDiscount = function(item)
    {
        item.SaleOrderDetailDiscount = 20;

    }

    $scope.removeDiscount = function(item)
    {
        item.SaleOrderDetailDiscount = 0;

    }
    
    
    
   
    $scope.addProduct = function(product){
        

        
        //if(product.test){
                //if(product.ProductPrice > 0){
        
        
        
                    var item = { "SaleOrderDetailCode" : "", "SaleOrderDetailDescripton" : "","SaleOrderDetailQTY" : 1, "SaleOrderDetailPrice" : 0,"SaleOrderDetailTax": 0 , "IsNew" : true };
                    
                    
        
                    item.Product = product;
        
                    if(item.Product){
                                        item.SaleOrderDetailPrice = item.Product.ProductPrice;
                                        item.SaleOrderDetailUM = item.Product.ProductUM;
        
                        if(item.Product.addQTY){
                            item.SaleOrderDetailQTY = item.Product.addQTY;
                        }
                    }
        
                    $scope.sale_order.items.push(item);
                    $scope.showProductInfo = false;
                /*}
                else {
                    
                    $scope.selected_product = angular.copy(product);
                    $scope.selected_product.test = 1
                    $scope.selected_product.addQTY  = 1;
                    $scope.showProductInfo = true; 
                    
                }*/
       /* }
        else {


            $scope.selected_product = angular.copy(product);
            $scope.selected_product.addQTY  = 1;
            $scope.selected_product.test = 1
            $scope.showProductInfo = true;
        }*/

    };
    
    $scope.editItemPrice = function(item){
        
        $scope.selected_item = item;
        $scope.backup_selected_item = angular.copy(item);
        $scope.selected_item.addQTY  = 1;
        $scope.showItemChangePrice = true; 
    };
    
    $scope.changeItemPrice = function(){
        
        
        $scope.showItemChangePrice = false;
    };
    
    
    $scope.cancelChangeItemPrice = function(){
        
        $scope.selected_item = $scope.backup_selected_item;
        $scope.showItemChangePrice = false;
    };
    
    $scope.changeCustomer  = function(){
        $scope.selected_customer_temp = angular.copy($scope.selected_customer);
        $scope.selected_customer = "";
        $scope.showCustomerSearch = true;
    };
    
    
    $scope.cancelChangeCustomer  = function(){
        
        $scope.selected_customer = $scope.selected_customer_temp;
        $scope.showCustomerSearch = false;
    }
    
    
    
    $scope.onCustomerSelection = function(customer){    
         $scope.selected_customer = customer;
   		$scope.showCustomerSearch = false;
    };
   
   
    $scope.customerSearch = function (viewValue) {
        return $http.get('api/customers/search/'+viewValue).then(function(response){
            return limitToFilter(response.data,15);
        });
    };


    $scope.closeProductInfo = function(){

        $scope.showProductInfo = false;
    };

    
    $scope.removeItem = function(index) {        
        $scope.sale_order.items.splice(index,1);
    };
   

    $scope.payExactAmount = function(){
        $scope.sale_order.SaleOrderCash = $scope.sale_order.SaleOrderTotal;
    };

    

    $scope.saveSaleOrder = function(){
       
       if($scope.remaining() === 0){

            if($scope.selected_customer){
               
               $scope.sale_order.CustomerId = $scope.selected_customer.CustomerId;
            }
           
            $scope.sale_order.$save({SaleOrderId: $scope.sale_order.SaleOrderId},function(){
                 $scope.printOrder();
                 $scope.showPaymentForm = false;
                 $notification.success( $scope.sale_order.SaleOrderFolio, 'La orden se guardo correctamente');
                 $scope.limpiarVenta();
                 
                 
                  
            });          
       }
        
    };
    
    $scope.$watch('search',function(query){

         $scope.products = $filter("filter")(products_data, query);
          $scope.totalItems = $scope.products.length;
         //$scope.tableParams.total = $scope.products.length;

    });
    
    

    
    $scope.limpiarVenta();
    
    $scope.printOrder = function() {

					
							//$notification.success($scope.sale_order.SaleOrderFolio, 'La orden se guardo correctamente');
							var win = window.open('api/sale_orders/report/' + $scope.sale_order.SaleOrderId+'?access_token='+$scope.currentTokenInfo.access_token);
							win.focus();
							
					
					};

    var calculateTotals = function(){

                        var taxes = 0;
                        var subTotal = 0;
                        var discounts = 0;

                        for(var i=0, len=$scope.sale_order.items.length; i < len ; i ++)
                        {
                            var total = $scope.sale_order.items[i].SaleOrderDetailPrice * $scope.sale_order.items[i].SaleOrderDetailQTY;

                            $scope.sale_order.items[i].SaleOrderDetailTotal = total;
                            


                            var discount = total * ( $scope.sale_order.items[i].SaleOrderDetailDiscount / 100);

                            $scope.sale_order.items[i].SaleOrderDetailDiscountAmount = discount;

                            var tax = total * ( $scope.sale_order.items[i].SaleOrderDetailTax / 100);

                            $scope.sale_order.items[i].SaleOrderDetailTaxAmount = tax;

                            subTotal += total;

                            taxes = taxes + tax ;

                            if($scope.sale_order.items[i].SaleOrderDetailDiscount > 0){
                               discounts = discounts + discount;                 
                            }



    

                        };

                        $scope.sale_order.SaleOrderSubtotal = subTotal;
                        $scope.sale_order.SaleOrderTaxes = taxes;
                        $scope.sale_order.SaleOrderDiscounts = discounts;
                        $scope.sale_order.SaleOrderTotal = $scope.sale_order.SaleOrderSubtotal +  $scope.sale_order.SaleOrderTaxes -  $scope.sale_order.SaleOrderDiscounts;

                        if($scope.sale_order.SaleOrderExchangeRate){

                            if($scope.sale_order.CurrencyId == 1){
                                $scope.sale_order.SaleOrderTotalDlls= $scope.sale_order.SaleOrderTotal / $scope.sale_order.SaleOrderExchangeRate;
                            }
                            else
                            {
                                $scope.sale_order.SaleOrderTotalDlls= $scope.sale_order.SaleOrderTotal * $scope.sale_order.SaleOrderExchangeRate;
                            }
                        }

                        

                    };

                    $scope.$watch('sale_order.items', calculateTotals , true);



    $scope.$on('$locationChangeStart', function(event,next,current){
        if($scope.sale_order.SaleOrderTotal > 0 ){
           if(!confirm("Hay una venta en proceso, deseas cancelarla?")){
            event.preventDefault();
            } 
        }
            
    });
}




