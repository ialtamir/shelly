
function QuoteDetailCtrl ($scope,$resource,$http,$location,tokenHandler,currentUser,QuoteService,CurrencyService,ProductService,PlaceService,limitToFilter,$notification,$routeParams,$position){

	$scope.customers = [];
    //$scope.customer = {};
	$scope.quotes = [];
    $scope.currencies =[];
    $scope.currentItem = {};
    $scope.places = [];

    
    $scope.currentTokenInfo = tokenHandler.info();
    $scope.userInfo = currentUser;
    
    
    

    $scope.dateOptions = {
        changeYear:true,
        changeMonth:true,
        dateformat: "yy-mm-dd"
    };
    
    
   

	$scope.quote = new QuoteService({
        QuoteFolio: "COT-##",
        IsNew : true, 
		QuoteStatus : "Pendiente",
        QuoteDate : new Date().yyyymmdd(),
		items: [{ "QuoteDetailCode" : "", "QuoteDetailDescripton" : "","QuoteDetailQTY" : 1, "QuoteDetailPrice" : 0,"QuoteDetailTax": 0 }],
        deleted_items : [],
        CurrencyId : 2
    });
    
    
    
    



    $scope.showCustomerModal = function (){
        $scope.showCustModal = true;
    };

	if($routeParams.QuoteId > 0 ){ 
        
        $scope.QuoteId = $routeParams.QuoteId;

    	QuoteService.get({QuoteId:$routeParams.QuoteId},function(response) {
    		$scope.quote = response;
            $scope.quote.deleted_items= [];
    		if($scope.quote.customer){
    			$scope.selectedCustomer = $scope.quote.customer;
    		}
    	});

   	}


    $scope.removeQuoteItem = function(item){

    	if(item.IsNew) {

    		var index = $scope.quote.items.indexOf(item,0); 
            
            if (index != -1 )
            {

                $scope.quote.items.splice(index,1) ;
                
            }  
    	}
    	else {

    		$scope.quote.deleted_items.push(item);

            var index = $scope.quote.items.indexOf(item,0); 
            
            if (index != -1 )
            {
                $scope.quote.items.splice(index,1) ;
            }  

    	}
    };

    $scope.removeProductObject = function(item){
        item.Product = "";
    };

   $scope.onProductSelection = function(item){    
   		if(item.Product){

   			item.QuoteDetailPrice = item.Product.ProductPrice;
   			item.QuoteDetailUM = item.Product.ProductUM;

   		}
   };

	$scope.selectAnotherCustomer = function (){
		$scope.selectedCustomer = '';
	};

	$scope.addItem = function (){
		var item = { "QuoteDetailCode" : "", "QuoteDetailDescripton" : "","QuoteDetailQTY" : 1, "QuoteDetailPrice" : 0,"QuoteDetailTax": 0 , "IsNew" : true };

		$scope.quote.items.push(item);
	};


	var calculateTotals = function(){

		var taxes = 0;
        var subTotal = 0;
        var discounts = 0;

		for(var i=0, len=$scope.quote.items.length; i < len ; i ++)
		{
			var total = $scope.quote.items[i].QuoteDetailPrice * $scope.quote.items[i].QuoteDetailQTY;
            
             
            $scope.quote.items[i].QuoteDetailTotal=total ;

            var discount = total * ( $scope.quote.items[i].QuoteDetailDiscount / 100);

            $scope.quote.items[i].QuoteDetailDiscountAmount = discount;

            var tax = total * ( $scope.quote.items[i].QuoteDetailTax / 100);

            $scope.quote.items[i].QuoteDetailSubTotal = (total+tax)-discount;
           
            $scope.quote.items[i].QuoteDetailTaxAmount = tax;

            subTotal += total;

			taxes = taxes + tax ;

            if($scope.quote.items[i].QuoteDetailDiscount > 0){
               discounts = discounts + discount;                 
            }
            
		};

		$scope.quote.QuoteSubtotal = subTotal;
        $scope.quote.QuoteTaxes = taxes;
        $scope.quote.QuoteDiscounts = discounts;
		$scope.quote.QuoteTotal = $scope.quote.QuoteSubtotal +  $scope.quote.QuoteTaxes - $scope.quote.QuoteDiscounts ;
        
        if($scope.quote.QuoteExchangeRate){
            
            if($scope.quote.QuoteExchangeRate > 0){
                if($scope.quote.CurrencyId == 1) {
                    $scope.quote.QuoteTotalDlls = $scope.quote.QuoteTotal/$scope.quote.QuoteExchangeRate;
                }else{
                    $scope.quote.QuoteTotalDlls = $scope.quote.QuoteTotal * $scope.quote.QuoteExchangeRate;
                }
                
            }

        }        
	};

	$scope.$watch('quote.items', calculateTotals , true);

    $scope.$watch('quote.QuoteExchangeRate',calculateTotals,true);

    $scope.$watch('quote.CurrencyId', calculateTotals,true);

    /// Start Interface && controls handling

    $scope.showItemExtendEditModal = function(item){

        $scope.showEditModal = true;
        $scope.editQuoteItem = item;

    };

    $scope.hideItemExtendEditWindow = function(){
        $scope.showEditModal = false;
    };

     $scope.onNewProductSelection = function(item){
        $scope.currentItem = item;
        $scope.product = new ProductService({ ProductName : item.Product});
             
        $scope.showProductModal = true;
     };

    $scope.selectProduct = function(item){
        
        $scope.currentItem.Product = $scope.product;
         $scope.onProductSelection($scope.currentItem);  
        $scope.showProductModal = false;
    }; 

    $scope.onItemNotFound = function(item){

        item.notFound = true;
        item.IsManualItem = false;
        
    };

    $scope.onItemMatchesFound = function(item){
        item.notFound = false;
    };
    
    $scope.itIsManualItem = function(item){
        return item.IsManualItem && item.Product.length > 0;
    };

    $scope.setAsManualItem = function(item){
        item.IsManualItem = true;
    };

    

    /// End Interface && controls handling

    ///--> Service API Calls Start 
    
    $scope.saveQuote = function() {

        if($scope.selectedCustomer){
          
          if($scope.selectedCustomer.CustomerId){

            $scope.quote.CustomerId = $scope.selectedCustomer.CustomerId;

          };

        }

        $scope.quote.$save({ QuoteId : $scope.quote.QuoteId} , function (){
                $notification.success($scope.quote.QuoteFolio, 'La cotización se guardo correctamente');
        });
    };

    $scope.delete = function(){

        if($scope.quote.QuoteDeleteWord === "ELIMINAR"){

            $scope.quote.$remove({ QuoteId : $scope.quote.QuoteId },function (){
                
                
                    $scope.showRemoveModal = false;
                    $notification.success('Eliminación correcta', 'La cotización se elimino correctamente');
                    $location.path("/cotizaciones/");
                
                
            },function (response){

                
                  $notification.error('Eliminación incorrecta', 'La cotización no se pudo eliminar');
                
            });
        }    
        else {
            $notification.error('Captura incorrecta', 'Intente capturar nuevamente');
        }    
    };


    $scope.approveQuote = function () {

        if($scope.quote.QuoteStatus != 'Aprobada'){

            $scope.quote.QuoteStatus = 'Aprobada';

            $scope.quote.$approveQuote({QuoteId : $scope.quote.QuoteId },function(response){

                $notification.success("cotización aprobada","La cotización se aprobo correctamente");

            });
        }
        else
        {
            $notification.error("Error al aprobar","La cotización ya ha sido aprobada anteriormente");
        }
    };

    $scope.cancelQuote = function () {
        
        if($scope.quote.QuoteStatus != 'Cancelada'){
            
            if(confirm("Tambien se cancelara la orden de venta, desea continuar?")){

                //$scope.quote.QuoteStatus = 'Cancelada';

                $scope.quote.$cancelQuote({QuoteId : $scope.quote.QuoteId },function(response){
                    
                    $notification.success("cotización cancelada","La cotización fue cancelada");

                },function(response){
                    $notification.error("Error al cancelar","La cotización no pudo ser cancelada");
                });
            }
        }
        else
        {
            $notification.error("Error al cancelar","La cotización ya esta cancelada");
        }

    };

    $scope.sendEmail = function(quote){
        
        if(quote.QuoteStatus !== 'Cancelada')
        {

            QuoteService.emailQuote({ QuoteId : quote.QuoteId },quote,function(){

                $notification.success("Envio correcto", "La cotizacion de envio correctamente");
                $scope.quote.email = {};
                $scope.showEmailModal = false;
            },
            function(response){
                console.log(response.data);
                $notification.error("Error en el envio", response.data);
            });
        }
            
    };

    $scope.editEmail = function(){
        $scope.showEmailModal = true;
    };

    $scope.removeQuote = function(quote){
        if(quote.QuoteStatus !== "Aprobada"){
            $scope.showRemoveModal = true;
        }
        else{
            $notification.error("Error al eliminar","No se puede eliminar una cotización Aprobada")
        }
    }

    $scope.customerSearch = function (viewValue) {
        return $http.get('api/customers/search/'+viewValue).then(function(response){
            return limitToFilter(response.data,15);
        });
    };

    $scope.productSearch = function (viewValue) {
        return $http.get('api/products/search/'+viewValue).then(function(response){
            return limitToFilter(response.data, 15);
        });
    };

    CurrencyService.query(function(response){
        $scope.currencies = response;
    });
    
    PlaceService.query(function(response){
        $scope.places = response;
        
        
    });


    ProductService.unidades(function(response){

        $scope.unidades = response;
               
    });
    

    /// < Service API calls End 

}

function QuoteCtrl ($scope,$resource,$location,$http,tokenHandler,QuoteService,CurrencyService,ngTableParams,$filter,$notification){

	var data =[];
	$scope.customers = [];
	$scope.quotes = [];
    $scope.currentPath = $location.path();
    $scope.currentTokenInfo = tokenHandler.info();

    if($scope.currentPath === "/cotizaciones/procesadas") {
       
        QuoteService.procesadas(function(response){
             data = response;
            $scope.tableParams.total = data.length;
        },function(response){
           
        });        

    }
    else if($scope.currentPath === "/cotizaciones/canceladas") {
       
        QuoteService.canceladas(function(response){
             data = response;
            $scope.tableParams.total = data.length;
        },function(response){
           
        });        

    }
    else if($scope.currentPath === "/cotizaciones/completadas") {
       
        QuoteService.completadas(function(response){
             data = response;
            $scope.tableParams.total = data.length;
        },function(response){
           
        });        

    }
    else  {
        QuoteService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
        },function(response){
           //Bad request handler
            
        });
    }


	

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.quotes = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.quotes = $filter("filter")(data, query);

         //$scope.tableParams.total = $scope.quotes.length;
         //$scope.tableParams.count = 100;

    });



    $scope.editEmail = function(quote){
        $scope.showEmailModal = true;
        $scope.selectedquote = quote;
    };



    $scope.sendEmail = function(selectedquote){

        var quote = selectedquote;

        if(quote.QuoteStatus != 'Cancelada')
        {

            QuoteService.emailQuote({ QuoteId : quote.QuoteId },quote,function(){
                $notification.success("Envio correcto", "La cotizacion de envio correctamente");
                quote.email = {};
                $scope.showEmailModal = false;
            },
            function(response){
               
                $notification.error("Error en el envio", response.data);
                
            });
        };
            
    };
   

}



function QuoteSaleOrderCtrl($scope,$routeParams,QuoteService,SaleOrderService,$notification){


    if($routeParams.QuoteId >0 ){

        $scope.QuoteId = $routeParams.QuoteId ;

        if($scope.QuoteId > 0 ){

            QuoteService.get({QuoteId:$scope.QuoteId},function(response) {

                $scope.quote = response;    

                if($scope.quote.SaleOrderId) {

                    $scope.SaleOrderId = $scope.quote.SaleOrderId;
                }  

                if($scope.quote.WorkOrderId){
                    $scope.WorkOrderId = $scope.quote.WorkOrderId;
                }          
            });


        };

    };

    $scope.createSaleOrder=function(){
        
        $scope.quote.$createSaleOrder( {QuoteId : $scope.quote.QuoteId} , function(){
            //$scope.quote = response;    

            if($scope.quote.SaleOrderId) {

                $scope.SaleOrderId = $scope.quote.SaleOrderId;
            } 
                
            $notification.success($scope.quote.SaleOrderId,'Se creo la orden de venta correctamente');
        })
    };



}

function QuoteAttachmentsCtrl($scope,$timeout,$resource,$routeParams,$location,$http,QuoteService,CurrencyService,ngTableParams,$filter,$notification,$upload){

    var data = [];
    
    $scope.category = { IsNew : true };
    $scope.quote_attachments = [];

    if($routeParams.QuoteId >0 ){

        $scope.QuoteId = $routeParams.QuoteId ;

        if($scope.QuoteId > 0 ){

            QuoteService.get({QuoteId:$scope.QuoteId},function(response) {

                $scope.quote = response;   


                QuoteService.attachments({QuoteId:$scope.quote.QuoteId},function(response){

                    data = response;
                    $scope.quote_attachments = data;
                    
                           
                }); 

                if($scope.quote.SaleOrderId) {

                    $scope.SaleOrderId = $scope.quote.SaleOrderId;
                }  

                if($scope.quote.WorkOrderId){
                    $scope.WorkOrderId = $scope.quote.WorkOrderId;
                }   
            });


        };

    };


    $scope.fileReaderSupported = window.FileReader != null;
    $scope.uploadRightAway = true;
    $scope.howToSend = 1;

    

    $scope.simpleThumb = 'http://placehold.it/160x120';


    $scope.hasUploader = function(index) {
        return $scope.upload[index] != null;
    };


    $scope.abort = function(index) {
        $scope.upload[index].abort(); 
        $scope.upload[index] = null;
    };

    $scope.onFileSelect = function($files,quote_attachment) {
        $scope.selectedFiles = [];
        $scope.progress = [];

        if ($scope.upload && $scope.upload.length > 0) {
            for (var i = 0; i < $scope.upload.length; i++) {
                if ($scope.upload[i] != null) {
                    $scope.upload[i].abort();
                }
            }
        }

        $scope.upload = [];
        $scope.uploadResult = [];
        $scope.selectedFiles = $files;
        $scope.dataUrls = [];


        for ( var i = 0; i < $files.length; i++) {
            var $file = $files[i];
            if (window.FileReader && $file.type.indexOf('image') > -1) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL($files[i]);
                function setPreview(fileReader, index) {
                    fileReader.onload = function(e) {
                        $timeout(function() {
                            $scope.dataUrls[index] = e.target.result;
                        });
                    }
                }
                setPreview(fileReader, i);
            }
            $scope.progress[i] = -1;
            if ($scope.uploadRightAway) {
                $scope.start(i,quote_attachment);
            }
        }
    };


    $scope.start = function(index,quote_attachment) {
        $scope.progress[index] = 0;
        if ($scope.howToSend == 1) {
            $scope.upload[index] = $upload.upload({
                url : '/api/quotes/' + $scope.quote.QuoteId + '/attachments/',
                method: $scope.httpMethod,
                headers: {'myHeaderKey': 'myHeaderVal'},
                data : {
                    myModel : $scope.myModel
                },
                /* formDataAppender: function(fd, key, val) {
                    if (angular.isArray(val)) {
                        angular.forEach(val, function(v) {
                          fd.append(key, v);
                        });
                      } else {
                        fd.append(key, val);
                      }
                }, */
                file: $scope.selectedFiles[index],
                fileFormDataName: 'attachment'
            }).then(function(response) {
                $scope.uploadResult.push(response.data.QuoteAttachmentUrl);
                //alert(response.data.url);
                $scope.quote_attachments.push(response.data);
            }, null, function(evt) {
                $scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
            });
        } else {
            var fileReader = new FileReader();
            fileReader.readAsArrayBuffer($scope.selectedFiles[index]);
            fileReader.onload = function(e) {
                $scope.upload[index] = $upload.http({
                    url : '/api/quotes/' + $scope.quote.QuoteId + '/attachments/',
                    headers: {'Content-Type': $scope.selectedFiles[index].type },
                    data: e.target.result
                }).then(function(response) {
                    //$scope.uploadResult.push(response.data);
                    $scope.uploadResult.push(response.data.QuoteAttachmentUrl);
                    //alert(response.data.url);
                    $scope.quote_attachments.push(response.data);

                }, null, function(evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        }
    }

    $scope.onUploadImage = function(attachment){

    }

    $scope.delete = function (attachment){
        QuoteService.deleteAttachment({QuoteId: $scope.quote.QuoteId,  QuoteAttachmentId : attachment.QuoteAttachmentId},function(response){
            

             var index = $scope.quote_attachments.indexOf(attachment,0); 
            
            if (index != -1 )
            {

                $scope.quote_attachments.splice(index,1) ;
               
               $notification.success("Adjunto","Se  borro correctamente");

              
            }


            
        });
    }


}