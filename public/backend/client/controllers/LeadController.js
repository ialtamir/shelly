'use strict';

function LeadCtrl($scope,$filter,LeadService,currentUser,ngTableParams,$notification) {

	var data = [];
    
	$scope.lead = { IsNew : true };
	$scope.leads = [];
    $scope.userInfo = currentUser;

	LeadService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.leads = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.leads = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.leads.length;
         $scope.tableParams.page = 1;

    });

    $scope.refresh = function(){
        LeadService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };


    $scope.add = function(){
        
        $scope.lead = new LeadService();
    	$scope.lead.IsNew = true;

     	$scope.showLeadModal = true;
         
    };

    $scope.edit = function(user){
       
    	$scope.lead = user;
        $scope.showLeadModal = true;
        
    };

    $scope.cancel = function(){
        
        $scope.lead = {};
        $scope.showLeadModal = false;

    };

    $scope.save = function(){

        $scope.lead.$save({ LeadId:$scope.lead.LeadId },function(){

        if($scope.lead.LeadId   >   0   &&  $scope.lead.IsNew){
               
            data.push($scope.lead);

            $notification.success($scope.lead.LeadName, 'El prospecto de cliente se guardo correctamente');
            
            $scope.lead = {};
            $scope.tableParams.total = data.length;
        }
        
    	$scope.showLeadModal = false;
            
        },function(){        	 
             $notification.error('Funcion incorrecta', 'El prospecto no se pudo guardar');
        });
        
    };

    $scope.delete = function(lead){

        lead.$remove({ LeadId : lead.LeadId },function (){
            
            var index = data.indexOf(lead,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
                $notification.success('Eliminación Correcta', 'El prospecto se elimino correctamente');
                
                $scope.tableParams.total = data.length;
            }
            
        },function (){
             
     
              $notification.error('Eliminación incorrecta', 'El prospecto no se pudo eliminar');
        });
    };


    $scope.convertLeadToCustomer = function(lead){
        lead.$openCode({ LeadId : lead.LeadId},function(){
            var index = data.indexOf(lead,0); 
             if (index != -1 )
            {

                data.splice(index,1) ;
                $notification.success('Conversion Exitosa', 'El prospecto ahora esta en el Catalogo de Clientes');
                
                $scope.tableParams.total = data.length;
            }

            
            },
            function(){

            }
        );
    };
}