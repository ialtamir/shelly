'use strict';

function CurrencyCtrl($scope,$filter,CurrencyService,currentUser,ngTableParams,$notification) {

	var data = [];
    
	$scope.currency = { IsNew : true };
	$scope.currencies = [];
    $scope.userInfo = currentUser;

	CurrencyService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.currencies = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.currencies = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.currencies.length;

    });

    $scope.refresh = function(){
        CurrencyService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };


    $scope.add = function(){
        
        $scope.currency = new CurrencyService();
    	$scope.currency.IsNew = true;

     	$scope.showCurrencyModal = true;
         
    };

    $scope.edit = function(user){
       
    	$scope.currency = user;
        $scope.showCurrencyModal = true;
        
    };

    $scope.cancel = function(){
        
        $scope.currency = {};
        $scope.showCurrencyModal = false;

    };

    $scope.save = function(){

        $scope.currency.$save({ CurrencyId:$scope.currency.CurrencyId },function(){

        if($scope.currency.CurrencyId   >   0   &&  $scope.currency.IsNew){
               
            data.push($scope.currency);

            $notification.success($scope.currency.CurrencyName, 'Unidad de Medida guardada correctamente');
            
            $scope.currency = {};
            $scope.tableParams.total = data.length;
        }
            
    	$scope.showCurrencyModal = false;
            
        },function(){        	 
             $notification.error('Guardado incorrecto', 'La unidad no se pudo guardar');
        });
        
    };

    $scope.delete = function(currency){

        currency.$remove({ CurrencyId : currency.CurrencyId },function (){
            
            var index = data.indexOf(currency,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
                $notification.success($scope.currency.CurrencyName, 'Unidad de Medida eliminada correctamente');
                
                $scope.tableParams.total = data.length;
            }
            
        },function (){
             
     
              $notification.error('Eliminación incorrecta', 'La Unidad no se pudo eliminar');
        });
    };


}