function HomeCtrl($scope,$http, currentUser,AuthenticationService) {


	var date = new Date();
	var firstDay = Date.today().moveToFirstDayOfMonth().toString('yyyy-MM-dd');
	var lastDay = Date.today().toString('yyyy-MM-dd');
    

    $scope.countQuotes = 0;
    $scope.CountPendingQuotes = 0;
    $scope.CountAcceptedQuotes = 0;
    $scope.CountCanceledQuotes = 0;
    $scope.countSales = 0;
    $scope.CountPendingSales = 0;
    $scope.CountAcceptedSales = 0;
    $scope.CountCanceledQuotes = 0;
    $scope.CountWorks = 0;
    $scope.CountNewWorks = 0;
    $scope.CountOpenedWorks = 0;
    $scope.CountClosedWorks = 0;
    $scope.countPayments = 0;
    $scope.getAmountPaymentsMxn = 0;
    $scope.getAmountPaymentsDlls = 0;
    $scope.getAmountSales = 0;
    //$scope.chart = { labels:[],datasets : [] };
    //s$scope.lineChart = { labels:[],datasets : [] };


    $scope.userInfo = currentUser.info;

    $scope.dateOptions = {
        changeYear:true,
        changeMonth:true,
        dateformat: "yy-mm-dd"
    }
    


$scope.lineData = [
                ];

$scope.barData  = [];


    $scope.lineOptions = {
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Pesos', 'Dolares'],
        hideHover: true,
        dateFormat: function(date) {
              d = new Date(date);
              return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear(); 
              }
    };

    $scope.barOptions = {
    xkey: 'y',
    ykeys: ['a', 'b'],
    labels: ['Pesos', 'Dolares'],
    hideHover: true,
    dateFormat: function(date) {
          d = new Date(date);
          return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear(); 
          }
    };
    


    $scope.dateFrom = firstDay;
    $scope.dateTo = lastDay;
    
    $scope.selectDateFrom = firstDay;
    $scope.selectDateTo = lastDay;


        $scope.loadDashboardData = function (){

        $http.get('api/dashboard/?dateFrom='+$scope.dateFrom+'&dateTo='+$scope.dateTo).then(function(response){
                
            var DashboardData = response.data;

           // $scope.barData = DashboardData

            if(DashboardData.Quotes){

                $scope.countQuotes = DashboardData.Quotes.Total;
                $scope.CountPendingQuotes = DashboardData.Quotes.Pendings;
                $scope.CountAcceptedQuotes = DashboardData.Quotes.Approved;
                $scope.CountCanceledQuotes = DashboardData.Quotes.Cancelled;
                

            }

            if(DashboardData.Sales){

                $scope.countSales = DashboardData.Sales.Total;
                $scope.CountPendingSales = DashboardData.Sales.Pendings;
                $scope.CountAcceptedSales = DashboardData.Sales.Approved;
                $scope.CountCanceledSales = DashboardData.Sales.Cancelled;
                $scope.getAmountSales = DashboardData.Sales.Today;
            }

            if(DashboardData.Works){

                $scope.CountWorks = DashboardData.Works.Total;
                $scope.CountNewWorks = DashboardData.Works.New;
                $scope.CountOpenedWorks = DashboardData.Works.Opened;
                $scope.CountClosedWorks = DashboardData.Works.Closed;               

            }

            if(DashboardData.Payments){

                $scope.countPayments = DashboardData.Payments.Total;
                $scope.getAmountPaymentsMxn = DashboardData.Payments.MxnAmount;
                $scope.getAmountPaymentsDlls = DashboardData.Payments.DllsAmount;

                
            }

          

        });


		
		console.log($scope.dateFrom.toString("yyyy"));

        //Grafica de ventas
        $http.get('api/dashboard/chartsaleorders/?year='+new Date($scope.dateFrom).getFullYear()).then(function(response){
            var saleOrderCharData = response.data;

            $scope.barData = saleOrderCharData;
            
         });


        $http.get('api/dashboard/chartpayments/?year='+new Date($scope.dateFrom).getFullYear()).then(function(response){
            var paymentsCharData = response.data;


            $scope.lineData = paymentsCharData;

 
         });


    }

    $scope.loadDashboardData();


    

    $scope.showDates= function(){
    	$scope.selectDateFrom = $scope.dateFrom;
    	$scope.selectDateTo = $scope.dateTo;
    	$scope.showDatePicker = true;
    };

    $scope.selectDates = function(){

    	$scope.dateFrom = $scope.selectDateFrom;
    	$scope.dateTo = $scope.selectDateTo;
        $scope.loadDashboardData();
    	$scope.showDatePicker = false;

    };

    

    







	
}