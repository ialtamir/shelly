function AccountsReceivableDetailCtrl($scope,$routeParams,SaleOrderService){

   
   
   
	if($routeParams.SaleOrderId)
	{
		$scope.SaleOrderId = $routeParams.SaleOrderId;

		SaleOrderService.get({ SaleOrderId: $scope.SaleOrderId},function(response){
			$scope.sale_order = response;
		});
	}

	if($routeParams.QuoteId)
	{

		SaleOrderService.getOrderByQuote({ QuoteId : $routeParams.QuoteId },function(response){
			
				$scope.sale_order = response;
				    			
		});
	}


};


function AccountsReceivableCtrl($scope,$filter,$notification,$location,AccountsPayableService,SaleOrderService,ngTableParams){

    var data =[];
	$scope.receivable_orders = [];
    $scope.currentPath = $location.path();
    

    if($scope.currentPath === "/cobranza/pagadas/") {

        AccountsPayableService.pagadas(function(response){
            data = response;
            $scope.tableParams.total = data.length;
         });

        
      

    }
    else {
            AccountsPayableService.query(function(response){
            data = response;
            $scope.tableParams.total = data.length;
        });
    }

	

	$scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.receivable_orders = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);



    $scope.$watch('search',function(query){

         $scope.receivable_orders = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.receivable_orders.length;

    });

    $scope.showPayments= function(receivable_order){

    	SaleOrderService.get({ SaleOrderId: receivable_order.SaleOrderId},function(response) {
    		$scope.showPaymentForm = true;
    		$scope.sale_order = response;
            
    	});
    };

};