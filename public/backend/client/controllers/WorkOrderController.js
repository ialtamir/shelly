function WorkOrderDetailCtrl($scope,tokenHandler,$http,$routeParams){

    $scope.currentTokenInfo = tokenHandler.info();
    
	if($routeParams.WorkOrderId >0 ){ 

        $scope.WorkOrderId = $routeParams.WorkOrderId; 
    	
   	}


};

function WorkOrderQuoteCtrl($scope,$http,$routeParams,tokenHandler,WorkOrderService,QuoteService,ngTableParams,$filter,$location){

    var data =[];
    $scope.currentTokenInfo = tokenHandler.info();

    if($routeParams.WorkOrderId >0 ){ 

        $scope.WorkOrderId = $routeParams.WorkOrderId; 
        
    }


    QuoteService.readyForWorkOrder({},function(response){
        data = response;
        $scope.tableParams.total = data.length;
                
    });

     $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        $scope.quotes = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);


    $scope.createFromQuote = function(quote){

        WorkOrderService.createFromQuote({ QuoteId: quote.QuoteId },{},function(response){

           $location.path("/trabajos/"+response.WorkOrderId );
           //console.log(response);

        },function(){

            $notification.error("Ocurrio un error", response.data.message);

        });
    };
              


};


function WorkOrderCtrl($scope,$resource,$http,$location,tokenHandler,WorkOrderService,ngTableParams,$filter,$notification){

	var data =[];

	$scope.customers = [];
	$scope.work_orders = [];
	$scope.currentTokenInfo = tokenHandler.info();
 
    $scope.currentPath = $location.path();

	
     if($scope.currentPath === "/trabajos/cerrados/") {

        WorkOrderService.getByStatus({ WorkOrderStatus : 'Cerrada'},{},function(response){

            data = response;
            $scope.tableParams.total = data.length;

        });
        


    }
    else {

         WorkOrderService.getByStatus({ WorkOrderStatus : 'Abierta'},{},function(response){

            data = response;
            $scope.tableParams.total = data.length;

        });
    }



    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.work_orders = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.work_orders = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.work_orders.length;

    });



    $scope.openWorkOrder = function(work_order){
        work_order.$openWork({ WorkOrderId : work_order.WorkOrderId},function(){
                $notification.success( work_order.WorkOrderFolio, 'El trabajo se ha iniciado correctamente');
            },
            function(){

            }
        );
    };

    $scope.reOpenWorkOrder = function(work_order){
        work_order.$openWork({ WorkOrderId : work_order.WorkOrderId},function(){
                $notification.success( work_order.WorkOrderFolio, 'El trabajo se ha reabierto correctamente');
            },
            function(){

            }
        );
    };

    $scope.closeWorkOrder = function(work_order){
        work_order.$closeWork({ WorkOrderId : work_order.WorkOrderId},function(){
                $notification.success( work_order.WorkOrderFolio, 'El trabajo se ha cerrado correctamente');
            },
            function(){

            }
        );
    };

    $scope.deleteWorkOrder  = function (work_order){
    
        work_order.$remove({ WorkOrderId : work_order.WorkOrderId },function (){
            
            var index = data.indexOf(work_order,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
               
                $notification.success(work_order.WorkOrderFolio, 'La orden de trabajo se elimino correctamente');
                $scope.tableParams.total = data.length;
            }
            
        },function (){
              $notification.error('Eliminación incorrecta', 'La orden de trabajo no se pudo eliminar');
        });


    };
};