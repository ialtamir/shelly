 var Services =angular.module('erp.services', ['ngResource','services.authentication']).
  value('version', '0.1');

    Services.factory( 'AccountService', function($resource){
        return $resource('api/account/:id',{},{
            'configuration' : { method :'GET' , url: 'api/account/configurations/'},
            
            'update_pkey_password' : { method : 'POST', url :'api/account/:id/update_pkey_password/'}
        });
    });
    
    


    Services.factory( 'UserService', function($resource){
        return $resource('api/users/:UserId',{},{
        	'savePassword' : { method :'POST' , url: 'api/users/:UserId/changepassword/'},
            'userScopes' : { method:'GET',url: 'api/users/:UserId/scopes', isArray : true},
            'saveScopes' : { method:'POST',url: 'api/users/:UserId/scopes'}
        });
    });

    Services.factory( 'CustomerService', function($resource){
        return $resource('api/customers/:CustomerId',{},{
            'getContacts' : { method : 'GET', url : 'api/customers/:CustomerId/contacts', isArray : true },
            'getContact' : { method : 'GET', url : 'api/customers/:CustomerId/contacts/:ContactId'},
            'getByCode' : { method : 'GET', url : 'api/customers/by_code/:CustomerCode'},
            'saveContact' : { method: 'POST', url : 'api/customers/:CustomerId/contacts/:ContactId'},
            'saveAddress' : { method: 'POST', url : 'api/customers/:CustomerId/saveAddress/'},
            'deleteContact' : { method: 'DELETE', url: 'api/customers/:CustomerId/contacts/:ContactId'},
            'sales' : { method : 'GET', url : 'api/customers/:CustomerId/sales', isArray : true }
        });
    });
	
    
    Services.factory( 'EmployeeService', function($resource){
        return $resource('api/employees/:EmployeeId',{},{
             'getEmployeesByPosition' : { method: 'GET' , url: 'api/employees/by_position/:EmployeePosition' , isArray:true },
        });
    });
	
	

   
    Services.factory('InventoryService', function($resource){
        return $resource('api/inventories/:InventoryId',{},{
            'closeCode' : { method: 'POST' , url:'api/inventories/close/:InventoryId'},
            'openCode' : { method: 'POST' , url:'api/inventories/open/:InventoryId'},
            'savePayment' : { method:'POST' , url: 'api/inventories/:InventoryId/details/:InventoryDetailId' },
            'inventoryDetails' : { method: 'GET' , url: 'api/inventories/:InventoryId/inventory_details/' , isArray:true },
            'saveInventoryDetails' : { method:'POST' , url: 'api/inventories/:InventoryId/inventory_details/:InventoryDetailId' },
            'getByStatus' : { method: 'GET', url:'api/inventories/by_status/:InventoryStatus', isArray:true}
        });
    });
	
   
    
	Services.factory( 'MaterialService', function($resource){
        return $resource('api/materials/:MaterialId',{},{});
    });

    Services.factory( 'SaleOrderService', function($resource){
        return $resource('api/sale_orders/:SaleOrderId',{},{
            'approved' : { method: 'GET' , url: 'api/sale_orders/approved/' , isArray:true },
            'facturadas' : { method: 'GET' , url: 'api/sale_orders/facturadas/' , isArray:true },
            'canceladas' : { method : 'GET', url: 'api/sale_orders/canceled/:SaleOrderId',isArray:true},
            'completadas' : { method : 'GET', url: 'api/sale_orders/completed/:SaleOrderId',isArray:true},
            'approveOrder' : { method:'POST', url: 'api/sale_orders/:SaleOrderId/approve'},
            'shippingOrder' : { method:'POST', url: 'api/sale_orders/:SaleOrderId/shipping'},
            'cancelOrder'  : { method:'POST', url: 'api/sale_orders/:SaleOrderId/cancel'},
            'getOrderByQuote' : { method: 'GET' , url: 'api/sale_orders/get_by_quote/:QuoteId'},
            'payments' : { method: 'GET' , url: 'api/sale_orders/:SaleOrderId/payments/' , isArray:true },
            'savePayment' : { method:'POST' , url: 'api/sale_orders/:SaleOrderId/payments/:SaleOrderPaymentId' },
            'deletePayment' : { method: 'DELETE' , url: 'api/sale_orders/:SaleOrderId/payments/:SaleOrderPaymentId' },
            'createWorkOrder' : { method: 'POST' , url: 'api/sale_orders/:SaleOrderId/create_work_order/'},
            'facturar' : { method: 'POST' , url: 'api/facturar/:SaleOrderId/'}
            
        });
    });

    Services.factory('WorkOrderService', function($resource){
        return $resource('api/work_orders/:WorkOrderId',{},{
            'createFromQuote' : { method: 'POST' , url:'api/work_orders/from_quote/:QuoteId'},
            'closeWork' : { method: 'POST' , url:'api/work_orders/close/:WorkOrderId'},
            'openWork' : { method: 'POST' , url:'api/work_orders/open/:WorkOrderId'},
            'getByStatus' : { method: 'GET', url:'api/work_orders/by_status/:WorkOrderStatus', isArray:true}
        });
    });

    Services.factory('AccountsPayableService', function($resource){

        return $resource('api/receivable/sale_orders/:SaleOrderId',{},{
            pagadas: { method: 'GET' , url : 'api/receivable/sale_orders/completed/:SaleOrderId' , isArray : true }
        });

        
    });

    Services.factory( 'QuoteService', function($resource){
        return $resource('api/quotes/:QuoteId',{},{
            'procesadas' : { method : 'GET', url: 'api/quotes/processed/:QuoteId',isArray:true},
            'canceladas' : { method : 'GET', url: 'api/quotes/canceled/:QuoteId',isArray:true},
            'completadas' : { method : 'GET', url: 'api/quotes/completed/:QuoteId',isArray:true},
            'approveQuote' : { method:'POST', url: 'api/quotes/:QuoteId/approve'},
            'createSaleOrder' : { method:'POST', url: 'api/quotes/:QuoteId/create_sale_order'},
            'cancelQuote'  : { method:'POST', url: 'api/quotes/:QuoteId/cancel'},
            'emailQuote' : { method: 'POST', url: 'api/quotes/email/:QuoteId'},
            'readyForWorkOrder' : { method : 'GET', url: 'api/quotes/ready_for_wo/:QuoteId',isArray:true},
            'attachments' : { method : 'GET' , url : 'api/quotes/:QuoteId/attachments/' , isArray : true },
            'saveAttachment': { method: 'POST' , url: 'api/quotes/:QuoteId/attachments/:QuoteAttachmentId'},
            'deleteAttachment': { method: 'DELETE' , url: 'api/quotes/:QuoteId/attachments/:QuoteAttachmentId'}
        });
    });

    Services.factory( 'PurchaseOrderService', function($resource){
        return $resource('api/purchase_orders/:PurchaseOrderId',{},{
           'procesadas' : { method : 'GET', url: 'api/purchase_orders/processed/:PurchaseOrderId',isArray:true},
           'approvePurchaseOrder' : { method:'POST', url: 'api/purchase_orders/:PurchaseOrderId/approve'}
        });
    });

    
    Services.factory( 'CategoryService', function($resource){
        return $resource('api/categories/:CategoryId',{},{
            'products' : { method: 'GET' , url: 'api/categories/:CategoryId/products/' , isArray:true }
        });
    });
    
     Services.factory( 'brandService', function($resource){
        return $resource('api/brands/:brandId',{},{
            'products' : { method: 'GET' , url: 'api/brands/:brandId/products/' , isArray:true }
        });
    });
	
	Services.factory( 'PlaceService', function($resource){
        return $resource('api/places/:PlaceId',{},{});
    });
	
	Services.factory( 'LeadService', function($resource){
       return $resource('api/leads/:LeadId',{},{
       'openCode' : { method: 'POST' , url:'api/leads/open/:LeadId'}
      });
   });
    
    Services.factory( 'UnitOfMeasureService', function($resource){
        return $resource('api/ums/:UMId',{},{});
    });
   
	
	Services.factory( 'CurrencyService', function($resource){
        return $resource('api/currencies/:CurrencyId',{},{});
    });
	
	Services.factory( 'SupplierService', function($resource){
        return $resource('api/suppliers/:SupplierId',{},{});
    });

    Services.factory( 'ProductService', function($resource){
        return $resource('api/products/:ProductId',{},{
            'productsByCategory' : { method : 'GET' , url: 'api/products/by_category/:CategoryId' , isArray:true },
            'All' : { method : 'GET' , url: 'api/products/all/' , isArray:true },
            'ecommerce' : { method : 'GET' , url: 'api/products/ecommerce/' , isArray:true },
            'featured' : { method : 'GET' , url: 'api/products/featured/' , isArray:true },
            'variants' : { method : 'GET' , url: 'api/products/variants/' , isArray:true },
            'unidades' : { method : 'GET' , url: 'api/ums/' , isArray:true },
            'feature_groups' : { method : 'GET' , url: 'api/feature_groups/' , isArray:true }

           
        });
    });

    Services.factory('FeatureService', function($resource){
        return $resource('api/feature_groups/:FeatureGroupId',{},{
            'saveFeature' : { method:'POST', url: 'api/feature_groups/:FeatureGroupId/features/'},
            'features' : { method : 'GET' , url: 'api/feature_groups/:FeatureGroupId/features/' , isArray:true }
        });
    });
	
	
	//Payments
	Services.factory( 'PaymentService', function($resource){
        return $resource('api/payments/:PaymentId',{},{
            'payments' : { method: 'GET' , url: 'api/payments/:PaymentId/payments/' , isArray:true },
            'completedPayments' : { method: 'GET' , url: 'api/payments/completed/:PaymentId' , isArray:true },
            'savePayment' : { method:'POST' , url: 'api/payments/:PaymentId/' },
            'deletePayment' : { method: 'DELETE' , url: 'api/payments/:PaymentId' },
            'paymentDetails' : { method: 'GET' , url: 'api/payments/:PaymentId/payment_details/' , isArray:true },
            'savePaymentDetails' : { method:'POST' , url: 'api/payments/:PaymentId/payment_details/:PaymentDetailId' },
            'deletePaymentDetails' : { method: 'DELETE' , url: 'api/payments/:PaymentId/payment_details/:PaymentDetailId' },
        });
    });

    Services.factory( 'CalendarService', function($resource){
        return $resource('api/calendar/:CalendarId',{},{});
    });

    Services.factory( 'EventService', function($resource){
        return $resource('api/events/:EventId',{},{});
    });
	

    Services.factory( 'StockService', function($resource){
       return $resource('api/stock/:StockId',{},{});
    });

    Services.factory( 'ContractTemplateService', function($resource){
        return $resource('api/contract_templates/:CotractTemplateId',{},{});
    });

    Services.factory('SliderService', function($resource){
        return $resource('api/sliders/:id',{},{});
    });
	

	
	
