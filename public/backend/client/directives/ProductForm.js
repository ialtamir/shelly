var directiveModule = angular.module('erp.directives.product',[]);



directiveModule.directive('productForm', ['ProductService','FeatureService','CategoryService','$routeParams','$filter','$notification','$http','$parse', function(ProductService,FeatureService,CategoryService,$routeParams,$filter,$notification,$http,$parse){
	// Runs during compile
	return {

		restrict: 'E', 

		templateUrl: 'client/partials/products/product-form-template.html',
		replace: true,
		
		link: function($scope, iElm, iAttrs, controller) {


			

			$scope.categories =[];
		    $scope.feature_groups = [];
		    $scope.unidades=[];
		    $scope.variants = [];

		  

            var onSelectCallback = $parse(iAttrs.productFormOnSelect);


		    CategoryService.query(function (response){
		        $scope.categories = response;       
		    });



			$scope.product = new ProductService({
		        CategoryId : parseInt($routeParams.CategoryId),        
				IsNew : true,
				variants : [],
				product_variants: [],
		        feature_groups: [],
		        pictures : []
		    });

		    if($routeParams.ProductId > 0)
		    {
		        ProductService.get({ProductId : $routeParams.ProductId},function(response){
		            $scope.product = response;
		        });
		    }


		 
		    ProductService.variants(function(response){

		    	$scope.variants = response;
		               
		    });

		    ProductService.unidades(function(response){

		        $scope.unidades = response;
		               
		    });



		    FeatureService.query(function(response){
		        $scope.feature_groups = response;
		    });

		   

		    $scope.selectFeatures = function(feature_group){
		       
		    	$scope.selected_feature_group	= feature_group;
		    	$scope.selected_feature_group.features =  FeatureService.features({FeatureGroupId : feature_group.FeatureGroupId });
		        		    	
		    }

		    $scope.addFeature = function(feature_group,feature){

		        var newFeature = {
		            FeatureGroupId : feature_group.FeatureGroupId,
		            FeatureName : feature,
		            FeatureSelected : true
		        };

		        FeatureService.saveFeature({FeatureGroupId : feature_group.FeatureGroupId },newFeature,function(){
		             
		             feature_group.features.push(newFeature);
		             feature = "";
		        });
		 
		    };


		    $scope.$watch('product.variants',function(){

		        var arreglo=[];


		        if($scope.product.variants){ arreglo = $scope.product.variants;  }

		            var combinations = [], arg = arreglo || [], max= arreglo.length-1;
		            
		            function helper(arr,i){
		          
		                if(arg[i]){
		                	for (var j=0, l=arg[i].VariantValues.length; j< l; j++)
		                	{
		                		var a = arr.slice(0);

		                        a.push( { 'VariantId': arg[i].VariantId,'VariantValueName' : arg[i].VariantValues[j].VariantValueName})
		                        
		                		if(i==max){

		                			combinations.push(a);

		                		} else {

		                			helper(a, i+1);
		                		}        			
		                	}
		                }
		            }


		        	helper([], 0);
		            

		            if($scope.product.IsNew){
		            var newVariants=[]; 

		            //Creamos las variantes dadas las  combinacones
		            for(var c = 0, len = combinations.length; c < len ; c++){
		               
		                    var name="",code="";
		                    
		                   for(var d= 0, largo = combinations[c].length; d< largo;d++)
		                   {
		                        var  separator = "";

		                        if(d > 0) separator = "/" ; 

		                        name += separator+combinations[c][d].VariantValueName ;
		                        code += separator+combinations[c][d].VariantValueName.substring(0,1);

		                   }
		          
		                 var newVariant = {
		                    "ProductName" : name,
		                    "ProductCode" : code,
		                    "Combinations" : combinations[c]
		                 }

		                 newVariants.push(newVariant);
		              

		            }

		            $scope.product.product_variants = newVariants;
		        }
		        
		       

		  
		    },true);

			$scope.saveProduct = function() {

				$scope.product.$save({ ProductId : $scope.product.ProductId} , function (){
						$notification.success($scope.product.ProductName, 'El articulo se guardo correctamente');
						onSelectCallback( $scope, { $item : $scope.product} );
				}, function(){
					$notification.error($scope.product.ProductName, 'El articulo no se pudo guardar');
				});
			};

			$scope.addVariant = function(variant){

		        
				var index = $scope.product.variants.indexOf(variant,0);

				if(index === -1){
					variant.values = [];
					$scope.product.variants.push(variant);
				}
			};

		    $scope.removeProductVariant = function(variant)
		    {
		        var index = $scope.product.variants.indexOf(variant);

		        if(index === -1){
		             $scope.product.variants.splice(index,1);

		        }
		    };

		    $scope.addPicture = function() {
		        var image = {};
		        $scope.product.pictures.push(image);
		    };

		}
	};
}]);