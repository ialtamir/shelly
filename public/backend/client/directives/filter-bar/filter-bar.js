var directiveModule = angular.module('erp.directives.filter-bar',[]);

directiveModule.directive('filterBar',['$parse',function($parse){
    
    var directive = {

		restrict: 'E',
		replace: true ,
		link: function ($scope, $element, $attrs, $controller){
			
			
			var today = Date.parse('today').toString('yyyy-MM-dd');
			var firstDayOfMonth = Date.today().moveToFirstDayOfMonth().toString('yyyy-MM-dd');
			var month = Date.parse('today').toString('MM');
			var year = Date.parse('today').toString('yyyy');
			var quarterMonth = (Math.floor(month/3)*3)+1;
			var quarter = (Math.floor(month/3))+1;
			var lastQuarter = (quarter > 1) ? quarter - 1 : lastQuarter = 4;
			var quarterStartDate = (quarterMonth < 10) ? year+'-0'+quarterMonth+'-01' : year+'-'+quarterMonth+'-01';
			var lastSevenDaysStart = Date.today().add(-6).days().toString('yyyy-MM-dd');
			var lastThirtyDaysStart = Date.today().add(-29).days().toString('yyyy-MM-dd');
			var lastYearDayStart = Date.today().add(-364).days().toString('yyyy-MM-dd');
			var lastFullWeekStart = Date.today().add(-7).days().last().monday().toString('yyyy-MM-dd');
			var lastFullWeekEnd = Date.today().add(-7).days().last().monday().add(6).days().toString('yyyy-MM-dd');
			var lastFullMonthStart = Date.today().add(-1).months().moveToFirstDayOfMonth().toString('yyyy-MM-dd');
			var lastFullMonthEnd = Date.today().add(-1).months().moveToLastDayOfMonth().toString('yyyy-MM-dd');
			var lastFullQuarterStart = ((((lastQuarter-1)*3)+1) < 10) ? year+'-0'+(((lastQuarter-1)*3)+1)+'-01' : year+'-'+(((lastQuarter-1)*3)+1)+'-01';
			var lastFullQuarterEnd = Date.parse(lastFullQuarterStart).add(2).months().moveToLastDayOfMonth().toString('yyyy-MM-dd');
			var lastFullYearStart = Date.today().add(-1).years().toString('yyyy') + '-01-01';
			var lastFullYearEnd = Date.today().add(-1).years().toString('yyyy') + '-12-31';
			 
			// Prepare JavaScript array with the JSON 
			$scope.selectOptions = [
				{ text : 'Ultimos 365 dias' , startDate : lastYearDayStart , end: today },
				{ text : 'Hoy'   , startDate : today , end: today },
			    { text : 'Este mes'   , startDate : firstDayOfMonth , end: today },
			    { text : 'Este trimestre' , startDate : quarterStartDate , end: today },
			    { text : 'Ultimos 7 dias' , startDate : lastSevenDaysStart , end: today },
			    { text : 'Ultimos 30 dias' , startDate : lastThirtyDaysStart , end: today },
			    { text : 'Ultima semana (Lun a Dom) ' , startDate : lastFullWeekStart , end: lastFullWeekEnd },
			    { text : 'Last full month ' , startDate : lastFullMonthStart , end: lastFullMonthEnd },
			    
			    
			    

			];
			
			$scope.dateSelected = $scope.selectOptions[0] ;
			
			$scope.showFilters = false;
			
            $scope.openFilter = function(toogle){
            	
            	$scope.showFilters = !toogle;	
            };
            
            $scope.applyFilters = function(){
            	
            	$scope.showFilters = false;
            };
            
            $scope.setSelectedDates = function(selectedOption){
            	$scope.dateSelected = selectedOption ;
            	$scope.startDate = selectedOption.startDate;
            		
            };
		}
		,
		templateUrl: 'client/directives/filter-bar/filter-bar-tpl.html'
	};

	return directive;

}]);