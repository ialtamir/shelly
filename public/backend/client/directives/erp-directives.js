'use strict';

/* Directives */
var directiveModule = angular.module('erp.directives',['ui','erp.directives.customer','erp.directives.quote','erp.directives.payment','erp.directives.workorder','erp.directives.product','erp.directives.supplier','erp.directives.customer.crud','erp.directives.um','erp.directives.filter-bar']);

directiveModule.directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
}]);

directiveModule.directive('selectOnClick', function(){
  	return function (scope,element,attrs){
  			element.click(function(){
  				element.select();
  			});
  	};
});

directiveModule.directive('ngFocus', ['$parse', function($parse) {
  return function(scope, element, attr) {
    var fn = $parse(attr['ngFocus']);
    element.bind('focus', function(event) {
      scope.$apply(function() {
        fn(scope, {$event:event});
      });
    });
  }
}]);
 
directiveModule.directive('ngBlur', ['$parse', function($parse) {
  return function(scope, element, attr) {
    var fn = $parse(attr['ngBlur']);
    element.bind('blur', function(event) {
      scope.$apply(function() {
        fn(scope, {$event:event});
      });
    });
  }
}]);

directiveModule.directive('keyFocus', function() {
  return {
    restrict: 'A',
    link: function(scope, elem, attrs) {
      elem.bind('keyup', function (e) {
        // up arrow
        if (e.keyCode == 38) {
          if(!scope.$first) {
            elem[0].previousElementSibling.focus();
          }
        }
        
        // down arrow
        else if (e.keyCode == 40) {
          if(!scope.$last) {
            elem[0].nextElementSibling.focus();
          }
        }
      });
    }
  };
});



directiveModule.directive('keyTab', function() {
  return {
    restrict: 'A',
    link: function(scope, elem, attrs) {
      elem.bind('keyup', function (e) {
        
        switch (e.keyCode ) {
          case 37:
            console.log(this);
            elem[0].previousElementSibling.focus();
            break;
          case 39:
            console.log(this);
            elem[0].nextElementSibling.focus();
            break;
          
          
          default:
            // code
        }
        // up arrow
      });
    }
  };
});

directiveModule.directive('focusIter', function () {

    return function (scope, elem, attrs) {
        var atomSelector = attrs.focusIter;

        elem.on('keyup', atomSelector, function (e) {
            var atoms = elem.find(atomSelector),
                toAtom = null;

            for (var i = atoms.length - 1; i >= 0; i--) {
                if (atoms[i] === e.target) {
                    if (e.keyCode === 37) {
                        toAtom = atoms[i - 1];
                    } else if (e.keyCode === 39) {
                        toAtom = atoms[i + 1];
                    } else if (e.keyCode === 13){
                        //toAtom = atoms[i + 1];
                    }
                    break;
                }
            }

            if (toAtom) toAtom.focus();

        });

        elem.on('keydown', atomSelector, function (e) {
            if (e.keyCode === 37 || e.keyCode === 39)
                e.preventDefault();
        });

    };
});

directiveModule.directive('validPasswordC', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var noMatch = viewValue != scope.pwdform.password.$viewValue
                ctrl.$setValidity('noMatch', !noMatch)
            })
        }
    }
});


directiveModule.directive("confirmButton", function($document, $parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var buttonId, html, message, nope, title, yep;
      
      buttonId = Math.floor(Math.random() * 10000000000);
      
      attrs.buttonId = buttonId;
      
      message = attrs.message || "Are you sure?";
      yep = attrs.yes || "Yes";
      nope = attrs.no || "No";
      title = attrs.title || "";
      
      html = "<div id=\"button-" + buttonId + "\">\n  <span class=\"confirmbutton-msg\">" + message + "</span><br>\n  <button class=\"confirmbutton-yes btn btn-danger\">" + yep + "</button>\n <button class=\"confirmbutton-no btn\">" + nope + "</button>\n</div>";
      
      element.popover({
        content: html,
        html: true,
        trigger: "manual",
        title: title
      });
      
      return element.bind('click', function(e) {
        var dontBubble, pop;
        dontBubble = true;
        
        e.stopPropagation();
        
        element.popover('show');
        
        pop = $("#button-" + buttonId);
        
        pop.closest(".popover").click(function(e) {
          if (dontBubble) {
            e.stopPropagation();
          }
        });
        
        pop.find('.confirmbutton-yes').click(function(e) {
          dontBubble = false;
          
          var func = $parse(attrs.confirmButton);

          //scope.$eval(attrs.confirmButton);

          func(scope);

          scope.$apply();
        });
        
        pop.find('.confirmbutton-no').click(function(e) {
          dontBubble = false;
          
          $document.off('click.confirmbutton.' + buttonId);
          
          element.popover('hide');
        });
        
        $document.on('click.confirmbutton.' + buttonId, ":not(.popover, .popover *)", function() {
          $document.off('click.confirmbutton.' + buttonId);
          element.popover('hide');
        });
      });
    }
  };
});

directiveModule.directive('fileupload', function() {
  return {
    restrict: 'A',
    scope: {
      done: '&',
      progress: '&'
    },
    link: function(scope, element, attrs) {
      var optionsObj = {
        dataType: 'json'
      };

      if (scope.done) {
        optionsObj.done = function() {
          scope.$apply(function() {
            scope.done({e: e, data: data});
          });
        };
      }

      if (scope.progress) {
        optionsObj.progress = function(e, data) {
          scope.$apply(function() {
            scope.progress({e: e, data: data});
          });
        }
      }

      // the above could easily be done in a loop, to cover
      // all API's that Fileupload provides

      element.fileupload(optionsObj);
    }
  };
});

directiveModule.directive('ngBindHtmlUnsafe', ['$sce', function($sce){
    return {
        scope: {
            ngBindHtmlUnsafe: '=',
        },
        template: "<div ng-bind-html='trustedHtml'></div>",
        link: function($scope, iElm, iAttrs, controller) {
            $scope.updateView = function() {
                $scope.trustedHtml = $sce.trustAsHtml($scope.ngBindHtmlUnsafe);
            }

            $scope.$watch('ngBindHtmlUnsafe', function(newVal, oldVal) {
                $scope.updateView(newVal);
            });
        }
    };
}]);














