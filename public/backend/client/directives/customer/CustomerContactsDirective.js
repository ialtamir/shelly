var directiveModule = angular.module('erp.directives.customer.contact',[]);



directiveModule.directive('contactForm', ['CustomerService','$notification', function(CustomerService, $notification){
	// Runs during compile
	return {

		restrict: 'E', 

		templateUrl: 'client/partials/customer/customer-contact-form.html',
		replace: true,
		
		link: function($scope, iElm, iAttrs, controller) {


			$scope.loadCustomerContacts= function(){
				CustomerService.getContact({ CustomerId : $scope.localCustomerId , ContactId : 1 }, function(response){
					$scope.contact = response;
				});
			};

			$scope.$watch('CustomerId', function(newValue, oldValue, scope) {
				if(newValue){
					$scope.localCustomerId = newValue;
					$scope.loadCustomerContacts();
				}
			});

			$scope.saveContact = function(){
				$scope.contact.$saveContact({ CustomerId : $scope.localCustomerId , ContactId : $scope.contact.CustomerContactId } , function(){
					$notification.success('Se guardo correctamente','El contacto se guardo correctamente');
					
				});
			};

			$scope.cancelContact = function(){
				$scope.showCustomerContactModal = false;
			};

		}
	};
}]);