var directiveModule = angular.module('erp.directives.customer.crud',[]);

directiveModule.directive('customerCrud',['$filter','CustomerService','currentUser','ngTableParams','$notification',function($filter,CustomerService,currentUser,ngTableParams,$notification){
	
	var directive = {

		restrict: 'E',
		replace: true ,
		link: function ($scope) {

	var data = [];
    
	$scope.customer = { IsNew : true };
	$scope.customers = [];
    $scope.userInfo = currentUser;

	CustomerService.query(function(response){

        data = response;
        $scope.tableParams.total = data.length;
               
    });

    $scope.tableParams = new ngTableParams({
           
        page: 1,            // show first page
        total: data.length , // length of data
        count: 10          // count per page

    });

    $scope.$watch('tableParams', function(params) {
        // slice array data on pages

        $scope.customers = data.slice(
            ( params.page - 1) * params.count,
            params.page * params.count
        );

    }, true);

    $scope.$watch('search',function(query){

         $scope.customers = $filter("filter")(data, query);
         $scope.tableParams.total = $scope.customers.length;
         $scope.tableParams.page = 1;

    });

    $scope.refresh = function(){
        CustomerService.query(function(response){

            data = response;
            $scope.tableParams.total = data.length;
            $scope.search = '';

        });
    };


    $scope.add = function(){
        
        $scope.customer = new CustomerService();
    	$scope.customer.IsNew = true;

     	$scope.showCustomerModal = true;
         
    };

    $scope.edit = function(user){
       
    	$scope.customer = user;
        $scope.showCustomerModal = true;
        
    };

    $scope.cancel = function(){
        
        $scope.customer = {};
        $scope.showCustomerModal = false;

    };

    $scope.save = function(){

        $scope.customer.$save({ CustomerId:$scope.customer.CustomerId },function(){

        if($scope.customer.CustomerId   >   0   &&  $scope.customer.IsNew){
               
            data.push($scope.customer);

            $notification.success($scope.customer.CustomerName, 'El cliente se guardo correctamente');
            
            $scope.customer = {};
            $scope.tableParams.total = data.length;
        }
            
    	$scope.showCustomerModal = false;
            
        },function(){        	 
             $notification.error('Eliminación incorrecta', 'El cliente no se pudo guardar');
        });
        
    };

    $scope.delete = function(customer){

        customer.$remove({ CustomerId : customer.CustomerId },function (){
            
            var index = data.indexOf(customer,0); 
            
            if (index != -1 )
            {

                data.splice(index,1) ;
                $notification.success($scope.customer.CustomerName, 'El cliente se elimino correctamente');
                
                $scope.tableParams.total = data.length;
            }
            
        },function (){
             
     
              $notification.error('Eliminación incorrecta', 'El cliente no se pudo eliminar');
        });
    };

    $scope.editContacts = function(customer){
        $scope.customer = customer;
        $scope.CustomerId = customer.CustomerId;
        $scope.showCustomerContactModal = true;
    };


}
		,
		templateUrl: 'client/partials/customers.html'
	}

	return directive;

}]);
