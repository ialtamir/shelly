var directiveModule = angular.module('erp.directives.customer',['erp.directives.customer.contact']);

directiveModule.directive('customerDirective',['CustomerService','currentUser','$notification','$parse',function(CustomerService,currentUser,$notification,$parse){
	
	var directive = {

		restrict: 'E',
		replace: true ,
		link: function ($scope, $element, $attrs, $controller){

			$scope.showCustomerModal = false;  
			$scope.customerAdded = {};
			
			var onAddCallback  = $parse($attrs.customerOnAdd);

			$scope.clearCustomerForm = function() {
        		$scope.customer = {};
      		};

      		$scope.addCustomer = function(){
        
        		$scope.customer = new CustomerService();
    			$scope.customer.IsNew = true;
     			$scope.showCustomerModal = true;  

   			 };

		    $scope.editCustomer = function(user){
		       
		    	$scope.customer = user;
		        $scope.showCustomerModal = true;
		        
		    };

		    $scope.cancelCustomer = function(){
		        
		        $scope.customer = {};
		        $scope.showCustomerModal = false;

		    };

		    $scope.saveCustomer = function(){

		        $scope.customer.$save({ CustomerId:$scope.customer.CustomerId },function(){

			        	if($scope.customer.CustomerId > 0 &&  $scope.customer.IsNew){

			            	$notification.success($scope.customer.CustomerName, 'El cliente se guardo correctamente');	
			            	$scope.selectedCustomer = $scope.customer;
			            	onAddCallback( $scope, { $customer: $scope.customer });
			        	}
		            
			    		$scope.showCustomerModal = false;
			        	
			        },
			        function(){        	 
			             $notification.error('Eliminación incorrecta', 'El cliente no se pudo guardar');
			       	});		       
		    };
		}
		,
		templateUrl: 'client/partials/customer/customer-form-template.html'
	}

	return directive;

}]);
