var directiveModule = angular.module('erp.directives.workorder',[]);


directiveModule.directive('workOrderForm', ['WorkOrderService','SaleOrderService','QuoteService','EmployeeService','CurrencyService','currentUser','$notification','$http', function(WorkOrderService,SaleOrderService,QuoteService,EmployeeService,CurrencyService,currentUser,$notification,$http,$resource){
	// Runs during compile
	return {

		restrict: 'E', 

		templateUrl: 'client/partials/work_orders/work-order-form-template.html',
		replace: false,
		
		link: function($scope, iElm, iAttrs, controller) {

			$scope.supervisors=[];
			$scope.employees = [];
			$scope.materials = [];
			$scope.currencies =[];

			$scope.methods = [{ MethodId: 1, MethodName: "Efectivo"},{ MethodId: 2, MethodName: "Tarjeta"},{ MethodId: 3, MethodName: "Cheque"}];


			$scope.dateOptions = {
			   	changeYear:true,

			   	changeMonth:true,
			   	dateFormat: 'yy-mm-dd'
				
			};

			 CurrencyService.query(function(response){
		        $scope.currencies = response;
		    });



			EmployeeService.getEmployeesByPosition({ EmployeePosition : 'Supervisor'},function(response){

				$scope.supervisors = response;
				               
			});

			EmployeeService.query(function(response){

				
				$scope.employees = response;
				               
			});

			


			$scope.work_order = new WorkOrderService({
				WorkOrderStatus : "Nueva",
				WorkOrderCreationDate : new Date(),
				employees : [],
				materials : [],
				notes: [],
				expenses: []
			});


			$scope.loadWorkOrder = function(){

				if($scope.localWorkOrderId > 0){

				WorkOrderService.get(
					{ WorkOrderId: $scope.localWorkOrderId },
					function(response){
				    	
				    	$scope.work_order = response;
				    	$scope.work_order.deleted_materials = [];
				    	$scope.work_order.deleted_employees = [];
				    	$scope.work_order.deleted_expenses = [];		
				    });
				};
			}

			
			$scope.$watch('WorkOrderId', function(newValue, oldValue, scope) {
				if(newValue){
					$scope.localWorkOrderId = newValue;
					$scope.loadWorkOrder();
				}
			});


			$scope.onMaterialSelection = function(material){

				var idx = -1;


				for (var i = $scope.work_order.materials.length - 1; i >= 0; i--) {
					
					if ( $scope.work_order.materials[i].MaterialId === material.MaterialId ) 
					{
						idx =1;
						break;
					};
				};


				if(idx === -1){
					material.IsNew = true;
					
					$scope.work_order.materials.push(material);
					
				}
				else{
					$notification.warning("Material", "El material ya existe en la lista")
				}

				

				$scope.showMaterialSearchModal=false;

			};

			$scope.removeMaterial = function(material)
			{
				if(material.IsNew) {

				   	var index = $scope.work_order.materials.indexOf(material,0); 
				            
		            if (index != -1 )
		            {

		                $scope.work_order.materials.splice(material,1) ;
		                
		             }  

		    	}
		    	else {

		    		$scope.work_order.deleted_materials.push(material);

		            var index = $scope.work_order.materials.indexOf(material,0); 
		            
		            if (index != -1 )
		            {
		                $scope.work_order.materials.splice(index,1) ;
		            }  

		    	}
			}


			$scope.removeExpense = function(expense)
			{
				if(expense.IsNew) {

				   	var index = $scope.work_order.expenses.indexOf(expense,0); 
				            
		            if (index != -1 )
		            {

		                $scope.work_order.expenses.splice(expense,1) ;
		                
		             }  

		    	}
		    	else {

		    		$scope.work_order.deleted_expenses.push(expense);

		            var index = $scope.work_order.expenses.indexOf(expense,0); 
		            
		            if (index != -1 )
		            {
		                $scope.work_order.expenses.splice(index,1) ;
		            }  

		    	}
			}


			$scope.materialSearch = function (viewValue) {
				return $http.get('api/materials/search/'+viewValue).then(function(response){
					return response.data;
				});
			};

			$scope.$watch('materialSearchFilter', function(newValue, oldValue, scope) { 
				
				return $http.get('api/materials/search/'+newValue).then(function(response){
					$scope.materials =  response.data;
				});
			});

			$scope.addNote = function(){

				var newNote1 = { WorkOrderNoteText : $scope.newNote.WorkOrderNoteText , WorkOrderNoteCDate : new Date(), UserName : currentUser.information().UserName  }
				
				$scope.work_order.notes.push( newNote1);
				$scope.newNote.WorkOrderNoteText = "";
			};

			$scope.addNewExpense = function(){

				$scope.currentExpense = { WorkOrderExpenseDate : new Date(), UserName : currentUser.information().UserName,IsNew : true  }
				$scope.showExpenseModal = true;
				
			};

			$scope.editExpense = function(expense){
				$scope.currentExpense = expense;
				$scope.showExpenseModal = true;
			}

			$scope.addExpense = function(){

				if($scope.currentExpense.IsNew){
					$scope.work_order.expenses.push( $scope.currentExpense );
				}

				$scope.showExpenseModal = false;
			}

			$scope.saveWorkOrder = function(){
				$scope.work_order.$save({ WorkOrderId : $scope.work_order.WorkOrderId},function(){
						$notification.success($scope.work_order.WorkOrderFolio, 'El trabajo se guardo correctamente');
					},
					function(){

					}
				);
			};

			$scope.openWorkOrder = function(){
            	$scope.work_order.$openWork({ WorkOrderId : $scope.work_order.WorkOrderId},function(){
						$notification.success($scope.work_order.WorkOrderFolio, 'El trabajo se ha iniciado  correctamente');
					},
					function(){

					}
				);
            };

            $scope.reOpenWorkOrder = function(){
            	$scope.work_order.$openWork({ WorkOrderId : $scope.work_order.WorkOrderId},function(){
						$notification.success($scope.work_order.WorkOrderFolio, 'El trabajo se ha iniciado nuevamente');
					},
					function(){

					}
				);
            };



            $scope.closeWorkOrder = function(){
            	$scope.work_order.$closeWork({ WorkOrderId : $scope.work_order.WorkOrderId},function(){
						$notification.success($scope.work_order.WorkOrderFolio, 'El trabajo se ha cerrado  correctamente');
					},
					function(){

					}
				);
            };

			$scope.addEmployee = function(employee){
				$scope.work_order.employees.push(employee);
			};

			$scope.removeEmployee = function(employee){

                if(confirm('Desea eliminar este colaborador de la lista?')){
					if(employee.IsNew){

						var index = $scope.work_order.employees.indexOf(employee,0); 
					            
					            if (index != -1 )
					            {

					                $scope.work_order.employees.splice(employee,1) ;
					                
					             }  

					}else{

						$scope.work_order.deleted_employees.push(employee);

			            var index = $scope.work_order.employees.indexOf(employee,0); 
			            
			            if (index != -1 )
			            {
			                $scope.work_order.employees.splice(index,1) ;
			            }  
					};
				};
			};			
		}
	};
}]);