
var directiveModule = angular.module('erp.directives.um',[]);

directiveModule.directive('unitOfMeasure',['UnitOfMeasureService','$notification','ngTableParams','$parse',function(UnitOfMeasureService,$notification,ngTableParams,$parse){
	
	var directive = {

		restrict: 'E',
		replace: true ,
		link: function ($scope, $element, $attrs, $controller){

			var data = [];
            
            $scope.showUMModal = false;  			
            
            var onSelectCallback = $parse($attrs.umOnSelect);
            var onAddCallback  = $parse($attrs.umOnAdd);
            
            UnitOfMeasureService.query(function(response){
                
                data = response;
                $scope.tableParams.total = data.length;
            });
            
             $scope.tableParams = new ngTableParams({

                page: 1,            // show first page
                total: data.length , // length of data
                count: 10          // count per page

            });

            $scope.$watch('tableParams', function(params) {
                // slice array data on pages

                $scope.ums = data.slice(
                    ( params.page - 1) * params.count,
                    params.page * params.count
                );

            }, true);

   

            $scope.openCrud = function(){
                $scope.showUnitsOfMeasureModal = true;    
            };
            
            $scope.closeCrud = function(){
                $scope.showUnitsOfMeasureModal = false;  
            };
            
            
			$scope.clear = function() {
        		$scope.um = { };
      		};

      		$scope.add = function(){
        
        		$scope.um = new UnitOfMeasureService();
    			$scope.um.IsNew = true;
     			$scope.showUMModal = true;  

   			 };

		    $scope.edit = function(um){
		       
		    	$scope.um = um;
		        $scope.showUMModal = true;
		        
		    };

		    $scope.cancel = function(){
		        
		        $scope.um = {};
		        $scope.showUMModal = false;

		    };

		    $scope.save = function(){

		        $scope.um.$save({ UMId:$scope.um.UMId },function(){

			        	if($scope.um.UMId > 0 &&  $scope.um.IsNew){

			            	$notification.success($scope.um.UMName, 'La unidad se agrego correctamete');	
                            $scope.ums.push($scope.um);
                            onAddCallback( $scope, { $um: $scope.um });
			        	}
		            
			    		$scope.showUMModal = false;
			        	
			        },
			        function(){        	 
			             $notification.error('Eliminación incorrecta', 'La unidad no se pudo guardar');
			       	});		       
		    };
            
            $scope.select = function(um){
                $scope.showUnitsOfMeasureModal = false;
                onSelectCallback( $scope, { $um : $scope.um} );  
                
            };
		}
		,
		templateUrl: 'client/directives/um/um-tpl.html'
	}

	return directive;

}]);