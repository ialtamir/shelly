var directiveModule = angular.module('erp.directives.quote',[]);

directiveModule.directive('saleOrderForm',['SaleOrderService','PlaceService','ProductService','tokenHandler','currentUser','CurrencyService','$notification','$http','$routeParams',function(SaleOrderService,PlaceService,ProductService,tokenHandler,currentUser,CurrencyService,$notification,$http,$resource,$routeParams){
	
	var directive = {

		restrict: 'E',
		replace: true ,
		link: function ($scope, $element, $attrs, $controller){

					
					$scope.customers = [];				    
					$scope.sale_orders = [];
				    //$scope.currencies =[{ CurrencyId: 1, CurrencyName: "MXN $"}];
				    $scope.contracts = [{ ContractId: 1, ContractName: "** Contrato 1"},{ ContractId: 2, ContractName: "** Contrato 2"},{ ContractId: 3, ContractName: "** Contrato 3"}];
                    $scope.delivery_times = [{ DeliveryId: 1, DeliveryName:"1 Mes" }, { DeliveryId: 2, DeliveryName:"2 Meses" }, { DeliveryId: 3, DeliveryName:"3 Meses" }, { DeliveryId: 4, DeliveryName:"4 Meses" }, { DeliveryId: 5, DeliveryName:"5 Meses"},{ DeliveryId: 6, DeliveryName:"6 Meses" }];
                    $scope.iva_codes = [{ TaxId : 0 , TaxName: "No aplica"},{ TaxId : 11 , TaxName: "IVA 11%" },{ TaxId : 16 , TaxName: "IVA 16%"}];
					$scope.userInfo = currentUser;
					
				    $scope.dateOptions = {
			        	changeYear:true,
			        	changeMonth:true,
			       		dateformat: "yy-mm-dd"
			    	}
			    	
			    	$scope.currentTokenInfo = tokenHandler.info();

					$scope.sale_order = new SaleOrderService({
						SaleOrderStatus : "Pendiente",
				        SaleOrderDate : new Date().yyyymmdd(),
				        SaleOrderFolio : 'SO-##',
                        CurrencyId: 2,
						items: [{ "SaleOrderDetailCode" : "", "SaleOrderDetailDescripton" : "","SaleOrderDetailQTY" : 1, "SaleOrderDetailPrice" : 0,"SaleOrderDetailTax": 0 }],
				        deleted_items : [],
				        SaleOrderType: "Normal"

				    });

				    $scope.showCustomerModal = function (){
				        $scope.showCustModal = true;
				    };
				    
				    $scope.showShippingModal = function (){
				        $scope.showShipping = true;
				    };


				    $scope.$watch('SaleOrderId', function(newValue, oldValue, scope) {
						if(newValue){
							//$scope.localSaleOrderId = newValue;
							$scope.loadSaleOrder();
						}
					});
				    

				    $scope.loadSaleOrder = function(){

				    	if($scope.SaleOrderId >0 ){ 

				    	SaleOrderService.get({ SaleOrderId: $scope.SaleOrderId },function(response) {

				    		$scope.sale_order = response;
				            $scope.sale_order.deleted_items= [];

				    		if($scope.sale_order.customer){
				    			$scope.selectedCustomer = $scope.sale_order.customer;
				    		};

				    	});

				   	}

				    }

				     CurrencyService.query(function(response){
				        $scope.currencies = response;
				    });


					

				   	PlaceService.query(function(response){
				   		$scope.places = response;
				   	});

				   	ProductService.unidades(function(response){

				        $scope.unidades = response;
				               
				    });


				    $scope.customerSearch = function (viewValue) {
				    	return $http.get('api/customers/search/'+viewValue).then(function(response){
				    		//return limitToFilter(response.data,15);
				    		return response.data;
				    	});
				    };

					$scope.productSearch = function (viewValue) {
						return $http.get('api/products/search/'+viewValue).then(function(response){
				      		//return limitToFilter(response.data, 15);
				      		return response.data;
				    	});
					};

				    $scope.removeSaleOrderItem = function(item){

				    	if(item.IsNew) {

				    		var index = $scope.sale_order.items.indexOf(item,0); 
				            
				            if (index != -1 )
				            {

				                $scope.sale_order.items.splice(index,1) ;
				                
				             }  
				    	}
				    	else {

				    		$scope.sale_order.deleted_items.push(item);

				            var index = $scope.sale_order.items.indexOf(item,0); 
				            
				            if (index != -1 )
				            {

				                $scope.sale_order.items.splice(index,1) ;
				             }  

				    	}
				    };

				    $scope.removeProductObject = function(item){

				    		item.Product = "";
				    };

				   $scope.onProductSelection = function(item){
				   		if(item.Product){
				   			item.SaleOrderDetailPrice = item.Product.ProductPrice;
				   			item.SaleOrderDetailUM = item.Product.ProductUM;
				   		}
				   };

				       /// Start Interface && controls handling

				    $scope.showItemExtendEditModal = function(item){

				        $scope.showEditModal = true;
				        $scope.editQuoteItem = item;

				    };

				    $scope.hideItemExtendEditWindow = function(){
				        $scope.showEditModal = false;
				    };
				    
				    $scope.hideShippingWindow = function(){
				        $scope.showShipping = false;
				    };

				     $scope.onNewProductSelection = function(item){
				        $scope.currentItem = item;
				        $scope.product = new ProductService({ ProductName : item.Product});
				             
				        $scope.showProductModal = true;
				     };

				    $scope.selectProduct = function(item){
				        
				        $scope.currentItem.Product = $scope.product;
				         $scope.onProductSelection($scope.currentItem);  
				        $scope.showProductModal = false;
				    }; 

				    $scope.onItemNotFound = function(item){

				        item.notFound = true;
				        item.IsManualItem = false;
				        
				    };

				    $scope.onItemMatchesFound = function(item){
				        item.notFound = false;
				    };
				    
				    $scope.itIsManualItem = function(item){
				        return item.IsManualItem && item.Product.length > 0;
				    };

				    $scope.setAsManualItem = function(item){
				        item.IsManualItem = true;
				    };

					$scope.selectAnotherCustomer = function (){
						$scope.selectedCustomer = '';
					};

					$scope.addItem = function (){
						var item = { "SaleOrderDetailCode" : "", "SaleOrderDetailDescripton" : "","SaleOrderDetailQTY" : 1, "SaleOrderDetailPrice" : 0,"SaleOrderDetailTax": 0 , "IsNew" : true };
						$scope.sale_order.items.push(item);
					};

					$scope.saveSaleOrder = function() {

						if($scope.selectedCustomer.CustomerId){

							$scope.sale_order.CustomerId = $scope.selectedCustomer.CustomerId;
						}

						$scope.sale_order.$save({ SaleOrderId : $scope.sale_order.SaleOrderId} , function (){
								$notification.success($scope.sale_order.SaleOrderFolio, 'La orden se guardo correctamente');
						});
					};

					$scope.printOrder = function() {

						if($scope.selectedCustomer.CustomerId){

							$scope.sale_order.CustomerId = $scope.selectedCustomer.CustomerId;
						}

						$scope.sale_order.$save({ SaleOrderId : $scope.sale_order.SaleOrderId} , function (){
							
							$notification.success($scope.sale_order.SaleOrderFolio, 'La orden se guardo correctamente');
							var win = window.open('api/sale_orders/report/' + $scope.sale_order.SaleOrderId+'?access_token='+$scope.currentTokenInfo.access_token);
							win.focus();
							
						});
					};

					$scope.approveOrder = function () {

				        if($scope.sale_order.SaleOrderStatus!= 'Aprobada'){

				            $scope.sale_order.SaleOrderStatus = 'Aprobada';

				            $scope.sale_order.$approveSaleOrder({SaleOrderId : $scope.sale_order.SaleOrderId },function(response){

				                $notification.success("Venta aprobada","La venta se aprobo correctamente");

				            });
				        }
				        else
				        {
				            $notification.error("Error al aprobar","La venta ya ha sido aprobada anteriormente");
				        }
				    };
				    
				    $scope.saveShipping = function () {

				        if($scope.sale_order.SaleOrderStatus == 'Aprobada'){

				            

				            $scope.sale_order.$shippingOrder({SaleOrderId : $scope.sale_order.SaleOrderId },function(response){

				                $notification.success("Correcto","La guia se guardo correctamente");

				            });
				        }
				        else
				        {
				            $notification.error("Error","No se pudo guardar el numero de guía");
				        }
				    };



				    $scope.cancelOrder = function () {

				        if($scope.sale_order.SaleOrderStatus!= 'Cancelada'){

				            $scope.sale_order.SaleOrderStatus = 'Cancelada';

				            $scope.sale_order.$cancelOrder({SaleOrderId : $scope.sale_order.SaleOrderId },function(response){

				                $notification.success("Venta cancelada","La venta fue cancelada");

				            });
				        }
				        else
				        {
				            $notification.error("Error al cancelar","La venta ya ha sido cancelada anteriormente");
				        }
				    };

					var calculateTotals = function(){

						var taxes = 0;
				        var subTotal = 0;
				        var discounts = 0;

						for(var i=0, len=$scope.sale_order.items.length; i < len ; i ++)
						{
							var total = $scope.sale_order.items[i].SaleOrderDetailPrice * $scope.sale_order.items[i].SaleOrderDetailQTY;

							$scope.sale_order.items[i].SaleOrderDetailTotal = total;
                            


							var discount = total * ( $scope.sale_order.items[i].SaleOrderDetailDiscount / 100);

							$scope.sale_order.items[i].SaleOrderDetailDiscountAmount = discount;

            				var tax = total * ( $scope.sale_order.items[i].SaleOrderDetailTax / 100);

            				$scope.sale_order.items[i].SaleOrderDetailTaxAmount = tax;

            				subTotal += total;

							taxes = taxes + tax ;
							
							 $scope.sale_order.items[i].SaleOrderDetailSubTotal = (total+tax)-discount;

				            if($scope.sale_order.items[i].SaleOrderDetailDiscount > 0){
				               discounts = discounts + discount;                 
				            }



	

						};

						$scope.sale_order.SaleOrderSubtotal = subTotal;
				        $scope.sale_order.SaleOrderTaxes = taxes;
				        $scope.sale_order.SaleOrderDiscounts = discounts;
						$scope.sale_order.SaleOrderTotal = $scope.sale_order.SaleOrderSubtotal +  $scope.sale_order.SaleOrderTaxes -  $scope.sale_order.SaleOrderDiscounts;

				        if($scope.sale_order.SaleOrderExchangeRate){

				        	if($scope.sale_order.CurrencyId == 1){
				            	$scope.sale_order.SaleOrderTotalDlls= $scope.sale_order.SaleOrderTotal / $scope.sale_order.SaleOrderExchangeRate;
				            }
				            else
				            {
				            	$scope.sale_order.SaleOrderTotalDlls= $scope.sale_order.SaleOrderTotal * $scope.sale_order.SaleOrderExchangeRate;
				            }
				        }

				        

					};

					$scope.$watch('sale_order.items', calculateTotals , true);

					$scope.$watch('sale_order.SaleOrderExchangeRate',calculateTotals,true);
					$scope.$watch('sale_order.CurrencyId',calculateTotals,true);
            
            $scope.keypressCallback = function($event) {
                console.log("Enter");
                $event.preventDefault();
            };

		}
		,
		templateUrl: 'client/partials/sale_orders/sale-orders-form-template.html'
	}

	return directive;

}]);