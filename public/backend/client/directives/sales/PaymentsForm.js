var directiveModule = angular.module('erp.directives.payment',[]);

directiveModule.directive('paymentForm',['SaleOrderService','currentUser','tokenHandler','$notification','$http',function(SaleOrderService,currentUser,tokenHandler,$notification,$http,$resource){
	
		// Runs during compile
		return {
			restrict : 'E',
			templateUrl: 'client/partials/sale_orders/payment-form-template.html',
			replace: false,
			
			link: function($scope, iElm, iAttrs, controller) {

				$scope.newpayment = { SaleOrderPaymentDate : new Date().yyyymmdd(), SaleOrderPaymentAmount : 0 };
				$scope.payments = [];
				$scope.currentProgress = 0;
				$scope.PaymentTotalAmount = 0;
				$scope.paymentMethods = [{ PaymentMethodId: 1, PaymentMethodName: "Efectivo"},{ PaymentMethodId: 2, PaymentMethodName: "Tarjeta"},{ PaymentMethodId: 3, PaymentMethodName: "Cheque"}];
				
				$scope.currentTokenInfo = tokenHandler.info();

				$scope.dateOptions = {
			        changeYear:true,
			        changeMonth:true,
			        dateformat: "yy-mm-dd"
			    }
							
				$scope.addPayment = function(){

   					if ( ($scope.PaymentTotalAmount + parseFloat($scope.newpayment.SaleOrderPaymentAmount) ) <= $scope.sale_order.SaleOrderTotal) {

						SaleOrderService.savePayment({ SaleOrderId: $scope.localSaleOrderId},$scope.newpayment,function(response){

							$scope.payments.push(response);
							$scope.newpayment = { SaleOrderPaymentDate : new Date().yyyymmdd() , SaleOrderPaymentAmount : 0 };
							$notification.success("Pago correcto", 'El pago se guardo correctamente!');
							
						});
					}
					else{
						
						$notification.error("Pago invalido", "La cantidad del pago excede el monto total a pagar");
						return false;
					};

				};

				$scope.editPayment = function(payment){
					//original = angular.copy(payment);
					original = angular.copy(payment);					
					payment.inEditMode=true;
			
				};

				$scope.cancelEditPayment = function(payment){
					//payment.SaleOrderPaymentAmount = original.SaleOrderPaymentAmount;
					payment.inEditMode = false;
				};
				

				$scope.updatePayment = function(payment){

					if ( ($scope.PaymentTotalAmount  ) <= $scope.sale_order.SaleOrderTotal) {

						SaleOrderService.savePayment({ SaleOrderId: payment.SaleOrderId , SaleOrderPaymentId : payment.SaleOrderPaymentId},payment,function(){

							$notification.success("Pago", 'El pago se actualizo correctamente!');
							
						});

					}else{
						
						$notification.error("Pago invalido", "La cantidad del pago excede el monto total a pagar");
						return false;
					};;
				};



				$scope.removePayment = function(payment){
					SaleOrderService.deletePayment({ SaleOrderId: payment.SaleOrderId , SaleOrderPaymentId : payment.SaleOrderPaymentId},function(){

						var index = $scope.payments.indexOf(payment,0); 
                            
                            if (index != -1 ) $scope.payments.splice(index,1) ;   
     
                       
						$notification.success("Pago", 'El pago se elimino correctamente!');

					});
				};

				$scope.loadPayments = function (){
					SaleOrderService.payments({ SaleOrderId : $scope.localSaleOrderId },function (response){
						$scope.payments = response;
					});
				};

				$scope.createWorkOrder = function(){
					
					SaleOrderService.createWorkOrder({ SaleOrderId: $scope.localSaleOrderId},{},function(response){
						$scope.quote.WorkOrderId = response.WorkOrderId;
						$notification.success("Orden de trabajo #" +response.WorkOrderId,"Orden de trabajo creada correctamente")
					});
				};


				var calculateBalance = function (){

					if($scope.sale_order){

						var Balance = $scope.sale_order.SaleOrderTotal;
						var PaymentTotal = 0;
						$scope.currentProgress = 0;


						if($scope.payments.length > 0){
							
							for(var i=0, len=$scope.payments.length; i < len ; i ++)
							{

									Balance -= parseFloat($scope.payments[i].SaleOrderPaymentAmount);
									PaymentTotal += parseFloat($scope.payments[i].SaleOrderPaymentAmount);

									$scope.payments[i].SaleOrderPaymentBalance = Balance;

							};
						    
							$scope.currentProgress =Math.floor((PaymentTotal / $scope.sale_order.SaleOrderTotal)  * 100);
							$scope.PaymentTotalAmount = PaymentTotal;

						};
					};
					
				};

				$scope.$watch('payments',calculateBalance,true);

				$scope.$watch('sale_order.SaleOrderId', function(newValue, oldValue, scope) {
					if(newValue){
						$scope.localSaleOrderId = newValue;
						$scope.loadPayments();
					}
				});

				
			}
		};
	
}]);

