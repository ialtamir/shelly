var directiveModule = angular.module('erp.directives.supplier',[]);

directiveModule.directive('supplierDirective',['SupplierService','currentUser','$notification',function(SupplierService,currentUser,$notification){
	
	var directive = {

		restrict: 'E',
		replace: true ,
		link: function ($scope, $element, $attrs, $controller){

			$scope.showSupplierModal = false;  
			$scope.supplierAdded = {};

			$scope.clearSupplierForm = function() {
        		$scope.supplier = {};
      		};

      		$scope.addSupplier = function(){
        
        		$scope.supplier = new SupplierService();
    			$scope.supplier.IsNew = true;
     			$scope.showSupplierModal = true;  

   			 };

		    $scope.editSupplier = function(user){
		       
		    	$scope.supplier = user;
		        $scope.showSupplierModal = true;
		        
		    };

		    $scope.cancelSupplier = function(){
		        
		        $scope.supplier = {};
		        $scope.showSupplierModal = false;

		    };

		    $scope.saveSupplier = function(){

		        $scope.supplier.$save({ SupplierId:$scope.supplier.SupplierId },function(){

			        	if($scope.supplier.SupplierId > 0 &&  $scope.supplier.IsNew){

			            	$notification.success($scope.supplier.SupplierName, 'El proveedor se guardo correctamente');	
			            	$scope.selectedSupplier = $scope.supplier;
			        	}
		            
			    		$scope.showSupplierModal = false;
			        	
			        },
			        function(){        	 
			             $notification.error('Eliminación incorrecta', 'El Proveedor no se pudo guardar');
			       	});		       
		    };
		}
		,
		templateUrl: 'client/partials/suppliers.html'
	}

	return directive;

}]);
