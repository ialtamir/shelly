'use strict';

/**
 * AngularJS directive that uses the plupload library to upload an image to
 * the web server.
 *
 * Tested with noted issues on:
 * IE8 - HTML4           IE9 - HTML4        Safari - HTML4        Firefox - HTML4
 * IE8 - Flash (1)       IE9 - Flash        Safari - Flash        Firefox - Flash
 * IE8 - Silverlight (1) IE9 - Silverlight  Safari - Silverlight  Firefox - Silverlight
 *                                          Safari - HTML5        Firefox - HTML5
 *
 * Chrome - HTML4        IOS6 - HTML4       ChromeiOS - HTML4
 * Chrome - Flashimg
 * Chrome - HTML5        IOS6 - HTML5 (2)   ChromeiOS - HTML5
 *
 * (1) When the Flash object is injected into the DOM, an error occurs.
 * (2) Spotty upload issues where the selected image is not always uploaded to the server,
 *     resulting in the wrong image downloaded. (Image name is always image.jpg)
 *
 * Long Story: The plupload library injects some DOM into an element on the page referenced
 * by the plupload option, 'container'. In HTML4 and HTML5 runtimes, this container should
 * be the DIV element surrounding the file upload component, however in Flash the container
 * element is the button itself. The problem is we can't tell whether plupload will use the
 * Flash implementation until after it is initialized, and therefore it is too late to modify
 * the container. I have worked around the problem by discovering that I can remove the
 * 'browse_button' setting during the initialization handler, which causes plupload to use
 * the entire container as a click area. Not ideal, but it seems to work.
 * (update) - Managed to get most of the browser/os combos working by adding a subcontainer
 * called 'browse-panel' to the mix. Now there is less trickeration.
 *
 * todo: Add an error handler
 *
 * @link https://github.com/pykl/
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

(function (window, ng, undefined) {
    var assetKey;
    var originalImgWidth, originalImgHeight;

    ng.module('com.pykl.fileupload', []);


    /**
     * This directive watches for changes to the image property, and springs into action when a
     * change is detected.
     */
    ng.module('com.pykl.fileupload').directive('imgCrop',
        ['$log', '$window', '$http', function ($log, $window, $http) {
            return {
                template: '\
                    <div class="modal cms-modal hide fade" tabindex="-1" role="dialog"> \
                        <div class="modal-header"> \
                            <button type="button" class="close" data-dismiss="modal">&times;</button>\
                            <h3>Crop Image</h3>\
                        </div>\
                        <div class="modal-body">\
                            <img class="crop-image"/>\
                        </div>\
                        <div class="modal-footer">\
                            <p>\
                                Drag in the image to define a crop area. When ready, press the crop button to proceed.\
                            </p>\
                            <button class="btn cancel" data-dismiss="modal">Cancel</button>\
                            <button class="btn submit btn-primary">Crop</button>\
                        </div>\
                    </div>',
                replace: true,
                restrict: 'A',
                require: '?ngModel',
                link: function (scope, elm, attrs, ngModel) {
                    var lastValue, loadedValue, submission = false;
                    var jcrop, sizes;
                    var maintainAspectRatio = false;

                    scope.$on('FILE_RESOURCE_CREATED', function(event, key){
                        assetKey = key;
                    });

                    /**
                     * Loads the image based on the provided url. This is made unnecessarily complicated because
                     * of IE8 issues, especially lack of support for natural width/height of image.
                     *
                     * @param url
                     */
                    function loadImage(url) {
                        // We want the image to fit the dimensions of the dialog, but height is ultimately the
                        // most important constraint.
                        var sizeImage = function (e) {
                            var event = e || window.event;                      // damn you ie8
                            var target = event.srcElement || this || event.currentTarget;  // damn you ie8 & ff
                            var width = target.naturalWidth || target.width;    // damn you ie8
                            var height = target.naturalHeight || target.height; // damn you ie8
                            sizes = { naturalWidth: width, naturalHeight: height };
                            var maxHeight = ng.element($window).height() * .7;
                            var maxWidth = container.width();
                            if (height > maxHeight) {
                                width = width * (maxHeight / height);
                                height = maxHeight;
                            }
                            if (width > maxWidth) {
                                height = height * (maxWidth / width);
                                width = maxWidth;
                            }
                            $log.info('imgCrop::loadImage, Sizing image to ', width, 'x', height);
                            img.width(width);
                            img.height(height);
                            sizes.scaledWidth = width;
                            sizes.scaledHeight = height;
                        };

                        var sizeImageNatural = function (e) {
                            var target = e.srcElement || e.currentTarget;  // FF needs 'currentTarget'
                            if (target.naturalWidth) sizeImage(e);
                            else {
                                // IE8 does not support naturalWidth/Height
                                var newImg = new Image();
                                newImg.onload = sizeImage;
                                newImg.src = target.src;
                            }
                        };
                        img.one('load', sizeImageNatural);
                        img.attr('src', url || '');
                    }

                    function showDialog() {
                        if (!jcrop) {
                            $log.info('imgCrop::initJcrop, initializing JCrop');
                            img.Jcrop(options, function () {
                                $log.info('imgCrop::initJcrop, setting jcrop instance');
                                jcrop = this;

                                if(maintainAspectRatio){
                                    jcrop.setOptions({
                                        aspectRatio: originalImgWidth/originalImgHeight
                                    });
                                }
                            });
                            $log.info('imgCrop::initJcrop, initializing completed');
                        }
                    }

                    function closeDialog() {
                        // No great way to tell if the dialog was closed by pressing esc,
                        // clicking off or pressing cancel. So setting submission to true
                        // when user clicks the crop button.
                        if (!submission) {
                            // When we cancel a cropping operation, the underlying image property has already been
                            // set to the new uncropped value. We need to restore the image that was in place
                            // _before_ the attempt to upload a new image.
                            $log.info('imgCrop::closeDialog, resetting loaded value', loadedValue);
                            if (loadedValue) {
                                scope.$apply(function () {
                                    ngModel.$setViewValue(loadedValue);
                                    lastValue = loadedValue;
                                });
                            }
                        }
                        $log.info('imgCrop::closing the dialog, destroying jcrop instance');
                        jcrop.destroy();
                        jcrop = null;
                        submission = false;
                    }

                    function cropImage(url, coords) {
                        $log.info('imgCrop::cropImage, url:', url, 'coords:',
                            JSON.stringify(coords), 'original:', JSON.stringify(sizes));
                        var widthFactor = sizes.naturalWidth / sizes.scaledWidth;
                        var heightFactor = sizes.naturalHeight / sizes.scaledHeight;
                        var data = {
                            x1: coords.x * widthFactor,
                            x2: coords.x2 * widthFactor,
                            y1: coords.y * heightFactor,
                            y2: coords.y2 * heightFactor,
                            w: sizes.finalWidth || coords.w * widthFactor,
                            h: sizes.finalHeight || coords.h * heightFactor,
                            ref: url
                        };

                        if(assetKey){
                            data = ng.extend({
                                'assetKey': assetKey
                            }, data);
                        }

                        var successCrop = function(data) {
                            $log.info('imgCrop::cropImage, success: ', JSON.stringify(arguments));
                            ngModel.$setViewValue(data.url);
                            lastValue = loadedValue = data.url

                            if(scope.onCrop){
                                scope.onCrop(data.url);
                            }
                        };

                        var errorCrop = function() {
                            $log.info('imgCrop::cropImage, error: ', JSON.stringify(arguments));
                        };

                        $log.info('imgCrop::cropImage, submitting data:', JSON.stringify(data));
                        if (data.w > 0 && data.h > 0)
                            $http.post(options.url, data).success(successCrop).error(errorCrop);
                        else
                            successCrop({ url: url });
                    }

                    function submitCrop() {
                        submission = true;
                        $log.info('imgCrop::submit, crop data:', JSON.stringify(
                            {
                                select: jcrop.tellSelect(),
                                scaled: jcrop.tellScaled()
                            }));
                        cropImage(lastValue, jcrop.tellSelect());

                        // Crop coordinates will be scaled because the image is scaled before jcrop
                        // is invoked. For this reason,
                        container.modal('hide');
                    }

                    // Initialize options. These options can be overwritten using by setting
                    // a JSON object to the img-crop attribute.
                    // ie. <div x-img-crop="{ aspectRatio: 16/9 }" />
                    var options = attrs.imgCrop ? scope.$eval(attrs.imgCrop) : {};
                    options = ng.extend({
                        bgColor: 'transparent'
                    }, options);

                    if (!options.url) throw 'img-crop directive requires a url property.';
                    if (options.finalWidth) sizes.finalWidth = options.finalWidth;
                    if (options.finalHeight) sizes.finalHeight = options.finalHeight;
                    if (options.aspectRatio == true) maintainAspectRatio = true;

                    // Generate a relatively unique id in cases when multiple uploads are instantiated.
                    var uid = Math.round(13 * Math.random() * Math.random() * 100000) + '';
                    // Assign a unique ID to the modal crop dialog bound to this element
                    var container = elm.attr('id', 'modal-' + uid);

                    // Locate the image element.
                    var img = container.find('img.crop-image');
                    if (!img) throw 'Image must be included in template with a "crop-image" class';

                    // Handlers for the dialog 
                    container.find('div.modal-footer .submit').click(submitCrop);
                    container.on('shown', showDialog);
                    container.on('hidden', closeDialog);

                    // When the ngModel attribute is updated, the $formatters are called
                    ngModel.$formatters.push(function (value) {
                        $log.info('imgCrop::$formatters, value:', arguments, 'lastValue:', lastValue);
                        if (lastValue && lastValue !== value) {
                            loadImage(value);
                            container.modal();
                        }
                        if (!lastValue) loadedValue = value;
                        lastValue = value;
                        return value;
                    });
                }
            }

        }]);

    /**
     * @ngdoc directive
     * @name com.pykl.fileupload:imgUpload
     *
     * @description
     * Simplifies the use of image upload by making it dead simple to integrate on the
     * page. There is some config that must be set up, but sensible defaults and the
     * uploading of a single file reduces the options significantly.
     *
     * There is a fair amount of boilerplate that is added to the user's original image tag.
     *
     * @element ANY
     * @param {expression} Contains a config object that controls the capabilities of the plupload plugin.
     *
     * @example
     * <example>
     *    <div x-img-upload="{ max_file_size: '3mb', resize: { width: 320, height: 240 } }"
     *         ng-model="{{thumb}}"
     *     ></div>
     * </example>
     *
     */
    ng.module('com.pykl.fileupload').directive('imgUpload',
        ['$log', function ($log) {
            return {
                template: '\
                    <div class="img-upload" x-ng-transclude> \
                        <div class="fade"> \
                            <div class="drop"><p>Drop files here</p></div> \
                        </div> \
                        <div class="browse"> \
                            <p id="browse-bar"><a href="#" class="btn">Browse</a></p> \
                        </div> \
                        <div class="error"> \
                            <p>Error message</p> \
                            <a class="close" href="#">&times;</a> \
                        </div> \
                        <div class="working"> \
                            <div>[x]</div> \
                            <div class="progress progress-striped active"> \
                                <div class="bar"></div> \
                            </div> \
                            <div class="name"></div> \
                        </div> \
                    </div>',
                replace: true,
                restrict: 'A',
                require: '?ngModel',
                transclude: true,
                link: function (scope, elm, attrs, ngModel) {
                    var container, uploader;

                    // Generate a relatively unique id in cases when multiple uploads are instantiated.
                    var uid = Math.round(13 * Math.random() * Math.random() * 100000) + '';
                    var idBrowseButton = 'browse-' + uid;
                    var idDropTarget = 'drop-' + uid;
                    var idBrowsePanel = 'browse-panel-' + uid;

                    function applyImageHandlers(img) {
                        var count = 0;
                        // We will maximise our width to the same size as the parent container.
                        var maxWidth = container.parent().width();

                        container.css({
                            width: '100%',
                            height: 200
                        });

                        var calcAndSetImageSize = function (img, imgWidth, imgHeight) {
                            if(count < 1){
                                originalImgWidth = imgWidth;
                                originalImgHeight = imgHeight;

                                $log.info('Original image width: ' + imgWidth + ', Original image height: ' + imgHeight);
                            }
                            
                            // The loaded image will not exceed the container, so it may need to be scaled.
                            var contWidth = maxWidth;
                            var calcWidth = Math.min(contWidth, imgWidth);
                            var calcHeight = imgHeight * (calcWidth / imgWidth);

                            // Resize image and container height and make sure the image is centered
                            container.css({
                                //height: Math.max(calcHeight, 300)
                            });
//                            $log.info('Uid:' + uid + ', setting container height:' + Math.max(calcHeight, 200));
                            ng.element(img).css({
                                position: 'absolute',
                                top: (container.height() - calcHeight) >> 1,
                                left: (contWidth - calcWidth) >> 1,
                                width: calcWidth,
                                height: calcHeight
                            });

                            $log.info('Uid:' + uid + ', Container width:' + contWidth +
                                ', image width:' + imgWidth + ', image height:' + imgHeight,
                                ', calc width:' + calcWidth + ', calc height:' + calcHeight);
                            uploader.refresh();
                        };

                        img.load(function (e) {
                            var img = e.srcElement || this;   // FF does not use srcElement
                            if (img.naturalWidth) {
                                calcAndSetImageSize(img, img.naturalWidth, img.naturalHeight);
                            } else {
                                // Because IE8 does not support the naturalWidth/Height property, we have
                                // to create a new image object without any CSS styling and _reload_ the
                                // image again just to get its dimensions!
                                var newImg = new Image();
                                newImg.onload = function () {
                                    calcAndSetImageSize(img, newImg.width, newImg.height);
                                };
                                newImg.src = img.src;
                            }
                        });
                    }

                    function initPlupload(up) {
                        $log.info('Plupload is using runtime:', up.runtime);
                        if (up.features.dragdrop) {
                            var addDrag = function () {
                                ng.element(this).removeClass('inerror');
                                ng.element(this).addClass('dragover');
                            };
                            var removeDrag = function () {
                                ng.element(this).removeClass('dragover');
                            };

                            container.find('div.browse p').prepend('Drag and Drop or ');

                            container.bind('dragover', addDrag);
                            container.bind('dragleave', removeDrag);
                            container.bind('drop', removeDrag);
                        } else {
                            container.find('div.drop').remove();
                        }
                    }

                    function addContainerEvents(ele) {
                        // Locate the primary container
                        container = ele.hover(
                            function () {
                                ng.element(this).addClass('hover')
                            },
                            function () {
                                ng.element(this).removeClass('hover')
                            }
                        );
                        container.find('div.error a.close').click(function () {
                            container.removeClass('inerror');
                        });
                    }

                    /**
                     * Error object has code and message properties, along with a plupload File property.
                     * @param error
                     */
                    function showError(error) {
                        container.addClass('inerror');
                        container.find('div.error p').html(error.message);
                    }

                    addContainerEvents(elm);

                    // Initialize options. These options can be overwritten using by setting
                    // a JSON object to the img-upload attribute.
                    // ie. <div x-img-upload="{ max_file_size: '2mb' }" />
                    var options = attrs.imgUpload ? scope.$eval(attrs.imgUpload) : {};
                    options = ng.extend({
                        runtimes: 'html5, silverlight, flash, html4',
                        browse_button: idBrowseButton,
                        drop_element: idDropTarget,
                        container: idBrowsePanel,
                        flash_swf_url: 'lib/plupload/js/plupload.flash.swf',
                        silverlight_xap_url: 'lib/plupload/js/plupload.silverlight.xap',
                        max_file_size: '1mb',
                        filters: [
                            {title: "Image files", extensions: "jpg,gif,png,jpeg"}
                        ],
                        crop: {

                        }
                    }, options);

                    if (!options.url) throw 'The img-upload directive requires a url property.';

                    // Locate or create the image element. If one does not exist, create one.
                    var img = container.find('img');
                    if (img.length === 0) img = ng.element('<img>').appendTo(container);

                    // We want to know when the image source loads
                    applyImageHandlers(img);

                    // The image's src is updated to the value stored in ngModel
                    ngModel.$formatters.push(function (value) {
                        $log.info('imgUpload::formatter, ngModel change:', arguments);
                        img.attr('src', value || '');
                    });

                    /*
                     ngModel.$render = function () {
                     $log.info('Received render call', arguments, ngModel);
                     img.attr('src', ngModel.$viewValue || '');
                     };
                     */

                    // The plupload libraries requires a few of our elements to have set
                    // id values (that we generated earlier)
                    container.find('div.browse > p').attr('id', idBrowsePanel);
                    container.find('div.browse a.btn').attr('id', idBrowseButton);
                    container.find('div.drop').attr('id', idDropTarget);

                    // Content areas
                    var elName = container.find('div.name');
                    var elProgressBar = container.find('div.bar');

                    // Initialize the plupload upload library (http://www.plupload.com/documentation.php#configuration)
                    uploader = new plupload.Uploader(options);
//                    $log.info('Uploader options', options);

                    // Update the overlay with the appropriate messaging if drag & drop is supported.
                    uploader.bind('Init', function (up) {
//                        $log.info('Init', arguments);
                        initPlupload(up);
                    });

                    // Start the upload process when a file is added to the queue.
                    uploader.bind('FilesAdded', function (up, files) {
//                        $log.info('FilesAdded', arguments);
                        container.addClass('inprogress');
                        elName.html(files[0].name);
                        elProgressBar.width(0);
                        setTimeout(function () {
                            up.start();
                        }, 0);
                    });

                    uploader.bind('FileUploaded', function (up, file, data) {
//                        $log.info('FileUploaded', arguments, ng.toJson(data, true));
                        // Update the model with the new URL
                        if (data.response) {
                            // When using HTML4, the response object is wrapped in <pre ...></pre> tags
                            var re = /<pre[^>]*>(.*)<\/pre>/i;
                            var respStr = re.exec(data.response);
                            var response = ng.fromJson(respStr ? respStr[1] : data.response);
                            if (response.url) {
                                scope.$apply(function () {
                                    ngModel.$setViewValue(response.url);
                                    // Why doesn't $formatter get called?
                                    img.attr('src', response.url);
                                });

                                if(scope.onFileUploaded){
                                    scope.onFileUploaded(response);
                                }

//                                $log.info('Updating model to new image src:', response.url);
                            }
                        }
                    });

                    uploader.bind('UploadComplete', function (uploader, file) {
                        container.removeClass('inprogress');
                        if (options.crop) {

                        }
                    });

                    uploader.bind('Error', function (uploader, error) {
                        container.removeClass('inprogress');
//                        $log.info('Error:', error);
                        showError(error);
                    });

                    uploader.bind('UploadProgress', function (up, file) {
//                        $log.info('UploadProgress', ng.toJson(arguments) + '');
                        elProgressBar.width(file.percent + '%');
                    });

                    uploader.init();
                }
            }
        }]);
})(window, window.angular);

