<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require '../../libraries/Slim/Slim.php';
require '../../config/configuration.php';
require '../../database/connection.php';
require '../../database/database.php';
require '../../database/utils.php';
require "../../libraries/mpdf/mpdf.php";
require '../../libraries/PHPMailer/class.phpmailer.php';
require '../../libraries/Intervention/Image/Image.php';
require '../../libraries/CSVImporter/CSVImporter.php';
require '../../libraries/PasswordLib/PasswordLib.php';
require '../../libraries/conekta/Conekta.php';

require '../../libraries/LessQL/Literal.php';
require '../../libraries/LessQL/Row.php';
require '../../libraries/LessQL/Result.php';
require '../../libraries/LessQL/Database.php';






\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, and `Slim::delete`
 * is an anonymous function.
 */

// GET route
$app->get('/',function () use ($app){
	

	try {

		
		$app->config('templates.path', 'client/'); 
		$app->render('index.html');
		
	}
	catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e)
	{
		//$app->render('../client/login.html');
	}
	



});

$app->get('/server/upload/images/:image_name',function ($image_name) use ($app){
	

	try{
	        
	        $account_configuration = AccountModel::getAccountConfiguration();                
	        $image_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path.$image_name;
			
			
			if(file_exists ( $image_path )){
				
				$image = file_get_contents( $image_path );	
			}
			else {
				$image = file_get_contents( 'client/img/image_dbeaf0_64.png' );	
				
			}
			
			
	        
	        $finfo = new finfo(FILEINFO_MIME_TYPE);        
	        $app->response()->header('Content-Type', 'content-type: ' . $finfo->buffer($image));
	        $app->response()->body($image);
        
	}
	catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e)
	{
		//$app->render('../client/login.html');
	}
	



    
});

$app->get('/server/uploads/images/:image_name',function ($image_name) use ($app){
	

	try{
	        
	        $account_configuration = AccountModel::getAccountConfiguration();                
	        $image_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path.$image_name;
	
	        
	        if(file_exists ( $image_path )){
				
				$image = file_get_contents( $image_path );	
			}
			else {
				$image = file_get_contents( 'client/img/image_dbeaf0_64.png' );	
				
			}
			
	        $finfo = new finfo(FILEINFO_MIME_TYPE);        
	        $app->response()->header('Content-Type', 'content-type: ' . $finfo->buffer($image));
	        $app->response()->body($image);
        
	}
	catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e)
	{
		//$app->render('../client/login.html');
	}
	



    
});


$app->get('/server/uploads/images/wix-filters/:image_name',function ($image_name) use ($app){
	

	try{
	        
	        $account_configuration = AccountModel::getAccountConfiguration();                
	        $image_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path."/wix-filters/".$image_name;
	
	        
	        if(file_exists ( $image_path )){
				
				$image = file_get_contents( $image_path );	
			}
			else {
				$image = file_get_contents( 'client/img/image_dbeaf0_64.png' );	
				
			}
			
	        $finfo = new finfo(FILEINFO_MIME_TYPE);        
	        $app->response()->header('Content-Type', 'content-type: ' . $finfo->buffer($image));
	        $app->response()->body($image);
        
	}
	catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e)
	{
		//$app->render('../client/login.html');
	}
	



    
});

$app->get('/images/:image_name',function ($image_name) use ($app){
	

	try{
	        
	        $account_configuration = AccountModel::getAccountConfiguration();  
	        
	        $image_path = ABSOLUTE_BASE_PATH.$account_configuration->base_path.CONST_ACCOUNT_PUBLIC_FOLDER.$image_name;
	
	        $image = file_get_contents( $image_path );
	        $finfo = new finfo(FILEINFO_MIME_TYPE);        
	        $app->response()->header('Content-Type', 'content-type: ' . $finfo->buffer($image));
	        $app->response()->body($image);
        
	}
	catch (League\OAuth2\Server\Exception\InvalidAccessTokenException $e)
	{
		//$app->render('../client/login.html');
	}
	
    
});








require 'server/controllers/account_controller.php';
require 'server/controllers/security_controller.php';
require 'server/controllers/user_controller.php';
require 'server/controllers/customer_controller.php';
require 'server/controllers/supplier_controller.php';
require 'server/controllers/category_controller.php';
require 'server/controllers/brand_controller.php';
require 'server/controllers/place_controller.php';
require 'server/controllers/sale_order_controller.php';
require 'server/controllers/quote_controller.php';
require 'server/controllers/um_controller.php';
require 'server/controllers/product_controller.php';
require 'server/controllers/work_order_controller.php';
require 'server/controllers/feature_controller.php';
require 'server/controllers/currency_controller.php';
require 'server/controllers/material_controller.php';
require 'server/controllers/employee_controller.php';
require 'server/controllers/payment_controller.php';
require 'server/controllers/calendar_controller.php';
require 'server/controllers/inventory_controller.php';
require 'server/controllers/dashboard_controller.php';
require 'server/controllers/lead_controller.php';
require 'server/controllers/purchase_order_controller.php';
require 'server/controllers/stock_controller.php';
require 'server/controllers/facturas_controller.php';
require 'server/controllers/pago_controller.php';

require 'server/controllers/transaction_controller.php';
require 'server/controllers/contract_template_controller.php';
require 'server/controllers/label_controller.php';
require 'server/controllers/slider_controller.php';










/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */

$app->run();
?>