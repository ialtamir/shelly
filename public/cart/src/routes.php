<?php

// Routes for v1 version

$app->group('/v1', function() use ($app) {

    // add an item to the cart
    // POST "/cart/{user_id}/add" and POST data as the item details
    $app->post('/cart/{user_id}/add', 'App\Controller\CartController:addToCart');

        // removing the item from the cart
    // DELETE "/cart/{user_id}/remove" item id in the body
    $app->post('/cart/{user_id}/remove', 'App\Controller\CartController:removeFromCart');

    // updating the quantity of an item in the cart
    // PUT "cart/{user_id}/update" item id and new quantity in the body
    $app->post('/cart/{user_id}/update', 'App\Controller\CartController:updateCart');

    // getting all the items in the cart
    // GET "cart/{user_id}/get/items"
    $app->get('/cart/{user_id}/get/items', 'App\Controller\CartController:getItems');
    
    $app->get('/cart/{user_id}/get/orders', 'App\Controller\CartController:getOrders');

    // getting user information - billing address from the cart
    // GET "cart/{user_id}/get/user"
    $app->get('/cart/{user_id}/get/user', 'App\Controller\CartController:getUser');
    
    $app->get('/cart/{user_id}/checkout/paypal/payment/create/', 'App\Controller\CartController:createPayment');
    $app->get('/cart/{user_id}/checkout/paypal/payment/execute/', 'App\Controller\CartController:executePayment');
    $app->post('/cart/{user_id}/checkout/paypal/payment/check/', 'App\Controller\CartController:checkPayment');
    
    $app->post('/user/{user_id}/welcome/', 'App\Controller\UserController:welcomeMessage');
    $app->post('/user/{user_id}/contact/', 'App\Controller\UserController:contactMessage');
    


})->add($auth_middleware);



$app->group('/v1/open', function() use ($app) {
    
    
    $app->post('/user/contact/', 'App\Controller\UserController:contactMessage');
    $app->get('/order/{order_id}/file/', 'App\Controller\CartController:getOrder');
    $app->post('/user/recovery/', 'App\Controller\UserController:recoveryMessage');
    $app->post('/user/change_password/', 'App\Controller\UserController:changePassword');
});
//$app->group('/v1/open', function() use ($app) {
    

    
//}
// fallback for home page
$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});


