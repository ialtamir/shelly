<?php
namespace App\Lib;

require_once('../../../config/configuration.php');


require_once('../../../database/utils.php');



class OrderModel extends Model
{
    
    public function create($user_id,$checkout,$payment){
        
        //$SaleOrderModel = new SaleOrderModel();
        
        
        $shipment = $this->calculateShipping($user_id, $checkout["items"]);
        //var_dump($shipment);

    	$SO = (object)array();
    	
    	$customer = $this->getCustomerInfo($user_id);
    	
    	$customer_id = $customer["customer_id"];
    	
    	
    	$subtotal  = $checkout["subtotal"];
    	$taxes     = $checkout["taxes"];
    	$discounts = $checkout["discounts"];
    	$total = $checkout["total"];
    	
    	$SO->SaleOrderId = null;
    	
		
		$SO->SaleOrderTitle = "Venta en linea";
		$SO->SaleOrderDescription ="Venta en linea";
		$SO->SaleOrderTerms= "Venta en linea";
		$SO->SaleOrderDate = date("Y-m-d H:i:s");
		$SO->CustomerId =$customer_id;
		$SO->CustomerName = $customer["customer_name"];
		$SO->customer =  $customer;
		$SO->SaleOrderStatus = "Aprobada";
		$SO->SaleOrderSubtotal = $subtotal;
		$SO->SaleOrderTaxes = $taxes;
		$SO->SaleOrderDiscounts = $discounts;
		$SO->SaleOrderTotal = $total;

		$SO->SaleOrderTotalDlls = $total_usd;
        $SO->UserId= $user_id;
        $SO->CurrencyId = 1;
        $SO->PlaceId = "2";
        $SO->SaleOrderExchangeRate = 1;//$order["QuoteExchangeRate"];
        $SO->TransactionId = NULL; 
        $SO->SaleOrderType = "Online";
        $SO->UserId = $user_id;
        $SO->SaleOrderShipping =  $shipment["TrackingNumber"];
        $SO->SaleOrderCarrier = "FEDEX";

       
        if(isset($checkout["items"]))
        {
        	$SO->items = array();
			$counter = 1;
			foreach ($checkout["items"] as $checkout_item){

				$item = (object)array();
				$item->SaleOrderDetailId = $counter ;
				
				if(isset($checkout_item["item"]["product_name"])) $item->SaleOrderDetailDescription=$checkout_item["item"]["product_name"];
				$item->SaleOrderDetailQTY= $checkout_item["quantity"];
				if(isset($checkout_item["item"]["product_um"])) $item->SaleOrderDetailUM = $checkout_item["item"]["product_um"];				
				//if(isset($checkout_item["QuoteDetailDiscount"])) $item->SaleOrderDetailDiscount = $checkout_item["QuoteDetailDiscount"];
				if(isset($checkout_item["taxes"])) $item->SaleOrderDetailTax = $checkout_item["taxes"];
				if(isset($checkout_item["item"]["product_price"])) $item->SaleOrderDetailPrice = $checkout_item["item"]["product_price"];

				if(isset($checkout_item["item"]["product_id"])) $item->ProductId = $checkout_item["item"]["product_id"];

				array_push($SO->items, $item);
				 $counter = $counter + 1;
			}		

			
        }
        
        
         $SO =  $this->save($SO,$payment);
         
        return $SO;
        
        
        
        



    }
    
    public function save($sale_order,$payment_array){

 		try{

 				$this->db->beginTransaction();

				$sql = " 
					INSERT INTO `erp_sale_orders`(
						`sale_order_id`, 
						`quote_id`,
						`sale_order_folio`, 
						`sale_order_title`, 
						`sale_order_description`,
						`sale_order_date`, 
						`customer_id`, 
						`sale_order_status`, 
        				`sale_order_subtotal`, 
        				`sale_order_taxes`, 
        				`sale_order_discounts`, 
        				`sale_order_total`, 
        				`sale_order_total_dlls`,
        					`sale_order_terms`, 
						`sale_order_cdate`,
						`user_id`,
						`sale_order_exchange_rate`,
						`place_id`,
						`currency_id`,
						`contract_id`,
						`sale_order_delivery`,
						`sale_order_delivery_date`,
						`sale_order_type`,
        				`transaction_id`,
        				`created_by`,
        				sale_order_payment_method, 
        				sale_order_payment_form , 
        				sale_order_proof_type,
        				sale_order_shipping,
        				sale_order_carrier
					) VALUES (
						:SaleOrderId,
						:QuoteId,
						:SaleOrderFolio,
						:SaleOrderTitle,
						:SaleOrderDescription,
						:SaleOrderDate,
						:CustomerId,
						:SaleOrderStatus,
						:SaleOrderSubtotal, 
        					:SaleOrderTaxes, 
        					:SaleOrderDiscounts, 
        					:SaleOrderTotal, 
        				:SaleOrderTotalDlls,
        				:SaleOrderTerms,
						NOW(),
						:UserId,
						:SaleOrderExchangeRate,
						:PlaceId,
						:CurrencyId,
						:ContractId,
						:SaleOrderDelivery,
						:SaleOrderDeliveryDate,
						:SaleOrderType,
        				:TransactionId,
        				:UserId,
        				:SaleOrderPaymentMethod, 
        				:SaleOrderPaymentForm , 
        				:SaleOrderProofType,
        				:SaleOrderShipping,
        				:SaleOrderCarrier
					)";

				$statement = $this->db->prepare($sql);
				//$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillParams($sale_order));
		
				$sale_order->SaleOrderId = (int) $this->db->lastInsertId();
				$sale_order->SaleOrderFolio = 'SO'.$sale_order->SaleOrderId;

				if( isset($sale_order->items) ){

  					$this->saveDetails($sale_order->SaleOrderId, $sale_order->items);

				}

				if(isset($sale_order->SaleOrderType)){

					if($sale_order->SaleOrderType == "Online"){
						
						$payment = (object)array();
						$payment->SaleOrderId = $sale_order->SaleOrderId;
						$payment->SaleOrderPaymentText = "Venta en linea: Paypal";
						$payment->SaleOrderPaymentDate = date("Y-m-d H:i:s");
						$payment->SaleOrderPaymentMethod = 0;
						$payment->SaleOrderPaymentAmount = $sale_order->SaleOrderTotal;
						$payment->SaleOrderPaymentReceiptNumber = $payment_array["id"];
						$payment->UserId = $sale_order->UserId;
						$payment = $this->createOrUpdateSaleOrderPayment($payment);
					}
				}
				
				
				$this->removeCart( $sale_order->UserId);

				$this->db->commit();

				return $sale_order;

		}catch(Exception $ex){
			
			$this->db->rollback();
			
			throw new Exception($ex->getMessage());
		}
	}
	
	public function saveDetails($sale_order_id,$sale_order_items){

		$sql_insert = "
			INSERT INTO `erp_sale_order_details`(
				`sale_order_id`, 
				`sale_order_detail_id`, 
				`sale_order_detail_description`, 
				`product_id`, 
				`sale_order_detail_qty`, 
				`sale_order_detail_um`, 
				`sale_order_detail_price`,
				`sale_order_detail_discount`,
				`sale_order_detail_tax`,
				`sale_order_detail_total`
			) 
			VALUES (
				:SaleOrderId,
				:SaleOrderDetailId,
				:SaleOrderDetailDescription,
				:ProductId,
				:SaleOrderDetailQTY,
				:SaleOrderDetailUM,
				:SaleOrderDetailPrice,
				:SaleOrderDetailDiscount,
				:SaleOrderDetailTax,
				:SaleOrderDetailTotal
			)";

		$sql_update = "
			UPDATE `erp_sale_order_details` SET
						
				`sale_order_detail_description`=:SaleOrderDetailDescription,
				`product_id` =:ProductId, 
				`sale_order_detail_qty`=:SaleOrderDetailQTY, 
				`sale_order_detail_um`=:SaleOrderDetailUM, 
				`sale_order_detail_price`=:SaleOrderDetailPrice,
				`sale_order_detail_discount`=:SaleOrderDetailDiscount,
				`sale_order_detail_tax` =:SaleOrderDetailTax,
				`sale_order_detail_total` = :SaleOrderDetailTotal
				WHERE `sale_order_id` = :SaleOrderId and `sale_order_detail_id` = :SaleOrderDetailId
			";


			foreach ($sale_order_items as $sale_order_item){

				$sql = $sql_insert;

				/*if(isset($sale_order_item->SaleOrderDetailId)){ 
					
					$sql = $sql_update; 
				}
				else{
					
					$sale_order_item->SaleOrderDetailId = $this->getSaleOrderDetailIdentity($sale_order_id);
				}*/

				$statement = $this->db->prepare($sql);
				//$statement->setFetchMode(PDO::FETCH_OBJ);
				$statement->execute($this->fillDetailParams($sale_order_id,$sale_order_item));

				
				
			}		
	}
	
	private function fillParams($sale_order){

		$params = [];


		if(isset($sale_order->SaleOrderId)){  $params[':SaleOrderId'] = $sale_order->SaleOrderId; } else {  $params[':SaleOrderId'] = NULL; };
		if(isset($sale_order->QuoteId)){  $params[':QuoteId'] = $sale_order->QuoteId; } else {  $params[':QuoteId'] = NULL; };
		if(isset($sale_order->SaleOrderFolio)){   $params[':SaleOrderFolio'] = $sale_order->SaleOrderFolio; } else {    $params[':SaleOrderFolio'] = NULL; };
		if(isset($sale_order->SaleOrderTitle)){   $params[':SaleOrderTitle'] = $sale_order->SaleOrderTitle; } else {   $params[':SaleOrderTitle'] = NULL; };
		if(isset($sale_order->SaleOrderDescription)){   $params[':SaleOrderDescription'] = $sale_order->SaleOrderDescription; } else {   $params[':SaleOrderDescription'] = NULL; };
		if(isset($sale_order->SaleOrderDate)){   $params[':SaleOrderDate'] = $sale_order->SaleOrderDate; } else {   $params[':SaleOrderDate'] = NULL; };
		if(isset($sale_order->CustomerId)){   $params[':CustomerId'] = $sale_order->CustomerId; } else {   $params[':CustomerId'] = NULL; };
		if(isset($sale_order->SaleOrderStatus)){   $params[':SaleOrderStatus'] = $sale_order->SaleOrderStatus; } else {   $params[':SaleOrderStatus'] = NULL;};
		if(isset($sale_order->SaleOrderSubtotal)){   $params[':SaleOrderSubtotal'] = $sale_order->SaleOrderSubtotal; } else {   $params[':SaleOrderSubtotal'] = NULL;};
		if(isset($sale_order->SaleOrderDiscounts)){   $params[':SaleOrderDiscounts'] = $sale_order->SaleOrderDiscounts; } else {   $params[':SaleOrderDiscounts'] = NULL;};
		if(isset($sale_order->SaleOrderTaxes)){   $params[':SaleOrderTaxes'] = $sale_order->SaleOrderTaxes; } else {   $params[':SaleOrderTaxes'] = NULL;};
		if(isset($sale_order->SaleOrderTotal)){   $params[':SaleOrderTotal'] = $sale_order->SaleOrderTotal; } else {   $params[':SaleOrderTotal'] = NULL;};
		if(isset($sale_order->SaleOrderTotalDlls)){   $params[':SaleOrderTotalDlls'] = $sale_order->SaleOrderTotalDlls; } else {   $params[':SaleOrderTotalDlls'] = NULL;};
		if(isset($sale_order->SaleOrderExchangeRate)){   $params[':SaleOrderExchangeRate'] = $sale_order->SaleOrderExchangeRate; } else {   $params[':SaleOrderExchangeRate'] = NULL;};
		if(isset($sale_order->SaleOrderTerms)){   $params[':SaleOrderTerms'] = $sale_order->SaleOrderTerms; } else {   $params[':SaleOrderTerms'] = NULL;};
	        
	        if(isset($sale_order->CurrencyId)){   $params[':CurrencyId'] = $sale_order->CurrencyId; } else {   $params[':CurrencyId'] = NULL;}; 
	        if(isset($sale_order->ContractId)){   $params[':ContractId'] = $sale_order->ContractId; } else {   $params[':ContractId'] = NULL;}; 
	        if(isset($sale_order->SaleOrderDelivery)){   $params[':SaleOrderDelivery'] = $sale_order->SaleOrderDelivery; } else {   $params[':SaleOrderDelivery'] = NULL;}; 
	        if(isset($sale_order->SaleOrderDeliveryDate)){   $params[':SaleOrderDeliveryDate'] = $sale_order->SaleOrderDeliveryDate; } else {   $params[':SaleOrderDeliveryDate'] = NULL;}; 
	        if(isset($sale_order->SaleOrderType)){   $params[':SaleOrderType'] = $sale_order->SaleOrderType; } else {   $params[':SaleOrderType'] = "Normal";}; 
	        if(isset($sale_order->TransactionId)){   $params[':TransactionId'] = $sale_order->TransactionId; } else {   $params[':TransactionId'] = uniqid();}; 
	        if(isset($sale_order->SaleOrderPaymentForm)){   $params[':SaleOrderPaymentForm'] = $sale_order->SaleOrderPaymentForm; } else {   $params[':SaleOrderPaymentForm'] = "NULL" ;}; 
	        if(isset($sale_order->SaleOrderPaymentMethod)){   $params[':SaleOrderPaymentMethod'] = $sale_order->SaleOrderPaymentMethod; } else {   $params[':SaleOrderPaymentMethod'] = "NULL";}; 
	        if(isset($sale_order->SaleOrderProofType)){   $params[':SaleOrderProofType'] = $sale_order->SaleOrderProofType; } else {   $params[':SaleOrderProofType'] = "NULL";}; 
	        
	        if(isset($sale_order->SaleOrderShipping)){   $params[':SaleOrderShipping'] = $sale_order->SaleOrderShipping; } else {   $params[':SaleOrderShipping'] = "NULL";}; 
	        if(isset($sale_order->SaleOrderCarrier)){   $params[':SaleOrderCarrier'] = $sale_order->SaleOrderCarrier; } else {   $params[':SaleOrderCarrier'] = "NULL";}; 
	        
	 
        				
        				
	        if(isset($sale_order->PlaceId)){   $params[':PlaceId'] = $sale_order->PlaceId; } else {   
			
			/*$UserModel = new UserModel();
			$user = $UserModel->getUser(getOwnerId());
			
			if( $user->place_id > 0){ $params[":PlaceId"] = $user->place_id; } else {$params[':PlaceId'] = NULL;}
			*/
			};
	        	

        	$params[':UserId']  = $sale_order->UserId;
        	
		return $params;
	}
	
	private function fillDetailParams($sale_order_id , $sale_order_detail){

		$params=[];
		$params[':SaleOrderId'] = $sale_order_id ;
		if(isset($sale_order_detail->SaleOrderDetailId)){   $params[':SaleOrderDetailId'] = $sale_order_detail->SaleOrderDetailId ; } else { $params[':SaleOrderDetailId'] = NULL ; }
		
		if(isset($sale_order_detail->SaleOrderDetailDescription)){   $params[':SaleOrderDetailDescription'] = $sale_order_detail->SaleOrderDetailDescription ; } else { $params[':SaleOrderDetailDescription'] = NULL ; }


		if(isset($sale_order_detail->SaleOrderDetailQTY)){   $params[':SaleOrderDetailQTY'] = $sale_order_detail->SaleOrderDetailQTY ; } else { $params[':SaleOrderDetailQTY'] = NULL ; }
		if(isset($sale_order_detail->SaleOrderDetailUM)){   $params[':SaleOrderDetailUM'] = $sale_order_detail->SaleOrderDetailUM ; } else { $params[':SaleOrderDetailUM'] = NULL ; }
		if(isset($sale_order_detail->SaleOrderDetailDiscount)){   $params[':SaleOrderDetailDiscount'] = $sale_order_detail->SaleOrderDetailDiscount ; } else { $params[':SaleOrderDetailDiscount'] = NULL ; }
		if(isset($sale_order_detail->SaleOrderDetailTax)){   $params[':SaleOrderDetailTax'] = $sale_order_detail->SaleOrderDetailTax ; } else { $params[':SaleOrderDetailTax'] = NULL ; }
		if(isset($sale_order_detail->SaleOrderDetailPrice)){   $params[':SaleOrderDetailPrice'] = $sale_order_detail->SaleOrderDetailPrice ; } else { $params[':SaleOrderDetailPrice'] = NULL ; }
		if(isset($sale_order_detail->SaleOrderDetailTotal)){   $params[':SaleOrderDetailTotal'] = $sale_order_detail->SaleOrderDetailTotal ; } else { $params[':SaleOrderDetailTotal'] = $sale_order_detail->SaleOrderDetailPrice*$sale_order_detail->SaleOrderDetailQTY ; }

		//if(isset($sale_order_detail->ProductId)){   

			$params[':ProductId'] = $sale_order_detail->ProductId ; 



		return $params;
	}
	
	public function createOrUpdateSaleOrderPayment($payment){

		$sql_insert = "
			INSERT INTO `erp_sale_order_payments`(
				`sale_order_id`, 
				`sale_order_payment_id`,
				`sale_order_payment_receipt_number`,
				`sale_order_payment_text`, 
				`sale_order_payment_date`, 
				`sale_order_payment_amount`,		
				`sale_order_payment_method`, 
				`sale_order_payment_cdate`, 
				`sale_order_payment_lmdate`, 
				`user_id`
			) VALUES (
				:SaleOrderId,
				:SaleOrderPaymentId,
				:SaleOrderPaymentReceiptNumber,
				:SaleOrderPaymentText,
				:SaleOrderPaymentDate,
				:SaleOrderPaymentAmount,
				:SaleOrderPaymentMethod,
				NOW(),
				NOW(),
				:UserId
			)
		";


	
			$sql = $sql_insert;
			$payment->SaleOrderPaymentId = 1;
			//$FolioModel = new FolioModel();

			//$FolioNumber = $FolioModel->getLastFolioByCode('RECEIPT');
			//$FolioNumber = str_pad($FolioNumber, 7, "0", STR_PAD_LEFT);
			//$payment->SaleOrderPaymentReceiptNumber = "A-".$FolioNumber;

		
        //var_dump($payment);
		$statement = $this->db->prepare($sql);
		$statement->setFetchMode(\PDO::FETCH_OBJ);
		$statement->execute($this->fillPaymentParams($payment));

		return $payment;
	}
	
	private function fillPaymentParams($payment){

		$params= [];
		$params[':SaleOrderId'] = $payment->SaleOrderId;
		$params[':SaleOrderPaymentId'] = $payment->SaleOrderPaymentId;

		if(isset($payment->SaleOrderPaymentText)){   $params[':SaleOrderPaymentText'] = $payment->SaleOrderPaymentText ; } else { $params[':SaleOrderPaymentText'] = NULL ; }
		if(isset($payment->SaleOrderPaymentDate)){   $params[':SaleOrderPaymentDate'] = $payment->SaleOrderPaymentDate ; } else { $params[':SaleOrderPaymentDate'] = NULL ; }
		if(isset($payment->SaleOrderPaymentAmount)){   $params[':SaleOrderPaymentAmount'] = $payment->SaleOrderPaymentAmount ; } else { $params[':SaleOrderPaymentAmount'] = NULL ; }
		if(isset($payment->SaleOrderPaymentMethod)){   $params[':SaleOrderPaymentMethod'] = $payment->SaleOrderPaymentMethod ; } else { $params[':SaleOrderPaymentMethod'] = NULL ; }
		if(isset($payment->SaleOrderPaymentReceiptNumber)){   $params[':SaleOrderPaymentReceiptNumber'] = $payment->SaleOrderPaymentReceiptNumber ; } else { $params[':SaleOrderPaymentReceiptNumber'] = NULL ; }

		$params[':UserId']  = $payment->user_id;

		return $params;

	}
	
	public function removeCart($user_id){

		$sql = "
			DELETE FROM `cart` 
			WHERE 
				`user_id`= :user_id 
		
		";


		$statement = $this->db->prepare($sql);
		$statement->setFetchMode(\PDO::FETCH_OBJ);
		$statement->execute(array(':user_id' => $user_id));

	}
	
	public function getCustomerInfo($user_id){
		
			$sql = " SELECT * FROM erp_customers WHERE user_id = :user_id
					";
		//	$user_id = (int)$this->user_id;
			$statement = $this->db->prepare($sql);
			$result = $statement->execute(['user_id' => $user_id]);
			$user_details = $result ? $statement->fetch() : [];

			//$this->user_details = $user_details;
			//var_dump($user_details);
			
			return $user_details;

	}
	
	
	
	public function getOrder($order_id)
	    {
	        
	        
	        
	     
	      
	        $statement = $this->db->prepare("select * from erp_sale_orders  where sale_order_id = :order_id  ");
	    
	        $statement->bindValue(':order_id', $order_id, \PDO::PARAM_INT);
	
	        $result = $statement->execute();
	
	        $item = (object)[];
	        
	        while ($row = $statement->fetch()) {
	        	
	        	
	        	$row["items"] = $this->getOrderDetails($row["sale_order_id"]);
	           
	           $item = $row;
	            //array_push($items,$row);
	        }
	
	        return $item;
	    }
    
    
    
	public function getOrders($user_id)
    {
        
        
        
        $customer = $this->getCustomerInfo($user_id);
    	
    	$customer_id = $customer["customer_id"];

      
        $statement = $this->db->prepare("select * from erp_sale_orders  where customer_id = :customer_id  ");
    
        $statement->bindValue(':customer_id', $customer_id, \PDO::PARAM_INT);

        $result = $statement->execute();

        $items = [];
        
        while ($row = $statement->fetch()) {
        	
        	
        	$row["items"] = $this->getOrderDetails($row["sale_order_id"]);
           
            array_push($items,$row);
        }

        return $items;
    }
    
    
    public function getOrderDetails($order_id)
    {
        
        
 
        $statement = $this->db->prepare("select od.*,p.product_external_code from erp_sale_order_details od LEFT OUTER JOIN erp_products p ON   p.product_id = od.product_id  WHERE od.sale_order_id = :sale_order_id ");
    
        $statement->bindValue(':sale_order_id', $order_id, \PDO::PARAM_INT);

        $result = $statement->execute();

        $items = [];
        
        while ($row = $statement->fetch()) {
           
            array_push($items,$row);
        }

        return $items;
    }
    
    
 
	public function sendEmail($order){


		
		$body = file_get_contents( "../templates/pay-success-email.html" );
		
		$order = (array)$order;


        //$mail->AddReplyTo($this->app_configuration->CompanyAddReplyTo,$this->app_configuration->CompanyAddReplyToName);
		

		$address =  $order["customer"]["customer_email"];
		$name = $order["customer"]["customer_name"];

        //Enviar correo para confirmacion de creacion de la cuenta
        date_default_timezone_set('America/Tijuana');

    	
    	if( $order["CurrencyId"] == 1 ){
    		$currency_code = 'MXN';
    	}
    	
    	if( $order["CurrencyId"] == 2 ){
    		$currency_code = 'USD';
    	}
    	
    
    	
    	
    	
    	$data_table_data = '
				<table border="0">
					<tr style="background-color:#B0B0B0; color:#ffffff;">
						<td width="40%" >Descripcion</td>
						<td width="8%">UM</td>
						<td width="6%">Cant.</td>
						<td width="10%">Precio</td>
						<td width="12%" style="text-align:center">Desc. %</td>
						<td width="12%" style="text-align:center">IVA %</td>
						<td width="15%" >Total</td>
					</tr>';


		

			foreach ($order["items"] as $item) {
				
				
				$item = (array)$item;
				$Description = "";



				$DiscountAmountText = "";
                if( $item["SaleOrderDetailDiscount"] > 0 ){

					$DiscountAmount =  ( $item["SaleOrderDetailDiscount"] / 100) * $item["SaleOrderDetailTotal"];
					$DiscountAmount =  number_format((float) $DiscountAmount, 2, '.', '');
					$DiscountAmountText = ''.$item["SaleOrderDetailDiscount"].'% <span style="color: #444444" >($'.$DiscountAmount.')</span>';
				}

				$TaxAmount="";
				if($item["SaleOrderDetailTax"] > 0){

					$TaxAmount = ( $item["SaleOrderDetailTax"] / 100) * $item["SaleOrderDetailTotal"];
					$TaxAmount =  number_format((float) $TaxAmount, 2, '.', '');
					$TaxAmount = ''.$item["SaleOrderDetailTax"].'% <span style="color: #444444" >($'.$TaxAmount.')</span>';


				}


			
			    $item["SaleOrderDetailTotal"] = ($item["SaleOrderDetailQTY"] * $item["SaleOrderDetailPrice"]) + $TaxAmount - $DiscountAmount;
				$Description = $item["SaleOrderDetailDescription"];
			

				$data_table_data .='
					<tr >
						<td style="font-size:12px" >'.$Description.'</td>
						<td style="text-align:center">'.$item["SaleOrderDetailUM"].'</td>
						<td style="text-align:center">'.$item["SaleOrderDetailQTY"].'</td>
						<td style="text-align:right">'.$item["SaleOrderDetailPrice"].'</td>
						<td style="text-align:center; font-size:12px">'.$DiscountAmountText .'</td>
						<td style="text-align:center; font-size:12px">'.$TaxAmount.'</td>
						<td style="text-align:right">'.$item["SaleOrderDetailTotal"].'</td>
					</tr>
				';
				
				
			}
			
			
		   $data_table_data .= '</table>';
		   
		   
		   		
    	$body = str_replace("[PAYNUMBER]",$order["payment"]["id"],$body);
    	$body = str_replace("[OENUMBER]",$order["SaleOrderFolio"],$body);
    	$body = str_replace("[USER NAME]",$order["customer"]["customer_name"],$body);
    	$body = str_replace("[LSTPRODUCTS]",$data_table_data,$body);
    	$body = str_replace("[SUMTOTAL]",$order["SaleOrderTotal"].' '.$currency_code,$body);
    	
    	
    	

    	//Solo va a entrar aqui si estamos en produccion en el aws
    	$sendgrid_username = $_ENV['SENDGRID_USERNAME'];
        $sendgrid_password = $_ENV['SENDGRID_PASSWORD'];
        $to                = $address; 
        
        $sendgrid = new \SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));
        $email    = new \SendGrid\Email();
        $email->addTo($to)->
               setFrom('sistema@cuatroo.com')->
               setFromName('Shelly')->
               setSubject("Venta en linea: ".$order["SaleOrderFolio"]." , ".$order["SaleOrderTitle"]."")->
               setHtml($body)->
               addHeader('X-Sent-Using', 'SendGrid-API')->
               addHeader('X-Transport', 'web');
        $response = $sendgrid->send($email);
        
        return $account->email;

	}
	
	public function downloadFile($order_id){
		
		
		$order = $this->getOrder($order_id);
		
		$xml = '<?xml version="1.0" standalone="yes"?>
					<DATAPACKET Version="2.0">
					    <METADATA>
					        <FIELDS>
					            <FIELD attrname="CVE_CLPV" fieldtype="string" WIDTH="10" />
					            <FIELD attrname="NUM_ALMA" fieldtype="i4" />
					            <FIELD attrname="CVE_PEDI" fieldtype="string" WIDTH="20" />
					            <FIELD attrname="ESQUEMA" fieldtype="i4" />
					            <FIELD attrname="DES_TOT" fieldtype="r8" />
					            <FIELD attrname="DES_FIN" fieldtype="r8" />
					            <FIELD attrname="CVE_VEND" fieldtype="string" WIDTH="5" />
					            <FIELD attrname="COM_TOT" fieldtype="r8" />
					            <FIELD attrname="NUM_MONED" fieldtype="i4" />
					            <FIELD attrname="TIPCAMB" fieldtype="r8" />
					            <FIELD attrname="STR_OBS" fieldtype="string" WIDTH="255" />
					            <FIELD attrname="ENTREGA" fieldtype="string" WIDTH="25" />
					            <FIELD attrname="SU_REFER" fieldtype="string" WIDTH="20" />
					            <FIELD attrname="TOT_IND" fieldtype="r8" />
					            <FIELD attrname="MODULO" fieldtype="string" WIDTH="4" />
					            <FIELD attrname="CONDICION" fieldtype="string" WIDTH="25" />
					            <FIELD attrname="dtfield" fieldtype="nested">
					                <FIELDS>
					                    <FIELD attrname="CANT" fieldtype="r8" />
					                    <FIELD attrname="CVE_ART" fieldtype="string" WIDTH="20" />
					                    <FIELD attrname="DESC1" fieldtype="r8" />
					                    <FIELD attrname="DESC2" fieldtype="r8" />
					                    <FIELD attrname="DESC3" fieldtype="r8" />
					                    <FIELD attrname="IMPU1" fieldtype="r8" />
					                    <FIELD attrname="IMPU2" fieldtype="r8" />
					                    <FIELD attrname="IMPU3" fieldtype="r8" />
					                    <FIELD attrname="IMPU4" fieldtype="r8" />
					                    <FIELD attrname="COMI" fieldtype="r8" />
					                    <FIELD attrname="PREC" fieldtype="r8" />
					                    <FIELD attrname="NUM_ALM" fieldtype="i4" />
					                    <FIELD attrname="STR_OBS" fieldtype="string" WIDTH="255" />
					                    <FIELD attrname="REG_GPOPROD" fieldtype="i4" />
					                    <FIELD attrname="REG_KITPROD" fieldtype="i4" />
					                    <FIELD attrname="NUM_REG" fieldtype="i4" />
					                    <FIELD attrname="COSTO" fieldtype="r8" />
					                    <FIELD attrname="TIPO_PROD" fieldtype="string" WIDTH="1" />
					                    <FIELD attrname="TIPO_ELEM" fieldtype="string" WIDTH="1" />
					                    <FIELD attrname="MINDIRECTO" fieldtype="r8" />
					                    <FIELD attrname="TIP_CAM" fieldtype="r8" />
					                    <FIELD attrname="FACT_CONV" fieldtype="r8" />
					                    <FIELD attrname="UNI_VENTA" fieldtype="string" WIDTH="10" />
					                    <FIELD attrname="IMP1APLA" fieldtype="i4" />
					                    <FIELD attrname="IMP2APLA" fieldtype="i4" />
					                    <FIELD attrname="IMP3APLA" fieldtype="i4" />
					                    <FIELD attrname="IMP4APLA" fieldtype="i4" />
					                    <FIELD attrname="PREC_SINREDO" fieldtype="r8" />
					                    <FIELD attrname="COST_SINREDO" fieldtype="r8" />
					                    <FIELD attrname="LOTE" fieldtype="string" WIDTH="16" />
					                    <FIELD attrname="PEDIMENTO" fieldtype="string" WIDTH="16" />
					                    <FIELD attrname="FECHCADUC" fieldtype="dateTime" />
					                    <FIELD attrname="FECHADUANA" fieldtype="dateTime" />
					                    <FIELD attrname="LINK_FIELD" fieldtype="ui4" hidden="true" linkfield="true" />
					                </FIELDS>
					            </FIELD>
					        </FIELDS>
					    </METADATA>
					    <ROWDATA>
					        <ROW RowState="4" CVE_CLPV="[[TipoOrden]]" NUM_ALMA="[[NoAlmacen]]" CVE_PEDI="[[NoOrden]]" ESQUEMA="1" DES_TOT="3" DES_FIN="0" CVE_VEND="    4" COM_TOT="4" NUM_MONED="1" TIPCAMB="1" STR_OBS="NOTA DE VENTA PARCIAL" MODULO="FACT" CONDICION="">[[items]]
					        </ROW>
					    </ROWDATA>
					</DATAPACKET>';
        
     
       
        
        
        $xml_items = '<dtfield>';
        
        	foreach ($order["items"] as $item) {
				
				$xml_item = '<ROWdtfield RowState="4" CANT="[[CantidadProducto]]" CVE_ART="[[IdProductoSae]]" DESC1="0" DESC2="0" DESC3="0" IMPU1="0" IMPU2="0" IMPU3="0" IMPU4="16" COMI="4" PREC="[[PrecioSinIva]]" NUM_ALM="[[NoAlmacen]]" STR_OBS="[[ComentariosDeLaOrden]]" REG_GPOPROD="0" COSTO="0" TIPO_PROD="P" TIPO_ELEM="N" TIP_CAM="1" UNI_VENTA="PZ" IMP1APLA="4" IMP2APLA="4" IMP3APLA="4" IMP4APLA="0" PREC_SINREDO="[[PrecioVentaSinIva]]" COST_SINREDO="0" LINK_FIELD="1" />';
					            
				$xml_item = str_replace("[[CantidadProducto]]",$item["sale_order_detail_qty"],$xml_item);
        		$xml_item = str_replace("[[IdProductoSae]]",$item["product_external_code"],$xml_item);
        
        		$xml_item = str_replace("[[PrecioSinIva]]",$item["sale_order_detail_price"],$xml_item);
        		$xml_item = str_replace("[[PrecioVentaSinIva]]",$item["sale_order_detail_price"],$xml_item);
        		$xml_item = str_replace("[[ComentariosDeLaOrden]]","",$xml_item);
        		$xml_items .= $xml_item;
         
				
				
			}
			
		$xml_items .= '</dtfield>';
		
		
		$xml =  str_replace("[[items]]",$xml_items,$xml);
		
		 $xml = str_replace("[[TipoOrden]]","1402",$xml);
        $xml = str_replace("[[NoAlmacen]]","9",$xml);
        $xml = str_replace("[[NoOrden]]",$order["SaleOrderFolio"],$xml);
        $xml = str_replace("[[ComentariosDeLaOrden]]","1402",$xml);
        
      
			
        echo $xml;
        
        //1402 ; //Sin Factura
      //  1403 Con Factura
        
       
    	
    	
    	
       /* CVE_CLPV="{{TipoOrden}}"   1402 Sin Factura  o 1403 Con Factura
		NUM_ALMA="{{NoAlmacen}}"  Siempre debe ser 9 
		CVE_PEDI="{{NoOrden}}" 
		CANT="{{CantidadProducto}}" 
		CVE_ART="{{IdProductoSae}}" 
		PREC="{{PrecioSinIva}}" 
		NUM_ALM="{{NoAlmacen}}"  Siempre debe ser 9
		STR_OBS="{{ComentariosDeLaOrden}}" 
		PREC_SINREDO="{{PrecioVentaSinIva}}" */
	}
	
	
	public function calculateShipping($user_id, $items){
        
        
        
        
        $user = new UserModel($this->db);
        $user->init($user_id);

        $user_details = $user->getDetails();
        $user_customer_details = $user->getCustomerDetails();
      
      
        
        $newline = "<br />";
        //The WSDL is not included with the sample code.
        //Please include and reference in $path_to_wsdl variable.
        //$path_to_wsdl = "../libraries/RateService_v22.wsdl";
        $path_to_wsdl = "../libraries/ShipService/ShipService_v21.wsdl";
        
        ini_set("soap.wsdl_cache_enabled", "0");
         
        $client = new \SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
        
        $request['WebAuthenticationDetail'] = array(
        	'ParentCredential' => array( 'Key' => getProperty('parentkey'),'Password' => getProperty('parentpassword')
        	),
        	'UserCredential' => array( 'Key' => getProperty('key'), 'Password' => getProperty('password')
        	)
        ); 
        
        $request['ClientDetail'] = array( 'AccountNumber' => getProperty('shipaccount'), 'MeterNumber' => getProperty('meter'));
        
        
        //$request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request using PHP ***');
        //$request['Version'] = array( 'ServiceId' => 'crs', 'Major' => '22', 'Intermediate' => '0',  'Minor' => '0' );
        
        $request['TransactionDetail'] = array('CustomerTransactionId' => '*** Express Domestic Shipping Request using PHP ***');
		$request['Version'] = array('ServiceId' => 'ship', 'Major' => '21', 'Intermediate' => '0', 	'Minor' => '0');
        
        $request['ReturnTransitAndCommit'] = true;
        
        $request['RequestedShipment']['ShipTimestamp'] = date('c');
        $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
        
        //$request['RequestedShipment']['ServiceType'] = 'INTERNATIONAL_PRIORITY'; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
        $request['RequestedShipment']['ServiceType'] = 'FEDEX_EXPRESS_SAVER'; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
        $request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING'; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
       
       
       
        
        $shipper = array(
			'Contact' => array(
				'PersonName' => 'Shelly',
				'CompanyName' => 'Comercializadora Shelly',
				'PhoneNumber' => '6643266410'
			),
			'Address' => array(
				'StreetLines' => array('Blvd. Manuel J. Clouthier #114-A7','Col. Campestre Murua Martinez'),
				'City' => 'Tijuana',
				'StateOrProvinceCode' => 'BC',
				'PostalCode' => '22455',
				'CountryCode' => 'MX'
			)
		);
	
	//var_dump($user_customer_details);
	
		$recipient = array(
		    'Contact' => array(
			    'PersonName' => $user_customer_details['customer_name'],
			    'CompanyName' => $user_customer_details['customer_name'],
			    'PhoneNumber' => $user_customer_details['customer_telephone']
		    ),
		    'Address' => array(
			    'StreetLines' => array($user_customer_details['customer_address'],$user_customer_details['customer_delivery_place']),
			    'City' => $user_customer_details['customer_city'],
			    'StateOrProvinceCode' => $user_customer_details['customer_state'],
			    'PostalCode' => $user_customer_details['customer_cp'],
			    'CountryCode' => 'MX',
			    'Residential' => false
		    )
	    );
	
        $shippingChargesPayment = array(
    		'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
    		'Payor' => array(
    			'ResponsibleParty' => array(
    				'AccountNumber' => getProperty('billaccount'),
    				'CountryCode' => 'MX'
    			)
    		)
    	);
	
        $request['RequestedShipment']['Shipper'] = $shipper;
        $request['RequestedShipment']['Recipient'] = $recipient;
        $request['RequestedShipment']['ShippingChargesPayment'] = $shippingChargesPayment;
        
        $PackageCount = count($items);
        
        
        
        $PackageLineItems = array();
        $TotalProductValue = 0;
        
    $GroupPackageCount = 0;
       for ($i = 0; $i < $PackageCount; $i++) {


            $item = $items[$i];
            
            
	        $packageLineItem = array();
	        $packageLineItem['SequenceNumber'] = $i+1;
	        $packageLineItem['GroupPackageCount'] = $item['quantity'];
	        
	        
	        $packageLineItem['Weight'] = array('Value' => floatval($item['item']['product_weight']),'Units' => $item['item']['product_weight_unit']);
	        
	        
	        if(isset($item['item']['product_length']) && isset($item['item']['product_width']) && isset($item['item']['product_height']) && isset($item['item']['product_dimensions_unit']))
	        {
	            $packageLineItem['Dimensions'] = array('Length' => floatval($item['item']['product_length']),'Width' => floatval($item['item']['product_width']),'Height' => floatval($item['item']['product_height']),'Units' => $item['item']['product_dimensions_unit']);
	            //$packageLineItem['Dimensions'] = array('Length' => $item['item']['product_length'],'Width' => $item['item']['product_width'],'Height' => $item['item']['product_height'],'Units' => 'cm');
	        }
	        
	        $GroupPackageCount = $GroupPackageCount + intval($item['quantity']);
	        $TotalProductValue = $TotalProductValue + ( intval($item['quantity']) * floatval($item['item']['product_price']) );
	        $PackageLineItems[] = $packageLineItem;
	        
            
        }
        
        //Determinar el valor de las mercancias
        //$request['RequestedShipment']['TotalInsuredValue']=array(
        //	'Ammount'=>$TotalProductValue,
        //	'Currency'=>'NMP'
        //);
        
        //$package->setParameter('insuredCurrency','USD');
		//$package->setParameter('insuredValue','1001.00');


        
        $request['RequestedShipment']['PackageCount'] = $GroupPackageCount;
        $request['RequestedShipment']['LabelSpecification']=array(
		'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
		'ImageType' => 'PDF',  // valid values DPL, EPL2, PDF, ZPLII and PNG
		'LabelStockType' => 'PAPER_7X4.75'
		);
        
        
        
        
        
        $request['RequestedShipment']['RequestedPackageLineItems'] =$PackageLineItems;
        
        
        //var_dump($request);
        
        try {
            
        	if(setEndpoint('changeEndpoint')){
        	    
        		$newLocation = $client->__setLocation(setEndpoint('endpoint'));
        		
        	}
        	
        	//$response = $client -> getRates($request);
			$response = $client->processShipment($request);  // FedEx web service invocation

             $fedex_response = [];
             $fedex_response["message"] = $client->__getLastResponse();
             
            if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){  
            	
            	
            	$shipmentReply = $response->CompletedShipmentDetail;
            	
            	
            	/*<MasterTrackingId>
					<TrackingIdType>FEDEX</TrackingIdType>
					<FormId>0301</FormId>
					<TrackingNumber>794670479456</TrackingNumber>
				</MasterTrackingId>*/
                
            	$rateReply = $response -> ShipmentRating;
            	
            	
            	//var_dump($shipmentReply);
            	
            	//echo '<table border="1">';
                //echo '<tr><td>Service Type</td><td>Amount</td><td>Delivery Date</td></tr><tr>';
            	$serviceType = $shipmentReply -> ServiceTypeDescription;
            	
            	
            	if($shipmentReply->ShipmentRateDetails && is_array($shipmentReply->ShipmentRateDetails)){
            		
        			$amount = $shipmentReply->ShipmentRateDetails[0]->TotalNetCharge->Amount;
        			
        		}elseif($shipmentReply->ShipmentRateDetails && ! is_array($shipmentReply->ShipmentRateDetails)){
        			
        			$amount = $shipmentReply->ShipmentRateDetails->TotalNetCharge->Amount;
        			
        		}
        		
        		if($shipmentReply->OperationalDetail->DeliveryDate){
        			
        				$deliveryDate=  $shipmentReply->OperationalDetail->DeliveryDate;
        		}
        		
             
                
                $fedex_response = [];
                $fedex_response["TrackingNumber"] = $shipmentReply->MasterTrackingId->TrackingNumber;
                $fedex_response['ServiceType'] = $serviceType ;
                $fedex_response['Amount'] = $amount ;
                $fedex_response['DeliveryDate'] = $deliveryDate;
                $fedex_response['success'] = true;
                $fedex_response["last_request"] = $client->__getLastRequest();
                $fedex_response["last_response"] = $client->__getLastResponse();
               // printSuccess($client, $response);
               
                
            }else{
                
                 $fedex_response['success'] = false;
                 //$fedex_response['message'] = $request;
                //printError($client, $response);
                
            } 
            
            //writeToLog($client);    // Write to log file   
            
        } catch (SoapFault $exception) {
            
             $fedex_response['success'] = false;
             $fedex_response['message'] = $exception->faultstring;
           printFault($exception, $client);  
           
        }
        
        return $fedex_response;


        
    }
    
    
	
	
	/*public function getHTML($sale_order){


        
		
		//$logo = $this->account_customization->logo_absolute_path;
		
		$html = "";

		 $title = '
			<table width="100%">
				<tr>
					<td width="33%"><img src="'.$logo.'"></td>
					<td width="33%" align="center" style="font-size:24px;font-weight:normal;font-family:Arial;color:#666666;">Orden de Venta</td>
					<td width="33%"></td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="33%" align="center"></td>
					<td width="33%" >
						<span style="font-size:18px;">
						#<span style="font-weight:bold">'.$sale_order["SaleOrderFolio"].'</span></span> <br>
						'.$sale_order["SaleOrderDate"].'
					</td>
				</tr>
			</table>
		';

		
		$header = '<div align="center" style="height:135px;background-color: #f0f2ff;background: transparent scroll left top;border-radius: 4mm;font-size:18pt;font-weight:bold;font-style:italic;vertical-align:middle;">'.$title.'</div>';

		$html=$header.'
				<table width="100%">
					<tr>
						<td width="33%">
						    <p>'."Shelly".'</p>
						</td>
						<td width="18%"></td>
						<td width="48%">
							<p>
								<span style="font-weight: bold">Atn:'.$sale_order["customer"]["CustomerName"].'</span><br>
								'.$sale_order["customer"]["CustomerAddress"].'<br>
				  				'.$sale_order["customer"]["CustomerCity"].', '.$sale_order["customer"]["CustomerCountry"].'<br>
			       				'.$sale_order["customer"]["CustomerTelephone"].'
							</p>
						</td>
					</tr>
					<tr>
						<td width="80%" colspan="2">Titulo: '.$sale_order["SaleOrderTitle"].'</td>
						
						<td width="20%" style="text-align:right"><br>Tipo de cambio: $'.$sale_order["SaleOrderExchangeRate"].'</td>
					</tr>
				</table>
				
			';


			
			$data_table_header = '<br>
				<table width="100%" style="border-top: 1mm solid #666666;">
					<tr style="background-color:#B0B0B0; color:#ffffff;">
						<td width="40%" >Descripcion</td>
						<td width="8%">UM</td>
						<td width="6%">Cant.</td>
						<td width="10%">Precio</td>
						<td width="12%" style="text-align:center">Desc. %</td>
						<td width="12%" style="text-align:center">IVA %</td>
						<td width="15%" >Total</td>
					</tr>';


			$data_table_data='		<tr >';

			foreach ($sale_order["items"] as $item) {

				$item = (array)$item;
				$Description = "";



				$DiscountAmountText = "";
                if( $item["SaleOrderDetailDiscount"] > 0 ){

					$DiscountAmount =  ( $item["SaleOrderDetailDiscount"] / 100) * $item["SaleOrderDetailTotal"];
					$DiscountAmount =  number_format((float) $DiscountAmount, 2, '.', '');
					$DiscountAmountText = ''.$item["SaleOrderDetailDiscount"].'% <span style="color: #444444" >($'.$DiscountAmount.')</span>';
				}

				$TaxAmount="";
				if($item["SaleOrderDetailTax"] > 0){

					$TaxAmount = ( $item["SaleOrderDetailTax"] / 100) * $item["SaleOrderDetailTotal"];
					$TaxAmount =  number_format((float) $TaxAmount, 2, '.', '');
					$TaxAmount = ''.$item["SaleOrderDetailTax"].'% <span style="color: #444444" >($'.$TaxAmount.')</span>';


				}


				if(isset($item["Product"]["ProductName"])){

					
					$Description = $item["Product"]["ProductName"];
				}else
				{
					$Description = $item["SaleOrderDetailDescription"];
				}

				$data_table_data .='
					<tr >
						<td style="font-size:12px" >'.$Description.'</td>
						<td style="text-align:center">'.$item["SaleOrderDetailUM"].'</td>
						<td style="text-align:center">'.$item["SaleOrderDetailQTY"].'</td>
						<td style="text-align:right">'.$item["SaleOrderDetailPrice"].'</td>
						<td style="text-align:center; font-size:12px">'.$DiscountAmountText .'</td>
						<td style="text-align:center; font-size:12px">'.$TaxAmount.'</td>
						<td style="text-align:right">'.$item["SaleOrderDetailTotal"].'</td>
					</tr>
				';
				
			}

			if( $sale_order["CurrencyId"] == 1 ){
				$totales = '
				<tr>
									<td width="60%">Total MXN</td>
									<td width="40%" style="text-align:right;border-top: 1px solid #666666">$ '.$sale_order["SaleOrderTotal"].'</td>
								</tr>
								<tr>
									<td width="60%">Total Dlls</td>
									<td width="40%" style="text-align:right">$ '.$sale_order["SaleOrderTotalDlls"].'</td>
								</tr>';
			}
			else
			{
				$totales = '
				<tr>
									<td width="60%">Total DLLS</td>
									<td width="40%" style="text-align:right;border-top: 1px solid #666666">$ '.$sale_order["SaleOrderTotal"].'</td>
								</tr>
								<tr>
									<td width="60%">Total MXN</td>
									<td width="40%" style="text-align:right">$ '.$sale_order["SaleOrderTotalDlls"].'</td>
								</tr>';

			}



			$ValorLetras = num2letras($sale_order["SaleOrderTotal"], false, true,$sale_order["CurrencyName"]) ; 
			
			$data_table_footer = '
				<tr><td colspan="7" style="border-top: 1px solid #666666"><br><br></td>
				</tr>
				<tr>
						<td width="60%" colspan="4" style="vertical-align: top" >Descripci&oacute;n, comentarios o notas:<br><br>

						'.$sale_order["SaleOrderDescription"].'
						</td>
						<td width="40%" colspan="3"  style="text-align:right">
							<table width="100%">
								<tr>
									<td width="60%">Subtotal</td>
									<td width="40%" style="text-align:right">$ '.$sale_order["SaleOrderSubtotal"].'</td>
								</tr>
								<tr>
									<td width="60%">IVA</td>
									<td width="40%" style="text-align:right">$ '.$sale_order["SaleOrderTaxes"].'</td>
								</tr>
								<tr>
									<td width="60%">Descuentos</td>
									<td width="40%" style="color:#0000FF;text-align:right">-$ '.$sale_order["SaleOrderDiscounts"].'</td>
								</tr>
								'.$totales.'
							</table>
						</td>
						
					</tr>
					<tr><td colspan="7" style="text-align: center; font-size:13px"><br><br>'.$ValorLetras.'</td>
				</tr>
			';


			


		    $data_table = $data_table_header.$data_table_data.$data_table_footer.'</table>';

		    $html = $html.$data_table;

		    return $html;

	}*/
	
}

?>