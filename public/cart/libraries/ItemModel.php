<?php
namespace App\Lib;

class ItemModel extends Model {

	protected $item_id = null;
	protected $item_details = null;

	public function init($item_id) {
		$this->item_id = $item_id;
	}

  public function getDetails() {
    if(!$this->item_details) {
      $item_id = (int)$this->item_id;
      $statement = $this->db->prepare("select p.*,pp.product_picture_default,pp.product_picture_id,pp.product_picture_thumbnail,pp.product_picture_url,p.product_external_id,cat.category_external_link,cat.category_external_image_site FROM erp_products p  left outer join erp_product_categories cat ON cat.category_id = p.category_id LEFT OUTER JOIN erp_product_pictures pp ON pp.product_id = p.product_id AND pp.product_picture_default = 1 where p.product_id = :item_id");
      $result = $statement->execute(['item_id' => $item_id]);
      $item_details = $result ? $statement->fetch() : [];
      
      
      
      
      if(isset($item_details["category_external_link"])){
			
			
			    $item_details["category_external_link"]  = str_replace("{{product_external_id}}",$item_details["product_external_id"],$item_details["category_external_link"]);
			
		} else{
			
			$item_details["category_external_link"] = null;
		}
		
		
			if(isset($item_details["category_external_image_site"])){
			
			$item_details["product_picture_id"] = 99;
			$item_details["product_picture_url"] = str_replace("{{product_external_id}}",$item_details["product_external_id"],$item_details["category_external_image_site"]);
			//$ProductArray["default_picture"]["ProductPictureThumbnail"] = str_replace("{{product_external_id}}",$item_details["product_external_id"],$item_details["category_external_image_site"]);

			
		} else{
			$item_details["product_picture_url"] = "/backend/" . $item_details["product_picture_url"];
			$item_details["category_external_image_site"] = null;
		}
		
		

      $this->item_details = $item_details;
    }

    return $this->item_details;
  }

}
