<?php
namespace App\Lib;

require_once('../../../config/configuration.php');




class UserModel extends Model {

	protected $user_id = null;
	protected $user_details = null;
	protected $user_customer_details = null;

	public function init($user_id) {
		$this->user_id = $user_id;
	}

	/*public function initWithToken($token) {
		
        $statement = $this->db->prepare("select id from users where token = :token");
        $result = $statement->execute(['token' => $token]);
        $user_details = $result ? $statement->fetch() : [];

        if(!empty($user_details['id'])) {
            $this->user_id = $user_details['id'];
            
        }
    }
    */
    
    public function initWithToken($token){

	



    	$statement = $this->db->prepare('
    			SELECT 
    			session_id, 
    			oauth_sessions.client_id, 
    			oauth_sessions.owner_id, 
    			oauth_sessions.owner_type
       		FROM oauth_session_access_tokens 
       		JOIN oauth_sessions ON oauth_sessions.id = session_id 
       		WHERE access_token = :accessToken 
       		AND access_token_expires >= :accessTokenExpires
      		'
      			
      		);
      	
      	
      	$result = $statement->execute([
      				':accessToken' => $token , ':accessTokenExpires' => time()

      			]);
      			
      			
      	$row = $result ? $statement->fetch() : [];
      	
    
      	
      	$this->user_id = $row['owner_id'];
    		
        
            
      

       

    }
    
	public function getDetails() {
		
		if(!$this->user_details) {
			$user_id = (int)$this->user_id;
			$statement = $this->db->prepare("select user_name, user_email from oauth_users where user_id = :user_id");
			$result = $statement->execute(['user_id' => $user_id]);
			$user_details = $result ? $statement->fetch() : [];

			$this->user_details = $user_details;
		}

		return $this->user_details;
	}
	
	
	public function getCustomerDetails() {
		
		if(!$this->user_customer_details) {
			
			$user_id = (int)$this->user_id;
			
			$statement = $this->db->prepare("select * from erp_customers where user_id = :user_id");
			$result = $statement->execute(['user_id' => $user_id]);
			$user_details = $result ? $statement->fetch() : [];

			$this->user_customer_details = $user_details;
		}

		return $this->user_customer_details;
	}
	
	public function welcomeMessage(){
		
		if(!$this->user_details) {
			
			$user_id = (int)$this->user_id;
			$statement = $this->db->prepare("select user_name, user_email from oauth_users where user_id = :user_id");
			$result = $statement->execute(['user_id' => $user_id]);
			$user_details = $result ? $statement->fetch() : [];

			$this->user_details = $user_details;
			
			
			$body = file_get_contents( "../templates/welcome-email.html" );
			
			
			
			//Solo va a entrar aqui si estamos en produccion en el aws
	    	$sendgrid_username = $_ENV['SENDGRID_USERNAME'];
	        $sendgrid_password = $_ENV['SENDGRID_PASSWORD'];
	        $to                = $user_details["user_email"]; 
	        
	        
	       
	        
	    
    	
    		$body = str_replace("[USER NAME]",$user_details["user_name"],$body);
    		$body = str_replace("[EMAIL]",$user_details["user_email"],$body);
    		//$body = str_replace("[EMAIL]",$user_details["EMAIL"],$body);
    	
    	
	        
	        $sendgrid = new \SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));
	        $email    = new \SendGrid\Email();
	        $email->addTo($to)->
	               setFrom('sistema@cuatroo.com')->
	               setFromName('Shelly')->
	               setSubject("Bienvenido a shelly")->
	               setHtml($body)->
	               addHeader('X-Sent-Using', 'SendGrid-API')->
	               addHeader('X-Transport', 'web');
	        $response = $sendgrid->send($email);
	        
	        return $response;
		}
		
		return $this->user_details;
		
	}
	
	
	public function contactMessage($message){
		
		//if(!$this->user_details) {
			
		
			
			$body = file_get_contents( "../templates/mensaje-enviado.html" );
			
			
			
			//Solo va a entrar aqui si estamos en produccion en el aws
	    /*	$sendgrid_username = $_ENV['SENDGRID_USERNAME'];
	        $sendgrid_password = $_ENV['SENDGRID_PASSWORD'];
	        $to                = $message["email"]; 
	        

	        $sendgrid = new \SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));
	        $email    = new \SendGrid\Email();
	        $email->addTo($to)->
	               setFrom('sistema@cuatroo.com')->
	               setFromName('Shelly')->
	               setSubject("Mensaje enviado")->
	               setHtml($body)->
	               addHeader('X-Sent-Using', 'SendGrid-API')->
	               addHeader('X-Transport', 'web');
	        $response = $sendgrid->send($email);
	        */
	        
	   
	        
	        
	        $to = "ivan.altamirano@gmail.com";
	        
	        $body = file_get_contents( "../templates/message-email.html" );
	        
	        
	        $body = str_replace("[MESSAGE]",$message["message"],$body);
    		$body = str_replace("[EMAIL]",$message["email"],$body);
    		$body = str_replace("[NAME]",$message["name"],$body);
    		$body = str_replace("[SUBJECT]",$message["subject"],$body);
    		
    		




	        
	        
	        $sendgrid = new \SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));
	        $email    = new \SendGrid\Email();
	        $email->addTo($to)->
	               setFrom('sistema@cuatroo.com')->
	               setFromName('Shelly')->
	               setSubject("Nuevo mensaje desde el portal!")->
	               setHtml($body)->
	               addHeader('X-Sent-Using', 'SendGrid-API')->
	               addHeader('X-Transport', 'web');
	        $response = $sendgrid->send($email);
	        
	        return $response;
		//}
		
		//return $this->user_details;
		
	}
	
	public function recoveryMessage($user_email){
		
		date_default_timezone_set('America/Tijuana');
		
		if(!$this->user_details) {
			
			//$user_id = (int) $this->user_id;
			$statement = $this->db->prepare("select user_id,user_name, user_email from oauth_users where user_email = :user_email");
			$result = $statement->execute([':user_email' => $user_email]);
			
			
			
			$user_details = $result ? $statement->fetch() : [];
			
			//var_dump($user_email);
			//var_dump($user_details);
			//var_dump($statement->fetch());
			
			
			if(!$user_details){
				
				throw new \Exception("No se encontró el correo electrónico");
				
			}

            
			
			$this->user_details = $user_details;
			
			
			$body = file_get_contents( "../templates/recupera-password.html" );
			
			
			
			//Solo va a entrar aqui si estamos en produccion en el aws
	    	$sendgrid_username = $_ENV['SENDGRID_USERNAME'];
	        $sendgrid_password = $_ENV['SENDGRID_PASSWORD'];
	        $to                = $user_details["user_email"]; 
	        
	        $tokenInfo = $this->generateRandomString($user_details["user_id"]);
	       
	        //https://shelly-ialtamirano.c9users.i
	    	$recovery_page = "https://www.shellydiesel.com/cambiar-contrasena.html?token=".$tokenInfo;
	    	//$recovery_page = "https://shelly-ialtamirano.c9users.io/cambiar-contrasena.html?token=".$tokenInfo;
    	
    		$body = str_replace("[USER NAME]",$user_details["user_name"],$body);
    		$body = str_replace("[[recovery_page]]",$recovery_page,$body);
    		//$body = str_replace("[EMAIL]",$user_details["EMAIL"],$body);
    	
    	
	        
	        $sendgrid = new \SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));
	        $email    = new \SendGrid\Email();
	        $email->addTo($to)->
	               setFrom('sistema@cuatroo.com')->
	               setFromName('Shelly')->
	               setSubject("Shelly - Recuperación de contraseña")->
	               setHtml($body)->
	               addHeader('X-Sent-Using', 'SendGrid-API')->
	               addHeader('X-Transport', 'web');
	        $response = $sendgrid->send($email);
	        
	        return $response;
		}
		
		return $this->user_details;
		
	}
	
	
	public function changePassword($user) {
		
		
		
		if(!$this->user_details) {
			
			$user_id = (int)$this->user_id;
			$statement = $this->db->prepare("select user_name, user_email from oauth_users where user_id = :user_id");
			$result = $statement->execute(['user_id' => $user_id]);
			$user_details = $result ? $statement->fetch() : [];

			$this->user_details = $user_details;
		}

		return $this->user_details;
	}
	
	
	
	
	
	private function generateRandomString($user_id,$length = 15) {
		
		
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $tokenInfo = '';
        
        for ($i = 0; $i < $length; $i++) {
            $tokenInfo .= $characters[rand(0, $charactersLength - 1)];
        }
        
		
        
		$sql = "INSERT INTO oauth_session_email_token (id ,id_user ,token,type,status,date_creation) VALUES (NULL ,:IdUser,:Token,:Type,1,NOW())";
		
		$statement = $this->db->prepare($sql);
		$result = $statement->execute(
		
		array
			( 
				':IdUser' 	    => $user_id,
				':Token' 	    => $tokenInfo,
				':Type'         => 1
			)
			
		);
			


		 return $tokenInfo;
		 
		 
		//
        
        
  
    }
}
