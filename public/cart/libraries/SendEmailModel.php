<?php

 namespace App\Lib;
 
 
class SendEmailModel  extends Model {
    
    
   
    
    private $statements = array("SELECT"      => "SELECT * FROM oauth_session_email_token WHERE token=:Token and status=1",
                                "UPDATE"      => "UPDATE oauth_session_email_token set status=0 WHERE token=:Token",
                                "INSERTTOKEN" => "INSERT INTO oauth_session_email_token (id ,id_user ,token,type,status,date_creation) VALUES (NULL ,:IdUser,:Token,:Type,1,NOW())"
        	                   );
		
	private $db;


	
    public function sendCreationEmail($account){
        
        $tokenInfo = $this->EmailToken(2,$account->UserId);
        
        $body = file_get_contents( "templateEmail/confirmation.html" );
            
        $activation_link = DEFAULT_SERVER_HOST."/".$tokenInfo."/Activate";
            
    	//$body = str_replace("[[ActivationLink]]",$activation_link,$body);
    	
    	$body = str_replace("[[User_Name]]",$account->UserName,$body);
    	
    	    
        //Enviar correo para confirmacion de creacion de la cuenta
        date_default_timezone_set('America/Hermosillo');
        
        //Solo va a entrar aqui si estamos en produccion en el aws
        $sendgrid_username = $_ENV['SENDGRID_USERNAME'];
        $sendgrid_password = $_ENV['SENDGRID_PASSWORD'];
        $to                = $account->UserEmail;
        
        $sendgrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));
        $email    = new SendGrid\Email();
        $email->addTo($to)->
               setFrom('soporte@enviapp.mx')->
               setFromName('ENVI')->
               setSubject('Bienvenido a ENVI')->
               setHtml($body)->
               addHeader('X-Sent-Using', 'SendGrid-API')->
               addHeader('X-Transport', 'web');
        $response = $sendgrid->send($email);
            
    	return $account;
    }
    
    public function RecoveryEmail($account){
        
        $tokenInfo = $this->EmailToken(1,$account->user_id);
        
        //echo '$account:'.$account->user_email;
       
        $body = file_get_contents( "templateEmail/password.html" );
            
        //Enviar correo para confirmacion de creacion de la cuenta
        date_default_timezone_set('America/Hermosillo');
        
        $recovery_page = DEFAULT_SERVER_HOST."/".$tokenInfo."/Recovery";
        
    	$body = str_replace("[[recovery_page]]",$recovery_page,$body);
    	
    	//Solo va a entrar aqui si estamos en produccion en el aws
    	$sendgrid_username = $_ENV['SENDGRID_USERNAME'];
        $sendgrid_password = $_ENV['SENDGRID_PASSWORD'];
        $to                = $account->user_email; 
        
        $sendgrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));
        $email    = new SendGrid\Email();
        $email->addTo($to)->
               setFrom('soporte@enviapp.mx')->
               setFromName('ENVI')->
               setSubject('Solicitud de restablecer Contraseña')->
               setHtml($body)->
               addHeader('X-Sent-Using', 'SendGrid-API')->
               addHeader('X-Transport', 'web');
        $response = $sendgrid->send($email);
        
        return $account->user_email;
    }
    
    public function WelcomeEmail($account){
        
        //$tokenInfo = $this->EmailToken(2,$account->UserId);
       
        $body = file_get_contents( "templates/welcome-emal.html" );
        //$body = file_get_contents( "templateEmail/welcome.html" );
            
        //Enviar correo para confirmacion de creacion de la cuenta
        date_default_timezone_set('America/Tijuana');

    	$body = str_replace("[USER NAME]",$account->user_name,$body);
    	
    	 //$activation_link = DEFAULT_SERVER_HOST."/".$tokenInfo."/Activate";
    	 
    	//$body = str_replace("[[recovery_page]]",$activation_link,$body);
    	
    	//Solo va a entrar aqui si estamos en produccion en el aws
    	$sendgrid_username = $_ENV['SENDGRID_USERNAME'];
        $sendgrid_password = $_ENV['SENDGRID_PASSWORD'];
        $to                = $account->email; 
        
        $sendgrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));
        $email    = new SendGrid\Email();
        $email->addTo($to)->
               setFrom('sistema@cuatroo.com')->
               setFromName('Shelly')->
               setSubject('Bienvenido a SHELLY')->
               setHtml($body)->
               addHeader('X-Sent-Using', 'SendGrid-API')->
               addHeader('X-Transport', 'web');
        $response = $sendgrid->send($email);
        
        return $account->email;
    }
    
    public function ContactEmail($message){
        
        //$tokenInfo = $this->EmailToken(2,$account->UserId);
       //var_dump($message);
        $body = file_get_contents( "templateEmail/contact.html" );
        //$body = file_get_contents( "templateEmail/welcome.html" );
            
        //Enviar correo para confirmacion de creacion de la cuenta
        date_default_timezone_set('America/Hermosillo');

    	$body = str_replace("[[contact_name]]",$message->contact_name,$body);
    	$body = str_replace("[[contact_email]]",$message->contact_email,$body);
    	
    //	$activation_link = DEFAULT_SERVER_HOST."/".$tokenInfo."/Activate";
    	 
    	 $body = str_replace("[[contact_message]]",$message->contact_message,$body);
    	
    	//Solo va a entrar aqui si estamos en produccion en el aws
    	$sendgrid_username = $_ENV['SENDGRID_USERNAME'];
        $sendgrid_password = $_ENV['SENDGRID_PASSWORD'];
        $to                = CONST_WEB_MASTER_EMAIL; 
        
        $sendgrid = new SendGrid($sendgrid_username, $sendgrid_password, array("turn_off_ssl_verification" => true));
        $email    = new SendGrid\Email();
        $email->addTo($to)->
               setFrom('soporte@enviapp.mx')->
               setFromName('ENVI')->
               setSubject('Haz recibido un nuevo mensaje desde la pagina de ENVI')->
               setHtml($body)->
               addHeader('X-Sent-Using', 'SendGrid-API')->
               addHeader('X-Transport', 'web');
        $response = $sendgrid->send($email);
        
        return $account->UserEmail;
    }
    
    public function EmailToken($tipo,$user_id)
    {
        $tokenInfo = $this->generateRandomString();
        
        	//echo $user->user_id;
		$sql = $this->statements["INSERTTOKEN"];
		
		 $this->db->query($sql,
		 array
			( 
				':IdUser' 	    => $user_id,
				':Token' 	    => $tokenInfo,
				':Type'         => $tipo
			)
		);

		 return $tokenInfo;
		 
    }
    
    function generateRandomString($length = 15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        
        return $randomString;
    }
    
    function UserToken($token){
        
        $sql = $this->statements["SELECT"];
        	
        $result = $this->db->query($sql,
			array
			(
				':Token' => $token
			)
		);

		 $user=$result->fetch(PDO::FETCH_OBJ);

		
		return $user;
    }
    
    function UpdateToken($token){
        
    $sql = $this->statements["UPDATE"];
    $result = $this->db->query($sql,
    	array
    	(
    	':Token' => $token
    	)
    );
		
	return $token;
	
    }
    
}
?>
