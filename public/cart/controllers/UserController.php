<?php
namespace App\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface as ContainerInterface;
use App\Lib\UserModel;


define('MODE', 'sandbox');
define('CURRENCY', 'MXN');
define('APP_URL', 'http://demos.itechempires.com/paypal-javascript-express-checkout-live-demo/'); 
 
define("PayPal_CLIENT_ID", "Ab751ldbZ7u96UZeQ-B4-YxP5Upgjn_IWORzA-P-JJ9v20Q4P4WmvAK4p4EH65zwo2RksIHfVKLXVTxZ");
define("PayPal_SECRET", "EL0T8Rm1V4TtJ83ulaWJIg68vLKAb0sEsJyfzXrULLMRz3MjpWdAcdTj6kxz_k_9_Ig8xTOHEQ96RAhd");
define("PayPal_BASE_URL", "https://api.sandbox.paypal.com/v1/");


class UserController{

    private $db = null;
    const PAGE_LIMIT = 10;

    public function __construct(ContainerInterface $container) {
      $this->db = $container->get('db');
    }

    /**
     * Gives the user details of the cart owner
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function getUser(Request $request, Response $response, $args) {
    	$user_id = (int)$args['user_id'];
    	if(empty($user_id))
    		return $response->withJson(['error' => 'Invalid request'], 400);

      $user = new UserModel($this->db);
      $user->init($user_id);

      $user_details = $user->getDetails();
    	$response = $response->withJson($user_details);
    	return $response;
    }

    /**
     * Gets all the items in the cart,
     *      GET param "offset" the timestamp. Gives all items added before this.
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
   
    public function welcomeMessage(Request $request, Response $response, $args) {
        
        
   
       
        $request_body = $request->getParsedBody();
        
        $user_id = (int)$args['user_id'];
       
        
     
                
                $user = new UserModel($this->db);
                $user->init($user_id);
                $so = $user->welcomeMessage($user_id,$checkout,$payment);
                
         
                
                
                
                return $response->withJson(['success' => "Bienvenido a shelly!"]);
               
                
   
                
 
           
 
       
 
        
    }
    
    public function contactMessage(Request $request, Response $response, $args) {
        
        
   
       
        $request_body = $request->getParsedBody();
        
       // $user_id = (int)$args['user_id'];
       
        
     
                
                $user = new UserModel($this->db);
                //$user->init($user_id);
                $so = $user->contactMessage($request_body);
                
         
                
                
                
                return $response->withJson(['message' => "Gracias, su mensaje ha sido enviado!"]);
               
                
   
                
 
           
 
       
 
        
    }
    
    public function recoveryMessage(Request $request, Response $response, $args) {
        
        
   try{
       
        $request_body = $request->getParsedBody();
        //var_dump($request_body);
        
        $user_email = $request_body['email'];
       
        
     
                
                $user = new UserModel($this->db);

                $ud = $user->recoveryMessage($user_email);
            
            if($ud){
                return $response->withJson(['message' => "Gracias, su mensaje ha sido enviado!"]);
            }
                
         
                
                
                
                
               
   }catch(Exception $ex){
      return $response->withJson(['error' => $ex->getMessage()], 500);
       
   }  
   
                
 
           
 
       
 
        
    }
    
    

    
    public function getOrders(Request $request, Response $response, $args) {
        
        $user_id = (int) $args['user_id'];
        
        if(empty($user_id))
            return $response->withJson(['error' => 'Invalid request'], 400);

        $query_params = $request->getQueryParams();
       

        $cart = new OrderModel($this->db);
        $items = $cart->getOrders($user_id);
    
        return $response->withJson(['orders' => $items]);
    }
    
    
}
