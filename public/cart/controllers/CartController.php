<?php
namespace App\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface as ContainerInterface;
use App\Lib\UserModel;
use App\Lib\ItemModel;
use App\Lib\CartModel;
use App\Lib\OrderModel;

define('MODE', 'sandbox');
define('CURRENCY', 'MXN');
define('APP_URL', 'http://demos.itechempires.com/paypal-javascript-express-checkout-live-demo/'); 
 
define("PayPal_CLIENT_ID", "Ab751ldbZ7u96UZeQ-B4-YxP5Upgjn_IWORzA-P-JJ9v20Q4P4WmvAK4p4EH65zwo2RksIHfVKLXVTxZ");
define("PayPal_SECRET", "EL0T8Rm1V4TtJ83ulaWJIg68vLKAb0sEsJyfzXrULLMRz3MjpWdAcdTj6kxz_k_9_Ig8xTOHEQ96RAhd");
define("PayPal_BASE_URL", "https://api.sandbox.paypal.com/v1/");


class CartController{

    private $db = null;
    //private $adminEmail = "";
    const PAGE_LIMIT = 10;

    public function __construct(ContainerInterface $container) {
      $this->db = $container->get('db');
      //$this->$adminEmail = $container->get('db');
    }

    /**
     * Gives the user details of the cart owner
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function getUser(Request $request, Response $response, $args) {
    	$user_id = (int)$args['user_id'];
    	if(empty($user_id))
    		return $response->withJson(['error' => 'Invalid request'], 400);

      $user = new UserModel($this->db);
      $user->init($user_id);

      $user_details = $user->getDetails();
    	$response = $response->withJson($user_details);
    	return $response;
    }

    /**
     * Gets all the items in the cart,
     *      GET param "offset" the timestamp. Gives all items added before this.
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function getItems(Request $request, Response $response, $args) {
        
        $user_id = (int)$args['user_id'];
        
        if(empty($user_id))
            return $response->withJson(['error' => 'Invalid request'], 400);

        $query_params = $request->getQueryParams();
        $offset = !empty($query_params['offset']) ? $query_params['offset'] : null;

        $cart = new CartModel($this->db);
        $items = $cart->getItems($user_id, self::PAGE_LIMIT, $offset);
        
        $shipping = $this->calculateShipping($user_id,$items);
        
        

        return $response->withJson(['items' => $items,'shipping' => $shipping]);
    }

    /**
     * For updating the quantity of an existing item in the cart
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function updateCart(Request $request, Response $response, $args) {
        $user_id = (int)$args['user_id'];
        if(empty($user_id))
            return $response->withJson(['error' => 'Invalid request'], 400);

        $request_body = $request->getParsedBody();
        $item_id = isset($request_body['item_id']) ? (int)$request_body['item_id'] : null;
        $item_quanity = !empty($request_body['quantity']) ? (int)$request_body['quantity'] : null;
        $item = new ItemModel($this->db);
        $item->init($item_id);
        if(empty($item->getDetails()) || empty($item_quanity))
            return $response->withJson(['error' => 'Invalid request'], 400);

        $cart = new CartModel($this->db);
        list($status, $error) = $cart->modifyCart($user_id, $item_id, $item_quanity);

        return $status ? $response->withJson(['success' => true]) : $response->withJson(['error' => $error ?: "something went wrong"], 500);
    }

    /**
     * Removing an already exiting item from the cart
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function removeFromCart(Request $request, Response $response, $args) {
        $user_id = (int)$args['user_id'];
        if(empty($user_id))
            return $response->withJson(['error' => 'Invalid request'], 400);

        $request_body = $request->getParsedBody();
        $item_id = isset($request_body['item_id']) ? $request_body['item_id'] : null;
        $item = new ItemModel($this->db);
        $item->init($item_id);
        if(empty($item->getDetails()))
            return $response->withJson(['error' => 'Invalid request'], 400);

        $cart = new CartModel($this->db);
        list($status, $error) = $cart->remvoveFromCart($user_id, $item_id);

        return $status ? $response->withJson(['success' => true]) : $response->withJson(['error' => $error ?: "something went wrong"], 500);
    }

    /**
     * Adding an item in the cart
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function addToCart(Request $request, Response $response, $args) {
        $user_id = (int)$args['user_id'];
        if(empty($user_id))
            return $response->withJson(['error' => 'Invalid request'], 400);

        $request_body = $request->getParsedBody();
        $item_id = isset($request_body['item_id']) ? $request_body['item_id'] : null;
        $item_quanity = isset($request_body['quantity']) ? (int)$request_body['quantity'] : 1;
        $item = new ItemModel($this->db);
        $item->init($item_id);
        if(empty($item->getDetails()))
            return $response->withJson(['error' => 'Invalid request'], 400);

        $cart = new CartModel($this->db);
        list($status, $error) = $cart->addToCart($user_id, $item_id, $item_quanity);

        return $status ? $response->withJson(['success' => true]) : $response->withJson(['error' => $error ?: "something went wrong"], 500);
    }
    
    public function checkPayment(Request $request, Response $response, $args) {
        
        
      // var_dump($checkout);
       
        $request_body = $request->getParsedBody();
        
        $user_id = (int)$args['user_id'];
        $checkout = $request_body;
        $payment  = $checkout["payment"];
        
        
        //var_dump($checkout);
        
       // return $status ? $response->withJson(['success' => true]) : $response->withJson(['error' => $error ?: "something went wrong"], 500);
        
        
        $payment_id = $payment["id"];

        
        
        // request http using curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, PayPal_BASE_URL . 'oauth2/token');
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_USERPWD, PayPal_CLIENT_ID . ":" . PayPal_SECRET);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        $result = curl_exec($ch);
        $accessToken = NULL;
 
 
        if (empty($result)) {
            
            return FALSE;
            
        } else {
 
            // get Order details along with the status and update order
            $json = json_decode($result);
            $accessToken = $json->access_token;
            
            
            $curl = curl_init(PayPal_BASE_URL . 'payments/payment/' . $payment_id);
            curl_setopt($curl, CURLOPT_POST, FALSE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, FALSE);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $accessToken,
                'Accept: application/json',
                'Content-Type: application/xml'
            ));
            
            $response_curl = curl_exec($curl);
            $result = json_decode($response_curl);
            
            
         
            
            $state = $result->state;
            $total = $result->transactions[0]->amount->total;
            $currency = $result->transactions[0]->amount->currency;
            $subtotal = $result->transactions[0]->amount->details->subtotal;
            $recipient_name = $result->transactions[0]->item_list->shipping_address->recipient_name;
        
        
            
            
            curl_close($ch);
            curl_close($curl);
            
            //var_dump($result);
            
 /*$order = new OrderModel($this->db);
 $order->sendEmai();*/
 
            if ($state == 'approved') {
                
                $order = new OrderModel($this->db);
                
                $so = $order->create($user_id,$checkout,$payment);
                
                $so->payment = $payment;
                
                $order->sendEmail($so);
                
                
                
                return $response->withJson(['order' => $so]);
               
                
   
                
 
            } else {
 
                $response->withJson(['error' => $error ?: "something went wrong"], 500);
                
            }
 
        }
 
 
        
    }
    
    public function getOrder(Request $request, Response $response, $args) {
        
        $order_id = (int) $args['order_id'];
        
        if(empty($order_id))
            return $response->withJson(['error' => 'Invalid request'], 400);

        $query_params = $request->getQueryParams();
       

        $cart = new OrderModel($this->db);
        $items = $cart->downloadFile($order_id);
    
        return true;//$response->withJson(['order' => $items]);
    }
    
    public function getOrders(Request $request, Response $response, $args) {
        
        $user_id = (int) $args['user_id'];
        
        if(empty($user_id))
            return $response->withJson(['error' => 'Invalid request'], 400);

        $query_params = $request->getQueryParams();
       

        $cart = new OrderModel($this->db);
        $items = $cart->getOrders($user_id);
    
        return $response->withJson(['orders' => $items]);
    }
    
    public function calculateShipping($user_id, $items){
        
        
        
        
        $user = new UserModel($this->db);
        $user->init($user_id);

        $user_details = $user->getDetails();
        $user_customer_details = $user->getCustomerDetails();
      
      
        
        $newline = "<br />";
        //The WSDL is not included with the sample code.
        //Please include and reference in $path_to_wsdl variable.
        $path_to_wsdl = "../libraries/RateService_v22.wsdl";
        
        ini_set("soap.wsdl_cache_enabled", "0");
         
        $client = new \SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
        
        $request['WebAuthenticationDetail'] = array(
        	'ParentCredential' => array( 'Key' => getProperty('parentkey'),'Password' => getProperty('parentpassword')
        	),
        	'UserCredential' => array( 'Key' => getProperty('key'), 'Password' => getProperty('password')
        	)
        ); 
        
         $request['WebAuthenticationDetail'] = array(
        	
        	'UserCredential' => array( 'Key' => getProperty('key'), 'Password' => getProperty('password')
        	)
        ); 
        
        $request['ClientDetail'] = array( 'AccountNumber' => getProperty('shipaccount'), 'MeterNumber' => getProperty('meter')
        );
        
        $request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request using PHP ***');
        $request['Version'] = array( 'ServiceId' => 'crs', 'Major' => '22', 'Intermediate' => '0',  'Minor' => '0' );
        $request['ReturnTransitAndCommit'] = true;
        $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
        $request['RequestedShipment']['ShipTimestamp'] = date('c');
        //$request['RequestedShipment']['ServiceType'] = 'INTERNATIONAL_PRIORITY'; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
        $request['RequestedShipment']['ServiceType'] = 'FEDEX_EXPRESS_SAVER'; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
        $request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING'; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
       
       
       
        
         $shipper = array(
			'Contact' => array(
				'PersonName' => 'Shelly',
				'CompanyName' => 'Comercializadora Shelly',
				'PhoneNumber' => '6643266410'
			),
			'Address' => array(
				'StreetLines' => array('Blvd. Manuel J. Clouthier #114-A7','Col. Campestre Murua Martinez'),
				'City' => 'Tijuana',
				'StateOrProvinceCode' => 'BC',
				'PostalCode' => '22455',
				'CountryCode' => 'MX'
			)
		);
	
	
	//var_dump($user_customer_details);
	
		$recipient = array(
		    'Contact' => array(
			    'PersonName' => $user_customer_details['customer_name'],
			    'CompanyName' => $user_customer_details['customer_name'],
			    'PhoneNumber' => $user_customer_details['customer_telephone']
		    ),
		    'Address' => array(
			    'StreetLines' => array($user_customer_details['customer_address'],$user_customer_details['customer_delivery_place']),
			    'City' => $user_customer_details['customer_city'],
			    'StateOrProvinceCode' => $user_customer_details['customer_state'],
			    'PostalCode' => $user_customer_details['customer_cp'],
			    'CountryCode' => 'MX',
			    'Residential' => false
		    )
	    );
	
        $shippingChargesPayment = array(
    		'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
    		'Payor' => array(
    			'ResponsibleParty' => array(
    				'AccountNumber' => getProperty('billaccount'),
    				'CountryCode' => 'MX'
    			)
    		)
    	);
	
        $request['RequestedShipment']['Shipper'] = $shipper;
        $request['RequestedShipment']['Recipient'] = $recipient;
        $request['RequestedShipment']['ShippingChargesPayment'] = $shippingChargesPayment;
        
        $PackageCount = count($items);
        
        
        
        $PackageLineItems = array();
        $TotalProductValue = 0;
        
    $GroupPackageCount = 0;
       for ($i = 0; $i < $PackageCount; $i++) {


            $item = $items[$i];
            
            
	        $packageLineItem = array();
	        $packageLineItem['SequenceNumber'] = $i+1;
	        $packageLineItem['GroupPackageCount'] = $item['quantity'];
	        
	        
	        $packageLineItem['Weight'] = array('Value' => floatval($item['item']['product_weight']),'Units' => $item['item']['product_weight_unit']);
	        //$packageLineItem['Weight'] = array('Value' => $item['item']['product_weight'],'Units' => 'KG');
	        
	        if(isset($item['item']['product_length']) && isset($item['item']['product_width']) && isset($item['item']['product_height']) && isset($item['item']['product_dimensions_unit']))
	        {
	            $packageLineItem['Dimensions'] = array('Length' => floatval($item['item']['product_length']),'Width' => floatval($item['item']['product_width']),'Height' => floatval($item['item']['product_height']),'Units' => $item['item']['product_dimensions_unit']);
	            //$packageLineItem['Dimensions'] = array('Length' => $item['item']['product_length'],'Width' => $item['item']['product_width'],'Height' => $item['item']['product_height'],'Units' => 'cm');
	        }
	        
	        $GroupPackageCount = $GroupPackageCount + intval($item['quantity']);
	        $TotalProductValue = $TotalProductValue + ( intval($item['quantity']) * floatval($item['item']['product_price']) );
	        $PackageLineItems[] = $packageLineItem;
	        
            
        }
        
        //Determinar el valor de las mercancias
        $request['RequestedShipment']['TotalInsuredValue']=array(
        	'Ammount'=>$TotalProductValue,
        	'Currency'=>'NMP'
        );
        
        $request['RequestedShipment']['PackageCount'] = $GroupPackageCount;
        
        
        
        
        
        $request['RequestedShipment']['RequestedPackageLineItems'] =$PackageLineItems;
        
        
        //var_dump($request);
        
        try {
            
        	if(setEndpoint('changeEndpoint')){
        	    
        		$newLocation = $client->__setLocation(setEndpoint('endpoint'));
        		
        	}
        	
        	$response = $client -> getRates($request);
                
             $fedex_response = [];
             $fedex_response["message"] = $client->__getLastResponse();
             
            if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){  
                
            	$rateReply = $response -> RateReplyDetails;
            	//echo '<table border="1">';
                //echo '<tr><td>Service Type</td><td>Amount</td><td>Delivery Date</td></tr><tr>';
            	$serviceType = $rateReply -> ServiceType;
            	
            	
            	if($rateReply->RatedShipmentDetails && is_array($rateReply->RatedShipmentDetails)){
        			$amount = $rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount;
        		}elseif($rateReply->RatedShipmentDetails && ! is_array($rateReply->RatedShipmentDetails)){
        			$amount = $rateReply->RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Amount;
        		}
                if(array_key_exists('DeliveryTimestamp',$rateReply)){
                	$deliveryDate=  $rateReply->DeliveryTimestamp;
                }else if(array_key_exists('TransitTime',$rateReply)){
                	$deliveryDate= $rateReply->TransitTime ;
                }else {
                	//$deliveryDate='<td>&nbsp;</td>';
                }
                //echo $serviceType . $amount. $deliveryDate;
                //echo '</tr>';
                //echo '</table>';
                
                $fedex_response = [];
                $fedex_response['ServiceType'] = $rateReply -> ServiceType ;
                $fedex_response['Amount'] = $amount ;
                $fedex_response['DeliveryDate'] = $deliveryDate;
                $fedex_response['success'] = true;
                $fedex_response["last_request"] = $client->__getLastRequest();
                $fedex_response["last_response"] = $client->__getLastResponse();
               // printSuccess($client, $response);
                
            }else{
                
                 $fedex_response['success'] = false;
                  $fedex_response["last_request"] = $client->__getLastRequest();
                 //$fedex_response['message'] = $request;
                //printError($client, $response);
                
            } 
            
            //writeToLog($client);    // Write to log file   
            
        } catch (SoapFault $exception) {
            
             $fedex_response['success'] = false;
             $fedex_response['message'] = $exception->faultstring;
             $fedex_response["last_request"] = $client->__getLastRequest();
             printFault($exception, $client);  
           
        }
        
        return $fedex_response;


        
    }
    
    
    
}
