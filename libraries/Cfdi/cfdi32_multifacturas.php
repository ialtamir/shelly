<?php
// <!-- phpDesigner :: Timestamp [20/01/2014 09:59:46 p.m.] -->
date_default_timezone_set('America/Mexico_City');
include('../../libraries/phpqrcode/qrlib.php'); 
     

/*
codigo de error
0 = OK
1 = SALDO INSUFICIENTE MULTIFACTURAS
2 = RFC, XML o SELLO INVALIDO
3 = TIMEOUT
4 = USUARIO O CONTRASE�A INCORRECTOS EN EL PAC
5 = USUARIO O CONTRASE�A INCORRECTOS EN MULTIFACTURAS
6 = PAC SIN SALDO
7 = Internos
*/
function array_map_recursive($fn, $arr) {
    $rarr = array();
    foreach ($arr as $k => $v) {
        $rarr[$k] = is_array($v)
            ? array_map_recursive($fn, $v)
            : $fn($v); // or call_user_func($fn, $v)
    }
    return $rarr;
}

function cfdi_generar_xml($datos)
{

    if($datos['html_a_txt']=='SI')
    {
        $datos= array_map_recursive('cfd_fix_dato_xml_html_txt', $datos);
    }


    if($datos['remueve_acentos']=='SI')
    {
        $datos= array_map_recursive('cfd_fix_dato_xml_acentos', $datos);
    }
    else
    {
        $datos= array_map_recursive('cfd_fix_dato_xml', $datos);
    }
            
    $ruta=$datos['SDK']['ruta'];
    $ruta=str_replace('\\','/',$ruta);
 

    $produccion=$datos['PAC']['produccion'];
        
        
    if($datos['xml']!='')
    {
// SOLO TIMBRAR
        $xml=$datos['xml'];
    }
    else
    {
// GENERA XML y SELLO
    
            $cer= $datos['conf']['cer'];
            $key= $datos['conf']['key'];
            $pass= $datos['conf']['pass'];

            $cer=str_replace('\\','/',$cer);
            $key=str_replace('\\','/',$key);
            
            //SI EL CERTIFICADO NO ESTA PREPARADO
            if(file_exists("$cer")==false OR  file_exists("$key")==false )
            {
        
                $res_certificado= certificado_pem($datos);
                $certificado_numero= $res_certificado['certificado_no_serie'];
                //crea archivo

                $file_target="$cer.txt";
                @unlink($file_target);
                if (file_exists($file_target)) {
                    @chmod($file_target, 0777);
                } // add write permission
                if (($wh = fopen($file_target, 'wb')) === false) {
                    return false;
                } // error messages.
                if (fwrite($wh, $certificado_numero) === false) {
                    fclose($wh);
                    return false;
                }
                fclose($wh);
                @chmod($file_target, 0777);
                
                                
            }
            if($datos['factura']['noCertificadoSAT']=='')
            {
                $datos['factura']['noCertificadoSAT']=file_get_contents("$cer.txt");
            }
           



        
            $Factura_Serie = $datos['factura']['serie'];
            $Factura_Numero = $datos['factura']['folio'];
            $Factura_Fecha_Expedicion = $datos['factura']['fecha_expedicion'];
            $Factura_Fecha_Expedicion = str_replace(' ', 'T', $Factura_Fecha_Expedicion);
        
            $certificado = $datos['factura']['certificado'];

            if($certificado=='ND' OR $certificado=='')
            {
                $cer=$datos['conf']['cer'];
                $certificado=cfd_certificado_pub($cer);

            }
            
            $noSertificadoSAT = $datos['factura']['noCertificadoSAT'];
        
        
            $total = $datos['factura']['total'];
            $total = sprintf('%1.2f', $total);
            
            

//TRANSLADOS
            $translados='';
            if($datos['impuestos']['translados']>0)
            {
                $transladostotal=0.00;
                foreach($datos['impuestos']['translados'] AS $id=>$datostranslados)
                {
                    $trasladoimpuesto=$datostranslados['impuesto'];
                    $transladotasa=$datostranslados['tasa'];
                    $transladoimporte=$datostranslados['importe'];
                    $transladoimporte = sprintf('%1.2f', $transladoimporte);
                    $translados.="<cfdi:Traslado impuesto=\"$trasladoimpuesto\" tasa=\"$transladotasa\" importe=\"$transladoimporte\" /> ";
                    $transladostotal=$transladostotal+$transladoimporte;
                }
                $translados="
                    <cfdi:Traslados>
                   $translados   
                    </cfdi:Traslados> ";

            }
            
//RETENCIONES            
            $retenidos='';
            if($datos['impuestos']['retenidos']>0)
            {
                $retenidostotal=0.00;
                foreach($datos['impuestos']['retenidos'] AS $id=>$datosretenidos)
                {
                    $retenidoimpuesto=$datosretenidos['impuesto'];
                    $retenidoimporte=$datosretenidos['importe'];
                    
                    $retenidoimporte = sprintf('%1.2f', $retenidoimporte);
                    $retenidos.="<cfdi:Retencion impuesto=\"$retenidoimpuesto\" importe=\"$retenidoimporte\" /> ";
                    $retenidostotal=$retenidostotal+$retenidoimporte;
                }
                $retenidos="
                    <cfdi:Retenciones>
                   $retenidos   
                    </cfdi:Retenciones>";

            }


            $transladostotal = sprintf('%1.2f', $transladostotal);
            $retenidostotal = sprintf('%1.2f', $retenidostotal);
            $impuestosfinal="
                  <cfdi:Impuestos totalImpuestosTrasladados=\"$transladostotal\"  totalImpuestosRetenidos=\"$retenidostotal\" >
                    $retenidos
                    $translados
                  </cfdi:Impuestos>
            ";


/*
            $trasladoimpuesto = 'IVA';
            $iva = $datos['impuestos']['translados'][0]['importe'];
            $iva = sprintf('%1.2f', $iva);
            $Factura_IVA_Tasa =  $datos['impuestos']['translados'][0]['tasa'];
            $Factura_IVA_Tasa = sprintf('%1.2f', $Factura_IVA_Tasa);
*/        
        
            $sub_total = $datos['factura']['subtotal'] + $datos['factura']['descuento'];
            $sub_total = sprintf('%1.2f', $sub_total);
            $descuento = 0;
        
            $Forma_de_Pago = $datos['factura']['forma_pago'];
        
            $Metodo_Pago = $datos['factura']['metodo_pago'];
            $tipocomprobante = $datos['factura']['tipocomprobante'];
            if($tipocomprobante=='')
            {
                $tipocomprobante='ingreso';
            }
        
        
        
        
        
            $emisorrfc = $datos['emisor']['rfc'];
            $emisorrfcmin=$emisorrfc;
            $emisorrfc = cfd_formato_rfc($emisorrfc);
            $emisornombre = $datos['emisor']['nombre'];
        
            $emisorcalle = $datos['emisor']['DomicilioFiscal']['calle'];
            $emisornoexterior = $datos['emisor']['DomicilioFiscal']['noExterior'];
            $emisornointerior = $datos['emisor']['DomicilioFiscal']['noInterior'];
            $emisorcolonia = $datos['emisor']['DomicilioFiscal']['colonia'];
            $emisorlocalidad = $datos['emisor']['DomicilioFiscal']['localidad'];
            $emisormunicipio = $datos['emisor']['DomicilioFiscal']['municipio'];
            if ($emisorlocalidad == 'ND') {
                $emisorlocalidad = $emisormunicipio;
            }
            if ($emisormunicipio == 'ND') {
                $emisormunicipio = $emisorlocalidad;
            }
        
        
            $emisorcodigopostal = $datos['emisor']['DomicilioFiscal']['CodigoPostal'];
            $emisorcodigopostal=sprintf('%05d',$emisorcodigopostal);
        
            $emisorestado = $datos['emisor']['DomicilioFiscal']['estado'];
        
            $emisorpais = $datos['emisor']['DomicilioFiscal']['pais'];
        
        
            $expedidocalle = $datos['emisor']['ExpedidoEn']['calle'];
            $expedidonoexterior = $datos['emisor']['ExpedidoEn']['noExterior'];
            $expedidocolonia = $datos['emisor']['ExpedidoEn']['noInterior'];
            $expedidocolonia = $datos['emisor']['ExpedidoEn']['colonia'];
            $expedidolocalidad = $datos['emisor']['ExpedidoEn']['localidad'];
            $expedidomunicipio = $datos['emisor']['ExpedidoEn']['municipio'];
            if ($expedidolocalidad == 'ND')
            {
                $expedidolocalidad = $expedidomunicipio;
            }
            if ($expedidomunicipio == 'ND')
            {
                $expedidomunicipio = $expedidolocalidad;
            }
        
            $expedidocodigopostal = $datos['emisor']['ExpedidoEn']['CodigoPostal'];
            $expedidoestado = $datos['emisor']['ExpedidoEn']['estado'];
            $expedidopais = $datos['emisor']['ExpedidoEn']['pais'];
            $expedidocodigopostal=sprintf('%05d',$expedidocodigopostal);
        
        
            $receptorrfc = $datos['receptor']['rfc'];
            $receptorrfc = cfd_formato_rfc($receptorrfc);
            $receptornombre = $datos['receptor']['nombre'];
        
            $receptorcalle = $datos['receptor']['Domicilio']['calle'];
            $receptornoexterior = $datos['receptor']['Domicilio']['noExterior'];
            $receptornointerior = $datos['receptor']['Domicilio']['noInterior'];
        
            $receptorcolonia = $datos['receptor']['Domicilio']['colonia'];
            $receptorlocalidad = $datos['receptor']['Domicilio']['localidad'];
            $receptormunicipio = $datos['receptor']['Domicilio']['municipio'];
            if ($receptorlocalidad == 'ND') {
                $receptorlocalidad = $receptormunicipio;
            }
            if ($receptormunicipio == 'ND') {
                $receptormunicipio = $receptorlocalidad;
            }
        
            $receptorcodigopostal = $datos['receptor']['Domicilio']['CodigoPostal'];
            $receptorestado = $datos['receptor']['Domicilio']['estado'];
            $receptorpais = $datos['receptor']['Domicilio']['pais'];
            $receptorcodigopostal=sprintf('%05d',$receptorcodigopostal);

//NOMINA

if(count($datos['nomina'])==0)
{
    $nominaxmlns='';
    $nomina='';
}
else
{
    
    $nominaxmlns='xmlns:nomina="http://www.sat.gob.mx/nomina"';
    $nomina='';
    $datosnomina=$datos['nomina']['datos'];
    $percepciones = $datos['nomina']['percepciones'];
    $deducciones= $datos['nomina']['deducciones'];
    $incapacidades= $datos['nomina']['incapacidades'];
    $horasextras= $datos['nomina']['horasextras'];
//NOMINA PERCEPCION    
    if(count($percepciones)>0)
    {
        $total_gravado=0.0;
        $total_excento=0.0;
        foreach($percepciones AS $id => $percepcion)
        {
     
            $TipoPercepcion=$percepcion['TipoPercepcion'];
            $Clave=$percepcion['Clave'];
            $Concepto=$percepcion['Concepto'];
            $ImporteGravado=$percepcion['ImporteGravado'];
            $ImporteExento=$percepcion['ImporteExento'];
            $total_gravado+=$ImporteGravado;
            $total_excento+=$ImporteExento;
            $nomina_percepciones.="<nomina:Percepcion TipoPercepcion=\"$TipoPercepcion\" Clave=\"$Clave\" Concepto=\"$Concepto\" ImporteGravado=\"$ImporteGravado\" ImporteExento=\"$ImporteExento\" />";
        }

        $nomina.="
        <nomina:Percepciones TotalGravado=\"$total_gravado\" TotalExento=\"$total_excento\">
        $nomina_percepciones
        </nomina:Percepciones>
        ";        
    }
    
//NOMINA DEDUCCION
    if(count($deducciones)>0)
    {
        $total_gravado=0.0;
        $total_excento=0.0;
        foreach($deducciones AS $id => $deduccion)
        {
            
            $TipoDeduccion=$deduccion['TipoDeduccion'];
            $Clave=$deduccion['Clave'];
            $Concepto=$deduccion['Concepto'];
            $ImporteGravado=$deduccion['ImporteGravado'];
            $ImporteExento=$deduccion['ImporteExento'];
            $total_gravado+=$ImporteGravado;
            $total_excento+=$ImporteExento;

            $nomina_ducciones.="<nomina:Deduccion TipoDeduccion=\"$TipoDeduccion\" Clave=\"$Clave\" Concepto=\"$Concepto\" ImporteGravado=\"$ImporteGravado\" ImporteExento=\"$ImporteExento\" />";
        }
        
        $nomina.="
        <nomina:Deducciones TotalGravado=\"$total_gravado\" TotalExento=\"$total_excento\">
        $nomina_ducciones
        </nomina:Deducciones>
        ";
    }


//NOMINA INCAPACIDADES
    if(count($incapacidades)>0)
    {

        foreach($incapacidades AS $id => $incapacidadx)
        {
            
            $DiasIncapacidad=$incapacidadx['DiasIncapacidad'];
            $TipoIncapacidad=$incapacidadx['TipoIncapacidad'];
            $Descuento=$incapacidadx['Descuento'];
            $nomina_incapacidades.="<nomina:Incapacidad DiasIncapacidad=\"$DiasIncapacidad\" TipoIncapacidad=\"$TipoIncapacidad\" Descuento=\"$Descuento\"/>";
        }
        
        $nomina.="
        <nomina:Incapacidades>$nomina_incapacidades
        </nomina:Incapacidades>
        ";
    }

//NOMINA HORAS EXTRAS
    if(count($horasextras)>0)
    {

        foreach($horasextras AS $id => $horasextra)
        {
            
            $Dias=$horasextra['Dias'];
            $TipoHoras=$horasextra['TipoHoras'];
            $HorasExtra=$horasextra['HorasExtra'];
            $ImportePagado=$horasextra['ImportePagado'];

            $nomina_horasextras.="<nomina:HorasExtra Dias=\"$Dias\" TipoHoras=\"$TipoHoras\" HorasExtra=\"$HorasExtra\"   ImportePagado=\"$ImportePagado\" />";
        }

        $nomina.="
        <nomina:HorasExtras>
        $nomina_horasextras
        </nomina:HorasExtras>
        ";
    }

    
    
    if(count($datosnomina)>0)
    {
        foreach($datosnomina AS $key=>$val)
        {
            $nomina_txt.=" $key=\"$val\" ";
        }
    }
    $antiguedad=intval($datosnomina['Antiguedad']);
    if($antiguedad==0)
    {
        
        $fecha_inicial=$datosnomina['FechaInicioRelLaboral'];
        $fecha_final=$datosnomina['FechaFinalPago'];
        
        list($ano1,$mes1,$dia1)=explode('-',$fecha_inicial);
        list($ano2,$mes2,$dia2)=explode('-',$fecha_final);
        
        $tiempo1= mktime (0,0,1,$mes1, $dia1, $ano1);
        $tiempo2= mktime (0,0,1,$mes2, $dia2, $ano2);
        $antiguedad=intval( ($tiempo2-$tiempo1)/(3600*24)   );
        
        $nomina_txt.=" Antiguedad=\"$antiguedad\" ";
    }
    
    
    
    
    $nomina="
    <nomina:Nomina Version=\"1.1\" $nomina_txt  xsi:schemaLocation=\"http://www.sat.gob.mx/nomina http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina11.xsd\" >
    $nomina
    </nomina:Nomina>
    
    ";
    $complementos[]=$nomina;
    

}        



if(count($datos['ImpuestosLocales'])>0)
{
    $localtotalretenciones=$localtotantranslados=0.00;
//translados
    if(count($datos['ImpuestosLocales']['TrasladosLocales'])>0)
    {
        foreach($datos['ImpuestosLocales']['TrasladosLocales'] AS $idx=>$transladolocal)
        {
            $ImpLocTrasladado= $transladolocal['ImpLocTrasladado'];
            $TasadeTraslado= $transladolocal['TasadeTraslado'];
            $Importe=$transladolocal['Importe'];

            $localtotantranslados=$localtotantranslados+$Importe;
            $impuestolocal.="<implocal:TrasladosLocales ImpLocTrasladado=\"$ImpLocTrasladado\" TasadeTraslado=\"$TasadeTraslado\" Importe=\"$Importe\" />";
        }
        
    }
//retenciones
    if(count($datos['ImpuestosLocales']['RetencionesLocales'])>0)
    {
        foreach($datos['ImpuestosLocales']['RetencionesLocales'] AS $idx=>$retencionlocal)
        {
            $ImpLocRetenido=$retencionlocal['ImpLocRetenido'];
            $TasadeRetencion=$retencionlocal['TasadeRetencion'];  //varia 2 o 3% segun el tipo de cliente
            $Importe=$retencionlocal['Importe'];
            
            $localtotalretenciones=$localtotalretenciones+$Importe;
            $impuestolocal.="<implocal:RetencionesLocales ImpLocRetenido=\"$ImpLocRetenido\" TasadeRetencion=\"$TasadeRetencion\" Importe=\"$Importe\" />";
        }
    }
    
    $impuestoslocales_final="
        <implocal:ImpuestosLocales  version=\"1.0\" TotaldeRetenciones=\"$localtotalretenciones\" TotaldeTraslados=\"$localtotantranslados\"  xmlns:implocal=\"http://www.sat.gob.mx/implocal\" xsi:schemaLocation=\"http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd\"  >    
        $impuestolocal
        </implocal:ImpuestosLocales>
    ";
    $complementos[]=$impuestoslocales_final;
}



if(count($complementos)>0)
{
    foreach($complementos AS $id=>$valorcomplemento)
    {
        $complemento_final.="
        $valorcomplemento
        ";
    }
    $complemento="
    <cfdi:Complemento>
    $complemento_final
    </cfdi:Complemento>
    
    ";
    
    
}

        
            $conceptos = '';
        
            $total_desgloce = 0.00;
            $conceptos='';
            foreach($datos['conceptos'] AS $key=>$datos_producto)
            {
                $cantidad = $datos_producto['cantidad'];
                $descripcion = $datos_producto['descripcion'];
                $valorunitario = $datos_producto['valorunitario'];
                $unidad = $datos_producto['unidad'];
                $noidentificacion = $datos_producto['ID'];
                $importe = $datos_producto['importe'];
                $predial = $datos_producto['predial'];
        
                if(strlen($predial)>4)
                {
                    $predial_txt="<cfdi:CuentaPredial numero=\"$predial\"/>";
                }
                else
                {
                    $predial_txt='';
                }
                $conceptos .= "<cfdi:Concepto cantidad=\"$cantidad\" unidad=\"$unidad\" noIdentificacion=\"$noidentificacion\" descripcion=\"$descripcion\" valorUnitario=\"$valorunitario\" importe=\"$importe\">$predial_txt</cfdi:Concepto>
                ";
        
                if ($cantidad == 0 OR $cantidad=='ND') {
                    $error_mensaje .= "; CANTIDAD DICE 0 en $noidentificacion $descripcion";
                }
                if ($descripcion == '' OR $descripcion == 'ND' ) {
                    $error_mensaje .= "; DESCRIPCION EN BLANCO  EN  $noidentificacion $descripcion";
                }
                if ($unidad == '' OR $unidad == 'ND') {
                    $error_mensaje .= "; FALTA UNIDAD DE PRODUCTO EN  $noidentificacion $descripcion";
                }
                
            }
        
            $descuento = $datos['factura']['descuento'];
            $descuento = sprintf('%1.2f', $descuento);
        
        
        
            $idempresa = $datosfactura['idempresa'];
            $regimenfiscal = $datos['factura']['RegimenFiscal'];
        
            $tipocambio = $datos['factura']['tipocambio'];
            $moneda = $datos['factura']['moneda'];
            if($moneda=='' OR $moneda=='ND')
            {
                $moneda='MXN';
            }
        
            if($tipocambio=='' OR $tipocambio=='ND')
            {
                $tipocambio='1.00';
            }
        
            $NumCtaPago = $datos['factura']['NumCtaPago'];
        
            if($NumCtaPago=='' OR $NumCtaPago=='ND')
            {
                $NumCtaPago='';
            }
        
            $LugarExpedicion = $datos['factura']['LugarExpedicion'];
        
            if($LugarExpedicion=='')
            {
                if ($expedidolocalidad != $expedidomunicipio) {
                    $LugarExpedicion = "$expedidomunicipio $expedidolocalidad, $expedidoestado ";
                } else {
                    $LugarExpedicion = "$expedidolocalidad, $expedidoestado";
                }
            }
        
        
        
            if ($emisornointerior != 'ND' AND $emisornointerior != '') {
                $noInterior_emisor = "noInterior=\"$emisornointerior\"";
            }
        
        
            if ($expedidonointerior != 'ND' AND $expedidonointerior != '') {
                $noInterior_expedido = "noInterior=\"$expedidonointerior\"";
            }
        
        
            if ($receptornointerior != 'ND' AND $receptornointerior != '') {
                $noInterior_receptor = "noInterior=\"$receptornointerior\"";
            }
        
            if (intval($NumCtaPago) > 0) {
                $NumCtaPago=sprintf('%04d',$NumCtaPago);
                $NumCtaPago_txt = "NumCtaPago=\"$NumCtaPago\" >";
            }
        
            if ($emisornoexterior != 'ND' AND $emisornoexterior != '') {
                $noExterior_emisor = "noExterior=\"$emisornoexterior\"";
            }
        
            if ($expedidonoexterior != 'ND' AND $expedidonoexterior != '') {
                $noExterior_expedido = "noExterior=\"$expedidonoexterior\"";
            }
        
            if ($receptornoexterior != 'ND' AND $receptornoexterior != '') {
                $noExterior_receptor = "noExterior=\"$receptornoexterior\"";
            }
            $receptorrfc = strtoupper($receptorrfc);
            if ($receptorrfc == 'XAXX010101000') {
                $xml_receptor = "
                  <cfdi:Receptor
                        rfc=\"$receptorrfc\"
                        nombre=\"$receptornombre\">
                  </cfdi:Receptor>
                  ";
            } else {
                $xml_receptor = "
                  <cfdi:Receptor
                        rfc=\"$receptorrfc\"
                        nombre=\"$receptornombre\">
                    <cfdi:Domicilio
                        calle=\"$receptorcalle\"
                        $noExterior_receptor
                        $noInterior_receptor
                        colonia=\"$receptorcolonia\"
                        localidad=\"$receptorlocalidad\"
                        municipio=\"$receptormunicipio\"
                        estado=\"$receptorestado\"
                        pais=\"$receptorpais\"
                        codigoPostal=\"$receptorcodigopostal\" />
                  </cfdi:Receptor>
                ";
            }


       
        
            $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                <cfdi:Comprobante xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
                                  xmlns:cfdi=\"http://www.sat.gob.mx/cfd/3\"
                                  $nominaxmlns
                                  xmlns:tfd=\"http://www.sat.gob.mx/TimbreFiscalDigital\"
                                  xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/3
                                  http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd
                                  http://www.sat.gob.mx/TimbreFiscalDigital
                                  http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital.xsd\"
                                  version=\"3.2\"
                                  serie=\"$Factura_Serie\"
                                  folio=\"$Factura_Numero\"
                                  fecha=\"$Factura_Fecha_Expedicion\"
                                  formaDePago=\"$Forma_de_Pago\"
                                  noCertificado=\"$noSertificadoSAT\"
                                  certificado=\"$certificado\"
                                  subTotal=\"$sub_total\"
                                  descuento=\"$descuento\"
                                  total=\"$total\"
                                  metodoDePago=\"$Metodo_Pago\"
                                  tipoDeComprobante=\"$tipocomprobante\"
                                  TipoCambio=\"$tipocambio\"
                                  Moneda=\"$moneda\"
                                    LugarExpedicion=\"$LugarExpedicion\"
                                  $NumCtaPago_txt  >
        
                  <cfdi:Emisor
                         rfc=\"$emisorrfc\"
                         nombre=\"$emisornombre\">
                    <cfdi:DomicilioFiscal
                                calle=\"$emisorcalle\"
                                $noExterior_emisor
                                $noInterior_emisor
                                colonia=\"$emisorcolonia\"
                                localidad=\"$emisorlocalidad\"
                                municipio=\"$emisormunicipio\"
                                estado=\"$emisorestado\"
                                pais=\"$emisorpais\"
                                codigoPostal=\"$emisorcodigopostal\" />
                    <cfdi:ExpedidoEn
                                calle=\"$expedidocalle\"
                                $noExterior_expedido 
                                $noInterior_expedido
                                colonia=\"$expedidocolonia\"
                                localidad=\"$expedidolocalidad\"
                                municipio=\"$expedidomunicipio\"
                                estado=\"$expedidoestado\"
                                pais=\"$expedidopais\"
                                codigoPostal=\"$expedidocodigopostal\" />
                    <cfdi:RegimenFiscal Regimen=\"$regimenfiscal\" />
                  </cfdi:Emisor>
                $xml_receptor
                  <cfdi:Conceptos>
        $conceptos            
                  </cfdi:Conceptos>
                  $impuestosfinal
                  $complemento
                </cfdi:Comprobante>
        ";
        
        //aki mash
            $xmlutf8 = utf8_encode($xml);


        /// AGREGAR SELLO
        // GENERAR CADENA
            @mkdir('tmp');
            @chmod('tmp',0777);
        
            //nombre temporal del archivo xml a generar la cadena
           $file_target = $ruta.'tmp/'.time() . rand() . '.xml';
        
            @unlink($file_target);
            if (file_exists($file_target)) {
                @chmod($file_target, 0777);
            } // add write permission
            if (($wh = fopen($file_target, 'wb')) === false) {
                return false;
            } // error messages.
            if (fwrite($wh, $xmlutf8) === false) {
                fclose($wh);
                return false;
            }
            fclose($wh);
            @chmod($file_target, 0777);
        

            // Load the XML source
            $xml = new DOMDocument;
            $xml->load($file_target);

            $xsl = new DOMDocument;

            $xsl->load($ruta.'xslt/cadenaoriginal_3_2.xslt');
            
            // Configure the transformer
            $proc = new XSLTProcessor;
            $proc->importStyleSheet($xsl); // attach the xsl rules

            $cadenaoriginal = $proc->transformToXML($xml);

            if(strlen($cadenaoriginal)<10)
            {

                unset($res);
                $res['pac']=0;
                $res['produccion']=$produccion;
                $res['codigo_mf_numero']=7;
                $res['codigo_mf_texto']='ERROR AL GENERAR CADENA ORIGINAL: XML MAL GENERADO O FALTA XSLTPROC';
                $res['cancelada']=1;
                $res['servidor']=0;
                return $res;
                //die('ERROR AL GENERAR CADENA ORIGINAL : REVISA QUE ESTE INSTALADO xsltproc');
            }
        //genera sello
            $pem=$datos['conf']['key'];
//echo debug_mash($datos);

             $sello = cfd_genera_sello($cadenaoriginal,$pem,$datos);
            if(strlen($sello)<30)
            {
                unset($res);
                $res['pac']=0;
                $res['produccion']=$produccion;
                $res['codigo_mf_numero']=7;
                $res['codigo_mf_texto']='ERROR AL GENERAR EL SELLO, REVISA LOS DATOS DE TU CERTIFICADO CSD';
                $res['cancelada']=1;
                $res['servidor']=0;
                return $res;
                //die("ERROR AL GENERAR EL SELLO : REVISA TUS CERTIFICADOS, QUE ESTE INSTALADO OPENSSL O CAMBIA EL METODO DE SELLAR CON php_openssl= ['NO'|'SI'] ");
            }
            @unlink($file_target); // elimina XML temporal para generar la cadena


            $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                <cfdi:Comprobante xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
                                  xmlns:cfdi=\"http://www.sat.gob.mx/cfd/3\"
                                  $nominaxmlns
                                  xmlns:tfd=\"http://www.sat.gob.mx/TimbreFiscalDigital\"
                                  xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/3
                                  http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd
                                  http://www.sat.gob.mx/TimbreFiscalDigital
                                  http://www.sat.gob.mx/sitio_internet/TimbreFiscalDigital/TimbreFiscalDigital.xsd\"
                                  version=\"3.2\"
                                  sello=\"$sello\"
                                  serie=\"$Factura_Serie\"
                                  folio=\"$Factura_Numero\"
                                  fecha=\"$Factura_Fecha_Expedicion\"
                                  formaDePago=\"$Forma_de_Pago\"
                                  noCertificado=\"$noSertificadoSAT\"
                                  certificado=\"$certificado\"
                                  subTotal=\"$sub_total\"
                                  descuento=\"$descuento\"
                                  total=\"$total\"
                                  metodoDePago=\"$Metodo_Pago\"
                                  tipoDeComprobante=\"$tipocomprobante\"
                                  TipoCambio=\"$tipocambio\"
                                  Moneda=\"$moneda\"
                                    LugarExpedicion=\"$LugarExpedicion\"
                                  $NumCtaPago_txt  >
       
                  <cfdi:Emisor
                         rfc=\"$emisorrfc\"
                         nombre=\"$emisornombre\">
                    <cfdi:DomicilioFiscal
                                calle=\"$emisorcalle\"
                                $noExterior_emisor
                                $noInterior_emisor
                                colonia=\"$emisorcolonia\"
                                localidad=\"$emisorlocalidad\"
                                municipio=\"$emisormunicipio\"
                                estado=\"$emisorestado\"
                                pais=\"$emisorpais\"
                                codigoPostal=\"$emisorcodigopostal\" />
                    <cfdi:ExpedidoEn
                                calle=\"$expedidocalle\"
                                $noExterior_expedido 
                                $noInterior_expedido
                                colonia=\"$expedidocolonia\"
                                localidad=\"$expedidolocalidad\"
                                municipio=\"$expedidomunicipio\"
                                estado=\"$expedidoestado\"
                                pais=\"$expedidopais\"
                                codigoPostal=\"$expedidocodigopostal\" />
                    <cfdi:RegimenFiscal Regimen=\"$regimenfiscal\" />
                  </cfdi:Emisor>
                $xml_receptor
                  <cfdi:Conceptos>
        $conceptos            
                  </cfdi:Conceptos>
                  $impuestosfinal
                  $complemento
                </cfdi:Comprobante>";
        
        
        
            /////////  TIMBRAR ///////////
        
        
        
        //encode si funciono con cadena default
        
        $file_target=$datos['xml_debug'];

        $tmpx=(string)$xml;
        if(strlen($tmpx)>10)
        {
            if($file_target!='')
            {
                @unlink($file_target);
                if (file_exists($file_target)) {
                    @chmod($file_target, 0777);
                } // add write permission
                if (($wh = fopen($file_target, 'wb')) === false) {
                    return "ERROR ESCRITURA EN  $file_target";
                } // error messages.
                if (fwrite($wh,utf8_encode((string)$xml)) === false) {
                    fclose($wh);
                    return "ERROR ESCRITURA EN  $file_target";
                }
                fclose($wh);
                @chmod($file_target, 0777);
                
            }
            
        }
            
    }//fin solo timbrar

    $cfdi = (string)$xml;//$xmlutf8
    if(!class_exists('nusoap_client'))
    {
        if(function_exists('libreria_mash'))
        {
            libreria_mash('nusoap');
        }
        else
        {

            include "../libraries/Cfdi/nusoap/nusoap.php";
        }
    }


    $produccion=$datos['PAC']['produccion'];


    $pac=rand(1,10);

    if($datos['PAC']['produccion']=='NO')
    {
    # Username and Password de PRUEBA, assigned by FINKOK
    $username = 'jesus.palma@hotmail.com';
    $password = 'Internet15*';
     $url = "http://demo-facturacion.finkok.com/servicios/soap/stamp.wsdl";
    /*$username = 'jesus.palma15@gmail.com';
    $password = 'Internet15*';*/
    }
    else
    {
    # Username and Password de PRODUCCION, assigned by FINKOK
    $username = 'jesus.palma15@gmail.com';
    $password = 'Internet15-';
     $url = "https://facturacion.finkok.com/servicios/soap/stamp.wsdl";
    }
     
    $invoice_path = $datos['xml_debug'];
    $xml_file = fopen($invoice_path, "rb");
    $xml_content = fread($xml_file, filesize($invoice_path));
    fclose($xml_file);

    # Consuming the stamp service
   
    $client = new SoapClient($url);

    $params = array(
      "xml" => $xml_content,
      "username" => $username,
      "password" => $password
    );
    $soap_timbrado = $client->__soapCall("stamp", array($params));

    //Verificamos que el llamado se pudo hacer correctamente
    unset($valor);
    $valor=$soap_timbrado;
    
     $ret = new stdClass();
     
      $ret->status="Timbrado";
      
      //print_r($ret);
    if ($soap_timbrado == false) 
    {
// ERROR TIMEOUT
        $status = "12345 - TimeOut Falla de Conexion SERVIDOR PAC$pac.MULTIFACTURAS.COM";
        list($codigo_numero, $codigo_txt) = explode('-', $status);
        $codigo_numero = intval(trim($codigo_numero));



    }
    else 
    {   //print_r($soap_timbrado);
        $Object = get_object_vars($soap_timbrado);
        $Object12 = get_object_vars($Object['stampResult']);
        $xml=$Object12['xml'];
        $UUID=$Object12['UUID'];
        $Fecha=$Object12['Fecha'];
        $CodEstatus=$Object12['CodEstatus'];
        $SatSeal=$Object12['SatSeal'];
        $NoCertificadoSAT=$Object12['NoCertificadoSAT'];
        $codigo_numero=0;
        $codigo=$NoCertificadoSAT;
        $timbre= $xml;

        if($CodEstatus!='Comprobante timbrado satisfactoriamente')
        {
            $ObjectInsidencias=get_object_vars($Object12['Incidencias']);
            $ObjectInsidencia=get_object_vars($ObjectInsidencias['Incidencia']);
            if($ObjectInsidencia['MensajeIncidencia']=="XML mal formado")
            {
                $ret->status="Verificar Datos de Factura y/o Cliente";
                return $ret;
                //return "Verificar Datos de Factura y/o Cliente";
            }
            else
            {
                $ret->status= $ObjectInsidencia['MensajeIncidencia'];
                return $ret;
            //return $ObjectInsidencia['MensajeIncidencia'];
            }
        }

            $ruta_xml = $datos['cfdi'];
            if($ruta_xml=='')
            {
                $ruta_xml=time().'.xml';
            }
//            $ruta_xml=strtolower($ruta_xml);
            
//almacena xml
            $timbreutf8 = utf8_encode($timbre);
            $file_target = $ruta_xml;
            if(strlen($timbreutf8)>10)
            {
                @unlink($file_target);
                if (file_exists($file_target)) {
                    @chmod($file_target, 0777);
                } // add write permission
                if (($wh = fopen($file_target, 'wb')) === false) {
                    return "ERROR ESCRITURA EN  $ruta_xml";
                } // error messages.
                if (fwrite($wh, $timbreutf8) === false) {
                    fclose($wh);
                    return "ERROR ESCRITURA EN  $ruta_xml";
                }
                fclose($wh);
                @chmod($file_target, 0777);
                
            }


//almacena PNG
            if(strlen($codigo)>10)
            {
                $codigoQR='?re='.$emisorrfc.'&&rr='.$receptorrfc.'&&tt='.$total.'&&id='.$UUID;
                // outputs image directly into browser, as PNG stream 
                 QRcode::png($codigoQR,'../accounts_data/facturas/qr/'.$emisorrfcmin.'-'.$Factura_Numero.'.png', QR_ECLEVEL_L, 3);

            }
    
    }

    //UUID
    if(filesize($datos['cfdi'])>100)
    {
        $xml = simplexml_load_file($datos['cfdi']);
        $ns = $xml->getNamespaces(true);
        $xml->registerXPathNamespace('c', $ns['cfdi']);
        $xml->registerXPathNamespace('t', $ns['tfd']);
        foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd)
        {
            $uuid=$tfd['UUID'];
        }

    }

    unset($xml_sin_timbrar);
    
    $ret->status="Timbrado";
    $ret->cadena=$cadenaoriginal;
   
    return $ret;
    //return "Timbrado";

}

/////////////////////////////////////////////////////////////////////////////////
function cfd_formato_rfc($rfc)
{
    $rfc = strtoupper($rfc);
    $rfc = str_replace(' ', '', $rfc);
    $rfc = str_replace(' ', '', $rfc);
    $rfc = str_replace(' ', '', $rfc);
    $rfc = str_replace('-', '', $rfc);
    $rfc = str_replace('/', '', $rfc);
    $rfc = str_replace('.', '', $rfc);
    $rfc=cfd_fix_dato_xml($rfc);
    return $rfc;

}
/////////////////////////////////////////////////////////////////////////////////
function cfdi_cancelar($SaleOrderId,$datos)
{
    if(!class_exists('nusoap_client'))
    {
        if(function_exists('libreria_mash'))
        {
            libreria_mash('nusoap');
        }
        else
        {
            //include $ruta."lib/nusoap/nusoap.php";
             include "../libraries/Cfdi/nusoap/nusoap.php";
        }
    }
    
    //$soapclient = new nusoap_client('http://demo-facturacion.finkok.com/servicios/soap/cancel.wsdl');

    $xml = simplexml_load_file($datos['cfdi']);
    $ns = $xml->getNamespaces(true);
    $xml->registerXPathNamespace('c', $ns['cfdi']);
    $xml->registerXPathNamespace('t', $ns['tfd']);
    foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd)
    {
        $uuid=$tfd['UUID'];
    }
    $cer= $datos['conf']['cer'];
    $key= $datos['conf']['key'];
    $uuid=(string)$uuid->__toString();

    $pass_key=$datos['conf']['pass'];
//  $cer_txt=cfd_certificado_pub($cer,'SI');
//    $key_txt = key_certificado_txt($key,'SI');
    //$cer_txt=base64_encode(file_get_contents($datos['conf']['cer']));
    //$key_txt=base64_encode(file_get_contents($datos['conf']['key'])); 

    
    # Consuming the cancel service
    # Read the x509 certificate file on PEM format and encode it on base64
    $cer_path = $datos['conf']['cer']; //.key
    $cer_file = fopen($cer_path, "r");
    $cer_txt = fread($cer_file, filesize($cer_path));
    fclose($cer_file);
    # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
    #$cer_content = base64_encode($cer_content);

    # Read the Encrypted Private Key (des3) file on PEM format and encode it on base64
    $key_path = $datos['conf']['key'];
    //$key_path = "key.pem";
    $key_file = fopen($key_path, "r");
    $key_txt = fread($key_file,filesize($key_path));
    fclose($key_file);
    //$key_txt = base64_encode($key_txt);
    
    //$key_txt=base64_encode(file_get_contents($datos['conf']['key']));
    /*
    # Read the Encrypted Private Key (des3) file on PEM format and encode it on base64
    $key_path = "key.pem";
    $key_file = fopen($key_path, "r");
    $key_content = fread($key_file,filesize($key_path));
    fclose($key_file);
    # In newer PHP versions the SoapLib class automatically converts FILE parameters to base64, so the next line is not needed, otherwise uncomment it
    #$key_content = base64_encode($key_content);
    */
    //creamos arreglo con parametos de Solicitud de Cancelaci�n
   // $sol = array('UUIDS' => $cer_txt, 'llavePrivada' => $key_txt, 'password' =>$pass_key, 'uuid' => $uuid);


    //Parametros para cancelar
     if($datos['PAC']['produccion']=='NO')
    {
    # Username and Password de PRUEBA, assigned by FINKOK
    $usuario = 'jesus.palma@hotmail.com';
    $clave = 'Internet15*';
    $url = "http://demo-facturacion.finkok.com/servicios/soap/cancel.wsdl";
    }
    else
    {
    # Username and Password de PRODUCCION, assigned by FINKOK
    $usuario = 'jesus.palma15@gmail.com';
    $clave = 'Internet15-';
    $url = "https://facturacion.finkok.com/servicios/soap/cancel.wsdl";

    }
    
    $invoices = array($uuid); # A list of UUIDs

    //$url = "http://demo-facturacion.finkok.com/servicios/soap/cancel.wsdl";
    //$client = new SoapClient($url);

    $params = array(  
      "UUIDS" => array('uuids' => $invoices),
      "username" => $usuario,
      "password" => $clave,
      "taxpayer_id" => $datos['rfcEmisor'],
      "cer" => $cer_txt,
      "key" => $key_txt      
    );
    $client = new SoapClient($url,array('trace' => 1));
   
    $response = $client->__soapCall("cancel", array($params));
    //echo "REQUEST:\n" . $client->__getLastRequest() . "\n";
    
    if ($response == false)
    {
    return "ERROR DE COMUNICACION";
    }
    else
    {
        if (isset($response->cancelResult->Acuse))
        {
        file_put_contents("../accounts_data/facturas/canceladas/".$datos['rfcEmisor']."-".$SaleOrderId.".xml",$response->cancelResult->Acuse);
        }
    }
    
    return $response;
   
        
}

/*
/////////////////////////////////////////////////////////////////////////////////
function cfdi_cancelar($datos)
{
    if(!class_exists('nusoap_client'))
    {
        if(function_exists('libreria_mash'))
        {
            libreria_mash('nusoap');
        }
        else
        {
            include $ruta."lib/nusoap/nusoap.php";
        }
    }

    $produccion=$datos['PAC']['produccion'];
    if($produccion=='SI')
    {
        $soapclient = new nusoap_client('https://www.sefactura.com.mx/sefacturapac/TimbradoService?wsdl',$esWSDL=true);
    }
    else
    {
        $soapclient = new nusoap_client('http://www.jonima.com.mx:3014/sefacturapac/TimbradoService?wsdl',$esWSDL = true);
    }

    $xml = simplexml_load_file($datos['cfdi']);
    $ns = $xml->getNamespaces(true);
    $xml->registerXPathNamespace('c', $ns['cfdi']);
    $xml->registerXPathNamespace('t', $ns['tfd']);
    foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd)
    {
        $uuid=$tfd['UUID'];
    }
    $cer= $datos['conf']['cer'];
    $key= $datos['conf']['key'];
    $uuid=(string)$uuid->__toString();

    $pass_key=$datos['conf']['pass'];
//	$cer_txt=cfd_certificado_pub($cer,'SI');
//    $key_txt = key_certificado_txt($key,'SI');
    $cer_txt=base64_encode(file_get_contents($datos['conf']['cer']));
    $key_txt=base64_encode(file_get_contents($datos['conf']['key']));


    //creamos arreglo con parametos de Solicitud de Cancelaci�n
    $sol = array('certificado' => $cer_txt, 'llavePrivada' => $key_txt, 'password' =>
        $pass_key, 'uuid' => $uuid);


    //Parametros para cancelar
    $usuario = $datos['PAC']['usuario'];
    $clave = $datos['PAC']['pass'];

    //Creamos arreglo con parametros para llamar el servicio de cancelaci�n
    $can = array('solicitud' => $sol, 'usuario' => $usuario, 'clave' => $clave);

//print_r($can);
    //Llamamos al servicio de Cancelacion asignandole los parametros correspondientes
    $soap_cancelacion = $soapclient->call('cancelacion', $can);

    if ($soap_cancelacion == false)
    {
        RETURN "ERROR DE COMUNICACION";
    }

    return $soap_cancelacion;


}
*/
/////////////////////////////////////////////////////////////////////////////////
// ELIMINA CARACTERES HTML Y JAVA SCRIPT
function cfd_fix_dato_xml_html_txt($dato)
{
    $dato=_html_txt($dato);
    return $dato;
}

/////////////////////////////////////////////////////////////////////////////////
// ELIMINA CARACTERES INVALIDOS Y DA FORMATO A UN DATO SEGUN EL ANEXO 20
function cfd_fix_dato_xml_acentos($dato)
{
    $dato=acentos_remueve_multifacturas($dato);
    return cfd_fix_dato_xml($dato);
    
}
/////////////////////////////////////////////////////////////////////////////////

function cfd_fix_dato_xml($dato)
{
    $dato=trim($dato);
    $dato = str_replace("\r", ' ', $dato);
    $dato = str_replace("\n", ' ', $dato);
    $dato = str_replace("\t", ' ', $dato);
    $dato = str_replace('|', '', $dato);
    $dato = str_replace('  ', ' ', $dato);

    $dato = str_replace('&', '&amp;', $dato);
    $dato = str_replace('"', '&quot;', $dato);
    $dato = str_replace('<', '&lt;', $dato);
    $dato = str_replace('>', '&gt;', $dato);
    $dato = str_replace("'", '&apos;', $dato);
    if($dato=='')
    {
        $dato='ND';
    }
    return $dato;
}




/////////////////////////////////////////////////////////////////////////////////
function cfd_genera_sello($cadena,$key_pem,$datos)
{
    @mkdir('tmp');
    @chmod('tmp',0777);
    if(function_exists('openssl_sign')==false OR $datos['php_openssl']=='NO')
    {
        $linea_comandos=true;
    }
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' OR $linea_comandos==true)
    {
//WINDOWS
        //ALMACENA CADENA
            $ruta=$datos['SDK']['ruta'];
            $hash=md5($cadena).rand(1111,9999);
            @mkdir('tmp');
            @chmod('tmp',0777);
            $ruta_cadena= $ruta."tmp/cadena.txt";
            $file_target = $ruta_cadena;
            
            if (file_exists($file_target)) {
                @unlink($file_target);
                //chmod($file_target, 0777);
            } // add write permission
            if (($wh = fopen($file_target, 'wb')) === false) {
                return "ERROR ESCRITURA EN  $ruta_xml";
            } // error messages.
            if (fwrite($wh, $cadena) === false) {
                fclose($wh);
                return "ERROR ESCRITURA EN  $ruta_xml";
            }
            fclose($wh);
            //@chmod($file_target, 0777);
            @mkdir('tmp');
            @chmod('tmp',0777);
            $ruta_bat= $ruta."tmp/sello.bat";
            $ruta_bat_txt= $ruta."tmp/sello.txt";
            $rura_key_pem=$datos['conf']['key'];

            echo $comando_bat="openssl dgst -sha1 -sign \"$rura_key_pem\" \"$ruta_cadena\" | openssl enc -base64 -out \"$ruta_bat_txt\" ";
//            $comando_bat2="openssl dgst -sha1 -sign \"$rura_key_pem\" \"$ruta_cadena\" | openssl enc -base64 -out \"$ruta_bat_txt\" ";
//echo $comando_bat;

            $ruta_bat_txt=str_replace('\\','/',$ruta_bat_txt);
            $ruta_bat_txt=str_replace('///','/',$ruta_bat_txt);

            $res = shell_exec($comando_bat);
            $sello= file_get_contents($ruta_bat_txt);
            
            $sello=str_replace(' ','',$sello);
            $sello=str_replace("\n",'',$sello);
            $sello=str_replace("\r",'',$sello);
            $sello=str_replace("\t",'',$sello);
            @chmod($ruta_bat_txt,0777);
            @chmod($ruta_cadena,0777);
            @unlink($ruta_bat_txt);
            @unlink($ruta_cadena);
            

    }
    else
    {
//LINUX        
    	$pkeyid = openssl_get_privatekey(file_get_contents($key_pem));
    	openssl_sign($cadena, $cadena_generada, $pkeyid, OPENSSL_ALGO_SHA1);
    	openssl_free_key($pkeyid);
    	$sello = base64_encode($cadena_generada);
    }
 
	return (string) $sello;
}

/////////////////////////////////////////////////////////////////////////////////

function certificado_pem($datos)
{

//echo debug_mash($datos);


	$cer=$datos['conf']['cer'];
	$key=$datos['conf']['key'];
	$pass=$datos['conf']['pass'];


    $cer=str_replace('\\','/',$cer);
    $key=str_replace('\\','/',$key);

//echo $cer;
@unlink("$cer.pem");
@unlink("$key.pem");

    $cer=str_replace('.pem','',$cer);
    $key=str_replace('.pem','',$key);

	//genera PEM privado
	$comando="openssl pkcs8 -inform DER -in $key -out $key.pem -passin pass:$pass";

	$resultado=shell_exec($comando);
	//genera PEM publico
	$comando="openssl x509 -inform DER -outform PEM -in $cer -pubkey >$cer.pem";
	$resultado=shell_exec($comando);

	//LEE Certificado
	//datos generales
	$comando="openssl x509 -in $cer.pem  -issuer -noout";
	$resultado1=shell_exec($comando);
    $resultado1="$resultado1";
	//fecha valides
	$comando="openssl x509 -in $cer.pem  -startdate -enddate -noout";
	$resultado2=shell_exec($comando);
	// serie matriz
	$comando="openssl x509 -in $cer.pem  -subject -noout";
	$resultado3=shell_exec($comando);
	
	//serie
	$comando="openssl x509 -in $cer.pem  -serial -noout";
	$resultado4=shell_exec($comando);

	$resultado="$resultado1
$resultado2
$resultado3
$resultado4";


    if(filesize("$key.pem")<10)
    {
        unlink("$key.pem");
    }
    if(filesize("$cer.pem")<10)
    {
        unlink("$cer.pem");
    }
	
	$resultado=str_replace('\x','%',$resultado);
	$resultado=rawurldecode($resultado);

	list($fecha_inicial_tmp,$fecha_final_tmp)=explode("\n",$resultado2);
	list($tmp,$fecha_inicial_txt)=explode('=',$fecha_inicial_tmp);
	list($tmp,$fecha_final_txt)=explode('=',$fecha_final_tmp);
	$fecha_inicial_time=hora_txt_time($fecha_inicial_txt);
	$fecha_final_time=hora_txt_time($fecha_final_txt);
	list($tmp,$tmp,$rfc)=explode('/',$resultado3);
	$rfc=str_replace(' ','',$rfc);
	list($tmp,$serial)=explode('=',$resultado4);
	$serial=str_replace(' ','',$serial);
    

    $cnt=strlen($serial);
    $s=1;
    $no_serie;
    for($i=0;$i<$cnt;$i++)
    {
        if($s==0)
        {
            $no_serie="$no_serie".$serial[$i];
            $s=1;
        }
        else
        {
            $s=0;
        }
    }
    $serial=$no_serie;
    
    

	$cer_txt=cfd_certificado_pub("$cer.pem");
  
  
    unset($res);
    $res['certificado_pem_txt']=$cer_txt;
    $res['certificado_pem']="$cer.pem";
    $res['key_pem']="$key.pem";
    $res['certificado_info']=$resultado;
    $res['fecha_valido_inicia']=$fecha_inicial_time;
    $res['fecha_valido_fin']=$fecha_final_time;
    $res['certificado_no_serie']=$serial;
   
    return $res;
  

}

////////////////////////////////////////////////////////////////////////////////
function hora_txt_time($hora_txt)
{
//$hora="Aug 21 15:22:08 2008";
//$hora="Aug 21 15:22:08 2010 GMT";

        $segundos=strtotime($hora_txt);

        return $segundos;
}
/////////////////////////////////////////////////////////////////////////////////

function key_certificado_txt($archivo_key,$cancelacion='NO')
{
	$file=$archivo_key;
	$datos = file($file);

	$certificado = ''; $carga=false;

	for ($i=0; $i<sizeof($datos); $i++) 
    {
	    if (strstr($datos[$i],"PRIVATE"))
        {
           $carga=false;
	    }
        if ($carga) 
        {
            $certificado.=trim($datos[$i]);
            if($certificado!='NO')
            {
                $certificado.="\n";
            }
        }
        $carga=true;
	}
	$certificado=str_replace(' ','',$certificado);
	return $certificado;
}
////////////////////////////////////////////////////////////////////////////////
function cfd_certificado_pub($cer,$cancelacion='NO')
{
	$file="$cer";
	$datos = file($file);
	$certificado = ""; $carga=false;
	for ($i=0; $i<sizeof($datos); $i++) {
	    if (strstr($datos[$i],"END CERTIFICATE")) $carga=false;
	    if ($carga) 
        {
            $certificado .= trim($datos[$i]);
            if($cancelacion!='NO')
            {
                $certificado.="\n";
            }
        }
	    if (strstr($datos[$i],"BEGIN CERTIFICATE")) $carga=true;
	}
	$certificado=str_replace(' ','',$certificado);
	return $certificado;
}


////////////////////////////////////////////////////////////////////////////////
function acentos_remueve_multifacturas($str)
{
$orig=$str;
    $strmd5=md5($str);
    //CACHE        
    $cache_nombre="acentos_remueve_mash:$strmd5";
    
    static $acentos_remueve_mash_cache_static;
    if(isset($acentos_remueve_mash_cache_static[$cache_nombre]))
    {
        return $acentos_remueve_mash_cache_static[$cache_nombre];
    }
    if(function_exists('apc_fetch'))
    {
        $resultado = apc_fetch($cache_nombre);
        $acentos_remueve_mash_cache_static[$cache_nombre]=$resultado;
    }
    
    if ($resultado!='') 
    {
    
    }
    else 
    { 

        $text=str_replace('&aacute;','�',$str);
        $text=str_replace('&eacute;','�',$text);
        $text=str_replace('&iacute;','�',$text);
        $text=str_replace('&oacute;','�',$text);
        $text=str_replace('&uacute;','�',$text);
        
        $text=str_replace('&Aacute;','�',$text);
        $text=str_replace('&Eacute;','�',$text);
        $text=str_replace('&Iacute;','�',$text);
        $text=str_replace('&Oacute;','�',$text);
        $text=str_replace('&Uacute;','�',$text);
        
        
        $text=str_replace('&aacute;','�',$text);
        $text=str_replace('&eacute;','�',$text);
        $text=str_replace('&iacute;','�',$text);
        $text=str_replace('&oacute;','�',$text);
        $text=str_replace('&uacute;','�',$text);
        
        $text=str_replace('&auml;','�',$text);
        $text=str_replace('&euml;','�',$text);
        $text=str_replace('&iuml;','�',$text);
        $text=str_replace('&ouml;','�',$text);
        $text=str_replace('&uuml;','�',$text);
        
        $a = array('�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',  '�', '�', '�', '�', '�', '�',  '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I',  'N', 'O', 'O', 'O', 'O', 'O',  'U', 'U', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u');
        $text=str_replace($a, $b, $text);

        
        $text=str_replace('&ntilde;','�',$text);
        $resultado=str_replace('&Ntilde;','�',$text);
        
        $acentos_remueve_mash_cache_static[$cache_nombre]=$resultado;
        if(function_exists('apc_add'))
        {
            $tiempo_cache=3600;
            apc_add($cache_nombre, $resultado, $tiempo_cache*48);//24hrs de cache
        }
    }


    return $resultado;
} 

////////////////////////////////////////////////////////////////////////////////

function _html_txt($html){
    
    
    if($html=='')
        return $html;
        
    $htmlmd5=md5($html);
    //CACHE        
    $cache_nombre="html_txt:$htmlmd5";
    $cache_nombre_global="html_txt_global";
    
    static $html_txt_cache_static;
    
    if(isset($html_txt_cache_static[$cache_nombre]))
    {
        return $html_txt_cache_static[$cache_nombre];
    }
    if(function_exists('apc_fetch'))
    {
        $text = apc_fetch($cache_nombre);
        $html_txt_cache_static[$cache_nombre]=$text;
    }

    if ($text!='') 
    {
    
    } 
    else 
    { 

        $html=strip_tags($html);
        $buscar = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
                       '@<[\\/\\!]*?[^<>]*?>@si',            // Strip out HTML tags
                       '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
                       '@<![\\s\\S]*?--[ \\t\\n\\r]*>@'          // Strip multi-line comments including CDATA
        );
        $text = preg_replace($buscar, '', $html);

        $a = array('&nbsp;', '   ', '  ', '  ', '\n\n', '\r\r', '&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;',  '&Uacute;', '&auml;', '&euml;', '&iuml;', '&ouml;', '&uuml;',  '&ntilde;', '&Ntilde;');
        $b = array(' ', ' ', ' ', ' ', '\n',              '\r', '�', '�', '�', '�', '�', 'A', 'E', 'I', 'O',                                                                    'U',        'A', 'E', 'I', 'O',             'U',        '�', '�');
        $text=str_replace($a, $b, $text);

        if(function_exists('apc_add'))
        {
            if($html!='')
            {
                $tiempo_cache=3600;
                $html_txt_cache_static[$cache_nombre]=$text;
                apc_add($cache_nombre, $text, $tiempo_cache*1);//1hr
            }            
        }
    }

    

return $text;
}

////////////////////////////////////////////////////////////////////////////////
function ini_to_array_cfdi($ini_string)
{
    $renglones=explode("\n",$ini_string);
    $posicion='';
    foreach($renglones AS $renglon)
    {
        //detecta posicion arreglo
        list($x1,$x2)=explode('[',$renglon);
        if($x2!='')
        {
            //nueva posicion
            $posicion=str_replace(']','',$x2);
    
        }
        else
        {
                list($nombre,$valor)=explode('=',$renglon);
                list($tmp1,$tmp2,$tmp3)=explode('.',$posicion);
                $tmp1= str_replace("\n",'',$tmp1);
                $tmp2= str_replace("\n",'',$tmp2);
                $tmp3= str_replace("\n",'',$tmp3);
                $tmp1= str_replace("\r",'',$tmp1);
                $tmp2= str_replace("\r",'',$tmp2);
                $tmp3= str_replace("\r",'',$tmp3);
                $tmp1= str_replace("\t",'',$tmp1);
                $tmp2= str_replace("\t",'',$tmp2);
                $tmp3= str_replace("\t",'',$tmp3);

                $valor= str_replace("\n",'',$valor);
                $valor= str_replace("\r",'',$valor);
                
                
                
                if($tmp1!='' AND $tmp2!=''  AND $tmp3!='' )
                {
                    if($nombre!='')
                        $arreglo[$tmp1][$tmp2][$tmp3][$nombre]=$valor;
                }
                elseif($tmp1!='' AND $tmp2!='' )
                {
                    if($nombre!='')
                        $arreglo[$tmp1][$tmp2][$nombre]=$valor;
                }
                elseif($tmp1!='' )
                {
                    if($nombre!='')
                        $arreglo[$tmp1][$nombre]=$valor;
                }
                elseif($tmp1=='' )
                {
                    if($nombre!='')
                        $arreglo[$nombre]=$valor;
                }        
        }
    }
    return $arreglo;
}

////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////
function arr2ini(array $a, array $parent = array())
{
    $out = '';
    foreach ($a as $k => $v)
    {
        if (is_array($v))
        {
            //subsection case
            //merge all the sections into one array...
            $sec = array_merge((array) $parent, (array) $k);
            //add section information to the output
            $out .= '[' . join('.', $sec) . ']' . PHP_EOL;
            //recursively traverse deeper
            $out .= arr2ini($v, $sec);
        }
        else
        {
            //plain key->value case
            $out .= "$k=$v" . PHP_EOL;
        }
    }
    return $out;
}



////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////


?>