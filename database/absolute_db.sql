-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-11-2017 a las 18:37:08
-- Versión del servidor: 5.5.53-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `absolute_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` varchar(64) NOT NULL,
  `domain` varchar(128) NOT NULL,
  `company_name` varchar(256) NOT NULL,
  `trade_name` varchar(128) NOT NULL,
  `trade_regimen` varchar(80) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `country` varchar(64) NOT NULL,
  `address` varchar(256) DEFAULT NULL,
  `street` varchar(80) DEFAULT NULL,
  `numExt` varchar(10) DEFAULT NULL,
  `numInt` varchar(10) DEFAULT NULL,
  `colony` varchar(80) DEFAULT NULL,
  `localidad` varchar(80) DEFAULT NULL,
  `cp` varchar(8) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `state` varchar(64) NOT NULL,
  `business_phone` varchar(32) NOT NULL,
  `mobile` varchar(32) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `cert_file` varchar(128) DEFAULT NULL,
  `cert_key` varchar(128) DEFAULT NULL,
  `cert_password` varchar(512) DEFAULT NULL,
  `invoice_folios` int(11) DEFAULT NULL,
  `invoice_folios_expiration_date` date DEFAULT NULL,
  `web_url` varchar(512) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `accounts`
--

INSERT INTO `accounts` (`id`, `domain`, `company_name`, `trade_name`, `trade_regimen`, `email`, `country`, `address`, `street`, `numExt`, `numInt`, `colony`, `localidad`, `cp`, `city`, `state`, `business_phone`, `mobile`, `rfc`, `cert_file`, `cert_key`, `cert_password`, `invoice_folios`, `invoice_folios_expiration_date`, `web_url`, `created`, `updated`, `active`) VALUES
('7d38f8fc-21fe-4b92-a841-b7154ed18721', 'shelly-ialtamirano.c9users.io', 'Shelly', 'SISTEMAS COMPUTACIONALES', 'RÉGIMEN DE INCORPORACIÓN FISCAL', 'ivan.altamirano@gmail.com', 'Mexico', 'Tijuana Baja california', 'Calle test', '10', '20', 'Colinas del mar', 'Guaymas', '85440', 'Guaymas', 'Sonora', '6222286216', '6222286216', 'AAD990814BP7', 'xyz', 'yzx', '123456', 650, NULL, NULL, NULL, '2017-01-29 18:43:02', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `account_configurations`
--

CREATE TABLE IF NOT EXISTS `account_configurations` (
  `account_id` varchar(64) NOT NULL,
  `account_config_code` varchar(64) NOT NULL DEFAULT '',
  `account_config_value` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`account_id`,`account_config_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `account_configurations`
--

INSERT INTO `account_configurations` (`account_id`, `account_config_code`, `account_config_value`) VALUES
('7d38f8fc-21fe-4b92-a841-b7154ed18721', 'base_path', 'demo_files/'),
('7d38f8fc-21fe-4b92-a841-b7154ed18721', 'database_host', '127.0.0.1'),
('7d38f8fc-21fe-4b92-a841-b7154ed18721', 'database_name', 'merofa_sync'),
('7d38f8fc-21fe-4b92-a841-b7154ed18721', 'database_password', ''),
('7d38f8fc-21fe-4b92-a841-b7154ed18721', 'database_user', 'ialtamirano'),
('7d38f8fc-21fe-4b92-a841-b7154ed18721', 'private_folder', 'private/'),
('7d38f8fc-21fe-4b92-a841-b7154ed18721', 'public_folder', 'public/'),
('ec507213-5874-4e64-b76d-a583e5d69441', 'base_path', 'eileen_files/'),
('ec507213-5874-4e64-b76d-a583e5d69441', 'database_host', '127.0.0.1'),
('ec507213-5874-4e64-b76d-a583e5d69441', 'database_name', 'eileen_sales_db'),
('ec507213-5874-4e64-b76d-a583e5d69441', 'database_password', 'fTB5D9Dq72AMAYqt'),
('ec507213-5874-4e64-b76d-a583e5d69441', 'database_user', 'sales_user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `account_customization`
--

CREATE TABLE IF NOT EXISTS `account_customization` (
  `account_id` varchar(64) NOT NULL,
  `menu_color` varchar(12) DEFAULT NULL,
  `logo_path` varchar(64) DEFAULT NULL,
  `logo_small_path` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `account_customization`
--

INSERT INTO `account_customization` (`account_id`, `menu_color`, `logo_path`, `logo_small_path`) VALUES
('7d38f8fc-21fe-4b92-a841-b7154ed18721', NULL, 'logo.jpg', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `account_profiles`
--

CREATE TABLE IF NOT EXISTS `account_profiles` (
  `id` varchar(64) NOT NULL,
  `account_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `profile_type_id` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profile_types`
--

CREATE TABLE IF NOT EXISTS `profile_types` (
  `id` varchar(16) NOT NULL,
  `profile_type_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `profile_types`
--

INSERT INTO `profile_types` (`id`, `profile_type_name`) VALUES
('Owner', 'Application Owner'),
('User', 'User Profile');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
