`item_id` int(11) DEFAULT NULL


CREATE TABLE IF NOT EXISTS `erp_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(64) DEFAULT NULL,
  `product_name` varchar(128) NOT NULL,
  `product_description` varchar(512) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `product_cost_price` decimal(10,2) DEFAULT NULL,
  `product_price` decimal(10,2) DEFAULT NULL,
  `product_stock` decimal(10,2) DEFAULT NULL,
  `product_um` varchar(16) DEFAULT NULL,
  `product_has_variants` int(1) DEFAULT NULL,
  `product_variant_product_id` int(11) DEFAULT NULL,
  `product_last_image_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

los articulos existentes deben der ser actualizados a item_id = 1 lo cual indica que es un producto.... el item_id= 2 sera para almacenar materiales.