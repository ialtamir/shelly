-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-01-2015 a las 05:32:14
-- Versión del servidor: 5.5.40-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `demo_sales_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `erp_transaction_permissions`
--

CREATE TABLE IF NOT EXISTS `erp_transaction_permissions` (
  `transaction_id` int(11) NOT NULL,
  `transaction_type` varchar(32) NOT NULL,
  `permission_type` varchar(16) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`,`transaction_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

ALTER TABLE  `erp_quotes` ADD  `has_especial_permissions` TINYINT( 1 ) NULL ;
ALTER TABLE  `erp_purchase_orders` ADD  `has_especial_permissions` TINYINT( 1 ) NULL ;
ALTER TABLE  `erp_events` ADD  `has_especial_permissions` TINYINT( 1 ) NULL ;
ALTER TABLE  `erp_work_orders` ADD  `has_especial_permissions` TINYINT( 1 ) NULL ;
ALTER TABLE  `erp_sale_orders` ADD  `has_especial_permissions` TINYINT( 1 ) NULL ;
ALTER TABLE  `erp_payments` ADD  `has_especial_permissions` TINYINT( 1 ) NULL ;