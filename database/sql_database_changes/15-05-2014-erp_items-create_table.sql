CREATE TABLE IF NOT EXISTS `erp_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `erp_items`
--

INSERT INTO `erp_items` (`item_id`, `item_name`) VALUES
(1, 'Productos'),
(2, 'Materiales');