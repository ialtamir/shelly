<?php

class DB {

	public $conn;
	private $statement;

	public function __construct()
	{
        //$this->conn = getConnection();
	}
    
    public static function withAccount($account_configuration){
        
        $instance = new self();
    	$instance->setConnection($account_configuration);
    	return $instance;
    	
    }
    
    
    public function setConnection($configuration_data){
        
		$dbh = new PDO("mysql:host=".$configuration_data->database_host.";dbname=".$configuration_data->database_name.";", $configuration_data->database_user, $configuration_data->database_password);
	    $dbh->exec("SET CHARACTER SET utf8");
	    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
	    $this->conn =  $dbh;
	}
        

	public function query($sql = '', $params = array())
	{
		//echo $sql;
		$statement = $this->conn->prepare($sql);
		$statement->setFetchMode(PDO::FETCH_OBJ);
		$statement->execute($params);
		return $statement;
	}

	public function getInsertId()
	{
		return (int) $this->conn->lastInsertId();
	}

}
?>