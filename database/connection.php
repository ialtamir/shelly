<?php



function getConnection() {

    $dbh = new PDO("mysql:host=".DB_HOST.";dbname=".DB_DATABASE."", DB_USER, DB_PWD);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

function getConnectionByAccount($account_configuration){
    
   
    $dbh = new PDO("mysql:host=".$account_configuration->database_host.";dbname=".$account_configuration->database_name."", $account_configuration->database_user, $account_configuration->database_password);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
   
}



?>