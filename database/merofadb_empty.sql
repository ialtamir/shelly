-- phpMyAdmin SQL Dump
-- version 3.5.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 02, 2014 at 12:02 PM
-- Server version: 5.5.21
-- PHP Version: 5.4.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `merofadb_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `erp_currencies`
--

CREATE TABLE IF NOT EXISTS `erp_currencies` (
  `currency_id` int(11) NOT NULL,
  `currency_name` varchar(32) NOT NULL,
  `currency_symbol` varchar(8) DEFAULT NULL,
  `currency_default` int(1) DEFAULT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `erp_currencies`
--

INSERT INTO `erp_currencies` (`currency_id`, `currency_name`, `currency_symbol`, `currency_default`) VALUES
(1, 'Pesos', 'MXN', 0),
(2, 'Dolares', 'USD', 1);

-- --------------------------------------------------------

--
-- Table structure for table `erp_customers`
--

CREATE TABLE IF NOT EXISTS `erp_customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_code` varchar(16) DEFAULT NULL,
  `customer_name` varchar(64) NOT NULL,
  `customer_rfc` varchar(16) DEFAULT NULL,
  `customer_address` varchar(128) DEFAULT NULL,
  `customer_cp` varchar(8) DEFAULT NULL,
  `customer_city` varchar(32) DEFAULT NULL,
  `customer_country` varchar(32) DEFAULT NULL,
  `customer_state` varchar(32) NOT NULL,
  `customer_telephone` varchar(16) DEFAULT NULL,
  `customer_cellphone` varchar(16) DEFAULT NULL,
  `customer_email` varchar(64) DEFAULT NULL,
  `customer_web` varchar(256) DEFAULT NULL,
  `customer_references` varchar(128) DEFAULT NULL,
  `customer_notes` varchar(128) DEFAULT NULL,
  `customer_delivery_place` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_customer_contacts`
--

CREATE TABLE IF NOT EXISTS `erp_customer_contacts` (
  `customer_id` int(11) NOT NULL,
  `customer_contact_id` int(11) NOT NULL,
  `customer_contact_name` varchar(256) NOT NULL,
  `customer_contact_description` varchar(128) DEFAULT NULL,
  `customer_contact_email` varchar(256) DEFAULT NULL,
  `customer_contact_telephone` varchar(32) DEFAULT NULL,
  `customer_contact_telephone2` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`customer_id`,`customer_contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `erp_employees`
--

CREATE TABLE IF NOT EXISTS `erp_employees` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(64) DEFAULT NULL,
  `employee_last_name` varchar(64) DEFAULT NULL,
  `employee_last_name2` varchar(64) DEFAULT NULL,
  `employee_initials` varchar(8) DEFAULT NULL,
  `employee_position` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`employee_id`),
  UNIQUE KEY `employee_name` (`employee_name`,`employee_last_name`,`employee_last_name2`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_events`
--

CREATE TABLE IF NOT EXISTS `erp_events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_title` varchar(256) NOT NULL,
  `event_type` int(11) DEFAULT NULL,
  `event_date` date DEFAULT NULL,
  `event_time` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `event_notes` varchar(512) DEFAULT NULL,
  `event_cdate` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_exchange_rate`
--

CREATE TABLE IF NOT EXISTS `erp_exchange_rate` (
  `exchange_rate_date` date NOT NULL,
  `exchange_rate_amount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`exchange_rate_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `erp_features`
--

CREATE TABLE IF NOT EXISTS `erp_features` (
  `feature_group_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `feature_name` varchar(64) NOT NULL,
  `feature_description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`feature_group_id`,`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `erp_feature_groups`
--

CREATE TABLE IF NOT EXISTS `erp_feature_groups` (
  `feature_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_group_name` varchar(64) NOT NULL,
  `feature_group_description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`feature_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_folios`
--

CREATE TABLE IF NOT EXISTS `erp_folios` (
  `folio_id` int(11) NOT NULL AUTO_INCREMENT,
  `folio_code` varchar(8) NOT NULL,
  `folio_number` int(11) NOT NULL,
  PRIMARY KEY (`folio_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_inventory`
--

CREATE TABLE IF NOT EXISTS `erp_inventory` (
  `inventory_id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_cdate` datetime NOT NULL,
  `inventory_end_date` date NOT NULL,
  `inventory_start_date` date NOT NULL,
  `place_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `inventory_folio` varchar(10) NOT NULL,
  `inventory_notes` varchar(500) NOT NULL,
  `inventory_status` varchar(25) NOT NULL,
  PRIMARY KEY (`inventory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_inventory_details`
--

CREATE TABLE IF NOT EXISTS `erp_inventory_details` (
  `inventory_detail_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `inventory_detail_qty` decimal(12,2) NOT NULL,
  `inventory_cdate` datetime NOT NULL,
  `um_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`inventory_detail_id`),
  UNIQUE KEY `inventory_detail_id` (`inventory_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `erp_leads`
--

CREATE TABLE IF NOT EXISTS `erp_leads` (
  `lead_id` int(11) NOT NULL AUTO_INCREMENT,
  `lead_name` varchar(64) NOT NULL,
  `lead_address` varchar(128) DEFAULT NULL,
  `lead_city` varchar(32) DEFAULT NULL,
  `lead_country` varchar(32) DEFAULT NULL,
  `lead_state` varchar(32) NOT NULL,
  `lead_telephone` varchar(16) DEFAULT NULL,
  `lead_cellphone` varchar(16) DEFAULT NULL,
  `lead_email` varchar(64) DEFAULT NULL,
  `lead_notes` varchar(128) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`lead_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `erp_materials`
--

CREATE TABLE IF NOT EXISTS `erp_materials` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(128) DEFAULT NULL,
  `material_category_id` int(11) DEFAULT NULL,
  `material_stock` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_material_categories`
--

CREATE TABLE IF NOT EXISTS `erp_material_categories` (
  `material_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_category_name` varchar(64) NOT NULL,
  `material_category_show_in_products` tinyint(1) NOT NULL,
  PRIMARY KEY (`material_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `erp_movements`
--

CREATE TABLE IF NOT EXISTS `erp_movements` (
  `movement_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) NOT NULL DEFAULT '0',
  `sale_order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `um_id` int(11) NOT NULL,
  `movement_qty` decimal(12,2) NOT NULL,
  `movement_cdate` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `movement_type` int(11) NOT NULL,
  `movement_active` tinyint(1) NOT NULL,
  `movement_um` varchar(64) NOT NULL,
  `movement_reference` varchar(250) NOT NULL,
  `movement_locator` varchar(64) NOT NULL,
  `movement_product` varchar(250) NOT NULL,
  PRIMARY KEY (`movement_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
-- --------------------------------------------------------

--
-- Table structure for table `erp_payments`
--

CREATE TABLE IF NOT EXISTS `erp_payments` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_date` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `payment_title` varchar(150) DEFAULT NULL,
  `payment_total` decimal(10,0) DEFAULT NULL,
  `payment_cdate` date NOT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `payment_exchange_rate` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `erp_payment_details`
--

CREATE TABLE IF NOT EXISTS `erp_payment_details` (
  `payment_id` int(11) NOT NULL,
  `payment_detail_id` int(11) NOT NULL,
  `payment_detail_receipt_number` varchar(16) DEFAULT NULL,
  `payment_detail_receipt_prefix` varchar(5) DEFAULT NULL,
  `payment_detail_text` varchar(128) DEFAULT NULL,
  `payment_detail_date` datetime DEFAULT NULL,
  `payment_detail_time` varchar(10) DEFAULT NULL,
  `payment_detail_amount` decimal(14,2) DEFAULT NULL,
  `payment_detail_method` varchar(64) DEFAULT NULL,
  `payment_detail_cdate` datetime DEFAULT NULL,
  `payment_detail_lmdate` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`payment_id`,`payment_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------



--
-- Table structure for table `erp_places`
--

CREATE TABLE IF NOT EXISTS `erp_places` (
  `place_id` int(11) NOT NULL AUTO_INCREMENT,
  `place_name` varchar(64) DEFAULT NULL,
  `place_description` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`place_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Table structure for table `erp_products`
--

CREATE TABLE IF NOT EXISTS `erp_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(64) DEFAULT NULL,
  `product_name` varchar(128) NOT NULL,
  `product_description` varchar(512) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `product_cost_price` decimal(10,2) DEFAULT NULL,
  `product_price` decimal(10,2) DEFAULT NULL,
  `product_stock` decimal(10,2) DEFAULT NULL,
  `product_um` varchar(16) DEFAULT NULL,
  `product_has_variants` int(1) DEFAULT NULL,
  `product_variant_product_id` int(11) DEFAULT NULL,
  `product_last_image_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `erp_product_categories`
--

CREATE TABLE IF NOT EXISTS `erp_product_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(32) NOT NULL,
  `category_description` varchar(128) DEFAULT NULL,
  `category_picture` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `erp_product_categories`
--

-- --------------------------------------------------------

--
-- Table structure for table `erp_product_features`
--

CREATE TABLE IF NOT EXISTS `erp_product_features` (
  `product_id` int(11) NOT NULL,
  `feature_group_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`feature_group_id`,`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `erp_product_pictures`
--

CREATE TABLE IF NOT EXISTS `erp_product_pictures` (
  `product_id` int(11) NOT NULL,
  `product_picture_id` int(11) NOT NULL,
  `product_picture_url` varchar(512) NOT NULL,
  `product_picture_thumbnail` varchar(512) DEFAULT NULL,
  `product_picture_default` int(1) DEFAULT NULL,
  PRIMARY KEY (`product_id`,`product_picture_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `erp_product_variants`
--

CREATE TABLE IF NOT EXISTS `erp_product_variants` (
  `product_id` int(11) NOT NULL,
  `variant_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Table structure for table `erp_product_variant_details`
--

CREATE TABLE IF NOT EXISTS `erp_product_variant_details` (
  `product_id` int(11) NOT NULL,
  `variant_id` int(4) NOT NULL,
  `variant_detail_value` varchar(32) NOT NULL,
  PRIMARY KEY (`product_id`,`variant_id`,`variant_detail_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `erp_product_variant_values`
--

CREATE TABLE IF NOT EXISTS `erp_product_variant_values` (
  `product_id` int(11) NOT NULL,
  `variant_id` int(4) NOT NULL,
  `product_variant_value` varchar(32) NOT NULL,
  PRIMARY KEY (`product_id`,`variant_id`,`product_variant_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `erp_purchase_orders`
--

CREATE TABLE IF NOT EXISTS `erp_purchase_orders` (
  `purchase_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_folio` varchar(20) DEFAULT NULL,
  `purchase_order_title` varchar(128) DEFAULT NULL,
  `purchase_order_date` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `purchase_order_status` varchar(16) DEFAULT NULL,
  `purchase_order_total` decimal(12,2) DEFAULT NULL,
  `purchase_order_cdate` datetime NOT NULL,
  `purchase_order_terms` varchar(1024) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `purchase_order_detail_last_id` int(11) DEFAULT '0',
  `currency_id` int(11) DEFAULT '1',
  `purchase_order_lmdate` datetime DEFAULT NULL,
  `purchase_order_exchange_rate` decimal(12,2) DEFAULT NULL,
  `purchase_order_subtotal` decimal(12,2) NOT NULL,
  `purchase_order_taxes` decimal(10,2) DEFAULT NULL,
  `purchase_order_discounts` decimal(10,2) DEFAULT NULL,
  `purchase_order_total_dlls` decimal(12,2) DEFAULT NULL,
  `purchase_order_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`purchase_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_purchase_order_details`
--

CREATE TABLE IF NOT EXISTS `erp_purchase_order_details` (
  `purchase_order_id` int(11) NOT NULL,
  `purchase_order_detail_id` int(4) NOT NULL,
  `purchase_order_detail_description` varchar(256) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `purchase_order_detail_qty` int(11) DEFAULT NULL,
  `purchase_order_detail_um` varchar(16) DEFAULT NULL,
  `purchase_order_detail_cost` decimal(10,2) DEFAULT NULL,
  `purchase_order_detail_tax` decimal(10,0) DEFAULT NULL,
  `purchase_order_detail_discount` int(11) DEFAULT NULL,
  `purchase_order_detail_total` decimal(12,2) DEFAULT NULL,
  `purchase_order_detail_location` varchar(100) DEFAULT NULL,
  `purchase_order_detail_reference` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`purchase_order_id`,`purchase_order_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `erp_quotes`
--

CREATE TABLE IF NOT EXISTS `erp_quotes` (
  `quote_id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_folio` varchar(20) DEFAULT NULL,
  `quote_title` varchar(128) DEFAULT NULL,
  `quote_date` date DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `quote_status` varchar(16) DEFAULT NULL,
  `quote_total` decimal(12,2) DEFAULT NULL,
  `quote_cdate` datetime NOT NULL,
  `quote_terms` varchar(1024) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `quote_detail_last_id` int(11) DEFAULT '0',
  `currency_id` int(11) DEFAULT '1',
  `quote_lmdate` datetime DEFAULT NULL,
  `quote_exchange_rate` decimal(12,2) DEFAULT NULL,
  `quote_subtotal` decimal(12,2) NOT NULL,
  `quote_taxes` decimal(10,2) DEFAULT NULL,
  `quote_discounts` decimal(10,2) DEFAULT NULL,
  `quote_total_dlls` decimal(12,2) DEFAULT NULL,
  `quote_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`quote_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `erp_quote_attachments`
--

CREATE TABLE IF NOT EXISTS `erp_quote_attachments` (
  `quote_id` int(11) NOT NULL,
  `quote_attachment_id` int(11) NOT NULL,
  `quote_attachment_name` varchar(256) DEFAULT NULL,
  `quote_attachment_description` varchar(512) DEFAULT NULL,
  `quote_attachment_file_name` varchar(512) DEFAULT NULL,
  `quote_attachment_date` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`quote_id`,`quote_attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `erp_quote_details`
--

CREATE TABLE IF NOT EXISTS `erp_quote_details` (
  `quote_id` int(11) NOT NULL,
  `quote_detail_id` int(4) NOT NULL,
  `quote_detail_description` varchar(256) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quote_detail_qty` int(11) DEFAULT NULL,
  `quote_detail_um` varchar(16) DEFAULT NULL,
  `quote_detail_price` decimal(10,2) DEFAULT NULL,
  `quote_detail_tax` decimal(10,0) DEFAULT NULL,
  `quote_detail_discount` int(11) DEFAULT NULL,
  `quote_detail_total` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`quote_id`,`quote_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `erp_sale_orders`
--

CREATE TABLE IF NOT EXISTS `erp_sale_orders` (
  `sale_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_id` int(11) DEFAULT NULL,
  `sale_order_folio` varchar(20) DEFAULT NULL,
  `sale_order_title` varchar(128) DEFAULT NULL,
  `sale_order_description` varchar(1024) DEFAULT NULL,
  `sale_order_date` date DEFAULT NULL,
  `sale_order_due_date` date DEFAULT NULL,
  `sale_order_delivery` smallint(6) DEFAULT NULL,
  `sale_order_delivery_date` date DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `sale_order_status` varchar(16) DEFAULT NULL,
  `sale_order_total` decimal(12,2) DEFAULT NULL,
  `sale_order_cdate` datetime DEFAULT NULL,
  `sale_order_terms` varchar(1024) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sale_order_detail_last_id` int(11) DEFAULT '0',
  `sale_order_lmdate` date DEFAULT NULL,
  `currency_id` int(11) DEFAULT '1',
  `sale_order_payment_type` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `place_id` int(11) DEFAULT NULL,
  `sale_order_exchange_rate` decimal(12,2) DEFAULT NULL,
  `sale_order_subtotal` decimal(12,2) DEFAULT NULL,
  `sale_order_taxes` decimal(10,2) DEFAULT NULL,
  `sale_order_discounts` decimal(10,2) DEFAULT NULL,
  `sale_order_total_dlls` decimal(12,2) DEFAULT NULL,
  `contract_id` int(11) DEFAULT NULL,
  `contract_printed` int(1) DEFAULT NULL,
  `tax_id` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`sale_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_sale_order_details`
--

CREATE TABLE IF NOT EXISTS `erp_sale_order_details` (
  `sale_order_id` int(11) NOT NULL,
  `sale_order_detail_id` int(4) NOT NULL,
  `sale_order_detail_description` varchar(256) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sale_order_detail_qty` int(11) DEFAULT NULL,
  `sale_order_detail_um` varchar(16) DEFAULT NULL,
  `sale_order_detail_price` decimal(10,2) DEFAULT NULL,
  `sale_order_detail_tax` decimal(10,0) DEFAULT NULL,
  `sale_order_detail_discount` int(11) DEFAULT NULL,
  `sale_order_detail_total` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`sale_order_id`,`sale_order_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `erp_sale_order_payments`
--

CREATE TABLE IF NOT EXISTS `erp_sale_order_payments` (
  `sale_order_id` int(11) NOT NULL,
  `sale_order_payment_id` int(11) NOT NULL,
  `sale_order_payment_receipt_number` varchar(16) DEFAULT NULL,
  `sale_order_payment_receipt_prefix` varchar(5) DEFAULT NULL,
  `sale_order_payment_text` varchar(128) DEFAULT NULL,
  `sale_order_payment_date` datetime DEFAULT NULL,
  `sale_order_payment_time` varchar(10) DEFAULT NULL,
  `sale_order_payment_amount` decimal(14,2) DEFAULT NULL,
  `sale_order_payment_method` varchar(64) DEFAULT NULL,
  `sale_order_payment_cdate` datetime DEFAULT NULL,
  `sale_order_payment_lmdate` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`sale_order_id`,`sale_order_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `erp_suppliers`
--

CREATE TABLE IF NOT EXISTS `erp_suppliers` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(64) DEFAULT NULL,
  `supplier_rfc` varchar(16) DEFAULT NULL,
  `supplier_telephone` varchar(16) DEFAULT NULL,
  `supplier_email` varchar(128) DEFAULT NULL,
  `supplier_address` varchar(256) DEFAULT NULL,
  `supplier_cp` varchar(8) DEFAULT NULL,
  `supplier_city` varchar(32) DEFAULT NULL,
  `supplier_country` varchar(32) DEFAULT NULL,
  `supplier_notes` varchar(128) DEFAULT NULL,
  `supplier_web` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `erp_ums`
--

CREATE TABLE IF NOT EXISTS `erp_ums` (
  `um_id` int(11) NOT NULL AUTO_INCREMENT,
  `um_name` varchar(16) NOT NULL,
  `um_abbreviation` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`um_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `erp_ums`
--

INSERT INTO `erp_ums` (`um_id`, `um_name`, `um_abbreviation`) VALUES
(1, 'Pieza', 'Pz'),
(2, 'Lote', NULL),


-- --------------------------------------------------------

--
-- Table structure for table `erp_variants`
--


CREATE TABLE IF NOT EXISTS `erp_variants` (
  `variant_id` int(4) NOT NULL AUTO_INCREMENT,
  `variant_name` varchar(32) NOT NULL,
  PRIMARY KEY (`variant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_variant_values`
--

CREATE TABLE IF NOT EXISTS `erp_variant_values` (
  `variant_id` int(11) NOT NULL,
  `variant_value_id` int(11) NOT NULL,
  `variant_value_name` varchar(64) DEFAULT NULL,
  `variant_value_picture_url` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`variant_id`,`variant_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `erp_work_orders`
--

CREATE TABLE IF NOT EXISTS `erp_work_orders` (
  `work_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `work_order_number` varchar(32) DEFAULT NULL,
  `work_order_title` varchar(128) DEFAULT NULL,
  `work_order_creation_date` date DEFAULT NULL,
  `work_order_delivery_date` date DEFAULT NULL,
  `work_order_supervisor` int(11) DEFAULT NULL,
  `work_order_status` varchar(16) DEFAULT NULL,
  `work_order_priority` varchar(16) DEFAULT NULL,
  `work_order_notes` varchar(512) DEFAULT NULL,
  `sale_order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`work_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `erp_work_order_expenses`
--

CREATE TABLE IF NOT EXISTS `erp_work_order_expenses` (
  `work_order_id` int(11) NOT NULL,
  `work_order_expense_id` int(11) NOT NULL,
  `work_order_expense_receipt_number` varchar(16) DEFAULT NULL,
  `work_order_expense_receipt_prefix` varchar(5) DEFAULT NULL,
  `work_order_expense_text` varchar(128) DEFAULT NULL,
  `work_order_expense_date` datetime DEFAULT NULL,
  `work_order_expense_time` varchar(10) DEFAULT NULL,
  `work_order_expense_amount` decimal(14,2) DEFAULT NULL,
  `work_order_expense_method` varchar(64) DEFAULT NULL,
  `work_order_expense_cdate` datetime DEFAULT NULL,
  `work_order_expense_lmdate` datetime DEFAULT NULL,
  `currency_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`work_order_id`,`work_order_expense_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `erp_work_order_materials`
--

CREATE TABLE IF NOT EXISTS `erp_work_order_materials` (
  `work_order_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `material_qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `erp_work_order_notes`
--

CREATE TABLE IF NOT EXISTS `erp_work_order_notes` (
  `work_order_id` int(11) NOT NULL,
  `work_order_note_id` int(11) NOT NULL,
  `work_order_note_text` varchar(512) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `work_order_note_cdate` datetime DEFAULT NULL,
  PRIMARY KEY (`work_order_id`,`work_order_note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `erp_work_order_technicians`
--

CREATE TABLE IF NOT EXISTS `erp_work_order_technicians` (
  `work_order_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`work_order_id`,`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` char(40) NOT NULL,
  `secret` char(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `auto_approve` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_oacl_clse_clid` (`secret`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `secret`, `name`, `auto_approve`) VALUES
('2Kv9vbSb9yc311L3H9k4wPPJXna9o586', 'QIWaZ82U9vHs7p2G2J6OGHTgGcK3y9oz', 'Merofa ERP', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_client_endpoints`
--

CREATE TABLE IF NOT EXISTS `oauth_client_endpoints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` char(40) NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i_oaclen_clid` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_scopes` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `scope` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_oasc_sc` (`scope`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `oauth_scopes`
--

INSERT INTO `oauth_scopes` (`id`, `scope`, `name`, `description`) VALUES
(1, 'access', 'Acceso a la aplicacion', NULL),
(2, 'clients', 'Accesso a clientes', NULL),
(7, 'sales.view', 'Modulo de Ventas - Consulta', NULL),
(8, 'sales.edit', 'Modulo de Ventas - Edicion', NULL),
(9, 'maintainance.view', 'Catalogos - Consulta', NULL),
(10, 'maintainance.edit', 'Catalogos - Edicion', NULL),
(11, 'inventory.view', 'Inventarios - Consulta', NULL),
(12, 'inventory.edit', 'Inventarios - Edicion', NULL),
(13, 'collect.view', 'Cobranza - Consulta', NULL),
(14, 'collect.edit', 'Cobranza - Edicion', NULL),
(15, 'work.view', 'Trabajos - Consulta', NULL),
(16, 'work.edit', 'Trabajos - Edicion', NULL),
(17, 'reports.view', 'Reportes - Consulta', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_sessions`
--

CREATE TABLE IF NOT EXISTS `oauth_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` char(40) NOT NULL,
  `owner_type` enum('user','client') NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i_uase_clid_owty_owid` (`client_id`,`owner_type`,`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1575 ;


-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_access_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_session_access_tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(10) unsigned NOT NULL,
  `access_token` char(40) NOT NULL,
  `access_token_expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_oaseacto_acto_seid` (`access_token`,`session_id`),
  KEY `f_oaseto_seid` (`session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1574 ;


-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_authcodes`
--

CREATE TABLE IF NOT EXISTS `oauth_session_authcodes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(10) unsigned NOT NULL,
  `auth_code` char(40) NOT NULL,
  `auth_code_expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_authcode_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_session_authcode_scopes` (
  `oauth_session_authcode_id` int(10) unsigned NOT NULL,
  `scope_id` smallint(5) unsigned NOT NULL,
  KEY `oauth_session_authcode_id` (`oauth_session_authcode_id`),
  KEY `scope_id` (`scope_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_redirects`
--

CREATE TABLE IF NOT EXISTS `oauth_session_redirects` (
  `session_id` int(10) unsigned NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_refresh_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_session_refresh_tokens` (
  `session_access_token_id` int(10) unsigned NOT NULL,
  `refresh_token` char(40) NOT NULL,
  `refresh_token_expires` int(10) unsigned NOT NULL,
  `client_id` char(40) NOT NULL,
  PRIMARY KEY (`session_access_token_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `oauth_session_token_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_session_token_scopes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `session_access_token_id` int(10) unsigned DEFAULT NULL,
  `scope_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_setosc_setoid_scid` (`session_access_token_id`,`scope_id`),
  KEY `f_oasetosc_scid` (`scope_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2044 ;


-- --------------------------------------------------------

--
-- Table structure for table `oauth_users`
--

CREATE TABLE IF NOT EXISTS `oauth_users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) NOT NULL,
  `user_email` varchar(128) NOT NULL,
  `user_password` varchar(512) NOT NULL,
  `user_is_admin` int(5) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `oauth_users`
--

INSERT INTO `oauth_users` (`user_id`, `user_name`, `user_email`, `user_password`, `user_is_admin`) VALUES
(2, 'Pablo', 'bretado.up@gmail.com', '$2y$10$WIP5jlqsl8NI.Bs2v.2.7eWxhtwrl3IbwRVbbNknaw6bezgQuhZ/W', 1),
(46, 'Ivan Altamirano', 'ivan.altamirano@gmail.com', '$2y$10$frf9T8X4pv/utT/DF0VHuuJNab6IpEXvxOblxSOXsR3BAE3oO9m9m', 1),
(52, 'Fadrique Medina', 'ventasfad@icloud.com', '$2y$10$aLR23qBw63BCEsxzmIfFFuad.MxDddQPtsfv0nnqkdrjbTpMbru7W', 1),
(64, 'demo', 'demo@ventamex.net', '$2y$10$SFfmi6L/Fp1vRkicocUjHuWCyneyBOXTGEWg996OYcRFHUKeunXV6', 0),
(65, 'juancarlos', 'jc@ventamex.net', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_user_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_user_scopes` (
  `user_id` int(11) NOT NULL,
  `scope_id` int(5) NOT NULL,
  PRIMARY KEY (`user_id`,`scope_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_user_scopes`
--

INSERT INTO `oauth_user_scopes` (`user_id`, `scope_id`) VALUES
(51, 1),
(52, 1),
(52, 2),
(52, 7),
(52, 8),
(52, 9),
(52, 10),
(52, 11),
(52, 12),
(52, 13),
(52, 14),
(52, 15),
(52, 16),
(52, 17),
(62, 1),
(62, 2),
(62, 7),
(62, 8),
(62, 13),
(62, 15),
(63, 15),
(64, 2),
(64, 9),
(64, 10),
(65, 11);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  ADD CONSTRAINT `f_oaclen_clid` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  ADD CONSTRAINT `f_oase_clid` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `oauth_session_access_tokens`
--
ALTER TABLE `oauth_session_access_tokens`
  ADD CONSTRAINT `f_oaseto_seid` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `oauth_session_authcodes`
--
ALTER TABLE `oauth_session_authcodes`
  ADD CONSTRAINT `oauth_session_authcodes_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_session_authcode_scopes`
--
ALTER TABLE `oauth_session_authcode_scopes`
  ADD CONSTRAINT `oauth_session_authcode_scopes_ibfk_1` FOREIGN KEY (`oauth_session_authcode_id`) REFERENCES `oauth_session_authcodes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_session_authcode_scopes_ibfk_2` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_session_redirects`
--
ALTER TABLE `oauth_session_redirects`
  ADD CONSTRAINT `f_oasere_seid` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `oauth_session_refresh_tokens`
--
ALTER TABLE `oauth_session_refresh_tokens`
  ADD CONSTRAINT `f_oasetore_setoid` FOREIGN KEY (`session_access_token_id`) REFERENCES `oauth_session_access_tokens` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `oauth_session_refresh_tokens_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `oauth_session_token_scopes`
--
ALTER TABLE `oauth_session_token_scopes`
  ADD CONSTRAINT `f_oasetosc_scid` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `f_oasetosc_setoid` FOREIGN KEY (`session_access_token_id`) REFERENCES `oauth_session_access_tokens` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
